package com.prestashopemc.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import com.prestashopemc.R;
import com.prestashopemc.adapter.ReviewRatingsAdapter;
import com.prestashopemc.controller.WriteReviewController;
import com.prestashopemc.customView.CustomTextView;
import com.prestashopemc.model.RatingMainModel;
import com.prestashopemc.model.RatingResult;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.VolleyCallback;
import com.prestashopemc.view.ProductDetailActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * <h1>Review Fragment!</h1>
 * The Review Fragment contains product reviews details..
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 1 /4/16
 */
public class ReviewFragment extends Fragment implements View.OnClickListener {

    private ReviewRatingsAdapter mReviewAdapter;
    private RecyclerView mReviewListview;
    private LinearLayoutManager mLayoutManager;
    private ProductDetailActivity mPrdDetailActivity;
    private List<RatingResult> mRatingresult;
    private Button mReviewBtn;
    private CustomTextView mProductNameText, mTitleLabel;
    private EditText mSummaryReview, mYourReview;
    private String mProductId, mSummaryText, mReviewText, mValue, mProductName;
    private WriteReviewController mWriteReviewController;
    private RatingMainModel mRatingMainModel;
    private JSONObject mObjectRating = new JSONObject();

    /**
     * This method will call when creating fragment view.
     *
     * @param inflater           This is the first parameter to onCreateView method.
     * @param container          This is the second parameter to onCreateView method.
     * @param savedInstanceState This is the third parameter to onCreateView method.
     * @return View returs layout view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.review_ratings, container, false);
    }

    /**
     * This method will call when fragment view is created..
     *
     * @param view               This is the first parameter to onViewCreated method.
     * @param savedInstanceState This is the first parameter to onViewCreated method.
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mPrdDetailActivity = (ProductDetailActivity) getActivity();
        mWriteReviewController = new WriteReviewController(getActivity());
        mWriteReviewController.gettingReviewOption(getRatingResponse);
        mProductId = mPrdDetailActivity.productId;
        mProductName = mPrdDetailActivity.productName;
        mProductNameText = (CustomTextView) view.findViewById(R.id.product_name);
        mProductNameText.setText(mProductName);
        initialization(view);
    }

    /**
     * This method is used to Initialized Layout
     *
     * @param view This is the parameter to initializedLayout method.
     */
    private void initialization(View view) {
        //prdDetailActivity.showProgDialiog();
        mReviewListview = (RecyclerView) view.findViewById(R.id.review_recycler_view);
        mReviewBtn = (Button) view.findViewById(R.id.review_submit);
        mTitleLabel = (CustomTextView) view.findViewById(R.id.ratings_reviews);
        mSummaryReview = (EditText) view.findViewById(R.id.summary_review);
        mYourReview = (EditText) view.findViewById(R.id.your_review);
        CustomBackground.setBackgroundText(mReviewBtn);
        mLayoutManager = new LinearLayoutManager(getActivity());
        mReviewListview.setLayoutManager(mLayoutManager);
        mReviewListview.setHasFixedSize(true);
        dynamicTextValue();
        mSummaryReview.setFilters(new InputFilter[]{
                new InputFilter() {
                    @Override
                    public CharSequence filter(CharSequence cs, int start,
                                               int end, Spanned spanned, int dStart, int dEnd) {
                        // TODO Auto-generated method stub
                        if (cs.equals("")) { // for backspace
                            return cs;
                        }
                        if (cs.toString().matches("[a-zA-Z ]+")) {
                            return cs;
                        }
                        return "";
                    }
                }
        });
        mYourReview.setFilters(new InputFilter[]{
                new InputFilter() {
                    @Override
                    public CharSequence filter(CharSequence cs, int start,
                                               int end, Spanned spanned, int dStart, int dEnd) {
                        // TODO Auto-generated method stub
                        if (cs.equals("")) { // for backspace
                            return cs;
                        }
                        if (cs.toString().matches("[a-zA-Z ]+")) {
                            return cs;
                        }
                        return "";
                    }
                }
        });
        mReviewBtn.setOnClickListener(this);
        setCustomFont();
    }

    /**
     * This method is used to Dynamic Text Value Settings
     */
    private void dynamicTextValue() {
        mTitleLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.rateThisProductText));
        mSummaryReview.setHint(AppConstants.getTextString(getActivity(), AppConstants.summaryText));
        mYourReview.setHint(AppConstants.getTextString(getActivity(), AppConstants.reviewText));
        mReviewBtn.setText(AppConstants.getTextString(getActivity(), AppConstants.submitReviewText));
    }

    /**
     * This method is used to Set Custom Font
     */
    private void setCustomFont() {
        mProductNameText.setTypeface(mPrdDetailActivity.robotoRegular);
        mTitleLabel.setTypeface(mPrdDetailActivity.robotoLight);
        mSummaryReview.setTypeface(mPrdDetailActivity.robotoLight);
        mYourReview.setTypeface(mPrdDetailActivity.robotoLight);
        mReviewBtn.setTypeface(mPrdDetailActivity.robotoMedium);
    }

    /**
     * This method is used to API Response From WriteReviewController
     */
    VolleyCallback getRatingResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                mRatingMainModel = (MyApplication.getGsonInstance().fromJson(response, RatingMainModel.class));
                mRatingresult = mRatingMainModel.getResult();
                mReviewAdapter = new ReviewRatingsAdapter(getActivity(), mRatingresult);
                mReviewListview.setAdapter(mReviewAdapter);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((ProductDetailActivity) getActivity()).getratingFailure(AppConstants.getTextString(getActivity(), AppConstants.reviewSubmittedAlertText));
        }
    };

    /**
     * This method will call when click event occured.
     * @param v This is the parameter to onClick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * Submit Review Listener*/
            case R.id.review_submit:
                if (mPrdDetailActivity.productDetail.getGuestComment() ||
                        !SharedPreference.getInstance().getValue("customerid").equals("0")) {
                    mObjectRating = mReviewAdapter.mRatingObject;
                    if (mObjectRating.length() == 0) {
                        mPrdDetailActivity.getratingFailure(AppConstants.getTextString(getActivity(), AppConstants.rateSelectText));
                    } else if (mSummaryReview.getText().toString().length() == 0) {
                        mPrdDetailActivity.getratingFailure(AppConstants.getTextString(getActivity(), AppConstants.rateSummaryText));
                    } else if (mYourReview.getText().toString().length() == 0) {
                        mPrdDetailActivity.getratingFailure(AppConstants.getTextString(getActivity(), AppConstants.rateYourReviewText));
                    } else {
                        // prdDetailActivity.showProgDialiog();
                        mSummaryText = AppConstants.stringEncode(mSummaryReview.getText().toString());
                        mReviewText = AppConstants.stringEncode(mYourReview.getText().toString());
                        JSONObject objectValue = new JSONObject();
                        try {
                            objectValue.put("ratings", mObjectRating);
                            objectValue.put("nickname", "Test");
                            objectValue.put("validate_rating", "");
                            objectValue.put("title", mSummaryText);
                            objectValue.put("detail", mReviewText);
                            mValue = objectValue.toString();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //Log.e("Write rating value ======= ",mValue );
                        mWriteReviewController.submitReview(mProductId, mValue);
                        mSummaryReview.getText().clear();
                        mYourReview.getText().clear();
                    }
                } else {
                    mPrdDetailActivity.snackBar(AppConstants.getTextString(getActivity(), AppConstants.needLoginText));
                }
                break;
        }
    }
}
