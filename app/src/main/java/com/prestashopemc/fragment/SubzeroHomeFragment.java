package com.prestashopemc.fragment;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.adapter.HomePageBanner;
import com.prestashopemc.adapter.ViewPagerAdapter;
import com.prestashopemc.customView.CustomEdittext;
import com.prestashopemc.database.RecentSearchService;
import com.prestashopemc.model.HomePageList;
import com.prestashopemc.model.Imagelist;
import com.prestashopemc.model.RecentSearchModel;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.view.MainActivity;
import com.prestashopemc.view.SearchActivity;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Rajesh on 22/09/16.
 */
public class SubzeroHomeFragment extends Fragment {

    private MainActivity activity;
    private RelativeLayout mSubzeroSearchLayout;
    private CustomEdittext mSubzeroSearchEdit;
    private ImageView mSubzeroMainImage;
    private TabLayout mSubzeroTabLayout;
    private ViewPager mSubzeroViewPager;
    private RecyclerView mSubzeroBanner;
    private List<Imagelist> subBannerList;
    private List<HomePageList> itemList;
    private HomePageBanner homePageBanner;
    private LinearLayoutManager mLinearLayout;
    private CollapsingToolbarLayout collapsingToolbar;
    private RecentSearchModel recentSearchModel;
    private RecentSearchService recentSearchService;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.subzero_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity = (MainActivity) getActivity();
        //activity.updateCart();

        collapsingToolbar = (CollapsingToolbarLayout)view.findViewById(R.id.collapse_toolbar);
        collapsingToolbar.setTitleEnabled(false);

        initializeLayout(view);

    }

    /** Setup View Pager **/

    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getFragmentManager());
        adapter.addFragment(new SubzeroSubFragment(), getString(R.string.popularText));
        adapter.addFragment(new NewArrivalsFragment(), getString(R.string.sortNewArrivalsText));
        adapter.addFragment(new BestSellerFragment(), getString(R.string.specialsText));
        viewPager.setAdapter(adapter);
        if ((getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {
            mSubzeroTabLayout.setTabMode(TabLayout.MODE_FIXED);
        } else if ((getResources().getConfiguration().screenLayout &
                Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            mSubzeroTabLayout.setTabMode(TabLayout.MODE_FIXED);
        } else {
            mSubzeroTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        }
        mSubzeroTabLayout.setupWithViewPager(mSubzeroViewPager);
        mSubzeroTabLayout.addOnTabSelectedListener(tabSelectedListener);

    }

    /**
     * The Tab selected listener.
     */
    TabLayout.OnTabSelectedListener tabSelectedListener = new TabLayout.OnTabSelectedListener() {
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            mSubzeroViewPager.setCurrentItem(tab.getPosition());
        }

        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    /* Initialized Layout*/

    private void initializeLayout(View view) {
        recentSearchService = new RecentSearchService(getActivity());
        mSubzeroSearchEdit = (CustomEdittext)view.findViewById(R.id.subzero_search_edit);
        mSubzeroSearchEdit.setHint(getString(R.string.subzeroSearchText));
        mSubzeroTabLayout = (TabLayout)view.findViewById(R.id.subzero_tab_layout);
        mSubzeroViewPager = (ViewPager)view.findViewById(R.id.subzero_viewpager);
        mLinearLayout = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        mSubzeroBanner = (RecyclerView)view.findViewById(R.id.subzer_home_banner);
        mSubzeroBanner.setLayoutManager(mLinearLayout);
        mSubzeroBanner.smoothScrollToPosition(0);
        mSubzeroBanner.setHasFixedSize(true);
        mSubzeroBanner.setNestedScrollingEnabled(true);
        Log.e("Theme Color == ",  CustomBackground.mThemeColor);

        mSubzeroTabLayout.setSelectedTabIndicatorColor(Color.parseColor(CustomBackground.mThemeColor));

        /*Search Click Listener*/
        mSubzeroSearchEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                        || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    if (!mSubzeroSearchEdit.getText().toString().trim().isEmpty()) {
                        activity.showProgDialiog();
                        recentSearchModel = new RecentSearchModel();
                        recentSearchModel.setSearchKeyword(mSubzeroSearchEdit.getText().toString().trim());
                        List<RecentSearchModel> recentSearchModels = new ArrayList<RecentSearchModel>();
                        recentSearchModels.add(recentSearchModel);
                        try {
                            recentSearchService.insertRecentSearch(recentSearchModels);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Intent searchIntent = new Intent(getActivity(), SearchActivity.class);
                        searchIntent.putExtra("search_text", mSubzeroSearchEdit.getText().toString().trim());
                        mSubzeroSearchEdit.getText().clear();
                        startActivity(searchIntent);

                    } else {
                        activity.snackBar(getString(R.string.searchKeywordText));
                    }
                }
                return false;
            }
        });

        setCustomFont();
        loadBanner();
        setupViewPager(mSubzeroViewPager);

    }

    /*Set Custom Font*/
    private void setCustomFont() {
        mSubzeroSearchEdit.setTypeface(activity.robotoLight);
        ViewGroup vg = (ViewGroup) mSubzeroTabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(activity.robotoRegular);
                }
            }
        }
    }

    /* Home Banners Loading*/

    private void loadBanner() {

        if (activity.mainPageList != null){
            itemList = activity.mainPageList;

            if (itemList.size() != 0) {
               // homePageBanner = new HomePageBanner(getActivity(), activity.mainPageList.get(0).getImagelist());
                mSubzeroBanner.setAdapter(homePageBanner);
                mSubzeroBanner.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
                    @Override
                    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                        int action = e.getAction();
                        switch (action) {
                            case MotionEvent.ACTION_MOVE:
                                rv.getParent().requestDisallowInterceptTouchEvent(false);
                                break;
                        }
                        return false;
                    }

                    @Override
                    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

                    }

                    @Override
                    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

                    }
                });
            }
        }


    }
}
