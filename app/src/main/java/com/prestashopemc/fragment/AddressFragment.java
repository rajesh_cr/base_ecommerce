package com.prestashopemc.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.prestashopemc.R;
import com.prestashopemc.model.Address;
import com.prestashopemc.model.AddressValues;
import com.prestashopemc.model.MagentoDefaultAddressShipping;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.view.AddNewAddress;
import com.prestashopemc.view.ChangeAddress;
import com.prestashopemc.view.CheckoutActivity;

/**
 * <h1>Address Fragment!</h1>
 * The Address Fragment contains shipping and billing address and edit delete address.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class AddressFragment extends Fragment implements View.OnClickListener {

    private TextView mShippingLabel, mBillingLabel, mShippingName, mBillingName, mShippingAddress, mBillingAddress, mShippingChange, mBillingChange;
    private CheckoutActivity mCheckoutActivity;
    private Address mAddressModel;
    private MagentoDefaultAddressShipping mMagentoDefaultAddressShipping;
    private String mName, mAddress, mBillName, mBillAddress;
    private View mView;

    /**
     * This method is used to create view for fragment.
     * @param inflater This is the first parameter to onCreateView method.
     * @param container This is the second parameter to onCreateView method.
     * @param savedInstanceState This is the third parameter to onCreateView method.
     * @return View returs layout view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (mView == null) {
            mView = inflater.inflate(R.layout.address_fragment, container, false);
            initalizedLayout(mView);
            checkoutGoogleAnalytics();
        }
        return mView;
    }

    /**
     * This method is used to created view for fragment.
     * @param view This is the first parameter to onViewCreated method.
     * @param savedInstanceState This is the first parameter to onViewCreated method.
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    /**
     * This method is used to Set google analytics checkout tracking
     */
    private void checkoutGoogleAnalytics() {
        // Add the step number and additional info about the checkout to the action.
        ProductAction productAction = new ProductAction(ProductAction.ACTION_CHECKOUT)
                .setCheckoutStep(1)
                .setCheckoutOptions("Address");
        HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder()
                .setProductAction(productAction);

        GoogleAnalytics googleAnalytics = GoogleAnalytics.getInstance(getActivity());
        //Tracker sTracker = googleAnalytics.newTracker(R.xml.app_tracker);
        Tracker sTracker = googleAnalytics.newTracker(SharedPreference.getInstance().getValue("google_tracking"));
        sTracker.setScreenName("Address Checkout");
        sTracker.send(builder.build());
    }

    /**
     *This method is used to Initalized Layout
     * @param view This is the parameter to initalizedLayout method.
     */
    private void initalizedLayout(View view) {
        mCheckoutActivity = (CheckoutActivity) getActivity();
        mShippingLabel = (TextView) view.findViewById(R.id.shipping_addr);
        mBillingLabel = (TextView) view.findViewById(R.id.billing_addr);
        mShippingName = (TextView) view.findViewById(R.id.shipping_name);
        mBillingName = (TextView) view.findViewById(R.id.billing_name);
        mShippingAddress = (TextView) view.findViewById(R.id.ship_address);
        mBillingAddress = (TextView) view.findViewById(R.id.bill_address);
        mShippingChange = (TextView) view.findViewById(R.id.change_address);
        mBillingChange = (TextView) view.findViewById(R.id.billing_change_address);
        dynamicTextValue();
        setCustomFont();
        mShippingChange.setOnClickListener(this);
        mBillingChange.setOnClickListener(this);
        gettingValues();
    }

    /**
     * This method is used to Dynamic Text Value Settings
     */
    private void dynamicTextValue() {
        mShippingLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.checkoutShippingAddressText));
        mShippingChange.setText(AppConstants.getTextString(getActivity(), AppConstants.changeText));
        mBillingLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.billingAddressText));
        mBillingChange.setText(AppConstants.getTextString(getActivity(), AppConstants.changeText));
    }

    /**
     * This method is used to Getting Address Details From CheckoutActivity
     */
    private void gettingValues() {

        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            mMagentoDefaultAddressShipping = mCheckoutActivity.magentoDefaultAddressShipping;
            if (mMagentoDefaultAddressShipping != null){
                mName = mMagentoDefaultAddressShipping.getFirstname() + " " + mMagentoDefaultAddressShipping.getLastname();
                mAddress = mMagentoDefaultAddressShipping.getStreet() + ", " + mMagentoDefaultAddressShipping.getCity() + "\n"
                        + mMagentoDefaultAddressShipping.getCountryId() + "," + mMagentoDefaultAddressShipping.getPostcode() + "\n"
                        + mMagentoDefaultAddressShipping.getTelephone();
                mBillName = mName;
                mBillAddress = mAddress;
            }else {
                savedAddressValues();
            }
        } else {
            mAddressModel = mCheckoutActivity.addressmodel;
            if (mAddressModel != null) {
                mName = mAddressModel.getFirstname() + " " + mAddressModel.getLastname();
                mAddress = mAddressModel.getStreet() + ", " + mAddressModel.getCity() + "\n" + mAddressModel.getCountry() + "," + mAddressModel.getPostcode() + "\n" + mAddressModel.getTelephone();
                mBillName = mName;
                mBillAddress = mAddress;
            } else {
                savedAddressValues();
            }
        }

        changeAddr();
    }

    /**
     * This method is used to Saved Address values from SharedPreference
     */
    private void savedAddressValues() {
        String shipObj = SharedPreference.getInstance().getValue("shippingAddressValues");
        AddressValues shippingValues = MyApplication.getGsonInstance().fromJson(shipObj, AddressValues.class);
        String mShipStreet = shippingValues.getStreet();
        String mShipCity = shippingValues.getCity();
        mName = shippingValues.getFname() + " " + shippingValues.getLname();
        mAddress = mShipStreet.replace("+", " ") + ", " + mShipCity.replace("+", " ") + "\n"
                + shippingValues.getCountry() + ", " + shippingValues.getZipcode() + "\n"
                + shippingValues.getPhone();

        String billObj = SharedPreference.getInstance().getValue("billingAddressValues");
        AddressValues billingValues = MyApplication.getGsonInstance().fromJson(billObj, AddressValues.class);
        String mBillStreet = billingValues.getStreet();
        String mBillCity = billingValues.getCity();
        mBillName = billingValues.getFname() + " " + billingValues.getLname();
        mBillAddress = mBillStreet.replace("+", " ") + ", " + mBillCity.replace("+", " ") + "\n"
                + billingValues.getCountry() + ", " + billingValues.getZipcode() + "\n"
                + billingValues.getPhone();
    }

    /**
     * This method is used to Shipping and Billing Address change
     */
    private void changeAddr() {
        if (mCheckoutActivity.name != null) {

            if (!SharedPreference.getInstance().getValue("shipping_name").equals("0") ||
                    !SharedPreference.getInstance().getValue("billing_name").equals("0")) {
                if (!SharedPreference.getInstance().getValue("shipping_name").equals("0") &&
                        !SharedPreference.getInstance().getValue("billing_name").equals("0")) {
                    mShippingName.setText(SharedPreference.getInstance().getValue("shipping_name"));
                    mShippingAddress.setText(SharedPreference.getInstance().getValue("shipping_address"));
                    mBillingName.setText(SharedPreference.getInstance().getValue("billing_name"));
                    mBillingAddress.setText(SharedPreference.getInstance().getValue("billing_address"));
                } else if (!SharedPreference.getInstance().getValue("shipping_name").equals("0")) {
                    mShippingName.setText(SharedPreference.getInstance().getValue("shipping_name"));
                    mShippingAddress.setText(SharedPreference.getInstance().getValue("shipping_address"));
                    mBillingName.setText(mBillName);
                    mBillingAddress.setText(mBillAddress);
                } else {
                    mShippingName.setText(mName);
                    mShippingAddress.setText(mAddress);
                    mBillingName.setText(SharedPreference.getInstance().getValue("billing_name"));
                    mBillingAddress.setText(SharedPreference.getInstance().getValue("billing_address"));
                }
            } else {
                mShippingName.setText(mName);
                mShippingAddress.setText(mAddress);
                mBillingName.setText(mBillName);
                mBillingAddress.setText(mBillAddress);
            }


        } else {
            if (mCheckoutActivity.selectedAddress.equals("shipping") || mCheckoutActivity.selectedAddress.equals("billing")) {
                mShippingName.setText(mName);
                mShippingAddress.setText(mAddress);
                mBillingName.setText(mBillName);
                mBillingAddress.setText(mBillAddress);
            } else {
                mShippingName.setText(mName);
                mShippingAddress.setText(mAddress);
                mBillingName.setText(mName);
                mBillingAddress.setText(mAddress);
            }
        }
    }

    /**
     * This method is used to Set Custome Font
     */
    private void setCustomFont() {
        mShippingLabel.setTypeface(mCheckoutActivity.robotoMedium);
        mBillingLabel.setTypeface(mCheckoutActivity.robotoMedium);
        mShippingName.setTypeface(mCheckoutActivity.robotoRegular);
        mBillingName.setTypeface(mCheckoutActivity.robotoRegular);
        mShippingAddress.setTypeface(mCheckoutActivity.robotoLight);
        mBillingAddress.setTypeface(mCheckoutActivity.robotoLight);
        mShippingChange.setTypeface(mCheckoutActivity.robotoLight);
        mBillingChange.setTypeface(mCheckoutActivity.robotoLight);
    }

    /**
     * This method is used to destroy view.
     */
    @Override
    public void onDestroyView() {
        if (mView.getParent() != null) {
            ((ViewGroup) mView.getParent()).removeView(mView);
        }
        super.onDestroyView();
    }

    /**
     * This method is used to Calling Change Address activity
     * @param addressType This is the parameter to intentMethod.
     */
    private void intentMethod(String addressType) {
        if (!SharedPreference.getInstance().getValue("checkout").equals("checkout")) {
            Intent changeAddress = new Intent(getActivity(), ChangeAddress.class);
            changeAddress.putExtra("addressSelect", addressType);
            getActivity().finish();
            getActivity().startActivity(changeAddress);
        } else {
            Intent addNewAddress = new Intent(getActivity(), AddNewAddress.class);
            addNewAddress.putExtra("addressSelect", addressType);
            getActivity().finish();
            getActivity().startActivity(addNewAddress);
        }
    }

    /**
     * This method is used to click event.
     * @param v This is the parameter to onClick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * Shipping Change Listener
             */
            case R.id.change_address:
                intentMethod("shipping");
                break;

            /**
             * Billing Change Listener
             */
            case R.id.billing_change_address:
                intentMethod("billing");
                break;
        }
    }
}
