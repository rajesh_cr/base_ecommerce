package com.prestashopemc.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.prestashopemc.R;
import com.prestashopemc.adapter.SubzeroHomeAdapter;
import com.prestashopemc.controller.HomePageController;
import com.prestashopemc.model.SubzeroHomeModel;
import com.prestashopemc.model.SubzeroHomeResult;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.VolleyCallback;
import com.prestashopemc.view.MainActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>New Arrivals Fragment!</h1>
 * The Arrivals Fragment contains new arrival details.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author Rajesh
 * @version 1.2
 * @since 24 /09/16
 */
public class NewArrivalsFragment extends Fragment {

    private RecyclerView mHomeProductList;
    private SubzeroHomeAdapter mSubzeroAdapter;
    private GridLayoutManager mGridLayoutManger;
    private SubzeroHomeModel mSubzeroArrivalModel;
    private List<SubzeroHomeResult> mSubzeroArrivalResult = new ArrayList<SubzeroHomeResult>();
    private int mProductCount = 0;
    private HomePageController mHomePageController;
    private int mPreviousTotal = 0;
    private int mVisibleThreshold = 5;
    private int mFirstVisibleItem, mVisibleItemCount, mTotalItemCount;
    private boolean mLoading = true;
    private int mCurrentPage = 1;
    private int mTotalPageNumber = 0;
    private ProgressBar mProgressBar, mBottomProgressBar;
    private View mView;

    /**
     * This method will call when creating fragment view.
     *
     * @param inflater           This is the first parameter to onCreateView method.
     * @param container          This is the second parameter to onCreateView method.
     * @param savedInstanceState This is the third parameter to onCreateView method.
     * @return View returs layout view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (mView == null) {
            mView = inflater.inflate(R.layout.new_arrivals_fragment, container, false);
            initializeLayout(mView);
            mHomePageController = new HomePageController(getActivity());
            mHomePageController.subzeroHome(mSubzeroHome, AppConstants.sSubzeroHomeArrivals, mCurrentPage);
        }
        return mView;
    }

    /**
     * This method will call when fragment view is created..
     *
     * @param view               This is the first parameter to onViewCreated method.
     * @param savedInstanceState This is the first parameter to onViewCreated method.
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    /**
     * This method is used to Initalized Layout
     *
     * @param view This is the parameter to initalizeLayout method.
     */
    private void initializeLayout(View view) {
        mHomeProductList = (RecyclerView) view.findViewById(R.id.subzero_homeproducts_list);
        mGridLayoutManger = new GridLayoutManager(getActivity(), 2);
        mHomeProductList.setLayoutManager(mGridLayoutManger);
        mHomeProductList.setHasFixedSize(true);
        mHomeProductList.addOnScrollListener(mScrollListener);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        mBottomProgressBar = (ProgressBar) view.findViewById(R.id.bottom_progress_bar);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    /**
     * The M subzero home.
     */
    VolleyCallback mSubzeroHome = new VolleyCallback() {
        /**
         * This method is used to get volley success response.
         * @param response This parameter passing volley response.
         */
        @Override
        public void Success(String response) {
            mSubzeroArrivalModel = (MyApplication.getGsonInstance().fromJson(response, SubzeroHomeModel.class));
            mSubzeroArrivalResult.addAll(mSubzeroArrivalModel.getResult());
            mProductCount = mSubzeroArrivalModel.getProductCount();
            mProgressBar.setVisibility(View.GONE);
            mBottomProgressBar.setVisibility(View.GONE);
            if (mSubzeroArrivalResult != null) {
                if (mCurrentPage == 1) {
                    totalPageNo();
                    mSubzeroAdapter = new SubzeroHomeAdapter(getActivity(), mSubzeroArrivalResult);
                    mHomeProductList.setAdapter(mSubzeroAdapter);
                } else {
                    mSubzeroAdapter.notifyDataSetChanged();
                }
            }
        }

        /**
         * This method is used to get volley failure response.
         * @param errorResponse This parameter passing volley error response.
         */
        @Override
        public void Failure(String errorResponse) {
            ((MainActivity) getActivity()).homePageFailure(errorResponse);
        }
    };

    private RecyclerView.OnScrollListener mScrollListener = new RecyclerView.OnScrollListener() {
        /**
         * This method calling when scrolling the view.
         * @param recyclerView This is the first parameter of onScrolled method.
         * @param dx This is the second parameter of onScrolled method.
         * @param dy This is the third parameter of onScrolled method.
         */
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            mVisibleItemCount = mHomeProductList.getChildCount();
            mTotalItemCount = mGridLayoutManger.getItemCount();
            mFirstVisibleItem = mGridLayoutManger.findFirstVisibleItemPosition();

            if (mLoading) {
                if (mTotalItemCount > mPreviousTotal) {
                    mLoading = false;
                    mPreviousTotal = mTotalItemCount;
                }
            }
            if (!mLoading && (mTotalItemCount - mVisibleItemCount)
                    <= (mFirstVisibleItem + mVisibleThreshold)) {
                mBottomProgressBar.setVisibility(View.VISIBLE);
                mCurrentPage++;

                if (mTotalPageNumber >= mCurrentPage) {
                    // Log.e("Arrival Total page number === ", totalPageNumber+" Current page === "+mcurrentPage);
                    mHomePageController.subzeroHome(mSubzeroHome, AppConstants.sSubzeroHomeArrivals, mCurrentPage);
                } else {
                    mBottomProgressBar.setVisibility(View.GONE);
                }

                mLoading = true;
            }
        }
    };

    /**
     * This method is used to get total page number .
     */
    private void totalPageNo() {
        int extraPage = 0;
        if ((mProductCount % AppConstants.productsPerPage) == 0) {
            extraPage = 0;
        } else {
            extraPage = 1;
        }
        mTotalPageNumber = (mProductCount / AppConstants.productsPerPage) + extraPage;
    }

    /**
     * This method is calling when view destroyed.
     */
    @Override
    public void onDestroyView() {
        if (mView.getParent() != null) {
            ((ViewGroup) mView.getParent()).removeView(mView);
        }
        super.onDestroyView();
    }
}
