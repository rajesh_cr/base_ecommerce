package com.prestashopemc.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.prestashopemc.R;
import com.prestashopemc.adapter.WebHomeAdapter;
import com.prestashopemc.model.HomePage;
import com.prestashopemc.model.HomePageList;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.view.MainActivity;
import com.prestashopemc.view.RecentSearchActivity;

import java.util.List;

/**
 * <h1>Web home Fragment!</h1>
 * The Review Fragment contains product reviews details..
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author Rajesh
 * @version 1.2
 * @since 1 /11/16
 */
public class WebHomeFragment extends Fragment implements View.OnClickListener {

    private RecyclerView mWebHomeView;
    private WebHomeAdapter mWebHomeAdapter;
    private MainActivity mActivity;
    private LinearLayout mMainLayout, mMainSearchLayout;
    private EditText mSearchEdit;
    private Boolean mSearchBelowHeader;

    /**
     * This method will call when creating fragment view.
     *
     * @param inflater           This is the first parameter to onCreateView method.
     * @param container          This is the second parameter to onCreateView method.
     * @param savedInstanceState This is the third parameter to onCreateView method.
     * @return View returs layout view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.web_home, container, false);
    }

    /**
     * This method will call when fragment view is created..
     *
     * @param view               This is the first parameter to onViewCreated method.
     * @param savedInstanceState This is the first parameter to onViewCreated method.
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mActivity = (MainActivity) getActivity();
        initializedLayout(view);
    }

    private void initializedSearchLayout() {

        mSearchBelowHeader = SharedPreference.getInstance().getBoolean("search_below_header");

        if (mSearchBelowHeader) {
            mMainSearchLayout.setVisibility(View.VISIBLE);
        } else {
            mMainSearchLayout.setVisibility(View.GONE);
        }

    }

    /**
     * This method is used to Initialized Layout
     *
     * @param v This is the parameter to initializedLayout method.
     */
    private void initializedLayout(View v) {

        mMainLayout = (LinearLayout) v.findViewById(R.id.web_main_layout);
        mSearchEdit = (EditText) v.findViewById(R.id.search_edit);
        mMainSearchLayout = (LinearLayout) v.findViewById(R.id.main_page_search_layout);
        mSearchEdit.setInputType(InputType.TYPE_NULL);
        mSearchEdit.setHint(AppConstants.getTextString(mActivity, AppConstants.searchPlaceHolderText));
        String layoutBgColor = SharedPreference.getInstance().getValue("template_background");
        if (!layoutBgColor.equals("0") && !layoutBgColor.isEmpty()) {
            mMainLayout.setBackgroundColor(Color.parseColor(layoutBgColor));
        } else {
            mMainLayout.setBackgroundColor(Color.LTGRAY);
        }
        mMainSearchLayout.setBackgroundColor(Color.parseColor("#" + SharedPreference.getInstance().getValue("theme_color")));
        initializedSearchLayout();
        mWebHomeView = (RecyclerView) v.findViewById(R.id.webhome_recycler_view);
        mWebHomeView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mWebHomeView.setHasFixedSize(true);
        mWebHomeView.setNestedScrollingEnabled(true);
        mSearchEdit.setOnClickListener(this);
        /**
         * Retrive home page list values from sharedpreference */
        String res = SharedPreference.getInstance().getValue("homePageList");
        HomePage homePage = MyApplication.getGsonInstance().fromJson(res, HomePage.class);
        List<HomePageList> homePageLists = homePage.getResult().getHomePageList();
        if (mActivity.mainPageList != null) {
            //webHomeAdapter = new WebHomeAdapter(getActivity(), activity.mainPageList);
            mWebHomeAdapter = new WebHomeAdapter(getActivity(), homePageLists);
            mWebHomeView.setAdapter(mWebHomeAdapter);
        }
        setCustomFont();
    }

    /**
     * Set Custom Font
     */

    private void setCustomFont() {
        mSearchEdit.setTypeface(mActivity.robotoRegular);
    }


    /**
     * Set Clicklistener
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search_edit: /** Start a new Activity MyCards.java */
                Intent intent = new Intent(mActivity, RecentSearchActivity.class);
                mActivity.startActivity(intent);
                break;
        }
    }
}
