package com.prestashopemc.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.adapter.SubzeroHomeAdapter;
import com.prestashopemc.controller.HomePageController;
import com.prestashopemc.model.SubzeroHomeModel;
import com.prestashopemc.model.SubzeroHomeResult;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.VolleyCallback;
import com.prestashopemc.view.MainActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Best Seller Fragment!</h1>
 * The Best Seller Fragment contains shipping and billing address and edit delete address.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author Rajesh
 * @version 1.2
 * @since 24 /09/16
 */
public class BestSellerFragment extends Fragment {

    private RecyclerView mHomeProductList;
    private SubzeroHomeAdapter mSubzeroAdapter;
    private GridLayoutManager mGridLayoutManger;
    private SubzeroHomeModel mSubzeroSellerModel;
    private List<SubzeroHomeResult> mSubzeroSellerResult = new ArrayList<SubzeroHomeResult>();
    private int mProductCount = 0;
    private HomePageController homePageController;
    private int previousTotal = 0;
    private int visibleThreshold = 5;
    private int mFirstVisibleItem, mVisibleItemCount, mTotalItemCount;
    private boolean loading = true;
    private int currentPage = 1;
    private int totalPageNumber = 0;
    private ProgressBar mProgressBar, mBottomProgressBar;
    private View mView;
    private MainActivity activity;
    private TextView mErrorText;

    /**
     * This method is used to create view for fragment.
     * @param inflater This is the first parameter to onCreateView method.
     * @param container This is the second parameter to onCreateView method.
     * @param savedInstanceState This is the third parameter to onCreateView method.
     * @return View returs layout view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (mView == null){
            mView = inflater.inflate(R.layout.best_seller_fragment, container, false);
            initializeLayout(mView);
            activity = (MainActivity)getActivity();
            homePageController = new HomePageController(getActivity());
            homePageController.subzeroHome(mSubzeroHome, AppConstants.sSubzeroHomeSeller, currentPage);
        }
        return mView;
    }

    /**
     * This method is used to created view for fragment.
     * @param view This is the first parameter to onViewCreated method.
     * @param savedInstanceState This is the first parameter to onViewCreated method.
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    /**
     *This method is used to Initalized Layout
     * @param view This is the parameter to initalizedLayout method.
     */
    private void initializeLayout(View view) {
        mHomeProductList = (RecyclerView)view.findViewById(R.id.subzero_homeproducts_list);
        mErrorText = (TextView) view.findViewById(R.id.error_text);
        mGridLayoutManger = new GridLayoutManager(getActivity(), 2);
        mHomeProductList.setLayoutManager(mGridLayoutManger);
        mHomeProductList.setHasFixedSize(true);
        mHomeProductList.addOnScrollListener(mScrollListener);
        mProgressBar = (ProgressBar)view. findViewById(R.id.progress_bar);
        mBottomProgressBar = (ProgressBar)view. findViewById(R.id.bottom_progress_bar);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    /**
     * The M subzero home.
     */
    VolleyCallback mSubzeroHome = new VolleyCallback() {
        /**
         * This method is used to get success response subzero list
         * @param response This is the parameter to success method.
         */
        @Override
        public void Success(String response) {
            mSubzeroSellerModel = (MyApplication.getGsonInstance().fromJson(response, SubzeroHomeModel.class));
            if (mSubzeroSellerModel.getResult().size() != 0){
                mSubzeroSellerResult.addAll(mSubzeroSellerModel.getResult());
                mProductCount = mSubzeroSellerModel.getProductCount();
                mProgressBar.setVisibility(View.GONE);
                mBottomProgressBar.setVisibility(View.GONE);
                if (mSubzeroSellerResult != null){
                    if (currentPage == 1) {
                        totalPageNo();
                        mSubzeroAdapter = new SubzeroHomeAdapter(getActivity(), mSubzeroSellerResult);
                        mHomeProductList.setAdapter(mSubzeroAdapter);
                    }else {
                        mSubzeroAdapter.notifyDataSetChanged();
                    }

                }
            }else {
                mProgressBar.setVisibility(View.GONE);
                mHomeProductList.setVisibility(View.GONE);
                mErrorText.setText(getString(R.string.noProductsText));
                mErrorText.setVisibility(View.VISIBLE);
            }
        }

        /**
         * This method is used to get failure response subzero list
         * @param errorResponse This is the parameter to Failure method.
         */
        @Override
        public void Failure(String errorResponse) {
            if (errorResponse != null){
                mProgressBar.setVisibility(View.GONE);
                mHomeProductList.setVisibility(View.GONE);
                mErrorText.setText(errorResponse);
                mErrorText.setVisibility(View.VISIBLE);
            }
        }
    };

    private RecyclerView.OnScrollListener mScrollListener = new RecyclerView.OnScrollListener() {
        /**
         * This method is used to scroll action
         * @param recyclerView This is the first parameter to onScrolled method.
         * @param dx This is the second parameter to onScrolled method.
         * @param dy This is the third parameter to onScrolled method.
         */
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            mVisibleItemCount = mHomeProductList.getChildCount();
            mTotalItemCount = mGridLayoutManger.getItemCount();
            mFirstVisibleItem = mGridLayoutManger.findFirstVisibleItemPosition();

            if (loading) {
                if (mTotalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = mTotalItemCount;
                }
            }
            if (!loading && (mTotalItemCount - mVisibleItemCount)
                    <= (mFirstVisibleItem + visibleThreshold)) {
                mBottomProgressBar.setVisibility(View.VISIBLE);
                currentPage++;

                if (totalPageNumber >= currentPage) {
                    //Log.e("Seller Total page number === ", totalPageNumber+" Current page === "+currentPage);
                    homePageController.subzeroHome(mSubzeroHome, AppConstants.sSubzeroHomeSeller, currentPage);
                }
                else
                {
                    mBottomProgressBar.setVisibility(View.GONE);
                }

                loading = true;
            }
        }
    };

    /**
     * This method is used to get total page number.
     */
    private void totalPageNo()
    {
        int extraPage = 0;
        if((mProductCount % AppConstants.productsPerPage) == 0)
        {
            extraPage = 0;
        }else {
            extraPage = 1;
        }
        totalPageNumber = (mProductCount / AppConstants.productsPerPage)+extraPage;
    }

    /**
     * This method is used to destroy view
     */
    @Override
    public void onDestroyView() {
        if (mView.getParent() != null) {
            ((ViewGroup)mView.getParent()).removeView(mView);
        } /**
     *This method is used to Initalized Layout
     */
        super.onDestroyView();
    }

}
