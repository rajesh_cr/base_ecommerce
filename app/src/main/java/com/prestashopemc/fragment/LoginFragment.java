package com.prestashopemc.fragment;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.prestashopemc.R;
import com.prestashopemc.controller.LoginController;
import com.prestashopemc.model.LoginValues;
import com.prestashopemc.model.RegisterValues;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CommonValidation;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.view.AccountActivity;
import com.prestashopemc.view.AddNewAddress;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;

/**
 * <h1>Login Fragment!</h1>
 * The Login Fragment contains Login page and its details.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 5 /8/2016
 */
public class LoginFragment extends Fragment implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {
    private Button mSignWithGoogle, mSignWithFb, mSignUp, mSubmit;
    private GradientDrawable mFacebookGradient, mGoogleGradient, mSignUpGradient, mSubmitGradient;
    private AccountActivity mAccountActivity;
    private EditText mEmailEdit, mPasswordEdit, mEmail;
    private AppCompatCheckBox mRememberCheckBox;
    private TextView mForgotPasswordLabel, mSignInLabel, mGuestLabel, mHeader, mMsg, mSend, mFooter, mLogin;
    private LoginController mloginController;
    private com.facebook.login.LoginManager mLoginManager;
    private CallbackManager mCallbackManager;
    /**
     * The constant RC_SIGN_IN.
     */
    public static final int RC_SIGN_IN = 101;
    private GoogleApiClient mGoogleApiClient;
    private ImageView mClose;
    private PopupWindow mPopupWindow;
    private RelativeLayout mLoginMainLayout, mHeaderLayout;
    private String mMail, mRememberEmail, mRememberPassword;
    private boolean mRemember = false;
    private File mFile;
    private boolean firstVisit;

    /**
     * This method will call when fragment started.
     */
    @Override
    public void onStart() {
        super.onStart();
        /**
         * Connect with Google*/
        mGoogleApiClient.connect();
    }

    /**
     * This method will call when fragment stopped.
     */
    @Override
    public void onStop() {
        super.onStop();
        /**
         * Check connection with Google is connect or disconnect*/
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        /**
         * Check connection with Facebook is connect or disconnect*/
        if (mLoginManager != null) {
            mLoginManager.logOut();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        if (firstVisit) {
            firstVisit = false;
        } else {
            mAccountActivity.showProgDialiog();
        }
    }

    /**
     * This method will call when creating fragment.
     *
     * @param savedInstanceState This is the parameter to onCreateView method.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getActivity());
        mLoginManager = LoginManager.getInstance();
        mCallbackManager = CallbackManager.Factory.create();
        if(mGoogleApiClient == null || !mGoogleApiClient.isConnected()){
            try {
                GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestEmail()
                        .build();
                mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                        .enableAutoManage(getActivity(), this)
                        .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                        .build();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This method will call when creating fragment view.
     *
     * @param inflater           This is the first parameter to onCreateView method.
     * @param container          This is the second parameter to onCreateView method.
     * @param savedInstanceState This is the third parameter to onCreateView method.
     * @return View returs layout view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        firstVisit = true;
        mLoginManager.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                GraphRequest graphRequest = GraphRequest.newMeRequest(loginResult.getAccessToken(),
                        new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject object, GraphResponse response) {
                                try {
                                    JSONObject profileOject = response.getJSONObject();
                                    String profileId = profileOject.getString("id");
                                    Log.e("Facebook User Id = ", profileId);
                                    SharedPreference.getInstance().save("social_profile_id", profileId);
                                    try {
                                        URL url = new URL("https://graph.facebook.com/" + profileId + "/picture?type=large");
                                        mFile = new File(url.getFile());
                                    } catch (MalformedURLException e) {
                                        e.printStackTrace();
                                    }
                                    final String email = profileOject.getString("email");
                                    final String name = profileOject.getString("name");
                                    System.out.println("email:" + email + "\nname:" + name);
                                    String[] names = name.split(" ");
                                    RegisterValues registerValues = new RegisterValues();
                                    registerValues.setFirstName(names[0]);
                                    registerValues.setLastName(names[1]);
                                    registerValues.setEmail(email);
                                    registerValues.setPassword("");
                                    registerValues.setAndroidId(SharedPreference.getInstance().getValue("fcm_token"));
                                    mloginController.createAccount(registerValues, "facebook");
                                    //loginController.createAccountImage(registerValues, "facebook", file);

                                } catch (JSONException e) {
                                    firstVisit = true;
                                    e.printStackTrace();
                                }

                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday");
                graphRequest.setParameters(parameters);
                graphRequest.executeAsync();
            }

            @Override
            public void onCancel() {
                firstVisit = true;
                mAccountActivity.snackBar(getString(R.string.loginFailureText));
            }

            @Override
            public void onError(FacebookException error) {
                firstVisit = true;
                mAccountActivity.snackBar(error + "");
            }
        });
        View view = inflater.inflate(R.layout.login_fragment, container, false);
        return view;
    }

    /**
     * This method will call when fragment view is created..
     *
     * @param view               This is the first parameter to onViewCreated method.
     * @param savedInstanceState This is the first parameter to onViewCreated method.
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MyApplication.getDefaultTracker("Login Screen");
        initalizeLayout(view);
    }


    /**
     * This method is used to Initalized Layout
     *
     * @param view This is the parameter to initalizeLayout method.
     */
    private void initalizeLayout(View view) {
        mloginController = new LoginController(getActivity());
        mAccountActivity = (AccountActivity) getActivity();
        mLoginMainLayout = (RelativeLayout) view.findViewById(R.id.login_main_layout);
        mSignWithGoogle = (Button) view.findViewById(R.id.login_with_google);
        mSignWithFb = (Button) view.findViewById(R.id.login_with_facebook);
        mSignUp = (Button) view.findViewById(R.id.sign_up);
        mGuestLabel = (TextView) view.findViewById(R.id.guest_checkout_button);
        mEmailEdit = (EditText) view.findViewById(R.id.phone_email_id);
        mPasswordEdit = (EditText) view.findViewById(R.id.password);
        mRememberCheckBox = (AppCompatCheckBox) view.findViewById(R.id.remember_checkbox);
        mForgotPasswordLabel = (TextView) view.findViewById(R.id.forgot_password);
        mSubmit = (Button) view.findViewById(R.id.submit);
        mSignInLabel = (TextView) view.findViewById(R.id.sign_in_with);
        mSignUp.setTextColor(Color.parseColor(CustomBackground.mThemeColor));
        CustomBackground.setBackgroundRedWhiteCombination(mSignUp);
        CustomBackground.setBackgroundText(mSubmit);
        mGoogleGradient = (GradientDrawable) mSignWithGoogle.getBackground();
        mFacebookGradient = (GradientDrawable) mSignWithFb.getBackground();
        AppConstants.backgroundColor(mGoogleGradient, R.color.google_color, getActivity(), R.color.google_color);
        AppConstants.backgroundColor(mFacebookGradient, R.color.facebook_color, getActivity(), R.color.facebook_color);

        dynamicTextValue();

        mSignUp.setOnClickListener(this);
        mSubmit.setOnClickListener(this);
        mSignWithFb.setOnClickListener(this);
        mSignWithGoogle.setOnClickListener(this);
        mForgotPasswordLabel.setOnClickListener(this);
        mRememberCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                boolean rememberCheck = mRememberCheckBox.isChecked();
                if (rememberCheck) {
                    mRemember = true;
                    CustomBackground.setCheckBoxColor(mRememberCheckBox);
                } else {
                    mRemember = false;
                    CustomBackground.setCheckBoxColor(mRememberCheckBox);
                }
            }
        });

        if (mAccountActivity.guest != null) {
            mGuestLabel.setVisibility(View.VISIBLE);
        }
        mGuestLabel.setOnClickListener(this);
        setCustomFont();

        mRememberEmail = SharedPreference.getInstance().getValue("user_name");
        mRememberPassword = SharedPreference.getInstance().getValue("password");
        if (!mRememberEmail.equals("0") && !mRememberPassword.equals("0")) {
            mRememberCheckBox.setChecked(true);
            mEmailEdit.setText(SharedPreference.getInstance().getValue("user_name"));
            mPasswordEdit.setText(SharedPreference.getInstance().getValue("password"));
        }
        CustomBackground.setCheckBoxColor(mRememberCheckBox);
    }

    /**
     * This method is used to Dynamic Text value settings
     */
    private void dynamicTextValue() {
        mAccountActivity.categoryTitle.setText(AppConstants.getTextString(getActivity(), AppConstants.loginText));
        mEmailEdit.setHint(AppConstants.getTextString(getActivity(), AppConstants.emailIdText));
        mPasswordEdit.setHint(AppConstants.getTextString(getActivity(), AppConstants.passwordText));
        mRememberCheckBox.setText(AppConstants.getTextString(getActivity(), AppConstants.rememberMeText));
        mForgotPasswordLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.forgotPasswordText));
        mSubmit.setText(AppConstants.getTextString(getActivity(), AppConstants.submitText));
        mSignUp.setText(AppConstants.getTextString(getActivity(), AppConstants.signUpText));
        mSignInLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.orSignUpWithText));
        mSignWithFb.setText(AppConstants.getTextString(getActivity(), AppConstants.facebookText));
        mSignWithGoogle.setText(AppConstants.getTextString(getActivity(), AppConstants.googleText));
        mGuestLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.continueGuestText));
    }

    /**
     * This method is used to Set Custom Font
     */
    private void setCustomFont() {
        mEmailEdit.setTypeface(mAccountActivity.robotoRegular);
        mPasswordEdit.setTypeface(mAccountActivity.robotoRegular);
        mRememberCheckBox.setTypeface(mAccountActivity.robotoRegular);
        mForgotPasswordLabel.setTypeface(mAccountActivity.robotoRegular);
        mSubmit.setTypeface(mAccountActivity.robotoRegular);
        mSignUp.setTypeface(mAccountActivity.robotoRegular);
        mSignInLabel.setTypeface(mAccountActivity.robotoLight);
        mSignWithFb.setTypeface(mAccountActivity.robotoMedium);
        mSignWithGoogle.setTypeface(mAccountActivity.robotoMedium);
    }

    /**
     * This method is used to get google signin result.
     *
     * @param requestCode This is the first parameter to onActivityResult method.
     * @param resultCode  This is the second parameter to onActivityResult method.
     * @param data        This is the third parameter to onActivityResult method.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (mCallbackManager.onActivityResult(requestCode, resultCode, data)) {
            return;
        }
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount acct = result.getSignInAccount();
                // Get account information
                getProfileInformation(acct);
            }else {
                firstVisit = true;
            }
        }
    }

    /**
     * This method is used to Getting Google Plus Details
     *
     * @param acct This is the parameter to getProfileInformation method.
     */
    private void getProfileInformation(GoogleSignInAccount acct) {
        Uri userId = acct.getPhotoUrl();
        //file = new File(userId.getPath());
        Log.e("Google Plus User Id = ", String.valueOf(userId));
        SharedPreference.getInstance().save("google_profile_id", String.valueOf(userId));
        String mFullName = acct.getDisplayName();
        String[] names = mFullName.split(" ", 2);
        String firstName = names[0];
        String lastName = names[1];
        String mEmail = acct.getEmail();
        RegisterValues registerValues = new RegisterValues();
        registerValues.setFirstName(firstName);
        registerValues.setLastName(lastName);
        registerValues.setEmail(mEmail);
        registerValues.setPassword("");
        registerValues.setAndroidId(SharedPreference.getInstance().getValue("fcm_token"));
        mloginController.createAccount(registerValues, "googleplus");
        //loginController.createAccountImage(registerValues, "googleplus", file);
    }

    /**
     * This method is calling when google connection failed
     *
     * @param connectionResult This parameter connection result passing.
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    /**
     * This method is used to Forgot password popup layout method
     */
    public void showPopup() {

        LayoutInflater layoutInflater = (LayoutInflater) getActivity().getSystemService(getActivity().LAYOUT_INFLATER_SERVICE);

        View popupView = layoutInflater.inflate(R.layout.forgot_layout, null);

        mPopupWindow = new PopupWindow(popupView,
                LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT, true);

        mPopupWindow.setTouchable(true);
        mPopupWindow.setFocusable(true);

        mPopupWindow.showAtLocation(popupView, Gravity.CENTER, 0, 0);
        mHeaderLayout = (RelativeLayout) popupView.findViewById(R.id.header_layout);
        mHeaderLayout.setBackgroundColor(Color.parseColor("#" + SharedPreference.getInstance().getValue("theme_color")));
        mHeader = (TextView) popupView.findViewById(R.id.forgot_header);
        mMsg = (TextView) popupView.findViewById(R.id.forgot_msg);
        mSend = (TextView) popupView.findViewById(R.id.forgot_send);
        mFooter = (TextView) popupView.findViewById(R.id.forgot_footer);
        mLogin = (TextView) popupView.findViewById(R.id.forgot_login);
        mEmail = (EditText) popupView.findViewById(R.id.forgot_mail);
        mClose = (ImageView) popupView.findViewById(R.id.forgot_close);

        CustomBackground.setBackgroundText(mSend);

        mHeader.setTypeface(mAccountActivity.robotoRegular);
        mMsg.setTypeface(mAccountActivity.robotoRegular);
        mEmail.setTypeface(mAccountActivity.robotoRegular);
        mSend.setTypeface(mAccountActivity.robotoMedium);
        mFooter.setTypeface(mAccountActivity.robotoLight);
        mLogin.setTypeface(mAccountActivity.robotoRegular);
        mSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMail = mEmail.getText().toString();
                if (mMail.length() != 0) {
                    mloginController.forgotPassword(mMail);
                    popupClose();
                } else {
                    mAccountActivity.snackBar(getString(R.string.mailTextfieldEmptyAlertText));
                }
            }
        });

        mLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupClose();
            }
        });


        mClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                popupClose();
            }
        });

        mPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                mLoginMainLayout.setAlpha((float) 1);
                mAccountActivity.toolbar.setAlpha((float) 1);
                mPopupWindow.dismiss();
            }
        });

    }

    /**
     * This method is used to Popup Close Method
     */
    public void popupClose() {
        mLoginMainLayout.setAlpha((float) 1);
        mAccountActivity.toolbar.setAlpha((float) 1);
        mPopupWindow.dismiss();
    }

    /**
     * This method is calling when click event occured.
     *
     * @param v This is the parameter onClick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * Guest checkout Listener*/
            case R.id.guest_checkout_button:
                mAccountActivity.deleteValue();
                mAccountActivity.deleteGuestDetails();
                SharedPreference.getInstance().removeValue("response");
                Intent newaddress = new Intent(getActivity(), AddNewAddress.class);
                startActivity(newaddress);
                mAccountActivity.finish();
                break;

            /**
             * Forgot password listener*/
            case R.id.forgot_password:
                mLoginMainLayout.setAlpha((float) 0.2);
                mAccountActivity.toolbar.setAlpha((float) 0.2);
                showPopup();
                break;

            /**
             * SingUp Listener*/
            case R.id.sign_up:
                mGoogleApiClient.disconnect();
                CreateAccountFragment homeFragment = new CreateAccountFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_layout, homeFragment);
                transaction.addToBackStack(null);
                transaction.commit();
                break;

            /**
             * Submit Listener*/
            case R.id.submit:
                mAccountActivity.showProgDialiog();
                LoginValues loginValues = new LoginValues();
                loginValues.setEmail(mEmailEdit.getText().toString());
                loginValues.setPassword(mPasswordEdit.getText().toString());
                loginValues.setCartId(SharedPreference.getInstance().getValue("cart_id"));
                loginValues.setAndroidId(SharedPreference.getInstance().getValue("fcm_token"));
                CommonValidation validation = new CommonValidation();
                boolean status = validation.loginValidation(getActivity(), loginValues);
                if (status) {
                    if (mRemember) {
                        SharedPreference.getInstance().save("user_name", loginValues.getEmail());
                        SharedPreference.getInstance().save("password", loginValues.getPassword());
                    }else {
                        SharedPreference.getInstance().removeValue("user_name");
                        SharedPreference.getInstance().removeValue("password");
                    }
                    mloginController.loginDetails(loginValues);
                } else {
                    mAccountActivity.hideProgDialog();
                }
                break;

            /**
             * Login with FB Listener*/
            case R.id.login_with_facebook:
                mLoginManager.logInWithReadPermissions(LoginFragment.this, Arrays.asList("email", "public_profile", "user_birthday"));
                break;

            /**
             * Google Plus Login Listener*/
            case R.id.login_with_google:
                mGoogleApiClient.connect();
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
        }
    }
}
