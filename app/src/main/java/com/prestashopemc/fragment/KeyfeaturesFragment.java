package com.prestashopemc.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prestashopemc.R;
import com.prestashopemc.adapter.KeyFeaturesAdapter;
import com.prestashopemc.customView.CustomTextView;
import com.prestashopemc.model.Feature;
import com.prestashopemc.model.MagentoProductDetail;
import com.prestashopemc.model.ProductDetail;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.view.MagentoProductDetailActivity;
import com.prestashopemc.view.ProductDetailActivity;

import java.util.List;

/**
 * <h1>Key Features Fragment!</h1>
 * The Key Features Fragment contains Key features of product.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author Rajesh
 * @version 1.2
 * @since 10 /09/2016
 */
public class KeyfeaturesFragment extends Fragment {

    private RecyclerView mKeyfeatureList;
    private ProductDetailActivity mPrdDetailActivity;
    private MagentoProductDetailActivity mMagentoPrdDetailActivity;
    private MagentoProductDetail mMagentoProductDetail;
    private List<Feature> mFeature;
    private ProductDetail mProductDetail;
    private KeyFeaturesAdapter mKeyfeatureAdapter;
    private Context mContext;
    private LinearLayoutManager mLayoutManager;
    private CustomTextView mGeneralText;

    /**
     * This method is used to create view for fragment.
     * @param inflater This is the first parameter to onCreateView method.
     * @param container This is the second parameter to onCreateView method.
     * @param savedInstanceState This is the third parameter to onCreateView method.
     * @return View returs layout view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.keyfeatures_fragment, container, false);
    }

    /**
     * This method is used to created view for fragment.
     * @param view This is the first parameter to onViewCreated method.
     * @param savedInstanceState This is the first parameter to onViewCreated method.
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContext = this.getActivity();

        if(AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)){
            initializeMagentoLayout(view);
        }else {
            initializeLayout(view);
        }


    }

    /**
     * This method is used to Initalized Layout
     * @param view This is the parameter to initalizedLayout method.
     * */
    private void initializeMagentoLayout(View view) {
        mMagentoPrdDetailActivity = (MagentoProductDetailActivity) getActivity();
        mGeneralText = (CustomTextView)view.findViewById(R.id.keyfeature_Title);
        mGeneralText.setTypeface(mMagentoPrdDetailActivity.robotoMedium);
        dynamicTextValue();
        mKeyfeatureList = (RecyclerView)view.findViewById(R.id.keyFeatures_recyclerview);
        mLayoutManager = new LinearLayoutManager(mContext);
        mKeyfeatureList.setLayoutManager(mLayoutManager);
        mMagentoProductDetail = mMagentoPrdDetailActivity.productDetail;
        mFeature = mMagentoProductDetail.getFeatures();

        mKeyfeatureList.setHasFixedSize(true);
        if (mFeature.size() != 0)
        {
            mKeyfeatureAdapter = new KeyFeaturesAdapter(mContext, mFeature);
            mKeyfeatureList.setAdapter(mKeyfeatureAdapter);
        }
        else
        {
            mGeneralText.setVisibility(View.GONE);
            mMagentoPrdDetailActivity.updateCartFailure(getString(R.string.noKeyFeaturesText));
        }
    }

    /**
     * This method is used to Initalized Layout
     * @param view This is the parameter to initalizedLayout method.
     * */
    private void initializeLayout(View view) {
        mPrdDetailActivity = (ProductDetailActivity) getActivity();
        mGeneralText = (CustomTextView)view.findViewById(R.id.keyfeature_Title);
        mGeneralText.setTypeface(mPrdDetailActivity.robotoMedium);
        dynamicTextValue();
        mKeyfeatureList = (RecyclerView)view.findViewById(R.id.keyFeatures_recyclerview);
        mLayoutManager = new LinearLayoutManager(mContext);
        mKeyfeatureList.setLayoutManager(mLayoutManager);
        mProductDetail = mPrdDetailActivity.productDetail;
        mFeature = mProductDetail.getFeatures();

        mKeyfeatureList.setHasFixedSize(true);
        if (mFeature.size() != 0)
        {
            mKeyfeatureAdapter = new KeyFeaturesAdapter(mContext, mFeature);
            mKeyfeatureList.setAdapter(mKeyfeatureAdapter);
        }
        else
        {
            mGeneralText.setVisibility(View.GONE);
            mPrdDetailActivity.updateCartFailure(getString(R.string.noKeyFeaturesText));
        }
    }

    /**
     * This method is used to Dynamic Text Value Settings
     * */
    private void dynamicTextValue() {
        mGeneralText.setText(AppConstants.getTextString(getActivity(), AppConstants.generalText));
    }

}
