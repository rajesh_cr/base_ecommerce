package com.prestashopemc.fragment;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.google.gson.Gson;
import com.prestashopemc.R;
import com.prestashopemc.adapter.PaymentMethodAdapter;
import com.prestashopemc.controller.CheckoutController;
import com.prestashopemc.model.Address;
import com.prestashopemc.model.AddressValues;
import com.prestashopemc.model.MagentoDefaultAddressShipping;
import com.prestashopemc.model.PaymentMainModel;
import com.prestashopemc.model.PaymentMethod;
import com.prestashopemc.model.PaymentPrices;
import com.prestashopemc.model.PaymentResult;
import com.prestashopemc.model.PaymentShippingMethod;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.VolleyCallback;
import com.prestashopemc.view.CheckoutActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Payment Fragment!</h1>
 * The Payment Fragment contains payment details view and functionality.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author Rajesh
 * @version 1.2
 * @since 30 /09/2016
 */
public class PaymentFragment extends Fragment {

    private PaymentMethodAdapter mPaymentMethodAdapter;
    private RecyclerView mPaymentMethodList;
    private TextView mPaymentTitleLabel, mPaymentShippinhOption, mPaymentSummaryLabel, mPaymentSubtotalLabel, mPaymentSubTotal, mPaymentShippingHandlingLabel, mPaymentShippinhHandling, mPaymentTaxLabel, mPaymentTax, mPaymentDiscountLabel, mPaymentDiscount, mPaymentAmountLabel, mPaymentAmount;
    private Spinner mPaymentCourierSpinner;
    private LinearLayoutManager mLinearLayoutManager;
    private CheckoutActivity mCheckoutActivity;
    private String mCartId, mCurrenySymbol;
    private CheckoutController mCheckoutController;
    private PaymentMainModel mPaymentMainModel;
    private PaymentResult mPaymentResult;
    private PaymentPrices mPaymentPrices;
    private List<PaymentShippingMethod> mPaymentShippingMethod = new ArrayList<PaymentShippingMethod>();
    private List<PaymentMethod> mPaymentMethod = new ArrayList<PaymentMethod>();
    private View mView;
    private String mShippingId, mBillingId;
    private Address mAddressModel;
    private RelativeLayout mMainLayout;
    private String mShippingMethodId, mShippingMethodName;
    public static String sTax = "0",sDiscount ="0",sShippingAmount = "0";
    private MagentoDefaultAddressShipping mMagentoDefaultAddressShipping;

    /**
     * This method will call when creating fragment view.
     *
     * @param inflater           This is the first parameter to onCreateView method.
     * @param container          This is the second parameter to onCreateView method.
     * @param savedInstanceState This is the third parameter to onCreateView method.
     * @return View returs layout view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (mView == null) {
            mView = inflater.inflate(R.layout.fragment_payment, container, false);
            initializedLayout(mView);
        }
        return mView;
    }

    /**
     * This method will call when fragment view is created..
     *
     * @param view               This is the first parameter to onViewCreated method.
     * @param savedInstanceState This is the first parameter to onViewCreated method.
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    /**
     * This method is used to Initialized Layout
     *
     * @param view This is the parameter to initializedLayout method.
     */
    private void initializedLayout(View view) {
        mCartId = SharedPreference.getInstance().getValue("cart_id");
        mCurrenySymbol = SharedPreference.getInstance().getValue("currency_symbol");
        mCheckoutController = new CheckoutController(getActivity());
        mCheckoutActivity = (CheckoutActivity) getActivity();
        mCheckoutActivity.showProgDialiog();
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mMainLayout = (RelativeLayout) view.findViewById(R.id.payment_main_layout);
        mPaymentMethodList = (RecyclerView) view.findViewById(R.id.payment_method_list);
        mPaymentCourierSpinner = (Spinner) view.findViewById(R.id.payment_spinner);
        mPaymentTitleLabel = (TextView) view.findViewById(R.id.payment_title_text);
        mPaymentShippinhOption = (TextView) view.findViewById(R.id.payment_select_shipping_option);
        mPaymentSummaryLabel = (TextView) view.findViewById(R.id.payment_summary);
        mPaymentSubtotalLabel = (TextView) view.findViewById(R.id.payment_subtotal_label);
        mPaymentSubTotal = (TextView) view.findViewById(R.id.payment_subTotal);
        mPaymentShippingHandlingLabel = (TextView) view.findViewById(R.id.payment_shipping_label);
        mPaymentShippinhHandling = (TextView) view.findViewById(R.id.payment_shippingCost);
        mPaymentTaxLabel = (TextView) view.findViewById(R.id.payment_tax_label);
        mPaymentTax = (TextView) view.findViewById(R.id.payment_tax);
        mPaymentDiscountLabel = (TextView) view.findViewById(R.id.payment_discount_label);
        mPaymentDiscount = (TextView) view.findViewById(R.id.payment_discount);
        mPaymentAmountLabel = (TextView) view.findViewById(R.id.payment_amount_label);
        mPaymentAmount = (TextView) view.findViewById(R.id.payment_amount);
        dynamicTextValue();
        setCustomFont();
        mPaymentMethodList.setLayoutManager(mLinearLayoutManager);
        mPaymentMethodList.setHasFixedSize(true);
        callPaymentApi();
    }

    /**
     * This method is used to Dynamic Text Value Settings
     */
    private void dynamicTextValue() {
        mPaymentShippinhOption.setText(AppConstants.getTextString(getActivity(), AppConstants.selectShipingOptionText) + " :");
        mPaymentTitleLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.selectPaymentMethodText));
        mPaymentSummaryLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.paymentSummaryText));
        mPaymentSubtotalLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.subTotalText));
        mPaymentShippingHandlingLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.paymentShippingHandlingText));
        mPaymentTaxLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.taxText));
        mPaymentDiscountLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.discountText));
        mPaymentAmountLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.amountPayableText));
    }

    /**
     * This method is used to Set Custom Font
     */
    private void setCustomFont() {
        mPaymentTitleLabel.setTypeface(mCheckoutActivity.robotoMedium);
        mPaymentShippinhOption.setTypeface(mCheckoutActivity.robotoMedium);
        mPaymentSummaryLabel.setTypeface(mCheckoutActivity.robotoRegular);
        mPaymentSubtotalLabel.setTypeface(mCheckoutActivity.robotoRegular);
        mPaymentShippingHandlingLabel.setTypeface(mCheckoutActivity.robotoRegular);
        mPaymentTaxLabel.setTypeface(mCheckoutActivity.robotoRegular);
        mPaymentDiscountLabel.setTypeface(mCheckoutActivity.robotoRegular);
        mPaymentAmountLabel.setTypeface(mCheckoutActivity.robotoMedium);
        mPaymentAmount.setTypeface(mCheckoutActivity.robotoMedium);
        mPaymentSubTotal.setTypeface(mCheckoutActivity.robotoLight);
        mPaymentShippinhHandling.setTypeface(mCheckoutActivity.robotoLight);
        mPaymentTax.setTypeface(mCheckoutActivity.robotoLight);
        mPaymentDiscount.setTypeface(mCheckoutActivity.robotoLight);
    }

    /**
     * This method is used to Payment Volley Callback
     */
    private void callPaymentApi() {
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            mMagentoDefaultAddressShipping = mCheckoutActivity.magentoDefaultAddressShipping;
            if (mMagentoDefaultAddressShipping != null) {
                if (mCheckoutActivity.shippingId != null) {
                    mShippingId = mCheckoutActivity.shippingId;
                } else {
                    mShippingId = mMagentoDefaultAddressShipping.getEntityId();
                }
                if (mCheckoutActivity.billingId != null) {
                    mBillingId = mCheckoutActivity.billingId;
                } else {
                    mBillingId = mMagentoDefaultAddressShipping.getEntityId();
                }
                mCheckoutController.loadPayment(paymentDetails, mShippingId, mBillingId, mCartId);
            } else {
                getStaticAddressValues();
            }
        }else {
            mAddressModel = mCheckoutActivity.addressmodel;
            if (mAddressModel != null) {
                if (mCheckoutActivity.shippingId != null) {
                    mShippingId = mCheckoutActivity.shippingId;
                } else {
                    mShippingId = mAddressModel.getIdAddress();
                }
                if (mCheckoutActivity.billingId != null) {
                    mBillingId = mCheckoutActivity.billingId;
                } else {
                    mBillingId = mAddressModel.getIdAddress();
                }
                mCheckoutController.loadPayment(paymentDetails, mShippingId, mBillingId, mCartId);
            } else {
                getStaticAddressValues();
            }
        }
    }

    /**
     * Get static address values from SharedPreference
     */
    private void getStaticAddressValues() {
        JSONObject shippingObject = new JSONObject();
        JSONObject billingObject = new JSONObject();
        try {
            Gson gson = new Gson();
            String shipObj = SharedPreference.getInstance().getValue("shippingAddressValues");
            AddressValues shippingValues = gson.fromJson(shipObj, AddressValues.class);

            shippingObject.put("firstname", shippingValues.getFname());
            shippingObject.put("lastname", shippingValues.getLname());
            shippingObject.put("city", shippingValues.getCity());
            shippingObject.put("country_id", SharedPreference.getInstance().getValue("shippingCountryId"));
            shippingObject.put("postcode", shippingValues.getZipcode());
            shippingObject.put("telephone", shippingValues.getPhone());
            shippingObject.put("street", shippingValues.getStreet());

            String billObj = SharedPreference.getInstance().getValue("billingAddressValues");
            AddressValues billingValues = gson.fromJson(billObj, AddressValues.class);

            billingObject.put("firstname", billingValues.getFname());
            billingObject.put("lastname", billingValues.getLname());
            billingObject.put("city", billingValues.getCity());
            billingObject.put("country_id", SharedPreference.getInstance().getValue("billingCountryId"));
            billingObject.put("postcode", billingValues.getZipcode());
            billingObject.put("telephone", billingValues.getPhone());
            billingObject.put("street", billingValues.getStreet());

            SharedPreference.getInstance().save("shippingStaticAddress", shippingObject.toString());
            SharedPreference.getInstance().save("billingStaticAddress", billingObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }
        mCheckoutController.loadGuestPayment(paymentDetails, shippingObject.toString(), billingObject.toString(), mCartId);
    }

    /**
     * The Payment details.
     */
    VolleyCallback paymentDetails = new VolleyCallback() {
        /**
         * This method is used to add Shipping Price success Response
         * @param response This parameter getting from success response method.
         */
        @Override
        public void Success(String response) {
            mCheckoutActivity.hideProgDialog();
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(response);

                if (jsonObject.getString("status").equalsIgnoreCase("true") || jsonObject.getBoolean("status")){
                    JSONObject resultObject = jsonObject.getJSONObject("result");
                    JSONArray paymentMethodObject = resultObject.getJSONArray("payment_method");
                    JSONArray shippingMethodObject = resultObject.getJSONArray("shipping_method");
                    JSONObject paymentPricesObject = resultObject.getJSONObject("prices");
                    PaymentPrices paymentPrices = new PaymentPrices();
                    if (!AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                        paymentPrices.setCouponTitle(paymentPricesObject.getString("coupon_title"));
                        paymentPrices.setCouponId(paymentPricesObject.getString("coupon_id"));
                       // paymentPrices.setTotalPriceWithoutTax(paymentPricesObject.getString("total_price_without_tax"));
                        paymentPrices.setIsTaxDispay(paymentPricesObject.getString("is_tax_dispay"));
                    }
                    paymentPrices.setSubtotal(paymentPricesObject.getString("subtotal"));
                    paymentPrices.setShippingAmount(paymentPricesObject.getString("shipping_amount"));
                    paymentPrices.setDiscount(paymentPricesObject.getString("discount"));
                    paymentPrices.setGrandtotal(paymentPricesObject.getString("grandtotal"));
                    paymentPrices.setTax(paymentPricesObject.getString("tax"));
                    mPaymentPrices = paymentPrices;
                    for (int i = 0; i < paymentMethodObject.length(); i++) {
                        PaymentMethod paymentMethod = new PaymentMethod();
                        JSONObject paymentObject = paymentMethodObject.getJSONObject(i);
                        paymentMethod.setCode(paymentObject.getString("code"));
                        paymentMethod.setIdentifier(paymentObject.getString("identifier"));
                        paymentMethod.setName(paymentObject.getString("name"));
                        mPaymentMethod.add(paymentMethod);
                    }

                    for (int i = 0; i < shippingMethodObject.length(); i++){
                        PaymentShippingMethod paymentShippingMethod = new PaymentShippingMethod();
                        JSONObject shippingObject = shippingMethodObject.getJSONObject(i);
                        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                            paymentShippingMethod.setShippingCode(shippingObject.getString("code"));
                            paymentShippingMethod.setName(shippingObject.getString("carrierName"));
                            paymentShippingMethod.setPrice(shippingObject.getString("value"));
                        }else {
                            paymentShippingMethod.setCode(shippingObject.getInt("code"));
                            paymentShippingMethod.setName(shippingObject.getString("name"));
                            paymentShippingMethod.setPrice(shippingObject.getString("price"));
                        }
                        mPaymentShippingMethod.add(paymentShippingMethod);
                    }
                    mMainLayout.setVisibility(View.VISIBLE);
                    mPaymentMethodAdapter = new PaymentMethodAdapter(getActivity(), mPaymentMethod);
                    mPaymentMethodList.setAdapter(mPaymentMethodAdapter);

                    spinnerMethod();
                    gettingValues();

                }else {
                    mCheckoutActivity.snackBar(jsonObject.getString("errormsg"));
                }

               /* mPaymentMainModel = (MyApplication.getGsonInstance().fromJson(response, PaymentMainModel.class));
                if (mPaymentMainModel.getStatus().equalsIgnoreCase("true")) {
                    mMainLayout.setVisibility(View.VISIBLE);
                    mPaymentResult = mPaymentMainModel.getResult();
                    mPaymentMethod = mPaymentResult.getPaymentMethod();
                    mPaymentShippingMethod = mPaymentResult.getShippingMethod();
                    mPaymentPrices = mPaymentResult.getPrices();

                    mPaymentMethodAdapter = new PaymentMethodAdapter(getActivity(), mPaymentMethod);
                    mPaymentMethodList.setAdapter(mPaymentMethodAdapter);

                    spinnerMethod();
                    gettingValues();

                }*/
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        /**
         * This method is used to apply Shipping Price failure Response
         * @param errorResponse This parameter getting from failure response method.
         */
        @Override
        public void Failure(String errorResponse) {
            //checkoutActivity.snackBar(getString(R.string.internal_server_error));
        }
    };

    /**
     * This method is used to Spinner Listener
     */
    private void spinnerMethod() {
        ArrayAdapter<PaymentShippingMethod> spinnerArrayAdapter = new ArrayAdapter<PaymentShippingMethod>(getActivity(),
                R.layout.spinner_text, mPaymentShippingMethod);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mPaymentCourierSpinner.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
        mPaymentCourierSpinner.setAdapter(spinnerArrayAdapter);

        mPaymentCourierSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.payment_page_paymentLabel));
                ((TextView) parent.getChildAt(0)).setTypeface(mCheckoutActivity.robotoLight);
                mCheckoutActivity.shippingMethod = mPaymentShippingMethod.get(position).getName();
                mShippingMethodId = String.valueOf(mPaymentShippingMethod.get(position).getCode());
                mShippingMethodName = mPaymentShippingMethod.get(position).getName();
                SharedPreference.getInstance().save("shippingMethod", mPaymentShippingMethod.get(position).getShippingCode());
                mCheckoutActivity.showProgDialiog();
                String shippingName;
                if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)){
                    shippingName = mPaymentShippingMethod.get(position).getShippingCode();
                }else {
                    shippingName = mShippingMethodName;
                }
                mCheckoutController.shippingPrice(mShippingMethodId, shippingName, shippingPriceResponse);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     * This method is used to Set Values to Payment Summary
     */
    private void gettingValues() {
        mPaymentSubTotal.setText(mCurrenySymbol + " " + mPaymentPrices.getSubtotal());
        mPaymentShippinhHandling.setText(mCurrenySymbol + " " + mPaymentPrices.getShippingAmount());
        mPaymentTax.setText(mCurrenySymbol + " " + mPaymentPrices.getTax());
        mPaymentDiscount.setText(mCurrenySymbol + " " + mPaymentPrices.getDiscount());
        mPaymentAmount.setText(mCurrenySymbol + " " + mPaymentPrices.getGrandtotal());
    }

    /**
     * This method is calling when fragment destroyed.
     */
    @Override
    public void onDestroyView() {
        if (mView.getParent() != null) {
            ((ViewGroup) mView.getParent()).removeView(mView);
        }
        super.onDestroyView();
    }

    /**
     * The Shipping price response.
     */
    VolleyCallback shippingPriceResponse = new VolleyCallback() {
        /**
         * This method is used to apply Shipping Price success Response
         * @param response This parameter getting from success response method.
         */
        @Override
        public void Success(String response) {
            mCheckoutActivity.hideProgDialog();
            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equals("true")) {
                    JSONObject priceValues = jsonObject.getJSONObject("prices");
                    mPaymentSubTotal.setText(mCurrenySymbol + " " + priceValues.getString("subtotal"));
                    mPaymentShippinhHandling.setText(mCurrenySymbol + " " + priceValues.getString("shipping_amount"));
                    mPaymentTax.setText(mCurrenySymbol + " " + priceValues.getString("tax"));
                    mPaymentDiscount.setText(mCurrenySymbol + " " + priceValues.getString("discount"));
                    mPaymentAmount.setText(mCurrenySymbol + " " + priceValues.getString("grandtotal"));
                    if (priceValues.getString("tax") != null){
                        sTax = priceValues.getString("tax");
                    }
                    if (priceValues.getString("discount")!=null) {
                        sDiscount = priceValues.getString("discount");
                    }
                    if (priceValues.getString("shipping_amount")!=null) {
                        sShippingAmount = priceValues.getString("shipping_amount");
                    }
                }

                // Add the step number and additional info about the checkout to the action.
                ProductAction productAction = new ProductAction(ProductAction.ACTION_CHECKOUT)
                        .setCheckoutStep(2)
                        .setCheckoutOptions("Payment");
                HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder()
                        .setProductAction(productAction);

                GoogleAnalytics googleAnalytics = GoogleAnalytics.getInstance(getActivity());
                //Tracker sTracker = googleAnalytics.newTracker(R.xml.app_tracker);
                Tracker sTracker = googleAnalytics.newTracker(SharedPreference.getInstance().getValue("google_tracking"));
                sTracker.setScreenName("Payment Checkout");
                sTracker.send(builder.build());

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        /**
         * This method is used to apply Shipping failure Response
         * @param errorResponse This parameter getting from failure response method.
         */
        @Override
        public void Failure(String errorResponse) {
            mCheckoutActivity.hideProgDialog();
        }
    };
}
