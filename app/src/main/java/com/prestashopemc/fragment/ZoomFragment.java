package com.prestashopemc.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.prestashopemc.R;

import java.util.ArrayList;
import java.util.List;
//import egrove.newbasemagento.adapter.ImageviewAdapter;

/**
 * Created by dinakaran on 5/4/16.
 */
public class ZoomFragment extends Fragment {

    private List<String> itemList = new ArrayList<String>();
    private ImageView mProductImage;
    private String mImageUrl;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.pager_image_item, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mProductImage = (ImageView) view.findViewById(R.id.imageView);
    }
}
