package com.prestashopemc.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prestashopemc.R;
import com.prestashopemc.adapter.HomePageAdapter;
import com.prestashopemc.database.RecentSearchService;
import com.prestashopemc.model.RecentSearchModel;
import com.prestashopemc.view.MainActivity;

/**
 * <h1>Home Fragment!</h1>
 * The Home Fragment contains home page details like category and banner.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 11 /3/16
 */
public class HomeFragment extends Fragment {
    private LinearLayoutManager mLinearLayout;
    private HomePageAdapter mHomePageAdapter;
    private RecyclerView mCategoryListView;
    private MainActivity activity;
    private RecentSearchModel recentSearchModel;
    private RecentSearchService recentSearchService;

    /**
     * This method is used to create view for fragment.
     * @param inflater This is the first parameter to onCreateView method.
     * @param container This is the second parameter to onCreateView method.
     * @param savedInstanceState This is the third parameter to onCreateView method.
     * @return View returs layout view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.home_fragment, container, false);
    }

    /**
     * This method is used to created view for fragment.
     * @param view This is the first parameter to onViewCreated method.
     * @param savedInstanceState This is the first parameter to onViewCreated method.
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity = (MainActivity) getActivity();
        initializedLayout(view);
    }

    /**
     * This method is used to Initalized Layout
     * @param view This is the parameter to initalizedLayout method.
     * */
    private void initializedLayout(View view) {
        recentSearchService = new RecentSearchService(getActivity());
        mLinearLayout = new LinearLayoutManager(getActivity());
        mCategoryListView = (RecyclerView) view.findViewById(R.id.category_recycler_view);
        mCategoryListView.setLayoutManager(mLinearLayout);
        mCategoryListView.setHasFixedSize(true);
        mCategoryListView.setNestedScrollingEnabled(true);
        if (activity.mainPageList != null){
            mHomePageAdapter = new HomePageAdapter(getActivity(), activity.mainPageList);
            mCategoryListView.setAdapter(mHomePageAdapter);
        }

        //mSearchEdit = (CustomEdittext) view.findViewById(R.id.search);
        //mSearchEdit.setHint(getString(R.string.baseprestashop_search));

         /*Search Click Listener*/
        /*mSearchEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                        || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    if (!mSearchEdit.getText().toString().trim().isEmpty()) {
                        activity.showProgDialiog();
                        recentSearchModel = new RecentSearchModel();
                        recentSearchModel.setSearchKeyword(mSearchEdit.getText().toString().trim());
                        List<RecentSearchModel> recentSearchModels = new ArrayList<RecentSearchModel>();
                        recentSearchModels.add(recentSearchModel);
                        try {
                            recentSearchService.insertRecentSearch(recentSearchModels);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        Intent searchIntent = new Intent(getActivity(), SearchActivity.class);
                        searchIntent.putExtra("search_text", mSearchEdit.getText().toString().trim());
                        mSearchEdit.getText().clear();
                        startActivity(searchIntent);
                    } else {
                        activity.snackBar(getString(R.string.search_keyword));
                    }
                }
                return false;
            }
        });*/
    }
}
