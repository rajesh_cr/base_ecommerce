package com.prestashopemc.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.adapter.MagentoRelatedProductsAdapter;
import com.prestashopemc.adapter.RelatedProductsAdapter;
import com.prestashopemc.controller.ProductDetailController;
import com.prestashopemc.model.CategoryProduct;
import com.prestashopemc.model.MagentoCategoryProduct;
import com.prestashopemc.model.MagentoProduct;
import com.prestashopemc.model.MagentoProductMain;
import com.prestashopemc.model.RelatedProductsMain;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.VolleyCallback;
import com.prestashopemc.view.MagentoProductDetailActivity;
import com.prestashopemc.view.ProductDetailActivity;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Payment Fragment!</h1>
 * The Payment Fragment contains payment details view and functionality.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 5 /4/16
 */
public class RelatedProductFragment extends Fragment {

    private RecyclerView mProductListView;
    private ProductDetailController mProductDetailController;
    private int mCurrentPage;
    private ProductDetailActivity mProductDetailActivity;
    private MagentoProductDetailActivity mProductMagentoDetailActivity;
    private String mProductId, mProductListLayout;
    private RelatedProductsMain mRelatedProductsMain;
    private List<CategoryProduct> mRelatedResults = new ArrayList<CategoryProduct>();
    private List<MagentoCategoryProduct> mRelatedMagentoResults = new ArrayList<MagentoCategoryProduct>();
    private RelatedProductsAdapter mRelatedProductsAdapter;
    private MagentoRelatedProductsAdapter mMagentoRelatedProductsAdapter;
    private int mPreviousTotal = 0;
    private boolean mLoading = true;
    private int mVisibleThreshold = 5;
    private int mFirstVisibleItem, mVisibleItemCount, mTotalItemCount;
    private ProgressBar mProgressBar, mBottomProgressBar;
    private int mProductCount = 0;
    private int mTotalPageNumber = 0;
    private GridLayoutManager mGridLayoutManger;
    private TextView mErrorText;
    private View mView;

    /**
     * This method will call when creating fragment view.
     *
     * @param inflater           This is the first parameter to onCreateView method.
     * @param container          This is the second parameter to onCreateView method.
     * @param savedInstanceState This is the third parameter to onCreateView method.
     * @return View returs layout view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ///if (mView == null){
        mView = inflater.inflate(R.layout.related_products_layout, container, false);
        initializedLayout(mView);
        //}
        return mView;
    }

    /**
     * This method will call when fragment view is created..
     *
     * @param view               This is the first parameter to onViewCreated method.
     * @param savedInstanceState This is the first parameter to onViewCreated method.
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

   /* @Override
    public void onResume() {
        super.onResume();
        if (ProductListActivity.isWishList) {
            ProductListActivity.isWishList = true;
            relatedResults.clear();
            productDetailController.relatedProducts(productId, 1+"", relatedProductsList);
        }
    }
*/

    /**
     * This method is used to Initialized Layout
     *
     * @param view This is the parameter to initializedLayout method.
     */
    private void initializedLayout(View view) {
        mCurrentPage = 1;

        if (AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
            mProductMagentoDetailActivity = (MagentoProductDetailActivity) getActivity();
            mProductId = mProductMagentoDetailActivity.productId;
        } else {
            mProductDetailActivity = (ProductDetailActivity) getActivity();
            mProductId = mProductDetailActivity.productId;
        }

        mProductDetailController = new ProductDetailController(getActivity());
        mProgressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        mBottomProgressBar = (ProgressBar) view.findViewById(R.id.bottom_progress_bar);
        mErrorText = (TextView) view.findViewById(R.id.error_text);
        mProductListView = (RecyclerView) view.findViewById(R.id.related_list_view);
        mProductListView.addOnScrollListener(mScrollListener);
        mGridLayoutManger = new GridLayoutManager(getActivity(), 2);
        mProductListView.setLayoutManager(mGridLayoutManger);
        mProgressBar.setVisibility(View.VISIBLE);
        mProductDetailController.relatedProducts(mProductId, mCurrentPage + "", relatedProductsList);
    }

    /**
     * The Related products list.
     */
    VolleyCallback relatedProductsList = new VolleyCallback() {
        /**
         * This method is used to related products Response
         * @param response This parameter getting from success response method.
         */
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = new JSONObject(response);
                if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                    if (jsonObject.getString("status").equals("true")) {
                        MagentoProductMain mMagentoProductMain = AppConstants.productListParser(response);
                        relatedMagentoProductsView(mMagentoProductMain.getMagentoProduct());
                    } else {
                        mProgressBar.setVisibility(View.GONE);
                        mProductListView.setVisibility(View.GONE);
                        mErrorText.setText(jsonObject.getString("errormsg"));
                        mErrorText.setVisibility(View.VISIBLE);
                    }
                } else {
                    mRelatedProductsMain = (MyApplication.getGsonInstance().fromJson(response, RelatedProductsMain.class));
                    if (mRelatedProductsMain.getStatus().equals("true")) {
                        relatedProductsView(mRelatedProductsMain);
                    } else {
                        mProgressBar.setVisibility(View.GONE);
                        mProductListView.setVisibility(View.GONE);
                        mErrorText.setText(jsonObject.getString("errormsg"));
                        mErrorText.setVisibility(View.VISIBLE);
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        /**
         * This method is used to related products Response
         * @param errorResponse This parameter getting from failure response method.
         */
        @Override
        public void Failure(String errorResponse) {
            mProgressBar.setVisibility(View.GONE);
            mProductListView.setVisibility(View.GONE);
            mErrorText.setText(errorResponse);
            mErrorText.setVisibility(View.VISIBLE);
        }
    };

    /**
     * This method is used to Related Products View
     *
     * @param mRelatedMain This parameter to relatedProductsView method.
     */
    private void relatedProductsView(RelatedProductsMain mRelatedMain) {
        mProgressBar.setVisibility(View.GONE);
        mBottomProgressBar.setVisibility(View.GONE);
        mProductListLayout = SharedPreference.getInstance().getValue("product_list_layout");
        if (mRelatedMain != null) {
            mRelatedResults.addAll(mRelatedMain.getResult().getCategoryProducts());
            mProductCount = mRelatedMain.getResult().getProductCount();
            if (mCurrentPage == 1) {
                totalPageNo();
                mRelatedResults.clear();
                mRelatedResults.addAll(mRelatedMain.getResult().getCategoryProducts());
                mRelatedProductsAdapter = new RelatedProductsAdapter(getActivity(), mRelatedResults, Integer.parseInt(mProductListLayout));
                mProductListView.setAdapter(mRelatedProductsAdapter);
            } else {
                mRelatedProductsAdapter.notifyDataSetChanged();
            }
        }
    }


    /**
     * This method is used to Related Products View
     *
     * @param mRelatedMain This parameter to relatedProductsView method.
     */
    private void relatedMagentoProductsView(MagentoProduct mRelatedMain) {
        mProgressBar.setVisibility(View.GONE);
        mBottomProgressBar.setVisibility(View.GONE);
        mProductListLayout = SharedPreference.getInstance().getValue("product_list_layout");
        if (mRelatedMain != null) {
            mRelatedMagentoResults.addAll(mRelatedMain.getCategoryProducts());
            mProductCount = mRelatedMain.getProductCount();
            if (mCurrentPage == 1) {
                totalPageNo();
                mRelatedMagentoResults.clear();
                mRelatedMagentoResults.addAll(mRelatedMain.getCategoryProducts());
                mMagentoRelatedProductsAdapter = new MagentoRelatedProductsAdapter(getActivity(), mRelatedMagentoResults, Integer.parseInt(mProductListLayout));
                mProductListView.setAdapter(mMagentoRelatedProductsAdapter);
            } else {
                mMagentoRelatedProductsAdapter.notifyDataSetChanged();
            }
        }
    }

    private RecyclerView.OnScrollListener mScrollListener = new RecyclerView.OnScrollListener() {
        /**
         * This method is used to Recyclerview Scroll Listener
         */
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            mVisibleItemCount = mProductListView.getChildCount();
            mTotalItemCount = mGridLayoutManger.getItemCount();
            mFirstVisibleItem = mGridLayoutManger.findFirstVisibleItemPosition();

            if (mLoading) {
                if (mTotalItemCount > mPreviousTotal) {
                    mLoading = false;
                    mPreviousTotal = mTotalItemCount;
                }
            }
            if (!mLoading && (mTotalItemCount - mVisibleItemCount)
                    <= (mFirstVisibleItem + mVisibleThreshold)) {
                // End has been reached
                mBottomProgressBar.setVisibility(View.VISIBLE);
                mCurrentPage++;
                //Log.e("Total page number === ", totalPageNumber+"");
                if (mTotalPageNumber >= mCurrentPage) {
                    mProductDetailController.relatedProducts(mProductId, mCurrentPage + "", relatedProductsList);
                } else {
                    mBottomProgressBar.setVisibility(View.GONE);
                }

                mLoading = true;
            }
        }
    };

    /**
     * This method is used to Find total page numbers
     */
    private void totalPageNo() {
        int extraPage = 0;
        if ((mProductCount % AppConstants.productsPerPage) == 0) {
            extraPage = 0;
        } else {
            extraPage = 1;
        }
        mTotalPageNumber = (mProductCount / AppConstants.productsPerPage) + extraPage;
    }

    /**
     * This method is used to destroy view.
     */
    @Override
    public void onDestroyView() {
        if (mView.getParent() != null) {
            ((ViewGroup) mView.getParent()).removeView(mView);
        }
        super.onDestroyView();
    }

}
