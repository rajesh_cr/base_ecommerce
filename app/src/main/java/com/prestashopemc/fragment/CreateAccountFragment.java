package com.prestashopemc.fragment;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.controller.LoginController;
import com.prestashopemc.customView.CustomButton;
import com.prestashopemc.model.RegisterValues;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CommonValidation;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.view.AccountActivity;
import com.prestashopemc.view.BaseActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * <h1>Create account Fragment!</h1>
 * The Create account Fragment contains create account details..
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author pradeep
 * @version 1.2
 * @since 5 /8/16
 */
public class CreateAccountFragment extends Fragment implements View.OnClickListener
{

    private CustomButton mSignWithGoogle, mSignWithFb, mSendOtp, mResendOtp, mSubmit, mCreateAccount;
    private GradientDrawable mFacebookGradient, mGoogleGradient, mSendOtpGradient, mResendOtpGradient,
            mSubmitGradient;
    private EditText mMobileEdit, mOtpEdit, mFirstNameEdit, mLastNameEdit, mEmailEdit, mPasswordEdit, mConfirmPasswordEdit, mMobileNumEdit;
    private TextView mSignInText, mAlreadyText, mSiginWithText;
    private ImageView mProfileImg;
    private AccountActivity mAccountActivity;
    private LoginController mLoginController;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    /**
     * The User choosen task.
     */
    public String userChoosenTask;
    private Bitmap mBitmap;
    private File mFile;
    /**
     * The My permissions request read external storage.
     */
    public final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    /**
     * This method is used to create fragment.
     * @param savedInstanceState This is the parameter to onCreateView method.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * This method is used to create view for fragment.
     * @param inflater This is the first parameter to onCreateView method.
     * @param container This is the second parameter to onCreateView method.
     * @param savedInstanceState This is the third parameter to onCreateView method.
     * @return View returs layout view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.create_account_fragment, container, false);
    }

    /**
     * This method is used to created view for fragment.
     * @param view This is the first parameter to onViewCreated method.
     * @param savedInstanceState This is the first parameter to onViewCreated method.
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        MyApplication.getDefaultTracker("Create Account Screen");
        initalizeLayout(view);
        loadBackGroundColor();
    }

    /**
     * This method is used to Backgound Color*/
    private void loadBackGroundColor() {
        AppConstants.backgroundColor(mGoogleGradient, R.color.google_color, getActivity(), R.color.google_color);
        AppConstants.backgroundColor(mFacebookGradient, R.color.facebook_color, getActivity(), R.color.facebook_color);
        AppConstants.backgroundColor(mSendOtpGradient, R.color.accent, getActivity(), R.color.red_color);
        AppConstants.backgroundColor(mResendOtpGradient, R.color.accent, getActivity(), R.color.third_primary_color);
        AppConstants.backgroundColor(mSubmitGradient, R.color.red_color, getActivity(), R.color.red_color);
        //AppConstants.backgroundColor(mCreateAccountGradient, R.color.red_color, getActivity(), R.color.red_color);
    }

    /**
     * This method is used to Initalized Layout
     * @param view This is the parameter to initalizedLayout method.
     * */
    private void initalizeLayout(View view) {
        mLoginController = new LoginController(getActivity());
        mAccountActivity = (AccountActivity) getActivity();
        mAccountActivity.categoryTitle.setText(R.string.createAccountText);
        mSignWithGoogle = (CustomButton) view.findViewById(R.id.login_with_google);
        mSignWithFb = (CustomButton) view.findViewById(R.id.login_with_facebook);
        mSendOtp = (CustomButton) view.findViewById(R.id.send_otp);
        mResendOtp = (CustomButton) view.findViewById(R.id.resend_otp);
        mSubmit = (CustomButton) view.findViewById(R.id.submit);
        mCreateAccount = (CustomButton) view.findViewById(R.id.submit_button);
        mGoogleGradient = (GradientDrawable) mSignWithGoogle.getBackground();
        mFacebookGradient = (GradientDrawable) mSignWithFb.getBackground();
        mSendOtpGradient = (GradientDrawable) mSendOtp.getBackground();
        mResendOtpGradient = (GradientDrawable) mResendOtp.getBackground();
        mSubmitGradient = (GradientDrawable) mSubmit.getBackground();
        //mCreateAccountGradient = (GradientDrawable) mCreateAccount.getBackground();
        mMobileEdit = (EditText) view.findViewById(R.id.mobile_number);
        mOtpEdit = (EditText) view.findViewById(R.id.otp_password);
        mFirstNameEdit = (EditText) view.findViewById(R.id.first_name);
        mLastNameEdit = (EditText) view.findViewById(R.id.last_name);
        mEmailEdit = (EditText) view.findViewById(R.id.email_id);
        mPasswordEdit = (EditText) view.findViewById(R.id.password);
        mConfirmPasswordEdit = (EditText) view.findViewById(R.id.confirm_password);
        mMobileNumEdit = (EditText) view.findViewById(R.id.mobile_num);
        mProfileImg = (ImageView) view.findViewById(R.id.profile_image);
        mSignInText = (TextView) view.findViewById(R.id.sign_in_an_account);
        mAlreadyText = (TextView) view.findViewById(R.id.already_have_an_account);
        mSiginWithText = (TextView) view.findViewById(R.id.sign_in_with);
        CustomBackground.setBackgroundText(mCreateAccount);
        mCreateAccount.setOnClickListener(this);
        mProfileImg.setOnClickListener(this);
        mSignInText.setOnClickListener(this);
        dynamicTextValue();
        setCustomFont();
    }

    /**
     * This method is used to Dynamic Text Value Settings*/
    private void dynamicTextValue() {
        mAccountActivity.categoryTitle.setText(AppConstants.getTextString(getActivity(), AppConstants.createAccountText));
        mFirstNameEdit.setHint(AppConstants.getTextString(getActivity(), AppConstants.firstnameText));
        mLastNameEdit.setHint(AppConstants.getTextString(getActivity(), AppConstants.lastnameText));
        mEmailEdit.setHint(AppConstants.getTextString(getActivity(), AppConstants.emailText));
        mPasswordEdit.setHint(AppConstants.getTextString(getActivity(), AppConstants.passwordText));
        mConfirmPasswordEdit.setHint(AppConstants.getTextString(getActivity(), AppConstants.confirmPasswordText));
        mMobileNumEdit.setHint(AppConstants.getTextString(getActivity(), AppConstants.mobileNumberText));
        mCreateAccount.setText(AppConstants.getTextString(getActivity(), AppConstants.submitText));
        mSignInText.setText(AppConstants.getTextString(getActivity(), AppConstants.signInText));
        mAlreadyText.setText(AppConstants.getTextString(getActivity(), AppConstants.alreadyHaveAccountText));
    }

    /**
     * This method is used to Set Custom Font*/
    private void setCustomFont() {
        mMobileEdit.setTypeface(mAccountActivity.robotoRegular);
        mOtpEdit.setTypeface(mAccountActivity.robotoRegular);
        mFirstNameEdit.setTypeface(mAccountActivity.robotoRegular);
        mLastNameEdit.setTypeface(mAccountActivity.robotoRegular);
        mEmailEdit.setTypeface(mAccountActivity.robotoRegular);
        mPasswordEdit.setTypeface(mAccountActivity.robotoRegular);
        mConfirmPasswordEdit.setTypeface(mAccountActivity.robotoRegular);
        mSendOtp.setTypeface(mAccountActivity.robotoRegular);
        mResendOtp.setTypeface(mAccountActivity.robotoRegular);
        mSubmit.setTypeface(mAccountActivity.robotoRegular);
        mCreateAccount.setTypeface(mAccountActivity.robotoRegular);
        mSignWithGoogle.setTypeface(mAccountActivity.robotoMedium);
        mSignWithFb.setTypeface(mAccountActivity.robotoMedium);
        mSignInText.setTypeface(mAccountActivity.robotoRegular);
        mAlreadyText.setTypeface(mAccountActivity.robotoRegular);
        mSiginWithText.setTypeface(mAccountActivity.robotoRegular);
    }

    /**
     * This method is used to Choose from Gallery
     */
    public void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.selectFileText)), SELECT_FILE);
    }

    /**
     * This method is used to Choose From Camera
     */
    public void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    /**
     * This method is used to getting selected image or camera image result.
     * @param requestCode This is the first parameter to onActivityResult method.
     * @param resultCode This is the second parameter to onActivityResult method.
     * @param data This is the third parameter to onActivityResult method.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data);
            }

        }
    }

    /**
     * This method is used to Check System Permissions*/
    private void loadPermissions() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
                alertBuilder.setCancelable(true);
                alertBuilder.setTitle("Permission necessary");
                alertBuilder.setMessage("External storage permission is necessary");
                alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                    }
                });
                AlertDialog alert = alertBuilder.create();
                alert.show();

            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

            }
        } else {

            if (userChoosenTask.equalsIgnoreCase(getString(R.string.takePhotoText))) {
                cameraIntent();
            } else {
                galleryIntent();
            }

        }
    }


    /**
     *  This method is used to Check Lollipop version
     *  @return boolean returns boolean value for chekcing device is lollipop or not*/
    private boolean isLollipop() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    /**
     *  This method is used to Select From Gallery
     *  @param data This is the parameter to onSelectFromGalleryResult method.
     *  */
    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        mBitmap = null;
        File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        if (data != null) {
            try {
                mBitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                mBitmap.compress(Bitmap.CompressFormat.JPEG, 20, bytes);
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String gallery = destination.getAbsolutePath();
        mFile = new File(gallery);
        mProfileImg.setImageBitmap(AppConstants.getCircleBitmap(mBitmap));
        //mProfileImg.setImageBitmap(bitmap);
    }

    /**
     *  This method is used to Capture From Camera
     *  @param data This is the parameter to onCaptureImageResult method.
     *  */
    private void onCaptureImageResult(Intent data) {

        mBitmap = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        mBitmap.compress(Bitmap.CompressFormat.JPEG, 20, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String camera = destination.getAbsolutePath();
        mFile = new File(camera);
        mProfileImg.setImageBitmap(AppConstants.getCircleBitmap(mBitmap));
        //mProfileImg.setImageBitmap(bitmap);
    }

    /**
     * This method is used for onclick action
     * @param v This is the parameter to onClick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            /**
             * Creat Account Listener*/
            case R.id.submit_button:
                String confirmPassword = "";
                RegisterValues registerValues;
                if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                    confirmPassword = mConfirmPasswordEdit.getText().toString();
                    registerValues = new RegisterValues();
                    registerValues.setFirstName(mFirstNameEdit.getText().toString().trim());
                    registerValues.setLastName(mLastNameEdit.getText().toString().trim());
                    registerValues.setEmail(mEmailEdit.getText().toString());
                    registerValues.setPassword(mPasswordEdit.getText().toString());
                    registerValues.setAndroidId(SharedPreference.getInstance().getValue("fcm_token"));
                    registerValues.setMobileNum(mMobileNumEdit.getText().toString());
                }else {
                    confirmPassword = mConfirmPasswordEdit.getText().toString();
                    registerValues = new RegisterValues();
                    registerValues.setFirstName(AppConstants.stringEncode(mFirstNameEdit.getText().toString().trim()));
                    registerValues.setLastName(AppConstants.stringEncode(mLastNameEdit.getText().toString().trim()));
                    registerValues.setEmail(mEmailEdit.getText().toString());
                    registerValues.setPassword(mPasswordEdit.getText().toString());
                    registerValues.setAndroidId(SharedPreference.getInstance().getValue("fcm_token"));
                    registerValues.setMobileNum(mMobileNumEdit.getText().toString());
                }
                CommonValidation validation = new CommonValidation();
                boolean status = validation.registerValidation(getActivity(), registerValues, confirmPassword);
                if (status) {

                    mLoginController.createAccountImage(registerValues, "", mFile);
                } else {
                    BaseActivity.hideProgDialog();
                }
                break;

            /**
             * Already Have Account Sign In method*/
            case R.id.sign_in_an_account:
                Intent intent = new Intent(getActivity(), AccountActivity.class);
                startActivity(intent);
                getActivity().finish();
                break;

            case R.id.profile_image:
                final CharSequence[] items = {AppConstants.getTextString(getActivity(),AppConstants.takePhotoText),
                        AppConstants.getTextString(getActivity(), AppConstants.galleryText),
                        AppConstants.getTextString(getActivity(), AppConstants.cancelText)};

                AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                builder.setTitle(AppConstants.getTextString(getActivity(),AppConstants.selectPhotoText));
                builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (items[item].equals(AppConstants.getTextString(getActivity(),AppConstants.takePhotoText))) {
                            userChoosenTask = AppConstants.getTextString(getActivity(),AppConstants.takePhotoText);

                            if (!isLollipop()) {
                                cameraIntent();
                            } else {
                                loadPermissions();
                            }

                        } else if (items[item].equals( AppConstants.getTextString(getActivity(), AppConstants.galleryText))) {
                            userChoosenTask =  AppConstants.getTextString(getActivity(), AppConstants.galleryText);
                            if (!isLollipop()) {
                                galleryIntent();
                            } else {
                                loadPermissions();
                            }

                        } else if (items[item].equals(AppConstants.getTextString(getActivity(), AppConstants.cancelText))) {
                            dialog.dismiss();
                        }
                    }
                });
                builder.show();
                break;
        }
    }
}