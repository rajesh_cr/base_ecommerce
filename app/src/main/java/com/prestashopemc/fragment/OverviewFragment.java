package com.prestashopemc.fragment;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.analytics.ecommerce.Product;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.view.MagentoProductDetailActivity;
import com.squareup.picasso.Picasso;
import com.prestashopemc.R;
import com.prestashopemc.controller.ProductListController;
import com.prestashopemc.customView.CustomTextView;
import com.prestashopemc.customView.TouchImageView;
import com.prestashopemc.model.CustomAttribute;
import com.prestashopemc.model.KeyValues;
import com.prestashopemc.model.ProductDetail;
import com.prestashopemc.model.Productinfo;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.view.ProductDetailActivity;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

//import egrove.newbasemagento.adapter.ImageviewAdapter;

/**
 * <h1>Overview Fragment!</h1>
 * The Overview Fragment contains product overview.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 26 /3/16
 */
public class OverviewFragment extends Fragment implements View.OnClickListener {

    private TouchImageView mZoomImage;
    private ImageView mProducImage, mProductShare, mProductLabel1, mProductLabel2, mProductLabel3, mProductLabel4, mProductWishlist, mCloseIcon, mLeftArrow, mRightArrow, mAddCart, mQuantityMinus, mQuantityPlus;
    private RelativeLayout mTopLayout, mZoomLayout;
    private RatingBar mReviewRatingBar;
    private LinearLayout mSpinnerLayout, mSpinnerMainLayout, mImageLayout, mImageIndicator, mEstimatedLayout;
    private int mImageCount = 1;
    private CustomTextView mProductName, mProductSku, mProductDescription, mReviewsText, mPrice, mSpecialPrice, mQuantityNumber, mNumberOfQuantity;
    private CustomTextView mEstimateStartDate, mEstimateEndDate, mEstimateStartDelivery, mEstimateEndDelivery, mOutOfStockText, mProductSKU, mEstimatedTitleText;
    private String mLeftTop, mLeftBottom, mRightTop, mRightBottom;
    private Productinfo mProductInfo;
    private ProductDetail mProductDetail;
    private ProductDetailActivity mPrdDetailActivity;
    private ImageView mConifgureImg[];
    private int mOutofStock;
    private int mPosition, mQuantityCount = 1;
    private int[] mCount = {1};
    private ProductListController mProductListController;
    private String mProductId;
    private List<KeyValues> mKeyvalues;
    private List<CustomAttribute> mCustomAttribute;
    private String mProductAttributeId, mRibbionVisibilty;
    /**
     * The M key value string.
     */
    ArrayList<String> mKeyValueString = new ArrayList<String>();
    private HorizontalScrollView mHorizontalView;
    /**
     * The constant sQuantityCount.
     */
    public static int sQuantityCount = 1;
    /**
     * The constant sProductAttributeId.
     */
    public static String sProductAttributeId;

    /**
     * This method will call when creating fragment view.
     *
     * @param inflater           This is the first parameter to onCreateView method.
     * @param container          This is the second parameter to onCreateView method.
     * @param savedInstanceState This is the third parameter to onCreateView method.
     * @return View returs layout view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ProductDetailActivity prdActivity = (ProductDetailActivity) getActivity();
        mProductId = prdActivity.getProductid();
        return inflater.inflate(R.layout.overview_fragment, container, false);
    }

    /**
     * This method will call when fragment view is created..
     *
     * @param view               This is the first parameter to onViewCreated method.
     * @param savedInstanceState This is the first parameter to onViewCreated method.
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeLayout(view);
    }

    /**
     * This method is used to Initalized Layout
     *
     * @param view This is the parameter to initalizeLayout method.
     */
    private void initializeLayout(View view) {
        mProductListController = new ProductListController(getActivity());
        mPrdDetailActivity = (ProductDetailActivity) getActivity();
        mTopLayout = (RelativeLayout) view.findViewById(R.id.top_layout);
        mZoomLayout = (RelativeLayout) view.findViewById(R.id.zoom_layout);
        mProducImage = (ImageView) view.findViewById(R.id.product_image);
        mProductLabel1 = (ImageView) view.findViewById(R.id.product_label_first);
        mProductLabel2 = (ImageView) view.findViewById(R.id.product_label_second);
        mProductLabel3 = (ImageView) view.findViewById(R.id.product_label_third);
        mProductLabel4 = (ImageView) view.findViewById(R.id.product_label_four);

        mProductWishlist = (ImageView) view.findViewById(R.id.product_wishlist);
        mProductShare = (ImageView) view.findViewById(R.id.share_image);
        mProductName = (CustomTextView) view.findViewById(R.id.product_name);
        mAddCart = (ImageView) view.findViewById(R.id.add_to_cart);
        mProductSku = (CustomTextView) view.findViewById(R.id.product_sku);
        mProductSKU = (CustomTextView) view.findViewById(R.id.sku);
        mPrice = (CustomTextView) view.findViewById(R.id.price);
        mSpecialPrice = (CustomTextView) view.findViewById(R.id.spl_price);
        mNumberOfQuantity = (CustomTextView) view.findViewById(R.id.no_Of_txt);
        mReviewRatingBar = (RatingBar) view.findViewById(R.id.review_rating_bar);
        mReviewsText = (CustomTextView) view.findViewById(R.id.reviews_text);
        mProductDescription = (CustomTextView) view.findViewById(R.id.product_short_description);
        mQuantityMinus = (ImageView) view.findViewById(R.id.quantity_minus);
        mQuantityPlus = (ImageView) view.findViewById(R.id.quantity_plus);
        mQuantityNumber = (CustomTextView) view.findViewById(R.id.quantity_text);
        mSpinnerLayout = (LinearLayout) view.findViewById(R.id.spinner_list_layout);
        mSpinnerMainLayout = (LinearLayout) view.findViewById(R.id.spin_main_lay);
        mEstimateStartDate = (CustomTextView) view.findViewById(R.id.estimated_text_startDate);
        mEstimateEndDate = (CustomTextView) view.findViewById(R.id.estimated_text_endDate);
        mEstimateStartDelivery = (CustomTextView) view.findViewById(R.id.estimated_text_startDelivery);
        mEstimateEndDelivery = (CustomTextView) view.findViewById(R.id.estimated_text_endDelivery);
        //mProductKeyvalue = (CustomTextView) view.findViewById(R.id.product_keyvalues);
        mOutOfStockText = (CustomTextView) view.findViewById(R.id.out_of_stock);
        mEstimatedLayout = (LinearLayout) view.findViewById(R.id.estimated_layout);
        mEstimatedTitleText = (CustomTextView) view.findViewById(R.id.estimated_shipping);

        mImageLayout = (LinearLayout) view.findViewById(R.id.image_layout);
        mHorizontalView = (HorizontalScrollView) view.findViewById(R.id.imagelist_layout);
        mImageIndicator = (LinearLayout) view.findViewById(R.id.indicator_layout);
        mCloseIcon = (ImageView) view.findViewById(R.id.close_icon);
        mZoomImage = (TouchImageView) view.findViewById(R.id.zoom_image);
        mLeftArrow = (ImageView) view.findViewById(R.id.left_arrow);
        mRightArrow = (ImageView) view.findViewById(R.id.right_arrow);
        mCloseIcon.setOnClickListener(this);
        mRightArrow.setOnClickListener(this);
        mLeftArrow.setOnClickListener(this);
        mProducImage.setOnClickListener(this);
        mAddCart.setOnClickListener(this);
        mQuantityPlus.setOnClickListener(this);
        mQuantityMinus.setOnClickListener(this);
        mProductWishlist.setOnClickListener(this);
        mProductShare.setOnClickListener(this);

        if (SharedPreference.getInstance().getValue("cart_section_display").equalsIgnoreCase("1")) {
            mAddCart.setVisibility(View.VISIBLE);
        }
        dynamicTextValue();
        mSpecialPrice.setPaintFlags(mSpecialPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        if (mPrdDetailActivity.wishlistStatus != null) {
            if (mPrdDetailActivity.wishlistStatus.equals("false")) {
                mProductWishlist.setVisibility(View.GONE);
            }
        }

        getProductDetails();

    }

    /**
     * This method is used to Dynamic Text Value Settings
     */
    private void dynamicTextValue() {
        mReviewsText.setText(AppConstants.getTextString(getActivity(), AppConstants.reviewsText));
        mNumberOfQuantity.setText(AppConstants.getTextString(getActivity(), AppConstants.noOfQuantityText));
        mEstimatedTitleText.setText(AppConstants.getTextString(getActivity(), AppConstants.estimatedShippingText));
        mEstimateStartDelivery.setText(AppConstants.getTextString(getActivity(), AppConstants.orderBeforeText));
        mEstimateEndDelivery.setText(AppConstants.getTextString(getActivity(), AppConstants.orderAfterText));
    }

    /**
     * This method is used to Bottom Image Border Indicator
     */
    private void imgBorder(int ii) {
        int x, y;

        for (int i = 0; i < mProductDetail.getImage().size(); i++) {
            mConifgureImg[i].setBackgroundResource(R.drawable.no_image_border);
        }

        if (mPosition == ii) {
            if (ii < 0) {
                CustomBackground.setImageBorder(mConifgureImg[0]);
            } else {
                x = mConifgureImg[ii].getLeft();
                y = mConifgureImg[ii].getRight();
                mHorizontalView.smoothScrollTo(x, y);
                CustomBackground.setImageBorder(mConifgureImg[ii]);
            }
        }

    }

    private void hideVisiblity() {
        mTopLayout.setVisibility(View.GONE);
    }

    private void showVisiblity() {
        mTopLayout.setVisibility(View.VISIBLE);
    }

    /**
     * This method is used to Get Product details From Product Deail Activity
     */
    public void getProductDetails() {

        mProductInfo = mPrdDetailActivity.productinfo;

        mProductDetail = mPrdDetailActivity.productDetail;
        /**Check Ribbion Enabled True or False*/
        if (mProductInfo.getRibbonEnabled()) {
            mPrdDetailActivity.getRibbionList();
            AppConstants.getDensityName(mPrdDetailActivity);
            Log.e("prdDetailActivity", "prdDetailActivity" + mPrdDetailActivity.getRibbionList().size());
            for (int i = 0; i < mPrdDetailActivity.getRibbionList().size(); i++) {
                Log.i("Ribbon name: ", String.valueOf(mPrdDetailActivity.getRibbionList().get(i).getRibbonImage().toString()));
                /**Check Ribbion Image not Null*/
                if (mPrdDetailActivity.getRibbionList().get(i).getRibbonImage() != null) {
                    mLeftTop = mPrdDetailActivity.getRibbionList().get(i).getLeftTop();
                    mLeftBottom = mPrdDetailActivity.getRibbionList().get(i).getLeftBottom();
                    mRightTop = mPrdDetailActivity.getRibbionList().get(i).getRightTop();
                    mRightBottom = mPrdDetailActivity.getRibbionList().get(i).getRightBottom();
                    mRibbionVisibilty = checkValue(mLeftTop, mLeftBottom, mRightTop, mRightBottom);
                    if (mRibbionVisibilty.contains("right_bottom")) {
                        mProductLabel4.setVisibility(View.VISIBLE);
                        Picasso.with(mPrdDetailActivity).load(mPrdDetailActivity.getRibbionList().get(i).getRibbonImage())
                                .placeholder(R.drawable.place_holder).resize(500, 500)
                                .into(mProductLabel4);
                    }
                    if (mRibbionVisibilty.contains("left_bottom")) {
                        mProductLabel3.setVisibility(View.VISIBLE);
                        Picasso.with(mPrdDetailActivity).load(mPrdDetailActivity.getRibbionList().get(i).getRibbonImage())
                                .placeholder(R.drawable.place_holder).resize(500, 500)
                                .into(mProductLabel3);
                    }
                    if (mRibbionVisibilty.contains("left_top")) {
                        mProductLabel1.setVisibility(View.VISIBLE);
                        Picasso.with(mPrdDetailActivity).load(mPrdDetailActivity.getRibbionList().get(i).getRibbonImage())
                                .placeholder(R.drawable.place_holder).resize(500, 500)
                                .into(mProductLabel1);
                    }
                    if (mRibbionVisibilty.contains("right_top")) {
                        mProductLabel2.setVisibility(View.VISIBLE);
                        Picasso.with(mPrdDetailActivity).load(mPrdDetailActivity.getRibbionList().get(i).getRibbonImage())
                                .placeholder(R.drawable.place_holder).resize(500, 500)
                                .into(mProductLabel2);
                    }
                }
            }
        }


        if (mProductDetail.getCustomAttributes().size() != 0) {
            mCustomAttribute = mProductDetail.getCustomAttributes();
        }

        if (mProductDetail.getImage().size() != 0) {
            Picasso.with(getActivity()).load(mProductDetail.getImage().get(0))
                    .placeholder(R.drawable.place_holder).into(mProducImage);
        }

        mOutofStock = Integer.parseInt(mProductInfo.getStock());
        if (mOutofStock == 0) {
            mProducImage.setAlpha((float) 0.2);
            mOutOfStockText.setVisibility(View.VISIBLE);
        }

        if (mProductDetail.getWishlist()) {
            mProductWishlist.setImageResource(R.drawable.ic_wishlist_enable);
        } else {
            mProductWishlist.setImageResource(R.drawable.ic_wishlist_disable);
        }
        mQuantityNumber.setText(String.valueOf(mCount[0]));
        mProductName.setTypeface(mPrdDetailActivity.robotoRegular);
        mProductName.setText(mProductInfo.getName());
        final String priceSymbol = SharedPreference.getInstance().getValue("currency_symbol");

        /**
         * Set Price and Spl price */
        if (mProductInfo.getSpecialprice().equalsIgnoreCase(getResources().getString(R.string.not_set)) || mProductInfo.getSpecialprice() == null) {
            mSpecialPrice.setText("");
            mPrice.setText(priceSymbol + " " + mProductInfo.getPrice());
        } else {
            mPrice.setText(priceSymbol + " " + mProductInfo.getSpecialprice());
        }
        if (mProductInfo.getPrice() != null) {
            if (!mProductInfo.getPrice().equalsIgnoreCase(getResources().getString(R.string.not_set))) {
                mSpecialPrice.setText(priceSymbol + " " + mProductInfo.getPrice());
            }
        }

        mProductDescription.setTypeface(mPrdDetailActivity.robotoLight);
        mProductDescription.setText(mProductInfo.getDescriptionShort());

        mProductSku.setTypeface(mPrdDetailActivity.robotoRegular);
        mProductSKU.setTypeface(mPrdDetailActivity.robotoRegular);
        mSpecialPrice.setTypeface(mPrdDetailActivity.robotoRegular);
        mPrice.setTypeface(mPrdDetailActivity.robotoRegular);
        mNumberOfQuantity.setTypeface(mPrdDetailActivity.robotoRegular);
        mQuantityNumber.setTypeface(mPrdDetailActivity.opensansRegular);
        mEstimatedTitleText.setTypeface(mPrdDetailActivity.robotoRegular);
        mEstimateStartDate.setTypeface(mPrdDetailActivity.robotoRegular);
        mEstimateEndDate.setTypeface(mPrdDetailActivity.robotoRegular);
        mEstimateStartDelivery.setTypeface(mPrdDetailActivity.robotoLight);
        mEstimateEndDelivery.setTypeface(mPrdDetailActivity.robotoLight);
        mReviewsText.setTypeface(mPrdDetailActivity.robotoRegular);

        if (mProductDetail.getSku().isEmpty()) {
            mProductSku.setVisibility(View.GONE);
            mProductSKU.setVisibility(View.GONE);
        } else {
            mProductSku.setText(mProductDetail.getSku());
        }

        mReviewsText.setText(mProductDetail.getReviewCount() + " " + AppConstants.getTextString(getActivity(), AppConstants.reviewsText));
        String estimatedFrom = mProductDetail.getEstimatedShippingFrom();
        String estimatedTo = mProductDetail.getEstimatedShippingTo();
        if (estimatedFrom == null) {
            mEstimatedTitleText.setVisibility(View.GONE);
            mEstimatedLayout.setVisibility(View.GONE);
        } else {
            mEstimateStartDate.setText(estimatedFrom);
            mEstimateEndDate.setText(estimatedTo);
        }

        mReviewRatingBar.setRating(Float.parseFloat(mProductDetail.getAverageRating() + ""));
        LayerDrawable stars = (LayerDrawable) mReviewRatingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(1).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);

        //For image indicator
        for (int i = 0; i < mProductDetail.getImage().size(); i++) {
            Button imageIndicator = new Button(getActivity());
            LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams((int) getResources().getDimension(R.dimen.imageIndicator_width),
                    (int) getResources().getDimension(R.dimen.imageIndicator_height));
            layoutParams1.setMargins(0, 0, (int) getResources().getDimension(R.dimen.imageIndicator_right), 0);
            layoutParams1.gravity = Gravity.CENTER;
            imageIndicator.setLayoutParams(layoutParams1);
            imageIndicator.setTag(i);
            if (i == 0) {
                AppConstants.setBackground(imageIndicator, R.drawable.indicator_circle, getActivity());
            } else {
                AppConstants.setBackground(imageIndicator, R.drawable.indicator_circle_light, getActivity());
            }
            mImageIndicator.addView(imageIndicator);
        }


        //For Dynamic spinner creator
        if (mProductDetail.getKeyValuesList().size() != 0) {

            mKeyvalues = mProductDetail.getKeyValuesList();

            if (mKeyvalues.size() != 0) {
                mSpinnerMainLayout.setVisibility(View.VISIBLE);
//            int keyvalueSize = mKeyvalues.size();

                for (int j = 0; j < mKeyvalues.size(); j++) {
                    TextView mConfigureSpinnerText = new TextView(getActivity());
                    final Spinner mConfigureSpinner = new Spinner(getActivity());

                    //Common layout for Text and Spinner
                    LinearLayout commonLinearLayout = new LinearLayout(getActivity());
                    commonLinearLayout.setOrientation(LinearLayout.VERTICAL);

                    LinearLayout.LayoutParams fullLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT, 0.9f);


                    LinearLayout.LayoutParams commonLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT, 0.9f);

                /*Create New Linear Layout*/
                    LinearLayout linLayoutSpinner = new LinearLayout(getActivity());
                    linLayoutSpinner.setOrientation(LinearLayout.HORIZONTAL);
                    LinearLayout.LayoutParams linLayoutParamSpinner = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                            LinearLayout.LayoutParams.WRAP_CONTENT);
                    linLayoutParamSpinner.setMargins(0, 0, 0, (int) getResources().getDimension(R.dimen.dynamicSpinner_layoutMarginBottom));
                    linLayoutSpinner.setLayoutParams(linLayoutParamSpinner);
                    linLayoutSpinner.setTag(j);
                    LinearLayout.LayoutParams layoutParams1 = null;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { //>= API 21
                        linLayoutSpinner.setBackground(getResources().getDrawable(R.drawable.background, getActivity().getTheme()));
                        layoutParams1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                (int) getResources().getDimension(R.dimen.dynamicSpinner_layoutHeight));
                    } else {
                        linLayoutSpinner.setBackground(getResources().getDrawable(R.drawable.background));
                        mConfigureSpinner.setBackgroundColor(ContextCompat.getColor(getActivity(),android.R.color.transparent));
                        layoutParams1 = new LinearLayout.LayoutParams((int) getResources().getDimension(R.dimen.dynamicSpinner_width),
                                (int) getResources().getDimension(R.dimen.dynamicSpinner_layoutHeight));
                    }

                    LinearLayout linLayoutText = new LinearLayout(getActivity());
                    linLayoutText.setOrientation(LinearLayout.VERTICAL);
                    LinearLayout.LayoutParams linLayoutParamText = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            LinearLayout.LayoutParams.MATCH_PARENT);
                    linLayoutParamText.setMargins((int) getResources().getDimension(R.dimen.dynamicSpinner_marginLeft), 0, 0, 0);
                    linLayoutText.setLayoutParams(linLayoutParamText);
                    linLayoutText.setTag(j);

                    //For text parms
                    LinearLayout.LayoutParams layoutParamsText = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT,
                            (int) getResources().getDimension(R.dimen.dynamicText_layoutHeight));
                    layoutParamsText.setMargins(0, 0, 0, 0);
                    layoutParamsText.gravity = Gravity.LEFT;
                    mConfigureSpinnerText.setLayoutParams(layoutParamsText);
                    mConfigureSpinnerText.setTag(j);
                    mConfigureSpinnerText.setTextSize((int) getResources().getDimension(R.dimen.dynamicText_textSize));
                    mConfigureSpinnerText.setTypeface(mPrdDetailActivity.robotoRegular);
                    mConfigureSpinnerText.setTextColor(getResources().getColor(R.color.product_spinner_title_text));
                    mConfigureSpinnerText.setText(mKeyvalues.get(j).getAttributeName() + " :");
                    linLayoutText.addView(mConfigureSpinnerText);
                    commonLinearLayout.addView(linLayoutText);

                    //For spinner params
                    layoutParams1.setMargins((int) getResources().getDimension(R.dimen.dynamicSpinner_marginLeft), 0,
                            (int) getResources().getDimension(R.dimen.dynamicSpinner_marginRight), 0);
                    layoutParams1.gravity = Gravity.CENTER;
                    mConfigureSpinner.setLayoutParams(layoutParams1);
                    mConfigureSpinner.setTag(j);
                    ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(),
                            android.R.layout.simple_spinner_dropdown_item, mKeyvalues.get(j).getAttributeValue());
                    mConfigureSpinner.setAdapter(spinnerArrayAdapter);
                    mConfigureSpinner.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                    linLayoutSpinner.addView(mConfigureSpinner);
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                        ImageView spinnerArrow = new ImageView(getActivity());
                        spinnerArrow.setImageResource(R.drawable.ic_spinner_arrow);
                        spinnerArrow.setPadding(0, (int) getResources().getDimension(R.dimen.setting_padding), 0, 0);
                        spinnerArrow.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                mConfigureSpinner.performClick();
                            }
                        });
                        linLayoutSpinner.addView(spinnerArrow);
                    }
                    commonLinearLayout.addView(linLayoutSpinner);

                    //Is odd or even


                    fullLayoutParams.setMargins((int) getResources().getDimension(R.dimen.commonLayout_leftmargin), 0, 0, 0);
                    commonLinearLayout.setLayoutParams(fullLayoutParams);
                    mSpinnerLayout.addView(commonLinearLayout);

                    mConfigureSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.spinner_Text_color));
                            ((TextView) parent.getChildAt(0)).setTypeface(mPrdDetailActivity.robotoRegular);
                            String value = parent.getSelectedItem().toString();
                            int configValue = Integer.parseInt(mConfigureSpinner.getTag() + "");
                            if (mKeyvalues.size() == mKeyValueString.size()) {
                                mKeyValueString.remove(configValue);
                                mKeyValueString.add(configValue, value);
                            } else {
                                mKeyValueString.add(value);
                            }

                            for (CustomAttribute custom : mCustomAttribute) {

                                if (equalLists(custom.getAttributeCombination(), mKeyValueString)) {
                                    mProductAttributeId = custom.getIdProductAttribute();
                                    sProductAttributeId = mProductAttributeId;

                                    if (custom.getPrice() != null) {
                                        mSpecialPrice.setText(priceSymbol + " " + custom.getPrice());
                                    }

                                    if (custom.getSpecialPrice() != null) {
                                        mPrice.setText(priceSymbol + " " + custom.getSpecialPrice());
                                    } else {
                                        mSpecialPrice.setText("");
                                        mPrice.setText(priceSymbol + " " + custom.getPrice());
                                    }
                                }
                            }


                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            }
        }
    }

    /**
     * This method is used to check equality of one are more than arrayList values
     */
    public boolean equalLists(List<String> attributeCombination, List<String> keyValueCombination) {
        if (attributeCombination == null && keyValueCombination == null) {
            return true;
        }

        if ((attributeCombination == null && keyValueCombination != null)
                || attributeCombination != null && keyValueCombination == null
                || attributeCombination.size() != keyValueCombination.size()) {
            return false;
        }

        attributeCombination = new ArrayList<String>(attributeCombination);
        keyValueCombination = new ArrayList<String>(keyValueCombination);

        Collections.sort(attributeCombination);
        Collections.sort(keyValueCombination);
        return attributeCombination.equals(keyValueCombination);
    }

    /**
     * This method is used to check product label location
     *
     * @param left_top     This is the first parameter to checkValue method.
     * @param left_bottom  This is the second parameter to checkValue method.
     * @param right_top    This is the third parameter to checkValue method.
     * @param right_bottom This is the fourth parameter to checkValue method.
     * @return String returns string value of ribbon.
     */
    public String checkValue(String left_top, String left_bottom, String right_top, String right_bottom) {
        String value = "";
        if (left_top.equals("1")) {
            value = "left_top";
        }
        if (left_bottom.equals("1")) {
            value = value + "left_bottom";
        }
        if (right_top.equals("1")) {
            value = value + "right_top";
        }
        if (right_bottom.equals("1")) {
            value = value + "right_bottom";
        }
        if (right_bottom.equals("1")) {
            value = value + "right_bottom";
        }
        return value;
    }

    /**
     * This method will call when click event occured.
     *
     * @param v This is the parameter to onClick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * Zoom Layout Listener
             */
            case R.id.product_image:
                Product relatedProduct = new Product()
                        .setId(mProductId)
                        .setName(mProductInfo.getName())
                        .setCategory(mProductInfo.getCategory());

                Product viewedProduct = new Product()
                        .setId(mProductId)
                        .setName(mProductInfo.getName())
                        .setCategory(mProductInfo.getCategory());

                ProductAction productAction = new ProductAction(ProductAction.ACTION_CLICK);
                HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder()
                        .addImpression(relatedProduct, "Related Products")
                        .addProduct(viewedProduct)
                        .setProductAction(productAction);

                GoogleAnalytics googleAnalytics = GoogleAnalytics.getInstance(getActivity());
                //Tracker sTracker = googleAnalytics.newTracker(R.xml.app_tracker);
                Tracker sTracker = googleAnalytics.newTracker(SharedPreference.getInstance().getValue("google_tracking"));
                sTracker.setScreenName("Product View");
                sTracker.send(builder.build());

                if (mOutofStock == 0) {
                    mPrdDetailActivity.updateCartFailure(AppConstants.getTextString(getActivity(), AppConstants.outOfStockText));
                } else {
                    hideVisiblity();
                    mZoomLayout.setVisibility(View.VISIBLE);


                    if (mProductDetail.getImage().size() == 0) {
                        Picasso.with(getActivity()).load(R.drawable.place_holder).resize(400, 400).centerCrop()
                                .into(mZoomImage);
                    } else {

                        Picasso.with(getActivity()).load(mProductDetail.getImage().get(0))
                                .placeholder(R.drawable.place_holder).resize(400, 400).centerCrop()
                                .into(mZoomImage);

                        mConifgureImg = new ImageView[mProductDetail.getImage().size()];
                        for (int i = 0; i < mProductDetail.getImage().size(); i++) {
                            mConifgureImg[i] = new ImageView(getActivity());
                            mConifgureImg[i].setPadding(2, 2, 2, 2);
                            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams((int) getResources().getDimension(R.dimen.zoom_dynamic_layout_width),
                                    (int) getResources().getDimension(R.dimen.zoom_dynamic_layout_height));
                            layoutParams.setMargins((int) getResources().getDimension(R.dimen.zoom_dynamic_layout_margin),
                                    0, (int) getResources().getDimension(R.dimen.zoom_dynamic_layout_margin), 0);
                            layoutParams.gravity = Gravity.CENTER;
                            mConifgureImg[i].setLayoutParams(layoutParams);
                            mConifgureImg[i].setScaleType(ImageView.ScaleType.FIT_XY);
                            Picasso.with(getActivity()).load(mProductDetail.getImage().get(i)).into(mConifgureImg[i]);
                            mImageLayout.addView(mConifgureImg[i]);
                            mConifgureImg[i].setBackgroundResource(R.drawable.no_image_border);

                        }
                        CustomBackground.setImageBorder(mConifgureImg[0]);
                    }
                }
                break;

            /**
             * Zoom layout Close Listener
             */
            case R.id.close_icon:
                showVisiblity();
                mZoomLayout.setVisibility(View.GONE);
                mImageLayout.removeAllViews();
                break;

            /**
             * Left Arrow Listener
             */
            case R.id.left_arrow:

                if (mImageCount != 0) {

                    if (mProductDetail.getImage().size() == mImageCount) {
                        mImageCount = mImageCount - 2;
                    } else {
                        mImageCount = mImageCount - 1;
                    }

                    Log.e("imageCount-------", mImageCount + "");
                    if (mImageCount < 0) {
                        mPrdDetailActivity.updateCartFailure(AppConstants.getTextString(getActivity(), AppConstants.noImagesText));
                    } else {

                        Picasso.with(getActivity()).load(mProductDetail.getImage().get(mImageCount))
                                .placeholder(R.drawable.place_holder).resize(265, 265).centerCrop()
                                .into(mZoomImage);

                        mPosition = mImageCount;
                        imgBorder(mPosition);
                    }

                } else {
                    mPrdDetailActivity.updateCartFailure(AppConstants.getTextString(getActivity(), AppConstants.noImagesText));
                }
                break;

            /**
             * Right Arrow Listener
             */
            case R.id.right_arrow:
                if (mImageCount < mProductDetail.getImage().size()) {
                    mPosition = mImageCount;
                    imgBorder(mPosition);
                    Picasso.with(getActivity()).load(mProductDetail.getImage().get(mImageCount))
                            .placeholder(R.drawable.place_holder).resize(400, 400).centerCrop()
                            .into(mZoomImage);
                    mImageCount = mImageCount + 1;
                    Log.e("imageCount++++++", mImageCount + "");
                } else {
                    mPrdDetailActivity.updateCartFailure(AppConstants.getTextString(getActivity(), AppConstants.noImagesText));
                }
                break;

            /**
             * Quantity Increase Listener
             */
            case R.id.quantity_plus:
                mCount[0]++;
                mQuantityNumber.setText(String.valueOf(mCount[0]));
                mQuantityCount = mCount[0];
                sQuantityCount = mQuantityCount;
               /* if (mOutofStock == 0) {
                    prdDetailActivity.updateCartFailure(AppConstants.getString(R.string.outOfStock, getActivity()));
                } else {
                    mCount[0]++;
                    mQuantityNumber.setText(String.valueOf(mCount[0]));
                    mQuantityCount = mCount[0];
                }*/
                break;

            /**
             * Quantity Decrease Listener
             */
            case R.id.quantity_minus:
                if (mCount[0] != 1) {
                    mCount[0]--;
                    mQuantityNumber.setText(String.valueOf(mCount[0]));
                    mQuantityCount = mCount[0];
                    sQuantityCount = mQuantityCount;
                }
                break;

            /**
             * Add to Cart Listener
             */
            case R.id.add_to_cart:
                MyApplication.eventTracking("cart click", "addto cart button clicked", "addto cart");
                //ecommerce tracking to get product action when click product image to view
                Product product = new Product()
                        .setId(mProductId)
                        .setName(mProductInfo.getName())
                        .setCategory(mProductInfo.getCategory());
                    /*.setBrand("Google")
                    .setVariant("Black")
                    .setPosition(1)
                    .setCustomDimension(1, "Member");*/
                ProductAction mProductAction = new ProductAction(ProductAction.ACTION_ADD)
                        .setProductActionList("Add to cart Results");

                HitBuilders.ScreenViewBuilder mBuilder = new HitBuilders.ScreenViewBuilder()
                        .addProduct(product)
                        .setProductAction(mProductAction);

                GoogleAnalytics mGoogleAnalytics = GoogleAnalytics.getInstance(getActivity());
                //Tracker sTracker = googleAnalytics.newTracker(R.xml.app_tracker);
                Tracker mTracker = mGoogleAnalytics.newTracker(SharedPreference.getInstance().getValue("google_tracking"));
                mTracker.setScreenName("Product Add to Cart");
                mTracker.send(mBuilder.build());

                mPrdDetailActivity.showProgDialiog();
                mKeyvalues = mProductDetail.getKeyValuesList();
                if (mKeyvalues.size() != 0) {
                    mProductListController.addToCart(mProductId, mQuantityCount + "", mProductAttributeId, mProductInfo.getName());
                } else {
                    mProductListController.addToCart(mProductId, mQuantityCount + "", "0", mProductInfo.getName());
                }
                mQuantityCount = 1;
                mCount[0] = 1;
                mQuantityNumber.setText("" + mCount[0]);

//            if (mOutofStock == 0) {
//                mPrdDetailActivity.updateCartFailure(AppConstants.getString(R.string.outOfStock, getActivity()));
//            } else {
//
//                if (mQuantityCount == 0) {
//                    mPrdDetailActivity.updateCartFailure(AppConstants.getString(R.string.selectQuantity, getActivity()));
//                } else {
//                    prdDetailActivity.showProgDialiog();
//                    mKeyvalues = productDetail.getKeyValuesList();
//                    if (mKeyvalues.size() != 0) {
//                        mProductListController.addToCart(productId, mQuantityCount + "",mProductAttributeId, mProductInfo.getName());
//                    } else {
//                        mProductListController.addToCart(productId, mQuantityCount + "", "0", mProductInfo.getName());
//                    }
//
//                    mCount[0] = 0;
//                    mQuantityNumber.setText("" + mCount[0]);
//                }
//            }
                break;

            /**
             * Wishlist Listener
             */
            case R.id.product_wishlist:
                if (!SharedPreference.getInstance().getValue("customerid").equals("0")) {
                    if (!mProductDetail.getWishlist()) {
                        mPrdDetailActivity.showProgDialiog();
                        mProductListController.addWishlist(mProductId, "addwishlist", mPrdDetailActivity.categoryProduct,null);
                    } else {
                        mPrdDetailActivity.showProgDialiog();
                        mProductListController.addWishlist(mProductId, "deletewishlist", mPrdDetailActivity.categoryProduct,null);
                    }
                } else {
                    mPrdDetailActivity.snackBar(getString(R.string.needLoginText));
                }
                break;

            /**
             * Product Share Listener
             */
            case R.id.share_image:
                String langId = SharedPreference.getInstance().getValue("id_language");
                String shareContent = AppConstants.sShareOptionController + "&id_product=" + mProductId + "&id_lang=" + langId;
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, shareContent);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
        }
    }
}
