package com.prestashopemc.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.prestashopemc.R;
import com.prestashopemc.adapter.ConfirmOrderListAdapter;
import com.prestashopemc.controller.CheckoutController;
import com.prestashopemc.model.Address;
import com.prestashopemc.model.AddressValues;
import com.prestashopemc.model.ConfirmList;
import com.prestashopemc.model.ConfirmMainModel;
import com.prestashopemc.model.ConfirmProduct;
import com.prestashopemc.model.MagentoDefaultAddressShipping;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.VolleyCallback;
import com.prestashopemc.view.CheckoutActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static com.prestashopemc.view.BaseActivity.hideProgDialog;

/**
 * <h1>Confirm Fragment!</h1>
 * The Confirm Fragment contains order confirm details..
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author Rajesh
 * @version 1.2
 * @since 30 /09/2016
 */
public class ConfirmFragment extends Fragment {

    private TextView mReviewYourOrderLabel, mShippingAddressLabel, mShippingName, mShippingAddress, mBillingAddressLabel, mBillingName, mBillingAddress, mPaymentMethodLabel, mPaymentMethodName, mShippingMethodLabel, mShippingMethodName, mPaymentSummary;
    private TextView mSubtotalLabel, mSubtotal, mShippingHandlingLabel, mShippingHandling, mTaxLabel, mTax, mDiscountLabel, mDiscount, mAmountLabel, mAmount;
    private RecyclerView mConfirmRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private CheckoutActivity mCheckoutActivity;
    private CheckoutController mCheckoutController;
    private ConfirmMainModel mConfirmMainModel;
    private ConfirmList mConfirmList;
    private List<ConfirmProduct> mConfirmProducts;
    private ConfirmOrderListAdapter mConfirmOrderListAdapter;
    private Address mAddressModel;
    private String mName, mAddress;
    private RelativeLayout mMainLayout;
    private MagentoDefaultAddressShipping mMagentoDefaultAddressShipping;

    /**
     * This method is used to create view for fragment.
     * @param inflater This is the first parameter to onCreateView method.
     * @param container This is the second parameter to onCreateView method.
     * @param savedInstanceState This is the third parameter to onCreateView method.
     * @return View returs layout view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_confirm, container, false);
    }

    /**
     * This method is used to created view for fragment.
     * @param view This is the first parameter to onViewCreated method.
     * @param savedInstanceState This is the first parameter to onViewCreated method.
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializedLayout(view);
    }

    /**
     *This method is used to Initalized Layout
     * @param view This is the parameter to initalizedLayout method.
     */
    private void initializedLayout(View view) {
        mLinearLayoutManager = new LinearLayoutManager(getActivity());
        mCheckoutActivity = (CheckoutActivity) getActivity();
        mCheckoutActivity.showProgDialiog();
        mMainLayout = (RelativeLayout) view.findViewById(R.id.confirm_main_layout);
        mConfirmRecyclerView = (RecyclerView) view.findViewById(R.id.confirm_order_list);
        mReviewYourOrderLabel = (TextView) view.findViewById(R.id.confirm_review_order);
        mShippingAddressLabel = (TextView) view.findViewById(R.id.confirm_shipping_address);
        mShippingName = (TextView) view.findViewById(R.id.shipping_name);
        mShippingAddress = (TextView) view.findViewById(R.id.ship_address);
        mBillingAddressLabel = (TextView) view.findViewById(R.id.confirm_billing_address);
        mBillingName = (TextView) view.findViewById(R.id.billing_name);
        mBillingAddress = (TextView) view.findViewById(R.id.bill_address);
        mPaymentMethodLabel = (TextView) view.findViewById(R.id.confirm_payment_method);
        mPaymentMethodName = (TextView) view.findViewById(R.id.confirm_payment_type);
        mShippingMethodLabel = (TextView) view.findViewById(R.id.confirm_shipping_method);
        mShippingMethodName = (TextView) view.findViewById(R.id.confirm_shipping_type);
        mPaymentSummary = (TextView) view.findViewById(R.id.payment_summary);
        mSubtotalLabel = (TextView) view.findViewById(R.id.payment_subtotal_label);
        mSubtotal = (TextView) view.findViewById(R.id.payment_subTotal);
        mShippingHandlingLabel = (TextView) view.findViewById(R.id.payment_shipping_label);
        mShippingHandling = (TextView) view.findViewById(R.id.payment_shippingCost);
        mTaxLabel = (TextView) view.findViewById(R.id.payment_tax_label);
        mTax = (TextView) view.findViewById(R.id.payment_tax);
        mDiscountLabel = (TextView) view.findViewById(R.id.payment_discount_label);
        mDiscount = (TextView) view.findViewById(R.id.payment_discount);
        mAmountLabel = (TextView) view.findViewById(R.id.payment_amount_label);
        mAmount = (TextView) view.findViewById(R.id.payment_amount);
        dynamicTextValue();
        setCustomFont();
        mConfirmRecyclerView.setLayoutManager(mLinearLayoutManager);
        mConfirmRecyclerView.setHasFixedSize(true);
        mCheckoutController = new CheckoutController(getActivity());
        mCheckoutController.loadConfirmPage(getConfirmDetails);
    }

    /**
     * This method is used to Dynamic Text Value Settings
     */
    private void dynamicTextValue() {
        mReviewYourOrderLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.reviewYourOrderText));
        mShippingAddressLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.confirmShippingAddressText));
        mBillingAddressLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.confirmBillingAddressText));
        mPaymentMethodLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.paymentMethodText));
        mShippingMethodLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.shippingMethodText));
        mPaymentSummary.setText(AppConstants.getTextString(getActivity(), AppConstants.paymentSummaryText));
        mSubtotalLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.subTotalText));
        mShippingHandlingLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.paymentShippingHandlingText));
        mTaxLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.taxText));
        mAmountLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.amountPayableText));
    }

    /**
     * This method is used to Set Custom Font
     */
    private void setCustomFont() {
        mReviewYourOrderLabel.setTypeface(mCheckoutActivity.robotoMedium);
        mShippingAddressLabel.setTypeface(mCheckoutActivity.robotoMedium);
        mBillingAddressLabel.setTypeface(mCheckoutActivity.robotoMedium);
        mPaymentMethodLabel.setTypeface(mCheckoutActivity.robotoMedium);
        mShippingMethodLabel.setTypeface(mCheckoutActivity.robotoMedium);
        mPaymentSummary.setTypeface(mCheckoutActivity.robotoMedium);
        mShippingName.setTypeface(mCheckoutActivity.robotoRegular);
        mBillingName.setTypeface(mCheckoutActivity.robotoRegular);
        mShippingAddress.setTypeface(mCheckoutActivity.robotoLight);
        mBillingAddress.setTypeface(mCheckoutActivity.robotoLight);
        mPaymentMethodName.setTypeface(mCheckoutActivity.robotoLight);
        mShippingMethodName.setTypeface(mCheckoutActivity.robotoLight);
        mSubtotalLabel.setTypeface(mCheckoutActivity.robotoRegular);
        mShippingHandlingLabel.setTypeface(mCheckoutActivity.robotoRegular);
        mTaxLabel.setTypeface(mCheckoutActivity.robotoRegular);
        mDiscountLabel.setTypeface(mCheckoutActivity.robotoRegular);
        mAmountLabel.setTypeface(mCheckoutActivity.robotoMedium);
        mAmount.setTypeface(mCheckoutActivity.robotoMedium);
        mSubtotal.setTypeface(mCheckoutActivity.robotoLight);
        mShippingHandling.setTypeface(mCheckoutActivity.robotoLight);
        mTax.setTypeface(mCheckoutActivity.robotoLight);
        mDiscount.setTypeface(mCheckoutActivity.robotoLight);
    }

    /**
     * The Get confirm details.
     */
    VolleyCallback getConfirmDetails = new VolleyCallback() {
        /**
         * This method is used to Volley Success Response
         */
        @Override
        public void Success(String response) {

            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(response);
                mConfirmMainModel = (MyApplication.getGsonInstance().fromJson(response, ConfirmMainModel.class));
                if (mConfirmMainModel.getStatus().equalsIgnoreCase("true")) {
                    mMainLayout.setVisibility(View.VISIBLE);
                    mConfirmList = mConfirmMainModel.getConfirmList();
                    mConfirmProducts = mConfirmList.getProduct();
                    SharedPreference mPref = SharedPreference.getInstance();
                    mPref.save("grand_total", mConfirmList.getTotal());

                    mConfirmOrderListAdapter = new ConfirmOrderListAdapter(getActivity(), mConfirmProducts);
                    mConfirmRecyclerView.setAdapter(mConfirmOrderListAdapter);
                    gettingAdditionalValues();
                } else {
                    hideProgDialog();
                    mCheckoutActivity.snackBar(jsonObject.getString("errormsg"));
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        /**
         * This method is used to Volley Failure Response
         */
        @Override
        public void Failure(String errorResponse) {
            hideProgDialog();
            mCheckoutActivity.snackBar(errorResponse);
        }
    };

    /**
     * This method is used to get Remaining Values
     */
    private void gettingAdditionalValues() {
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            mMagentoDefaultAddressShipping = mCheckoutActivity.magentoDefaultAddressShipping;
            if (mMagentoDefaultAddressShipping != null) {
                mName = mMagentoDefaultAddressShipping.getFirstname() + " " + mMagentoDefaultAddressShipping.getLastname();
                mAddress = mMagentoDefaultAddressShipping.getStreet() + ", " + mMagentoDefaultAddressShipping.getCity() + "\n" +
                        mMagentoDefaultAddressShipping.getCountryId() + "," + mMagentoDefaultAddressShipping.getPostcode() + "\n" +
                        mMagentoDefaultAddressShipping.getTelephone();
            }else {
                getStaticAddressValue();
            }
        }else {
            mAddressModel = mCheckoutActivity.addressmodel;
            if (mAddressModel != null) {
                mName = mAddressModel.getFirstname() + " " + mAddressModel.getLastname();
                mAddress = mAddressModel.getStreet() + ", " + mAddressModel.getCity() + "\n" +
                        mAddressModel.getCountry() + "," + mAddressModel.getPostcode() + "\n" +
                        mAddressModel.getTelephone();
            }else {
                getStaticAddressValue();
            }
        }
        String currenySymbol = SharedPreference.getInstance().getValue("currency_symbol");
        mPaymentMethodName.setText(mCheckoutActivity.paymentMethodName);
        mShippingMethodName.setText(mCheckoutActivity.shippingMethod);
        mSubtotal.setText(currenySymbol + " " + mConfirmList.getSubTotal());
        mShippingHandling.setText(currenySymbol + " " + mConfirmList.getShippingAmount());
        mTax.setText(currenySymbol + " " + mConfirmList.getTaxAmount());
        mDiscount.setText(currenySymbol + " " + mConfirmList.getDiscountAmount());
        mAmount.setText(currenySymbol + " " + mConfirmList.getTotal());

        if (SharedPreference.getInstance().getValue("shipping_name").equals("0")) {
            mShippingName.setText(mName);
            mShippingAddress.setText(mAddress);
        } else {
            mShippingName.setText(SharedPreference.getInstance().getValue("shipping_name"));
            mShippingAddress.setText(SharedPreference.getInstance().getValue("shipping_address"));
        }
        if (SharedPreference.getInstance().getValue("billing_name").equals("0")) {
            mBillingName.setText(mName);
            mBillingAddress.setText(mAddress);
        } else {
            mBillingName.setText(SharedPreference.getInstance().getValue("billing_name"));
            mBillingAddress.setText(SharedPreference.getInstance().getValue("billing_address"));
        }
        hideProgDialog();
    }

    /**
     * Get static address values from SharedPreference
     */
    private void getStaticAddressValue() {
        Gson gson = new Gson();
        String shipObj = SharedPreference.getInstance().getValue("shippingAddressValues");
        AddressValues shippingValues = gson.fromJson(shipObj, AddressValues.class);
        mName = shippingValues.getFname() + " " + shippingValues.getLname();
        mAddress = shippingValues.getStreet() + " " + shippingValues.getCity() + "\n"
                + shippingValues.getCountry() + "," + shippingValues.getZipcode() + "\n"
                + shippingValues.getPhone();
    }
}
