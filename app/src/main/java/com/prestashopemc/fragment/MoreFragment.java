package com.prestashopemc.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.android.gms.vision.barcode.Barcode;
import com.prestashopemc.R;
import com.prestashopemc.adapter.CmsListAdapter;
import com.prestashopemc.database.CmsService;
import com.prestashopemc.model.CmsPage;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.view.BarcodeCaptureActivity;
import com.prestashopemc.view.MainActivity;
import com.prestashopemc.view.MyAddressActivity;
import com.prestashopemc.view.MyOrdersActivity;
import com.prestashopemc.view.MyReviewsActivity;
import com.prestashopemc.view.MyWishlistActivity;
import com.prestashopemc.view.SettingActivity;
import com.prestashopemc.view.StoreLocatorActivity;

import java.util.List;

/**
 * <h1>More Fragment!</h1>
 * The More Fragment contains more cms pages.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 10 /3/2016
 */
public class MoreFragment extends Fragment implements View.OnClickListener {

    private RecyclerView mCmsListview;
    private CmsListAdapter mCmsAdapter;
    private List<CmsPage> mCmsPageList;
    private TextView mAccountLabel, mAccountText, mAddressLabel, mOrderLabel, mSettingLabel, mQrScannerLabel, mLocatorLabel, mWishlistLabel, mReviewsLabel;
    private RelativeLayout mSettingLayout, mAccountLayout, mAddressLayout, mOrderLayout, mWishlistLayout, mReviewLayout, mLocatorLayout, mQrScannerLayout;
    private static final int RC_BARCODE_CAPTURE = 9001;
    private CmsService mCmsService;
    /**
     * The constant sBarCode.
     */
    public static Barcode sBarCode;

    /**
     * This method will call when creating fragment view.
     *
     * @param inflater           This is the first parameter to onCreateView method.
     * @param container          This is the second parameter to onCreateView method.
     * @param savedInstanceState This is the third parameter to onCreateView method.
     * @return View returs layout view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.more_fragment, container, false);
    }

    /**
     * This method will call when fragment view is created..
     *
     * @param view               This is the first parameter to onViewCreated method.
     * @param savedInstanceState This is the first parameter to onViewCreated method.
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializedLayout(view);
    }

    /**
     * This method is used to Initalized Layout
     *
     * @param view This is the parameter to initalizeLayout method.
     */
    void initializedLayout(View view) {
        mCmsPageList = ((MainActivity) getActivity()).cmsList;
        mCmsService = new CmsService(getActivity());
        mCmsListview = (RecyclerView) view.findViewById(R.id.cms_list_view);
        mAccountText = (TextView) view.findViewById(R.id.account_text);
        mAccountLabel = (TextView) view.findViewById(R.id.account_label);
        mAddressLabel = (TextView) view.findViewById(R.id.address_text);
        mOrderLabel = (TextView) view.findViewById(R.id.order_text);
        mSettingLabel = (TextView) view.findViewById(R.id.settings_text);
        mLocatorLabel = (TextView) view.findViewById(R.id.locator_text);
        mQrScannerLabel = (TextView) view.findViewById(R.id.qrscanner_text);
        mWishlistLabel = (TextView) view.findViewById(R.id.wishlist_text);
        mReviewsLabel = (TextView) view.findViewById(R.id.review_text);
        mSettingLayout = (RelativeLayout) view.findViewById(R.id.settings_layout);
        mAccountLayout = (RelativeLayout) view.findViewById(R.id.my_account_layout);
        mAddressLayout = (RelativeLayout) view.findViewById(R.id.address_layout);
        mOrderLayout = (RelativeLayout) view.findViewById(R.id.order_layout);
        mWishlistLayout = (RelativeLayout) view.findViewById(R.id.wishlist_layout);
        mReviewLayout = (RelativeLayout) view.findViewById(R.id.review_layout);
        mLocatorLayout = (RelativeLayout) view.findViewById(R.id.locator_layout);
        mQrScannerLayout = (RelativeLayout) view.findViewById(R.id.qrscanner_layout);

        dynamicTextValue();
        setLayoutVisibility();
        setOnClickEvents();
        setCustomFont();
    }

    /**
     * This method is used to Set click listener
     */
    private void setOnClickEvents() {
        mQrScannerLayout.setOnClickListener(this);
        mLocatorLayout.setOnClickListener(this);
        mOrderLayout.setOnClickListener(this);
        mWishlistLayout.setOnClickListener(this);
        mReviewLayout.setOnClickListener(this);
        mSettingLayout.setOnClickListener(this);
        mAddressLayout.setOnClickListener(this);
    }

    /**
     * This method is used to Set Layout visibility
     */
    private void setLayoutVisibility() {
        if (!SharedPreference.getInstance().getValue("customerid").equals("0")) {
            mAccountLayout.setVisibility(View.GONE);
            mAddressLayout.setVisibility(View.VISIBLE);
            mOrderLayout.setVisibility(View.VISIBLE);
            mWishlistLayout.setVisibility(View.VISIBLE);
            mReviewLayout.setVisibility(View.VISIBLE);
        } else {
            mAccountLayout.setVisibility(View.GONE);
            mAddressLayout.setVisibility(View.GONE);
            mOrderLayout.setVisibility(View.GONE);
            mWishlistLayout.setVisibility(View.GONE);
            mReviewLayout.setVisibility(View.GONE);
        }

        if (SharedPreference.getInstance().getValue("store_locator_status").equals("true")) {
            mLocatorLayout.setVisibility(View.VISIBLE);
        } else {
            mLocatorLayout.setVisibility(View.GONE);
        }
        if (SharedPreference.getInstance().getValue("qrcode_scanner_status").equals("true")) {
            mQrScannerLayout.setVisibility(View.VISIBLE);
        } else {
            mQrScannerLayout.setVisibility(View.GONE);
        }

        try {
            if (mCmsPageList != null) {
                mCmsAdapter = new CmsListAdapter(getActivity(), mCmsPageList);
                mCmsListview.setLayoutManager(new LinearLayoutManager(getActivity()));
                mCmsListview.setAdapter(mCmsAdapter);
            } else {
                mCmsPageList = mCmsService.retrieveCmsList();
                mCmsAdapter = new CmsListAdapter(getActivity(), mCmsPageList);
                mCmsListview.setLayoutManager(new LinearLayoutManager(getActivity()));
                mCmsListview.setAdapter(mCmsAdapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is used to Dynamic Text Value Settings
     */
    private void dynamicTextValue() {
        mAccountLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.accountText));
        mAccountText.setText(AppConstants.getTextString(getActivity(), AppConstants.myAccountText));
        mAddressLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.myAddressText));
        mOrderLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.myOrdersText));
        mWishlistLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.myWishlistText));
        mReviewsLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.myReviewsText));
        mSettingLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.settingsText));
        mLocatorLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.storeLocateText));
        mQrScannerLabel.setText(AppConstants.getTextString(getActivity(), AppConstants.qrcodeText));
    }

    /**
     * This method is used to Set Custom Font
     */
    private void setCustomFont() {
        mAccountLabel.setTypeface(((MainActivity) getActivity()).proximanovaAltRegular);
        mAccountText.setTypeface(((MainActivity) getActivity()).proximanovaAltRegular);
        mAddressLabel.setTypeface(((MainActivity) getActivity()).proximanovaAltRegular);
        mOrderLabel.setTypeface(((MainActivity) getActivity()).proximanovaAltRegular);
        mWishlistLabel.setTypeface(((MainActivity) getActivity()).proximanovaAltRegular);
        mReviewsLabel.setTypeface(((MainActivity) getActivity()).proximanovaAltRegular);
        mSettingLabel.setTypeface(((MainActivity) getActivity()).proximanovaAltRegular);
        mLocatorLabel.setTypeface(((MainActivity) getActivity()).proximanovaAltRegular);
        mQrScannerLabel.setTypeface(((MainActivity) getActivity()).proximanovaAltRegular);

    }

    /**
     * This method is used to get barcode code scan results
     *
     * @param requestCode This parameter is passed when requested barcode capture
     * @param resultCode  This parameter giving result code of the scanned bar code
     * @param data        This parameter giving data of scanned barcode.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_BARCODE_CAPTURE) {
            if (resultCode == CommonStatusCodes.SUCCESS) {
                if (data != null) {
                    sBarCode = data.getParcelableExtra(BarcodeCaptureActivity.BARCODE_OBJECT);
                    Toast.makeText(getContext(), getContext().getResources().getString(R.string.barcode_success), Toast.LENGTH_LONG).show();
                    SharedPreference mPref = SharedPreference.getInstance();
                    mPref.save("barcode_data", sBarCode.displayValue);
                    //Log.d("BARCODE TAG", "Barcode read: " + sBarCode.displayValue);
                } else {
                    Toast.makeText(getContext(), getContext().getResources().getString(R.string.barcode_failure), Toast.LENGTH_LONG).show();
                }
            } else {
                Toast.makeText(getContext(), getContext().getResources().getString(R.string.barcode_error), Toast.LENGTH_LONG).show();

            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    /**
     * This method is calling when click event occured
     *
     * @param v This view parameter is used to passing which view clicked.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * Locator Listener
             */
            case R.id.locator_layout:
                ((MainActivity) getActivity()).closeDrawer();
                Intent storeLocator = new Intent(getActivity(), StoreLocatorActivity.class);
                startActivity(storeLocator);
                break;

            /**
             * Qrcode Listener
             */
            case R.id.qrscanner_layout:
                ((MainActivity) getActivity()).closeDrawer();
                Intent intent = new Intent(getActivity(), BarcodeCaptureActivity.class);
                startActivityForResult(intent, RC_BARCODE_CAPTURE);
                break;

            /**
             * My Order Click Listener
             */
            case R.id.order_layout:
                ((MainActivity) getActivity()).closeDrawer();
                Intent myOrderIntent = new Intent(getActivity(), MyOrdersActivity.class);
                startActivity(myOrderIntent);
                break;

            /**
             * My Wishlist Click Listener
             */
            case R.id.wishlist_layout:
                ((MainActivity) getActivity()).closeDrawer();
                Intent wishlistIntent = new Intent(getActivity(), MyWishlistActivity.class);
                startActivity(wishlistIntent);
                break;

            /**
             * My Reviews Click Listener
             */
            case R.id.review_layout:
                ((MainActivity) getActivity()).closeDrawer();
                Intent reviewsIntent = new Intent(getActivity(), MyReviewsActivity.class);
                startActivity(reviewsIntent);
                break;

            /**
             * Setting Click Listener
             */
            case R.id.settings_layout:
                ((MainActivity) getActivity()).closeDrawer();
                Intent settingIntent = new Intent(getActivity(), SettingActivity.class);
                startActivity(settingIntent);
                break;

            /**
             * My Address Click Listener
             */
            case R.id.address_layout:
                ((MainActivity) getActivity()).closeDrawer();
                Intent addressIntent = new Intent(getActivity(), MyAddressActivity.class);
                startActivity(addressIntent);
                break;
        }
    }
}
