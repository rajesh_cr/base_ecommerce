package com.prestashopemc.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.prestashopemc.R;
import com.prestashopemc.adapter.CategoryExpandableAdapter;
import com.prestashopemc.database.CategoryService;
import com.prestashopemc.database.DatabaseHelper;
import com.prestashopemc.model.Category;
import com.prestashopemc.model.ExpandCategory;
import com.prestashopemc.view.MainActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Category Fragment!</h1>
 * The Category Fragment contains Category informations.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 10 /3/16
 */
public class CategoryFragment extends Fragment {

    private RecyclerView mCategoryListview;
    private CategoryExpandableAdapter mCategoryAdapter;
    private List<Category> mCategoryList;
    /**
     * The Category service.
     */
    public CategoryService categoryService;
    /**
     * The Is db exists.
     */
    public boolean isDbExists;
    /**
     * The Database helper.
     */
    public DatabaseHelper databaseHelper;
    private View mView;

    /**
     * This method is used to create view for fragment.
     * @param inflater This is the first parameter to onCreateView method.
     * @param container This is the second parameter to onCreateView method.
     * @param savedInstanceState This is the third parameter to onCreateView method.
     * @return View returs layout view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (mView == null) {
            mView = inflater.inflate(R.layout.category_fragment, container, false);
            initializedLayout(mView);
        }
        return mView;

    }

    /**
     * This method is used to Initalized Layout.
     * @param mView This is the parameter to initalizedLayout method.
     */
    private void initializedLayout(View mView) {
        mCategoryList = ((MainActivity) getActivity()).categoryList;
        categoryService = new CategoryService(getActivity());
        databaseHelper = new DatabaseHelper(getActivity());
        mCategoryListview = (RecyclerView) mView.findViewById(R.id.category_recycler_view);

        isDbExists = databaseHelper.doesDatabaseExist(getActivity(), DatabaseHelper.DATABASE_NAME);

        if (isDbExists) {
            mCategoryAdapter = new CategoryExpandableAdapter(getActivity(), getDbCategory());
        } else {
            mCategoryAdapter = new CategoryExpandableAdapter(getActivity(), getCategory());
        }

        mCategoryListview.setAdapter(mCategoryAdapter);
        mCategoryListview.setLayoutManager(new LinearLayoutManager(getActivity()));
        mCategoryListview.setHasFixedSize(true);
    }

    /**
     * This method is used to created view for fragment.
     * @param view This is the first parameter to onViewCreated method.
     * @param savedInstanceState This is the first parameter to onViewCreated method.
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }


    /**
     * This method is used to Parent List Item
     * @return List<ParentListItem> returns parent category list items.
     */
    private List<ParentListItem> getCategory() {

        List<ParentListItem> parentListItems = new ArrayList<>();
        for (Category crime : mCategoryList) {
            List<ExpandCategory> childItemList = new ArrayList<>();
            childItemList.addAll(crime.getCategories());
            parentListItems.add(crime);
        }
        return parentListItems;
    }

    /**
     * This method is used to Parent List Item
     * @return List<ParentListItem> returns parent category list items from DB.
     */
    private List<ParentListItem> getDbCategory() {
        List<ParentListItem> parentListItems = new ArrayList<>();
        try {
            if (mCategoryList != null) {
                for (Category crime : mCategoryList) {
                    List<ExpandCategory> childItemList = new ArrayList<>();
                    crime.setCategories(categoryService.retrieveExpandCategory(crime.getIdCategory()));
                    childItemList.addAll(categoryService.retrieveExpandCategory(crime.getIdCategory()));
                    parentListItems.add(crime);
                }
            } else {
                mCategoryList = categoryService.retrieveCategoryList();
                for (Category crime : mCategoryList) {
                    List<ExpandCategory> childItemList = new ArrayList<>();
                    crime.setCategories(categoryService.retrieveExpandCategory(crime.getIdCategory()));
                    childItemList.addAll(categoryService.retrieveExpandCategory(crime.getIdCategory()));
                    parentListItems.add(crime);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return parentListItems;
    }

    /**
     * This method is used to activity created .
     * @param savedInstanceState This is the parameter to onActivityCreated method.
     */
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    /**
     *  This method is used to destroy view
     */
    @Override
    public void onDestroyView() {
        if (mView.getParent() != null) {
            ((ViewGroup) mView.getParent()).removeView(mView);
        }
        super.onDestroyView();
    }
}