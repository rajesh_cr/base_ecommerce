package com.prestashopemc.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prestashopemc.R;
import com.prestashopemc.customView.CustomTextView;
import com.prestashopemc.model.MagentoProductinfo;
import com.prestashopemc.model.Productinfo;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.view.MagentoProductDetailActivity;
import com.prestashopemc.view.ProductDetailActivity;

/**
 * <h1>Description Fragment!</h1>
 * The Description Fragment contains create account details..
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 29 /3/16
 */
public class DescriptionFragment extends Fragment {

    private CustomTextView mProductName, mProductDescription;
    private ProductDetailActivity mPrdDetailActivity;
    private Productinfo mProductInfo;
    private MagentoProductDetailActivity mMagentoPrdDetailActivity;
    private MagentoProductinfo mMagentoProductInfo;
    /**
     * This method is used to create view for fragment.
     * @param inflater This is the first parameter to onCreateView method.
     * @param container This is the second parameter to onCreateView method.
     * @param savedInstanceState This is the third parameter to onCreateView method.
     * @return View returs layout view.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.description_fragment, container, false);
    }

    /**
     * This method is used to created view for fragment.
     * @param view This is the first parameter to onViewCreated method.
     * @param savedInstanceState This is the first parameter to onViewCreated method.
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)){
            initializeMagentoLayout(view);
        }else {
            initializeLayout(view);
        }

    }

    /**
     * This method is used to Initalized Layout
     * @param view This is the parameter to initalizedLayout method.
     * */
    private void initializeLayout(View view) {
        mPrdDetailActivity = (ProductDetailActivity) getActivity();
        mProductName = (CustomTextView) view.findViewById(R.id.product_name);
        mProductDescription = (CustomTextView) view.findViewById(R.id.product_description);
        mProductInfo = mPrdDetailActivity.productinfo;
        if (mProductInfo != null) {
            mProductName.setText(mProductInfo.getName());
            mProductDescription.setText(mProductInfo.getDescription());
            mProductName.setTypeface(mPrdDetailActivity.robotoRegular);
            mProductDescription.setTypeface(mPrdDetailActivity.robotoLight);
        }
    }

    /**
     * This method is used to Initalized Layout
     * @param view This is the parameter to initalizedLayout method.
     * */
    private void initializeMagentoLayout(View view) {
        mMagentoPrdDetailActivity = (MagentoProductDetailActivity) getActivity();
        mProductName = (CustomTextView) view.findViewById(R.id.product_name);
        mProductDescription = (CustomTextView) view.findViewById(R.id.product_description);
        mMagentoProductInfo = mMagentoPrdDetailActivity.productinfo;
        if (mMagentoProductInfo != null) {
            mProductName.setText(mMagentoProductInfo.getName());
            mProductDescription.setText(mMagentoProductInfo.getDescription());
            mProductName.setTypeface(mMagentoPrdDetailActivity.robotoRegular);
            mProductDescription.setTypeface(mMagentoPrdDetailActivity.robotoLight);
        }
    }
}
