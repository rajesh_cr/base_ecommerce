package com.prestashopemc.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.prestashopemc.R;
import com.prestashopemc.adapter.SubzeroHomeAdapter;
import com.prestashopemc.controller.HomePageController;
import com.prestashopemc.model.SubzeroHomeModel;
import com.prestashopemc.model.SubzeroHomeResult;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.VolleyCallback;
import com.prestashopemc.view.MainActivity;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rajesh on 22/09/16.
 */
public class SubzeroSubFragment extends Fragment {

    private RecyclerView mHomeProductList;
    private SubzeroHomeAdapter mSubzeroAdapter;
    private GridLayoutManager mGridLayoutManger;
    private SubzeroHomeModel mSubzeroHomeModel;
    private List<SubzeroHomeResult> mSubzeroHomeResult = new ArrayList<SubzeroHomeResult>();
    private int mProductCount = 0;
    private HomePageController homePageController;
    private int previousTotal = 0;
    private int visibleThreshold = 5;
    private int mFirstVisibleItem, mVisibleItemCount, mTotalItemCount;
    private boolean loading = true;
    private int mCurrentPage = 1;
    private int totalPageNumber = 0;
    private ProgressBar mProgressBar, mBottomProgressBar;
    private View mView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (mView == null) {
            mView = inflater.inflate(R.layout.subzero_sub_fragment, container, false);
            initializeLayout(mView);
            homePageController = new HomePageController(getActivity());
            homePageController.subzeroHome(mSubzeroHome, AppConstants.sSubzeroHomePopular, mCurrentPage);
        }
        return mView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    private void initializeLayout(View view) {
        mHomeProductList = (RecyclerView)view.findViewById(R.id.subzero_homeproducts_list);
        mGridLayoutManger = new GridLayoutManager(getActivity(), 2);
        mHomeProductList.setLayoutManager(mGridLayoutManger);
        mHomeProductList.setHasFixedSize(true);
        mHomeProductList.addOnScrollListener(mScrollListener);
        mProgressBar = (ProgressBar)view. findViewById(R.id.progress_bar);
        mBottomProgressBar = (ProgressBar)view. findViewById(R.id.bottom_progress_bar);
        mProgressBar.setVisibility(View.VISIBLE);
    }

    /**
     * The M subzero home.
     */
    VolleyCallback mSubzeroHome = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                mSubzeroHomeModel = (MyApplication.getGsonInstance().fromJson(response, SubzeroHomeModel.class));
                if (jsonObject.getString("status").equals("true")){
                    mSubzeroHomeResult.addAll(mSubzeroHomeModel.getResult());
                    mProductCount = mSubzeroHomeModel.getProductCount();
                    mProgressBar.setVisibility(View.GONE);
                    mBottomProgressBar.setVisibility(View.GONE);
                    if (mSubzeroHomeResult != null){
                        if (mCurrentPage == 1) {
                            totalPageNo();
                            mSubzeroAdapter = new SubzeroHomeAdapter(getActivity(), mSubzeroHomeResult);
                            mHomeProductList.setAdapter(mSubzeroAdapter);
                        }else {
                            mSubzeroAdapter.notifyDataSetChanged();
                        }

                    }
                }else {
                    ((MainActivity) getActivity()).homePageFailure(jsonObject.getString("errormsg"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            if (errorResponse != null) {
                ((MainActivity) getActivity()).homePageFailure(errorResponse);
            }
        }
    };

    private RecyclerView.OnScrollListener mScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            mVisibleItemCount = mHomeProductList.getChildCount();
            mTotalItemCount = mGridLayoutManger.getItemCount();
            mFirstVisibleItem = mGridLayoutManger.findFirstVisibleItemPosition();

            if (loading) {
                if (mTotalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = mTotalItemCount;
                }
            }
            if (!loading && (mTotalItemCount - mVisibleItemCount)
                    <= (mFirstVisibleItem + visibleThreshold)) {
                mBottomProgressBar.setVisibility(View.VISIBLE);
                mCurrentPage++;

                if (totalPageNumber >= mCurrentPage) {
                    //Log.e("Popular Products Total page number === ", totalPageNumber+" Current page === "+mCurrentPage);
                    homePageController.subzeroHome(mSubzeroHome, AppConstants.sSubzeroHomePopular, mCurrentPage);
                }
                else
                {
                    mBottomProgressBar.setVisibility(View.GONE);
                }

                loading = true;
            }
        }
    };

    private void totalPageNo()
    {
        int extraPage = 0;
        if((mProductCount % AppConstants.productsPerPage) == 0)
        {
            extraPage = 0;
        }else {
            extraPage = 1;
        }
        totalPageNumber = (mProductCount / AppConstants.productsPerPage)+extraPage;
    }
    @Override
    public void onDestroyView() {
        if (mView.getParent() != null) {
            ((ViewGroup)mView.getParent()).removeView(mView);
        }
        super.onDestroyView();
    }
}
