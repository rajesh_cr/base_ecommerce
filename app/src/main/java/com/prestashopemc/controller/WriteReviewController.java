package com.prestashopemc.controller;

import android.content.Context;

import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.VolleyCallback;
import com.prestashopemc.view.MagentoProductDetailActivity;
import com.prestashopemc.view.ProductDetailActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * <h1>Write Review Controller!</h1>
 * The Write Review Controller to form the request and response for Write Review Api call.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class WriteReviewController {

    private Context mContext;

    /**
     * Instantiates a new Write review controller.
     *
     * @param con the con
     */
    public WriteReviewController(Context con) {
        this.mContext = con;
    }

    /**
     * Gets review option.
     *
     * @param getRatingResponse the get rating response
     */
    public void gettingReviewOption(VolleyCallback getRatingResponse) {
        Map<String, String> params = new HashMap<String, String>();

        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoRatingOption;
            MyApplication.magentoNetworkCall(url, params, getRatingResponse);
        } else {
            String url = AppConstants.sCategoriesController;
            params.put("action", AppConstants.sGetRatingAction);
            MyApplication.NetworkCall(url, params, getRatingResponse);
        }
    }

    /**
     * Submit review.
     *
     * @param productId   the product id
     * @param ratingValue the rating value
     */
    public void submitReview(String productId, String ratingValue) {
        String mProducid = productId;
        String mReviewValue = ratingValue;
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_product", mProducid);
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        params.put("sessionid", SharedPreference.getInstance().getValue("sessionid"));
        params.put("rating", mReviewValue);
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoReviewCreate;
            MyApplication.magentoNetworkCall(url, params, submitRatingResponse);
        } else {
            String url = AppConstants.sCartController;
            params.put("store_id", "1");
            params.put("action", AppConstants.sSubmitRating);
            MyApplication.NetworkCall(url, params, submitRatingResponse);
        }
    }

    /**
     * The Submit rating response.
     */
    VolleyCallback submitRatingResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;

                jsonObject = new JSONObject(response);

                if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                    String reviewResponse;
                    if (AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
                        reviewResponse = jsonObject.getString("message");
                        ((MagentoProductDetailActivity) mContext).submitSuccessResponse(reviewResponse);
                    } else {

                        reviewResponse = jsonObject.getString("result");
                        ((ProductDetailActivity) mContext).submitSuccessResponse(reviewResponse);
                    }
                } else {
                    if (AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
                        ((MagentoProductDetailActivity) mContext).getratingFailure(jsonObject.getString("errormsg"));

                    } else {
                        ((ProductDetailActivity) mContext).getratingFailure(jsonObject.getString("errormsg"));
                    }

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {

            if (AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
                ((MagentoProductDetailActivity) mContext).getratingFailure(errorResponse);

            } else {
                ((ProductDetailActivity) mContext).getratingFailure(errorResponse);
            }
        }
    };
}
