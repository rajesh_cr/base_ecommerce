package com.prestashopemc.controller;

import android.content.Context;
import android.util.Log;

import com.prestashopemc.model.LicenseMain;
import com.prestashopemc.model.LocatorMain;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.VolleyCallback;
import com.prestashopemc.view.SelectActivity;
import com.prestashopemc.view.SplashActivity;
import com.prestashopemc.view.StoreLocatorActivity;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * <h1>License Controller!</h1>
 * The License Controller to form the request and response for License Api call.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class LicenseController {
    private Context mContext;
    private LicenseMain mLicenseMain;
    private LocatorMain mLocatorMain;

    /**
     * Instantiates a new License controller.
     *
     * @param context the context
     */
    public LicenseController(Context context) {
        this.mContext = context;
    }

    /**
     * License check.
     */
/*License API Call*/
    public void licenseCheck() {
        Map<String, String> params = new HashMap<String, String>();

        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoLicense;
            //params.put("store_id", "1");
            MyApplication.magentoNetworkCall(url, params, licenseResponse);

//            String url = AppConstants.sControllLicense;
//            params.put("action", AppConstants.sLicense);
//            MyApplication.NetworkCall(url, params, licenseResponse);

        } else {
            String url = AppConstants.sControllLicense;
            params.put("action", AppConstants.sLicense);
            MyApplication.NetworkCall(url, params, licenseResponse);
        }
    }

    /**
     * The License response.
     */
    VolleyCallback licenseResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                mLicenseMain = (MyApplication.getGsonInstance().fromJson(response, LicenseMain.class));

                if (mLicenseMain.getStatus().equalsIgnoreCase("true")) {
                    sharedPreferences();
                    if (mContext instanceof SplashActivity)
                        ((SplashActivity) mContext).licenseSuccess(true, mLicenseMain);
                    if (mContext instanceof SelectActivity)
                        ((SelectActivity) mContext).licenseSuccess(true, mLicenseMain);
                } else {
                    if (mContext instanceof SplashActivity)
                        ((SplashActivity) mContext).licenseError(false);
                    if (mContext instanceof SelectActivity)
                        ((SelectActivity) mContext).licenseError(false);
                }

            } catch (Exception e) {
                e.printStackTrace();
                Log.e("Catch message ==== ", e.getMessage());
                if (mContext instanceof SplashActivity)
                    ((SplashActivity) mContext).licenseError(false);
                if (mContext instanceof SelectActivity)
                    ((SelectActivity) mContext).licenseError(false);
            }
        }

        @Override
        public void Failure(String errorResponse) {
            if (mContext instanceof SplashActivity)
                ((SplashActivity) mContext).licenseError(false);
            if (mContext instanceof SelectActivity)
                ((SelectActivity) mContext).licenseError(false);
        }
    };

    /**
     * Shared preferences.
     */
/*Save values to SharedPreferences*/
    public void sharedPreferences() {
        SharedPreference mPref = SharedPreference.getInstance();
        mPref.save("currency_symbol", mLicenseMain.getResult().getCurrencySymbol());
        mPref.save("security_key", mLicenseMain.getResult().getEgrsecuritykey());
        mPref.save("theme_color", mLicenseMain.getResult().getThemeColor());
        mPref.save("id_language", mLicenseMain.getResult().getDefaultLang());
        mPref.save("id_currency", mLicenseMain.getResult().getDefaultCurrency());
        mPref.save("id_language1", mLicenseMain.getResult().getDefaultLang());
        mPref.save("id_currency1", mLicenseMain.getResult().getDefaultCurrency());
    }

    /**
     * Location list.
     */
    /*Store Locator API*/
    public void locationList() {
        String url = AppConstants.sCategoriesController;
        Map<String, String> params = new HashMap<String, String>();
        params.put("action", AppConstants.sLocator);
        MyApplication.NetworkCall(url, params, locatorResponse);
    }

    /**
     * The Locator response.
     */
    VolleyCallback locatorResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                mLocatorMain = (MyApplication.getGsonInstance().fromJson(response, LocatorMain.class));

                if (mLocatorMain.getStatus().equalsIgnoreCase("true")) {
                    ((StoreLocatorActivity) mContext).success(mLocatorMain);
                } else {
                    ((StoreLocatorActivity) mContext).error(jsonObject.getString("errormsg"));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((StoreLocatorActivity) mContext).error(errorResponse);
        }
    };

    public void licenseCheck(String sMagentoBaseUrl, String sBaseUrl) {
        Map<String, String> params = new HashMap<String, String>();

        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            AppConstants.setsMagentoBaseUrl(AppConstants.sMagentoBaseUrl);
            String url = AppConstants.sMagentoLicense;
            //params.put("store_id", "1");
            MyApplication.magentoNetworkCall(url, params, licenseResponse);

//            String url = AppConstants.sControllLicense;
//            params.put("action", AppConstants.sLicense);
//            MyApplication.NetworkCall(url, params, licenseResponse);

        } else {
            AppConstants.setBaseUrl(AppConstants.sBaseUrl);
            String url = AppConstants.sControllLicense;
            params.put("action", AppConstants.sLicense);
            MyApplication.NetworkCall(url, params, licenseResponse);
        }
    }
}
