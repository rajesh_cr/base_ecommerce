package com.prestashopemc.controller;

import android.content.Context;
import android.util.Log;

import com.android.volley.Request;
import com.prestashopemc.R;
import com.prestashopemc.model.AddressMainModel;
import com.prestashopemc.model.AddressValues;
import com.prestashopemc.model.ChangeAddressMainModel;
import com.prestashopemc.model.CountryMainModel;
import com.prestashopemc.model.MagentoDefaultAddressModel;
import com.prestashopemc.model.MagentoOrderMain;
import com.prestashopemc.model.OrderConfirmModel;
import com.prestashopemc.model.PaypalResponse;
import com.prestashopemc.model.StateMainModel;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.VolleyCallback;
import com.prestashopemc.view.AddNewAddress;
import com.prestashopemc.view.BaseActivity;
import com.prestashopemc.view.CartActivity;
import com.prestashopemc.view.ChangeAddress;
import com.prestashopemc.view.CheckoutActivity;
import com.prestashopemc.view.OrderConfirmActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * <h1>Checkout Controller!</h1>
 * The Checkout Controller to form the request and response for Checkout Products Api call.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class CheckoutController {

    private Context mContext;
    private AddressMainModel mAddressMainModel;
    private ChangeAddressMainModel mChangeAddressMainModel;
    private CountryMainModel mCountryMainModel;
    private StateMainModel mStateMainModel;
    private static String mBase64ClientID = null;
    private MagentoDefaultAddressModel mMagentoDefaultAddressModel;
    public static String mAccessToken = "";
    private static String mPaymentKey;
    private OrderConfirmModel orderConfirmModel;
    private MagentoOrderMain magentoOrderMain;

    /**
     * Instantiates a new Checkout controller.
     *
     * @param context the context
     */
    public CheckoutController(Context context) {
        this.mContext = context;
    }

    /**
     * Initial address.
     */
/*Getting Registered Address From API*/
    public void initialAddress() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoDefaultAddress;
            params.put("sessionid", SharedPreference.getInstance().getValue("sessionid"));
            MyApplication.magentoNetworkCall(url, params, getInitialAddress);

        }else {
            String url = AppConstants.sCheckout;
            params.put("action", AppConstants.sCheckoutAction);
            MyApplication.NetworkCall(url, params, getInitialAddress);

        }
    }

    /**
     * The Get initial address.
     */
    VolleyCallback getInitialAddress = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                    mMagentoDefaultAddressModel = (MyApplication.getGsonInstance().fromJson(response, MagentoDefaultAddressModel.class));
                    if (mMagentoDefaultAddressModel.getStatus()) {
                        if (mContext instanceof CartActivity) {
                            ((CartActivity) mContext).addressResponse(response);
                        } else if (mContext instanceof AddNewAddress) {
                            ((AddNewAddress) mContext).addressResponse(response);
                        }
                    } else {
                        if (mContext instanceof CartActivity) {
                            ((CartActivity) mContext).addressFailure(jsonObject.getString("errormsg"));
                        } else if (mContext instanceof AddNewAddress) {
                            ((AddNewAddress) mContext).addressFailure(jsonObject.getString("errormsg"));
                        }
                    }
                }else {
                    mAddressMainModel = (MyApplication.getGsonInstance().fromJson(response, AddressMainModel.class));
                    if (mAddressMainModel.getStatus().equalsIgnoreCase("true")) {
                        if (mContext instanceof CartActivity) {
                            ((CartActivity) mContext).addressResponse(response);
                        } else if (mContext instanceof AddNewAddress) {
                            ((AddNewAddress) mContext).addressResponse(response);
                        }
                    } else {
                        if (mContext instanceof CartActivity) {
                            ((CartActivity) mContext).addressFailure(jsonObject.getString("errormsg"));
                        } else if (mContext instanceof AddNewAddress) {
                            ((AddNewAddress) mContext).addressFailure(jsonObject.getString("errormsg"));
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            if (mContext instanceof CartActivity) {
                //((CartActivity) context).failure(context.getString(R.string.internal_server_error));
                ((CartActivity) mContext).failure(errorResponse);
            } else if (mContext instanceof AddNewAddress) {
                //((AddNewAddress) context).failure(context.getString(R.string.internal_server_error));
                ((AddNewAddress) mContext).failure(errorResponse);
            }
        }
    };

    /**
     * Getting Registered Address List From API
     */
    public void addressList() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoMultipleBillingAddress;
            params.put("sessionid", SharedPreference.getInstance().getValue("sessionid"));
            MyApplication.magentoNetworkCall(url, params, getAddressList);
        }else {
            String url = AppConstants.sCheckout;
            params.put("action", AppConstants.sCheckoutChangeAction);
            params.put("type", "2");
            MyApplication.NetworkCall(url, params, getAddressList);
        }
    }

    /**
     * The Get address list.
     */
    VolleyCallback getAddressList = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                mChangeAddressMainModel = (MyApplication.getGsonInstance().fromJson(response, ChangeAddressMainModel.class));
                if (mChangeAddressMainModel.getStatus().equalsIgnoreCase("true")) {
                    ((ChangeAddress) mContext).updateAddressList(mChangeAddressMainModel);
                } else {
                    ((ChangeAddress) mContext).failure(jsonObject.getString("errormsg"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            //((ChangeAddress) context).failure(context.getString(R.string.internal_server_error));
            ((ChangeAddress) mContext).failure(errorResponse);
        }
    };

    /**
     * Submit new address.
     *
     * @param av        the av
     * @param editAddr  the edit addr
     * @param idAddress the id address
     * @param ctryId    the ctry id
     */
/*New Address Submit API*/
   /* public void submitNewAddress(JSONObject jsonObject, String editAddr, String idAddress){
        String url = AppConstants.sNewAddress;
        Map<String, String> params = new HashMap<String, String>();
        params.put("securitykey", AppConstants.sSecurityKey);
        if (editAddr != null){
            if (editAddr.equals("edit")){
                params.put("action", AppConstants.sEditAddress);
                params.put("id_address", idAddress);
            }
        }
        else {
            params.put("action", AppConstants.sAddNewAddress);
        }
        params.put("sessionid", "1");
        params.put("id_customer", "1");
        params.put("store_id", "1");
        params.put("customer_address", jsonObject.toString());
        SubzeroApplication.NetworkCall(url, params, submitAddress);

    }*/
    public void submitNewAddress(AddressValues av, String editAddr, String idAddress, String ctryId, boolean isDefault) {
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMddHHmmss");
        Date date = new Date();
        Map<String, String> params = new HashMap<String, String>();
        params.put("sessionid", SharedPreference.getInstance().getValue("sessionid"));
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));

        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)){
            String url = "";
            JSONObject addressObject = new JSONObject();
            try {
                addressObject.put("firstname", av.getFname());
                addressObject.put("lastname", av.getLname());
                addressObject.put("city", av.getCity());
                addressObject.put("country_id", ctryId);
                addressObject.put("region", av.getState());
                addressObject.put("region_id", av.getState_id());
                addressObject.put("postcode", av.getZipcode());
                addressObject.put("telephone", av.getPhone());
                addressObject.put("street", av.getStreet());
                if (editAddr != null) {
                    if (editAddr.equals("edit")) {
                        if (isDefault){
                            addressObject.put("is_default_billing", true);
                            addressObject.put("is_default_shipping", true);
                        }else {
                            addressObject.put("is_default_billing", false);
                            addressObject.put("is_default_shipping", false);
                        }
                    }
                } else {
                    addressObject.put("is_default_billing", true);
                    addressObject.put("is_default_shipping", true);
                }
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (editAddr != null) {
                if (editAddr.equals("edit")) {
                    params.put("id_address", idAddress);
                    url = AppConstants.sMagentoUpdateAddress;
                }
            } else {
                url = AppConstants.sMagentoNewAddress;
            }
            params.put("customer_address", addressObject.toString());
            MyApplication.magentoNetworkCall(url, params, submitAddress);
        }else {
            String url = AppConstants.sNewAddress;
            if (editAddr != null) {
                if (editAddr.equals("edit")) {
                    params.put("action", AppConstants.sEditAddress);
                    params.put("id_address", idAddress);
                }
            } else {
                params.put("action", AppConstants.sAddNewAddress);
            }
            params.put("store_id", "1");
            params.put("method", "andorid");
            params.put("alias", dateFormat.format(date));
            params.put("firstname", av.getFname());
            params.put("lastname", av.getLname());
            params.put("city", av.getCity());
            params.put("postcode", av.getZipcode());
            params.put("telephone", av.getPhone());
            params.put("street", av.getStreet());
            params.put("country_id", ctryId);
            if (av.getState_id() != null)
                params.put("region_id", av.getState_id());
            else
                params.put("region_id", "");
            MyApplication.NetworkCall(url, params, submitAddress);
        }
    }


    /**
     * The Submit address.
     */
    VolleyCallback submitAddress = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                    ((AddNewAddress) mContext).successSubmit();
                } else {
                    ((AddNewAddress) mContext).failure(jsonObject.getString("errormsg"));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            //((AddNewAddress)context).failure(context.getString(R.string.internal_server_error));
            ((AddNewAddress) mContext).failure(errorResponse);
        }
    };

    /**
     * Address delete API
     *
     * @param addressId the address id
     */
    public void addressDelete(String addressId) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_address", addressId);
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)){
            String url = AppConstants.sMagentoDeleteAddress;
            params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
            params.put("sessionid", SharedPreference.getInstance().getValue("sessionid"));
            MyApplication.magentoNetworkCall(url, params, deleteAddress);
        }else {
            String url = AppConstants.sNewAddress;
            params.put("action", AppConstants.sDeleteAddress);
            MyApplication.NetworkCall(url, params, deleteAddress);
        }
    }

    /**
     * The Delete address.
     */
    VolleyCallback deleteAddress = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                    ((ChangeAddress) mContext).successDelete();
                } else {
                    ((ChangeAddress) mContext).failure(jsonObject.getString("errormsg"));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            //((ChangeAddress)context).failure(context.getString(R.string.internal_server_error));
            ((ChangeAddress) mContext).failure(errorResponse);
        }
    };

    /**
     * Load country API.
     */
    public void loadCountry() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("type", "country");
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)){
            String url = AppConstants.sMagentoCountryList;
            MyApplication.magentoNetworkCall(url, params, gettingCountry);
        }else {
            String url = AppConstants.sNewAddress;
            params.put("action", AppConstants.sGetCountry);
            params.put("store_id", "1");
            MyApplication.NetworkCall(url, params, gettingCountry);
        }
    }

    /**
     * The Getting country.
     */
    VolleyCallback gettingCountry = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                mCountryMainModel = (MyApplication.getGsonInstance().fromJson(response, CountryMainModel.class));
                if (mCountryMainModel.getStatus().equalsIgnoreCase("true")) {
                    ((AddNewAddress) mContext).updateCountry(mCountryMainModel);
                } else {
                    ((AddNewAddress) mContext).failure(jsonObject.getString("errormsg"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            //((AddNewAddress) context).failure(context.getString(R.string.internal_server_error));
            ((AddNewAddress) mContext).failure(errorResponse);
        }
    };

    /**
     * Load state API
     *
     * @param countryId the country id
     */
    public void loadState(String countryId) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("type", "state");
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)){
            String url = AppConstants.sMagentoStateList;
            params.put("country_id", countryId);
            params.put("sessionid", SharedPreference.getInstance().getValue("sessionid"));
            MyApplication.magentoNetworkCall(url, params, gettingState);
        }else {
            String url = AppConstants.sNewAddress;
            params.put("action", AppConstants.sGetState);
            params.put("id_country", countryId);
            params.put("store_id", "1");
            MyApplication.NetworkCall(url, params, gettingState);
        }
    }

    /**
     * The Getting state.
     */
    VolleyCallback gettingState = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                mStateMainModel = (MyApplication.getGsonInstance().fromJson(response, StateMainModel.class));
                if (mStateMainModel.getStatus().equalsIgnoreCase("true")) {
                    ((AddNewAddress) mContext).updateState(mStateMainModel);
                } else {
                    //((AddNewAddress) context).failure(jsonObject.getString("errormsg"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            //((AddNewAddress) context).failure(context.getString(R.string.internal_server_error));
            ((AddNewAddress) mContext).failure(errorResponse);
        }
    };

    /**
     * Load payment.
     *
     * @param getPaymentDetails the get payment details
     * @param shippingId        the shipping id
     * @param billingId         the billing id
     * @param cartId            the cart id
     */
    public void loadPayment(VolleyCallback getPaymentDetails, String shippingId, String billingId, String cartId) {
        SharedPreference.getInstance().save("paymentShippingId", shippingId);
        SharedPreference.getInstance().save("paymentBillingId", billingId);
        Map<String, String> params = new HashMap<String, String>();
        params.put("sessionid", SharedPreference.getInstance().getValue("sessionid"));
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        params.put("cart_id", cartId);

        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoAddShippingAddress;
            params.put("shippingaddressId", shippingId);
            params.put("billingaddressId", billingId);
            params.put("billingaddressData", "");
            params.put("shippingaddressData", "");
            MyApplication.magentoNetworkCall(url, params, getPaymentDetails);
        } else {
            String url = AppConstants.sCartController;
            params.put("action", AppConstants.sPayment);
            params.put("shippingaddressid", shippingId);
            params.put("shippingaddressdata", "");
            params.put("billingaddressid", billingId);
            params.put("billingaddressdata", "");
            params.put("email", "");
            MyApplication.NetworkCall(url, params, getPaymentDetails);
        }
    }

    /**
     * Load guest payment.
     *
     * @param getGuestDetails the get guest details
     * @param shippingObject  the shipping object
     * @param billingObject   the billing object
     * @param cartId          the cart id
     */
    public void loadGuestPayment(VolleyCallback getGuestDetails, String shippingObject, String billingObject, String cartId) {
        String sameParam;
        Map<String, String> params = new HashMap<String, String>();
        params.put("cart_id", cartId);
        if (shippingObject.equals(billingObject)) {
            sameParam = "1";
        } else {
            sameParam = "0";
        }
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoAddShippingAddress;
            params.put("sessionid", "");
            params.put("id_customer", "");
            params.put("shippingaddressId", "");
            params.put("billingaddressId", "");
            params.put("billingaddressData", billingObject);
            params.put("shippingaddressData", shippingObject);
            MyApplication.magentoNetworkCall(url, params, getGuestDetails);
        }else {
            String url = AppConstants.sCartController;
            params.put("action", AppConstants.sPayment);
            params.put("sessionid", "");
            params.put("id_customer", "");
            params.put("shippingaddressid","");
            params.put("shippingaddressdata", shippingObject);
            params.put("billingaddressid", "");
            params.put("billingaddressdata", billingObject);
            params.put("email", SharedPreference.getInstance().getValue("email"));
            params.put("same", sameParam);
            MyApplication.NetworkCall(url, params, getGuestDetails);
        }
    }

    /**
     * Load confirm page.
     *
     * @param getConfirmListDetails the get confirm list details
     */
    public void loadConfirmPage(VolleyCallback getConfirmListDetails) {
        String cartId = SharedPreference.getInstance().getValue("cart_id");
        Map<String, String> params = new HashMap<String, String>();
        params.put("cart_id", cartId);
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoConfirmOrderList;
            MyApplication.magentoNetworkCall(url, params, getConfirmListDetails);
        }else {
            String url = AppConstants.sConfirmController;
            params.put("action", AppConstants.sConfirm);
            MyApplication.NetworkCall(url, params, getConfirmListDetails);
        }
    }

    /**
     * Order place.
     *
     * @param paymentCode the payment code
     * @param transid     the transid
     */
    public void orderPlace(String paymentCode, String transid) {
        String cartId = SharedPreference.getInstance().getValue("cart_id");
        Map<String, String> params = new HashMap<String, String>();
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoCreateOrder;
            JSONObject shippingObject = new JSONObject();
            JSONObject paymentObject = new JSONObject();
            try {
                shippingObject.put("method",SharedPreference.getInstance().getValue("shippingMethod"));
                paymentObject.put("method",SharedPreference.getInstance().getValue("paymentMethod"));
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            if (!SharedPreference.getInstance().getValue("customerid").equals("0")) {
                params.put("cart_id", cartId);
                params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
                params.put("billingaddressId", SharedPreference.getInstance().getValue("paymentBillingId"));
                params.put("shippingaddressId", SharedPreference.getInstance().getValue("paymentShippingId"));
                params.put("shippingMethod", shippingObject.toString());
                params.put("paymentMethod", paymentObject.toString());
                params.put("billingaddressData", "");
                params.put("shippingaddressData", "");
                MyApplication.magentoNetworkCall(url, params, confirmPlaceOrder);
            }else {
                params.put("cart_id", cartId);
                params.put("id_customer", "");
                params.put("billingaddressId", "");
                params.put("shippingaddressId", "");
                params.put("shippingMethod", shippingObject.toString());
                params.put("paymentMethod", paymentObject.toString());
                params.put("billingaddressData", SharedPreference.getInstance().getValue("billingStaticAddress"));
                params.put("shippingaddressData", SharedPreference.getInstance().getValue("shippingStaticAddress"));
                MyApplication.magentoNetworkCall(url, params, confirmPlaceOrder);
            }
        }else {
            String url = AppConstants.sConfirmController;
            params.put("action", AppConstants.sConfirmOrder);
            params.put("cart_id", cartId);
            params.put("payment_code", paymentCode);
            params.put("confirm", "1");
            params.put("transaction_id", transid);
            MyApplication.NetworkCall(url, params, confirmPlaceOrder);
        }
    }

    /**
     * The Confirm place order.
     */
    VolleyCallback confirmPlaceOrder = new VolleyCallback() {
        @Override
        public void Success(String response) {
            if (response.equals(mContext.getString(R.string.cartFailureText))) {
                ((OrderConfirmActivity) mContext).failure(response);
            } else {
                try {
                    JSONObject jsonObject = null;
                    jsonObject = new JSONObject(response);
                    if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                        magentoOrderMain = (MyApplication.getGsonInstance().fromJson(response, MagentoOrderMain.class));
                        if (magentoOrderMain.getStatus()) {
                            ((OrderConfirmActivity) mContext).updateOrderStatus(magentoOrderMain.getResult());
                        } else {
                            ((OrderConfirmActivity) mContext).failure(jsonObject.getString("errormsg"));
                        }
                    }else {
                        orderConfirmModel = (MyApplication.getGsonInstance().fromJson(response, OrderConfirmModel.class));
                        if (orderConfirmModel.getStatus().equalsIgnoreCase("true")) {
                            ((OrderConfirmActivity) mContext).updateOrderStatus(orderConfirmModel.getResult());
                        } else {
                            ((OrderConfirmActivity) mContext).failure(jsonObject.getString("errormsg"));
                        }
                    }
                } catch (Exception e) {
                    Log.e("error response", "confirmplaceorder == " + e);
                    e.printStackTrace();
                }
            }
        }

        @Override
        public void Failure(String errorResponse) {
            //((OrderConfirmActivity) context).failure(context.getString(R.string.internal_server_error));
            ((OrderConfirmActivity) mContext).failure(errorResponse);
        }
    };

    /**
     * Paypal trans id.
     *
     * @param paymentKey the payment key
     */
    public void paypalTransId(String paymentKey) {
        CheckoutController.mPaymentKey = paymentKey;
        Map<String, String> params = new HashMap<String, String>();
        params.put("grant_type", "client_credentials");
        String api_url = AppConstants.oauthUrl;

        for (Map.Entry<String, String> entry : params.entrySet()) {
            api_url = api_url + "?" + entry.getKey() + "=" + entry.getValue();
        }

        Log.e("Request url++++", api_url);
        byte[] encoded = null;
        String clientCredentials = OrderConfirmActivity.sPaypalClientId + ":" + OrderConfirmActivity.sPaySecretKey;
        try {
            encoded = com.prestashopemc.paypal.Base64.encodeBase64(clientCredentials
                    .getBytes("UTF-8"));
            mBase64ClientID = new String(encoded, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        HashMap<String, String> headers = new HashMap<String, String>();
        headers.put("Accept", "application/json");
        headers.put("Authorization", "Basic " + mBase64ClientID);
        MyApplication.NetworkCallParams(Request.Method.POST, api_url, headers, paypalreponse);


    }

    /**
     * The Paypalreponse.
     */
    VolleyCallback paypalreponse = new VolleyCallback() {
        @Override
        public void Success(String response) {

            PaypalResponse mPaypalResponse = (MyApplication.getGsonInstance().fromJson(response, PaypalResponse.class));

            mAccessToken = mPaypalResponse.getTokentype() + " "
                    + mPaypalResponse.getAccesstoken();

            HashMap<String, String> headers = new HashMap<String, String>();
            headers.put("Content-Type", "application/json");
            headers.put("Authorization", mAccessToken);

            MyApplication.NetworkCallParams(Request.Method.GET, AppConstants.paypalTransUrl + mPaymentKey, headers, paypaltransacitonreponse);

        }


        @Override
        public void Failure(String errorResponse) {
            //((OrderConfirmActivity) context).failure(context.getString(R.string.internal_server_error));
            ((OrderConfirmActivity) mContext).failure(errorResponse);
        }
    };


    /**
     * The Paypaltransacitonreponse.
     */
    VolleyCallback paypaltransacitonreponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            ((OrderConfirmActivity) mContext).paypalTransIdSuccess(response);
        }


        @Override
        public void Failure(String errorResponse) {
            //((OrderConfirmActivity) context).failure(context.getString(R.string.internal_server_error));
            ((OrderConfirmActivity) mContext).failure(errorResponse);
        }
    };

    /**
     * Order telr string.
     *
     * @param paymentCode the payment code
     * @return the string
     */
    public String orderTelr(String paymentCode) {
        String url = AppConstants.sConfirmController;
        String cartId = SharedPreference.getInstance().getValue("cart_id");
        Map<String, String> params = new HashMap<String, String>();
        params.put("securitykey", SharedPreference.getInstance().getValue("security_key"));
        params.put("action", AppConstants.sConfirmOrder);
        params.put("cart_id", cartId);
        params.put("payment_code", paymentCode);
        params.put("confirm", "1");
        params.put("id_currency", SharedPreference.getInstance().getValue("id_currency"));
        params.put("id_lang", SharedPreference.getInstance().getValue("id_language"));

        String api_url = url;

        for (Map.Entry<String, String> entry : params.entrySet()) {
            api_url = api_url + "&" + entry.getKey() + "=" + entry.getValue();
        }

        Log.e("Telr Request url++++", api_url);


        return api_url;

    }

    /**
     * Shipping price.
     *
     * @param shippingCode          the shipping code
     * @param shippingPriceResponse the shipping price response
     */
    public void shippingPrice(String shippingCode, String shippingName, VolleyCallback shippingPriceResponse) {
        String cartId = SharedPreference.getInstance().getValue("cart_id");
        Map<String, String> params = new HashMap<String, String>();
        params.put("cart_id", cartId);
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)){
            String url = AppConstants.sMagentoSetShippingMethod;
            params.put("shipping_method", shippingName);
            MyApplication.magentoNetworkCall(url, params, shippingPriceResponse);
        }else {
            String url = AppConstants.sCartController;
            params.put("action", AppConstants.sShippingPrice);
            params.put("id_carrier", shippingCode);
            MyApplication.NetworkCall(url, params, shippingPriceResponse);
        }
    }
}
