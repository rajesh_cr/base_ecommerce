package com.prestashopemc.controller;

import android.content.Context;
import android.util.Log;

import com.prestashopemc.model.ContactValues;
import com.prestashopemc.model.Customer;
import com.prestashopemc.model.LoginValues;
import com.prestashopemc.model.MagentoReviewMain;
import com.prestashopemc.model.MyAdditionalAddress;
import com.prestashopemc.model.MyOrderDetailMain;
import com.prestashopemc.model.MyOrderListMain;
import com.prestashopemc.model.MyReviewsMain;
import com.prestashopemc.model.NotificationMain;
import com.prestashopemc.model.RegisterValues;
import com.prestashopemc.model.WishlistMainModel;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.VolleyCallback;
import com.prestashopemc.view.AccountActivity;
import com.prestashopemc.view.ChangePasswordActivity;
import com.prestashopemc.view.CmsActivity;
import com.prestashopemc.view.ContactUsActivity;
import com.prestashopemc.view.EditProfileActivity;
import com.prestashopemc.view.MyAddressActivity;
import com.prestashopemc.view.MyOrdersActivity;
import com.prestashopemc.view.MyOrdersDetailsActivity;
import com.prestashopemc.view.MyReviewsActivity;
import com.prestashopemc.view.MyWishlistActivity;
import com.prestashopemc.view.NotificationActivity;
import com.prestashopemc.view.SettingActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * <h1>Login Controller!</h1>
 * The Login Controller to form the request and response for Login Api call.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class LoginController {

    private Context mContext;
    private MyOrderListMain mMyOrderListMain;
    private WishlistMainModel mWishlistMainModel;
    private String mPassword;

    /**
     * Instantiates a new Login controller.
     *
     * @param context the context
     */
    public LoginController(Context context) {
        this.mContext = context;
    }

    /**
     * Login API @param loginValues the login values
     *
     * @param loginValues the login values
     */
    public void loginDetails(LoginValues loginValues) {

        String cartId = SharedPreference.getInstance().getValue("cart_id");
        Map<String, String> params = new HashMap<String, String>();
        mPassword = loginValues.getPassword();
        params.put("cart_id", cartId);
        params.put("password", loginValues.getPassword());
        params.put("app_id", "3");
        params.put("device_type", "1");
        params.put("email", loginValues.getEmail());
        params.put("device_token", loginValues.getAndroidId());
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoLogin;
            MyApplication.magentoNetworkCall(url, params, loginResponse);
        } else {
            String url = AppConstants.sLogin;
            params.put("action", AppConstants.sLoginAction);
            MyApplication.NetworkCall(url, params, loginResponse);
        }
    }

    /**
     * The Login response.
     */
    VolleyCallback loginResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                    SharedPreference mPref = SharedPreference.getInstance();
                    JSONObject objValue = jsonObject.getJSONObject("customerdetails");
                    Customer customer = new Customer();
                    customer.setmCustomerId(objValue.getString("id"));
                    customer.setmEmail(objValue.getString("email"));
                    customer.setmFirstName(objValue.getString("first_name"));
                    customer.setmLastName(objValue.getString("last_name"));
                    customer.setmMobileNumber(objValue.getString("mobile_number"));
                    customer.setmProfileImage(objValue.getString("profile_image"));
                    customer.setmPushEnable(objValue.getBoolean("push_enable"));
                    String customerObject = MyApplication.getGsonInstance().toJson(customer);
                    mPref.save("sessionid", jsonObject.getString("sessionid"));
                    mPref.save("cart_id", jsonObject.getString("cart_id"));
                    mPref.save("cart_count", jsonObject.getString("cartcount"));
                    mPref.save("customerid", objValue.getString("id"));
                    mPref.save("customer", customerObject);
                    mPref.saveBoolean("isPush", objValue.getBoolean("push_enable"));
                    mPref.save("currentPassword", mPassword);
                    ((AccountActivity) mContext).updateLogin();
                } else {
                    ((AccountActivity) mContext).failure(jsonObject.getString("errormsg"));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((AccountActivity) mContext).failure(errorResponse);
        }
    };

    /**
     * Create Account API @param registerValues the register values
     *
     * @param registerValues the register values
     * @param socialName     the social name
     */
    public void createAccount(RegisterValues registerValues, String socialName) {
        mPassword = registerValues.getPassword();
        String cartId = SharedPreference.getInstance().getValue("cart_id");
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            Map<String, String> params = new HashMap<String, String>();
            String url = AppConstants.sMagentoRegister;
            JSONObject customerObj = new JSONObject();
            try {
                customerObj.put("email", registerValues.getEmail());
                customerObj.put("device_type", "1");
                customerObj.put("appid", "3");
                customerObj.put("lastname", registerValues.getLastName());
                customerObj.put("firstname", registerValues.getFirstName());
                customerObj.put("device_token", registerValues.getAndroidId());
                customerObj.put("password", registerValues.getPassword());
                customerObj.put("push_enable", "1");
                customerObj.put("mobile_number", registerValues.getMobileNum());
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            params.put("customerData", customerObj.toString());
            params.put("cart_id", cartId);
            SharedPreference.getInstance().saveBoolean("isSocial", true);
            MyApplication.imageMagentoNetworkCall(url, createAccountImageResponse, params, null, socialName,false);
        } else {
            Map<String, String> params = new HashMap<String, String>();
            String url = AppConstants.sLogin;
            params.put("action", AppConstants.sCreateAction);
            params.put("customer_firstname", registerValues.getFirstName());
            params.put("customer_lastname", registerValues.getLastName());
            params.put("email", registerValues.getEmail());
            params.put("password", registerValues.getPassword());
            params.put("device_type", "1");
            params.put("device_token", registerValues.getAndroidId());
            if (!socialName.isEmpty()) {
                params.put("login_type", socialName);
                SharedPreference.getInstance().saveBoolean("isSocial", true);
            }
            MyApplication.NetworkCall(url, params, createAccountResponse);
        }
    }

    /**
     * Create Account Api With Image  @param registerValues the register values
     *
     * @param registerValues the register values
     * @param socialName     the social name
     * @param imageFile      the image file
     */
    public void createAccountImage(RegisterValues registerValues, String socialName, File imageFile) {

        ((AccountActivity) mContext).showProgDialiog();
        mPassword = registerValues.getPassword();
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            Map<String, String> params = new HashMap<String, String>();
            JSONObject customerObj = new JSONObject();
            try {
                customerObj.put("email", registerValues.getEmail());
                customerObj.put("device_type", "1");
                customerObj.put("appid", "3");
                customerObj.put("lastname", registerValues.getLastName());
                customerObj.put("firstname", registerValues.getFirstName());
                customerObj.put("device_token", registerValues.getAndroidId());
                customerObj.put("password", registerValues.getPassword());
                customerObj.put("push_enable", "1");
                customerObj.put("mobile_number", registerValues.getMobileNum());
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            params.put("customerData", customerObj.toString());

            MyApplication.imageMagentoNetworkCall(AppConstants.sMagentoRegister, createAccountImageResponse, params, imageFile, socialName,false);

        } else {
            Map<String, String> params = new HashMap<String, String>();
            String cartId = SharedPreference.getInstance().getValue("cart_id");
            String url = AppConstants.sLogin;
            params.put("cart_id", cartId);
            params.put("customer_firstname", registerValues.getFirstName());
            params.put("customer_lastname", registerValues.getLastName());
            params.put("email", registerValues.getEmail());
            params.put("password", registerValues.getPassword());
            params.put("device_type", "1");
            params.put("device_token", registerValues.getAndroidId());
            params.put("mobile_number", registerValues.getMobileNum());
            params.put("action", AppConstants.sCreateAction);
            MyApplication.imageUploadNetworkCall(url, createAccountImageResponse, params, imageFile);
        }
    }


    /**
     * The Create account response.
     */
    VolleyCallback createAccountResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {


            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                    SharedPreference mPref = SharedPreference.getInstance();
                    JSONObject objValue = jsonObject.getJSONObject("customerdetails");
                    Customer customer = new Customer();
                    customer.setmCustomerId(objValue.getString("id"));
                    customer.setmEmail(objValue.getString("email"));
                    customer.setmFirstName(objValue.getString("first_name"));
                    customer.setmLastName(objValue.getString("last_name"));
                    customer.setmMobileNumber(objValue.getString("mobile_number"));
                    customer.setmProfileImage(objValue.getString("profile_image"));
                    customer.setmPushEnable(objValue.getBoolean("push_enable"));
                    String customerObject = MyApplication.getGsonInstance().toJson(customer);
                    mPref.save("sessionid", jsonObject.getString("sessionid"));
                    mPref.save("cart_id", jsonObject.getString("cart_id"));
                    mPref.save("cart_count", jsonObject.getString("cartcount"));
                    mPref.save("customerid", objValue.getString("id"));
                    mPref.save("customer", customerObject);
                    mPref.saveBoolean("isPush", objValue.getBoolean("push_enable"));
                    mPref.save("currentPassword", mPassword);
                    ((AccountActivity) mContext).updateCreateAccount();

                } else {
                    SharedPreference.getInstance().saveBoolean("isSocial", false);
                    ((AccountActivity) mContext).failure(jsonObject.getString("errormsg"));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }


        }

        @Override
        public void Failure(String errorResponse) {
            ((AccountActivity) mContext).failure(errorResponse);
        }
    };


    /**
     * The Create account image response.
     */
    VolleyCallback createAccountImageResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {

            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);

                if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                    SharedPreference mPref = SharedPreference.getInstance();
                    JSONObject objValue = jsonObject.getJSONObject("customerdetails");
                    Customer customer = new Customer();
                    customer.setmCustomerId(objValue.getString("id"));
                    customer.setmEmail(objValue.getString("email"));
                    customer.setmFirstName(objValue.getString("first_name"));
                    customer.setmLastName(objValue.getString("last_name"));
                    customer.setmMobileNumber(objValue.getString("mobile_number"));
                    customer.setmProfileImage(objValue.getString("profile_image"));
                    customer.setmPushEnable(objValue.getBoolean("push_enable"));
                    String customerObject = MyApplication.getGsonInstance().toJson(customer);
                    mPref.save("sessionid", jsonObject.getString("sessionid"));
                    mPref.save("cart_id", jsonObject.getString("cart_id"));
                    mPref.save("cart_count", jsonObject.getString("cartcount"));
                    mPref.save("customerid", objValue.getString("id"));
                    mPref.save("customer", customerObject);
                    mPref.saveBoolean("isPush", objValue.getBoolean("push_enable"));
                    mPref.save("currentPassword", mPassword);
                    ((AccountActivity) mContext).updateCreateAccount();
                } else {
                    SharedPreference.getInstance().saveBoolean("isSocial", false);
                    ((AccountActivity) mContext).failure(jsonObject.getString("errormsg"));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void Failure(String errorResponse) {
            ((AccountActivity) mContext).failure(errorResponse);
        }
    };


    /* Update Profile page */

    /**
     * Update profile.
     *
     * @param customer  the customer
     * @param imageFile the image file
     */
    public void updateProfile(Customer customer, File imageFile) {


        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoUpdateProfile;
            Map<String, String> params = new HashMap<String, String>();
            JSONObject customerObject = new JSONObject();
            try {
                customerObject.put("firstname", customer.getmFirstName());
                customerObject.put("lastname", customer.getmLastName());
                customerObject.put("email", customer.getmEmail());
                customerObject.put("mobile_number", customer.getmMobileNumber());
                customerObject.put("website_id", "1");
                customerObject.put("push_enable", "1");
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            params.put("customerData", customerObject.toString());
            params.put("id_customer", customer.getmCustomerId());
            MyApplication.imageMagentoNetworkCall(url, updateProfileResponse, params, imageFile, "",true);
        } else {
            Map<String, String> params = new HashMap<String, String>();
            params.put("firstname", customer.getmFirstName());
            params.put("lastname", customer.getmLastName());
            params.put("email", customer.getmEmail());
            params.put("mobile_number", customer.getmMobileNumber());
            params.put("id_customer", customer.getmCustomerId());
            String url = AppConstants.sCheckout;
            params.put("action", AppConstants.sUpdateProfile);
            MyApplication.imageUploadNetworkCall(url, updateProfileResponse, params, imageFile);
        }
    }


    /**
     * The Update profile response.
     */
    VolleyCallback updateProfileResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            Log.e("edit profile success", response);
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                    SharedPreference mPref = SharedPreference.getInstance();
                    JSONObject objValue = jsonObject.getJSONObject("customerdetails");
                    Customer customer = new Customer();
                    customer.setmCustomerId(objValue.getString("id"));
                    customer.setmEmail(objValue.getString("email"));
                    customer.setmFirstName(objValue.getString("first_name"));
                    customer.setmLastName(objValue.getString("last_name"));
                    customer.setmMobileNumber(objValue.getString("mobile_number"));
                    customer.setmProfileImage(objValue.getString("profile_image"));
                    customer.setmPushEnable(objValue.getBoolean("push_enable"));
                    String customerObject = MyApplication.getGsonInstance().toJson(customer);
                    mPref.save("sessionid", jsonObject.getString("sessionid"));
                    //mPref.save("cart_id", jsonObject.getString("cart_id"));
                    //mPref.save("cart_count", jsonObject.getString("cartcount"));
                    mPref.save("customerid", objValue.getString("id"));
                    mPref.save("customer", customerObject);
                    mPref.saveBoolean("isPush", objValue.getBoolean("push_enable"));
                    ((EditProfileActivity) mContext).updateSuccess();
                } else {
                    ((EditProfileActivity) mContext).failure(jsonObject.getString("errormsg"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void Failure(String errorResponse) {
            ((EditProfileActivity) mContext).failure(errorResponse);
        }
    };

    /**
     * Forgot password.
     *
     * @param email the email
     */
/*Forgot Password API*/
    public void forgotPassword(String email) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("store_id", "1");
        params.put("email", email);

        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoForgotPassword;
            MyApplication.magentoNetworkCall(url, params, forgotPasswordResponse);
        } else {
            String url = AppConstants.sCheckout;
            params.put("action", AppConstants.sForgotPassword);
            MyApplication.NetworkCall(url, params, forgotPasswordResponse);
        }
    }

    /**
     * The Forgotassword response.
     */
    VolleyCallback forgotPasswordResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                    ((AccountActivity) mContext).success();
                } else {
                    ((AccountActivity) mContext).failure(jsonObject.getString("errormsg"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void Failure(String errorResponse) {
            ((AccountActivity) mContext).failure(errorResponse);
        }
    };

    /**
     * Cms details.
     *
     * @param pageIdentifier the page identifier
     */
/*CMS display API*/
    public void cmsDetails(String pageIdentifier) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("page_identifier", pageIdentifier);
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoCmsResult;
            MyApplication.magentoNetworkCall(url, params, cmsDisplayResponse);
        } else {
            String url = AppConstants.sCategoriesController;
            params.put("action", AppConstants.sCmsAction);
            params.put("store_id", "1");
            MyApplication.NetworkCall(url, params, cmsDisplayResponse);
        }
    }

    /**
     * The Cms display response.
     */
    VolleyCallback cmsDisplayResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                    ((CmsActivity) mContext).cmsSuccess(jsonObject.getString("result"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            //((CmsActivity) context).cmsFailure(context.getString(R.string.internal_server_error));
            ((CmsActivity) mContext).cmsFailure(errorResponse);
        }
    };

    /**
     * My orders list API.
     */
    public void myOrdersList() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("sessionid", SharedPreference.getInstance().getValue("sessionid"));
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoMyOrderList;
            MyApplication.magentoNetworkCall(url, params, myOrderResponse);
        } else {
            String url = AppConstants.sConfirmController;
            params.put("store_id", "1");
            params.put("action", AppConstants.sMyorderAction);
            MyApplication.NetworkCall(url, params, myOrderResponse);
        }
    }

    /**
     * The My order response.
     */
    VolleyCallback myOrderResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                mMyOrderListMain = (MyApplication.getGsonInstance().fromJson(response, MyOrderListMain.class));
                if (mMyOrderListMain.getStatus().equalsIgnoreCase("true")) {
                    ((MyOrdersActivity) mContext).myOrdersSuccess(mMyOrderListMain);
                } else {
                    ((MyOrdersActivity) mContext).myOrdersFailure(jsonObject.getString("errormsg"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((MyOrdersActivity) mContext).myOrdersFailure(errorResponse);
        }
    };

    /**
     * Order information.
     *
     * @param orderId the order id
     */
    public void orderInformation(String orderId) {
        Map<String, String> params = new HashMap<String, String>();
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoMyOrderDetails;
            params.put("increment_id", orderId);
            MyApplication.magentoNetworkCall(url, params, orderInformationResponse);
        } else {
            String url = AppConstants.sConfirmController;
            params.put("action", AppConstants.sMyorderDetailsAction);
            params.put("store_id", "1");
            params.put("id_order", orderId);
            params.put("sessionid", SharedPreference.getInstance().getValue("sessionid"));
            params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
            MyApplication.NetworkCall(url, params, orderInformationResponse);
        }
    }

    /**
     * The Order information response.
     */
    VolleyCallback orderInformationResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {

            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                MyOrderDetailMain myOrderDetailMain = (MyApplication.getGsonInstance().fromJson(response, MyOrderDetailMain.class));
                if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                    ((MyOrdersDetailsActivity) mContext).informationSuccess(myOrderDetailMain);
                } else {
                    ((MyOrdersDetailsActivity) mContext).informationFailure(jsonObject.getString("errormsg"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


        }

        @Override
        public void Failure(String errorResponse) {
            ((MyOrdersDetailsActivity) mContext).informationFailure(errorResponse);
        }
    };

    /**
     * My wishlist details.
     */
    public void myWishlistDetails() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoGetWishlist;
            //params.put("store_id", "1");
            params.put("sessionid", SharedPreference.getInstance().getValue("sessionid"));
            MyApplication.magentoNetworkCall(url, params, myWishlistResponse);
        } else {
            String url = AppConstants.sCartController;
            params.put("action", AppConstants.sMyWishlist);
            MyApplication.NetworkCall(url, params, myWishlistResponse);
        }
    }

    /**
     * The My wishlist response.
     */
    VolleyCallback myWishlistResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                mWishlistMainModel = (MyApplication.getGsonInstance().fromJson(response, WishlistMainModel.class));
                if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                    ((MyWishlistActivity) mContext).mywishlistSuccess(mWishlistMainModel);
                } else {
                    ((MyWishlistActivity) mContext).mywishlistFailure(jsonObject.getString("errormsg"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            //((MyWishlistActivity) context).mywishlistFailure(context.getString(R.string.internal_server_error));
            ((MyWishlistActivity) mContext).mywishlistFailure(errorResponse);
        }
    };

    /**
     * My wishlist delete API.
     *
     * @param productId   the product id
     * @param attributeId the attribute id
     * @param wishlistId  the wishlist id
     */

    public void myWishlistDelete(String productId, String attributeId, String wishlistId) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        params.put("sessionid", SharedPreference.getInstance().getValue("sessionid"));
        params.put("id_product", productId);
        params.put("store_id", "1");
        params.put("id_wishlist", wishlistId);
//        params.put("id_product_attribute", attributeId);
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoRemoveWishlist;
            MyApplication.magentoNetworkCall(url, params, mywishlistDeleteResponse);
        } else {
            String url = AppConstants.sCartController;
            params.put("action", AppConstants.sMyWishlistDelete);
            MyApplication.NetworkCall(url, params, mywishlistDeleteResponse);
        }
    }

    /**
     * The Mywishlist delete response.
     */
    VolleyCallback mywishlistDeleteResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                    ((MyWishlistActivity) mContext).mywishlistDeleteSuccess(jsonObject.getString("result"));
                } else {
                    ((MyWishlistActivity) mContext).mywishlistFailure(jsonObject.getString("errormsg"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((MyWishlistActivity) mContext).mywishlistFailure(errorResponse);
        }
    };

    /**
     * My reviews details.
     */
/*My Reviews API*/
    public void myReviewsDetails() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        params.put("sessionid", SharedPreference.getInstance().getValue("sessionid"));
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoMyReviews;
            MyApplication.magentoNetworkCall(url, params, myreviewsResponse);
        } else {
            String url = AppConstants.sCategoriesController;
            params.put("action", AppConstants.sMyReviews);
            params.put("store_id", "1");
            MyApplication.NetworkCall(url, params, myreviewsResponse);
        }
    }

    /**
     * The Myreviews response.
     */
    VolleyCallback myreviewsResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                    MagentoReviewMain magentoReviewMain = (MyApplication.getGsonInstance().fromJson(response, MagentoReviewMain.class));
                    if (jsonObject.getBoolean("status")) {
                        ((MyReviewsActivity) mContext).magentoReviewSuccess(magentoReviewMain);
                    } else {
                        ((MyReviewsActivity) mContext).reviewsListFailure(jsonObject.getString("errormsg"));
                    }
                }else {
                    MyReviewsMain myReviewsMain = (MyApplication.getGsonInstance().fromJson(response, MyReviewsMain.class));
                    if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                        ((MyReviewsActivity) mContext).reviewsListSuccess(myReviewsMain);
                    } else {
                        ((MyReviewsActivity) mContext).reviewsListFailure(jsonObject.getString("errormsg"));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((MyReviewsActivity) mContext).reviewsListFailure(errorResponse);
        }
    };

    /**
     * Change password.
     *
     * @param oldPassword the old password
     * @param newPassword the new password
     */
/*Change password API*/
    public void changePassword(String oldPassword, String newPassword) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoChangePassword;
            params.put("oldpassword", oldPassword);
            params.put("newpassword", newPassword);
            MyApplication.magentoNetworkCall(url, params, changePasswordResponse);
        }else {
            String url = AppConstants.sCheckout;
            params.put("action", AppConstants.sUpdatePassword);
            params.put("old_passwd", oldPassword);
            params.put("new_passwd", newPassword);
            MyApplication.NetworkCall(url, params, changePasswordResponse);
        }
    }

    /**
     * The Change password response.
     */
    VolleyCallback changePasswordResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                    ((ChangePasswordActivity) mContext).changePasswordSuccess(jsonObject.getString("result"));
                } else {
                    if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                        ((ChangePasswordActivity) mContext).changePasswordFailure(jsonObject.getString("result"));
                    }else {
                        ((ChangePasswordActivity) mContext).changePasswordFailure(jsonObject.getString("errormsg"));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            //((ChangePasswordActivity) context).changePasswordFailure(context.getString(R.string.internal_server_error));
            ((ChangePasswordActivity) mContext).changePasswordFailure(errorResponse);
        }
    };

    /**
     * Contact us.
     *
     * @param contactValues the contact values
     */
/*Contact Us API*/
    public void contactUs(ContactValues contactValues) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("name", contactValues.getContactName());
        params.put("email", contactValues.getContactEmail());
        params.put("telephone", contactValues.getContactPhone());
        params.put("message", contactValues.getContactComent());

        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoContactUs;
            //params.put("store_id", "1");
            MyApplication.magentoNetworkCall(url, params, contactUsResponse);
        } else {
            String url = AppConstants.sLogin;
            params.put("action", AppConstants.sContactUs);
            MyApplication.NetworkCall(url, params, contactUsResponse);
        }
    }

    /**
     * The Contact us response.
     */
    VolleyCallback contactUsResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                    ((ContactUsActivity) mContext).contactUsSuccess(jsonObject.getString("result"));
                } else {
                    ((ContactUsActivity) mContext).contactUsFailure(jsonObject.getString("errormsg"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((ContactUsActivity) mContext).contactUsFailure(errorResponse);
        }
    };

    /**
     * My address.
     */
/*My Address Page Initial API*/
    public void myAddress() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoMultipleBillingAddress;
            params.put("sessionid", SharedPreference.getInstance().getValue("sessionid"));
            MyApplication.magentoNetworkCall(url, params, getMyAddress);
        }else {
            String url = AppConstants.sNewAddress;
            params.put("action", AppConstants.sGetAddress);
            MyApplication.NetworkCall(url, params, getMyAddress);
        }
    }

    /**
     * The Get my address.
     */
    VolleyCallback getMyAddress = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                MyAdditionalAddress addressMainModel = (MyApplication.getGsonInstance().fromJson(response, MyAdditionalAddress.class));
                if (addressMainModel.getStatus().equalsIgnoreCase("true")) {
                    ((MyAddressActivity) mContext).addressSuccessResponse(addressMainModel);
                } else {
                    ((MyAddressActivity) mContext).addressFailureResponse(jsonObject.getString("errormsg"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((MyAddressActivity) mContext).addressFailureResponse(errorResponse);
        }
    };

    /**
     * Sets default address.
     *
     * @param defaultObject the default object
     * @param jsonObject    the json object
     * @param newId         the new id
     * @param oldId         the old id
     */
/*Set Defaul Address API*/
    public void setDefaultAddress(JSONObject defaultObject, JSONObject jsonObject, String newId, String oldId) {
        String url = AppConstants.sNewAddress;
        Map<String, String> params = new HashMap<String, String>();
        params.put("action", AppConstants.sSetDefaultAddress);
        params.put("new_customer_data", jsonObject.toString());
        params.put("old_customer_data", defaultObject.toString());
        params.put("id_address_default_new", newId);
        params.put("id_address_default_old", oldId);
        MyApplication.NetworkCall(url, params, defaultAddressResponse);
    }

    /**
     * The Default address response.
     */
    VolleyCallback defaultAddressResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                    ((MyAddressActivity) mContext).defaultAddressSuccessResponse(jsonObject.getString("result"));
                } else {
                    ((MyAddressActivity) mContext).defaultAddressFailureResponse(jsonObject.getString("errormsg"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((MyAddressActivity) mContext).defaultAddressFailureResponse(errorResponse);
        }
    };

    /**
     * Default address delete.
     *
     * @param addressId the address id
     */
/*Address Delete API*/
    public void defaultAddressDelete(String addressId) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_address", addressId);
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoDeleteAddress;
            params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
            params.put("sessionid", SharedPreference.getInstance().getValue("sessionid"));
            MyApplication.magentoNetworkCall(url, params, deleteAddress);
        } else {
            String url = AppConstants.sNewAddress;
            params.put("action", AppConstants.sDeleteAddress);
            MyApplication.NetworkCall(url, params, deleteAddress);
        }
    }

    /**
     * The Delete address.
     */
    VolleyCallback deleteAddress = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                    ((MyAddressActivity) mContext).successDelete();
                } else {
                    ((MyAddressActivity) mContext).failure(jsonObject.getString("errormsg"));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((MyAddressActivity) mContext).failure(errorResponse);
        }
    };

    /**
     * Notification list.
     */
/*Notification List API*/
    public void notificationList() {
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoNotificationList;
            MyApplication.magentoNetworkCall(url, params, pushList);
        }else {
            String url = AppConstants.sConfirmController;
            params.put("action", AppConstants.sPush);
            MyApplication.NetworkCall(url, params, pushList);
        }
    }

    /**
     * The Push list.
     */
    VolleyCallback pushList = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                NotificationMain notificationMain = (MyApplication.getGsonInstance().fromJson(response, NotificationMain.class));
                if (notificationMain.getStatus().equalsIgnoreCase("true")) {
                    ((NotificationActivity) mContext).success(notificationMain);
                } else {
                    ((NotificationActivity) mContext).failure(jsonObject.getString("errormsg"));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((NotificationActivity) mContext).failure(errorResponse);
        }
    };

    /**
     * Push enable disable.
     *
     * @param mPush the m push
     */
/*Push Enable and Disable*/
    public void pushEnableDisable(String mPush) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        params.put("push_enable", mPush);
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoPushEnableDisable;
            MyApplication.magentoNetworkCall(url, params, enableDisable);
        }else {
            String url = AppConstants.sConfirmController;
            params.put("action", AppConstants.sPushEnable);
            MyApplication.NetworkCall(url, params, enableDisable);
        }
    }

    /**
     * The Enable disable.
     */
    VolleyCallback enableDisable = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                if (jsonObject.getBoolean("status") == true) {
                    ((SettingActivity) mContext).success(jsonObject.getBoolean("result"), jsonObject.getString("message"));
                } else {
                    ((SettingActivity) mContext).failure(jsonObject.getString("message"));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((SettingActivity) mContext).failure(errorResponse);
        }
    };

    /**
     * Set default address for magento customer
     */
    public void magentoSetDefaultAddress(String addressId){
        String url = AppConstants.sMagentoSetDefaultAddress;
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_address", addressId);
        MyApplication.magentoNetworkCall(url, params, magentoDefaultAddressResponse);
    }

    /**
     * The Magento Default address response.
     */
    VolleyCallback magentoDefaultAddressResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                if (jsonObject.getBoolean("status")) {
                    //jsonObject.getString("addressid")
                    ((MyAddressActivity) mContext).defaultAddressSuccessResponse(AppConstants.getTextString(mContext, AppConstants.newAddressUpdatedSuccess));
                } else {
                    ((MyAddressActivity) mContext).defaultAddressFailureResponse(jsonObject.getString("errormsg"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((MyAddressActivity) mContext).defaultAddressFailureResponse(errorResponse);
        }
    };
}
