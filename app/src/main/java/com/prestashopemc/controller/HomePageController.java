package com.prestashopemc.controller;

import android.content.Context;
import android.content.res.Configuration;
import android.util.Log;

import com.prestashopemc.database.BannerService;
import com.prestashopemc.database.CategoryProductService;
import com.prestashopemc.database.CategoryService;
import com.prestashopemc.database.CmsService;
import com.prestashopemc.database.CurrencyService;
import com.prestashopemc.database.DatabaseHelper;
import com.prestashopemc.database.LanguageService;
import com.prestashopemc.database.MagentoCategoryProductService;
import com.prestashopemc.database.MagentoProductService;
import com.prestashopemc.database.MultipleLanguageService;
import com.prestashopemc.database.ProductService;
import com.prestashopemc.model.Category;
import com.prestashopemc.model.CategoryProduct;
import com.prestashopemc.model.ExpandCategory;
import com.prestashopemc.model.HomePage;
import com.prestashopemc.model.Language;
import com.prestashopemc.model.UpdateVersion;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.VolleyCallback;
import com.prestashopemc.view.MainActivity;
import com.prestashopemc.view.SettingActivity;
import com.prestashopemc.view.SubCategoryActivity;

import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * <h1>Home Page Controller!</h1>
 * The Home Page Controller to form the request and response for root categories and update cart info Api call.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class HomePageController {

    private Context mContext;
    public DatabaseHelper mDatabaseHelper;
    public CategoryService mCategoryService;
    public BannerService mBannerService;
    public CmsService mCmsService;
    public LanguageService mLangService;
    public CurrencyService mCurrencyService;
    private HomePage mHomePageResponse;
    private UpdateVersion mUpdateVersion;
    private boolean mIsCategoryExists;
    public MultipleLanguageService mTranslationService;
    private boolean isCategoty = false, isHome = false, isProductSettings = false;
    public static boolean isLangCurrency = false;
    private CategoryProductService mCategoryProductService;
    private ProductService mProductService;
    private MagentoCategoryProductService mMagentoCategoryProductService;
    private MagentoProductService mMagentoProductService;
    private List<ExpandCategory> mExpandCategory;
    private List<CategoryProduct> mCategoryProducts;

    /**
     * Instantiates a new Home page controller.
     *
     * @param mContext the m context
     */
    public HomePageController(Context mContext) {
        this.mContext = mContext;
        mDatabaseHelper = new DatabaseHelper(mContext);
        mCategoryService = new CategoryService(mContext);
        mBannerService = new BannerService(mContext);
        mCmsService = new CmsService(mContext);
        mLangService = new LanguageService(mContext);
        mCurrencyService = new CurrencyService(mContext);
        mTranslationService = new MultipleLanguageService(mContext);
        if(AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)){
            mMagentoCategoryProductService = new MagentoCategoryProductService(mContext);
            mMagentoProductService = new MagentoProductService(mContext);
        }else {
            mCategoryProductService = new CategoryProductService(mContext);
            mProductService = new ProductService(mContext);
        }
    }

    /**
     * Gets category list. Root Category Api
     */
    public void getCategoryList() {

        if (isCategoty || isHome || isProductSettings) {
            rootApiCall();
        } else if (AppConstants.isDbExists(mContext)) {
            mIsCategoryExists = mDatabaseHelper.isItemExists(AppConstants.sCategory, true, null, null);
            try {
                if (mIsCategoryExists) {
                    if (mContext instanceof MainActivity){
                        ((MainActivity) mContext).updateVersion(mCategoryService.retrieveCategoryList(),
                                mBannerService.retrieveHomeBannerList(), mCmsService.retrieveCmsList());
                    }else if (mContext instanceof SettingActivity){
                        ((MainActivity) mContext).updateVersion(mCategoryService.retrieveCategoryList(),
                                mBannerService.retrieveHomeBannerList(), mCmsService.retrieveCmsList());
                    }
                } else {
                    rootApiCall();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            rootApiCall();
        }
    }

    public void rootApiCall() {
        Map<String, String> params = new HashMap<String, String>();
        String cartId = SharedPreference.getInstance().getValue("cart_id");
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            params.put("logout_reference", "0");
            params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
            if (cartId != null) {
                params.put("cart_id", cartId);
            } else {
                params.put("cart_id", "0");
            }
            MyApplication.magentoNetworkCall(AppConstants.sMagentoRootCategories, params, mCategoriesResponse);
        } else {
            params.put("action", AppConstants.sRootCategories);
            params.put("shop_id", "1");
            MyApplication.NetworkCall(AppConstants.sCategoriesController, params, mCategoriesResponse);
        }

    }

    /**
     * Gets sub category list.
     *
     * @param categoryId the category id
     */
    public void getSubCategoryList(String categoryId) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_category", categoryId);
        params.put("current_page_number", "1");
        params.put("page_limit", AppConstants.spageLimit);

        String key_ref[] = {"parentId"};
        String val_ref[] = {categoryId+ ""};

        boolean categoryExists;
        categoryExists = mDatabaseHelper.isItemExists("ExpandCategory", true, key_ref, val_ref);
        Log.e(" Is Subcategory exists == ", categoryExists+"");
        if (categoryExists) {
            try {
                mExpandCategory = mCategoryService.retrieveSubCategory(categoryId);
                mCategoryProducts = mCategoryProductService.categoryProductList(categoryId, "1");
                ((SubCategoryActivity) mContext).subCategoryDbUpdate(mExpandCategory, mCategoryProducts);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }else {
            if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                MyApplication.magentoNetworkCall(AppConstants.sMagentoSubCategories, params, mSubCategoriesResponse);
            }else {
                params.put("action", AppConstants.sSubCategories);
                MyApplication.NetworkCall(AppConstants.sCategoriesController, params, mSubCategoriesResponse);
            }
        }
    }

    /**
     * Update version.
     */
    public void updateVersion() {
        String cartId = SharedPreference.getInstance().getValue("cart_id");
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        if (cartId != null) {
            params.put("cart_id", cartId);
        } else {
            params.put("cart_id", "0");
        }

        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            //params.put("store_id", "1");
            MyApplication.magentoNetworkCall(AppConstants.sMagentoUpdateCart, params, mUpdateCartInfo);
        } else {
            params.put("action", AppConstants.sUpdateCartInfo);
            params.put("logout_reference", "0");
            MyApplication.NetworkCall(AppConstants.sCategoriesController, params, mUpdateCartInfo);
        }
    }

    /**
     * Subzero home.
     *
     * @param mSubzeroHome the m subzero home
     * @param action       the action
     * @param currentpage  the currentpage
     */
    public void subzeroHome(VolleyCallback mSubzeroHome, String action, int currentpage) {
        String url = AppConstants.sCartInfo;
        Map<String, String> params = new HashMap<String, String>();
        params.put("action", action);
        params.put("current_page_number", String.valueOf(currentpage));
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        params.put("page_limit", AppConstants.sSubzeroHomePagelimit);
        MyApplication.NetworkCall(url, params, mSubzeroHome);
    }

    // Rootcategories Call back response
    private VolleyCallback mCategoriesResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {

            try {
                JSONObject jsonObject = new JSONObject(response);
                mHomePageResponse = (MyApplication.getGsonInstance().fromJson(response, HomePage.class));

                /**
                 * Homepage list data store to SharedPreference,  concept*/
                String homePageValues = MyApplication.getGsonInstance().toJson(mHomePageResponse);
                SharedPreference.getInstance().save("homePageList", homePageValues);

                if (mHomePageResponse.getStatus().equals("true")) {
                    databaseInsert(true);
                    sharedPreferences();
                    storeIsoCode(mHomePageResponse);
                    ((MainActivity) mContext).homePageSuccess(mHomePageResponse);
                } else {
                    ((MainActivity) mContext).homePageFailure(jsonObject.getString("errormsg"));
                }

            } catch (Exception e) {
                e.printStackTrace();
                ((MainActivity) mContext).homePageFailure("");
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((MainActivity) mContext).homePageFailure(errorResponse);
        }
    };

    // Subcategories Call back response
    private VolleyCallback mSubCategoriesResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            ((SubCategoryActivity) mContext).subCategoryUpdate(response);
        }

        @Override
        public void Failure(String errorResponse) {
            //((SubCategoryActivity) mContext).subCategoryFailure(mContext.getString(R.string.internal_server_error));
            ((SubCategoryActivity) mContext).subCategoryFailure(errorResponse);
        }
    };

    // Update Cart info
    private VolleyCallback mUpdateCartInfo = new VolleyCallback() {
        @Override
        public void Success(String response) {

            try {
                JSONObject jsonObject = new JSONObject(response);
                mUpdateVersion = (MyApplication.getGsonInstance().fromJson(response, UpdateVersion.class));

                if (mUpdateVersion.getStatus().equalsIgnoreCase("false")) {
                    ((MainActivity) mContext).homePageFailure(jsonObject.getString("errormsg"));
                } else {
                    databaseInsert(false);

                    String prefHomePage = SharedPreference.getInstance().getValue("homepage_version");
                    String prefCatalog = SharedPreference.getInstance().getValue("catalog_version");
                    String prefSettings = SharedPreference.getInstance().getValue("product_setting_version");
                    updateSharedPreferences();
                    String homePageVersion = mUpdateVersion.getVersion().getHomepageVersion();
                    String catalogVersion = mUpdateVersion.getVersion().getCatalogVersion();
                    String settingsVersion = mUpdateVersion.getVersion().getProductSettingVersion();

                    /*if (!prefCatalog
                            .equalsIgnoreCase(catalogVersion) || !prefHomePage
                            .equalsIgnoreCase(homePageVersion) || !prefSettings.equalsIgnoreCase(settingsVersion)) {

                        mCategoryService.deleteCategory();
                        mBannerService.deleteBanner();
                        mLangService.deleteLanguage();
                        mCurrencyService.deleteCurrency();
                        mCmsService.deleteCms();
                        mTranslationService.deleteMultipleLanguage();
                        getCategoryList();
                    } else {
                        ((MainActivity) mContext).updateVersion(mCategoryService.retrieveCategoryList(),
                                mBannerService.retrieveHomeBannerList(), mCmsService.retrieveCmsList());
                    }*/

                    if (!prefCatalog.equalsIgnoreCase(catalogVersion)) {
                        Log.e("Category Version ==== ", "Changed");
                        isCategoty = true;
                        mCategoryService.deleteCategory();
                        mLangService.deleteLanguage();
                        mCurrencyService.deleteCurrency();
                        mCmsService.deleteCms();
                        if(AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)){
                            mMagentoCategoryProductService.deleteCategoryProductServiceRecord();
                            mMagentoProductService.deleteProductServiceRecord();
                        }else {
                            mCategoryProductService.deleteCategoryProductServiceRecord();
                            mProductService.deleteProductServiceRecord();
                        }
                        getCategoryList();
                    } else if (!prefHomePage.equalsIgnoreCase(homePageVersion)) {
                        Log.e("Home Version ==== ", "Changed");
                        isHome = true;
                        mBannerService.deleteBanner();
                        mTranslationService.deleteMultipleLanguage();
                        getCategoryList();
                    } else if (!prefSettings.equalsIgnoreCase(settingsVersion)) {
                        Log.e("Products Settings Versions === ", "Changed");
                        isProductSettings = true;
                        mCurrencyService.deleteCurrency();
                        mCmsService.deleteCms();
                        getCategoryList();
                    } else if (prefCatalog.equals(catalogVersion) && prefHomePage.equals(homePageVersion) &&
                            prefSettings.equals(settingsVersion)) {
                        Log.e("Versions ==== ", "No Changed");
                        if (isLangCurrency) {
                            Log.e("Language and Currency", "Changed");
                            mTranslationService.deleteMultipleLanguage();
                            mLangService.deleteLanguage();
                            mCurrencyService.deleteCurrency();
                            mCategoryService.deleteCategory();
                            mBannerService.deleteBanner();
                            mCmsService.deleteCms();
                            isLangCurrency = false;
                            getCategoryList();
                        } else {
                            ((MainActivity) mContext).updateVersion(mCategoryService.retrieveCategoryList(),
                                    mBannerService.retrieveHomeBannerList(), mCmsService.retrieveCmsList());
                        }
                    } else {
                        ((MainActivity) mContext).updateVersion(mCategoryService.retrieveCategoryList(),
                                mBannerService.retrieveHomeBannerList(), mCmsService.retrieveCmsList());
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((MainActivity) mContext).homePageFailure(errorResponse);
        }
    };


    // Category & Banner & Cms db insert
    private void databaseInsert(boolean isVersion) {
        try {

            /*if (isVersion) {
                SharedPreference.getInstance().save("logo", mHomePageResponse.getResult().getLogo());
                mCategoryService.insertCategory(mHomePageResponse.getResult().getCategories());
                mBannerService.insertHomeBanner(mHomePageResponse.getResult().getHomePageList());
                mCmsService.insertCmsPage(mHomePageResponse.getResult().getCmsList());
                mLangService.insertLanguage(mHomePageResponse.getResult().getLanguages());
                mCurrencyService.insertCurrency(mHomePageResponse.getResult().getCurrency());
                mTranslationService.insertTranslations(mHomePageResponse.getResult().getMultipleLang());
            } else {
                //mCmsService.insertCmsPage(mUpdateVersion.getVersion().getCmsList());
                //mLangService.insertLanguage(mUpdateVersion.getVersion().getLanguages());
                //mCurrencyService.insertCurrency(mUpdateVersion.getVersion().getCurrency());
            }*/

            if (isVersion) {
                SharedPreference.getInstance().save("logo", mHomePageResponse.getResult().getLogo());
                if (!isCategoty && !isHome && !isProductSettings) {
                    Log.e("All DB's Deleted ==== ", "Success");
                    mCategoryService.insertCategory(mHomePageResponse.getResult().getCategories());
                    mBannerService.insertHomeBanner(mHomePageResponse.getResult().getHomePageList());
                    mCmsService.insertCmsPage(mHomePageResponse.getResult().getCmsList());
                    mLangService.insertLanguage(mHomePageResponse.getResult().getLanguages());
                    mCurrencyService.insertCurrency(mHomePageResponse.getResult().getCurrency());
                    mTranslationService.insertTranslations(mHomePageResponse.getResult().getMultipleLang());
                } else if (isCategoty) {
                    Log.e("Category DB delete", "Success");
                    mCategoryService.insertCategory(mHomePageResponse.getResult().getCategories());
                    mCmsService.insertCmsPage(mHomePageResponse.getResult().getCmsList());
                    mLangService.insertLanguage(mHomePageResponse.getResult().getLanguages());
                    mCurrencyService.insertCurrency(mHomePageResponse.getResult().getCurrency());
                } else if (isProductSettings) {
                    Log.e("SharedPreference ==== ", "Success");
                    mCmsService.insertCmsPage(mHomePageResponse.getResult().getCmsList());
                    mCurrencyService.insertCurrency(mHomePageResponse.getResult().getCurrency());
                    sharedPreferences();
                } else if (isHome) {
                    Log.e("Banners and Translation", "Success");
                    mBannerService.insertHomeBanner(mHomePageResponse.getResult().getHomePageList());
                    mTranslationService.insertTranslations(mHomePageResponse.getResult().getMultipleLang());
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    // Storing shared preferences value
    private void sharedPreferences() {
        SharedPreference mPref = SharedPreference.getInstance();
        mPref.save("zopim_id", mHomePageResponse.getResult().getZopimId());
        mPref.save("cart_count", mHomePageResponse.getResult().getCartCount());
        if(SharedPreference.getInstance().getValue("currency_symbol").equals("0")){
            mPref.save("currency_symbol", mHomePageResponse.getResult().getCurrencySymbol());
        }
        mPref.save("catalog_version", mHomePageResponse.getResult().getCatalogVersion());
        mPref.save("homepage_version", mHomePageResponse.getResult().getHomepageVersion());
        mPref.save("product_setting_version", mHomePageResponse.getResult().getProductSettingVersion());
        if (mHomePageResponse.getResult().getProductListLayout() != null || mHomePageResponse.getResult().getProductListLayout().isEmpty()){
            mPref.save("product_list_layout", mHomePageResponse.getResult().getProductListLayout());
        }else {
            mPref.save("product_list_layout", "2");
        }
        mPref.save("tax_display", mHomePageResponse.getResult().getTaxDisplay());
        if (SharedPreference.getInstance().getValue("id_language").equals("0")) {
            mPref.save("id_language", mHomePageResponse.getResult().getDefaultLang());//default_lang
        }
        if (SharedPreference.getInstance().getValue("id_currency").equals("0")) {
            mPref.save("id_currency", mHomePageResponse.getResult().getDefaultCurrency());//default_currency
        }
        mPref.save("google_tracking", mHomePageResponse.getResult().getGoogleTracking());
        mPref.save("paypal_client_id", mHomePageResponse.getResult().getPaypalClientId());
        mPref.save("paypal_secret_key", mHomePageResponse.getResult().getPaypalSecretKey());
        mPref.save("paypal_mode", mHomePageResponse.getResult().getPaypalMode());
        if (mHomePageResponse.getResult().getThemeColor() != null || !mHomePageResponse.getResult().getThemeColor().isEmpty()){
            mPref.save("theme_color", mHomePageResponse.getResult().getThemeColor());
        }else {
            mPref.save("theme_color", "E8E6C5");
        }
        if (mHomePageResponse.getResult().getThemeColor1() !=null || !mHomePageResponse.getResult().getThemeColor1().isEmpty()){
            mPref.save("theme_color1", mHomePageResponse.getResult().getThemeColor1());
        }else {
            mPref.save("theme_color1", "CCC4AB");
        }
        mPref.save("guest_checkout_status", mHomePageResponse.getResult().getGuestCheckoutStatus());
        mPref.save("product_label_status", mHomePageResponse.getResult().getProductLabelStatus());
        mPref.save("wishlist_enable_status", mHomePageResponse.getResult().getWishlistEnableStatus());
        mPref.save("filter_enable_status", mHomePageResponse.getResult().getFilterEnableStatus());
        mPref.save("stock_management_status", mHomePageResponse.getResult().getStockManagementStatus());
        mPref.save("review_rating_status", mHomePageResponse.getResult().getReviewRatingStatus());
        mPref.save("qrcode_scanner_status", mHomePageResponse.getResult().getQrcodeScannerStatus());
        mPref.save("store_locator_status", mHomePageResponse.getResult().getStoreLocatoreStatus());
        mPref.save("reorder_status", mHomePageResponse.getResult().getReorderStatus());
        for (int i = 0; i < mHomePageResponse.getResult().getCurrency().size(); i++) {
            if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                if (mHomePageResponse.getResult().getCurrency().get(i).getIsoCode()
                        .equalsIgnoreCase(SharedPreference.getInstance().getValue("id_currency")))
                    mPref.save("currency_iso_code", mHomePageResponse.getResult().getCurrency().get(i).getIsoCode());
            } else {
                if (mHomePageResponse.getResult().getCurrency().get(i).getIdCurrency()
                        .equalsIgnoreCase(SharedPreference.getInstance().getValue("id_currency")))
                    mPref.save("currency_iso_code", mHomePageResponse.getResult().getCurrency().get(i).getIsoCode());
            }
        }
        mPref.save("call_back_number", mHomePageResponse.getResult().getCallBackNumber());
        mPref.save("coupon_section_display", mHomePageResponse.getResult().getCouponSectionDisplay());
        mPref.saveBoolean("search_below_header", mHomePageResponse.getResult().getSearchBelowHeader());
        mPref.save("cart_section_display", mHomePageResponse.getResult().getCartSectionDisplay());
        mPref.saveBoolean("is_version", true);
        if (mHomePageResponse.getResult().getTextColor() != null || !mHomePageResponse.getResult().getTextColor().isEmpty()){
            mPref.save("text_color", mHomePageResponse.getResult().getTextColor());
        }else {
            mPref.save("text_color", "FFFFFF");
        }
        if (mHomePageResponse.getResult().getTintColor() != null || !mHomePageResponse.getResult().getTintColor().isEmpty()){
            mPref.save("tint_color", mHomePageResponse.getResult().getTintColor());
        }else {
            mPref.save("tint_color", "59554C");
        }
        CustomBackground.mThemeColor = "#" + SharedPreference.getInstance().getValue("theme_color1");
        CustomBackground.mTitleTextColor = "#" + SharedPreference.getInstance().getValue("text_color");
        CustomBackground.mTintColor = "#" + SharedPreference.getInstance().getValue("tint_color");

    }


    // Storing shared preferences value
    private void updateSharedPreferences() throws Exception {
        SharedPreference mPref = SharedPreference.getInstance();
        mPref.save("cart_count", mUpdateVersion.getVersion().getCartCount() + "");
        mPref.save("catalog_version", mUpdateVersion.getVersion().getCatalogVersion());
        mPref.save("homepage_version", mUpdateVersion.getVersion().getHomepageVersion());
        mPref.save("product_setting_version", mUpdateVersion.getVersion().getProductSettingVersion());
    }

    /**
     * Language ISO_CODE store to SharedPreference
     *
     * @param mHomePageResponse
     */
    private void storeIsoCode(HomePage mHomePageResponse) {

        String defaultId = mHomePageResponse.getResult().getDefaultLang();
        List<Language> languages = mHomePageResponse.getResult().getLanguages();
        for (int i = 0; i < languages.size(); i++) {
            Language language = languages.get(i);
            if (language.getIdLang().equals(defaultId)) {
                if (SharedPreference.getInstance().getValue("iso_code").equals("0")) {
                    SharedPreference.getInstance().save("iso_code", language.getIsoCode());
                }
                // custom language
                Locale locale = new Locale(language.getIsoCode());
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                mContext.getResources().updateConfiguration(config, mContext.getResources().getDisplayMetrics());
            }
        }

    }

    /**
     * Delete Language and Currency Values from SharedPreferenc
     */
    private void deleteLangCurrencyValues() {
        SharedPreference.getInstance().removeValue("iso_code");
        SharedPreference.getInstance().removeValue("language_name");
        SharedPreference.getInstance().removeValue("id_language");
        SharedPreference.getInstance().removeValue("language_position");
        SharedPreference.getInstance().removeValue("currency_symbol");
        SharedPreference.getInstance().removeValue("id_currency");
        SharedPreference.getInstance().removeValue("currency_name");
        SharedPreference.getInstance().removeValue("currency_position");
        SharedPreference.getInstance().removeValue("currency_iso_code");
    }

    /**
     * Load json from asset string.
     *
     * @return the string
     */
    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = mContext.getAssets().open("jsonconverted_22_02.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

}
