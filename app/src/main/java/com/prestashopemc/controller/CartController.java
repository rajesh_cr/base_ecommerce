package com.prestashopemc.controller;

import android.content.Context;

import com.prestashopemc.model.CartDeleteModel;
import com.prestashopemc.model.CartMainModel;
import com.prestashopemc.model.CartQuantityModel;
import com.prestashopemc.model.CouponMainModel;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.VolleyCallback;
import com.prestashopemc.view.CartActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * <h1>Cart Controller!</h1>
 * The Cart Controller to form the request and response for cart Api call.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class CartController {
    private Context mContext;
    private CartMainModel mCartMainModel;
    private CartQuantityModel mCartQuantityModel;
    private CartDeleteModel mCartDeleteModel;
    private String mErrorMsg, mQuantity, mQuantityPosition;
    private CouponMainModel mCouponMainModel;

    /**
     * Instantiates a new Cart controller.
     *
     * @param con the con
     */
    public CartController(Context con) {
        this.mContext = con;
    }

    /**
     * Cart info.
     */
    public void cartInfo() {
        String cartId = SharedPreference.getInstance().getValue("cart_id");
        Map<String, String> params = new HashMap<String, String>();
        params.put("cart_id", cartId);

        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)){
            String url = AppConstants.sMagentoCartInfo;
            MyApplication.magentoNetworkCall(url, params, cartResponse);
        }else {
            String url = AppConstants.sCartInfo;
            params.put("action", AppConstants.sCartAction);
            MyApplication.NetworkCall(url, params, cartResponse);
        }
    }

    /**
     * The Cart response.
     */
    VolleyCallback cartResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;

                jsonObject = new JSONObject(response);
                mCartMainModel = (MyApplication.getGsonInstance().fromJson(response, CartMainModel.class));
                if (mCartMainModel.getStatus().equalsIgnoreCase("true")) {
                    ((CartActivity) mContext).updateView(mCartMainModel);
                } else {
                    ((CartActivity) mContext).failure(jsonObject.getString("errormsg"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            //((CartActivity) con).failure(con.getString(R.string.internal_server_error));
            ((CartActivity) mContext).failure(errorResponse);
        }
    };

    /**
     * Cart item delete.
     *
     * @param productId          the product id
     * @param productAttributeId the product attribute id
     */
/*Cart Item Delete API*/
    public void cartItemDelete(String productId, String productAttributeId) {
        String cartId = SharedPreference.getInstance().getValue("cart_id");
        Map<String, String> params = new HashMap<String, String>();
        params.put("cart_id", cartId);
        params.put("id_product", productId);
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)){
            String url = AppConstants.sMagentoCartRemove;
            MyApplication.magentoNetworkCall(url, params, cartDeleteResponse);
        }else {
            String url = AppConstants.sCartInfo;
            params.put("action", AppConstants.sCartDeletAction);
            params.put("store_id", "1");
            params.put("id_product_attribute", productAttributeId);
            params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
            MyApplication.NetworkCall(url, params, cartDeleteResponse);
        }
    }

    /**
     * The Cart delete response.
     */
    VolleyCallback cartDeleteResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                mCartDeleteModel = (MyApplication.getGsonInstance().fromJson(response, CartDeleteModel.class));
                if (mCartDeleteModel.getStatus().equalsIgnoreCase("true")) {
                    ((CartActivity) mContext).updateDeleteView(mCartDeleteModel);
                } else {
                    ((CartActivity) mContext).failure(AppConstants.getTextString(mContext, AppConstants.internalServerErrorText));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((CartActivity) mContext).failure(errorResponse);
        }
    };

    /**
     * Cart quantity update API
     *
     * @param mquantity          the mquantity
     * @param productId          the product id
     * @param productAttributeId the product attribute id
     */
    public void cartQuantityUpdate(String mquantity, String productId, String productAttributeId, String itemId) {
        String cartId = SharedPreference.getInstance().getValue("cart_id");
        mQuantity = mquantity;
        Map<String, String> params = new HashMap<String, String>();
        params.put("cart_id", cartId);
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)){
            JSONObject qtyObject = new JSONObject();
            JSONObject cartObject = new JSONObject();
            try {
                qtyObject.put("qty",mquantity);
                cartObject.put(itemId, qtyObject);
            } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            }
            String url = AppConstants.sMagentoCartQntyUpdate;
            params.put("cart", cartObject.toString());
            MyApplication.magentoNetworkCall(url, params, cartQuantityResponse);
        }else {
            String url = AppConstants.sCartInfo;
            params.put("action", AppConstants.sCartQuantityAction);
            params.put("quantity", mquantity);
            params.put("id_product", productId);
            params.put("id_product_attribute", productAttributeId);
            MyApplication.NetworkCall(url, params, cartQuantityResponse);
        }
    }

    /**
     * The Cart quantity response.
     */
    VolleyCallback cartQuantityResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;

                jsonObject = new JSONObject(response);
                mCartQuantityModel = (MyApplication.getGsonInstance().fromJson(response, CartQuantityModel.class));
                if (mCartQuantityModel.getStatus().equalsIgnoreCase("true")) {
                    ((CartActivity) mContext).updateQuantityView(mCartQuantityModel, mQuantity);
                } else {
                    mErrorMsg = jsonObject.getString("errormsg");
                    ((CartActivity) mContext).quantityFailure(mErrorMsg);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((CartActivity) mContext).quantityFailure(mErrorMsg);
        }
    };

    /**
     * Coupon sumit.
     *
     * @param couponCode the coupon code
     */
/*Cart Coupon Submit API*/
    public void couponSumit(String couponCode) {
        String cartId = SharedPreference.getInstance().getValue("cart_id");
        Map<String, String> params = new HashMap<String, String>();
        params.put("cart_id", cartId);
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)){
            String url = AppConstants.sMagentoCouponAdd;
            params.put("couponcode", couponCode);
            MyApplication.magentoNetworkCall(url, params, couponResponse);
        }else {
            String url = AppConstants.sCartInfo;
            params.put("action", AppConstants.sCouponVoucher);
            params.put("store_id", "1");
            params.put("coupon_id", couponCode);
            MyApplication.NetworkCall(url, params, couponResponse);
        }
    }

    /**
     * The Coupon response.
     */
    VolleyCallback couponResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;

                jsonObject = new JSONObject(response);
                mCouponMainModel = (MyApplication.getGsonInstance().fromJson(response, CouponMainModel.class));
                if (mCouponMainModel.getStatus().equalsIgnoreCase("true")) {
                    ((CartActivity) mContext).updateCouponValues(mCouponMainModel);
                } else {
                    mErrorMsg = jsonObject.getString("errormsg");
                    ((CartActivity) mContext).failure(mErrorMsg);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((CartActivity) mContext).failure(errorResponse);
        }
    };

    /**
     * Coupon remove.
     *
     * @param couponCode the coupon code
     */
/*Cart Coupon Remove API*/
    public void couponRemove(String couponCode) {
        String cartId = SharedPreference.getInstance().getValue("cart_id");
        Map<String, String> params = new HashMap<String, String>();
        params.put("cart_id", cartId);
        params.put("coupon_id", couponCode);
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)){
            String url = AppConstants.sMagentoCouponRemove;
            MyApplication.magentoNetworkCall(url, params, couponCancelResponse);
        }else {
            String url = AppConstants.sCartInfo;
            params.put("action", AppConstants.sCouponCancelVoucher);
            params.put("store_id", "1");
            MyApplication.NetworkCall(url, params, couponCancelResponse);
        }
    }

    /**
     * The Coupon cancel response.
     */
    VolleyCallback couponCancelResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;

                jsonObject = new JSONObject(response);
                mCouponMainModel = (MyApplication.getGsonInstance().fromJson(response, CouponMainModel.class));
                if (mCouponMainModel.getStatus().equalsIgnoreCase("true")) {
                    ((CartActivity) mContext).couponRemoveValues(mCouponMainModel);
                } else {
                    mErrorMsg = jsonObject.getString("errormsg");
                    ((CartActivity) mContext).failure(mErrorMsg);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((CartActivity) mContext).failure(errorResponse);
        }
    };
}

