package com.prestashopemc.controller;

import android.content.Context;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.prestashopemc.R;
import com.prestashopemc.database.CategoryProductService;
import com.prestashopemc.database.DatabaseHelper;
import com.prestashopemc.database.MagentoCategoryProductService;
import com.prestashopemc.database.MagentoProductService;
import com.prestashopemc.database.ProductService;
import com.prestashopemc.model.Attributes;
import com.prestashopemc.model.CartDeleteModel;
import com.prestashopemc.model.CartQuantityModel;
import com.prestashopemc.model.CategoryProduct;
import com.prestashopemc.model.FilterMain;
import com.prestashopemc.model.MagentoCategoryProduct;
import com.prestashopemc.model.MagentoProduct;
import com.prestashopemc.model.MagentoProductMain;
import com.prestashopemc.model.NavigationFilterMain;
import com.prestashopemc.model.Option;
import com.prestashopemc.model.PredictiveSearchModel;
import com.prestashopemc.model.ProductMain;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.FragmentInterface;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.VolleyCallback;
import com.prestashopemc.view.FilterActivity;
import com.prestashopemc.view.MagentoProductDetailActivity;
import com.prestashopemc.view.MainActivity;
import com.prestashopemc.view.ProductDetailActivity;
import com.prestashopemc.view.ProductListActivity;
import com.prestashopemc.view.RecentSearchActivity;
import com.prestashopemc.view.SearchActivity;
import com.prestashopemc.view.SearchFilterActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <h1>Product List Controller!</h1>
 * The Product List Controller to form the request and response for Product List Api call.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class ProductListController {

    /**
     * The M context.
     */
//    public SubzeroApplication mApplication;
    public Context mContext;
    /**
     * The M product service.
     */
    public ProductService mProductService;

    public MagentoProductService mMagentoProductService;

    /**
     * The M category product service.
     */
    public CategoryProductService mCategoryProductService;


    public MagentoCategoryProductService mMagentoCategoryProductService;
    /**
     * The M page number.
     */
    public String mPageNumber, /**
     * The M product id.
     */
    mProductId;
    /**
     * The M database helper.
     */
    public DatabaseHelper mDatabaseHelper;
    /**
     * The M is db exists.
     */
    public boolean mIsDbExists, /**
     * The M wish list status.
     */
    mWishListStatus;
    /**
     * The M product list.
     */
    public List<CategoryProduct> mProductList = new ArrayList<CategoryProduct>();

    public List<MagentoCategoryProduct> mMagentoProductList = new ArrayList<MagentoCategoryProduct>();
    /**
     * The M related view.
     */
    public FragmentInterface mRelatedView;
    private String mCategoryId, mCurrentPage, mCurrentpage, mSortType, mSortBy, mIdCategory, mProductName, mErrorMsg, mQuantity, mQuantityPosition;
    private FilterMain mFilterMainObject;
    private CartDeleteModel mCartDeleteModel;
    private CategoryProduct mCategoryProduct;
    private MagentoCategoryProduct mMagentoCategoryProduct;
    private CartQuantityModel mCartQuantityModel;
    private PredictiveSearchModel mPredictiveSearchModel;
    private long productRows;
    private List<CategoryProduct> categoryProducts = new ArrayList<>();
    private List<MagentoCategoryProduct> magentoCategoryProducts = new ArrayList<MagentoCategoryProduct>();
    /**
     * Instantiates a new Product list controller.
     *
     * @param ctx the ctx
     */
    public ProductListController(Context ctx) {
        this.mContext = ctx;
        mProductService = new ProductService(ctx);
        mMagentoProductService = new MagentoProductService(ctx);
        mCategoryProductService = new CategoryProductService(ctx);
        mMagentoCategoryProductService = new MagentoCategoryProductService(ctx);
    }

    /**
     * Instantiates a new Product list controller.
     *
     * @param ctx         the ctx
     * @param relatedView the related view
     */
    public ProductListController(Context ctx, FragmentInterface relatedView) {
        this.mContext = ctx;
        this.mRelatedView = relatedView;
        mProductService = new ProductService(ctx);
        mMagentoProductService = new MagentoProductService(ctx);
    }

    /**
     * ProductList api Call
     *
     * @param currentPage the current page
     * @param categoryId  the category id
     */
    public void getProductList(String currentPage, String categoryId) {

        this.mCategoryId = categoryId;
        this.mCurrentPage = currentPage;
        mDatabaseHelper = new DatabaseHelper(mContext);
        mPageNumber = currentPage;
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_category", mCategoryId);
        params.put("current_page_number", currentPage);
        params.put("page_limit", AppConstants.spageLimit);
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));

        mIsDbExists = mDatabaseHelper.doesDatabaseExist(mContext, DatabaseHelper.DATABASE_NAME);

        if (mIsDbExists) {
            String key_ref[] = {"categoryId", "currentPage"};
            String val_ref[] = {categoryId + "", currentPage};

            boolean categoryExists;
            if (AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
                categoryExists = mDatabaseHelper.isItemExists("MagentoCategoryProduct", true, key_ref, val_ref);
            }else {
                categoryExists = mDatabaseHelper.isItemExists("CategoryProduct", true, key_ref, val_ref);
            }

            if (categoryExists) {
                try {

                    if (AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
                        mMagentoProductList = mMagentoCategoryProductService.magentoCategoryProductList(mCategoryId, mPageNumber);
                        ((ProductListActivity) mContext).updateMagentoDatabaseView(mMagentoProductList);
                    } else {
                        mProductList = mCategoryProductService.categoryProductList(mCategoryId, mPageNumber);
                        ((ProductListActivity) mContext).updateDatabaseView(mProductList);
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    if (AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
                        productRows = mMagentoCategoryProductService.getTotalRows();
                        magentoCategoryProducts = mMagentoCategoryProductService.getFirstTenProducts();
                    }else {
                        productRows = mCategoryProductService.getTotalRows();
                        categoryProducts = mCategoryProductService.getFirstTenProducts();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if ((productRows + AppConstants.productsPerPage) > AppConstants.totalProductsRow) {
                    if (AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
                        mMagentoCategoryProductService.deleteParticularRecord(magentoCategoryProducts);
                    }else {
                        mCategoryProductService.deleteParticularRecord(categoryProducts);
                    }
                }
                apiCall(params);
            }
        } else {
            try {
                if (AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
                    productRows = mMagentoCategoryProductService.getTotalRows();
                    magentoCategoryProducts = mMagentoCategoryProductService.getFirstTenProducts();
                }else {
                    productRows = mCategoryProductService.getTotalRows();
                    categoryProducts = mCategoryProductService.getFirstTenProducts();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            if ((productRows + AppConstants.productsPerPage) > AppConstants.totalProductsRow) {
                if (AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
                    mMagentoCategoryProductService.deleteParticularRecord(magentoCategoryProducts);
                }else {
                    mCategoryProductService.deleteParticularRecord(categoryProducts);
                }
            }
            apiCall(params);
        }
    }

    private void apiCall(Map<String, String> params) {
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            params.put("cart_id", SharedPreference.getInstance().getValue("cart_id"));
            MyApplication.magentoNetworkCall(AppConstants.sMagentoProductList, params, productListResponse);
        } else {
            params.put("store_id", AppConstants.sStoreId);
            params.put("action", AppConstants.sProductList);
            MyApplication.NetworkCall(AppConstants.sCategoriesController, params, productListResponse);
        }
    }

    /**
     * ProductList api Response
     */
    VolleyCallback productListResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            ProductMain mProductMainObject = new ProductMain();
            if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {

                MagentoProductMain mMagentoProductMain = AppConstants.productListParser(response);

                if (mMagentoProductMain.getStatus().equalsIgnoreCase("true")) {
                    try {
                        mMagentoProductService.addProduct(mMagentoProductMain.getMagentoProduct(), mCategoryId, mCurrentPage, SharedPreference.getInstance().getValue("customerid"));
                        ((ProductListActivity) mContext).updateMagentoView(mMagentoProductMain.getMagentoProduct());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    ((ProductListActivity) mContext).updateFailure(mMagentoProductMain.getmErrorMsg());
                }

            } else {
                mProductMainObject = (MyApplication.getGsonInstance().
                        fromJson(response, ProductMain.class));
                if (mProductMainObject.getStatus().equalsIgnoreCase("true")) {
                    try {
                        mProductService.addProduct(mProductMainObject.getmProduct(), mCategoryId, mCurrentPage, SharedPreference.getInstance().getValue("customerid"));
                        ((ProductListActivity) mContext).updateView(mProductMainObject.getmProduct());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    ((ProductListActivity) mContext).updateFailure(mProductMainObject.getmErrorMsg());
                }
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((ProductListActivity) mContext).updateFailure(errorResponse);
        }
    };

    /**
     * Cart quantity update.
     *
     * @param mquantity          the mquantity
     * @param productId          the product id
     * @param productAttributeId the product attribute id
     * @param itemId             the pos
     */
/*Cart Quantity Update API*/
    public void cartQuantityUpdate(String mquantity, String productId, String productAttributeId, String itemId) {
        String cartId = SharedPreference.getInstance().getValue("cart_id");
        mQuantity = mquantity;
        mQuantityPosition = itemId;
        Map<String, String> params = new HashMap<String, String>();
        params.put("cart_id", cartId);
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            JSONObject qtyObject = new JSONObject();
            JSONObject cartObject = new JSONObject();
            try {
                qtyObject.put("qty", mquantity);
                cartObject.put(itemId, qtyObject);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            String url = AppConstants.sMagentoCartQntyUpdate;
            params.put("cart", cartObject.toString());
            MyApplication.magentoNetworkCall(url, params, cartQuantityResponse);
        } else {
            String url = AppConstants.sCartInfo;
            params.put("action", AppConstants.sCartQuantityAction);
            params.put("quantity", mquantity);
            params.put("id_product", productId);
            params.put("id_product_attribute", productAttributeId);
            MyApplication.NetworkCall(url, params, cartQuantityResponse);
        }

    }

    /**
     * The Cart quantity response.
     */
    VolleyCallback cartQuantityResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;

                jsonObject = new JSONObject(response);
                mCartQuantityModel = (MyApplication.getGsonInstance().fromJson(response, CartQuantityModel.class));
                if (mCartQuantityModel.getStatus().equalsIgnoreCase("true")) {
                    if (mContext instanceof ProductListActivity)
                        ((ProductListActivity) mContext).updateQuantityView(mCartQuantityModel, mQuantity, mQuantityPosition);
                    else if (mContext instanceof SearchActivity)
                        ((SearchActivity) mContext).updateQuantityView(mCartQuantityModel, mQuantity, mQuantityPosition);
                } else {
                    mErrorMsg = jsonObject.getString("errormsg");
                    if (mContext instanceof ProductListActivity)
                        ((ProductListActivity) mContext).failure(mErrorMsg);
                    else if (mContext instanceof SearchActivity)
                        ((SearchActivity) mContext).failure(mErrorMsg);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            if (mContext instanceof ProductListActivity)
                ((ProductListActivity) mContext).failure(errorResponse);
            else if (mContext instanceof SearchActivity)
                ((SearchActivity) mContext).failure(errorResponse);
        }
    };

    /**
     * Add to cart api Call
     *
     * @param productId        the product id
     * @param quantity         the quantity
     * @param productAttribute the product attribute
     * @param productName      the product name
     */
    public void addToCart(String productId, String quantity, String productAttribute, String productName) {

        String cartId = SharedPreference.getInstance().getValue("cart_id");
        mProductName = productName;
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        params.put("id_product", productId);
        params.put("quantity", quantity);
        if (cartId != null) {
            params.put("cart_id", cartId);
        } else {
            params.put("cart_id", "0");
        }
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoAddToCart;
            params.put("simple_product_id", productAttribute);
            MyApplication.magentoNetworkCall(url, params, addToCartResponse);
        } else {
            String url = AppConstants.sCartController;
            params.put("action", AppConstants.sActionCart);
            params.put("store_id", AppConstants.sStoreId);
            params.put("id_product_attribute", productAttribute);
            MyApplication.NetworkCall(url, params, addToCartResponse);
        }
    }

    /**
     * Add to cart api Response
     */
    VolleyCallback addToCartResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {

            try {
                JSONObject jsonObject = null;

                jsonObject = new JSONObject(response);

                if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                    SharedPreference mPref = SharedPreference.getInstance();
                    JSONObject cartObj = jsonObject.getJSONObject("result");
                    String cartid = cartObj.getString("cart_id");
                    int cartcount = cartObj.getInt("cart_count");
                    mPref.save("cart_id", cartid);
                    mPref.save("cart_count", cartcount + "");
                    mPref.save("cart_total", cartObj.getInt("cart_total") + "");
                    if (mContext instanceof ProductListActivity) {
                        ((ProductListActivity) mContext).updateCart(mProductName);
                    } else if (mContext instanceof ProductDetailActivity) {
                        ((ProductDetailActivity) mContext).updateCart(mProductName);
                    } else if (mContext instanceof MainActivity) {
                        ((MainActivity) mContext).updateCart(mProductName);
                    } else if (mContext instanceof SearchActivity) {
                        ((SearchActivity) mContext).updateCart(mProductName);
                    } else if (mContext instanceof MagentoProductDetailActivity) {
                        ((MagentoProductDetailActivity) mContext).updateCart(mProductName);
                    }
                } else {

                    String errorMsg = jsonObject.getString("errormsg");
                    if (mContext instanceof ProductListActivity) {
                        ((ProductListActivity) mContext).updateCartFailure(errorMsg);
                    } else if (mContext instanceof ProductDetailActivity) {
                        ((ProductDetailActivity) mContext).updateCartFailure(errorMsg);
                    } else if (mContext instanceof MainActivity) {
                        ((MainActivity) mContext).homePageFailure(errorMsg);
                    } else if (mContext instanceof SearchActivity) {
                        ((SearchActivity) mContext).updateCartFailure(errorMsg);
                    } else if (mContext instanceof MagentoProductDetailActivity) {
                        ((MagentoProductDetailActivity) mContext).updateCartFailure(errorMsg);
                    }
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        @Override
        public void Failure(String errorResponse) {
            if (mContext instanceof ProductListActivity) {
                ((ProductListActivity) mContext).updateCartFailure(errorResponse);
            } else if (mContext instanceof ProductDetailActivity) {
                ((ProductDetailActivity) mContext).updateCartFailure(errorResponse);
            } else if (mContext instanceof MainActivity) {
                ((MainActivity) mContext).homePageFailure(errorResponse);
            } else if (mContext instanceof SearchActivity) {
                ((SearchActivity) mContext).updateCartFailure(errorResponse);
            }
        }
    };

    /**
     * Sort By Product api
     *
     * @param currentPage the current page
     * @param sortType    the sort type
     * @param sortBy      the sort by
     * @param idCategory  the id category
     */
    public void sortByProduct(String currentPage, String sortType, String sortBy, String idCategory) {
        mCurrentpage = currentPage;
        mSortType = sortType;
        mSortBy = sortBy;
        mIdCategory = idCategory;
        Map<String, String> params = new HashMap<String, String>();
        params.put("current_page_number", currentPage);
        params.put("page_limit", AppConstants.spageLimit);
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        params.put("sorttype", sortType);
        params.put("sortby", sortBy);
        params.put("id_category", idCategory);
        params.put("filtertype", AppConstants.sFilterType);
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoProductListType;
            MyApplication.magentoNetworkCall(url, params, sortByProductList);
        } else {
            String url = AppConstants.sSortByProduct;
            params.put("action", AppConstants.sCategorySort);
            MyApplication.NetworkCall(url, params, sortByProductList);
        }
    }

    /**
     * Sort By Product response
     */
    VolleyCallback sortByProductList = new VolleyCallback() {
        @Override
        public void Success(String response) {


            if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {

                MagentoProductMain mMagentoProductMain = AppConstants.productListParser(response);

                if (mMagentoProductMain.getStatus().equalsIgnoreCase("true")) {
                    try {
                        ((ProductListActivity) mContext).updateMagentoSortView(mMagentoProductMain.getMagentoProduct(), mSortType, mSortBy);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    ((ProductListActivity) mContext).updateFailure(mMagentoProductMain.getmErrorMsg());
                }

            } else {

                ProductMain mProductMainObject = (MyApplication.getGsonInstance().
                        fromJson(response, ProductMain.class));

                if (mProductMainObject.getStatus().equalsIgnoreCase("true")) {
                    try {
                        ((ProductListActivity) mContext).updateSortView(mProductMainObject.getmProduct(), mSortType, mSortBy);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    ((ProductListActivity) mContext).updateFailure(mProductMainObject.getmErrorMsg());
                }
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((ProductListActivity) mContext).updateFailure(errorResponse);
        }
    };

    /**
     * Product filter Attribute api
     *
     * @param categoryId the category id
     */
    public void filterAttributeList(String categoryId) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_category", categoryId);
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoFilterAttribute;
            MyApplication.magentoNetworkCall(url, params, filterAttributeResponse);
        } else {
            String url = AppConstants.sSortByProduct;
            params.put("action", AppConstants.sFilterAction);
            MyApplication.NetworkCall(url, params, filterAttributeResponse);
        }
    }

    /**
     * Product filter Attribute response
     */
    VolleyCallback filterAttributeResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                    mFilterMainObject = (MyApplication.getGsonInstance().fromJson(response, FilterMain.class));
                    if (mFilterMainObject.getStatus().equalsIgnoreCase("true")) {
                        if (mContext instanceof FilterActivity) {
                            ((FilterActivity) mContext).updateFilterView(mFilterMainObject);
                        } else if (mContext instanceof SearchFilterActivity) {
                            ((SearchFilterActivity) mContext).filterAttribueValues(mFilterMainObject);
                        }
                    } else {
                        if (mContext instanceof FilterActivity) {
                            ((FilterActivity) mContext).updateFailure(jsonObject.getString("errormsg"));
                        } else if (mContext instanceof SearchFilterActivity) {
                            ((SearchFilterActivity) mContext).categoryFailure(jsonObject.getString("errormsg"));
                        }
                    }
                }else{
                    NavigationFilterMain navigationFilterMain = (MyApplication.getGsonInstance().fromJson(response, NavigationFilterMain.class));
                    if (navigationFilterMain.getStatus().equalsIgnoreCase("true")) {
                        if (mContext instanceof FilterActivity) {
                            ((FilterActivity) mContext).updateNavigationFilterView(navigationFilterMain);
                        } else if (mContext instanceof SearchFilterActivity) {
                            ((SearchFilterActivity) mContext).NavigationFilterAttribueValues(navigationFilterMain);
                        }
                    } else {
                        if (mContext instanceof FilterActivity) {
                            ((FilterActivity) mContext).updateFailure(jsonObject.getString("errormsg"));
                        } else if (mContext instanceof SearchFilterActivity) {
                            ((SearchFilterActivity) mContext).categoryFailure(jsonObject.getString("errormsg"));
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            if (mContext instanceof FilterActivity) {
                ((FilterActivity) mContext).updateFailure(errorResponse);
            } else if (mContext instanceof SearchFilterActivity) {
                ((SearchFilterActivity) mContext).categoryFailure(errorResponse);
            }
        }
    };

    /**
     * Product filter api
     *
     * @param categoryId  the category id
     * @param currentPage the current page
     * @param filterValue the filter value
     */
    public void produtFilter(String categoryId, String currentPage, JSONObject filterValue) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("current_page_number", currentPage);
        params.put("page_limit", AppConstants.spageLimit);
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        params.put("id_category", categoryId);
        params.put("value", filterValue.toString());

        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoProductFilter;
            MyApplication.magentoNetworkCall(url, params, filterResponse);
        } else {
            String url = AppConstants.sSortByProduct;
            params.put("action", AppConstants.sProductFilter);
            MyApplication.NetworkCall(url, params, filterResponse);
        }
    }

    /**
     * Product navigation filter api
     *
     * @param categoryId  the category id
     * @param currentPage the current page
     * @param featureValue the filter value
     * @param attributeValue the filter value
     * @param conditionValue the filter value
     * @param manufactureValue the filter value
     * @param priceValue the filter value
     * @param weightValue the filter value
     * @param availabilityValue the availability value
     */
    public void produtNavigationFilter(String categoryId, String currentPage, JSONObject featureValue, JSONObject attributeValue,
                                       JSONArray conditionValue, JSONArray manufactureValue, JSONObject priceValue,
                                       JSONObject weightValue, String availabilityValue) {
        String mAvailability = "0";
        if (availabilityValue != null){
            mAvailability = availabilityValue;
        }
        String url = AppConstants.sSortByProduct;
        Map<String, String> params = new HashMap<String, String>();
        params.put("current_page_number", currentPage);
        params.put("page_limit", AppConstants.spageLimit);
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        params.put("id_category", categoryId);
        params.put("action", AppConstants.sProductFilter);
        params.put("attributes", attributeValue.toString());
        params.put("features", featureValue.toString());
        params.put("availability", mAvailability);
        params.put("condition", conditionValue.toString());
        params.put("manufacturers", manufactureValue.toString());
        params.put("weight", weightValue.toString());
        params.put("price", priceValue.toString());
        MyApplication.NetworkCall(url, params, filterResponse);

    }


    /**
     * Filter Api Response
     */
    VolleyCallback filterResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {

            if (response.isEmpty()){
                ((ProductListActivity) mContext).updateFailure(mContext.getString(R.string.noProductsFoundText));
            }else {
                if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {

                    MagentoProductMain mMagentoProductMain = AppConstants.productListParser(response);

                    if (mMagentoProductMain.getStatus().equalsIgnoreCase("true")) {
                        try {
                            ((ProductListActivity) mContext).updateMagentoFilterView(mMagentoProductMain.getMagentoProduct());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        ((ProductListActivity) mContext).updateFailure(mMagentoProductMain.getmErrorMsg());
                    }

                } else {
                    ProductMain mProductMainObject = (MyApplication.getGsonInstance().
                            fromJson(response, ProductMain.class));

                    if (mProductMainObject.getStatus().equalsIgnoreCase("true")) {
                        try {
                            ((ProductListActivity) mContext).updateFilterView(mProductMainObject.getmProduct());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        ((ProductListActivity) mContext).updateFailure(mProductMainObject.getmErrorMsg());
                    }
                }

            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((ProductListActivity) mContext).updateFailure(errorResponse);
        }
    };

    /**
     * Delete item.
     *
     * @param productId          the product id
     * @param productAttributeId the product attribute id
     */
/*Product list item delete API*/
    public void deleteItem(String productId, String productAttributeId) {
        String url = AppConstants.sCartInfo;
        String cartId = SharedPreference.getInstance().getValue("cart_id");
        Map<String, String> params = new HashMap<String, String>();
        params.put("cart_id", cartId);
        params.put("action", AppConstants.sCartDeletAction);
        params.put("id_product", productId);
        params.put("id_product_attribute", productAttributeId);
        params.put("store_id", "1");
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        MyApplication.NetworkCall(url, params, cartDeleteResponse);
    }

    /**
     * The Cart delete response.
     */
    VolleyCallback cartDeleteResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                mCartDeleteModel = (MyApplication.getGsonInstance().fromJson(response, CartDeleteModel.class));
                if (mCartDeleteModel.getStatus().equalsIgnoreCase("true")) {
                    if (mContext instanceof ProductListActivity)
                        ((ProductListActivity) mContext).updateDeleteView(mCartDeleteModel);
                    else if (mContext instanceof SearchActivity)
                        ((SearchActivity) mContext).updateDeleteView(mCartDeleteModel);
                } else {
                    if (mContext instanceof ProductListActivity)
                        ((ProductListActivity) mContext).updateFailure(mContext.getString(R.string.internalServerErrorText));
                    else if (mContext instanceof SearchActivity)
                        ((SearchActivity) mContext).updateFailure(mContext.getString(R.string.internalServerErrorText));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            if (mContext instanceof ProductListActivity)
                ((ProductListActivity) mContext).updateFailure(errorResponse);
            else if (mContext instanceof SearchFilterActivity)
                ((SearchActivity) mContext).updateFailure(errorResponse);
        }
    };

    /**
     * Add wishlist.
     *
     * @param productId      the product id
     * @param actionWishlist the action wishlist
     * @param mProduct       the m product
     */
/*Product Wishlist Add*/
    public void addWishlist(String productId, String actionWishlist, CategoryProduct mProduct,MagentoCategoryProduct magentoCategoryProduct) {
        mProductId = productId;
        mCategoryProduct = mProduct;
        mMagentoCategoryProduct=magentoCategoryProduct;
        String url;
        if (actionWishlist.equals("addwishlist")) {
            url = AppConstants.sMagentoAddWishlist;
            mWishListStatus = true;
        } else {
            url = AppConstants.sMagentoRemoveWishlist;
            mWishListStatus = false;
        }
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_product", productId);
        params.put("quantity", "1");
        params.put("sessionid", SharedPreference.getInstance().getValue("sessionid"));
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));

        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            MyApplication.magentoNetworkCall(url, params, addWishlistResponse);
        } else {
            url = AppConstants.sCartInfo;
            params.put("action", actionWishlist);
            params.put("store_id", "1");
            MyApplication.NetworkCall(url, params, addWishlistResponse);
        }
    }

    /**
     * The Add wishlist response.
     */
    VolleyCallback addWishlistResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                    if (mCategoryProduct != null) {
                        mCategoryProductService.updateValue(mCategoryProduct, SharedPreference.getInstance().getValue("customerid"),
                                mWishListStatus);
                    }
                    if(mMagentoCategoryProduct!=null){
                        mMagentoCategoryProductService.updateMagentoValue(mMagentoCategoryProduct, SharedPreference.getInstance().getValue("customerid"),
                                mWishListStatus);
                    }
                    if (mContext instanceof ProductListActivity) {
                        ((ProductListActivity) mContext).wishlistSuccess(jsonObject.getString("result"));
                    } else if (mContext instanceof ProductDetailActivity) {
                        ((ProductDetailActivity) mContext).wishlistSuccess(jsonObject.getString("result"));
                    } else if (mContext instanceof SearchActivity) {
                        ((SearchActivity) mContext).wishlistSuccess(jsonObject.getString("result"));
                    } else if (mContext instanceof MagentoProductDetailActivity) {
                        ((MagentoProductDetailActivity) mContext).wishlistSuccess(jsonObject.getString("result"));
                    }

                } else {
                    String errorMsg = "";
                    if (jsonObject.has("errormsg")){
                        errorMsg = jsonObject.getString("errormsg");
                    }else if (jsonObject.has("result")){
                        errorMsg = jsonObject.getString("result");
                    }
                    if (mContext instanceof ProductListActivity) {
                        ((ProductListActivity) mContext).wishlistFailure(errorMsg);
                    } else if (mContext instanceof ProductDetailActivity) {
                        ((ProductDetailActivity) mContext).wishlistFailure(errorMsg);
                    } else if (mContext instanceof SearchActivity) {
                        ((SearchActivity) mContext).wishlistFailure(errorMsg);
                    } else if (mContext instanceof MagentoProductDetailActivity) {
                        ((MagentoProductDetailActivity) mContext).wishlistFailure(errorMsg);
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            if (mContext instanceof ProductListActivity) {
                ((ProductListActivity) mContext).failure(errorResponse);
            } else if (mContext instanceof ProductDetailActivity) {
                ((ProductDetailActivity) mContext).failure(errorResponse);
            } else if (mContext instanceof SearchActivity) {
                ((SearchActivity) mContext).failure(errorResponse);
            }
        }
    };

    /**
     * Add wish list.
     *
     * @param productId      the product id
     * @param actionWishlist the action wishlist
     */
/*Home page Product Wishlist Add*/
    public void addWishList(String productId, String actionWishlist) {
        String url = AppConstants.sCartInfo;
        mProductId = productId;
        if (actionWishlist.equals("addwishlist")) {
            mWishListStatus = true;
        } else {
            mWishListStatus = false;
        }
        Map<String, String> params = new HashMap<String, String>();
        params.put("action", actionWishlist);
        params.put("id_product", productId);
        params.put("store_id", "1");
        params.put("quantity", "1");
        params.put("sessionid", SharedPreference.getInstance().getValue("sessionid"));
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        MyApplication.NetworkCall(url, params, homeWishlistResponse);
    }

    /**
     * The Home wishlist response.
     */
    VolleyCallback homeWishlistResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                    if (mContext instanceof MainActivity) {
                        ((MainActivity) mContext).wishlistSuccess(jsonObject.getString("result"));
                    } else if (mContext instanceof SearchActivity) {
                        ((SearchActivity) mContext).wishlistSuccess(jsonObject.getString("result"));
                    }
                } else {
                    if (mContext instanceof MainActivity) {
                        ((MainActivity) mContext).wishlistFailure(jsonObject.getString("errormsg"));
                    } else if (mContext instanceof SearchActivity) {
                        ((SearchActivity) mContext).wishlistFailure(jsonObject.getString("result"));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            if (mContext instanceof MainActivity) {
                ((MainActivity) mContext).wishlistFailure(errorResponse);
            } else if (mContext instanceof SearchActivity) {
                ((SearchActivity) mContext).wishlistFailure(errorResponse);
            }
        }
    };

    /**
     * Predictive Search API @param mKey the m key
     *
     * @param mKey the m key
     */
    public void predictiveSearch(String mKey) {
        Map params = new HashMap();
        params.put("key", mKey);
        if (AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoPredictiveSearch;
            MyApplication.magentoNetworkCall(url, params, predictiveSearchResponse);
        } else {
            String url = AppConstants.sSortByProduct;
            params.put("action", AppConstants.sPredictiveSearch);
            MyApplication.NetworkCall(url, params, predictiveSearchResponse);
        }

    }

    /**
     * The Predictive search response.
     */
    VolleyCallback predictiveSearchResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;

                jsonObject = new JSONObject(response);
                mPredictiveSearchModel = (MyApplication.getGsonInstance().fromJson(response, PredictiveSearchModel.class));
                if (mPredictiveSearchModel.getStatus().equalsIgnoreCase("true")) {
                    if (mContext instanceof RecentSearchActivity)
                        ((RecentSearchActivity) mContext).predictiveSuccess(mPredictiveSearchModel);
                } else {
                    if (mContext instanceof RecentSearchActivity)
                        ((RecentSearchActivity) mContext).predictiveFailure(jsonObject.getString("errormsg"));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            if (mContext instanceof RecentSearchActivity)
                ((RecentSearchActivity) mContext).predictiveFailure(errorResponse);

        }
    };


}
