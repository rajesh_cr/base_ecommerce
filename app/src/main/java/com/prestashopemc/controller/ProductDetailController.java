package com.prestashopemc.controller;

import android.content.Context;
import android.util.Log;

import com.prestashopemc.model.Attributes;
import com.prestashopemc.model.Feature;
import com.prestashopemc.model.MagentoProductDetail;
import com.prestashopemc.model.MagentoProductDetailMain;
import com.prestashopemc.model.MagentoProductMain;
import com.prestashopemc.model.MagentoProductinfo;
import com.prestashopemc.model.Option;
import com.prestashopemc.model.ProductDetailMain;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.VolleyCallback;
import com.prestashopemc.view.MagentoProductDetailActivity;
import com.prestashopemc.view.ProductDetailActivity;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * <h1>Product Detail Controller!</h1>
 * The Product Detail Controller to form the request and response for Product Detail Api call.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class ProductDetailController {
    private Context mContext;
    private ProductDetailMain mProductDetailMainResponse;

    /**
     * Instantiates a new Product detail controller.
     *
     * @param con the con
     */
    public ProductDetailController(Context con) {
        this.mContext = con;
    }

    /**
     * Product detail.
     *
     * @param productId the product id
     */
    public void productDetail(String productId) {

        Map<String, String> params = new HashMap<String, String>();

        params.put("id_product", productId);
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoProductDetail;
            MyApplication.magentoNetworkCall(url, params, productdetailResponse);
        } else {
            String url = AppConstants.sCategoriesController;
            params.put("action", AppConstants.sActionProduct);
            params.put("store_id", "1");
            MyApplication.NetworkCall(url, params, productdetailResponse);
        }
    }

    /**
     * The Productdetail response.
     */
    VolleyCallback productdetailResponse = new VolleyCallback() {
        @Override
        public void Success(String response) {

            try {
                JSONObject jsonObject = new JSONObject(response);

                if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {

                    MagentoProductDetailMain mProdutDetail = productDetailParser(response);

                    if (mProdutDetail.getStatus().equalsIgnoreCase("false")) {
                        ((MagentoProductDetailActivity) mContext).productDetailFailure(jsonObject.getString("errormsg"));
                    } else {
                        ((MagentoProductDetailActivity) mContext).productDetailSuccess(mProdutDetail);
                    }
                } else {
                    mProductDetailMainResponse = (MyApplication.getGsonInstance().fromJson(response, ProductDetailMain.class));
                    if (mProductDetailMainResponse.getStatus().equalsIgnoreCase("false")) {
                        ((ProductDetailActivity) mContext).productDetailFailure(jsonObject.getString("errormsg"));
                    } else {
                        ((ProductDetailActivity) mContext).productDetailSuccess(mProductDetailMainResponse);
                    }
                }


            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        @Override
        public void Failure(String errorResponse) {
            if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                ((MagentoProductDetailActivity) mContext).productDetailFailure(errorResponse);
            }else {
                ((ProductDetailActivity) mContext).productDetailFailure(errorResponse);
            }
        }
    };

    /**
     * Related products.
     *
     * @param productId               the product id
     * @param currentPage             the current page
     * @param relatedProductsResponse the related products response
     */

     /*Related Products API*/
    public void relatedProducts(String productId, String currentPage, VolleyCallback relatedProductsResponse) {
        String url = AppConstants.sCategoriesController;
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_product", productId);
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        params.put("current_page_number", currentPage);
        params.put("page_limit", AppConstants.spageLimit);
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            params.put("cart_id", SharedPreference.getInstance().getValue("cart_id"));
            MyApplication.magentoNetworkCall(AppConstants.sMagentoRelatedProduct, params, relatedProductsResponse);
        } else {
            params.put("action", AppConstants.sRelatedProducts);
            params.put("store_id", "1");
            MyApplication.NetworkCall(url, params, relatedProductsResponse);
        }
    }

    private MagentoProductDetailMain productDetailParser(String response) {

        MagentoProductDetailMain productDetailMain = new MagentoProductDetailMain();
        ArrayList<String> imageList = new ArrayList<>();
        ArrayList<Attributes> attributesList = new ArrayList<>();
        ArrayList<Feature> featureList = new ArrayList<>();
        ArrayList<String> productLabelList = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONObject result = jsonObject.getJSONObject("result");
            JSONArray imageArray = result.getJSONArray("image");
            JSONArray productLabelArray = null;
            if (result.has("product_label")) {
                productLabelArray = result.getJSONArray("product_label");
            }
            JSONObject productInfo = result.getJSONObject("productinfo");
            JSONArray attributesArray = result.getJSONArray("additional_attributes");
            JSONObject keyFeaturesObject = result.getJSONObject("key_features");
            for (int attributes = 0; attributes < attributesArray.length(); attributes++) {
                Feature feature = new Feature();
                JSONObject featureObject = keyFeaturesObject.getJSONObject(attributesArray.get(attributes).toString());
                feature.setName(featureObject.getString("label"));
                feature.setValue(featureObject.getString("value"));
                featureList.add(feature);
            }

            JSONArray keyArray = null;
            if (result.has("key")) {
                keyArray = result.getJSONArray("key");
            }
            for (int image = 0; image < imageArray.length(); image++) {
                imageList.add(imageArray.getString(image));
            }

            if (productLabelArray != null) {
                for (int label = 0; label < productLabelArray.length(); label++) {
                    productLabelList.add(productLabelArray.getString(label));
                }
            }
            if (productInfo.getString("type").equalsIgnoreCase("configurable")) {

                for (int key = 0; key < keyArray.length(); key++) {

                    JSONObject attributeObject = result.getJSONObject("attributes");

                    JSONArray keyValueArray = attributeObject.getJSONArray(keyArray.get(key).toString());

                    for (int keyValue = 0; keyValue < keyValueArray.length(); keyValue++) {
                        ArrayList<String> valueList = new ArrayList<>();
                        ArrayList<Option> optionsList = new ArrayList<>();
                        valueList.clear();
                        optionsList.clear();
                        Attributes attributes = new Attributes();
                        JSONObject keyValueObject = keyValueArray.getJSONObject(keyValue);
                        attributes.setmId(keyValueObject.getString("id"));
                        attributes.setmCode(keyValueObject.getString("code"));
                        attributes.setmLabel(keyValueObject.getString("label"));
                        JSONArray optionArray = keyValueObject.getJSONArray("options");

                        for (int optionValue = 0; optionValue < optionArray.length(); optionValue++) {
                            Option option = new Option();
                            ArrayList<String> productList = new ArrayList<>();
                            JSONObject optionObject = optionArray.getJSONObject(optionValue);
                            JSONArray productArray = optionObject.getJSONArray("products");

                            for (int productValue = 0; productValue < productArray.length(); productValue++) {
                                productList.add(productArray.get(productValue).toString());
                            }
                            option.setId(optionObject.getString("id"));
                            option.setLabel(optionObject.getString("label"));
                            option.setPrice(optionObject.getString("price"));
                            option.setProducts(productList);
                            valueList.add(optionObject.getString("label"));
                            optionsList.add(option);
                             }
                        attributes.setmOptions(optionsList);
                        attributes.setOptionValue(valueList);
                        attributesList.add(attributes);
                    }
                }
            }

            MagentoProductinfo magentoProductinfo = new MagentoProductinfo();
            MagentoProductDetail productDetail = new MagentoProductDetail();
            magentoProductinfo.setIdProduct(productInfo.getString("product_id"));
            magentoProductinfo.setDescription(productInfo.getString("description"));
            magentoProductinfo.setSku(productInfo.getString("sku"));
            magentoProductinfo.setType(productInfo.getString("type"));
            magentoProductinfo.setName(productInfo.getString("name"));
            magentoProductinfo.setDescriptionShort(productInfo.getString("short_description"));
            magentoProductinfo.setPrice(productInfo.getString("price"));
            magentoProductinfo.setSpecialprice(productInfo.getString("special_price"));
            magentoProductinfo.setStock(productInfo.getInt("stock"));
            magentoProductinfo.setManageStock(productInfo.getBoolean("manage_stock"));

            productDetail.setReviewCount(result.getInt("review_count"));
            productDetail.setSku(productInfo.getString("sku"));
            productDetail.setProductInfo(magentoProductinfo);
            productDetail.setAttributesList(attributesList);
            productDetail.setDescription(result.getBoolean("is_description"));
            productDetail.setProductReviews(result.getBoolean("is_product_reviews"));
            productDetail.setWishlist(result.getBoolean("wishlist"));
            productDetail.setProductUrl(result.getString("product_url"));
            productDetail.setImage(imageList);
            productDetail.setProductLabel(productLabelList);
            productDetail.setFeatures(featureList);
            productDetailMain.setprdDetail(productDetail);
            productDetailMain.setStatus(jsonObject.getString("status"));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return productDetailMain;
    }
}
