package com.prestashopemc.controller;

import android.content.Context;

import com.prestashopemc.model.FilterCategory;
import com.prestashopemc.model.MagentoProductMain;
import com.prestashopemc.model.ProductMain;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.VolleyCallback;
import com.prestashopemc.view.ProductListActivity;
import com.prestashopemc.view.SearchActivity;
import com.prestashopemc.view.SearchFilterActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * <h1>Search Controller!</h1>
 * The Search Controller to form the request and response for Search Product Api call.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class SearchController {

    /**
     * The M context.
     */
    public Context mContext;
    private ProductMain mProductMainObject;
    private FilterCategory mFilterCategory;
    private String mSortType, mSortBy;

    /**
     * Instantiates a new Search controller.
     *
     * @param context the context
     */
    public SearchController(Context context) {
        this.mContext = context;
    }


    /**
     * Search Product API @param currentPage the current page
     *
     * @param currentPage the current page
     * @param searchText  the search text
     */
    public void searchProducts(String currentPage, String searchText) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("current_page_number", currentPage);
        params.put("page_limit", AppConstants.spageLimit);
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        params.put("store_id", "1");

        if (AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoProductSearch;
            if(searchText.contains("All products")){
                params.put("id_category", "2");
            }else {
                params.put("keyword", searchText);
            }
            MyApplication.magentoNetworkCall(url, params, searchProductList);
        } else {
            String url = AppConstants.sCategoriesController;
            if(searchText.contains("All products")){
                params.put("action", AppConstants.sSearchProductList);
                params.put("id_category", "2");
            }else {
                params.put("q", AppConstants.stringEncode(searchText));
                params.put("action", AppConstants.sSearchProduct);
            }
            MyApplication.NetworkCall(url, params, searchProductList);
        }
    }

    /**
     * The Search product list.
     */
    VolleyCallback searchProductList = new VolleyCallback() {
        @Override
        public void Success(String response) {

            if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {

                MagentoProductMain mMagentoProductMain = AppConstants.productListParser(response);

                if (mMagentoProductMain.getStatus().equalsIgnoreCase("true")) {
                    try {
                        ((SearchActivity) mContext).searchMagentoSuccess(mMagentoProductMain);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    ((SearchActivity) mContext).searchFailure(mMagentoProductMain.getmErrorMsg());
                }

            } else {
                mProductMainObject = (MyApplication.getGsonInstance().fromJson(response, ProductMain.class));

                if (mProductMainObject.getStatus().equalsIgnoreCase("true")) {
                    try {
                        ((SearchActivity) mContext).searchSuccess(mProductMainObject);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    ((SearchActivity) mContext).searchFailure(mProductMainObject.getmErrorMsg());
                }
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((SearchActivity) mContext).searchFailure(errorResponse);
        }
    };

    /**
     * Search Firlter Categories @param searchKeyword the search keyword
     *
     * @param searchKeyword the search keyword
     */
    public void filterCategories(String currentPage, String searchKeyword) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        params.put("store_id", "1");
        if (AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoKeywordCategory;
            params.put("q", searchKeyword);
            params.put("'current_page_number'", currentPage);
            MyApplication.magentoNetworkCall(url, params, searchFilterCategories);
        } else {
            String url = AppConstants.sSearchFilter;
            params.put("action", AppConstants.sSearchFilterAction);
            params.put("q", AppConstants.stringEncode(searchKeyword));
            MyApplication.NetworkCall(url, params, searchFilterCategories);
        }

    }

    /**
     * The Search filter categories.
     */
    VolleyCallback searchFilterCategories = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                mFilterCategory = (MyApplication.getGsonInstance().fromJson(response, FilterCategory.class));
                if (mFilterCategory.getStatus().equalsIgnoreCase("true")) {
                    ((SearchFilterActivity) mContext).categorySuccess(mFilterCategory);
                } else {
                    ((SearchFilterActivity) mContext).categoryFailure(AppConstants.getTextString(mContext, AppConstants.internalServerErrorText));
                }
            } catch (Exception e) {
                e.printStackTrace();
                ((SearchFilterActivity) mContext).categoryFailure("No Categories Found");
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((SearchFilterActivity) mContext).categoryFailure(errorResponse);
        }
    };

    /**
     * Search filter submit API @param category the category
     *
     * @param category    the category
     * @param value       the value
     * @param currentPage the current page
     * @param keyWord the product name
     */
    public void searchFilterSubmit(String category, JSONObject value, String currentPage, String keyWord) {
        Map<String, String> params = new HashMap<String, String>();
        params.put("id_category", category);
        params.put("page_limit", AppConstants.spageLimit);
        params.put("value", value.toString());
        params.put("current_page_number",currentPage);
         if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoSearchFilter;
            params.put("q", keyWord);params.put("value", value.toString());
            MyApplication.magentoNetworkCall(url, params, searchFilterProducts);
        } else {
            String url = AppConstants.sSearchFilter;
             params.put("action", AppConstants.sSearchFilterProductAction);
            params.put("product_name", AppConstants.stringEncode(keyWord));
            MyApplication.NetworkCall(url, params, searchFilterProducts);
        }
    }

    /**
     * Product navigation filter api
     *
     * @param categoryId  the category id
     * @param currentPage the current page
     * @param featureValue the filter value
     * @param attributeValue the filter value
     * @param conditionValue the filter value
     * @param manufactureValue the filter value
     * @param priceValue the filter value
     * @param weightValue the filter value
     * @param availabilityValue the availability value
     */
    public void produtNavigationFilter(String categoryId, String currentPage, JSONObject featureValue, JSONObject attributeValue,
                                       String conditionValue, String manufactureValue, JSONObject priceValue,
                                       JSONObject weightValue, String availabilityValue) {
        String mAvailability = "0";
        if (availabilityValue != null){
            mAvailability = availabilityValue;
        }
        String url = AppConstants.sSortByProduct;
        Map<String, String> params = new HashMap<String, String>();
        params.put("current_page_number", currentPage);
        params.put("page_limit", AppConstants.spageLimit);
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        params.put("id_category", categoryId);
        params.put("action", AppConstants.sProductFilter);
        params.put("attributes", attributeValue.toString());
        params.put("features", featureValue.toString());
        params.put("availability", mAvailability);
        params.put("condition", conditionValue);
        params.put("manufacturers", manufactureValue);
        params.put("weight", weightValue.toString());
        params.put("price", priceValue.toString());
        MyApplication.NetworkCall(url, params, searchFilterProducts);

    }


    /**
     * The Search filter products.
     */
    VolleyCallback searchFilterProducts = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {
                JSONObject jsonObject = null;
                jsonObject = new JSONObject(response);
                if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {

                    MagentoProductMain mMagentoProductMain = AppConstants.productListParser(response);

                    if (mMagentoProductMain.getStatus().equalsIgnoreCase("true")) {
                        try {
                            ((SearchActivity) mContext).filterMagentoSuccess(mMagentoProductMain);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        ((SearchActivity) mContext).searchFailure(mMagentoProductMain.getmErrorMsg());
                    }

                } else {
                    ProductMain mProductMainObject = (MyApplication.getGsonInstance().
                            fromJson(response, ProductMain.class));
                    if (jsonObject.getString("status").equalsIgnoreCase("true")) {
                        ((SearchActivity) mContext).filterSuccess(mProductMainObject);
                    } else {
                        ((SearchActivity) mContext).filterFailure(jsonObject.getString("errormsg"));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((SearchActivity) mContext).filterFailure(errorResponse);
        }
    };

    /**
     * Search Sort API @param currentPage the current page
     *
     * @param currentPage the current page
     * @param orderWay    the order way
     * @param orderBy     the order by
     * @param keyWord     the key word
     */
    public void searchSort(String currentPage, String orderWay, String orderBy, String keyWord) {

        mSortType = orderWay;
        mSortBy = orderBy;
        Map<String, String> params = new HashMap<String, String>();
        params.put("current_page_number", currentPage);
        params.put("page_limit", AppConstants.spageLimit);
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoSearchSort;
            params.put("sorttype", orderWay);
            params.put("sortby", orderBy);
            params.put("keyword", keyWord);
            MyApplication.magentoNetworkCall(url, params, searchSortProducts);
        }else {
            String url = AppConstants.sCategoriesController;
            params.put("action", AppConstants.sSearchProduct);
            params.put("orderway", orderWay);
            params.put("orderby", orderBy);
            params.put("q", keyWord);
            params.put("store_id", "1");
            MyApplication.NetworkCall(url, params, searchSortProducts);
        }

    }

    /**
     * The Search sort products.
     */
    VolleyCallback searchSortProducts = new VolleyCallback() {
        @Override
        public void Success(String response) {
            try {

                if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {

                    MagentoProductMain mMagentoProductMain = AppConstants.productListParser(response);

                    if (mMagentoProductMain.getStatus().equalsIgnoreCase("true")) {
                        try {
                            ((SearchActivity) mContext).sortMagentoSuccess(mMagentoProductMain.getMagentoProduct(), mSortType, mSortBy);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        ((SearchActivity) mContext).searchFailure(mMagentoProductMain.getmErrorMsg());
                    }

                } else {
                    ProductMain mProductMainObject = (MyApplication.getGsonInstance().
                            fromJson(response, ProductMain.class));
                    JSONObject jsonObject = null;
                    jsonObject = new JSONObject(response);
                    if (mProductMainObject.getStatus().equalsIgnoreCase("true")) {
                        ((SearchActivity) mContext).sortSuccess(mProductMainObject.getmProduct(), mSortType, mSortBy);
                    } else {
                        ((SearchActivity) mContext).sortFailure(jsonObject.getString("errormsg"));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public void Failure(String errorResponse) {
            ((SearchActivity) mContext).sortFailure(errorResponse);
        }
    };

    /**
     * Advanced Search Product API @param currentPage the current page
     *
     * @param currentPage the current page
     * @param searchText  the search text
     * @param categoryId  the category id
     */
    public void advancedSearchProducts(String currentPage, String searchText, String categoryId) {

        Map<String, String> params = new HashMap<String, String>();
        params.put("current_page_number", currentPage);
        params.put("page_limit", AppConstants.spageLimit);
        params.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
        params.put("id_category", categoryId);
        params.put("store_id", "1");

        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            String url = AppConstants.sMagentoSearchCategoryProduct;
            params.put("keyword", searchText);
            MyApplication.magentoNetworkCall(url, params, advancedSearchProductList);
        } else {
            String url = AppConstants.sSortByProduct;
            params.put("action", AppConstants.sAdvancedSearch);
            params.put("product_name", AppConstants.stringEncode(searchText));
            MyApplication.NetworkCall(url, params, advancedSearchProductList);
        }


    }

    /**
     * The Advanced search product list.
     */
    VolleyCallback advancedSearchProductList = new VolleyCallback() {
        @Override
        public void Success(String response) {


            if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {

                MagentoProductMain mMagentoProductMain = AppConstants.productListParser(response);

                if (mMagentoProductMain.getStatus().equalsIgnoreCase("true")) {
                    try {
                        ((SearchActivity) mContext).searchMagentoSuccess(mMagentoProductMain);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    ((SearchActivity) mContext).searchFailure(mMagentoProductMain.getmErrorMsg());
                }

            } else {

                mProductMainObject = (MyApplication.getGsonInstance().fromJson(response, ProductMain.class));

                if (mProductMainObject.getStatus().equalsIgnoreCase("true")) {
                    try {
                        ((SearchActivity) mContext).searchSuccess(mProductMainObject);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                } else {
                    ((SearchActivity) mContext).searchFailure(mProductMainObject.getmErrorMsg());
                }
            }

        }

        @Override
        public void Failure(String errorResponse) {
            ((SearchActivity) mContext).searchFailure(errorResponse);
        }
    };
}
