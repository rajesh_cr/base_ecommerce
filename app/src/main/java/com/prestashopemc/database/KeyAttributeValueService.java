package com.prestashopemc.database;

import android.content.Context;
import android.database.SQLException;

import com.j256.ormlite.dao.Dao;
import com.prestashopemc.model.KeyAttributeValue;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Key Attribute Value Service!</h1>
 * The Key Attribute Value Service is used to create database for Key Attribute Values.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class KeyAttributeValueService {

    private DatabaseHelper db;
    private Dao<KeyAttributeValue, Integer> mKeyAttributeValuesDao;

    /**
     * Instantiates a new Key attribute value service.
     *
     * @param ctx the ctx
     */
    public KeyAttributeValueService(Context ctx) {
        DatabaseManager dbManager = new DatabaseManager();
        db = dbManager.getHelper(ctx);
        mKeyAttributeValuesDao = db.getKeyAttributeDao();
    }

    /**
     * Add key attribute values.
     *
     * @param keyValuesObj the key values obj
     * @param productId    the product id
     * @throws Exception the exception
     */
    public void addKeyAttributeValues(List<String> keyValuesObj, String productId) throws Exception {
        // TODO Auto-generated method stub
        try {

            for (String keyValues : keyValuesObj) {
                KeyAttributeValue mKeyValue = new KeyAttributeValue();
                mKeyValue.setKeyValue(keyValues);
                mKeyValue.setProductId(productId);
                mKeyAttributeValuesDao.create(mKeyValue);
            }


        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        }
    }


    /**
     * Key attribute value list list.
     *
     * @param productId the product id
     * @return the list
     * @throws Exception the exception
     */
    public List<String> keyAttributeValueList(String productId) throws Exception {
        // TODO Auto-generated method stub
        List<String> keyValueList = new ArrayList<String>();
        List<KeyAttributeValue> keyValueAttributeList = new ArrayList<KeyAttributeValue>();
        keyValueAttributeList = mKeyAttributeValuesDao.queryBuilder().where().eq("productId", productId).query();
        for (KeyAttributeValue keyValue : keyValueAttributeList) {
            keyValueList.add(keyValue.getKeyValue());
        }
        try {
            return keyValueList;
        } catch (SQLException e) {

        }
        return null;
    }
}
