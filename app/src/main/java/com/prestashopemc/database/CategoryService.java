package com.prestashopemc.database;

import android.content.Context;
import android.database.SQLException;

import com.j256.ormlite.dao.Dao;
import com.prestashopemc.model.Category;
import com.prestashopemc.model.ExpandCategory;
import com.prestashopemc.model.SubCategoryModel;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Category Service!</h1>
 * The Category Service is used to create database for Category.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class CategoryService {

    private Dao<Category, Integer> mCategoryDao;
    private Dao<ExpandCategory, Integer> mExpandCategoryDao;
    private DatabaseManager mDbManager;
    private DatabaseHelper mDataBase;


    /**
     * Instantiates a new Category service.
     *
     * @param ctx the ctx
     */
    public CategoryService(Context ctx) {
        mDbManager = new DatabaseManager();
        mDataBase = mDbManager.getHelper(ctx);
        mCategoryDao = mDataBase.getCategoryDao();
        mExpandCategoryDao = mDataBase.getExpandCategoryDao();
    }

    /**
     * Insert category.
     *
     * @param categoryObj the category obj
     * @throws Exception the exception
     */
    public void insertCategory(List<Category> categoryObj) throws Exception {
        // TODO Auto-generated method stub
        try {
            for (Category category : categoryObj) {

                for (ExpandCategory expandCategory : category.getCategories()) {
                    expandCategory.setParentId(category.getIdCategory());
                    mExpandCategoryDao.create(expandCategory);
                }
                mCategoryDao.create(category);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Insert sub category.
     *
     * @param subCategory the subCategory
     * @throws Exception the exception
     */
    public void insertSubCategory(SubCategoryModel subCategory) throws Exception {
        // TODO Auto-generated method stub
        try {
            for (ExpandCategory expandCategory : subCategory.getResult()) {
                expandCategory.setParentId(subCategory.getIdCategory());
                mExpandCategoryDao.create(expandCategory);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieve category list list.
     *
     * @return the list
     * @throws Exception the exception
     */
    public List<Category> retrieveCategoryList()
            throws Exception {
        // TODO Auto-generated method stub
        List<Category> productList = new ArrayList<Category>();
        productList = mCategoryDao.queryBuilder().query();
        try {
            return productList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Retrieve expand category list.
     *
     * @param cat_id the cat id
     * @return the list
     * @throws Exception the exception
     */
    public List<ExpandCategory> retrieveExpandCategory(String cat_id)
            throws Exception {
        // TODO Auto-generated method stub
        List<ExpandCategory> categoryList = new ArrayList<ExpandCategory>();
        categoryList = mExpandCategoryDao.queryBuilder().where().eq("parentId", cat_id).query();
        try {
            return categoryList;
        } catch (SQLException e) {

        }
        return null;
    }


    /**
     * Getting subcategory values*/
    public List<ExpandCategory> retrieveSubCategory(String catId)
            throws Exception {
        // TODO Auto-generated method stub
        List<ExpandCategory> categoryList = new ArrayList<ExpandCategory>();
        categoryList = mExpandCategoryDao.queryBuilder().where().eq("parentId", catId).query();
        try {
            return categoryList;
        } catch (SQLException e) {

        }
        return null;
    }

    /**
     * Delete category.
     *
     * @throws Exception the exception
     */
    public void deleteCategory() throws Exception {
        try {
            mExpandCategoryDao.deleteBuilder().delete();
            mCategoryDao.deleteBuilder().delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
