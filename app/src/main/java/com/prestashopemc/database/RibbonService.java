package com.prestashopemc.database;

import android.content.Context;
import android.database.SQLException;

import com.j256.ormlite.dao.Dao;
import com.prestashopemc.model.Ribbon;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Ribbon Service!</h1>
 * The Ribbon Service is used to create database for Ribbon for Product label.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class RibbonService {
    private DatabaseHelper mDataBase;
    private Dao<Ribbon, Integer> mRibbonDao;
    private DatabaseManager mDbManager;

    /**
     * Instantiates a new Ribbon service.
     *
     * @param ctx the ctx
     */
    public RibbonService(Context ctx) {
        mDbManager = new DatabaseManager();
        mDataBase = mDbManager.getHelper(ctx);
        mRibbonDao = mDataBase.getRibbonDao();
    }

    /**
     * Add ribbon.
     *
     * @param keyValuesObj the key values obj
     * @param productId    the product id
     * @throws Exception the exception
     */
    public void addRibbon(List<Ribbon> keyValuesObj, String productId) throws Exception {
        // TODO Auto-generated method stub
        try {
            for (Ribbon mRibbon : keyValuesObj) {
                mRibbon.setProductId(productId);
                mRibbonDao.create(mRibbon);
            }
        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        }
    }

    /**
     * Ribbon list list.
     *
     * @param productId the product id
     * @return the list
     * @throws Exception the exception
     */
    public List<Ribbon> ribbonList(String productId) throws Exception {
        // TODO Auto-generated method stub
        List<Ribbon> ribbonList = new ArrayList<Ribbon>();
        ribbonList = mRibbonDao.queryBuilder().where().eq("productId", productId).query();

        try {
            return ribbonList;
        } catch (SQLException e) {

        }
        return null;
    }

    /**
     * Insert ribbon list.
     *
     * @param mRibbonList the m ribbon list
     * @throws Exception the exception
     */
    public void insertRibbonList(List<Ribbon> mRibbonList) throws Exception {
        // TODO Auto-generated method stub
        try {
            for (Ribbon mRibbon : mRibbonList) {
                mRibbonDao.create(mRibbon);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieve ribbon list list.
     *
     * @return the list
     * @throws Exception the exception
     */
    public List<Ribbon> retrieveRibbonList()throws Exception{
            // TODO Auto-generated method stub
            List<Ribbon> ribbonList = new ArrayList<Ribbon>();

            try {
                ribbonList = mRibbonDao.queryBuilder().query();

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return ribbonList;
        }

    /**
     * Deleted database list.
     *
     * @return the list
     * @throws Exception the exception
     */
    public List<Ribbon> deletedDatabase() throws Exception {
            // TODO Auto-generated method stub
            List<Ribbon> jobPageList = new ArrayList<Ribbon>();

            try {
                mRibbonDao.deleteBuilder().delete();

            } catch (SQLException e) {
                e.printStackTrace();
            }
            return jobPageList;
        }
    }




