package com.prestashopemc.database;

import android.content.Context;
import android.database.SQLException;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.prestashopemc.model.RecentSearchModel;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Recent Search Service!</h1>
 * The Recent Search Service is used to create database for Recent Search Products.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class RecentSearchService {

    private Dao<RecentSearchModel, Integer> mRecentSearchDao;
    private DatabaseManager mDbManager;
    private DatabaseHelper mDataBase;

    /**
     * Instantiates a new Recent search service.
     *
     * @param ctx the ctx
     */
/*Constructor*/
    public RecentSearchService(Context ctx) {
        mDbManager = new DatabaseManager();
        mDataBase = mDbManager.getHelper(ctx);
        mRecentSearchDao = mDataBase.getmRecentSearchModelDao();

    }

    /**
     * Insert recent search.
     *
     * @param recentSearchList the recent search list
     * @throws Exception the exception
     */
/*Add Values*/
    public void insertRecentSearch(List<RecentSearchModel> recentSearchList) throws Exception {
        // TODO Auto-generated method stub
        try {
            for (RecentSearchModel recentSearch : recentSearchList) {
                mRecentSearchDao.createIfNotExists(recentSearch);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieve recent search list.
     *
     * @return the list
     * @throws Exception the exception
     */
/*Retrieve Values*/
    public List<RecentSearchModel> retrieveRecentSearch()
            throws Exception {
        // TODO Auto-generated method stub
        List<RecentSearchModel> recentSearchList = new ArrayList<RecentSearchModel>();
        recentSearchList = mRecentSearchDao.queryBuilder().orderBy("searchKeyword", true).query();

        try {
            return recentSearchList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Delete recent search record.
     */
/*Delete Method*/
    public void deleteRecentSearchRecord() {
        try {
            DeleteBuilder<RecentSearchModel, Integer> deleteBuilder = mRecentSearchDao.deleteBuilder();
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete item.
     *
     * @param id the id
     */
/*Delete Particular Item from DB*/
    public void deleteItem(int id) {
        try {
            DeleteBuilder<RecentSearchModel, Integer> deleteBuilder = mRecentSearchDao.deleteBuilder();
            deleteBuilder.where().eq("id", id);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }


}
