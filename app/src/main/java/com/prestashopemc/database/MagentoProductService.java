package com.prestashopemc.database;

import android.content.Context;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.prestashopemc.model.MagentoProduct;
import com.prestashopemc.model.Product;

/**
 * <h1>Product Service!</h1>
 * The Product Service is used to create database for Products.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class MagentoProductService {

    private DatabaseHelper db;
    private Dao<MagentoProduct, Integer> mProductDao;
    private MagentoCategoryProductService mMagentoCategoryProductService;
    private Context mContext;

    /**
     * Instantiates a new Product service.
     *
     * @param ctx the ctx
     */
    public MagentoProductService(Context ctx) {
        mContext = ctx;
        DatabaseManager dbManager = new DatabaseManager();
        db = dbManager.getHelper(ctx);
        mMagentoCategoryProductService = new MagentoCategoryProductService(ctx);
        mProductDao = db.getMagentoProductDao();
    }

    /**
     * Add product.
     *
     * @param productObj  the product obj
     * @param categoryId  the category id
     * @param currentPage the current page
     * @param customerId  the customer id
     * @throws Exception the exception
     */
    public void addProduct(MagentoProduct productObj,String categoryId,String currentPage, String customerId) throws Exception {
        // TODO Auto-generated method stub
        try {
            mProductDao.create(productObj);
            mMagentoCategoryProductService.addProduct(productObj.getCategoryProducts(),categoryId,currentPage, customerId);
        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        }
    }

    /**
     * Delete product service record.
     */
/*Delete Method*/
    public void deleteProductServiceRecord(){
        try {
            DeleteBuilder<MagentoProduct, Integer> deleteBuilder = mProductDao.deleteBuilder();
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

}
