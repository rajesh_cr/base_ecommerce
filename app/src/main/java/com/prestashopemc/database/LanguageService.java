package com.prestashopemc.database;

import android.content.Context;
import android.database.SQLException;

import com.j256.ormlite.dao.Dao;
import com.prestashopemc.model.Language;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Language Service!</h1>
 * The Language Service is used to create database for Languages.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class LanguageService {

    private Dao<Language, Integer> mLanguageDao;
    private DatabaseManager mDbManager;
    private DatabaseHelper mDataBase;

    /**
     * Instantiates a new Language service.
     *
     * @param ctx the ctx
     */
    public LanguageService(Context ctx) {
        mDbManager = new DatabaseManager();
        mDataBase = mDbManager.getHelper(ctx);
        mLanguageDao = mDataBase.getLanguageDao();

    }

    /**
     * Insert language.
     *
     * @param languageList the language list
     * @throws Exception the exception
     */
    public void insertLanguage(List<Language> languageList) throws Exception {
        // TODO Auto-generated method stub
        try {
            for (Language language : languageList) {
                mLanguageDao.create(language);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieve lang list list.
     *
     * @return the list
     * @throws Exception the exception
     */
    public List<Language> retrieveLangList()
            throws Exception {
        // TODO Auto-generated method stub
        List<Language> languageList = new ArrayList<Language>();
        languageList = mLanguageDao.queryBuilder().query();
        try {
            return languageList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Delete language.
     *
     * @throws Exception the exception
     */
    public void deleteLanguage() throws Exception {
        try {
            mLanguageDao.deleteBuilder().delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
