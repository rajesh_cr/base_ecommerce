package com.prestashopemc.database;

import android.content.Context;
import android.database.SQLException;

import com.j256.ormlite.dao.Dao;
import com.prestashopemc.model.CustomAttribute;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Custom Attribute Service!</h1>
 * The Custom Attribute Service is used to create database for Custom Attribute for products.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class CustomAttributeService {

    private DatabaseHelper db;
    private Dao<CustomAttribute, Integer> mCustomAttributeDao;
    private CustomAttributeValueService mCustomAttributeValueService;

    /**
     * Instantiates a new Custom attribute service.
     *
     * @param ctx the ctx
     */
    public CustomAttributeService(Context ctx) {
        DatabaseManager dbManager = new DatabaseManager();
        db = dbManager.getHelper(ctx);
        mCustomAttributeValueService = new CustomAttributeValueService(ctx);
        mCustomAttributeDao = db.getCustomAttributeDao();
    }

    /**
     * Add custom attribute.
     *
     * @param customValuesObj the custom values obj
     * @param productId       the product id
     * @throws Exception the exception
     */
    public void addCustomAttribute(List<CustomAttribute> customValuesObj, String productId) throws Exception {
        // TODO Auto-generated method stub
        try {

            for (CustomAttribute customValues : customValuesObj) {
                customValues.setProductId(productId);
                mCustomAttributeDao.create(customValues);
                mCustomAttributeValueService.addCustomAttribute(customValues.getAttributeCombination(), productId);
            }

        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        }
    }


    /**
     * Custom attribute list list.
     *
     * @param productId the product id
     * @return the list
     * @throws Exception the exception
     */
    public List<CustomAttribute> customAttributeList(String productId)
            throws Exception {
        // TODO Auto-generated method stub
        List<CustomAttribute> customAttributeList = new ArrayList<CustomAttribute>();
        customAttributeList = mCustomAttributeDao.queryBuilder().where().eq("productId", productId).query();

        for (CustomAttribute mCustomValues : customAttributeList) {


//            mCustomValues.setAttributeValue(mKeyAttributeValueService.keyAttributeValueList(productId));
        }
        try {
            return customAttributeList;
        } catch (SQLException e) {

            return null;
        }

    }
}
