package com.prestashopemc.database;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;

/**
 * <h1>Database Manager!</h1>
 * The Database Manager is used to manage tables for all models.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class DatabaseManager {
	private DatabaseHelper mDatabaseHelper = null;

    /**
     * Gets helper.
     *
     * @param context the context
     * @return the helper
     */
//gets a helper once one is created ensures it doesnt create a new one
    public DatabaseHelper getHelper(Context context)
    {
        if (mDatabaseHelper == null) {
            mDatabaseHelper = OpenHelperManager.getHelper(context, DatabaseHelper.class);
        }
        return mDatabaseHelper;
    }

    /**
     * Release helper.
     *
     * @param helper the helper
     */
//releases the helper once usages has ended
    public void releaseHelper(DatabaseHelper helper)
    {
        if (mDatabaseHelper != null) {
            OpenHelperManager.releaseHelper();
            mDatabaseHelper = null;
        }
    }

}
