package com.prestashopemc.database;

import android.content.Context;
import android.database.SQLException;

import com.j256.ormlite.dao.Dao;
import com.prestashopemc.model.Currency;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Currency Service!</h1>
 * The Currency Service is used to create database for Currency.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class CurrencyService {

    private Dao<Currency, Integer> mCurrencyDao;
    private DatabaseManager mDbManager;
    private DatabaseHelper mDataBase;

    /**
     * Instantiates a new Currency service.
     *
     * @param ctx the ctx
     */
    public CurrencyService(Context ctx) {
        mDbManager = new DatabaseManager();
        mDataBase = mDbManager.getHelper(ctx);
        mCurrencyDao = mDataBase.getCurrencyDao();

    }

    /**
     * Insert currency.
     *
     * @param currencyList the currency list
     * @throws Exception the exception
     */
    public void insertCurrency(List<Currency> currencyList) throws Exception {
        // TODO Auto-generated method stub
        try {
            for (Currency currency : currencyList) {
                mCurrencyDao.create(currency);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieve currency list list.
     *
     * @return the list
     * @throws Exception the exception
     */
    public List<Currency> retrieveCurrencyList()
            throws Exception {
        // TODO Auto-generated method stub
        List<Currency> currencyList = new ArrayList<Currency>();
        currencyList = mCurrencyDao.queryBuilder().query();
        try {
            return currencyList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Delete currency.
     *
     * @throws Exception the exception
     */
    public void deleteCurrency() throws Exception {
        try {
            mCurrencyDao.deleteBuilder().delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
