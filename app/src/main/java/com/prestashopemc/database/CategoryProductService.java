package com.prestashopemc.database;

import android.content.Context;
import android.database.SQLException;
import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.prestashopemc.model.CategoryProduct;
import com.prestashopemc.model.MagentoCategoryProduct;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Category Product Service!</h1>
 * The Category Product Service is used to create database for Category Product.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class CategoryProductService {

    private DatabaseHelper db;
    private Dao<CategoryProduct, Integer> mCategoryProductDao;
    private Dao<MagentoCategoryProduct, Integer> mMagentoCategoryProductDao;
    private KeyValueService mKeyValueService;
    private RibbonService mRibbonService;
    private CustomAttributeService mCustomAttributeService;
    private Context mContext;

    /**
     * Instantiates a new Category product service.
     *
     * @param ctx the ctx
     */
/*Constructor method*/
    public CategoryProductService(Context ctx) {
        DatabaseManager dbManager = new DatabaseManager();
        mContext = ctx;
        db = dbManager.getHelper(ctx);
        mKeyValueService = new KeyValueService(ctx);
        mRibbonService = new RibbonService(ctx);
        mCustomAttributeService = new CustomAttributeService(ctx);
        mCategoryProductDao = db.getCategoryProductDao();
        mMagentoCategoryProductDao = db.getMagentoCategoryProductDao();
    }

    /**
     * Add product.
     *
     * @param productObj  the product obj
     * @param categoryId  the category id
     * @param currentPage the current page
     * @param customerId  the customer id
     * @throws Exception the exception
     */

    public void addProduct(List<CategoryProduct> productObj, String categoryId, String currentPage, String customerId) throws Exception {
        // TODO Auto-generated method stub
        try {

            for (CategoryProduct categoryProduct : productObj) {
                categoryProduct.setCurrentPage(currentPage);
                categoryProduct.setCategoryId(categoryId);
                categoryProduct.setCustomerId(customerId);
                mCategoryProductDao.create(categoryProduct);

                if (categoryProduct.getRibbonEnabled()) {
                  //  Log.e("CategoryProducts", "Ribbon" + categoryProduct.getRibbonEnabled());
                    mRibbonService.addRibbon(categoryProduct.getRibbon(), categoryProduct.getProductId());
                //    Log.e("CategoryProducts", "Ribbon" + categoryProduct.getRibbon() + "ProductId" + categoryProduct.getProductId().toString());
                }

                if (categoryProduct.getIsCombination()) {
                    mKeyValueService.addKeyValues(categoryProduct.getKeyValuesList(),
                            categoryProduct.getProductId());
                    mCustomAttributeService.addCustomAttribute(categoryProduct.getCustomAttributes(),
                            categoryProduct.getProductId());
                }
            }

        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        }
    }

    /**
     * Update value.
     *
     * @param categoryProduct the category product
     * @param customerId      the customer id
     * @param wishlistStatus  the wishlist status
     * @throws Exception the exception
     */

    public void updateValue(CategoryProduct categoryProduct, String customerId, Boolean wishlistStatus) throws Exception {
        try {
            categoryProduct.setCustomerId(customerId);
            categoryProduct.setWishlist(wishlistStatus);
            mCategoryProductDao.update(categoryProduct);
        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        }
    }


    /**
     * Category product list list.
     *
     * @param catId      the cat id
     * @param pageNumber the page number
     * @return the list
     * @throws Exception the exception
     */


    public List<CategoryProduct> categoryProductList(String catId, String pageNumber) throws Exception {
        // TODO Auto-generated method stub
        List<CategoryProduct> productList = new ArrayList<CategoryProduct>();
        productList = mCategoryProductDao.queryBuilder().where().eq("categoryId", catId).and().eq("currentPage", pageNumber).query();


        for (CategoryProduct mCategoryProduct : productList) {

            if (mCategoryProduct.getRibbonEnabled() != null) {
                if (mCategoryProduct.getRibbonEnabled()) {
                    mCategoryProduct.setRibbon(mRibbonService.ribbonList(mCategoryProduct.getProductId()));
                }
            }
            if (mCategoryProduct.getIsCombination()) {
                mCategoryProduct.setKeyValuesList( mKeyValueService.keyValuesList(mCategoryProduct.getProductId()));
                mCategoryProduct.setCustomAttributes(mCustomAttributeService.customAttributeList(mCategoryProduct.getProductId()));
            }
        }
        try {
            return productList;
        } catch (SQLException e) {

        }
        return null;
    }

    /**
     * Delete category product service record.
     */
    public void deleteCategoryProductServiceRecord() {
        try {
            DeleteBuilder<CategoryProduct, Integer> deleteBuilder = mCategoryProductDao.deleteBuilder();
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    /*
    * Get Total products Rows */
    public long getTotalRows() throws Exception {
        // TODO Auto-generated method stub
        long productsTotalRow = mCategoryProductDao.queryBuilder().countOf();
        try {
            return productsTotalRow;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Long.parseLong(null);
    }

    /*
    * Get first 10 products */
    public List<CategoryProduct> getFirstTenProducts() throws Exception {
        // TODO Auto-generated method stub
        QueryBuilder<CategoryProduct, Integer> builder = mCategoryProductDao.queryBuilder();
        builder.limit(10);
        builder.orderBy("id", true);  // true for ascending, false for descending
        List<CategoryProduct> productList = new ArrayList<CategoryProduct>();
        productList = mCategoryProductDao.query(builder.prepare());

       try {
            return productList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


    /**
     * Delete particular record.
     * @param categoryPrds
     */
    public void deleteParticularRecord(List<CategoryProduct> categoryPrds) {
        try {
            for (int i=0; i<categoryPrds.size();i++){
                mCategoryProductDao.deleteById(categoryPrds.get(i).getId());
            }
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

}
