package com.prestashopemc.database;

import android.content.Context;
import android.database.SQLException;

import com.j256.ormlite.dao.Dao;
import com.prestashopemc.model.CmsPage;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Cms Service!</h1>
 * The Cms Service is used to create database for Cms lists.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class CmsService {
    private Dao<CmsPage, Integer> mCmsPageDao;
    private DatabaseManager mDbManager;
    private DatabaseHelper mDataBase;

    /**
     * Instantiates a new Cms service.
     *
     * @param ctx the ctx
     */
    public CmsService(Context ctx) {
        mDbManager = new DatabaseManager();
        mDataBase = mDbManager.getHelper(ctx);
        mCmsPageDao = mDataBase.getCmsDao();

    }

    /**
     * Insert cms page.
     *
     * @param cmsPageList the cms page list
     * @throws Exception the exception
     */
    public void insertCmsPage(List<CmsPage> cmsPageList) throws Exception {
        // TODO Auto-generated method stub
        try {
            for (CmsPage cmsPage : cmsPageList) {
                mCmsPageDao.create(cmsPage);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieve cms list list.
     *
     * @return the list
     * @throws Exception the exception
     */
    public List<CmsPage> retrieveCmsList()
            throws Exception {
        // TODO Auto-generated method stub
        List<CmsPage> cmsPageList = new ArrayList<CmsPage>();
        cmsPageList = mCmsPageDao.queryBuilder().query();
        try {
            return cmsPageList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Delete cms.
     *
     * @throws Exception the exception
     */
    public void deleteCms() throws Exception {
        try {
            mCmsPageDao.deleteBuilder().delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
