package com.prestashopemc.database;

import android.content.Context;
import android.database.SQLException;

import com.j256.ormlite.dao.Dao;
import com.prestashopemc.model.HomePageList;
import com.prestashopemc.webModel.ImageBorder;
import com.prestashopemc.webModel.ImageList;
import com.prestashopemc.webModel.Link;
import com.prestashopemc.webModel.SubTitle;
import com.prestashopemc.webModel.Title;
import com.prestashopemc.webModel.WebCategory;
import com.prestashopemc.webModel.WebProduct;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Banner Service!</h1>
 * The Banner Service is used to create database for Banners.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class BannerService {

    private Dao<HomePageList, Integer> mHomeBannerDao;
    private Dao<ImageList, Integer> mSubBannerDao;
    private Dao<ImageBorder, Integer> mImageBorderDao;
    private Dao<Link, Integer> mLinkDao;
    private Dao<Title, Integer> mTitleDao;
    private Dao<WebCategory, Integer> mWebCategoryDao;
    private Dao<WebProduct, Integer> mWebProductDao;
    private Dao<SubTitle, Integer> mSubTitleDao;
    private DatabaseManager mDbManager;
    private DatabaseHelper mDataBase;


    /**
     * Instantiates a new Banner service.
     *
     * @param ctx the ctx
     */
    public BannerService(Context ctx) {
        mDbManager = new DatabaseManager();
        mDataBase = mDbManager.getHelper(ctx);
        mHomeBannerDao = mDataBase.getHomeBannerDao();
        mSubBannerDao = mDataBase.getSubBannerDao();
        mImageBorderDao = mDataBase.getBorderModelDao();
        mTitleDao = mDataBase.getTitleModelDao();
        mLinkDao = mDataBase.getLinkModelDao();
        mWebCategoryDao = mDataBase.getWebCategoryModelDao();
        mWebProductDao = mDataBase.getWebProductModelDao();
        mSubTitleDao = mDataBase.getSubTitleDao();
    }


    /**
     * Insert home banner.
     *
     * @param mHomePageList the m home page list
     * @throws Exception the exception
     */
    public void insertHomeBanner(List<HomePageList> mHomePageList) throws Exception {
        // TODO Auto-generated method stub
        try {
            for (int k = 0; k<mHomePageList.size(); k++) {

                HomePageList mHomeBanner = mHomePageList.get(k);

                String slotPosition = String.valueOf(mHomeBanner.getSlotposition());

                for (int j = 0; j<mHomeBanner.getImagelist().size(); j++){

                    ImageList mSubBanner = mHomeBanner.getImagelist().get(j);

                    mSubBanner.setSlotPosition(slotPosition);
                    mSubBanner.setPositionId(k);

                    if ( mSubBanner.getTitle() != null)
                        mSubBanner.getTitle().setSlotPosition(slotPosition);

                    mSubBanner.getLink().setSlotPosition(slotPosition);

                    if (mSubBanner.getLink().getCategory() != null)
                        mSubBanner.getLink().getCategory().setSlotPosition(slotPosition);
                    if (mSubBanner.getLink().getProduct() != null)
                        mSubBanner.getLink().getProduct().setSlotPosition(slotPosition);

                    //Log.e("Subtitle size",mSubBanner.getTitle().getTitle().size()+"");

                    for (int i = 0; i<mSubBanner.getTitle().getTitle().size(); i++){
                        //Log.e("Subtitle value",mSubBanner.getTitle().getTitle().get(i).getValue()+"");
                        SubTitle subTitle = mSubBanner.getTitle().getTitle().get(i);
                        subTitle.setSlotPosition(slotPosition);
                        subTitle.setPositionId(i);
                        mSubTitleDao.create(subTitle);
                    }

                    mSubBanner.getTitle().setPostionId(j);
                    mTitleDao.create(mSubBanner.getTitle());
                    mLinkDao.create(mSubBanner.getLink());
                    mWebCategoryDao.create(mSubBanner.getLink().getCategory());
                    mWebProductDao.create(mSubBanner.getLink().getProduct());
                    mSubBannerDao.create(mSubBanner);

                }

                for (SubTitle subTitle : mHomeBanner.getTitle()){
                    subTitle.setSlotPosition(slotPosition);
                    mSubTitleDao.create(subTitle);
                }
                if (mHomeBanner.getBorder() != null) {
                    mHomeBanner.getBorder().setSlotPosition(slotPosition);
                    mImageBorderDao.create(mHomeBanner.getBorder());
                }
                mHomeBannerDao.create(mHomeBanner);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrieve home banner list list.
     *
     * @return the list
     * @throws Exception the exception
     */
    public List<HomePageList> retrieveHomeBannerList()
            throws Exception {
        // TODO Auto-generated method stub
        List<HomePageList> homePageList = new ArrayList<HomePageList>();

        try {
            homePageList = mHomeBannerDao.queryBuilder().query();

            for (HomePageList mSubBanner : homePageList) {
                List<ImageBorder> imageBorderList = retrieveBorderList(String.valueOf(mSubBanner.getSlotposition()));
                mSubBanner.setImagelist(retrieveSubBannerList(String.valueOf(mSubBanner.getSlotposition())));

                if(imageBorderList.size()>0){
                    mSubBanner.setBorder(imageBorderList.get(0));
                }

                mSubBanner.setTitle(retrieveSubTitle(String.valueOf(mSubBanner.getSlotposition())));

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return homePageList;
    }


    /**
     * Retrieve sub banner list list.
     *
     * @param slotPosition the slot position
     * @return the list
     * @throws Exception the exception
     */
    public List<ImageList> retrieveSubBannerList(String slotPosition)
            throws Exception {
        // TODO Auto-generated method stub
        List<ImageList> subBannerList = new ArrayList<ImageList>();
        subBannerList = mSubBannerDao.queryBuilder().where().eq("slotPosition", slotPosition).query();


        for (int i = 0; i<subBannerList.size(); i++){

            ImageList mImageList = subBannerList.get(i);
            mImageList.setTitle(retrieveTitle(mImageList.getSlotPosition(),mImageList.getPositionId()).get(0));
            Link link = retrieveLinkList(mImageList.getSlotPosition()).get(0);
            if (link.getCategory() != null)
                link.setCategory(retrieveWebCategory(mImageList.getSlotPosition()).get(0));
            if (link.getProduct() != null)
                link.setProduct(retrieveWebProduct(mImageList.getSlotPosition()).get(0));

            mImageList.setLink(link);
        }

        try {
            return subBannerList;
        } catch (SQLException e) {

        }
        return null;
    }

    /**
     * Retrieve title list.
     *
     * @param slotPosition the slot position
     * @param positionId   the position id
     * @return the list
     * @throws Exception the exception
     */
    public List<Title> retrieveTitle(String slotPosition,int positionId)
            throws Exception {
        // TODO Auto-generated method stub
        List<Title> titleList = new ArrayList<Title>();
        titleList = mTitleDao.queryBuilder().where().eq("slotPosition", slotPosition).query();

        for (Title mTitle : titleList){
            mTitle.setTitle(retrieveSubitle(mTitle.getSlotPosition(), mTitle.getPostionId()));
        }

        try {
            return titleList;
        } catch (SQLException e) {

        }
        return null;
    }

    /**
     * Retrieve subitle list.
     *
     * @param slotPosition the slot position
     * @param positionId   the position id
     * @return the list
     * @throws Exception the exception
     */
    public List<SubTitle> retrieveSubitle(String slotPosition, int positionId) throws Exception {
        // TODO Auto-generated method stub
        List<SubTitle> subTitleList = new ArrayList<SubTitle>();
        subTitleList = mSubTitleDao.queryBuilder().where().eq("slotPosition", slotPosition).and().
                eq("positionId", positionId).query();
        try {
            //Log.e("retrieveSubitle",subTitleList.size()+"");
            return subTitleList;

        } catch (SQLException e) {

        }
        return null;
    }

    /**
     * Retrieve sub title list.
     *
     * @param slotPosition the slot position
     * @return the list
     * @throws Exception the exception
     */
    public List<SubTitle> retrieveSubTitle(String slotPosition) throws Exception {
        // TODO Auto-generated method stub
        List<SubTitle> subTitleList = new ArrayList<SubTitle>();
        subTitleList = mSubTitleDao.queryBuilder().where().eq("slotPosition", slotPosition).query();
        try {
            return subTitleList;
        } catch (SQLException e) {

        }
        return null;
    }

    /**
     * Retrieve border list list.
     *
     * @param slotPosition the slot position
     * @return the list
     * @throws Exception the exception
     */
//
    public List<ImageBorder> retrieveBorderList(String slotPosition)
            throws Exception {
        // TODO Auto-generated method stub
        List<ImageBorder> borderList = new ArrayList<ImageBorder>();
        borderList = mImageBorderDao.queryBuilder().where().eq("slotPosition", slotPosition).query();
        try {
            return borderList;
        } catch (SQLException e) {

        }
        return null;
    }

    /**
     * Retrieve link list list.
     *
     * @param slotPosition the slot position
     * @return the list
     * @throws Exception the exception
     */
//
    public List<Link> retrieveLinkList(String slotPosition)
            throws Exception {
        // TODO Auto-generated method stub
        List<Link> linkList = new ArrayList<Link>();
        linkList = mLinkDao.queryBuilder().where().eq("slotPosition", slotPosition).query();
        try {
            return linkList;
        } catch (SQLException e) {

        }

        return null;
    }

    /**
     * Retrieve web category list.
     *
     * @param slotPosition the slot position
     * @return the list
     * @throws Exception the exception
     */
//
    public List<WebCategory> retrieveWebCategory(String slotPosition)
            throws Exception {
        // TODO Auto-generated method stub
        List<WebCategory> titleList = new ArrayList<WebCategory>();
        titleList = mWebCategoryDao.queryBuilder().where().eq("slotPosition", slotPosition).query();
        try {
            return titleList;
        } catch (SQLException e) {

        }
        return null;
    }

    /**
     * Retrieve web product list.
     *
     * @param slotPosition the slot position
     * @return the list
     * @throws Exception the exception
     */
    public List<WebProduct> retrieveWebProduct(String slotPosition)
            throws Exception {
        // TODO Auto-generated method stub
        List<WebProduct> productList = new ArrayList<WebProduct>();
        productList = mWebProductDao.queryBuilder().where().eq("slotPosition", slotPosition).query();
        try {
            return productList;
        } catch (SQLException e) {

        }
        return null;
    }


    /**
     * Delete Banner values from DB @throws Exception the exception
     *
     * @throws Exception the exception
     */
    public void deleteBanner() throws Exception {
        try {
            mHomeBannerDao.deleteBuilder().delete();
            mSubBannerDao.deleteBuilder().delete();
            mImageBorderDao.deleteBuilder().delete();
            mLinkDao.deleteBuilder().delete();
            mTitleDao.deleteBuilder().delete();
            mSubTitleDao.deleteBuilder().delete();
            mWebCategoryDao.deleteBuilder().delete();
            mWebProductDao.deleteBuilder().delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
