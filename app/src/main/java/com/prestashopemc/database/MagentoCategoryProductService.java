package com.prestashopemc.database;

import android.content.Context;
import android.database.SQLException;
import android.util.Log;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.prestashopemc.model.CategoryProduct;
import com.prestashopemc.model.MagentoCategoryProduct;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dinakaran on 31/3/17.
 */

public class MagentoCategoryProductService {

    private DatabaseHelper db;
    private Dao<MagentoCategoryProduct, Integer> mMagentoCategoryProductDao;
    private Context mContext;

    /**
     * Instantiates a new Category product service.
     *
     * @param ctx the ctx
     */

    public MagentoCategoryProductService(Context ctx) {
        DatabaseManager dbManager = new DatabaseManager();
        mContext = ctx;
        db = dbManager.getHelper(ctx);
        mMagentoCategoryProductDao = db.getMagentoCategoryProductDao();
    }

    public void addProduct(List<MagentoCategoryProduct> productObj, String categoryId, String currentPage, String customerId) throws Exception {
        // TODO Auto-generated method stub
        try {

            for (MagentoCategoryProduct categoryProduct : productObj) {
                categoryProduct.setCurrentPage(currentPage);
                categoryProduct.setCategoryId(categoryId);
                categoryProduct.setCustomerId(customerId);
                mMagentoCategoryProductDao.create(categoryProduct);
            }

        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        }
    }

    /**
     * Update value.
     *
     * @param categoryProduct the category product
     * @param customerId      the customer id
     * @param wishlistStatus  the wishlist status
     * @throws Exception the exception
     */

    public void updateMagentoValue(MagentoCategoryProduct categoryProduct, String customerId, Boolean wishlistStatus) throws Exception {
        try {
            categoryProduct.setCustomerId(customerId);
            categoryProduct.setWishlist(wishlistStatus);
            mMagentoCategoryProductDao.update(categoryProduct);
        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        }
    }


    public List<MagentoCategoryProduct> magentoCategoryProductList(String catId, String pageNumber) throws Exception {
        // TODO Auto-generated method stub
        List<MagentoCategoryProduct> productList = new ArrayList<MagentoCategoryProduct>();
        productList = mMagentoCategoryProductDao.queryBuilder().where().eq("categoryId", catId).and().eq("currentPage", pageNumber).query();
        try {
            return productList;
        } catch (SQLException e) {

        }
        return null;
    }

    /**
     * Delete category product service record.
     */
    public void deleteCategoryProductServiceRecord() {
        try {
            DeleteBuilder<MagentoCategoryProduct, Integer> deleteBuilder = mMagentoCategoryProductDao.deleteBuilder();
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    /**
     * Get total products row */
    public long getTotalRows() throws Exception {
        // TODO Auto-generated method stub
        long productsTotalRow = mMagentoCategoryProductDao.queryBuilder().countOf();
        try {
            return productsTotalRow;
        } catch (SQLException e) {

        }
        return Long.parseLong(null);
    }

    /*
   * Get first 10 products */
    public List<MagentoCategoryProduct> getFirstTenProducts() throws Exception {
        // TODO Auto-generated method stub
        QueryBuilder<MagentoCategoryProduct, Integer> builder = mMagentoCategoryProductDao.queryBuilder();
        builder.limit(10);
        builder.orderBy("id", true);  // true for ascending, false for descending
        List<MagentoCategoryProduct> productList = new ArrayList<MagentoCategoryProduct>();
        productList = mMagentoCategoryProductDao.query(builder.prepare());

        try {
            return productList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Delete particular record.
     * @param categoryPrds
     */
    public void deleteParticularRecord(List<MagentoCategoryProduct> categoryPrds) {
        try {
            for (int i=0; i<categoryPrds.size();i++){
                mMagentoCategoryProductDao.deleteById(categoryPrds.get(i).getId());
            }
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }
}
