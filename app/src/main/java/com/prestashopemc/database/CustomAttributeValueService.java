package com.prestashopemc.database;

import android.content.Context;
import android.database.SQLException;

import com.j256.ormlite.dao.Dao;
import com.prestashopemc.model.CustomAttributeValue;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Custom Attribute Value Service!</h1>
 * The Custom Attribute Value Service is used to create database for Custom Attribute Value for products.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class CustomAttributeValueService {

    private DatabaseHelper db;
    private Dao<CustomAttributeValue, Integer> mCustomAttributeDao;

    /**
     * Instantiates a new Custom attribute value service.
     *
     * @param ctx the ctx
     */
    public CustomAttributeValueService(Context ctx) {
        DatabaseManager dbManager = new DatabaseManager();
        db = dbManager.getHelper(ctx);
        mCustomAttributeDao = db.getCustomAttributeValueDao();
    }

    /**
     * Add custom attribute.
     *
     * @param customValueObj the custom value obj
     * @param productId      the product id
     * @throws Exception the exception
     */
    public void addCustomAttribute(List<String> customValueObj,String productId) throws Exception {
        // TODO Auto-generated method stub
        try {

            for(String customValue:customValueObj){
                CustomAttributeValue customAttributeValue = new CustomAttributeValue();
                customAttributeValue.setProductId(productId);
                customAttributeValue.setCustomAttributeValue(customValue);
                mCustomAttributeDao.create(customAttributeValue);
            }

        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        }
    }


    /**
     * Custom attribute value list list.
     *
     * @param productId the product id
     * @return the list
     * @throws Exception the exception
     */
    public List<String> customAttributeValueList(String productId)
            throws Exception {
        // TODO Auto-generated method stub
        List<String> customAttributeValueList = new ArrayList<String>();
        List<CustomAttributeValue> customAttributeList = new ArrayList<CustomAttributeValue>();
        customAttributeList = mCustomAttributeDao.queryBuilder().where().eq("productId", productId).query();
        for (CustomAttributeValue keyValue : customAttributeList) {
            customAttributeValueList.add(keyValue.getCustomAttributeValue());
        }
        try {
            return customAttributeValueList;
        } catch (SQLException e) {

        }
        return null;
    }

}
