package com.prestashopemc.database;

import android.content.Context;
import android.database.SQLException;

import com.j256.ormlite.dao.Dao;
import com.prestashopemc.model.KeyValues;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Key Value Service!</h1>
 * The Key Value Service is used to create database for Key Value for product search.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class KeyValueService {

    private DatabaseHelper db;
    private Dao<KeyValues, Integer> mKeyValuesDao;
    private KeyAttributeValueService mKeyAttributeValueService;

    /**
     * Instantiates a new Key value service.
     *
     * @param ctx the ctx
     */
    public KeyValueService(Context ctx) {
        DatabaseManager dbManager = new DatabaseManager();
        db = dbManager.getHelper(ctx);
        mKeyAttributeValueService = new KeyAttributeValueService(ctx);
        mKeyValuesDao = db.getKeyValuesDao();
    }

    /**
     * Add key values.
     *
     * @param keyValuesObj the key values obj
     * @param productId    the product id
     * @throws Exception the exception
     */
    public void addKeyValues(List<KeyValues> keyValuesObj, String productId) throws Exception {
        // TODO Auto-generated method stub
        try {

            for (KeyValues keyValues : keyValuesObj) {
                keyValues.setProductId(productId);
                mKeyValuesDao.create(keyValues);
                mKeyAttributeValueService.addKeyAttributeValues(keyValues.getAttributeValue(), productId);
            }

        } catch (Exception e) {
            e.getMessage();
            e.printStackTrace();
        }
    }


    /**
     * Key values list list.
     *
     * @param productId the product id
     * @return the list
     * @throws Exception the exception
     */
    public List<KeyValues> keyValuesList(String productId)
            throws Exception {
        // TODO Auto-generated method stub
        List<KeyValues> keyValueList = new ArrayList<KeyValues>();
        keyValueList = mKeyValuesDao.queryBuilder().where().eq("productId", productId).query();

        for (KeyValues mKeyValues : keyValueList) {
            mKeyValues.setAttributeValue(mKeyAttributeValueService.keyAttributeValueList(productId));
        }
        try {
            return keyValueList;
        } catch (SQLException e) {

        }
        return null;
    }
}
