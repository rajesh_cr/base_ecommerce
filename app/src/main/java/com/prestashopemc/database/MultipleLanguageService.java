package com.prestashopemc.database;

import android.content.Context;
import android.database.SQLException;

import com.j256.ormlite.dao.Dao;
import com.prestashopemc.model.MultipleLanguageResult;
import com.prestashopemc.model.MultipleLanguageValue;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Multiple Language Service!</h1>
 * The Multiple Language Service is used to create database for Multiple Languages.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class MultipleLanguageService {

        private Dao<MultipleLanguageResult, Integer> mTranslationResultDao;
        private Dao<MultipleLanguageValue, Integer> mTranslationValueDao;
        private DatabaseManager mDbManager;
        private DatabaseHelper mDataBase;

    /**
     * Instantiates a new Multiple language service.
     *
     * @param ctx the ctx
     */
    public MultipleLanguageService(Context ctx) {
            mDbManager = new DatabaseManager();
            mDataBase = mDbManager.getHelper(ctx);
            mTranslationResultDao = mDataBase.getTranslationResultDao();
            mTranslationValueDao = mDataBase.getTranslationValueDao();
        }

    /**
     * Insert translations.
     *
     * @param translationResultList the translation result list
     * @throws Exception the exception
     */
    public void insertTranslations(List<MultipleLanguageResult> translationResultList) throws Exception {
        // TODO Auto-generated method stub
        try {
            for (MultipleLanguageResult mTranslationResult : translationResultList) {
                for (MultipleLanguageValue mTranslationValue : mTranslationResult.getValue()) {
                    mTranslationValue.setCode(mTranslationResult.getCode());
                    mTranslationValueDao.create(mTranslationValue);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Retrive translation value string.
     *
     * @param transCode the trans code
     * @param mKey      the m key
     * @return the string
     * @throws Exception the exception
     */
    public String retriveTranslationValue(String transCode, String mKey)
            throws Exception {
        // TODO Auto-generated method stub
        List<MultipleLanguageValue> translationValues = new ArrayList<MultipleLanguageValue>();
        translationValues = mTranslationValueDao.queryBuilder().where().eq("code", transCode).and().eq("key", mKey).query();
        try {
            if (translationValues.size()>0){
                return translationValues.get(0).getValue();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * Delete multiple language.
     *
     * @throws Exception the exception
     */
    public void deleteMultipleLanguage() throws Exception {
        try {
            mTranslationResultDao.deleteBuilder().delete();
            mTranslationValueDao.deleteBuilder().delete();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
