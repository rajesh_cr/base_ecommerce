package com.prestashopemc.database;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;
import com.prestashopemc.model.Attributes;
import com.prestashopemc.model.Category;
import com.prestashopemc.model.CategoryProduct;
import com.prestashopemc.model.CmsPage;
import com.prestashopemc.model.Currency;
import com.prestashopemc.model.CustomAttribute;
import com.prestashopemc.model.CustomAttributeValue;
import com.prestashopemc.model.ExpandCategory;
import com.prestashopemc.model.HomePageList;
import com.prestashopemc.model.KeyAttributeValue;
import com.prestashopemc.model.KeyValues;
import com.prestashopemc.model.Language;
import com.prestashopemc.model.MagentoCategoryProduct;
import com.prestashopemc.model.MagentoProduct;
import com.prestashopemc.model.MultipleLanguageResult;
import com.prestashopemc.model.MultipleLanguageValue;
import com.prestashopemc.model.Option;
import com.prestashopemc.model.Product;
import com.prestashopemc.model.RecentSearchModel;
import com.prestashopemc.model.Ribbon;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.webModel.ImageBorder;
import com.prestashopemc.webModel.ImageList;
import com.prestashopemc.webModel.Link;
import com.prestashopemc.webModel.SubTitle;
import com.prestashopemc.webModel.Title;
import com.prestashopemc.webModel.WebCategory;
import com.prestashopemc.webModel.WebProduct;

import java.io.File;

/**
 * <h1>Database helper!</h1>
 * The Database helper is used to create, insert, update, delete tables for all models.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {

    /**
     * The constant DATABASE_NAME.
     */
    public static final String DATABASE_NAME = "ElitemCommerce.db";
    /**
     * The constant DATABASE_VERSION.
     */
    public static final int DATABASE_VERSION = 1;
    private Dao<Product, Integer> mProductDao = null;
    private Dao<MagentoProduct, Integer> mMagentoProductDao = null;
    private Dao<Category, Integer> mCategoryDao = null;
    private Dao<ExpandCategory, Integer> mExpandCategoryDao = null;
    private Dao<HomePageList, Integer> mHomeBannerDao = null;
    private Dao<ImageList, Integer> mSubBannerDao = null;
    private Dao<CmsPage, Integer> mCmsPageDao = null;
    private Dao<Language, Integer> mLanguageDao = null;
    private Dao<Currency, Integer> mCurrencyDao = null;
    private Dao<CategoryProduct, Integer> mCategoryProductDao = null;
    private Dao<MagentoCategoryProduct, Integer> mMagentoCategoryProductDao = null;
    private Dao<KeyValues, Integer> mKeyValueDao = null;
    private Dao<KeyAttributeValue, Integer> mkeyAttributeDao = null;
    private Dao<CustomAttribute, Integer> mCustomAttributeDao = null;
    private Dao<CustomAttributeValue, Integer> mCustomAttributeValueDao = null;
    private Dao<RecentSearchModel, Integer> mRecentSearchModelDao = null;
    private Dao<MultipleLanguageResult, Integer> mTranslationResultDao = null;
    private Dao<MultipleLanguageValue, Integer> mTranslationValueDao = null;
    private Dao<Ribbon, Integer> mRibbonDao = null;
    private Dao<Title, Integer> mTitleModelDao = null;
    private Dao<Link, Integer> mLinkModelDao = null;
    private Dao<WebCategory, Integer> mWebCategoryDao = null;
    private Dao<WebProduct, Integer> mWebProductDao = null;
    private Dao<ImageBorder, Integer> mBorderModelDao = null;
    private Dao<SubTitle, Integer> mSubTitleDao = null;
    private SQLiteDatabase db;


    /**
     * Instantiates a new Database helper.
     *
     * @param context the context
     */
    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void onCreate(SQLiteDatabase db, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, HomePageList.class);
            TableUtils.createTable(connectionSource, ImageList.class);
            TableUtils.createTable(connectionSource, Category.class);
            TableUtils.createTable(connectionSource, ExpandCategory.class);
            TableUtils.createTable(connectionSource, CmsPage.class);
            TableUtils.createTable(connectionSource, Language.class);
            TableUtils.createTable(connectionSource, Currency.class);
            TableUtils.createTable(connectionSource, RecentSearchModel.class);
            TableUtils.createTable(connectionSource, MultipleLanguageResult.class);
            TableUtils.createTable(connectionSource, MultipleLanguageValue.class);
            TableUtils.createTable(connectionSource, Ribbon.class);
            TableUtils.createTable(connectionSource, Title.class);
            TableUtils.createTable(connectionSource, Link.class);
            TableUtils.createTable(connectionSource, WebCategory.class);
            TableUtils.createTable(connectionSource, WebProduct.class);
            TableUtils.createTable(connectionSource, ImageBorder.class);
            TableUtils.createTable(connectionSource, SubTitle.class);
            if(AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)){
                TableUtils.createTable(connectionSource, MagentoProduct.class);
                TableUtils.createTable(connectionSource, MagentoCategoryProduct.class);
//                TableUtils.createTable(connectionSource, Attributes.class);
//                TableUtils.createTable(connectionSource, Option.class);
            }else {
                TableUtils.createTable(connectionSource, Product.class);
                TableUtils.createTable(connectionSource, CategoryProduct.class);
                TableUtils.createTable(connectionSource, KeyValues.class);
                TableUtils.createTable(connectionSource, CustomAttribute.class);
                TableUtils.createTable(connectionSource, CustomAttributeValue.class);
                TableUtils.createTable(connectionSource, KeyAttributeValue.class);
            }

        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't create database", e);
            throw new RuntimeException(e);
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, ConnectionSource connectionSource,
                          int oldVersion, int newVersion) {
        try {

            TableUtils.dropTable(connectionSource, HomePageList.class, true);
            TableUtils.dropTable(connectionSource, ImageList.class, true);
            TableUtils.dropTable(connectionSource, Category.class, true);
            TableUtils.dropTable(connectionSource, ExpandCategory.class, true);
            TableUtils.dropTable(connectionSource, CmsPage.class, true);
            TableUtils.dropTable(connectionSource, Language.class, true);
            TableUtils.dropTable(connectionSource, Currency.class, true);
            TableUtils.dropTable(connectionSource, RecentSearchModel.class, true);
            TableUtils.dropTable(connectionSource, MultipleLanguageResult.class, true);
            TableUtils.dropTable(connectionSource, MultipleLanguageValue.class, true);
            TableUtils.dropTable(connectionSource, Ribbon.class, true);
            TableUtils.dropTable(connectionSource, Title.class, true);
            TableUtils.dropTable(connectionSource, Link.class, true);
            TableUtils.dropTable(connectionSource, WebCategory.class, true);
            TableUtils.dropTable(connectionSource, WebProduct.class, true);
            TableUtils.dropTable(connectionSource, ImageBorder.class, true);
            TableUtils.dropTable(connectionSource, SubTitle.class, true);
            if(AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)){
                TableUtils.dropTable(connectionSource, MagentoProduct.class, true);
                TableUtils.dropTable(connectionSource, MagentoCategoryProduct.class,true);
//                TableUtils.dropTable(connectionSource, Attributes.class,true);
//                TableUtils.dropTable(connectionSource, Option.class,true);
            }else {
                TableUtils.dropTable(connectionSource, Product.class, true);
                TableUtils.dropTable(connectionSource, CategoryProduct.class, true);
                TableUtils.dropTable(connectionSource, KeyValues.class, true);
                TableUtils.dropTable(connectionSource, CustomAttribute.class, true);
                TableUtils.dropTable(connectionSource, CustomAttributeValue.class, true);
                TableUtils.dropTable(connectionSource, KeyAttributeValue.class, true);
            }
            onCreate(db, connectionSource);
        } catch (SQLException e) {
            Log.e(DatabaseHelper.class.getName(), "Can't drop databases", e);
            throw new RuntimeException(e);
        } catch (java.sql.SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    /**
     * Does database exist boolean.
     *
     * @param context the context
     * @param dbName  the db name
     * @return the boolean
     */
// to see if db exists or not
    public boolean doesDatabaseExist(Context context, String dbName) {
        try {
            File dbFile = context.getDatabasePath(dbName);
            boolean db_exists = dbFile.exists();
            return db_exists;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * Is item exists boolean.
     *
     * @param tableName the table name
     * @param openDb    the open db
     * @param fieldName the field name
     * @param Value     the value
     * @return the boolean
     */
// table has rows
    public boolean isItemExists(String tableName, boolean openDb,
                                String[] fieldName, String[] Value) {
        boolean success = false;
        try {
            if (openDb) {
                if (db == null || !db.isOpen()) {
                    db = getReadableDatabase();
                }

                if (!db.isReadOnly()) {
                    db = getReadableDatabase();
                }
            }
            String sql = "";
            if (fieldName == null || fieldName.length == 0) {
                sql = "SELECT count(*) FROM '" + tableName + "'";
            } else {
                sql = "select count(*) from " + tableName + " where ";
                int len = fieldName.length;
                for (int kk = 0; kk < len; kk++) {
                    if (kk == 0)
                        sql += fieldName[kk] + "=" + Value[kk];
                    else
                        sql += " and " + fieldName[kk] + "=" + Value[kk];
                }
            }
            Cursor cur = db.rawQuery(sql, null);
            if (cur != null) {
                cur.moveToFirst(); // Always one row returned.
                if (cur.getInt(0) == 0) { // Zero count means empty table.
                    success = false;
                } else {
                    success = true;
                }
            }
            cur.close();
        } catch (Exception e) {
        } finally {
            db.close();
        }
        return success;
    }

    /**
     * Gets product dao.
     *
     * @return the product dao
     */
    public Dao<MagentoProduct, Integer> getMagentoProductDao() {
        if (mMagentoProductDao == null) {
            try {
                mMagentoProductDao = getDao(MagentoProduct.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mMagentoProductDao;
    }

    /**
     * Gets product dao.
     *
     * @return the product dao
     */
    public Dao<Product, Integer> getProductDao() {
        if (mProductDao == null) {
            try {
                mProductDao = getDao(Product.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mProductDao;
    }


    /**
     * Gets category dao.
     *
     * @return the category dao
     */
    public Dao<Category, Integer> getCategoryDao() {
        if (mCategoryDao == null) {
            try {
                mCategoryDao = getDao(Category.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mCategoryDao;
    }

    /**
     * Gets expand category dao.
     *
     * @return the expand category dao
     */
    public Dao<ExpandCategory, Integer> getExpandCategoryDao() {
        if (mExpandCategoryDao == null) {
            try {
                mExpandCategoryDao = getDao(ExpandCategory.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mExpandCategoryDao;
    }


    /**
     * Gets home banner dao.
     *
     * @return the home banner dao
     */
    public Dao<HomePageList, Integer> getHomeBannerDao() {
        if (mHomeBannerDao == null) {
            try {
                mHomeBannerDao = getDao(HomePageList.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mHomeBannerDao;
    }


    /**
     * Gets sub banner dao.
     *
     * @return the sub banner dao
     */
    public Dao<ImageList, Integer> getSubBannerDao() {
        if (mSubBannerDao == null) {
            try {
                mSubBannerDao = getDao(ImageList.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mSubBannerDao;
    }

    /**
     * Gets cms dao.
     *
     * @return the cms dao
     */
    public Dao<CmsPage, Integer> getCmsDao() {
        if (mCmsPageDao == null) {
            try {
                mCmsPageDao = getDao(CmsPage.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mCmsPageDao;
    }

    /**
     * Gets currency dao.
     *
     * @return the currency dao
     */
    public Dao<Currency, Integer> getCurrencyDao() {
        if (mCurrencyDao == null) {
            try {
                mCurrencyDao = getDao(Currency.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mCurrencyDao;
    }

    /**
     * Gets language dao.
     *
     * @return the language dao
     */
    public Dao<Language, Integer> getLanguageDao() {
        if (mLanguageDao == null) {
            try {
                mLanguageDao = getDao(Language.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mLanguageDao;
    }


    /**
     * Gets category product dao.
     *
     * @return the category product dao
     */
    public Dao<CategoryProduct, Integer> getCategoryProductDao() {
        if (mCategoryProductDao == null) {
            try {
                mCategoryProductDao = getDao(CategoryProduct.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mCategoryProductDao;
    }

    /**
     * Gets category product dao.
     *
     * @return the category product dao
     */
    public Dao<MagentoCategoryProduct, Integer> getMagentoCategoryProductDao() {
        if (mMagentoCategoryProductDao == null) {
            try {
                mMagentoCategoryProductDao = getDao(MagentoCategoryProduct.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mMagentoCategoryProductDao;
    }

    /**
     * Gets key values dao.
     *
     * @return the key values dao
     */
    public Dao<KeyValues, Integer> getKeyValuesDao() {
        if (mKeyValueDao == null) {
            try {
                mKeyValueDao = getDao(KeyValues.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mKeyValueDao;
    }

    /**
     * Gets key attribute dao.
     *
     * @return the key attribute dao
     */
    public Dao<KeyAttributeValue, Integer> getKeyAttributeDao() {
        if (mkeyAttributeDao == null) {
            try {
                mkeyAttributeDao = getDao(KeyAttributeValue.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mkeyAttributeDao;
    }


    /**
     * Gets custom attribute dao.
     *
     * @return the custom attribute dao
     */
    public Dao<CustomAttribute, Integer> getCustomAttributeDao() {
        if (mCustomAttributeDao == null) {
            try {
                mCustomAttributeDao = getDao(CustomAttribute.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mCustomAttributeDao;
    }


    /**
     * Gets custom attribute value dao.
     *
     * @return the custom attribute value dao
     */
    public Dao<CustomAttributeValue, Integer> getCustomAttributeValueDao() {
        if (mCustomAttributeValueDao == null) {
            try {
                mCustomAttributeValueDao = getDao(CustomAttributeValue.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mCustomAttributeValueDao;
    }

    /**
     * Gets recent search model dao.
     *
     * @return the recent search model dao
     */
    public Dao<RecentSearchModel, Integer> getmRecentSearchModelDao() {
        if (mRecentSearchModelDao == null) {
            try {
                mRecentSearchModelDao = getDao(RecentSearchModel.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mRecentSearchModelDao;
    }

    /**
     * Gets translation result dao.
     *
     * @return the translation result dao
     */
    public Dao<MultipleLanguageResult, Integer> getTranslationResultDao() {
        if (mTranslationResultDao == null) {
            try {
                mTranslationResultDao = getDao(MultipleLanguageResult.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mTranslationResultDao;
    }

    /**
     * Gets translation value dao.
     *
     * @return the translation value dao
     */
    public Dao<MultipleLanguageValue, Integer> getTranslationValueDao() {
        if (mTranslationValueDao == null) {
            try {
                mTranslationValueDao = getDao(MultipleLanguageValue.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mTranslationValueDao;
    }

    /**
     * Gets ribbon dao.
     *
     * @return the ribbon dao
     */
    public Dao<Ribbon, Integer> getRibbonDao() {
        if (mRibbonDao == null) {
            try {
                mRibbonDao = getDao(Ribbon.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mRibbonDao;
    }

    /**
     * Gets title model dao.
     *
     * @return the title model dao
     */
    public Dao<Title, Integer> getTitleModelDao() {
        if (mTitleModelDao == null) {
            try {
                mTitleModelDao = getDao(Title.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mTitleModelDao;
    }

    /**
     * Gets link model dao.
     *
     * @return the link model dao
     */
    public Dao<Link, Integer> getLinkModelDao() {
        if (mLinkModelDao == null) {
            try {
                mLinkModelDao = getDao(Link.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mLinkModelDao;
    }

    /**
     * Gets web category model dao.
     *
     * @return the web category model dao
     */
    public Dao<WebCategory, Integer> getWebCategoryModelDao() {
        if (mWebCategoryDao == null) {
            try {
                mWebCategoryDao = getDao(WebCategory.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mWebCategoryDao;
    }

    /**
     * Gets web product model dao.
     *
     * @return the web product model dao
     */
    public Dao<WebProduct, Integer> getWebProductModelDao() {
        if (mWebProductDao == null) {
            try {
                mWebProductDao = getDao(WebProduct.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mWebProductDao;
    }

    /**
     * Gets border model dao.
     *
     * @return the border model dao
     */
    public Dao<ImageBorder, Integer> getBorderModelDao() {
        if (mBorderModelDao == null) {
            try {
                mBorderModelDao = getDao(ImageBorder.class);
            } catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mBorderModelDao;
    }

    /**
     * Gets sub title dao.
     *
     * @return the sub title dao
     */
    public Dao<SubTitle, Integer> getSubTitleDao() {
        if (mSubTitleDao == null){
            try {
                mSubTitleDao = getDao(SubTitle.class);
            }catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return mSubTitleDao;
    }

}