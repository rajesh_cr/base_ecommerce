package com.prestashopemc.view;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Base64;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.adapter.ViewPagerAdapter;
import com.prestashopemc.controller.HomePageController;
import com.prestashopemc.database.CategoryProductService;
import com.prestashopemc.database.MagentoCategoryProductService;
import com.prestashopemc.database.MagentoProductService;
import com.prestashopemc.database.ProductService;
import com.prestashopemc.fragment.CategoryFragment;
import com.prestashopemc.fragment.MoreFragment;
import com.prestashopemc.fragment.WebHomeFragment;
import com.prestashopemc.model.Category;
import com.prestashopemc.model.CmsPage;
import com.prestashopemc.model.Customer;
import com.prestashopemc.model.HomePage;
import com.prestashopemc.model.HomePageList;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.SharedPreference;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.zopim.android.sdk.api.ZopimChat;
import com.zopim.android.sdk.prechat.PreChatForm;
import com.zopim.android.sdk.prechat.ZopimChatActivity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>Main Activity!</h1>
 * The Main Activity implements
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3/8/16
 */
public class MainActivity extends BaseActivity {

    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    private DrawerLayout mDrawerLayout;
    private HomePageController mHomePageController;
    private ActionBarDrawerToggle mDrawerToggle;
    public List<Category> categoryList;
    public List<CmsPage> cmsList;
    public List<HomePageList> mainPageList;
    private TextView mProfileName, mSignInSignUp;
    private RelativeLayout mProfileLayout;
    private ImageView mProfileImg;
    private Customer mCustomer;
    private CategoryProductService mCategoryProductService;
    private ProductService mProductService;
    private MagentoCategoryProductService mMagentoCategoryProductService;
    private MagentoProductService mMagentoProductService;
    private LinearLayout mMainLayout, mSupportLayout;
    public final int CALL_PERMISSIOM = 123;
    private TextView mContactUs;
    private ImageView mCallImg, mMailImg, mChatImg;
    private String mPhoneNumber, mZopimKey;
    public boolean isTitle, mSearchBelowHeader;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     *
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MyApplication.getDefaultTracker("Main Screen");
        printKeyHash(this);
        initToolBar();
        initalizeLayout();
        initNetworkError();
        initDrawerLayout();

    }

    private void initSearchLayout() {
        mSearchBelowHeader = SharedPreference.getInstance().getBoolean("search_below_header");
        if (mSearchBelowHeader) {
            searchIcon.setVisibility(View.GONE);
        } else {
            searchIcon.setVisibility(View.VISIBLE);
        }
    }

    /**
     * This method is used to Refresh Cart Count
     */
    @Override
    public void onResume() {
        super.onResume();
        cartCount();
    }

    /**
     * This method is used to restart activity default method.
     */
    @Override
    protected void onRestart() {
        super.onRestart();
        refreshActivity();
    }

    /**
     * This method is used to Initialized Layout
     */
    private void initalizeLayout() {

        AppConstants.customeLanguage(getApplicationContext());
        if (AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
            mMagentoCategoryProductService = new MagentoCategoryProductService(this);
            mMagentoProductService = new MagentoProductService(this);
        } else {
            mCategoryProductService = new CategoryProductService(this);
            mProductService = new ProductService(this);
        }
        cartIcon.setVisibility(View.VISIBLE);
        logoImage.setVisibility(View.VISIBLE);
        notificationImage.setVisibility(View.GONE);
        mMainLayout = (LinearLayout) findViewById(R.id.main_layout);
        mProfileName = (TextView) findViewById(R.id.profile_name);
        mSignInSignUp = (TextView) findViewById(R.id.signIn_signUp);
        mProfileImg = (ImageView) findViewById(R.id.profile_image);
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
        mProfileLayout = (RelativeLayout) findViewById(R.id.profile_layout);
        mContactUs = (TextView) findViewById(R.id.contact_us);
        mCallImg = (ImageView) findViewById(R.id.call_img);
        mMailImg = (ImageView) findViewById(R.id.msg_img);
        mChatImg = (ImageView) findViewById(R.id.chat_img);
        mSupportLayout = (LinearLayout) findViewById(R.id.support_layout);
        mContactUs.setTypeface(proximanovaAltRegular);
        dynamicTextValue();
        mTabLayout.setupWithViewPager(mViewPager);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayout));
        mTabLayout.addOnTabSelectedListener(tabSelectedListener);
        mProfileLayout.setOnClickListener(this);
        if (!SharedPreference.getInstance().getValue("theme_color").equals("0")) {
            mProfileLayout.setBackgroundColor(Color.parseColor("#" + SharedPreference.getInstance().getValue("theme_color")));
        }
        String customer = SharedPreference.getInstance().getValue("customer");
        if (!customer.equalsIgnoreCase("0")) {
            mSignInSignUp.setVisibility(View.GONE);
            mCustomer = MyApplication.getGsonInstance().fromJson(customer, Customer.class);
            mProfileName.setText(mCustomer.getmFirstName() + " " + mCustomer.getmLastName());
            if (!mCustomer.getmProfileImage().isEmpty()) {

                Picasso.with(this).load(mCustomer.getmProfileImage())
                        .placeholder(R.drawable.ic_profile).resize(200, 200).centerCrop()
                        .transform(new CircleTransform()).into(mProfileImg);
            } else if (!SharedPreference.getInstance().getValue("social_profile_id").equals("0")) {
                //For Facebook Profile picture
                String userId = SharedPreference.getInstance().getValue("social_profile_id");
                Picasso.with(this).load("https://graph.facebook.com/" + userId + "/picture?type=large")
                        .placeholder(R.drawable.ic_profile).resize(200, 200).centerCrop()
                        .transform(new CircleTransform()).into(mProfileImg);
            } else if (!SharedPreference.getInstance().getValue("google_profile_id").equals("0")) {
                //For Google Plus Profile picture
                String userId = SharedPreference.getInstance().getValue("google_profile_id");
                Picasso.with(this).load(userId)
                        .placeholder(R.drawable.ic_profile).resize(200, 200).centerCrop()
                        .transform(new CircleTransform()).into(mProfileImg);
            }
            /*searchIcon.setVisibility(View.VISIBLE);
            searchIcon.setImageResource(R.drawable.home_notification);*/

        }
        loadHomePage();
        setCustomFont();
        if (!SharedPreference.getInstance().getValue("barcode_data").equalsIgnoreCase("0")) {
            Intent filterIntent = new Intent(MainActivity.this, ProductDetailActivity.class);
            filterIntent.putExtra("product_id", "" + SharedPreference.getInstance().getValue("barcode_data"));
            startActivity(filterIntent);
            SharedPreference mPref = SharedPreference.getInstance();
            mPref.save("barcode_data", "0");
        }
        mCallImg.setOnClickListener(this);
        mMailImg.setOnClickListener(this);
        mChatImg.setOnClickListener(this);
        searchIcon.setOnClickListener(this);
        notificationImage.setOnClickListener(this);
    }

    /**
     * This method is used to Dynamic Text Value Settings
     */
    private void dynamicTextValue() {
        mContactUs.setText(AppConstants.getTextString(MainActivity.this, AppConstants.contactUsText));
        mProfileName.setText(AppConstants.getTextString(MainActivity.this, AppConstants.welcomeGuestText));
        mSignInSignUp.setText(AppConstants.getTextString(MainActivity.this, AppConstants.signInSignUpText));
    }

    /**
     * This method is used to Set Custom Font
     */
    private void setCustomFont() {
        mProfileName.setTypeface(robotoMedium);
        mSignInSignUp.setTypeface(robotoLight);
        ViewGroup vg = (ViewGroup) mTabLayout.getChildAt(0);
        int tabsCount = vg.getChildCount();
        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab = (ViewGroup) vg.getChildAt(j);
            int tabChildsCount = vgTab.getChildCount();
            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);
                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(robotoRegular);
                }
            }
        }
    }

    /**
     * This method is used to Load Home page
     */
    public void loadHomePage() {
        showProgDialiog();
        mHomePageController = new HomePageController(MainActivity.this);
        if (SharedPreference.getInstance().getBoolean("is_version")) {
            mHomePageController.updateVersion();
        } else {
            mHomePageController.getCategoryList();
        }
    }

    /**
     * This method is used to Load Fragment
     */
    public void loadFragment() {
        cartCount();
        //HomeFragment homeFragment = new HomeFragment();
        //SubzeroHomeFragment homeFragment = new SubzeroHomeFragment();
        WebHomeFragment homeFragment = new WebHomeFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.frame_layout, homeFragment);
        transaction.commitAllowingStateLoss();
    }

    /**
     * This method is used to Initiate Navigation layout
     */
    private void initDrawerLayout() {

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.open_drawer, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
                chatButton.setVisibility(View.GONE);
                supportInvalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
                chatButton.setVisibility(View.GONE);
                supportInvalidateOptionsMenu();
            }
        };

        //Setting the actionbarToggle to drawer layout
        mDrawerLayout.addDrawerListener(mDrawerToggle);
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerToggle.setHomeAsUpIndicator(R.drawable.icon_cart);
        //calling sync state is necessay or else your hamburger icon wont show up
        mDrawerToggle.syncState();
    }

    /**
     * This method is used to Setup View Pager
     *
     * @param viewPager This is the parameter to setupViewPager method.
     */
    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new CategoryFragment(), AppConstants.getTextString(this, AppConstants.categoryText));
        adapter.addFragment(new MoreFragment(), AppConstants.getTextString(this, AppConstants.moreText));
        viewPager.setAdapter(adapter);
    }

    /**
     * This method is used to Home page success response from API
     *
     * @param homePage This is the parameter to homePageSuccess method.
     */
    public void homePageSuccess(HomePage homePage) {
        hideProgDialog();
        isTitle = true;
        loadLogo();
        categoryList = homePage.getResult().getCategories();
        cmsList = homePage.getResult().getCmsList();
        mainPageList = homePage.getResult().getHomePageList();
        loadFragment();
        setupViewPager(mViewPager);
        AppConstants.setBackgroundColor(toolbar);
        textColors();
        initSearchLayout();
        iconColors();
        mProfileLayout.setBackgroundColor(Color.parseColor("#" + SharedPreference.getInstance().getValue("theme_color")));
        //mProfileLayout.setBackgroundColor(getResources().getColor(R.color.menu_color));
        chatButton.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#" + SharedPreference.getInstance().getValue("theme_color1"))));
        navigationIcon();
        zopimAccountKey();
    }

    /**
     * This method is used to Update Version from API
     *
     * @param mCategoryList This is the first parameter to updateVersion method.
     * @param mMainPageList This is the second parameter to updateVersion method.
     * @param mCmsList      This is the third parameter to updateVersion method.
     */
    public void updateVersion(List<Category> mCategoryList, List<HomePageList> mMainPageList, List<CmsPage> mCmsList) {
        hideProgDialog();
        isTitle = false;
        this.categoryList = mCategoryList;
        this.mainPageList = mMainPageList;
        this.cmsList = mCmsList;
        loadLogo();
        loadFragment();
        setupViewPager(mViewPager);
        AppConstants.setBackgroundColor(toolbar);
        textColors();
        iconColors();
        mProfileLayout.setBackgroundColor(Color.parseColor("#" + SharedPreference.getInstance().getValue("theme_color")));
        //mProfileLayout.setBackgroundColor(getResources().getColor(R.color.menu_color));
        chatButton.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor("#" + SharedPreference.getInstance().getValue("theme_color1"))));
        initSearchLayout();
        navigationIcon();
    }

    /**
     * This method is used to Update Cart Count and Name
     *
     * @param productName This is the parameter to updateCart method.
     */
    public void updateCart(String productName) {
        cartCount();
        hideProgDialog();
        snackBar(AppConstants.getTextString(this, AppConstants.addedCartText));
    }

    /**
     * This method is used to  Home page failure response from API.
     *
     * @param failureResponse This is the parameter to failureResponse method.
     */
    public void homePageFailure(String failureResponse) {
        hideProgDialog();
        snackBar(failureResponse);
    }

    /**
     * The Tab selected listener.
     */
    TabLayout.OnTabSelectedListener tabSelectedListener = new TabLayout.OnTabSelectedListener() {
        /**
         * This method is used to find selected tab information.
         * @param tab This is the parameter to onTabReselected method.
         */
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            mViewPager.setCurrentItem(tab.getPosition());

        }

        /**
         * This method is used to find unselected tab information.
         * @param tab This is the parameter to onTabUnselected method.
         */
        @Override
        public void onTabUnselected(TabLayout.Tab tab) {
        }

        /**
         * This method is used to find reselected tab information.
         * @param tab This is the parameter to onTabReselected method.
         */
        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    /**
     * This method is used to Navigation Layout
     */

    public void closeDrawer() {
        mDrawerLayout.closeDrawers();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    /**
     * This method is used to Refresh activity
     */
    public void refreshActivity() {

        boolean isInternet = isInternetOn(this);
        if (!isInternet) {
            mMainLayout.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    refreshActivity();
                }
            });

        } else {
            mMainLayout.setVisibility(View.VISIBLE);
            networkLayout.setVisibility(View.GONE);
            SearchActivity.sSearchText = "";
            SearchActivity.sCategoryId = "";
            SearchActivity.sCategoryName = "";
            SearchActivity.sFilterName = "";
            finish();
            startActivity(getIntent());
        }
    }

    /**
     * This method is used to Wishlist Success Response
     *
     * @param result This is the parameter to wishlistSuccess method.
     */
    public void wishlistSuccess(String result) {
        hideProgDialog();
        snackBar(result);
        refreshActivity();
    }

    /**
     * This method is used to Wishlist Failure Response
     *
     * @param errormsg This is the parameter to wishlistFailure method.
     */
    public void wishlistFailure(String errormsg) {
        hideProgDialog();
        snackBar(errormsg);
    }

    /**
     * This method is used to Inner Class for picasso circle image
     */
    public class CircleTransform implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap,
                    BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);

            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "circle";
        }
    }

    /**
     * This method is used to Print Hash Key
     *
     * @param context This is the parameter to printKeyHash method.
     * @return String returns string value of key hash.
     */
    public static String printKeyHash(Activity context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getApplicationContext().getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);
            String finalPackageName = context.getApplicationContext().getPackageName();
            SharedPreference.getInstance().save("package", finalPackageName);

            for (android.content.pm.Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));
                // String key = new String(Base64.encodeBytes(md.digest()));
            }
        } catch (PackageManager.NameNotFoundException e1) {
        } catch (NoSuchAlgorithmException e) {
        } catch (Exception e) {
        }
        return key;
    }

    /**
     * This method is used to Call Intent
     */
    private void callIntent() {
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.setData(Uri.parse("tel:" + mPhoneNumber));
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        startActivity(intent);
    }

    /**
     * This method is used to Check Lollipop version
     *
     * @return boolean returns boolean value of lollipop device or not.
     */
    private boolean isLollipop() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    /**
     * This method is used to Check System Permissions
     */
    private void loadPermissions() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(MainActivity.this,
                Manifest.permission.CALL_PHONE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(MainActivity.this, Manifest.permission.CALL_PHONE)) {
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(MainActivity.this);
                alertBuilder.setCancelable(true);
                alertBuilder.setTitle("Permission necessary");
                alertBuilder.setMessage("Call permission is necessary");
                alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CALL_PHONE}, CALL_PERMISSIOM);
                    }
                });
                AlertDialog alert = alertBuilder.create();
                alert.show();

            } else {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.CALL_PHONE}, CALL_PERMISSIOM);
            }
        } else {
            callIntent();
        }
    }

    /**
     * This method is used to Check System Permissions
     *
     * @param requestCode  This is the first parameter to onRequestPermissionsResult method.
     * @param permissions  This is the second parameter to onRequestPermissionsResult method.
     * @param grantResults This is the third parameter to onRequestPermissionsResult method.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case CALL_PERMISSIOM:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callIntent();
                } else {
                    //callIntent();
                }
                return;
        }
    }

    /**
     * This method is used to Navigation Icon.
     */
    private void navigationIcon() {
        if (!SharedPreference.getInstance().getValue("customer").equalsIgnoreCase("0")) {

            mSearchBelowHeader = SharedPreference.getInstance().getBoolean("search_below_header");
            if (mSearchBelowHeader) {
                notificationImage.setVisibility(View.GONE);
                searchIcon.setVisibility(View.VISIBLE);
                searchIcon.setImageBitmap(customIcon(BitmapFactory.decodeResource(getResources(), R.drawable.home_notification)));
            } else {
                searchIcon.setVisibility(View.VISIBLE);
                notificationImage.setVisibility(View.VISIBLE);
                notificationImage.setImageBitmap(customIcon(BitmapFactory.decodeResource(getResources(), R.drawable.home_notification)));
            }
        }

        mZopimKey = SharedPreference.getInstance().getValue("zopim_id");

        mSupportLayout.setOrientation(LinearLayout.HORIZONTAL);
        if (!mZopimKey.equals("0") && !mZopimKey.isEmpty()) {
            LinearLayout.LayoutParams mSupportParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
            mSupportLayout.setLayoutParams(mSupportParams);

            LinearLayout.LayoutParams mParams = new LinearLayout.LayoutParams((int) getResources().getDimension(R.dimen.more_fragment_img),
                    (int) getResources().getDimension(R.dimen.more_fragment_img), 0.33f);
            mChatImg.setVisibility(View.VISIBLE);
            mCallImg.setLayoutParams(mParams);
            mMailImg.setLayoutParams(mParams);
            mChatImg.setLayoutParams(mParams);
        } else {

            LinearLayout.LayoutParams mParams = new LinearLayout.LayoutParams((int) getResources().getDimension(R.dimen.more_fragment_img),
                    (int) getResources().getDimension(R.dimen.more_fragment_img));

            LinearLayout.LayoutParams mParam = new LinearLayout.LayoutParams((int) getResources().getDimension(R.dimen.more_fragment_img),
                    (int) getResources().getDimension(R.dimen.more_fragment_img));
            mParams.setMargins(0, 0, 30, 0);
            mParam.setMargins(30, 0, 0, 0);
            mCallImg.setLayoutParams(mParams);
            mMailImg.setLayoutParams(mParam);
        }
    }

    /**
     * This method is used to on click function.
     *
     * @param v This is the parameter to onClick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * Edit Profile Click Listener*/
            case R.id.profile_layout:
                closeDrawer();
                if (!SharedPreference.getInstance().getValue("customerid").equals("0")) {
                    Intent editProfile = new Intent(MainActivity.this, EditProfileActivity.class);
                    startActivity(editProfile);
                } else {
                    if(AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)){
                        mMagentoCategoryProductService.deleteCategoryProductServiceRecord();
                        mMagentoProductService.deleteProductServiceRecord();
                    }else {
                        mCategoryProductService.deleteCategoryProductServiceRecord();
                        mProductService.deleteProductServiceRecord();
                    }

                    Intent intent = new Intent(MainActivity.this, AccountActivity.class);
                    startActivity(intent);
                }
                break;

            /**
             * Call Click Listener*/
            case R.id.call_img:
                mPhoneNumber = SharedPreference.getInstance().getValue("call_back_number");
                closeDrawer();
                if (!mPhoneNumber.equals("0")) {
                    //callIntent();
                    if (!isLollipop()) {
                        callIntent();
                    } else {
                        loadPermissions();
                    }
                } else {
                    snackBar(AppConstants.getTextString(MainActivity.this, AppConstants.phoneNumberErrorAlertText));
                }
                break;

            /**
             * Mail Click Listener
             */
            case R.id.msg_img:
                closeDrawer();
                Intent contactIntent = new Intent(MainActivity.this, ContactUsActivity.class);
                startActivity(contactIntent);
                break;

            /**
             * Chat Click Listener
             */
            case R.id.chat_img:
                closeDrawer();
                // set pre chat fields as mandatory
                PreChatForm preChatForm = new PreChatForm.Builder()
                        .name(PreChatForm.Field.REQUIRED_EDITABLE)
                        .email(PreChatForm.Field.REQUIRED_EDITABLE)
                        .phoneNumber(PreChatForm.Field.REQUIRED_EDITABLE)
                        .department(PreChatForm.Field.REQUIRED_EDITABLE)
                        .message(PreChatForm.Field.REQUIRED_EDITABLE)
                        .build();

                // build chat config
                ZopimChat.SessionConfig config = new ZopimChat.SessionConfig().preChatForm(preChatForm);

                // start chat activity with config
                ZopimChatActivity.startActivity(MainActivity.this, config);

                // Sample breadcrumb
                ZopimChat.trackEvent("Started chat with mandatory pre-chat form");
                break;

            /**
             * notification Click Listener
             */
            case R.id.notification_icon:
                Intent notification = new Intent(MainActivity.this, NotificationActivity.class);
                startActivity(notification);
                break;

            /**
             * search Click Listener
             */
            case R.id.search_icon:
                if (!SharedPreference.getInstance().getValue("customer").equalsIgnoreCase("0")) {

                    mSearchBelowHeader = SharedPreference.getInstance().getBoolean("search_below_header");
                    if (mSearchBelowHeader) {
                        Intent notifi = new Intent(MainActivity.this, NotificationActivity.class);
                        startActivity(notifi);
                    } else {
                        Intent recentSearchIntent = new Intent(MainActivity.this, RecentSearchActivity.class);
                        startActivity(recentSearchIntent);
                    }
                }else {
                    Intent recentSearchIntent = new Intent(MainActivity.this, RecentSearchActivity.class);
                    startActivity(recentSearchIntent);
                }
                break;
        }
    }
}
