package com.prestashopemc.view;

import com.j256.ormlite.field.SqlType;
import com.j256.ormlite.field.types.SerializableType;

import java.lang.reflect.Field;
import java.util.Collection;

/**
 * Created by root on 6/3/17.
 */

public class LocalSerializableType  extends SerializableType {

            		private static LocalSerializableType singleton;

           		public LocalSerializableType() {
        			super(SqlType.SERIALIZABLE, new Class<?>[0]);
        		}

            		public static LocalSerializableType getSingleton() {
        			if (singleton == null) {
            				singleton = new LocalSerializableType();
            			}
        		return singleton;
        		}

            		@Override
    		public boolean isValidForField(Field field) {
        			return Collection.class.isAssignableFrom(field.getType());
        		}
    }


