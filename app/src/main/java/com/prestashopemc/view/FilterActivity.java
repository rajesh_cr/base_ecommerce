package com.prestashopemc.view;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.prestashopemc.R;
import com.prestashopemc.adapter.FilterExpandableAdapter;
import com.prestashopemc.adapter.NavigationFilterAdapter;
import com.prestashopemc.adapter.SearchFilterCategoryAdapter;
import com.prestashopemc.controller.ProductListController;
import com.prestashopemc.customView.CustomTextView;
import com.prestashopemc.model.FilterMain;
import com.prestashopemc.model.FilterOption;
import com.prestashopemc.model.FilterResult;
import com.prestashopemc.model.NavigationFilterMain;
import com.prestashopemc.model.NavigationFilterPrices;
import com.prestashopemc.model.NavigationFilterResult;
import com.prestashopemc.model.NavigationFilterValues;
import com.prestashopemc.model.NavigationFilterWeight;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.MyApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * Created by dinakaran on 18/3/16.
 */

/**
 * <h1>Filter Activity!</h1>
 * The Filter Activity implements filtering products to display.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 18 /3/16
 */
public class FilterActivity extends BaseActivity {

    private RecyclerView mFilterRecyclerView;
    private CustomTextView mFilterClear, mFilterApply;
    private ProductListController mProductController;
    /**
     * The Filter result.
     */
    public List<FilterResult> filterResult;
    private FilterExpandableAdapter mFilterAdapter;
    private NavigationFilterAdapter mNavigationFilterAdapter;
    private String mCategoryId, mCategoryname;
    private Bundle mBundle;
    private List<FilterResult> mGroupList = new ArrayList<>();
    private List<NavigationFilterResult> mGroupResultList = new ArrayList<>();
    private List<List<FilterOption>> mChildList = new ArrayList<List<FilterOption>>();
    private NavigationFilterPrices mChildFilterPrice;
    private NavigationFilterWeight mChildFilterWeight;
    private List<List<NavigationFilterValues>> mChildFilterValues = new ArrayList<List<NavigationFilterValues>>();
    /**
     * The constant sFilterObject.
     */
    public static JSONObject sFilterObject, sNavigationFilterFeatureObject, sNavigationFilterAttributeObject,
            sPriceObject, sWeightObject;
    public static ArrayList<String> sConditionObject = new ArrayList<>();
    public static ArrayList<String> sManufactureObject = new ArrayList<>();
    private List<NavigationFilterResult> navigationFilterResult;
    private RelativeLayout mCategoryLayout;
    private TextView mCategoryLabel, mCategotyName;
    private ImageView mCategoryEditImg;
    public static JSONArray mConditionArray = new JSONArray();
    public static JSONArray mManufacturesArray = new JSONArray();
    public static String sAvailability, sCondition, sManFactures;
    /*
     * The constant sCatId.
     */
    public static String sCatId, /**
     * The S categories id.
     */
    sCategoriesId, /**
     * The S filter.
     */
    sFilter;
    private LinearLayout mLinearLayout;
    private RelativeLayout mMainLayout;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     *
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_activity);
        MyApplication.getDefaultTracker("Filter Screen");
        initToolBar();
        initNetworkError();
        showProgDialiog();
        categoryTitle.setVisibility(View.GONE);
        clearAll.setVisibility(View.GONE);
        searchIcon.setVisibility(View.GONE);
        cartIcon.setVisibility(View.GONE);
        title.setVisibility(View.VISIBLE);
        title.setAllCaps(true);
        title.setText(AppConstants.getTextString(this, AppConstants.filterByText));
        logoImage.setVisibility(View.GONE);
        mBundle = getIntent().getExtras();
        if (mBundle != null) {
            mCategoryId = mBundle.getString("category_id");
            mCategoryname = mBundle.getString("category_name");
            sFilter = mBundle.getString("filter");
        }
        mMainLayout = (RelativeLayout) findViewById(R.id.main_layout);
        initializeLayout();
        mProductController.filterAttributeList(mCategoryId);
    }

    /**
     * This method is used to Initialize Layout
     **/
    private void initializeLayout() {
        boolean isInternet = isInternetOn(this);
        if (!isInternet) {
            mMainLayout.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initializeLayout();
                }
            });
            hideProgDialog();

        } else {
            //hideProgDialog();
            mMainLayout.setVisibility(View.VISIBLE);
            networkLayout.setVisibility(View.GONE);
            mFilterRecyclerView = (RecyclerView) findViewById(R.id.filter_recyclerview);
            mFilterApply = (CustomTextView) findViewById(R.id.filter_apply);
            mFilterClear = (CustomTextView) findViewById(R.id.filter_clear);
            mCategoryLayout = (RelativeLayout) findViewById(R.id.category_name_layout);
            mCategoryLabel = (TextView) findViewById(R.id.category_name_label);
            mCategotyName = (TextView) findViewById(R.id.category_select_name);
            mCategoryEditImg = (ImageView) findViewById(R.id.category_edit_img);
            mLinearLayout = (LinearLayout) findViewById(R.id.linearLayout);
            if (mCategoryname != null) {
                mCategotyName.setText(mCategoryname);
                mCategoryLayout.setVisibility(View.VISIBLE);
            }
            mProductController = new ProductListController(FilterActivity.this);
            mFilterRecyclerView.setLayoutManager(new LinearLayoutManager(FilterActivity.this));
            mFilterRecyclerView.setHasFixedSize(true);
            mFilterClear.setTextColor(Color.parseColor(CustomBackground.mThemeColor));
            mFilterApply.setBackgroundColor(Color.parseColor(CustomBackground.mThemeColor));
            mFilterApply.setOnClickListener(this);
            mFilterClear.setOnClickListener(this);
            mCategoryLayout.setOnClickListener(this);
            dynamicTextValue();
            setCustomFont();
            if (sCategoriesId == null) {
                sCategoriesId = mCategoryId;
            } else if (!sCategoriesId.equals(mCategoryId)) {
                FilterExpandableAdapter.mSelectedFilter.clear();
                sCategoriesId = mCategoryId;
            }
        }
    }

    /**
     * This method is used to Dynamic Text Value Settings
     */
    private void dynamicTextValue() {
        mCategoryLabel.setText(AppConstants.getTextString(this, AppConstants.categoryText));
        mFilterClear.setText(AppConstants.getTextString(this, AppConstants.clearText));
        mFilterApply.setText(AppConstants.getTextString(this, AppConstants.applyText));
    }

    /**
     * This method is used to Set Custom Font
     */
    private void setCustomFont() {
        mFilterClear.setTypeface(robotoRegular);
        mFilterApply.setTypeface(robotoRegular);
        mCategoryLabel.setTypeface(robotoRegular);
        mCategotyName.setTypeface(robotoLight);
    }

    /**
     * This method is used to Update Filter Api Success Response
     *
     * @param mFilterMain This is the parameter to updateFilterView method.
     */
    public void updateFilterView(FilterMain mFilterMain) {
        hideProgDialog();
        mLinearLayout.setVisibility(View.VISIBLE);
        filterResult = mFilterMain.getResult();
        mFilterAdapter = new FilterExpandableAdapter(FilterActivity.this, getFilters());
        mFilterRecyclerView.setAdapter(mFilterAdapter);
    }

    /**
     * Navigation filtered Response */
    public void updateNavigationFilterView(NavigationFilterMain navigationFilterMain) {
        hideProgDialog();
        mLinearLayout.setVisibility(View.VISIBLE);
        navigationFilterResult = navigationFilterMain.getResult();
        mNavigationFilterAdapter = new NavigationFilterAdapter(FilterActivity.this, getNavigationFilter());
        mFilterRecyclerView.setAdapter(mNavigationFilterAdapter);
    }

    /**
     * This method is used to Update Filter Api failure Response
     *
     * @param string This is the parameter to updateFailure method.
     */
    public void updateFailure(String string) {
        hideProgDialog();
        snackBar(string);
    }

    /**
     * This method is used to Get list of Navigation Filters
     *
     * @return List<ParentListItem>,
     **/
    private List<ParentListItem> getNavigationFilter() {

        List<ParentListItem> parentListItems = new ArrayList<>();

        for (NavigationFilterResult parentList : navigationFilterResult) {
            List<NavigationFilterValues> childValuesList = new ArrayList<>();
            NavigationFilterPrices childPricesList = new NavigationFilterPrices();
            NavigationFilterWeight childWeightList = new NavigationFilterWeight();
            for (NavigationFilterValues valuesFilter : parentList.getValues()) {
                valuesFilter.setAttributeId(parentList.getId());
                childValuesList.add(valuesFilter);
            }
            childPricesList.setAttributeId(parentList.getId());
            childWeightList.setAttributeId(parentList.getId());
            mChildFilterValues.add(childValuesList);
            mChildFilterPrice = parentList.getPrices();
            mChildFilterWeight = parentList.getWeight();
            parentListItems.add(parentList);
        }
        mGroupResultList.addAll(navigationFilterResult);

        return parentListItems;
    }


    /**
     * This method is used to Get list of Filters
     *
     * @return List<ParentListItem>,
     **/
    private List<ParentListItem> getFilters() {

        List<ParentListItem> parentListItems = new ArrayList<>();

        for (FilterResult parenList : filterResult) {
            List<FilterOption> childList = new ArrayList<>();
            for (FilterOption filter : parenList.getOptions()) {
                filter.setAttributeId(parenList.getAttributeId());
                childList.add(filter);
            }
            mChildList.add(childList);
            parentListItems.add(parenList);
        }
        mGroupList.addAll(filterResult);

        return parentListItems;
    }

    /**
     * This is filter Request url method
     **/
    private JSONObject applyFilter() {
        JSONObject attributeValue = new JSONObject();

        for (int i = 0; i < mGroupList.size(); i++) {
            JSONArray attributeArray = new JSONArray();
            String substring = "";
            for (int j = 0; j < mChildList.get(i).size(); j++) {
                for (int k = 0; k < FilterExpandableAdapter.mSelectedFilter.size(); k++) {
                    if (mChildList.get(i).get(j).getValue().equals(FilterExpandableAdapter.mSelectedFilter.get(k))) {

                        if(AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)){
                            substring = mGroupList.get(i).getName();

                        }/*else{
                            substring = mChildList.get(i).get(j)
                                    .getAttributeId();
                        }*/

                        attributeArray.put(FilterExpandableAdapter.mSelectedFilter.get(k));
                    }
                }
            }
            try {
                if (!substring.isEmpty())
                    attributeValue.put(substring,
                            attributeArray);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return attributeValue;
    }

    /**
     * This is filter Request url method
     **/
    private JSONObject applyFeatureFilter() {
        JSONObject attributeValue = new JSONObject();

        for (int i = 0; i < mGroupResultList.size(); i++) {
            JSONArray attributeArray = new JSONArray();
            String substring = "";
            for (int j = 0; j < mChildFilterValues.get(i).size(); j++) {
                for (int k = 0; k < NavigationFilterAdapter.mFeatureArray.size(); k++) {
                    if (mChildFilterValues.get(i).get(j).getValue().equals(NavigationFilterAdapter.mFeatureArray.get(k))) {

                        substring = mChildFilterValues.get(i).get(j).getAttributeId();
                        attributeArray.put(NavigationFilterAdapter.mFeatureArray.get(k));
                    }
                }
            }
            try {
                if (!substring.isEmpty())
                    attributeValue.put(substring,
                            attributeArray);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return attributeValue;
    }

    /**
     * This is filter Request url method
     **/
    private JSONObject applyAttributeFilter() {
        JSONObject attributeValue = new JSONObject();

        for (int i = 0; i < mGroupResultList.size(); i++) {
            JSONArray attributeArray = new JSONArray();
            String substring = "";
            for (int j = 0; j < mChildFilterValues.get(i).size(); j++) {
                for (int k = 0; k < NavigationFilterAdapter.mAttributeArray.size(); k++) {
                    if (mChildFilterValues.get(i).get(j).getValue().equals(NavigationFilterAdapter.mAttributeArray.get(k))) {

                        substring = mChildFilterValues.get(i).get(j).getAttributeId();
                        attributeArray.put(NavigationFilterAdapter.mAttributeArray.get(k));
                    }
                }
            }
            try {
                if (!substring.isEmpty())
                    attributeValue.put(substring,
                            attributeArray);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        return attributeValue;
    }



    public String applyConditionFilter() {
        for (int i = 0; i < NavigationFilterAdapter.mConditionObject.size(); i++) {
            mConditionArray.put(NavigationFilterAdapter.mConditionObject.get(i));
        }
        return mConditionArray.toString();
    }


    public String applyManfactureFilter() {

        for (int i = 0; i < NavigationFilterAdapter.mManufacturesObject.size(); i++) {
            mManufacturesArray.put(NavigationFilterAdapter.mManufacturesObject.get(i));
        }
        return mManufacturesArray.toString();
    }

    /*
    This is the method for onClick
    @param v This is the parameter to onclick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * Category Edit Listener*/
            case R.id.category_name_layout:
                Intent filterIntent = new Intent(FilterActivity.this, SearchFilterActivity.class);
                startActivity(filterIntent);
                finish();
                break;

            /**
             * Apply filter
             **/
            case R.id.filter_apply:
                if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                    if (sFilter.equals("productList")) {
                        ProductListActivity.sIsFilter = true;
                        sFilterObject = applyFilter();
                    }
                    if (sFilter.equals("searchList")) {
                        SearchActivity.sIsFilter = true;
                        sFilterObject = applyFilter();
                        sCatId = SearchFilterCategoryAdapter.mCategoryId;
                        SearchFilterActivity.mFa.finish();
                    }
                }else {
                    if (sFilter.equals("productList")) {
                        ProductListActivity.sIsFilter = true;
                        sNavigationFilterFeatureObject = applyFeatureFilter();
                        sNavigationFilterAttributeObject = applyAttributeFilter();

                        sCondition = applyConditionFilter();
                        sManFactures = applyManfactureFilter();
                        sAvailability = NavigationFilterAdapter.mAvailability;
                        sPriceObject = NavigationFilterAdapter.mPriceObject;
                        sWeightObject = NavigationFilterAdapter.mWeightObject;
                    }
                    if (sFilter.equals("searchList")) {
                        SearchActivity.sIsFilter = true;
                        sNavigationFilterFeatureObject = applyFeatureFilter();
                        sNavigationFilterAttributeObject = applyAttributeFilter();
                        sCondition = applyConditionFilter();
                        sManFactures = applyManfactureFilter();
                        sAvailability = NavigationFilterAdapter.mAvailability;
                        sPriceObject = NavigationFilterAdapter.mPriceObject;
                        sWeightObject = NavigationFilterAdapter.mWeightObject;
                        sCatId = SearchFilterCategoryAdapter.mCategoryId;
                        SearchFilterActivity.mFa.finish();
                    }
                }

                finish();
                break;

            /**
             * Clear Filter
             **/
            case R.id.filter_clear:
                ProductListActivity.sIsFilter = false;
                if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                    FilterExpandableAdapter.mSelectedFilter.clear();
                    mFilterAdapter = new FilterExpandableAdapter(FilterActivity.this, getFilters());
                    mFilterRecyclerView.setAdapter(mFilterAdapter);
                }else {
                    NavigationFilterAdapter.mFeatureArray.clear();
                    NavigationFilterAdapter.mAttributeArray.clear();
                    NavigationFilterAdapter.mConditionObject.clear();
                    NavigationFilterAdapter.mManufacturesObject.clear();
                    NavigationFilterAdapter.mAvailability = "";
                    try {
                        mConditionArray = new JSONArray("[]");
                        mManufacturesArray = new JSONArray("[]");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    mNavigationFilterAdapter = new NavigationFilterAdapter(FilterActivity.this, getNavigationFilter());
                    mFilterRecyclerView.setAdapter(mNavigationFilterAdapter);
                }
                break;
        }
    }
}

