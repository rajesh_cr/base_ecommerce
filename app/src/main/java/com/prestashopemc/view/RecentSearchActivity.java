package com.prestashopemc.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.adapter.PredictiveSearchAdapter;
import com.prestashopemc.adapter.RecentSearchAdapter;
import com.prestashopemc.controller.ProductListController;
import com.prestashopemc.database.RecentSearchService;
import com.prestashopemc.model.PredictiveSearchModel;
import com.prestashopemc.model.PredictiveSearchResult;
import com.prestashopemc.model.RecentSearchModel;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.MyApplication;

import java.util.ArrayList;
import java.util.List;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>Recent Search Activity!</h1>
 * The Recent Search Activity implements recent search details and functions.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class RecentSearchActivity extends BaseActivity {

    private ImageView mBackArrow;
    private EditText mSearchEdit;
    private RelativeLayout mRecentClearLayout, mMainLayout;
    private TextView mRecentLabel, mClearAllLabel;
    private RecyclerView mRecentListView;
    private RecentSearchModel mRecentSearchModel;
    private RecentSearchService mRecentSearchService;
    private RecentSearchAdapter mRecentSearchAdapter;
    private PredictiveSearchAdapter mPredictiveSearchAdapter;
    private String stringEntered;
    private ProductListController mProductListController;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recent_search);
        MyApplication.getDefaultTracker("Recent Search Screen");
        initNetworkError();
        mMainLayout = (RelativeLayout) findViewById(R.id.recent_main_layout);
        initializedLayout();
    }

    /**
     * This method is used to Initialized Layout
     */
    private void initializedLayout() {

        boolean isInternet = isInternetOn(this);
        if (!isInternet) {
            mMainLayout.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initializedLayout();
                }
            });
        } else {
            mMainLayout.setVisibility(View.VISIBLE);
            networkLayout.setVisibility(View.GONE);
            mRecentSearchService = new RecentSearchService(this);
            mBackArrow = (ImageView) findViewById(R.id.back_arrow);
            mSearchEdit = (EditText) findViewById(R.id.search_edit);
            mRecentClearLayout = (RelativeLayout) findViewById(R.id.label_layout);
            mRecentLabel = (TextView) findViewById(R.id.recent_label);
            mClearAllLabel = (TextView) findViewById(R.id.clear_label);
            mRecentListView = (RecyclerView) findViewById(R.id.recent_search_list);
            mRecentListView.setLayoutManager(new LinearLayoutManager(this));
            mClearAllLabel.setOnClickListener(this);
            mBackArrow.setOnClickListener(this);
            mClearAllLabel.setText(AppConstants.getTextString(this, AppConstants.clearAllText));
            mRecentLabel.setText(AppConstants.getTextString(this, AppConstants.recentlySearchesText));
            mSearchEdit.setHint(AppConstants.getTextString(this, AppConstants.searchPlaceHolderText));
            mProductListController = new ProductListController(RecentSearchActivity.this);
            setCustomFont();
            mSearchEdit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
                @Override
                public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                    if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER))
                            || (actionId == EditorInfo.IME_ACTION_DONE)) {
                        if (!mSearchEdit.getText().toString().trim().isEmpty()) {
                            mRecentSearchModel = new RecentSearchModel();
                            mRecentSearchModel.setSearchKeyword(mSearchEdit.getText().toString().trim());
                            List<RecentSearchModel> recentSearchModels = new ArrayList<RecentSearchModel>();
                            recentSearchModels.add(mRecentSearchModel);
                            try {
                                mRecentSearchService.insertRecentSearch(recentSearchModels);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Intent searchIntent = new Intent(RecentSearchActivity.this, SearchActivity.class);
                            searchIntent.putExtra("search_text", mSearchEdit.getText().toString());
                            mSearchEdit.getText().clear();
                            startActivity(searchIntent);
                            finish();
                        } else {
                            //snackBarRecentSearchActivity(AppConstants.getTextString(RecentSearchActivity.this, AppConstants.searchKeywordText));
                            Intent searchIntent = new Intent(RecentSearchActivity.this, SearchActivity.class);
                            searchIntent.putExtra("search_text", "All products");
                            mSearchEdit.getText().clear();
                            startActivity(searchIntent);
                            finish();
                        }
                    }
                    return false;
                }
            });

            mSearchEdit.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

                    try {
                        final StringBuilder stringBuilder = new StringBuilder(arg0.length());
                        stringBuilder.append(arg0);
                        stringEntered = stringBuilder.toString();
                        mProductListController.predictiveSearch(stringEntered);
                    } catch (Exception e) {
                        // error
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

                }

                @Override
                public void afterTextChanged(Editable arg0) {

                }
            });

            getRecentSearchValues();
        }
    }

    /**
     * Set Custom Font
     */
    private void setCustomFont() {
        mSearchEdit.setTypeface(robotoLight);
        mRecentLabel.setTypeface(robotoRegular);
        mClearAllLabel.setTypeface(robotoRegular);
    }

    /**
     * Getting Recent Search Values From DB
     */
    public void getRecentSearchValues() {
        List<RecentSearchModel> recentValues = new ArrayList<>();
        try {
            recentValues = mRecentSearchService.retrieveRecentSearch();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (recentValues.size() != 0) {
            mRecentClearLayout.setVisibility(View.VISIBLE);
            mRecentSearchAdapter = new RecentSearchAdapter(this, recentValues);
            mRecentListView.setAdapter(mRecentSearchAdapter);
            mRecentSearchAdapter.notifyDataSetChanged();
        } else {
            mRecentClearLayout.setVisibility(View.GONE);
            mRecentSearchAdapter = new RecentSearchAdapter(this, recentValues);
            mRecentListView.setAdapter(mRecentSearchAdapter);
            mRecentSearchAdapter.notifyDataSetChanged();
        }
    }

    /**
     * This method is used to on click events.
     * @param v This is the parameter to onClick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * Delete All Recent Search Values From DB*/
            case R.id.clear_label:
                mRecentSearchService.deleteRecentSearchRecord();
                getRecentSearchValues();
                break;

            /**
             * Back Click listener*/
            case R.id.back_arrow:
                finish();
                break;
        }
    };


    /**
     * Predictive Success Response
     *
     * @param predictiveSearchModelResponse This is the parameter to predictiveSuccess method.
     */
    public void predictiveSuccess(PredictiveSearchModel predictiveSearchModelResponse) {
//        hideProgDialog();
        List<PredictiveSearchResult> predictiveSearchResults = new ArrayList();
        try {
            predictiveSearchResults = predictiveSearchModelResponse.getResult();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (predictiveSearchResults.size() != 0) {
            mRecentClearLayout.setVisibility(View.GONE);
            mPredictiveSearchAdapter = new PredictiveSearchAdapter(this, predictiveSearchResults, stringEntered);
            mRecentListView.setAdapter(mPredictiveSearchAdapter);
            mPredictiveSearchAdapter.notifyDataSetChanged();
        } else {
            mRecentClearLayout.setVisibility(View.GONE);
            mPredictiveSearchAdapter = new PredictiveSearchAdapter(this, predictiveSearchResults, stringEntered);
            mRecentListView.setAdapter(mPredictiveSearchAdapter);
            mPredictiveSearchAdapter.notifyDataSetChanged();
        }
    }

    /**
     * This method is used to Predictive Failure Response
     *
     * @param result This is the parameter predictiveFailure method.
     */
    public void predictiveFailure(String result) {
        hideProgDialog();
        getRecentSearchValues();
        //snackBarRecentSearchActivity(result);
    }

    /**
     * This method is used to snack bar display for recent search.
     *
     * @param response This is the parameter to snackBarRecentSearchActivity method.
     */
    public void snackBarRecentSearchActivity(String response) {
        final Snackbar snackbar = Snackbar.make(mMainLayout, response, Snackbar.LENGTH_SHORT);
        final View snackBarView = snackbar.getView();
        AppConstants.setBackgroundColor(snackBarView);

        final InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        snackBarView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            /*
            This method is used to display custom snackbar
             */
            @Override
            public void onGlobalLayout() {

                Rect r = new Rect();
                snackBarView.getWindowVisibleDisplayFrame(r);
                int screenHeight = snackBarView.getRootView().getHeight();
                int keypadHeight = screenHeight - r.bottom;

                if (keypadHeight > screenHeight * 0.15) {
                    im.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

                } else {
                }
            }
        });
        TextView tv = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.parseColor(CustomBackground.mTitleTextColor));
        snackbar.show();
    }

}
