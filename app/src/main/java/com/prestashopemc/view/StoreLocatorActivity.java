package com.prestashopemc.view;

import android.Manifest;
import android.app.Dialog;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prestashopemc.R;
import com.prestashopemc.adapter.PopupAdapter;
import com.prestashopemc.controller.LicenseController;
import com.prestashopemc.model.LocatorMain;
import com.prestashopemc.model.LocatorResult;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.MyApplication;

import java.util.List;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>Store Locator Activity!</h1>
 * The Store Locator Activity implements store locator map .
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 19 /8/16
 */
public class StoreLocatorActivity extends BaseActivity implements OnMapReadyCallback {

    private List<LocatorResult> mLocatorResults;
    private LicenseController mLicenseController;
    private GoogleMap mGoogleMap;
    private MapView mMapView;
    private RelativeLayout mMainLayout;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     *
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store_locator);
        MyApplication.getDefaultTracker("Store Locator Screen");
        initToolBar();
        initNetworkError();
        logoImage.setVisibility(View.GONE);
        searchIcon.setVisibility(View.GONE);
        cartIcon.setVisibility(View.GONE);
        cartText.setVisibility(View.GONE);
        title.setVisibility(View.VISIBLE);
        //hideProgDialog();
        title.setText(AppConstants.getTextString(this, AppConstants.storeLocateText));
        title.setAllCaps(true);
        mMapView = (MapView) findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();
        mMainLayout = (RelativeLayout) findViewById(R.id.main_layout);
        initializedLayout();
    }

    /**
     * This method is used to Initialized Layout
     */
    public void initializedLayout() {
        showProgDialiog();

        boolean isInternet = isInternetOn(this);
        if (!isInternet) {
            mMainLayout.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initializedLayout();
                }
            });
            hideProgDialog();

        } else {
            mMainLayout.setVisibility(View.VISIBLE);
            networkLayout.setVisibility(View.GONE);
            mLicenseController = new LicenseController(StoreLocatorActivity.this);
            toolbar.setBackgroundColor(Color.parseColor("#" + SharedPreference.getInstance().getValue("theme_color")));
            mLicenseController.locationList();
        }
    }

    /**
     * This method is used to Google Map Locations Method
     */
    public void mapIntegrate() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());
        if (status != ConnectionResult.SUCCESS) {
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this, requestCode);
            dialog.show();
        } else {
            try {
                MapsInitializer.initialize(this);
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    mGoogleMap.setMyLocationEnabled(true);
                    mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
                } else {
                    Log.e("Location Error ==== ", " Marshmallow ");
                }

            } else {
                mGoogleMap.setMyLocationEnabled(true);
                mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
            }

            if (mLocatorResults.size() != 0) {

                String lat = "";
                String lng = "";
                for (int i = 0; i < mLocatorResults.size(); i++) {
                    lat = String.valueOf(mLocatorResults.get(i).getLatitude());
                    lng = String.valueOf(mLocatorResults.get(i).getLongitude());
                    drawMarker(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng)));
                }
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(Double.parseDouble(lat), Double.parseDouble(lng))));
                mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(6));
            }
            mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    LatLng lt = marker.getPosition();
                    //Log.e("LatLng == ", lt.latitude+", "+lt.longitude);
                    for (LocatorResult mResult : mLocatorResults) {
                        if (lt.latitude == mResult.getLatitude() && lt.longitude == mResult.getLongitude()) {
                            Log.e("Location Id == ", mResult.getLocationId());
                            mGoogleMap.setInfoWindowAdapter(new PopupAdapter(getLayoutInflater(), mResult, StoreLocatorActivity.this));
                        }
                    }
                    return false;
                }
            });
        }
    }

    /**
     * This method is used to Set Markers for multi positions
     *
     * @param point This is the parameter to drawMarker method.
     */
    private void drawMarker(LatLng point) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(point);
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.locator_icon));
        mGoogleMap.addMarker(markerOptions);
    }

    /**
     * This method is used to Error Response
     *
     * @param errorResponse This is the parameter to error method.
     */
    public void error(String errorResponse) {
        hideProgDialog();
        snackBar(errorResponse);
    }

    /**
     * This method is used to Success Response
     *
     * @param locatorMain This is the parameter to success method.
     */
    public void success(LocatorMain locatorMain) {
        hideProgDialog();
        mLocatorResults = locatorMain.getResult();
        mapIntegrate();
    }

    /**
     * This method is used to onMapReady
     *
     * @param googlemap This is the parameter to onMapReady method.
     */
    @Override
    public void onMapReady(GoogleMap googlemap) {
        mGoogleMap = googlemap;
    }
}
