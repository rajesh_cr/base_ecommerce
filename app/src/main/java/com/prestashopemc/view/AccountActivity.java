package com.prestashopemc.view;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Toast;

import com.prestashopemc.R;
import com.prestashopemc.fragment.CreateAccountFragment;
import com.prestashopemc.fragment.LoginFragment;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.SharedPreference;

import java.util.List;

/**
 * <h1>Account Activity Contains Login and Registration!</h1>
 * The Account Activity implements an Login and Register account that
 * simply .
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class AccountActivity extends BaseActivity {
    private Bundle mBundle;
    /**
     * The Guest.
     */
    public String guest;
    /**
     * The My permissions request read external storage.
     */
    public final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        MyApplication.getDefaultTracker("Account Screen");
        initToolBar();
        categoryTitle.setVisibility(View.VISIBLE);
        categoryTitle.setPadding(0, 13, 0, 0);
        categoryTitle.setTextSize(16);
        categoryTitle.setText(AppConstants.getTextString(this, AppConstants.loginText));
        categoryTitle.setAllCaps(true);
        searchIcon.setVisibility(View.GONE);
        cartIcon.setVisibility(View.GONE);
        logoImage.setVisibility(View.GONE);
        mBundle = getIntent().getExtras();
        if (mBundle != null) {
            guest = mBundle.getString("guest");
        }
        setCustomFont();
        loadFragment();
    }

    /**
     * This method is used to Set Custom Font.
     * @return Nothing.
     */
    private void setCustomFont() {
        categoryTitle.setTypeface(openSansCondBold);
    }

    /**
     * This method is used to Load Fragment.
     * @return Nothing.
     */
    private void loadFragment() {
        LoginFragment homeFragment = new LoginFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(R.id.frame_layout, homeFragment);
        transaction.commit();
    }

    /**
     * This method is used to Login Success Response From API
     *
     * @return Nothing.
     */
    public void updateLogin() {
        hideProgDialog();
        MyApplication.eventTracking("Login", "Login success", "Login");
        Toast.makeText(AccountActivity.this, AppConstants.getTextString(this, AppConstants.loginSuccessText), Toast.LENGTH_SHORT).show();
        finish();
    }

    /**
     * This method is used to Create Account & Login Failure Response.
     *
     * @param string This is the parameter to failure method.
     * @return Nothing.
     */
    public void failure(String string) {
        hideProgDialog();
        snackBar(string);
    }

    /**
     * This method is used to Create Account Success Response From API
     *
     * @return Nothing.
     */
    public void updateCreateAccount() {
        //hideProgDialog();
        MyApplication.eventTracking("Account", "Account created", "Account");
        if (SharedPreference.getInstance().getValue("socialLogin").equals("0")) {
            Toast.makeText(AccountActivity.this, AppConstants.getTextString(this, AppConstants.createSuccessText), Toast.LENGTH_SHORT).show();
        }else {
            Toast.makeText(AccountActivity.this, AppConstants.getTextString(this, AppConstants.loginSuccessText), Toast.LENGTH_SHORT).show();
        }
        SharedPreference.getInstance().save("socialLogin", "1");
        finish();
    }

    /**
     * This method is used to onRequestPermissionsResult result for getting photo permission from gallery.
     * @param requestCode This is the first parameter to onRequestPermissionsResult method.
     * @param permissions This is the second parameter to onRequestPermissionsResult method.
     * @param grantResults This is the third parameter to onRequestPermissionsResult method.
     * @return Nothing.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    CreateAccountFragment createAccount = (CreateAccountFragment) getVisibleFragment();
                    if (createAccount.userChoosenTask.equalsIgnoreCase(AppConstants.getTextString(this, AppConstants.takePhotoText))) {
                        createAccount.cameraIntent();
                    } else {
                        createAccount.galleryIntent();
                    }

                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
//                    Toast.makeText(EditMenuActivity.this, R.string.location_permission_denied, Toast.LENGTH_LONG).show();
//                    loadPermissions();
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    /**
     * This method is used to getting visible fragment.
     *
     * @return Fragment. This returns visible fragment object value.
     */
    public Fragment getVisibleFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        List<Fragment> fragments = fragmentManager.getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null && fragment.isVisible())
                    return fragment;
            }
        }
        return null;
    }

    /**
     * This method is used to Forgot Password Success Response.
     *
     * @return Nothing.
     */
    public void success() {
        snackBar(AppConstants.getTextString(this, AppConstants.referRegisteredMailText));
    }
}
