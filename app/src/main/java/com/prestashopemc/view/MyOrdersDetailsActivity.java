package com.prestashopemc.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.adapter.MyOrderDetailAdapter;
import com.prestashopemc.controller.LoginController;
import com.prestashopemc.model.MyOrderDetailBillingaddress;
import com.prestashopemc.model.MyOrderDetailItem;
import com.prestashopemc.model.MyOrderDetailMain;
import com.prestashopemc.model.MyOrderDetailOrderData;
import com.prestashopemc.model.MyOrderDetailShippingAddress;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.MyApplication;

import java.util.ArrayList;
import java.util.List;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>My Order Details Activity!</h1>
 * The My Order Details Activity implements my order details.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author Rajesh
 * @version 1.2
 * @since 22 -10-2016
 */
public class MyOrdersDetailsActivity extends BaseActivity {

    private Bundle mBundle;
    private String mOrderId;
    private LoginController mLoginController;
    private MyOrderDetailOrderData mMyOrderDetailOrderData;
    private MyOrderDetailBillingaddress mMyOrderDetailBillingaddress;
    private MyOrderDetailShippingAddress mMyOrderDetailShippingAddress;
    private List<MyOrderDetailItem> mMyOrderDetailItems = new ArrayList<>();
    private TextView mProductDetailLabel, mShippingAddressLabel, mShippingName, mShippingAddress, mBillingAddressLabel,
            mBillingName, mBillingAddress, mPaymentMethodLabel, mPaymentMethod, mShippingMethodLabel, mShippingMethod,
            mPaymentSummaryLabel, mSubtotalLabel, mSubtotal, mShippingHandlingLabel, mShippingHandling, mTaxLabel, mTax,
            mDiscountLabel, mDiscount, mOrderIdLabel, mOrderIdText, mPlacedLabel, mPlaced, mStatusLabel, mStatus, mAmountLabel,
            mAmount;
    private MyOrderDetailAdapter mMyOrderDetailAdapter;
    private RecyclerView mListView;
    private ScrollView mOrderMainLayout;
    private RelativeLayout mMainLayout;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     *
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myorders_details);
        MyApplication.getDefaultTracker("My order Details Screen");
        initToolBar();
        initNetworkError();
        logoImage.setVisibility(View.GONE);
        searchIcon.setVisibility(View.GONE);
        cartIcon.setVisibility(View.GONE);
        cartText.setVisibility(View.GONE);
        title.setVisibility(View.VISIBLE);
        title.setText(AppConstants.getTextString(this, AppConstants.orderDetailsPage));
        title.setAllCaps(true);
        mMainLayout = (RelativeLayout) findViewById(R.id.main_layout);
        mOrderMainLayout = (ScrollView) findViewById(R.id.order_detail_layout);
        showProgDialiog();
        mBundle = getIntent().getExtras();
        if (mBundle != null) {
            mOrderId = mBundle.getString("orderId");
        }
        initializedLayout();
    }

    /**
     * This method is used to Initialized Layout
     */
    private void initializedLayout() {
        showProgDialiog();
        boolean isInternet = isInternetOn(this);
        if (!isInternet) {
            mMainLayout.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initializedLayout();
                }
            });
            hideProgDialog();

        } else {
            mMainLayout.setVisibility(View.VISIBLE);
            networkLayout.setVisibility(View.GONE);
            mLoginController = new LoginController(MyOrdersDetailsActivity.this);
            mListView = (RecyclerView) findViewById(R.id.order_detail_list_view);
            mListView.setLayoutManager(new LinearLayoutManager(this));
            mProductDetailLabel = (TextView) findViewById(R.id.order_detail_product_label);
            mShippingAddressLabel = (TextView) findViewById(R.id.order_detail_shipping_label);
            mShippingName = (TextView) findViewById(R.id.shipping_name);
            mShippingAddress = (TextView) findViewById(R.id.ship_address);
            mBillingAddressLabel = (TextView) findViewById(R.id.order_detail_billing_label);
            mBillingName = (TextView) findViewById(R.id.billing_name);
            mBillingAddress = (TextView) findViewById(R.id.bill_address);
            mPaymentMethodLabel = (TextView) findViewById(R.id.order_payment_method);
            mPaymentMethod = (TextView) findViewById(R.id.order_payment_type);
            mShippingMethodLabel = (TextView) findViewById(R.id.order_shipping_method);
            mShippingMethod = (TextView) findViewById(R.id.order_shipping_type);
            mPaymentSummaryLabel = (TextView) findViewById(R.id.payment_summary);
            mSubtotalLabel = (TextView) findViewById(R.id.payment_subtotal_label);
            mSubtotal = (TextView) findViewById(R.id.payment_subTotal);
            mShippingHandlingLabel = (TextView) findViewById(R.id.payment_shipping_label);
            mShippingHandling = (TextView) findViewById(R.id.payment_shippingCost);
            mTaxLabel = (TextView) findViewById(R.id.payment_tax_label);
            mTax = (TextView) findViewById(R.id.payment_tax);
            mDiscountLabel = (TextView) findViewById(R.id.payment_discount_label);
            mDiscount = (TextView) findViewById(R.id.payment_discount);
            mOrderIdLabel = (TextView) findViewById(R.id.order_no_label);
            mOrderIdText = (TextView) findViewById(R.id.order_no);
            mPlacedLabel = (TextView) findViewById(R.id.order_date_label);
            mPlaced = (TextView) findViewById(R.id.order_date);
            mStatusLabel = (TextView) findViewById(R.id.order_success_label);
            mStatus = (TextView) findViewById(R.id.order_success);
            mAmountLabel = (TextView) findViewById(R.id.payment_amount_label);
            mAmount = (TextView) findViewById(R.id.payment_amount);
            dynamicTextValue();
            setCustomFont();
            mLoginController.orderInformation(mOrderId);

        }
    }

    /**
     * This method is used to Dynamic Text Value Settings
     */
    private void dynamicTextValue() {
        mProductDetailLabel.setText(AppConstants.getTextString(this, AppConstants.productDetailsText));
        mOrderIdLabel.setText(AppConstants.getTextString(this, AppConstants.orderIdLabelText));
        mPlacedLabel.setText(AppConstants.getTextString(this, AppConstants.placedOnText));
        mStatusLabel.setText(AppConstants.getTextString(this, AppConstants.statusText));
        mShippingAddressLabel.setText(AppConstants.getTextString(this, AppConstants.shippingAddressText));
        mBillingAddressLabel.setText(AppConstants.getTextString(this, AppConstants.billingAddressText));
        mPaymentMethodLabel.setText(AppConstants.getTextString(this, AppConstants.paymentMethodText));
        mShippingMethodLabel.setText(AppConstants.getTextString(this, AppConstants.shippingMethodText));
        mPaymentSummaryLabel.setText(AppConstants.getTextString(this, AppConstants.paymentSummaryText));
        mSubtotalLabel.setText(AppConstants.getTextString(this, AppConstants.subTotalText));
        mShippingHandlingLabel.setText(AppConstants.getTextString(this, AppConstants.paymentShippingHandlingText));
        mTaxLabel.setText(AppConstants.getTextString(this, AppConstants.taxText));
        mDiscountLabel.setText(AppConstants.getTextString(this, AppConstants.discountText));
        mAmountLabel.setText(AppConstants.getTextString(this, AppConstants.amountPayableText));
    }

    /**
     * This method is used to Set Custom Font
     */
    private void setCustomFont() {
        mProductDetailLabel.setTypeface(robotoMedium);
        mShippingAddressLabel.setTypeface(robotoMedium);
        mShippingName.setTypeface(robotoRegular);
        mShippingAddress.setTypeface(robotoLight);
        mBillingAddressLabel.setTypeface(robotoMedium);
        mBillingName.setTypeface(robotoRegular);
        mBillingAddress.setTypeface(robotoLight);
        mPaymentMethodLabel.setTypeface(robotoMedium);
        mPaymentMethod.setTypeface(robotoLight);
        mShippingMethodLabel.setTypeface(robotoMedium);
        mShippingMethod.setTypeface(robotoLight);
        mPaymentSummaryLabel.setTypeface(robotoMedium);
        mSubtotalLabel.setTypeface(robotoRegular);
        mSubtotal.setTypeface(robotoLight);
        mShippingHandlingLabel.setTypeface(robotoRegular);
        mShippingHandling.setTypeface(robotoLight);
        mTaxLabel.setTypeface(robotoRegular);
        mTax.setTypeface(robotoLight);
        mDiscountLabel.setTypeface(robotoRegular);
        mDiscount.setTypeface(robotoLight);
        mOrderIdLabel.setTypeface(robotoRegular);
        mOrderIdText.setTypeface(robotoLight);
        mPlacedLabel.setTypeface(robotoRegular);
        mPlaced.setTypeface(robotoLight);
        mStatusLabel.setTypeface(robotoRegular);
        mStatus.setTypeface(robotoLight);
        mAmountLabel.setTypeface(robotoMedium);
        mAmount.setTypeface(robotoMedium);
    }

    /**
     * This method is used to Information Success Response
     *
     * @param myOrderDetailMain This is the parameter to informationSuccess method.
     */
    public void informationSuccess(MyOrderDetailMain myOrderDetailMain) {
        hideProgDialog();
        mOrderMainLayout.setVisibility(View.VISIBLE);
        mMyOrderDetailOrderData = myOrderDetailMain.getOrderData();
        mMyOrderDetailBillingaddress = mMyOrderDetailOrderData.getBillingaddress();
        mMyOrderDetailShippingAddress = mMyOrderDetailOrderData.getShippingAddress();
        mMyOrderDetailItems = mMyOrderDetailOrderData.getItems();
        mMyOrderDetailAdapter = new MyOrderDetailAdapter(MyOrdersDetailsActivity.this, mMyOrderDetailItems, mMyOrderDetailOrderData.getCurrencySymbol());
        mListView.setAdapter(mMyOrderDetailAdapter);
        setValues();
    }

    /**
     * This method is used to Information Failure Response
     *
     * @param errormsg This is the parameter to informationFailure method.
     */
    public void informationFailure(String errormsg) {
        hideProgDialog();
        snackBar(errormsg);
    }

    /**
     * This method is used to Set Values
     */
    private void setValues() {
        hideProgDialog();
        mOrderIdText.setText(mOrderId);
        mPlaced.setText(mMyOrderDetailOrderData.getOrderDate());
        mStatus.setText(mMyOrderDetailOrderData.getStatus());
        mShippingName.setText(mMyOrderDetailShippingAddress.getFirstname() + " " + mMyOrderDetailShippingAddress.getLastname());
        mShippingAddress.setText(mMyOrderDetailShippingAddress.getStreet() + "\n" + mMyOrderDetailShippingAddress.getCity() + ", " +
                mMyOrderDetailShippingAddress.getCountry() + "\n" + mMyOrderDetailShippingAddress.getTelephone());
        mBillingName.setText(mMyOrderDetailBillingaddress.getFirstname() + " " + mMyOrderDetailBillingaddress.getLastname());
        mBillingAddress.setText(mMyOrderDetailBillingaddress.getStreet() + "\n" + mMyOrderDetailBillingaddress.getCity() + ", " +
                mMyOrderDetailBillingaddress.getCountry() + "\n" + mMyOrderDetailBillingaddress.getTelephone());
        mPaymentMethod.setText(mMyOrderDetailOrderData.getPaymentMethod());
        mShippingMethod.setText(mMyOrderDetailOrderData.getShippingMethod());
        mSubtotal.setText(mMyOrderDetailOrderData.getCurrencySymbol() + " " + mMyOrderDetailOrderData.getSubTotal());
        mShippingHandling.setText(mMyOrderDetailOrderData.getCurrencySymbol() + " " + mMyOrderDetailOrderData.getShippingAmount());
        mTax.setText(mMyOrderDetailOrderData.getCurrencySymbol() + " " + mMyOrderDetailOrderData.getTaxAmount());
        mDiscount.setText(mMyOrderDetailOrderData.getCurrencySymbol() + " " + mMyOrderDetailOrderData.getDiscountAmount());
        mAmount.setText(mMyOrderDetailOrderData.getCurrencySymbol() + " " + mMyOrderDetailOrderData.getGrandTotal());
    }

}
