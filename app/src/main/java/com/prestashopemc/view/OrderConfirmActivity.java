package com.prestashopemc.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.analytics.ecommerce.ProductAction;
import com.paypal.android.sdk.payments.PayPalConfiguration;
import com.paypal.android.sdk.payments.PayPalPayment;
import com.paypal.android.sdk.payments.PayPalService;
import com.paypal.android.sdk.payments.PaymentActivity;
import com.paypal.android.sdk.payments.PaymentConfirmation;
import com.prestashopemc.R;
import com.prestashopemc.controller.CheckoutController;
import com.prestashopemc.fragment.PaymentFragment;
import com.prestashopemc.model.ChequeDetail;
import com.prestashopemc.model.OrderConfirmResult;
import com.prestashopemc.model.OrderDetail;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.MyApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>Order Confirm Activity!</h1>
 * The Order Confirm Activity implements order confirm details.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author Rajesh
 * @version 1.2
 * @since 8 /11/16
 */
public class OrderConfirmActivity extends BaseActivity {

    private String mPaymentMethodCode, mPaymentIdentifier;
    private static CheckoutController mCheckoutController;
    private Bundle mBundle;
    private RelativeLayout mOrderCodLayout, mOrderFailureLayout, mOrderBankWireLayout, mMainLayout;
    private TextView mCodThankText, mCodOrderReceivedText, mCodOrderIdLabel, mCodOrderId, mCodOrderDescription;
    private TextView mOrderFailureText, mOrderFailureCancelledText, mOrderFailureOk;
    private TextView mBankWireThankText, mBankWireReceivedText, mBankWireOrderIdLabel, mBankWireOrderId, mBankWireOrderDescription,
            mBankWirePriceLabel, mBankWireAmount, mBankWireDetailLabel, mBankWireNameLabel, mBankWireName,
            mBankWireAccLabel, mBankWireAcc, mBankWireIfscLabel, mBankWireIfsc, mBankWireBankNameLabel, mBankWireBankName,
            mBankWireBranchLabel, mBankWireBranch, mBankWireBottomText, mBankWireBottomMainLabel, mBankWireBottomSubLabel;
    private OrderConfirmResult mOrderConfirmResult = new OrderConfirmResult();
    private OrderDetail mOrderDetail = new OrderDetail();
    private ChequeDetail mChequeDetail = new ChequeDetail();
    private static String CONFIG_ENVIRONMENT;
    private static final int REQUEST_CODE_PAYMENT = 1;
    public static String sPaypalClientId,sPaypalMode,sPaySecretKey;
    private PayPalConfiguration mConfig;
    private static String sBase64ClientID = null;
    public static String sAccessToken = "", mTransId;
    private static String sId;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderconfirm);
        MyApplication.getDefaultTracker("Order Confirm Screen");
        initToolBar();
        initNetworkError();
        logoImage.setVisibility(View.GONE);
        title.setVisibility(View.VISIBLE);
        title.setAllCaps(true);
        title.setText(AppConstants.getTextString(this, AppConstants.orderPageStatusText));
        cartText.setVisibility(View.GONE);
        cartIcon.setVisibility(View.GONE);
        mBundle = getIntent().getExtras();
        if (mBundle != null) {
            mPaymentMethodCode = mBundle.getString("paymentMethodCode");
            mPaymentIdentifier = mBundle.getString("paymentIdentifier");
        }
        mMainLayout = (RelativeLayout) findViewById(R.id.main_layout);
        showProgDialiog();
        initializedLayout();
        paypalService();
    }

    /**
     * This method is used to Initialized Layout
     */
    private void initializedLayout() {
        boolean isInternet = isInternetOn(this);
        if (!isInternet) {
            mMainLayout.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initializedLayout();
                }
            });
            hideProgDialog();

        } else {
            mMainLayout.setVisibility(View.VISIBLE);
            networkLayout.setVisibility(View.GONE);
            hideProgDialog();
            mCheckoutController = new CheckoutController(OrderConfirmActivity.this);

            if (mPaymentIdentifier.contains("paypal")) {
                paypalIntent();
            } else {
                mCheckoutController.orderPlace(mPaymentMethodCode, "0");
            }
            /**
             * For RelativeLayout*/
            mOrderCodLayout = (RelativeLayout) findViewById(R.id.cod_layout);
            mOrderFailureLayout = (RelativeLayout) findViewById(R.id.order_failure_layout);
            mOrderBankWireLayout = (RelativeLayout) findViewById(R.id.bankwire_success_layout);
            /**
             * For COD Layout Texts*/
            mCodThankText = (TextView) findViewById(R.id.order_thank);
            mCodOrderReceivedText = (TextView) findViewById(R.id.order_has_received);
            mCodOrderIdLabel = (TextView) findViewById(R.id.order_id_label);
            mCodOrderId = (TextView) findViewById(R.id.order_id);
            mCodOrderDescription = (TextView) findViewById(R.id.order_description);
            /**
             * For Order Failure Layout Texts*/
            mOrderFailureText = (TextView) findViewById(R.id.wrong_text);
            mOrderFailureCancelledText = (TextView) findViewById(R.id.order_cancelled_label);
            mOrderFailureOk = (TextView) findViewById(R.id.order_cancel_ok);
            /**
             * For Order Bank Wire Layout Texts*/
            mBankWireThankText = (TextView) findViewById(R.id.success_thank);
            mBankWireReceivedText = (TextView) findViewById(R.id.success_order_has_received);
            mBankWireOrderIdLabel = (TextView) findViewById(R.id.success_order_id_label);
            mBankWireOrderId = (TextView) findViewById(R.id.success_order_id);
            mBankWireOrderDescription = (TextView) findViewById(R.id.success_order_description);
            mBankWirePriceLabel = (TextView) findViewById(R.id.success_bank_wire_label);
            mBankWireAmount = (TextView) findViewById(R.id.order_amount_label);
            mBankWireDetailLabel = (TextView) findViewById(R.id.account_details);
            mBankWireNameLabel = (TextView) findViewById(R.id.account_name_label);
            mBankWireName = (TextView) findViewById(R.id.account_name);
            mBankWireAccLabel = (TextView) findViewById(R.id.account_number_label);
            mBankWireAcc = (TextView) findViewById(R.id.account_number);
            mBankWireIfscLabel = (TextView) findViewById(R.id.account_ifsc_label);
            mBankWireIfsc = (TextView) findViewById(R.id.account_ifsc);
            mBankWireBankNameLabel = (TextView) findViewById(R.id.account_bank_label);
            mBankWireBankName = (TextView) findViewById(R.id.account_bank);
            mBankWireBranchLabel = (TextView) findViewById(R.id.account_branch_label);
            mBankWireBranch = (TextView) findViewById(R.id.account_branch);
            mBankWireBottomText = (TextView) findViewById(R.id.bankwire_bottom_text);
            mBankWireBottomMainLabel = (TextView) findViewById(R.id.bankwire_bottom_main_label);
            mBankWireBottomSubLabel = (TextView) findViewById(R.id.bankwire_bottom_sub_label);
            mOrderFailureOk.setTextColor(Color.parseColor(CustomBackground.mThemeColor));
            CustomBackground.setBackgroundRedWhiteCombination(mOrderFailureOk);
            dynamicTextValue();
            setCustomFont();
            mOrderFailureOk.setOnClickListener(this);
        }
    }

    /**
     * This method is used to Dynamic Text Value Settings
     */
    private void dynamicTextValue() {
        mCodOrderReceivedText.setText(AppConstants.getTextString(this, AppConstants.orderStatusText));
        mCodThankText.setText(AppConstants.getTextString(this, AppConstants.thanksText));
        mCodOrderIdLabel.setText(AppConstants.getTextString(this, AppConstants.orderIdText));
        mCodOrderDescription.setText(AppConstants.getTextString(this, AppConstants.orderConfirmationText));
        mOrderFailureText.setText(AppConstants.getTextString(this, AppConstants.whoopsText));
        mOrderFailureCancelledText.setText(AppConstants.getTextString(this, AppConstants.orderCancelText));
        mOrderFailureOk.setText(AppConstants.getTextString(this, AppConstants.okText));
        mBankWireThankText.setText(AppConstants.getTextString(this, AppConstants.thanksText));
        mBankWireReceivedText.setText(AppConstants.getTextString(this, AppConstants.orderStatusText));
        mBankWireOrderIdLabel.setText(AppConstants.getTextString(this, AppConstants.orderIdText));
        mBankWireOrderDescription.setText(AppConstants.getTextString(this, AppConstants.orderConfirmationText));
        mBankWirePriceLabel.setText(AppConstants.getTextString(this, AppConstants.sendBankWireText));
        mBankWireAmount.setText(AppConstants.getTextString(this, AppConstants.orderAmountText) + " -");
        mBankWireDetailLabel.setText(AppConstants.getTextString(this, AppConstants.bankAccountDetailText) + " :");
        mBankWireNameLabel.setText(AppConstants.getTextString(this, AppConstants.accountnameText));
        mBankWireAccLabel.setText(AppConstants.getTextString(this, AppConstants.accountNoText));
        mBankWireIfscLabel.setText(AppConstants.getTextString(this, AppConstants.accountBanknametext));
        mBankWireBankNameLabel.setText(AppConstants.getTextString(this, AppConstants.accountBanknametext));
        mBankWireBranchLabel.setText(AppConstants.getTextString(this, AppConstants.accountBranchText));
        mBankWireBottomMainLabel.setText(AppConstants.getTextString(this, AppConstants.insertOrderReferenceText));
        mBankWireBottomSubLabel.setText(AppConstants.getTextString(this, AppConstants.subOfBankWireText));
    }

    /**
     * This method is used to Set Custom Font
     */
    private void setCustomFont() {
        /**
         * For COD Text*/
        mCodThankText.setTypeface(robotoMedium);
        mCodOrderReceivedText.setTypeface(robotoLight);
        mCodOrderIdLabel.setTypeface(robotoLight);
        mCodOrderId.setTypeface(robotoBold);
        mCodOrderDescription.setTypeface(robotoLight);
        /**
         * For Failure Layout Text*/
        mOrderFailureText.setTypeface(robotoRegular);
        mOrderFailureCancelledText.setTypeface(robotoLight);
        mOrderFailureOk.setTypeface(robotoMedium);
        /**
         *  For BankWire Layout Text*/
        mBankWireThankText.setTypeface(robotoMedium);
        mBankWireReceivedText.setTypeface(robotoLight);
        mBankWireOrderIdLabel.setTypeface(robotoLight);
        mBankWireOrderId.setTypeface(robotoBold);
        mBankWireOrderDescription.setTypeface(robotoLight);
        mBankWirePriceLabel.setTypeface(robotoRegular);
        mBankWireAmount.setTypeface(robotoMedium);
        mBankWireDetailLabel.setTypeface(robotoRegular);
        mBankWireNameLabel.setTypeface(robotoLight);
        mBankWireName.setTypeface(robotoRegular);
        mBankWireAccLabel.setTypeface(robotoLight);
        mBankWireAcc.setTypeface(robotoRegular);
        mBankWireIfscLabel.setTypeface(robotoLight);
        mBankWireIfsc.setTypeface(robotoRegular);
        mBankWireBankNameLabel.setTypeface(robotoLight);
        mBankWireBankName.setTypeface(robotoRegular);
        mBankWireBranchLabel.setTypeface(robotoLight);
        mBankWireBranch.setTypeface(robotoRegular);
        mBankWireBottomText.setTypeface(robotoRegular);
        mBankWireBottomMainLabel.setTypeface(robotoLight);
        mBankWireBottomSubLabel.setTypeface(robotoLight);
    }

    /**
     * This method is used to Confirm Order Failure Response
     *
     * @param string This is the parameter to failure method.
     */
    public void failure(String string) {
        hideProgDialog();
        if (string.equals(getString(R.string.cartFailureText))) {
            cartIdDelete();
            deleteGuestDetails();
            deleteValue();
            mOrderFailureLayout.setVisibility(View.VISIBLE);
            snackBar(string);
        } else {
            deleteGuestDetails();
            deleteValue();
            mOrderFailureLayout.setVisibility(View.VISIBLE);
            snackBar(string);
        }
    }

    /**
     * This method is used to Confirm Order Success Response
     *
     * @param mOrderConfirmModel This is the parameter to updateOrderStatus method.
     */
    public void updateOrderStatus(OrderConfirmResult mOrderConfirmModel) {
        hideProgDialog();
        SharedPreference.getInstance().removeValue("couponCode");
        mOrderConfirmResult = mOrderConfirmModel;
        mOrderDetail = mOrderConfirmModel.getOrderDetail();
        mChequeDetail = mOrderConfirmResult.getChequeDetail();
        deleteGuestDetails();
        deleteValue();

        ProductAction productAction = new ProductAction(ProductAction.ACTION_PURCHASE)
                .setTransactionId(mOrderConfirmResult.getOrderid())
                .setTransactionAffiliation("Google Store - Online")
                .setTransactionRevenue(Double.parseDouble(mOrderConfirmResult.getGrandtotal()))
                .setTransactionTax(Double.parseDouble(PaymentFragment.sTax))
                .setTransactionShipping(Double.parseDouble(PaymentFragment.sShippingAmount))
                .setTransactionCouponCode(CartActivity.sCouponCode);
        HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder()
                .setProductAction(productAction);

        GoogleAnalytics googleAnalytics = GoogleAnalytics.getInstance(OrderConfirmActivity.this);
        //Tracker sTracker = googleAnalytics.newTracker(R.xml.app_tracker);
        Tracker sTracker = googleAnalytics.newTracker(SharedPreference.getInstance().getValue("google_tracking"));
        sTracker.setScreenName("Transaction");
        sTracker.send(builder.build());

        if (mOrderDetail != null) {
            cartIdDelete();
            mOrderBankWireLayout.setVisibility(View.VISIBLE);
            mBankWireBottomMainLabel.setVisibility(View.VISIBLE);
            mBankWireBottomSubLabel.setVisibility(View.VISIBLE);
            mBankWireIfscLabel.setVisibility(View.GONE);
            mBankWireIfsc.setVisibility(View.GONE);
            mBankWireOrderId.setText(" #" + mOrderConfirmResult.getOrderid());
            mBankWireBottomText.setText(mOrderDetail.getReference());
            mBankWireName.setText(mOrderDetail.getBankWireOwner());
            mBankWireAcc.setText(mOrderDetail.getBankWireDetails());
            mBankWireIfsc.setText(mOrderDetail.getBankWireAddress());
            Spannable amountLabel = new SpannableString(AppConstants.getTextString(this, AppConstants.orderAmountText) + " -");
            amountLabel.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.order_amount_id)), 0, amountLabel.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            mBankWireAmount.setText(amountLabel + "  " + SharedPreference.getInstance().getValue("currency_symbol") + mOrderConfirmResult.getGrandtotal());
        } else if (mChequeDetail != null) {
            cartIdDelete();
            mOrderBankWireLayout.setVisibility(View.VISIBLE);
            mBankWireBottomMainLabel.setVisibility(View.GONE);
            mBankWireBottomSubLabel.setVisibility(View.GONE);
            mBankWireBankNameLabel.setVisibility(View.GONE);
            mBankWireIfscLabel.setVisibility(View.GONE);
            mBankWireIfsc.setVisibility(View.GONE);
            mBankWireOrderId.setText(" #" + mOrderConfirmResult.getOrderid());
            mBankWireDetailLabel.setText(AppConstants.getTextString(this, AppConstants.chequeAccountDetails));
            mBankWireNameLabel.setText(AppConstants.getTextString(this, AppConstants.chequeName));
            mBankWireAccLabel.setText(AppConstants.getTextString(this, AppConstants.chequeDetails));
            mBankWireName.setText(mChequeDetail.getChequeName());
            mBankWireAcc.setText(mChequeDetail.getChequeAddress());
            Spannable amountLabel = new SpannableString(AppConstants.getTextString(this, AppConstants.orderAmountText));
            amountLabel.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.order_amount_id)), 0, amountLabel.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            mBankWireAmount.setText(amountLabel + "  " + SharedPreference.getInstance().getValue("currency_symbol") + mOrderConfirmResult.getGrandtotal());
        } else {
            cartIdDelete();
            mOrderCodLayout.setVisibility(View.VISIBLE);
            mCodOrderId.setText(" #" + mOrderConfirmResult.getOrderid());
        }
    }

    /**
     * This method is used to back pressed.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        deleteGuestDetails();
        deleteValue();
        CartActivity.sCartActivity.finish();
    }

    /**
     * This method is used to Clear Cart and Address details
     */
    public void clearData() {
        deleteGuestDetails();
        deleteValue();
    }


    /**
     * This method useful to start paypal service.
     */
    public void paypalService() {

        sPaypalClientId = SharedPreference.getInstance().getValue("paypal_client_id");
        sPaypalMode = SharedPreference.getInstance().getValue("paypal_mode");
        sPaySecretKey = SharedPreference.getInstance().getValue("paypal_secret_key");

        if (!sPaypalClientId.equalsIgnoreCase("")) {

            if (sPaypalMode.equalsIgnoreCase("0")) {
                AppConstants.paypalTransUrl = "https://api.sandbox.paypal.com/v1/payments/payment/";
                AppConstants.oauthUrl = "https://api.sandbox.paypal.com/v1/oauth2/token";
                CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_SANDBOX;
            } else {
                AppConstants.paypalTransUrl = "https://api.paypal.com/v1/payments/payment/";
                AppConstants.oauthUrl = "https://api.paypal.com/v1/oauth2/token";
                CONFIG_ENVIRONMENT = PayPalConfiguration.ENVIRONMENT_PRODUCTION;
            }

            mConfig = new PayPalConfiguration()
                    .environment(CONFIG_ENVIRONMENT)
                    .clientId(OrderConfirmActivity.sPaypalClientId)
                    .merchantName("Base Pack")
                    .merchantPrivacyPolicyUri(Uri.parse("https://www.example.com/privacy"))
                    .merchantUserAgreementUri(Uri.parse("https://www.example.com/legal"));
            Intent intent = new Intent(OrderConfirmActivity.this, PayPalService.class);
            intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, mConfig);
            startService(intent);
        } else {
        }
    }

    /**
     * This method shows use of optional payment details and item list.
     * @param environment This is the parameter to getStuffToBuy method.
     */
    private PayPalPayment getStuffToBuy(String environment) {
        BigDecimal amount = new BigDecimal(SharedPreference.getInstance().getValue("grand_total").replaceAll(",", ""));
        PayPalPayment payment = new PayPalPayment(amount, SharedPreference.getInstance().getValue("currency_iso_code"), getResources().getString(R.string.app_name), environment);
        return payment;
    }

    /**
     * This method is used to Starts payment Activity.
     */
    private void paypalIntent() {
        PayPalPayment thingToBuy = getStuffToBuy(PayPalPayment.PAYMENT_INTENT_SALE);
        Intent paypalIntent = new Intent(OrderConfirmActivity.this,
                PaymentActivity.class);
        paypalIntent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, mConfig);
        paypalIntent.putExtra(PaymentActivity.EXTRA_PAYMENT, thingToBuy);
        startActivityForResult(paypalIntent, REQUEST_CODE_PAYMENT);
    }

    /**
     * This method is used to getting result for payment.
     * @param requestCode This is the first parameter to onActivityResult method.
     * @param resultCode This is the second parameter to onActivityResult method.
     * @param data This is the third parameter to onActivityResult method.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        // TODO Auto-generated method stub
        if (requestCode == REQUEST_CODE_PAYMENT) {
            if (resultCode == Activity.RESULT_OK) {
                PaymentConfirmation confirm = data
                        .getParcelableExtra(PaymentActivity.EXTRA_RESULT_CONFIRMATION);
                if (confirm != null) {
                    try {
                        showProgDialiog();
                        JSONObject obj = confirm.toJSONObject();
                        JSONObject resObj = obj.getJSONObject("response");

                        sId = resObj.getString("id");

                        mCheckoutController.paypalTransId(sId);

                    } catch (Exception e) {
                        mOrderFailureLayout.setVisibility(View.VISIBLE);
                    }
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                mOrderFailureLayout.setVisibility(View.VISIBLE);

            } else if (resultCode == PaymentActivity.RESULT_EXTRAS_INVALID) {
                mOrderFailureLayout.setVisibility(View.VISIBLE);

            }
        }
    }


    /**
     * This method is used to paypalTransId success response
     *
     * @param response This is the parameter to paypalTransIdSuccess method.
     */
    public void paypalTransIdSuccess(String response) {
        try {
            JSONObject in_obj = new JSONObject(
                    response);
            JSONArray transactions = in_obj
                    .getJSONArray("transactions");
            int out_len = transactions.length();
            for (int j = 0; j < out_len; j++) {
                JSONObject out_b = transactions
                        .getJSONObject(j);
                JSONArray related_resources = out_b
                        .getJSONArray("related_resources");
                int len = related_resources.length();
                for (int i = 0; i < len; i++) {
                    JSONObject ob = related_resources
                            .getJSONObject(i);
                    JSONObject sale = ob.getJSONObject("sale");
                    mTransId = sale.getString("id");

                    mCheckoutController.orderPlace(mPaymentMethodCode, mTransId);


                    /*System.out
                            .println("@@@@@@@@@@@@@@@@@@@@@@@ ============== "
                                    + mTransId);*/

                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method is used to on click function.
     * @param v This is the parameter to onClick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            /**
             * Failure page Ok Click Listener
             */
            case R.id.order_cancel_ok:
                clearData();
                finish();
                break;
        }
    }
}
