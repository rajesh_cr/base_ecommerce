package com.prestashopemc.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.adapter.MyWishlistAdapter;
import com.prestashopemc.controller.LoginController;
import com.prestashopemc.database.CategoryProductService;
import com.prestashopemc.database.MagentoCategoryProductService;
import com.prestashopemc.database.MagentoProductService;
import com.prestashopemc.database.ProductService;
import com.prestashopemc.model.WishlistMainModel;
import com.prestashopemc.model.WishlistResult;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.MyApplication;

import java.util.List;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>My Wishlist Activity!</h1>
 * The My Wishlist Activity implements my wishlist details.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class MyWishlistActivity extends BaseActivity {

    private LoginController mLoginController;
    private List<WishlistResult> mWishlistResult;
    private RecyclerView mWishListView;
    private MyWishlistAdapter mMyWishlistAdapter;
    private RelativeLayout mMainLayout, mNoWishlistLayout;
    private TextView mHeaderText, mMsgText, mContinueShopping;
    private CategoryProductService mCategoryProductService;
    private ProductService mProductService;
    private MagentoCategoryProductService mMagentoCategoryProductService;
    private MagentoProductService mMagentoProductService;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     *
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mywishlist);
        MyApplication.getDefaultTracker("My Wishlist Screen");
        initToolBar();
        initNetworkError();
        logoImage.setVisibility(View.GONE);
        searchIcon.setVisibility(View.GONE);
        cartIcon.setVisibility(View.GONE);
        cartText.setVisibility(View.GONE);
        title.setVisibility(View.VISIBLE);
        title.setText(AppConstants.getTextString(this, AppConstants.wishlistText));
        title.setAllCaps(true);
        mMainLayout = (RelativeLayout) findViewById(R.id.main_layout);
        showProgDialiog();
        initializedLayout();
    }

    /**
     * This method is used to Initialized Layout
     */
    private void initializedLayout() {

        boolean isInternet = isInternetOn(this);
        if (!isInternet) {
            mMainLayout.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initializedLayout();
                }
            });
            hideProgDialog();
        } else {
            if (AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
                mMagentoCategoryProductService = new MagentoCategoryProductService(this);
                mMagentoProductService = new MagentoProductService(this);
            } else {
                mCategoryProductService = new CategoryProductService(this);
                mProductService = new ProductService(this);
            }
            mMainLayout.setVisibility(View.VISIBLE);
            networkLayout.setVisibility(View.GONE);
            mNoWishlistLayout = (RelativeLayout) findViewById(R.id.no_wishlist_layout);
            mHeaderText = (TextView) findViewById(R.id.no_wishlist_header);
            mMsgText = (TextView) findViewById(R.id.no_wishlist_msg);
            mContinueShopping = (TextView) findViewById(R.id.no_wishlist_continue);
            CustomBackground.setBackgroundText(mContinueShopping);
            mLoginController = new LoginController(MyWishlistActivity.this);
            mWishListView = (RecyclerView) findViewById(R.id.mywishlist_list);
            mWishListView.setLayoutManager(new LinearLayoutManager(MyWishlistActivity.this));
            mLoginController.myWishlistDetails();
            dynamicTextValue();
            mHeaderText.setTypeface(robotoMedium);
            mMsgText.setTypeface(robotoLight);
            mContinueShopping.setTypeface(robotoMedium);
            mContinueShopping.setOnClickListener(this);
        }
    }

    /**
     * This method is used to Dynamic Text Value Settings
     */
    private void dynamicTextValue() {
        mHeaderText.setText(AppConstants.getTextString(this, AppConstants.wishlistText));
        mMsgText.setText(AppConstants.getTextString(this, AppConstants.emptyWishlistTitleText));
        mContinueShopping.setText(AppConstants.getTextString(this, AppConstants.emptyWishlistDescriptionText));
    }

    /**
     * This method is used to Wishlist Success Response
     *
     * @param wishlistMainModel This is the parameter to mywishlistSuccess method.
     */
    public void mywishlistSuccess(WishlistMainModel wishlistMainModel) {
        MyApplication.eventTracking("wishlist product", "product deleted from wishlist", "wishlist product");
        hideProgDialog();
        mWishlistResult = wishlistMainModel.getResult();
        mMyWishlistAdapter = new MyWishlistAdapter(MyWishlistActivity.this, mWishlistResult);
        mWishListView.setAdapter(mMyWishlistAdapter);
    }

    /**
     * This method is used to Wishlist Failure Response
     *
     * @param errormsg This is the parameter to mywishlistFailure method.
     */
    public void mywishlistFailure(String errormsg) {
        hideProgDialog();
        String msg = AppConstants.getTextString(this, AppConstants.noWishlistText);
        if (errormsg.equals(msg) || errormsg.equals(getString(R.string.noFavouriteWishlistText))) {
            mWishListView.setVisibility(View.GONE);
            mNoWishlistLayout.setVisibility(View.VISIBLE);
        } else {
            snackBar(errormsg);
        }
    }

    /**
     * This method is used to Delete Wishlist Success
     *
     * @param result This is the parameter to mywishlistDeleteSuccess method.
     */
    public void mywishlistDeleteSuccess(String result) {
        hideProgDialog();
        mMyWishlistAdapter.notifyDataSetChanged();
        mLoginController.myWishlistDetails();
        if(AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)){
            mMagentoCategoryProductService.deleteCategoryProductServiceRecord();
            mMagentoProductService.deleteProductServiceRecord();
        }else {
            mCategoryProductService.deleteCategoryProductServiceRecord();
            mProductService.deleteProductServiceRecord();
        }
        snackBar(result);
    }

    /**
     * This method is used to onClick function.
     *
     * @param v This is the parameter to onClick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * Continue Shopping*/
            case R.id.no_wishlist_continue:
                finish();
                break;
        }
    }
}
