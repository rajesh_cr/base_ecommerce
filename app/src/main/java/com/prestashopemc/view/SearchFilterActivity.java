package com.prestashopemc.view;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.prestashopemc.R;
import com.prestashopemc.adapter.FilterExpandableAdapter;
import com.prestashopemc.adapter.SearchFilterCategoryAdapter;
import com.prestashopemc.controller.ProductListController;
import com.prestashopemc.controller.SearchController;
import com.prestashopemc.model.FilterCategory;
import com.prestashopemc.model.FilterCategoryResult;
import com.prestashopemc.model.FilterMain;
import com.prestashopemc.model.FilterOption;
import com.prestashopemc.model.FilterResult;
import com.prestashopemc.model.NavigationFilterMain;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.MyApplication;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>Search Filter Activity!</h1>
 * The Search Filter Activity implements search product list filter details.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author Rajesh
 * @version 1.2
 * @since 31 -10-2016
 */
public class SearchFilterActivity extends BaseActivity {

    private TextView mFilterCategoryName;//mFilterApply, mFilterClear,
    private RecyclerView mFilterCategoryView;//, mFilterAttributeView;
    private SearchController mSearchController;
    private List<FilterCategoryResult> mFilterCategoryResult;
    private ProductListController mProductController;
    /**
     * The M filter attribute result.
     */
    public List<FilterResult> mFilterAttributeResult;
    private FilterExpandableAdapter mFilterAdapter;
    private List<FilterResult> mGroupList = new ArrayList<>();
    private List<List<FilterOption>> mChildList = new ArrayList<List<FilterOption>>();
    private RelativeLayout mFilterCategoryLayout;
    private ImageView mCategoryImg;
    private boolean mIsCategory = true;
    private SearchFilterCategoryAdapter searchAdapter;
    private Bundle mBundle;
    public static JSONObject mFilterObject;
    private static ArrayList<String> mSearchFilterObject;
    private RelativeLayout mMainlayout;
    public static Activity mFa;
    public Integer mCurrentPage;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_filter);
        MyApplication.getDefaultTracker("Search Filter Screen");

        mBundle = getIntent().getExtras();
        if (mBundle != null) {
            mCurrentPage = mBundle.getInt("currentPage");
        }

        initToolBar();
        initNetworkError();
        mFa = this;
        searchIcon.setVisibility(View.GONE);
        cartIcon.setVisibility(View.GONE);
        title.setVisibility(View.VISIBLE);
        title.setAllCaps(true);
        title.setText(AppConstants.getTextString(this, AppConstants.selectCategoryText));
        logoImage.setVisibility(View.GONE);
        mMainlayout = (RelativeLayout) findViewById(R.id.main_layout);
        initializeLayout();
    }

    /**
     * This method is used to Initialized Layout*/
    private void initializeLayout() {
        showProgDialiog();
        boolean isInternet = isInternetOn(this);
        if(!isInternet)
        {
            mMainlayout.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initializeLayout();
                }
            });
            hideProgDialog();

        } else {
            mMainlayout.setVisibility(View.VISIBLE);
            networkLayout.setVisibility(View.GONE);
            mSearchController = new SearchController(SearchFilterActivity.this);
            mProductController = new ProductListController(SearchFilterActivity.this);
            mFilterCategoryName = (TextView) findViewById(R.id.filter_category_header_name);
            mFilterCategoryView = (RecyclerView) findViewById(R.id.search_filter_recyclerview);
            mFilterCategoryView.setLayoutManager(new LinearLayoutManager(SearchFilterActivity.this));
            mFilterCategoryName.setText(AppConstants.getTextString(this, AppConstants.selectCategoryText));
            mSearchController.filterCategories(mCurrentPage+"",SearchActivity.sSearchText);
            setCustomFont();
        }
    }

    /**
     * This method is used to Set Custome Font*/
    private void setCustomFont(){
        mFilterCategoryName.setTypeface(robotoRegular);
    }

   /* *//**
     * Category List Listener*//*
    private View.OnClickListener categoryListClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            *//*if (isCategory) {
                mFilterCategoryView.setVisibility(View.VISIBLE);
                mCategoryImg.setImageResource(R.drawable.ic_cart_top);
                isCategory = false;
            } else {
                mFilterCategoryView.setVisibility(View.GONE);
                mCategoryImg.setImageResource(R.drawable.ic_cart_bottom);
                isCategory = true;
            }*//*

        }
    };*/

    /**
     * This method is used to Filter Category List Success Response
     *
     * @param filterCategory This is the parameter to categorySuccess method.
     */
    public void categorySuccess(FilterCategory filterCategory) {
        hideProgDialog();
        mFilterCategoryResult = filterCategory.getResult();
        searchAdapter = new SearchFilterCategoryAdapter(SearchFilterActivity.this, mFilterCategoryResult);
        mFilterCategoryView.setAdapter(searchAdapter);
    }

    /**
     * This method is used to Filter Category List Failure Response
     *
     * @param string This is the parameter to categoryFailure method.
     */
    public void categoryFailure(String string) {
        hideProgDialog();
        snackBar(string);
    }

    /**
     * This method is used to Filter Attribute Values Success Response
     *
     * @param mFilterMainObject This is the parameter filterAttribueValues method.
     */
    public void filterAttribueValues(FilterMain mFilterMainObject) {
        hideProgDialog();
    }

    /**
     * Get list of Filters
     * @return List<ParentListItem> returns getFilters method.
     **/
    private List<ParentListItem> getFilters() {

        List<ParentListItem> parentListItems = new ArrayList<>();

        for (FilterResult parenList : mFilterAttributeResult) {
            List<FilterOption> childList = new ArrayList<>();
            for (FilterOption filter : parenList.getOptions()) {
                filter.setAttributeId(parenList.getAttributeId());
                childList.add(filter);
            }
            mChildList.add(childList);
            parentListItems.add(parenList);
        }
        mGroupList.addAll(mFilterAttributeResult);

        return parentListItems;
    }

    /**
     * This method is used to filter Request url method
     * @return JSONObject returns applyFilter method values in jsonobject form.
     **/
    private JSONObject applyFilter() {
        JSONObject attributeValue = new JSONObject();

        for (int i = 0; i < mGroupList.size(); i++) {
            JSONArray attributeArray = new JSONArray();
            String substring = "";
            for (int j = 0; j < mChildList.get(i).size(); j++) {
                for (int k = 0; k < FilterExpandableAdapter.mSelectedFilter.size(); k++) {
                    if (mChildList.get(i).get(j).getValue().equals(FilterExpandableAdapter.mSelectedFilter.get(k))) {
                        substring = mChildList.get(i).get(j)
                                .getAttributeId();
                        attributeArray.put(FilterExpandableAdapter.mSelectedFilter.get(k));
                    }
                }
            }
            try {
                if (!substring.isEmpty())
                    attributeValue.put(substring,
                            attributeArray);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }

        return attributeValue;
    }

    /**
     * This is the method used to Search Filter URL method
     * @return JSONObject returns applySearchFilter values to json object form.
     **/
    private JSONObject applySearchFilter() {
        JSONObject attributeValue = new JSONObject();

            JSONArray attributeArray = new JSONArray();
            String substring = "";
            for (int j = 0; j < mFilterCategoryResult.size(); j++) {
                for (int k = 0; k < SearchFilterCategoryAdapter.mSelectedFilter.size(); k++) {
                    if (mFilterCategoryResult.get(j).getIdCategory().equals(SearchFilterCategoryAdapter.mSelectedFilter.get(k))) {
                        substring = "Category";
                        attributeArray.put(SearchFilterCategoryAdapter.mSelectedFilter.get(k));
                    }
                }
            }
            try {
                if (!substring.isEmpty())
                    attributeValue.put(substring,
                            attributeArray);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        return attributeValue;
    }

    public void NavigationFilterAttribueValues(NavigationFilterMain navigationFilterMain) {
        hideProgDialog();
    }
}
