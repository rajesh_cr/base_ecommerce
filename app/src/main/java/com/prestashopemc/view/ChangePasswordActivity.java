package com.prestashopemc.view;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.prestashopemc.R;
import com.prestashopemc.controller.LoginController;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CommonValidation;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.MyApplication;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>Change Password Activity!</h1>
 * The Change Password Activity implements change password for user.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 26 /10/16
 */
public class ChangePasswordActivity extends BaseActivity implements View.OnClickListener {

    private EditText mOldPassword, mNewPassword, mConfirmPassword;
    private Button mSubmit;
    private LoginController mLoginController;
    private RelativeLayout mMainLayout;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     *
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        MyApplication.getDefaultTracker("Change Password Screen");
        initToolBar();
        initNetworkError();
        logoImage.setVisibility(View.GONE);
        searchIcon.setVisibility(View.GONE);
        cartIcon.setVisibility(View.GONE);
        title.setVisibility(View.VISIBLE);
        title.setText(AppConstants.getTextString(this, AppConstants.changePasswordText));
        title.setAllCaps(true);
        mMainLayout = (RelativeLayout) findViewById(R.id.main_layout);
        showProgDialiog();
        initializeLayout();
    }

    /**
     * This method is used to Initialize Layout
     **/
    private void initializeLayout() {
        boolean isInternet = isInternetOn(this);
        if (!isInternet) {
            mMainLayout.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showProgDialiog();
                    initializeLayout();
                }
            });
            hideProgDialog();

        } else {
            mMainLayout.setVisibility(View.VISIBLE);
            networkLayout.setVisibility(View.GONE);
            hideProgDialog();
            mLoginController = new LoginController(ChangePasswordActivity.this);
            mOldPassword = (EditText) findViewById(R.id.old_password);
            mNewPassword = (EditText) findViewById(R.id.new_password);
            mConfirmPassword = (EditText) findViewById(R.id.confirm_password);
            mSubmit = (Button) findViewById(R.id.submit);
            mSubmit.setTextColor(Color.parseColor(CustomBackground.mThemeColor));
            CustomBackground.setBackgroundRedWhiteCombination(mSubmit);
            mSubmit.setOnClickListener(this);
            dynamicTextValue();
            setCustomFont();
        }
    }

    /**
     * This method is used to Dynamic Text Value Settings
     */
    private void dynamicTextValue() {
        mSubmit.setText(AppConstants.getTextString(this, AppConstants.submitText));
        mConfirmPassword.setHint(AppConstants.getTextString(this, AppConstants.confirmPasswordText));
        mNewPassword.setHint(AppConstants.getTextString(this, AppConstants.newPasswordText));
        mOldPassword.setHint(AppConstants.getTextString(this, AppConstants.oldPasswordText));
    }

    /**
     * This method is used to Set Custome Font
     */
    private void setCustomFont() {
        mOldPassword.setTypeface(robotoRegular);
        mNewPassword.setTypeface(robotoRegular);
        mConfirmPassword.setTypeface(robotoRegular);
        mSubmit.setTypeface(robotoRegular);
    }

    /**
     * This method is used to Change password Success Response
     *
     * @param result This is the parameter to changePasswordSuccess method.
     */
    public void changePasswordSuccess(String result) {
        MyApplication.eventTracking("Change password", "change password success", "change password");
        Toast.makeText(ChangePasswordActivity.this, result, Toast.LENGTH_SHORT).show();
        finish();
    }

    /**
     * This method is used to Change password Failure Response
     *
     * @param errormsg This is the parameter to changePasswordFailure method.
     */
    public void changePasswordFailure(String errormsg) {
        hideProgDialog();
        snackBar(errormsg);
    }

    /**
     * This method is used to on click function
     *
     * @param v This is the parameter to onClick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * Submit Listener*/
            case R.id.submit:
                CommonValidation validation = new CommonValidation();
                boolean status = validation.passwordValidation(ChangePasswordActivity.this, mOldPassword.getText().toString(),
                        mNewPassword.getText().toString(), mConfirmPassword.getText().toString());
                if (status) {
                    showProgDialiog();
                    mLoginController.changePassword(mOldPassword.getText().toString(), mNewPassword.getText().toString());
                } else {
                    hideProgDialog();
                }
                break;
        }
    }
}
