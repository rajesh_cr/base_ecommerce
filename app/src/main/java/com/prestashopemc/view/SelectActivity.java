package com.prestashopemc.view;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.controller.LicenseController;
import com.prestashopemc.model.LicenseMain;
import com.prestashopemc.model.LicenseResult;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CommonValidation;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.SharedPreference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

public class SelectActivity extends BaseActivity implements View.OnClickListener {
    private RelativeLayout mSelectSiteLayout, mLicenseLayout;
    private TextView mLicenseHeader, mLicenseMsg, mLicenseOk;
    private TextView mEnterSiteText, mSelectCategoryText;
    private Button mSubmitButton;
    private EditText mSiteEdit;
    private Spinner mSelectCategorySpinner;
    private String mSiteName = "";
    private String mSelectedCategorytype = "";
    private ArrayList<String> mSpinnerArray;
    private Context mContext;
    private int srch_text_size = 14;
    private CommonValidation mValidation;
    private ImageView spinnerImage, mLogoImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select);
        MyApplication.getDefaultTracker("Selection Screen");
        initNetworkError();
        mContext = this;
        mSelectSiteLayout = (RelativeLayout) findViewById(R.id.select_site_layout);
        mLicenseLayout = (RelativeLayout) findViewById(R.id.license_layout);
        mLicenseHeader = (TextView) findViewById(R.id.header_text);
        mLicenseMsg = (TextView) findViewById(R.id.msg_text);
        mLicenseOk = (TextView) findViewById(R.id.ok_text);
        mEnterSiteText = (TextView) findViewById(R.id.enter_site_text);
        mSelectCategoryText = (TextView) findViewById(R.id.select_category_text);
        mSubmitButton = (Button) findViewById(R.id.site_submit_button);
        mSiteEdit = (EditText) findViewById(R.id.enter_site_edit);
        mSelectCategorySpinner = (Spinner) findViewById(R.id.select_category_spinner);
        spinnerImage = (ImageView) findViewById(R.id.spinner_img);
        mLogoImage = (ImageView) findViewById(R.id.logo_img);
        mSiteEdit.setHint(R.string.baseUrl_hint);
        Picasso.with(this).load(R.drawable.toplogo)
                .placeholder(R.drawable.place_holder).resize(280, 120)
                .into(mLogoImage);
        setSpinner();
        setCustomFont();
        mSubmitButton.setOnClickListener(this);
        spinnerImage.setOnClickListener(this);
    }

    private void setSpinner() {
        mSpinnerArray = new ArrayList<>();
        mSpinnerArray.add("Magento");
        mSpinnerArray.add("Prestashop");
        ArrayAdapter<String> mCategoryAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, mSpinnerArray){
            @NonNull
            @Override
            public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View v = super.getView(position, convertView, parent);
                ((TextView) v).setTypeface(robotoRegular);
                ((TextView) v).setTextSize(srch_text_size);
                ((TextView) v).setTextColor(ContextCompat.getColor(mContext, R.color.black_color));
                return v;
            }

            @Override
            public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
                View view = super.getDropDownView(position, convertView, parent);
                ((TextView) view).setPadding(16, 16, 16, 16);
                ((TextView) view).setTypeface(robotoRegular);
                ((TextView) view).setTextSize(srch_text_size);
                ((TextView) view).setTextColor(ContextCompat.getColor(mContext, R.color.black_color));
                return view;
            }
        };
        mSelectCategorySpinner.setAdapter(mCategoryAdapter);
        mSelectCategorySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mSelectedCategorytype = mSelectCategorySpinner.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void checkValidation() {
        mSiteName = mSiteEdit.getText().toString();
        mValidation = new CommonValidation();
        AppConstants.sPrestashopMagento = mSelectedCategorytype;
        AppConstants.sBaseUrl = mSiteName+"/index.php?fc=module&module=emcapp&controller=";
        AppConstants.sMagentoBaseUrl = mSiteName+"/index.php/mobile/";

        SharedPreference.getInstance().save("base_url", AppConstants.sBaseUrl);
        SharedPreference.getInstance().save("magento_url", AppConstants.sMagentoBaseUrl);
        SharedPreference.getInstance().save("presmagento", mSelectedCategorytype);
        boolean status = mValidation.checkSiteValidation(mContext, mSiteName, mSelectedCategorytype);

        if(status)
        {
            licenseCheck();
        }
    }

    /**
     * This method is used to Dynamic Text Value Settings
     */
    private void dynamicTextValue() {
        mLicenseHeader.setText(AppConstants.getTextString(this, AppConstants.accessDeniedText));
        mLicenseMsg.setText(AppConstants.getTextString(this, AppConstants.contactAdminText));
        mLicenseOk.setText(AppConstants.getTextString(this, AppConstants.okayText));
    }

    /**
     * This method is used to Set Custom Font
     */
    public void setCustomFont() {
        mLicenseHeader.setTypeface(robotoMedium);
        mLicenseMsg.setTypeface(robotoLight);
        mLicenseOk.setTypeface(robotoMedium);

        mEnterSiteText.setTypeface(robotoLight);
        mSelectCategoryText.setTypeface(robotoLight);
        mSiteEdit.setTypeface(robotoRegular);
        mSubmitButton.setTypeface(robotoMedium);
    }

    private void licenseCheck() {
        long licCheck = SharedPreference.getInstance().getLong("lic_check");
        boolean hasLicense = SharedPreference.getInstance().getBoolean("has_license");
        long curTime = new Date().getTime();
        if (licCheck == 0 || !hasLicense) {
            callLicense();
        } else if (curTime == licCheck || curTime > licCheck) {
            callLicense();
        } else {
            boolean isInternet = isInternetOn(this);
            if (!isInternet) {
                dynamicTextValue();
                mSelectSiteLayout.setVisibility(View.GONE);
                mLicenseLayout.setVisibility(View.GONE);
                networkLayout.setVisibility(View.VISIBLE);
                networkTry.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        licenseCheck();
                    }
                });
                hideProgDialog();

            } else {
                if (getIntent().getAction() == Intent.ACTION_VIEW) {
                    Uri uri = getIntent().getData();
                    // do stuff with uri
                    Log.e("URI Response ==== ", uri + "");
                }
                networkLayout.setVisibility(View.GONE);
                Handler handler = new Handler();

                final Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        Intent loginIntent = new Intent(SelectActivity.this, MainActivity.class);
                        startActivity(loginIntent);
                        finish();
                    }
                };
                handler.postDelayed(r, 2000);
            }
        }
    }

    /**
     * This method is used to License Call Method
     */
    public void callLicense() {
        showProgDialiog();
        LicenseController mLicenseController = new LicenseController(SelectActivity.this);
        mLicenseController.licenseCheck(AppConstants.sMagentoBaseUrl, AppConstants.sBaseUrl);
        Date dtStartDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dtStartDate);
        c.add(Calendar.DATE, 30);// number of days to add
        SharedPreference.getInstance().saveLong("lic_check", c.getTimeInMillis());
    }

    /**
     * This method is used to License Success Response
     *
     * @param status      This is the first parameter to licenseSuccess method.
     * @param licenseMain This is the second parameter to licenseSuccess method.
     */
    public void licenseSuccess(boolean status, LicenseMain licenseMain) {
        LicenseResult licenseResult = new LicenseResult();
        licenseResult = licenseMain.getResult();
        if (!licenseResult.getStatus()) {
            SharedPreference.getInstance().saveBoolean("has_license", status);
            SharedPreference.getInstance().saveBoolean("is_siteurl_save", true);

            Handler handler = new Handler();

            final Runnable r = new Runnable() {
                @Override
                public void run() {
                    hideProgDialog();
                    Intent loginIntent = new Intent(SelectActivity.this, MainActivity.class);
                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    loginIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(loginIntent);
                }
            };
            handler.postDelayed(r, 2000);
        } else {
            mSelectSiteLayout.setVisibility(View.GONE);
            mLicenseLayout.setVisibility(View.VISIBLE);
            SharedPreference.getInstance().saveBoolean("has_license", false);
            mLicenseOk.setOnClickListener(this);
        }

    }

    /**
     * This method is used to License Failure Response
     *
     * @param status This is the parameter to licenseError method.
     */
    public void licenseError(boolean status) {
        hideProgDialog();
        mSelectSiteLayout.setVisibility(View.GONE);
        mLicenseLayout.setVisibility(View.VISIBLE);
        SharedPreference.getInstance().saveBoolean("has_license", status);
        mLicenseOk.setOnClickListener(this);
    }

    /**
     * This method is used to back pressed
     */
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    /**
     * This method is used to onclick action
     *
     * @param v This is the parameter to onClick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * License Failure Ok Listener*/
            case R.id.ok_text:
                finish();
                System.exit(0);
                break;

            case R.id.site_submit_button:
                checkValidation();
                break;

            case R.id.spinner_img:
                mSelectCategorySpinner.performClick();
                break;
        }
    }
}
