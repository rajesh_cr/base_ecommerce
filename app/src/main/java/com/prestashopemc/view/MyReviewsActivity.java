package com.prestashopemc.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.prestashopemc.R;
import com.prestashopemc.adapter.MyReviewsAdapter;
import com.prestashopemc.controller.LoginController;
import com.prestashopemc.model.MagentoReviewMain;
import com.prestashopemc.model.MagentoReviewResult;
import com.prestashopemc.model.MyReviewsMain;
import com.prestashopemc.model.MyReviewsResult;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.MyApplication;

import java.util.ArrayList;
import java.util.List;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>My Reviews Activity!</h1>
 * The My Reviews Activity implements my reviews.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class MyReviewsActivity extends BaseActivity {

    private LoginController mLoginController;
    private RecyclerView mReviewsList;
    private MyReviewsAdapter myReviewsAdapter;
    private List<MyReviewsResult> myReviewsResult = new ArrayList<>();
    private RelativeLayout mMainLayout;
    private List<MagentoReviewResult> magentoReviewResults = new ArrayList<>();

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     *
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myreviews);
        MyApplication.getDefaultTracker("My reviews Screen");
        initToolBar();
        initNetworkError();
        logoImage.setVisibility(View.GONE);
        searchIcon.setVisibility(View.GONE);
        cartIcon.setVisibility(View.GONE);
        cartText.setVisibility(View.GONE);
        title.setVisibility(View.VISIBLE);
        title.setText(AppConstants.getTextString(this, AppConstants.myReviewsText));
        title.setAllCaps(true);
        mMainLayout = (RelativeLayout) findViewById(R.id.main_layout);
        showProgDialiog();
        initializedLayout();
    }

    /**
     * This method is used to Initialized Layout
     */
    private void initializedLayout() {

        boolean isInternet = isInternetOn(this);
        if (!isInternet) {
            mMainLayout.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initializedLayout();
                }
            });
            hideProgDialog();

        } else {
            mMainLayout.setVisibility(View.VISIBLE);
            networkLayout.setVisibility(View.GONE);
            mLoginController = new LoginController(MyReviewsActivity.this);
            mLoginController.myReviewsDetails();
            mReviewsList = (RecyclerView) findViewById(R.id.reviews_list);
            mReviewsList.setLayoutManager(new LinearLayoutManager(this));
        }
    }

    /**
     * This method is used to Reviews List Success Response
     *
     * @param myReviewsMain This is the parameter to reviewsListSuccess method.
     */
    public void reviewsListSuccess(MyReviewsMain myReviewsMain) {
        hideProgDialog();
        myReviewsResult = myReviewsMain.getResult();
        if (myReviewsResult.size() != 0) {
            myReviewsAdapter = new MyReviewsAdapter(MyReviewsActivity.this, myReviewsResult, magentoReviewResults);
            mReviewsList.setAdapter(myReviewsAdapter);
        } else {
            snackBar(AppConstants.getTextString(this, AppConstants.noReviewText));
        }
    }

    /**
     * This method is used to Reviews List Failure Response
     *
     * @param errormsg This is the parameter to reviewsListFailure method.
     */
    public void reviewsListFailure(String errormsg) {
        hideProgDialog();
        snackBar(errormsg);
    }

    /**
     * This method is used to Reviews List Success Response
     *
     * @param magentoReviewMain This is the parameter to reviewsListSuccess method.
     */
    public void magentoReviewSuccess(MagentoReviewMain magentoReviewMain) {
        hideProgDialog();
        magentoReviewResults = magentoReviewMain.getResult();
        if (magentoReviewResults.size() != 0) {
            myReviewsAdapter = new MyReviewsAdapter(MyReviewsActivity.this, myReviewsResult, magentoReviewResults);
            mReviewsList.setAdapter(myReviewsAdapter);
        } else {
            snackBar(AppConstants.getTextString(this, AppConstants.noReviewText));
        }
    }
}
