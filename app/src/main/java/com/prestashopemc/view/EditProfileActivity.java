package com.prestashopemc.view;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.NotificationManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.prestashopemc.database.MagentoCategoryProductService;
import com.prestashopemc.database.MagentoProductService;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;
import com.prestashopemc.R;
import com.prestashopemc.controller.LoginController;
import com.prestashopemc.database.CategoryProductService;
import com.prestashopemc.database.ProductService;
import com.prestashopemc.model.Customer;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CommonValidation;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.MyApplication;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>Edit Profile Activity!</h1>
 * The Edit Profile Activity implements edit profile information.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 25 /10/16
 */
public class EditProfileActivity extends BaseActivity {

    private EditText mFirstName, mLastName, mMobileNum, mEmail;
    private TextView mChangePassword, mOtherLabel, mLogout;
    private LinearLayout mChangePassLayout, mLogoutLayout;
    private Bitmap mBitmap;
    private File mFile;
    private String mUserChoosenTask;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private ImageView mProfileImg, mCameraImg;
    private Customer mCustomer;
    private Button mSaveButton;
    private LoginController mLoginController;
    /**
     * The My permissions request read external storage.
     */
    public final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    private ScrollView mMainLayout;
    private CategoryProductService mCategoryProductService;
    private ProductService mProductService;
    private MagentoCategoryProductService mMagentoCategoryProductService;
    private MagentoProductService mMagentoProductService;
    private View mChangeViewLine;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     *
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        MyApplication.getDefaultTracker("Edit Profile Screen");
        initToolBar();
        initNetworkError();
        String customer = SharedPreference.getInstance().getValue("customer");

        if (!customer.equalsIgnoreCase("0")) {
            mCustomer = MyApplication.getGsonInstance().fromJson(customer, Customer.class);
        }

        logoImage.setVisibility(View.GONE);
        searchIcon.setVisibility(View.GONE);
        cartIcon.setVisibility(View.GONE);
        title.setVisibility(View.VISIBLE);
        title.setText(AppConstants.getTextString(this, AppConstants.editProfileText));
        title.setAllCaps(true);
        showProgDialiog();
        mMainLayout = (ScrollView) findViewById(R.id.main_layout);
        initializeLayout();
    }


    /**
     * This method is used to Initialize Layout
     **/
    private void initializeLayout() {

        boolean isInternet = isInternetOn(this);
        if (!isInternet) {
            mMainLayout.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initializeLayout();
                }
            });
            hideProgDialog();

        } else {
            mMainLayout.setVisibility(View.VISIBLE);
            networkLayout.setVisibility(View.GONE);
            hideProgDialog();
            if(AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)){
                mMagentoCategoryProductService = new MagentoCategoryProductService(this);
                mMagentoProductService = new MagentoProductService(this);
            }else {
                mCategoryProductService = new CategoryProductService(this);
                mProductService = new ProductService(this);
            }
            mLoginController = new LoginController(EditProfileActivity.this);
            mFirstName = (EditText) findViewById(R.id.first_name);
            mLastName = (EditText) findViewById(R.id.last_name);
            mMobileNum = (EditText) findViewById(R.id.mobile_num);
            mEmail = (EditText) findViewById(R.id.email_id);
            mEmail.setEnabled(false);
            mChangeViewLine = (View) findViewById(R.id.change_view);
            mChangePassword = (TextView) findViewById(R.id.change_password);
            mLogout = (TextView) findViewById(R.id.logout);
            mOtherLabel = (TextView) findViewById(R.id.other_label);
            mChangePassLayout = (LinearLayout) findViewById(R.id.change_pass_layout);
            mLogoutLayout = (LinearLayout) findViewById(R.id.logout_layout);
            mProfileImg = (ImageView) findViewById(R.id.profile_image);
            mCameraImg = (ImageView) findViewById(R.id.camera_img);
            mSaveButton = (Button) findViewById(R.id.save);
            mSaveButton.setTextColor(Color.parseColor(CustomBackground.mThemeColor));
            CustomBackground.setBackgroundRedWhiteCombination(mSaveButton);
            mLoginController = new LoginController(this);
            mProfileImg.setOnClickListener(this);
            mCameraImg.setOnClickListener(this);
            mChangePassLayout.setOnClickListener(this);
            mLogoutLayout.setOnClickListener(this);
            mSaveButton.setOnClickListener(this);
            dynamicTextValue();
            setEditProfile();
            setCustomFont();

            if (SharedPreference.getInstance().getBoolean("isSocial")) {
                mChangePassLayout.setVisibility(View.GONE);
                mChangeViewLine.setVisibility(View.GONE);
            }
        }
    }

    /**
     * This method is used to Dynamic Text Value Settings
     */
    private void dynamicTextValue() {
        mFirstName.setHint(AppConstants.getTextString(this, AppConstants.firstnameText));
        mLastName.setHint(AppConstants.getTextString(this, AppConstants.lastnameText));
        mEmail.setHint(AppConstants.getTextString(this, AppConstants.emailIdText));
        mMobileNum.setHint(AppConstants.getTextString(this, AppConstants.mobileNumberText));
        mSaveButton.setText(AppConstants.getTextString(this, AppConstants.saveChangesText));
        mOtherLabel.setText(AppConstants.getTextString(this, AppConstants.othersText));
        mChangePassword.setText(AppConstants.getTextString(this, AppConstants.changePasswordText));
        mLogout.setText(AppConstants.getTextString(this, AppConstants.logoutText));
    }

    /**
     * This method is used to Set Custom Font
     */
    private void setCustomFont() {
        mFirstName.setTypeface(robotoRegular);
        mLastName.setTypeface(robotoRegular);
        mEmail.setTypeface(robotoRegular);
        mMobileNum.setTypeface(robotoRegular);
        mSaveButton.setTypeface(robotoRegular);
        mChangePassword.setTypeface(robotoLight);
        mOtherLabel.setTypeface(robotoRegular);
        mLogout.setTypeface(robotoLight);

    }

    /**
     * This method is used to set edit profile
     */
    private void setEditProfile() {
        if (mCustomer != null) {
            mFirstName.setText(mCustomer.getmFirstName());
            mLastName.setText(mCustomer.getmLastName());
            mEmail.setText(mCustomer.getmEmail());
            if (!mCustomer.getmMobileNumber().equals("null")) {
                mMobileNum.setText(mCustomer.getmMobileNumber());
            }

            if (!mCustomer.getmProfileImage().isEmpty()) {
                Log.e("Image URL===== ", mCustomer.getmProfileImage());
                Picasso.with(this).load(mCustomer.getmProfileImage())
                        .placeholder(R.drawable.ic_profile).resize(200, 200).centerCrop()
                        .transform(new CircleTransform()).into(mProfileImg);
            } else if (!SharedPreference.getInstance().getValue("social_profile_id").equals("0")) {
                //For Facebook Profile picture
                String userId = SharedPreference.getInstance().getValue("social_profile_id");
                Picasso.with(this).load("https://graph.facebook.com/" + userId + "/picture?type=large")
                        .placeholder(R.drawable.ic_profile).resize(200, 200).centerCrop()
                        .transform(new CircleTransform()).into(mProfileImg);
            } else if (!SharedPreference.getInstance().getValue("google_profile_id").equals("0")) {
                //For Google Plus Profile picture
                String userId = SharedPreference.getInstance().getValue("google_profile_id");
                Picasso.with(this).load(userId)
                        .placeholder(R.drawable.ic_profile).resize(200, 200).centerCrop()
                        .transform(new CircleTransform()).into(mProfileImg);
            }
        }
    }

    /**
     * This method is used to AlertDialog
     */
    private void alertDialog() {
        new android.support.v7.app.AlertDialog.Builder(this)
                .setTitle(AppConstants.getTextString(this, AppConstants.informationText))
                .setMessage(AppConstants.getTextString(this, AppConstants.logoutAlertText))
                .setPositiveButton(AppConstants.getTextString(this, AppConstants.yesText), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        if(AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)){
                            mMagentoCategoryProductService.deleteCategoryProductServiceRecord();
                            mMagentoProductService.deleteProductServiceRecord();
                        }else {
                            mCategoryProductService.deleteCategoryProductServiceRecord();
                            mProductService.deleteProductServiceRecord();
                        }
                        deleteCredentials();
                        cartCount();
                        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        notificationManager.cancelAll();
                        MyApplication.eventTracking("logout", "logout button clicked", "logout");
                        finish();
                    }
                })
                .setNegativeButton(AppConstants.getTextString(this, AppConstants.noText), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    /**
     * This method is used to Check System Permissions
     */
    private void loadPermissions() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(this);
                alertBuilder.setCancelable(true);
                alertBuilder.setTitle("Permission necessary");
                alertBuilder.setMessage("External storage permission is necessary");
                alertBuilder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                        ActivityCompat.requestPermissions(EditProfileActivity.this, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                    }
                });
                AlertDialog alert = alertBuilder.create();
                alert.show();

            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

            }
        } else {

            if (mUserChoosenTask.equalsIgnoreCase(AppConstants.getTextString(this, AppConstants.takePhotoText))) {
                cameraIntent();
            } else {
                galleryIntent();
            }

        }
    }


    /**
     * This method is used to Check Lollipop version
     */
    private boolean isLollipop() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    /**
     * This method is used to Choose from Gallery
     */
    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, getString(R.string.selectFileText)), SELECT_FILE);
       /* intent.addCategory(Intent.CATEGORY_OPENABLE);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);*/
    }

    /**
     * This method is used to Choose From Camera
     */
    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }


    /**
     * This method is used to get result of getting gallery images from device
     *
     * @param requestCode This is the first parameter to onActivityResult method.
     * @param resultCode  This is the second parameter to onActivityResult method.
     * @param data        This is the third parameter to onActivityResult method.
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE) {
                onSelectFromGalleryResult(data);
            } else if (requestCode == REQUEST_CAMERA) {
                onCaptureImageResult(data);
            }
        }
    }

    /**
     * This method is used to Select From Gallery
     *
     * @param data This is the parameter to onSelectFromGalleryResult method.
     */
    @SuppressWarnings("deprecation")
    private void onSelectFromGalleryResult(Intent data) {

        mBitmap = null;
        File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        if (data != null) {
            try {
                mBitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                mBitmap.compress(Bitmap.CompressFormat.JPEG, 20, bytes);
                destination.createNewFile();
                fo = new FileOutputStream(destination);
                fo.write(bytes.toByteArray());
                fo.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        String gallery = destination.getAbsolutePath();
        mFile = new File(gallery);
        mProfileImg.setImageBitmap(AppConstants.getCircleBitmap(mBitmap));
    }

    /**
     * This method is used to Capture From Camera
     *
     * @param data This is the parameter to onCaptureImageResult method.
     */
    private void onCaptureImageResult(Intent data) {

        mBitmap = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        mBitmap.compress(Bitmap.CompressFormat.JPEG, 20, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(), System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String camera = destination.getAbsolutePath();
        mFile = new File(camera);
        mProfileImg.setImageBitmap(AppConstants.getCircleBitmap(mBitmap));
    }

    /**
     * This method is used to Update Success Response API
     */
    public void updateSuccess() {
        MyApplication.eventTracking("profile updated", "profile updated", "profile updated");
        Toast.makeText(EditProfileActivity.this, AppConstants.getTextString(this, AppConstants.updateSuccessText), Toast.LENGTH_SHORT).show();
        finish();
    }

    /**
     * This method is used to Failure response
     *
     * @param errormsg This is the parameter to failure method.
     */
    public void failure(String errormsg) {
        hideProgDialog();
        snackBar(errormsg);
    }

    /**
     * This method is used to Inner Class for picasso circle image
     */
    public class CircleTransform implements Transformation {
        @Override
        public Bitmap transform(Bitmap source) {
            int size = Math.min(source.getWidth(), source.getHeight());

            int x = (source.getWidth() - size) / 2;
            int y = (source.getHeight() - size) / 2;

            Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
            if (squaredBitmap != source) {
                source.recycle();
            }

            Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

            Canvas canvas = new Canvas(bitmap);
            Paint paint = new Paint();
            BitmapShader shader = new BitmapShader(squaredBitmap,
                    BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
            paint.setShader(shader);
            paint.setAntiAlias(true);

            float r = size / 2f;
            canvas.drawCircle(r, r, r, paint);

            squaredBitmap.recycle();
            return bitmap;
        }

        @Override
        public String key() {
            return "circle";
        }
    }

    /**
     * This method is used to onRequestPermissionsResult
     *
     * @param requestCode  This is the first parameter to onRequestPermissionsResult method.
     * @param permissions  This is the second parameter to onRequestPermissionsResult method.
     * @param grantResults This is the third parameter to onRequestPermissionsResult method.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE:

                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    if (mUserChoosenTask.equalsIgnoreCase(AppConstants.getTextString(this, AppConstants.takePhotoText))) {
                        cameraIntent();
                    } else {
                        galleryIntent();
                    }
                } else {
                    //callIntent();
                }
                return;
        }
    }

    /**
     * This is the method to on click
     * @param v This is the parameter to onClick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * Save Listener*/
            case R.id.save:
                showProgDialiog();
                Customer customer = new Customer();
                customer.setmFirstName(AppConstants.stringEncode(mFirstName.getText().toString()));
                customer.setmLastName(AppConstants.stringEncode(mLastName.getText().toString()));
                customer.setmEmail(mEmail.getText().toString());
                customer.setmMobileNumber(mMobileNum.getText().toString());
                customer.setmCustomerId(mCustomer.getmCustomerId());
                CommonValidation validation = new CommonValidation();
                boolean status = validation.editProfileValidation(EditProfileActivity.this, customer);
                if (status) {
                    Log.e("Image File === ", String.valueOf(mFile));
                    mLoginController.updateProfile(customer, mFile);
                } else {
                    hideProgDialog();
                }
                break;

            /**
             * Profile Image Select Listener
             */
            case R.id.profile_image:
                imageClickListener();
                break;

            /**
             * Camera Image Select Listener
             */
            case R.id.camera_img:
                imageClickListener();
                break;

            /**
             * Logout Click Listener
             */
            case R.id.logout_layout:
                SharedPreference.getInstance().saveBoolean("isSocial", false);
                alertDialog();
                break;

            /**
             * Change password Listener
             */
            case R.id.change_pass_layout:
                Intent changeIntent = new Intent(EditProfileActivity.this, ChangePasswordActivity.class);
                startActivity(changeIntent);
                break;
        }
    }

    /**
     * Image click listener.
     */
/* This is the method for profile image click listener
     */
    public void imageClickListener() {
        final CharSequence[] items = {AppConstants.getTextString(EditProfileActivity.this, AppConstants.takePhotoText),
                AppConstants.getTextString(EditProfileActivity.this, AppConstants.galleryText),
                AppConstants.getTextString(EditProfileActivity.this, AppConstants.cancelText)};

        AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
        builder.setTitle(AppConstants.getTextString(EditProfileActivity.this, AppConstants.selectPhotoText));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals(AppConstants.getTextString(EditProfileActivity.this, AppConstants.takePhotoText))) {
                    mUserChoosenTask = AppConstants.getTextString(EditProfileActivity.this, AppConstants.takePhotoText);
                    if (!isLollipop()) {
                        cameraIntent();
                    } else {
                        loadPermissions();
                    }

                } else if (items[item].equals(AppConstants.getTextString(EditProfileActivity.this, AppConstants.galleryText))) {
                    mUserChoosenTask = AppConstants.getTextString(EditProfileActivity.this, AppConstants.galleryText);
                    if (!isLollipop()) {
                        galleryIntent();
                    } else {
                        loadPermissions();
                    }

                } else if (items[item].equals(AppConstants.getTextString(EditProfileActivity.this, AppConstants.cancelText))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }
}
