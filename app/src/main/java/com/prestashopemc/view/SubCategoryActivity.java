package com.prestashopemc.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.adapter.MyCustomLayoutManager;
import com.prestashopemc.adapter.SubCategoryAdapter;
import com.prestashopemc.adapter.SubCategoryProductsAdapter;
import com.prestashopemc.controller.HomePageController;
import com.prestashopemc.database.CategoryProductService;
import com.prestashopemc.database.CategoryService;
import com.prestashopemc.model.Category;
import com.prestashopemc.model.CategoryProduct;
import com.prestashopemc.model.ExpandCategory;
import com.prestashopemc.model.SubCategoryModel;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.SharedPreference;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>Sub category Activity!</h1>
 * The Sub category Activity implements chat details.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 16 /8/16
 */
public class SubCategoryActivity extends BaseActivity {

    private HomePageController mHomePageController;
    private Bundle mBundle;
    private RecyclerView mSubCategoryListview;
    private SubCategoryAdapter mSubCategoryAdapter;
    private String mCategoryName, mCategoryId;
    private SubCategoryModel mSubCategory;
    private RelativeLayout mMainLayout, mSubcategoryProductsLayout;
    private TextView mProductCount, mViewAll;
    private RecyclerView mSubcategoryProductListView;
    private SubCategoryProductsAdapter mSubCategoryProductAdapter;
    private View mProductsShadowView;
    private CategoryService mCategoryService;
    private CategoryProductService mCategoryProductService;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     *
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);
        MyApplication.getDefaultTracker("Sub Category Screen");
        mBundle = getIntent().getExtras();
        if (mBundle != null) {
            mCategoryName = mBundle.getString("categoryName");
            mCategoryId = mBundle.getString("sCategoriesId");
        }
        initToolBar();
        initNetworkError();
        mMainLayout = (RelativeLayout) findViewById(R.id.main_layout);
        initalizeLayout();
    }

    /**
     * This method is used to initialize layout
     */
    private void initalizeLayout() {
        showProgDialiog();

        boolean isInternet = isInternetOn(this);
        if (!isInternet) {
            mMainLayout.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initalizeLayout();
                }
            });
            hideProgDialog();

        } else {
            mMainLayout.setVisibility(View.VISIBLE);
            networkLayout.setVisibility(View.GONE);
            title.setVisibility(View.VISIBLE);
            title.setText(mCategoryName);
            logoImage.setVisibility(View.GONE);
            mCategoryService = new CategoryService(SubCategoryActivity.this);
            mCategoryProductService = new CategoryProductService(SubCategoryActivity.this);
            mSubcategoryProductsLayout =(RelativeLayout)findViewById(R.id.products_layout);
            mProductCount =(TextView)findViewById(R.id.subcategory_product_count);
            mViewAll =(TextView)findViewById(R.id.view_all_text);
            mSubcategoryProductListView = (RecyclerView)findViewById(R.id.subcategory_products_list);
            mProductsShadowView = (View) findViewById(R.id.products_view);
            mHomePageController = new HomePageController(this);
            mSubCategoryListview = (RecyclerView) findViewById(R.id.sub_category_list_view);
            mHomePageController.getSubCategoryList(mCategoryId);
        }
    }

    /**
     * This method is used to sub category update.
     *
     * @param response This is the parameter to subCategoryUpdate method.
     */
    public void subCategoryUpdate(String response) {
        hideProgDialog();
        try {
            JSONObject jsonObject = new JSONObject(response);
            mSubCategory = (MyApplication.getGsonInstance().fromJson(response, SubCategoryModel.class));
            if (mSubCategory.getStatus()) {
                mCategoryService.insertSubCategory(mSubCategory);
                mCategoryProductService.addProduct(mSubCategory.getCategoryProducts(), mSubCategory.getIdCategory(),"1", SharedPreference.getInstance().getValue("customerid"));
                mSubCategoryAdapter = new SubCategoryAdapter(this, mSubCategory.getResult());
                mSubCategoryListview.setAdapter(mSubCategoryAdapter);
                mSubCategoryListview.setLayoutManager(new LinearLayoutManager(this));
                mSubCategoryListview.setHasFixedSize(true);

                if (mSubCategory.getCategoryProducts().size()!=0){
                    displaySubCategoryProducts(mSubCategory.getCategoryProducts());
                }else {
                    mSubcategoryProductsLayout.setVisibility(View.GONE);
                }
            } else {
                snackBar(jsonObject.getString("errormsg"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Display Subcategory products
     * @param categoryProducts*/
    private void displaySubCategoryProducts(final List<CategoryProduct> categoryProducts) {
        mProductCount.setTypeface(robotoLight);
        mViewAll.setTypeface(robotoRegular);
        mViewAll.setText(AppConstants.getTextString(this, AppConstants.subcategoryViewAll));
        CustomBackground.setRectangleBackground(mViewAll);
        mSubcategoryProductsLayout.setVisibility(View.VISIBLE);
        mProductsShadowView.setVisibility(View.VISIBLE);
        mProductCount.setText(categoryProducts.size()+" "+ AppConstants.getTextString(this, AppConstants.subcategoryProductCount));
        mViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SubCategoryActivity.this, ProductListActivity.class);
                intent.putExtra("sCategoriesId", mCategoryId);
                intent.putExtra("categoryName", mCategoryName);
                intent.putExtra("productCount",categoryProducts.size()+"");
                SharedPreference.getInstance().save("subCategoryName", mCategoryName);
                startActivity(intent);
            }
        });
        MyCustomLayoutManager mFirstLinearLayout = new MyCustomLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mSubcategoryProductListView.setLayoutManager(mFirstLinearLayout);
        mSubcategoryProductListView.smoothScrollToPosition(0);
        mSubcategoryProductListView.setHasFixedSize(true);
        mSubcategoryProductListView.setNestedScrollingEnabled(true);
        mSubCategoryProductAdapter = new SubCategoryProductsAdapter(this, categoryProducts);
        mSubcategoryProductListView.setAdapter(mSubCategoryProductAdapter);
    }

    /**
     * This method is used to subCategoryFailure response
     *
     * @param failureResponse This is the parameter to subCategoryFailure response message.
     */
    public void subCategoryFailure(String failureResponse) {
        hideProgDialog();
        snackBar(failureResponse);

    }

    /**
     * Getting Sub category values from DB*/
    public void subCategoryDbUpdate(List<ExpandCategory> mExpandCategory, List<CategoryProduct> mCategoryProducts) {
        hideProgDialog();
        mSubCategoryAdapter = new SubCategoryAdapter(this, mExpandCategory);
        mSubCategoryListview.setAdapter(mSubCategoryAdapter);
        mSubCategoryListview.setLayoutManager(new LinearLayoutManager(this));
        mSubCategoryListview.setHasFixedSize(true);

        if (mCategoryProducts.size()!=0){
            displaySubCategoryProducts(mCategoryProducts);
        }else {
            mSubcategoryProductsLayout.setVisibility(View.GONE);
        }
    }
}
