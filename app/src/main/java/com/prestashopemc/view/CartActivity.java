package com.prestashopemc.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.adapter.CartViewAdapter;
import com.prestashopemc.controller.CartController;
import com.prestashopemc.controller.CheckoutController;
import com.prestashopemc.customView.CustomEdittext;
import com.prestashopemc.customView.CustomTextView;
import com.prestashopemc.model.CartDeleteInfo;
import com.prestashopemc.model.CartDeleteModel;
import com.prestashopemc.model.CartDeleteResult;
import com.prestashopemc.model.CartMainModel;
import com.prestashopemc.model.CartQuantityModel;
import com.prestashopemc.model.CartQuantityResult;
import com.prestashopemc.model.CouponMainModel;
import com.prestashopemc.model.CouponResult;
import com.prestashopemc.model.Info;
import com.prestashopemc.model.Prices;
import com.prestashopemc.model.Productiteminfo;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.MyApplication;

import java.util.List;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>Cart Activity!</h1>
 * The Cart Activity implements cart functionality it contains cart products and details edit, delete product.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 19 /4/16
 */
public class CartActivity extends BaseActivity implements View.OnClickListener {

    private CartViewAdapter mCartAdapter;
    private RecyclerView mCartRecyclerView;
    private ImageView mCartCouponIcon;
    private RelativeLayout mCouponLayout, mApplyLayout, mDiscountLayout, mNoCartLayout;
    private boolean mCouponCheck = false;
    private CartController mCartController;
    private TextView mNoCartName, mNoCartMsg, mNoCartShop;
    private CustomTextView mCouponSubmit, mCartDiscount, mCartPriceLable, mCartPrice, mShippingCost, mCartTaxes, mCartTotal, mCartContinue, mCartProceed, mCartOption, mApplyCoupon, mCartpricelable;
    private CustomTextView mCartDiscountLable, mCartShippingLable, mCartTaxLable, mCartTotalLable;
    private CustomEdittext mCouponEdit;
    /**
     * The Info.
     */
    public Info info;
    /**
     * The Product item info.
     */
    public List<Productiteminfo> productItemInfo;
    /**
     * The Cart quantity result.
     */
    public CartQuantityResult cartQuantityResult;
    private ScrollView mScrollView;
    private LinearLayout mLinearLayout;
    /**
     * The Cart delete info.
     */
    public CartDeleteInfo cartDeleteInfo;
    /**
     * The Cart delete result.
     */
    public CartDeleteResult cartDeleteResult;
    private MainActivity mActivity;
    /**
     * The Coupon result.
     */
    public CouponResult couponResult;
    /**
     * The Prices.
     */
    public Prices prices;
    private String mCouponId, mSuppplierMsg;
    private boolean mIsCoupon = false;
    private CheckoutController mCheckoutController;
    /**
     * The Address response.
     */
    public String addressResponse;
    private SharedPreference mPref;
    /**
     * The constant sCartActivity.
     */
    public static Activity sCartActivity;
    /**
     * The constant sCouponCode.
     */
    public static String sCouponCode ="0";

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     *
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cart_layout);
        MyApplication.getDefaultTracker("Cart Screen");
        initToolBar();
        initNetworkError();
        sCartActivity = this;
        logoImage.setVisibility(View.GONE);
        searchIcon.setVisibility(View.VISIBLE);
        cartIcon.setVisibility(View.VISIBLE);
        cartText.setVisibility(View.VISIBLE);
        title.setVisibility(View.VISIBLE);
        title.setText(AppConstants.getTextString(this, AppConstants.myCartText));
        title.setAllCaps(true);
        mActivity = new MainActivity();
        mCartController = new CartController(CartActivity.this);
        mScrollView = (ScrollView) findViewById(R.id.scroll_lay);
        mLinearLayout = (LinearLayout) findViewById(R.id.linearLayout);
        cartCount();
        initialized();

    }

    /**
     * This method is used to restart activity.
     * Default Method for Activity restart.
     *
     * @return Nothing.
     */
    @Override
    protected void onRestart() {
        super.onRestart();
        deleteGuestDetails();
        cartCount();
        finish();
        startActivity(getIntent());
    }

    /**
     * This method is used to Initalized Layout
     *
     * @return Nothing.
     */
    private void initialized() {
        cartIcon.setOnClickListener(null);
        showProgDialiog();
        boolean isInternet = isInternetOn(this);
        if (!isInternet) {
            mScrollView.setVisibility(View.GONE);
            mLinearLayout.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showProgDialiog();
                    initialized();
                }
            });
            hideProgDialog();

        } else {
            //hideProgDialog();
            networkLayout.setVisibility(View.GONE);
            mCartController.cartInfo();
            mPref = SharedPreference.getInstance();
            mCheckoutController = new CheckoutController(CartActivity.this);
            mScrollView.setVisibility(View.GONE);
            mLinearLayout.setVisibility(View.GONE);
            mCartCouponIcon = (ImageView) findViewById(R.id.cart_coupon_icon);
            mCouponLayout = (RelativeLayout) findViewById(R.id.coupon_layout);
            mApplyLayout = (RelativeLayout) findViewById(R.id.apply_coupon_layout);
            mCartRecyclerView = (RecyclerView) findViewById(R.id.cart_recycler_view);
            mDiscountLayout = (RelativeLayout) findViewById(R.id.discount_layout);
            mNoCartLayout = (RelativeLayout) findViewById(R.id.no_cart_layout);
            mCartOption = (CustomTextView) findViewById(R.id.cart_option);
            mCartpricelable = (CustomTextView) findViewById(R.id.cart_price);
            mCouponSubmit = (CustomTextView) findViewById(R.id.coupon_submit);
            mCouponSubmit.setTextColor(Color.parseColor(CustomBackground.mThemeColor));
            CustomBackground.setBackgroundRedWhiteCombination(mCouponSubmit);
            mCartDiscount = (CustomTextView) findViewById(R.id.cart_discount);
            mCartPriceLable = (CustomTextView) findViewById(R.id.cart_pri);
            mCartPrice = (CustomTextView) findViewById(R.id.cart_Price);
            mShippingCost = (CustomTextView) findViewById(R.id.cart_shippingCost);
            mCartTaxes = (CustomTextView) findViewById(R.id.cart_taxes);
            mCartTotal = (CustomTextView) findViewById(R.id.cart_total);
            mCartProceed = (CustomTextView) findViewById(R.id.cart_checkout);
            mCartContinue = (CustomTextView) findViewById(R.id.cart_continue);
            mApplyCoupon = (CustomTextView) findViewById(R.id.apply_coupon);
            mCouponEdit = (CustomEdittext) findViewById(R.id.coupon_edit);
            mCartDiscountLable = (CustomTextView) findViewById(R.id.cart_dis);
            mCartShippingLable = (CustomTextView) findViewById(R.id.cart_ship);
            mCartTaxLable = (CustomTextView) findViewById(R.id.cart_tax);
            mCartTotalLable = (CustomTextView) findViewById(R.id.cart_tot);
            mNoCartName = (TextView) findViewById(R.id.no_cart_name);
            mNoCartMsg = (TextView) findViewById(R.id.no_cart_msg);
            mNoCartShop = (TextView) findViewById(R.id.no_cart_shop_now);
            mNoCartShop.setTextColor(Color.parseColor(CustomBackground.mThemeColor));
            CustomBackground.setBackgroundRedWhiteCombination(mNoCartShop);
            mCartContinue.setTextColor(Color.parseColor(CustomBackground.mThemeColor));
            mCartProceed.setBackgroundColor(Color.parseColor(CustomBackground.mThemeColor));
            mApplyLayout.setOnClickListener(this);
            mCartContinue.setOnClickListener(this);
            mCartProceed.setOnClickListener(this);
            mCouponSubmit.setOnClickListener(this);
            mNoCartShop.setOnClickListener(this);

            dynamicTextValue();
            if (SharedPreference.getInstance().getValue("cart_count").equals("0")) {
                mScrollView.setVisibility(View.GONE);
                mLinearLayout.setVisibility(View.GONE);
                mNoCartLayout.setVisibility(View.VISIBLE);
                hideProgDialog();
            }

            /**
             * Check Coupon code is apply or not
             */
            if (!SharedPreference.getInstance().getValue("couponCode").equals("0")){
                String couponCode = SharedPreference.getInstance().getValue("couponCode");
                mIsCoupon = true;
                mCouponEdit.setText(couponCode);
                //mCartController.couponSumit(couponCode);
                mCouponSubmit.setText(AppConstants.getTextString(this, AppConstants.removeCouponText));
            }else {
                mIsCoupon = false;
                mCouponEdit.getText().clear();
                mCouponSubmit.setText(AppConstants.getTextString(this, AppConstants.submitText));
            }
            setCustomFont();
        }

    }

    /**
     * This method is used to Dynamic Text Value Settings
     *
     * @return Nothing.
     */
    private void dynamicTextValue() {
        mCartOption.setText(AppConstants.getTextString(this, AppConstants.cartOptionText));
        mApplyCoupon.setText(AppConstants.getTextString(this, AppConstants.cartApplyCouponText));
        mCartpricelable.setText(AppConstants.getTextString(this, AppConstants.priceDetailText));
        mCartPriceLable.setText(AppConstants.getTextString(this, AppConstants.cartPriceText));
        mCartShippingLable.setText(AppConstants.getTextString(this, AppConstants.cartShippingcostText));
        mCartTaxLable.setText(AppConstants.getTextString(this, AppConstants.cartTaxesText));
        mCartTotalLable.setText(AppConstants.getTextString(this, AppConstants.cartTotalText));
        mCartContinue.setText(AppConstants.getTextString(this, AppConstants.continueShoppingText));
        mCartProceed.setText(AppConstants.getTextString(this, AppConstants.cartCheckoutText));
        mNoCartName.setText(AppConstants.getTextString(this, AppConstants.noItemsInyourCartText));
        mNoCartMsg.setText(AppConstants.getTextString(this, AppConstants.noItemsCartMessageText));
        mNoCartShop.setText(AppConstants.getTextString(this, AppConstants.shopNowText));
    }

    /**
     * This method is used to Set Custom Font
     *
     * @return Nothing.
     */
    private void setCustomFont() {
        mCouponEdit.setTypeface(robotoRegular);
        mCouponSubmit.setTypeface(robotoRegular);
        mCartOption.setTypeface(robotoMedium);
        mApplyCoupon.setTypeface(robotoLight);
        mCartpricelable.setTypeface(robotoMedium);
        mCartDiscountLable.setTypeface(robotoRegular);
        mCartDiscount.setTypeface(robotoRegular);
        mCartPriceLable.setTypeface(robotoRegular);
        mCartPrice.setTypeface(robotoRegular);
        mCartShippingLable.setTypeface(robotoRegular);
        mShippingCost.setTypeface(robotoRegular);
        mCartTaxLable.setTypeface(robotoRegular);
        mCartTaxes.setTypeface(robotoRegular);
        mCartTotalLable.setTypeface(robotoBold);
        mCartTotal.setTypeface(robotoBold);
        mCartContinue.setTypeface(robotoRegular);
        mCartProceed.setTypeface(robotoRegular);
        mNoCartName.setTypeface(robotoBold);
        mNoCartMsg.setTypeface(robotoLight);
        mNoCartShop.setTypeface(robotoMedium);
    }

    /**
     * This method is used to Getting values from Cart Info API
     *
     * @return Nothing.
     */
    private void gettingValues() {
        String couponSection = SharedPreference.getInstance().getValue("coupon_section_display");
        if (couponSection.equals("0")) {
            mCartOption.setVisibility(View.GONE);
            mApplyLayout.setVisibility(View.GONE);
        }
        String currenySymbol = SharedPreference.getInstance().getValue("currency_symbol");
        mCartRecyclerView.setLayoutManager(new LinearLayoutManager(CartActivity.this));
        mCartAdapter = new CartViewAdapter(this, productItemInfo);
        mCartRecyclerView.setAdapter(mCartAdapter);
        //mCartPriceLable.setText(getString(R.string.cart_price_label1)+mInfo.getItemcount()+getString(R.string.cart_price_label2));
        mCartPrice.setText(currenySymbol + " " + info.getSubtotal());
        mShippingCost.setText(currenySymbol + " " + info.getShippingAmount());
        mCartTaxes.setText(currenySymbol + " " + info.getTaxAmount());
        mCartTotal.setText(currenySymbol + " " + info.getGrandtotal());
        mPref.save("price", info.getGrandtotal());
        mPref.save("cart_count", info.getItemcount() + "");
        if (info.getGrandtotal().equals("0.00")) {
            mScrollView.setVisibility(View.VISIBLE);
            mLinearLayout.setVisibility(View.VISIBLE);
        } else {
            mScrollView.setVisibility(View.VISIBLE);
            mLinearLayout.setVisibility(View.VISIBLE);
        }
        cartCount();
        hideProgDialog();
    }

    /**
     * This method is used to Getting Item price details from API
     *
     * @return Nothing.
     */
    private void gettingQuantityValues() {
        String currenySymbol = SharedPreference.getInstance().getValue("currency_symbol");
        cartCount();
        mCartPrice.setText(currenySymbol + " " + cartQuantityResult.getSubtotal());
        mShippingCost.setText(currenySymbol + " " + cartQuantityResult.getShippingAmount());
        mCartTaxes.setText(currenySymbol + " " + cartQuantityResult.getTaxAmount());
        mCartTotal.setText(currenySymbol + " " + cartQuantityResult.getGrandtotal());
        mPref.save("price", cartQuantityResult.getGrandtotal());
        mScrollView.setVisibility(View.VISIBLE);
        mLinearLayout.setVisibility(View.VISIBLE);
       /* mSuppplierMsg = cartQuantityResult.getSupplierMessage();
        if (mSuppplierMsg != null){
            mCartController.cartInfo();
        }*/
        hideProgDialog();
    }

    /**
     * This method is used to Getting Values after Quantity update From API
     *
     * @return Nothing.
     */
    private void gettingDeleteValues() {
        String currenySymbol = SharedPreference.getInstance().getValue("currency_symbol");
        cartCount();
        mCartAdapter.notifyDataSetChanged();
        mCartPrice.setText(currenySymbol + " " + cartDeleteInfo.getSubtotal());
        mShippingCost.setText(currenySymbol + " " + cartDeleteInfo.getShippingAmount());
        mCartTaxes.setText(currenySymbol + " " + cartDeleteInfo.getTaxAmount());
        mCartTotal.setText(currenySymbol + " " + cartDeleteInfo.getGrandtotal());
        mPref.save("price", cartDeleteInfo.getGrandtotal());
        mScrollView.setVisibility(View.VISIBLE);
        mLinearLayout.setVisibility(View.VISIBLE);
        hideProgDialog();
    }

    /**
     * This method is used to Cart Info from API
     *
     * @param cartMainModel This parameter is used to updateView method.
     * @return Nothing.
     */
    public void updateView(CartMainModel cartMainModel) {
        info = cartMainModel.getInfo();
        productItemInfo = info.getProductiteminfo();
        gettingValues();
    }

    /**
     * This method is used to Cart items delete
     *
     * @param cartDeleteModel This parameter is used to updateDeleteView method.
     * @return Nothing.
     */
    public void updateDeleteView(CartDeleteModel cartDeleteModel) {
        cartDeleteResult = cartDeleteModel.getResult();
        cartDeleteInfo = cartDeleteResult.getInfo();
        SharedPreference mPref = SharedPreference.getInstance();
        mPref.save("cart_count", cartDeleteResult.getCartCount() + "");
        if (cartDeleteResult.getCartCount() == 0) {
            mScrollView.setVisibility(View.GONE);
            mLinearLayout.setVisibility(View.GONE);
            mNoCartLayout.setVisibility(View.VISIBLE);
            cartCount();
            //snackBar(getString(R.string.cart_delete));
            hideProgDialog();
        } else {
            gettingDeleteValues();
        }
    }

    /**
     * This method is used to Cart items Quantity update response
     *
     * @param cartQuantityModel This is first parameter of updateQuantityView method.
     * @param quant             This is second parameter of updateQuantityView method.
     * @return Nothing.
     */
    public void updateQuantityView(CartQuantityModel cartQuantityModel, String quant) {
        cartQuantityResult = cartQuantityModel.getResult();
        SharedPreference mPref = SharedPreference.getInstance();
        mPref.save("cart_id", cartQuantityResult.getCartId() + "");
        mPref.save("cart_count", cartQuantityResult.getCartCount() + "");
        gettingQuantityValues();
        mCartController.cartInfo();

//        mSuppplierMsg = cartQuantityResult.getSupplierMessage();

     /*   if (mSuppplierMsg != null){
            mCartController.cartInfo();
        }*/
        //Log.e("Quantity Number Position ==== ", quantPosition);
        //cartAdapter.updateQuantity(quant, quantPosition);
    }


    public void quantityFailure(String mErrorMsg) {
        snackBar(mErrorMsg);
        mCartController.cartInfo();
    }

    /**
     * This method is used to Cart info failure response from API
     *
     * @param response This parameter is used to failure method.
     * @return Nothing.
     */
    public void failure(String response) {
        hideProgDialog();
        snackBar(response);
    }

    private View.OnClickListener proceedPaymentClick = new View.OnClickListener() {
        /**
         * This method is used to Proceed to Payment
         * @param v This is the parameter used in onClick method.
         * @return Nothing.
         */
        @Override
        public void onClick(View v) {

        }
    };

    /**
     * This is the method to Coupon Success Response From API
     *
     * @param couponMainModel This is the parameter used in updateCouponValues method.
     */
    public void updateCouponValues(CouponMainModel couponMainModel) {
        couponResult = couponMainModel.getResult();
        prices = couponResult.getPrices();
        if (couponResult.getPrices().getCouponId()!=null) {
            sCouponCode = couponResult.getPrices().getCouponId();
        }
        getCouponValues();
    }

    /**
     * This is the method to Coupon Values
     */
    private void getCouponValues() {
        mDiscountLayout.setVisibility(View.VISIBLE);
        String currenySymbol = SharedPreference.getInstance().getValue("currency_symbol");
        mCouponSubmit.setText(AppConstants.getTextString(this, AppConstants.removeCouponText));
        mCartDiscount.setText(currenySymbol + " " + prices.getDiscount());
        mCartPrice.setText(currenySymbol + " " + prices.getSubtotal());
        mShippingCost.setText(currenySymbol + " " + prices.getShippingAmount());
        mCartTaxes.setText(currenySymbol + " " + prices.getTax());
        mCartTotal.setText(currenySymbol + " " + prices.getGrandtotal());
        hideProgDialog();
    }

    /**
     * This is the method to Coupon Remove Success Response From API
     *
     * @param couponMainModel This is the parameter use din couponMainModel method.
     */
    public void couponRemoveValues(CouponMainModel couponMainModel) {
        SharedPreference.getInstance().removeValue("couponCode");
        couponResult = couponMainModel.getResult();
        prices = couponResult.getPrices();
        getCouponRemoveValues();
    }

    /**
     * This is the method to Coupon Remove Values
     */
    private void getCouponRemoveValues() {
        mCouponSubmit.setText(AppConstants.getTextString(this, AppConstants.submitText));
        mCouponEdit.getText().clear();
        mDiscountLayout.setVisibility(View.GONE);
        String currenySymbol = SharedPreference.getInstance().getValue("currency_symbol");
        mCartPrice.setText(currenySymbol + " " + prices.getSubtotal());
        mShippingCost.setText(currenySymbol + " " + prices.getShippingAmount());
        mCartTaxes.setText(currenySymbol + " " + prices.getTax());
        mCartTotal.setText(currenySymbol + " " + prices.getGrandtotal());
        hideProgDialog();
    }

    /**
     * This is the method to Address Success Response From API
     *
     * @param response This is the parameter used in addressResponse method.
     */
    public void addressResponse(String response) {
        hideProgDialog();
        deleteValue();
        addressResponse = response;
        SharedPreference mPref = SharedPreference.getInstance();
        mPref.save("response", response);
        Intent intent = new Intent(CartActivity.this, CheckoutActivity.class);
        startActivity(intent);
    }

    /**
     * This is the method to Address Failure Response From API
     *
     * @param response This is the parameter used in addressFailure method.
     */
    public void addressFailure(String response) {
        hideProgDialog();
        if (response.equalsIgnoreCase(getString(R.string.noAddressText))) {
            Intent newaddress = new Intent(CartActivity.this, AddNewAddress.class);
            startActivity(newaddress);
        } else if (response.equalsIgnoreCase(getString(R.string.noResultsText))) {
            Intent newaddress = new Intent(CartActivity.this, AddNewAddress.class);
            startActivity(newaddress);
        }else if (response.equalsIgnoreCase(getString(R.string.noAddressAvailable))) {
            Intent newaddress = new Intent(CartActivity.this, AddNewAddress.class);
            startActivity(newaddress);
        } else {
            snackBar(response);
        }
    }

    /**
     * This is the method to back pressed
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        hideProgDialog();
    }

    /**
     * This is the method to on click
     *
     * @param v This is the parameter use din onClick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * Coupon Code section Visible*/
            case R.id.apply_coupon_layout:
                if (mCouponCheck == false) {
                    mCartCouponIcon.setImageResource(R.drawable.ic_cart_top);
                    mCouponLayout.setVisibility(View.VISIBLE);
                    mCouponCheck = true;

                } else {
                    mCartCouponIcon.setImageResource(R.drawable.ic_cart_bottom);
                    mCouponLayout.setVisibility(View.GONE);
                    mCouponCheck = false;
                }
                break;

            /**
             * Continue Shopping*/
            case R.id.cart_continue:
                finish();
                break;

            /**
             * Proceed to Payment*/
            case R.id.cart_checkout:
                MyApplication.eventTracking("Proceed to Checkout", "Proceed to Checkout button pressed", "Proceed to Checkout");
                showProgDialiog();
                //checkoutController.initialAddress();
                if (!SharedPreference.getInstance().getValue("customerid").equals("0")) {
                    mCheckoutController.initialAddress();
                } else {
                    hideProgDialog();
                    deleteValue();
                    deleteGuestDetails();
                    Intent newaddress = new Intent(CartActivity.this, AccountActivity.class);
                    newaddress.putExtra("guest", "guest");
                    startActivity(newaddress);
                    //snackBar(getString(R.string.login_alert));
                }
                break;

            /**
             * Coupon Submit Listener*/
            case R.id.coupon_submit:
                mCouponId = mCouponEdit.getText().toString();
                if (!mIsCoupon) {
                    if (mCouponEdit.getText().length() == 0) {
                        snackBar(AppConstants.getTextString(CartActivity.this, AppConstants.enterCouponText));
                    } else {
                        showProgDialiog();
                        mIsCoupon = true;
                        SharedPreference.getInstance().save("couponCode", mCouponId);
                        mCartController.couponSumit(mCouponId);
                    }
                } else {
                    //snackBar(getString(R.string.cart_processing));
                    showProgDialiog();
                    mIsCoupon = false;
                    mCartController.couponRemove(mCouponId);

                }
                break;

            /**
             * Shop Now Click Listener*/
            case R.id.no_cart_shop_now:
                Intent mainIntent = new Intent(CartActivity.this, MainActivity.class);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(mainIntent);
                //finish();
                break;
        }
    }
}
