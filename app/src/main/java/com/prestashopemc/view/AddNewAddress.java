package com.prestashopemc.view;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.prestashopemc.R;
import com.prestashopemc.controller.CheckoutController;
import com.prestashopemc.model.AddressValues;
import com.prestashopemc.model.CountryMainModel;
import com.prestashopemc.model.CountryResult;
import com.prestashopemc.model.StateMainModel;
import com.prestashopemc.model.StateResult;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CommonValidation;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.MyApplication;

import java.util.ArrayList;
import java.util.List;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>Add new Address Activity!</h1>
 * This Add new Address Activity implements add new address for user
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author Rajesh
 * @version 1.2
 * @since 30 /09/2016
 */
public class AddNewAddress extends BaseActivity implements View.OnClickListener {

    private TextView mNewAddressLabel, mNameLabel, mLastNameLabel, mEmailLabel, mMobileLabel, mStreetLabel, mPincodeLabel, mCityLabel, mStateLabel, mCountryLabel, mCancel, mSubmit;
    private EditText mNameEdit, mEmailEdit, mMobileEdit, mPincodeEdit, mCityEdit, mLastNameEdit, mStreetEdit;
    private Spinner mStateSpinner, mCountrySpinner;
    private String mFirstName, mLastName, mStreet, mCity, mPhone, mPinCode, mEditAddr, mIdAddress;
    private Boolean isDefault = false;
    private CheckoutController mCheckoutController;
    /**
     * The Country result.
     */
    public List<CountryResult> countryResult = new ArrayList<CountryResult>();
    private LinearLayout mStateLayout, mEmailLayout;
    /**
     * The State results.
     */
    public List<StateResult> stateResults = new ArrayList<StateResult>();
    /**
     * The Country.
     */
    public String country, /**
     * The Country id.
     */
    countryId, /**
     * The Region id.
     */
    regionId, /**
     * The Region.
     */
    region;
    private boolean mIsStateEdit = false;
    private Bundle mBundle;
    private String mAddressType, mName, mAddress, mDefaultAddress;
    private ScrollView mMainLayout;
    private ImageView mStateImg, mCountryImg;
    private String stateSelected, countrySelected;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_new_address);
        MyApplication.getDefaultTracker("Add New Screen");
        initNetworkError();
        initToolBar();
        logoImage.setVisibility(View.GONE);
        searchIcon.setVisibility(View.VISIBLE);
        cartIcon.setVisibility(View.VISIBLE);
        cartText.setVisibility(View.VISIBLE);
        title.setVisibility(View.VISIBLE);
        title.setText(AppConstants.getTextString(this, AppConstants.checkoutText));
        title.setAllCaps(true);
        mMainLayout = (ScrollView) findViewById(R.id.main_layout);
        cartCount();
        initialized();
    }

    /**
     * This method is used to Initialized Layout.
     * @return Nothing.
     */
    private void initialized() {
        showProgDialiog();
        boolean isInternet = isInternetOn(this);
        if (!isInternet) {
            mMainLayout.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showProgDialiog();
                    initialized();
                }
            });
            hideProgDialog();
        } else {
            mMainLayout.setVisibility(View.VISIBLE);
            networkLayout.setVisibility(View.GONE);
            //hideProgDialog();
            mCheckoutController = new CheckoutController(AddNewAddress.this);
            mStateSpinner = (Spinner) findViewById(R.id.state_spinner);
            mCountrySpinner = (Spinner) findViewById(R.id.country_spinner);
            mStateLayout = (LinearLayout) findViewById(R.id.state_layout);
            mEmailLayout = (LinearLayout) findViewById(R.id.email_layout);
            mCheckoutController.loadCountry();
            mNewAddressLabel = (TextView) findViewById(R.id.addnew_address_label);
            mNameLabel = (TextView) findViewById(R.id.new_address_name);
            mLastNameLabel = (TextView) findViewById(R.id.new_address_lastname);
            mEmailLabel = (TextView) findViewById(R.id.new_address_email);
            mMobileLabel = (TextView) findViewById(R.id.new_address_mobile);
            mStreetLabel = (TextView) findViewById(R.id.new_address_street);
            mPincodeLabel = (TextView) findViewById(R.id.new_address_pincode);
            mCityLabel = (TextView) findViewById(R.id.new_address_city);
            mStateLabel = (TextView) findViewById(R.id.new_address_state);
            mCountryLabel = (TextView) findViewById(R.id.new_address_country);
            mCancel = (TextView) findViewById(R.id.new_address_cancel);
            mSubmit = (TextView) findViewById(R.id.new_address_submit);
            mNameEdit = (EditText) findViewById(R.id.new_address_nameEdit);
            mLastNameEdit = (EditText) findViewById(R.id.new_address_lastnameEdit);
            mEmailEdit = (EditText) findViewById(R.id.new_address_emailEdit);
            mMobileEdit = (EditText) findViewById(R.id.new_address_mobileEdit);
            mStreetEdit = (EditText) findViewById(R.id.new_address_streetEdit);
            mPincodeEdit = (EditText) findViewById(R.id.new_address_pincodeEdit);
            mCityEdit = (EditText) findViewById(R.id.new_address_cityEdit);
            mStateImg = (ImageView) findViewById(R.id.state_img);
            mCountryImg = (ImageView) findViewById(R.id.country_img);
            CustomBackground.setBackgroundText(mSubmit);
            mCancel.setOnClickListener(this);
            mSubmit.setOnClickListener(this);
            mStateImg.setOnClickListener(this);
            mCountryImg.setOnClickListener(this);
            dynamicTextValue();
            setCustomFont();
            loadEditValues();
        }
    }

    /**
     * This method is used to Dynamic Text Value Settings.
     * @return Nothing.
     */
    private void dynamicTextValue() {
        mNewAddressLabel.setText(AppConstants.getTextString(this, AppConstants.addNewAddressText));
        mNameLabel.setText(AppConstants.getTextString(this, AppConstants.newAddressFirstnameText));
        mLastNameLabel.setText(AppConstants.getTextString(this, AppConstants.newAddressLastnameText));
        mEmailLabel.setText(AppConstants.getTextString(this, AppConstants.emailText));
        mMobileLabel.setText(AppConstants.getTextString(this, AppConstants.newAddressMobileText));
        mStreetLabel.setText(AppConstants.getTextString(this, AppConstants.newAddressStreetText));
        mPincodeLabel.setText(AppConstants.getTextString(this, AppConstants.newAddressPincodetext));
        mCityLabel.setText(AppConstants.getTextString(this, AppConstants.newAddressCityText));
        mStateLabel.setText(AppConstants.getTextString(this, AppConstants.newAddressStateText));
        mCountryLabel.setText(AppConstants.getTextString(this, AppConstants.newAddressCountryText));
        mCancel.setText(AppConstants.getTextString(this, AppConstants.cancelText));
        mSubmit.setText(AppConstants.getTextString(this, AppConstants.submitText));
    }

    /**
     * This method is used to Set Custom Font.
     * @return Nothing.
     */
    private void setCustomFont() {
        mNewAddressLabel.setTypeface(robotoRegular);
        mNameLabel.setTypeface(robotoLight);
        mLastNameLabel.setTypeface(robotoLight);
        mEmailLabel.setTypeface(robotoLight);
        mMobileLabel.setTypeface(robotoLight);
        mStreetLabel.setTypeface(robotoLight);
        mPincodeLabel.setTypeface(robotoLight);
        mCityLabel.setTypeface(robotoLight);
        mStateLabel.setTypeface(robotoLight);
        mCountryLabel.setTypeface(robotoLight);
        mCancel.setTypeface(robotoMedium);
        mSubmit.setTypeface(robotoMedium);
        title.setTypeface(robotoMedium);
        mNameEdit.setTypeface(robotoLight);
        mLastNameEdit.setTypeface(robotoLight);
        mEmailEdit.setTypeface(robotoLight);
        mMobileEdit.setTypeface(robotoLight);
        mStreetEdit.setTypeface(robotoLight);
        mPincodeEdit.setTypeface(robotoLight);
        mCityEdit.setTypeface(robotoLight);
    }

    /**
     * This method is used to Send Customer Address Details.
     * @return Nothing.
     */
    private void sendAddressDetails() {
        //showProgDialiog();
        AddressValues valuesObj = new AddressValues();

        valuesObj.setFname(mNameEdit.getText().toString().trim());
        valuesObj.setLname(mLastNameEdit.getText().toString().trim());
        if (mEmailEdit.getText().toString().isEmpty()) {
            valuesObj.setEmail("sample@gmail.com");
        } else {
            valuesObj.setEmail(mEmailEdit.getText().toString().trim());
        }
        valuesObj.setPhone(mMobileEdit.getText().toString().trim());
        valuesObj.setStreet(mStreetEdit.getText().toString().trim());
        valuesObj.setCity(mCityEdit.getText().toString().trim());
        valuesObj.setZipcode(mPincodeEdit.getText().toString().trim());
        valuesObj.setCountry(country);
        if (mIsStateEdit) {
            valuesObj.setState("");
            valuesObj.setState_id("0");
        } else {
            valuesObj.setState(region);
            valuesObj.setState_id(regionId);
        }
        CommonValidation validation = new CommonValidation();
        boolean status = validation.addressValidation(AddNewAddress.this, valuesObj, !mIsStateEdit);
        if (status) {
            showProgDialiog();
            mCheckoutController.submitNewAddress(valuesObj, mEditAddr, mIdAddress, countryId, isDefault);

        } else {
            hideProgDialog();
        }
    }

    /**
     * This method is used to Guest Checkout.
     * @return Nothing.
     */
    private void guestCheckOut() {
        showProgDialiog();
        AddressValues valuesObj = new AddressValues();
        valuesObj.setFname(mNameEdit.getText().toString().trim());
        valuesObj.setLname(mLastNameEdit.getText().toString().trim());
        valuesObj.setEmail(mEmailEdit.getText().toString().trim());
        valuesObj.setPhone(mMobileEdit.getText().toString().trim());
        valuesObj.setStreet(mStreetEdit.getText().toString().trim());
        valuesObj.setCity(mCityEdit.getText().toString().trim());
        valuesObj.setZipcode(mPincodeEdit.getText().toString().trim());
        valuesObj.setCountry(country);
        if (mIsStateEdit) {
            valuesObj.setState("");
            valuesObj.setState_id("0");
        } else {
            valuesObj.setState(region);
            valuesObj.setState_id(regionId);
        }
        //Gson gson = new Gson();
        String addressValuesObject = MyApplication.getGsonInstance().toJson(valuesObj);
        if (mAddressType == null) {
            CommonValidation validation = new CommonValidation();
            boolean status = validation.addressValidation(AddNewAddress.this, valuesObj, !mIsStateEdit);
            if (status) {
                SharedPreference.getInstance().save("shippingAddressValues", addressValuesObject);
                SharedPreference.getInstance().save("billingAddressValues", addressValuesObject);
                SharedPreference.getInstance().save("shippingCountryId", countryId);
                SharedPreference.getInstance().save("billingCountryId", countryId);
                SharedPreference.getInstance().save("checkout", "checkout");
                Intent intent = new Intent(AddNewAddress.this, CheckoutActivity.class);
                //intent.putExtra("checkout", "checkout");
                startActivity(intent);
                finish();
            } else {
                hideProgDialog();
            }
        } else {
            if (mAddressType.equals("shipping")) {
                SharedPreference.getInstance().save("shipping", "shipping");
                SharedPreference.getInstance().save("shippingAddressValues", addressValuesObject);
                SharedPreference.getInstance().save("shippingCountryId", countryId);
                mName = valuesObj.getFname() + " " + valuesObj.getLname();
                mAddress = valuesObj.getStreet() + "\n" + valuesObj.getCity() + ", " + valuesObj.getCountry() + "\n" +
                        valuesObj.getZipcode() + ", " + valuesObj.getPhone();
            } else if (mAddressType.equals("billing")) {
                SharedPreference.getInstance().save("billing", "billing");
                SharedPreference.getInstance().save("billingAddressValues", addressValuesObject);
                SharedPreference.getInstance().save("billingCountryId", countryId);
                mName = valuesObj.getFname() + " " + valuesObj.getLname();
                mAddress = valuesObj.getStreet() + "\n" + valuesObj.getCity() + ", " + valuesObj.getCountry() + "\n" +
                        valuesObj.getZipcode() + ", " + valuesObj.getPhone();
            }
            Intent intent = new Intent(AddNewAddress.this, CheckoutActivity.class);
            intent.putExtra("name", mName);
            intent.putExtra("address", mAddress);
            intent.putExtra("addressSelect", mAddressType);
            startActivity(intent);
            finish();
        }
    }

    /**
     * This method is used to Add new address Submit Success Response From API.
     *
     * @return Nothing.
     */
    public void successSubmit() {
        hideProgDialog();
        //snackBar(getString(R.string.new_address_success));
        if (mDefaultAddress != null && mDefaultAddress.equals("newAddress")) {
            //checkoutController.addressList();
            Toast.makeText(AddNewAddress.this, AppConstants.getTextString(this, AppConstants.newAddressSuccessText), Toast.LENGTH_SHORT).show();
            finish();
        } else if (mDefaultAddress != null && mDefaultAddress.equals("fromDefault")) {
            Toast.makeText(AddNewAddress.this, AppConstants.getTextString(this, AppConstants.newAddressSuccessText), Toast.LENGTH_SHORT).show();
            finish();
        } else if (mDefaultAddress != null && mDefaultAddress.equals("fromDefaultEdit")) {
            Toast.makeText(AddNewAddress.this, AppConstants.getTextString(this, AppConstants.newAddressUpdatedSuccess), Toast.LENGTH_SHORT).show();
            finish();
        } else {
            mCheckoutController.initialAddress();
            finish();
        }
    }

    /**
     * This method is used to add new address Submit Failure Response From API.
     *
     * @param errormsg the errormsg
     * @return Nothing.
     */
    public void failure(String errormsg) {
        hideProgDialog();
        snackBar(errormsg);
    }

    /**
     * This method is used to update country list.
     *
     * @param countryMainModel the country main model
     * @return Nothing.
     */
    public void updateCountry(CountryMainModel countryMainModel) {
        hideProgDialog();
        countryResult = countryMainModel.getResult();
        spinnerListener();
    }

    /**
     * This method is used to Country Spinner Listener.
     * @return Nothing.
     */
    private void spinnerListener() {
        ArrayAdapter<CountryResult> spinnerArrayAdapter = new ArrayAdapter<CountryResult>(AddNewAddress.this,
                R.layout.spinner_text, countryResult);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mCountrySpinner.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
        mCountrySpinner.setAdapter(spinnerArrayAdapter);
        if (countrySelected != null) {
            for (int i = 0; i < countryResult.size(); i++) {
                if (countryResult.get(i).getName().equals(countrySelected)) {
                    mCountrySpinner.setSelection(i);
                }
            }
        }
        mCountrySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.new_address_edit_text));
                ((TextView) parent.getChildAt(0)).setTypeface(robotoLight);
                countryId = countryResult.get(position).getCountryId() + "";
                country = countryResult.get(position).getName();
                showProgDialiog();
                mCheckoutController.loadState(countryResult.get(position).getCountryId() + "");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     * This method is used to update State List.
     *
     * @param stateMainModel This is the first parameter to update state method.
     * @return Nothing.
     */
    public void updateState(StateMainModel stateMainModel) {
        stateResults = stateMainModel.getResult();
        hideProgDialog();
        if (stateResults.size() != 0) {
            mStateLayout.setVisibility(View.VISIBLE);
            stateSpinnerListener();
        } else {
            mStateLayout.setVisibility(View.GONE);
        }
    }

    /**
     * This method is used to State Spinner Listener.
     * @return Nothing.
     */
    private void stateSpinnerListener() {
        ArrayAdapter<StateResult> spinnerArrayAdapter = new ArrayAdapter<StateResult>(AddNewAddress.this,
                R.layout.spinner_text, stateResults);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mStateSpinner.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
        mStateSpinner.setAdapter(spinnerArrayAdapter);
        if (stateSelected != null){
            for (int i = 0; i < stateResults.size(); i++){
                if (stateResults.get(i).getDefaultName().equals(stateSelected)){
                    mStateSpinner.setSelection(i);
                }
            }
        }
        mStateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(getResources().getColor(R.color.new_address_edit_text));
                ((TextView) parent.getChildAt(0)).setTypeface(robotoLight);
                region = stateResults.get(position).getDefaultName();
                regionId = stateResults.get(position).getRegionId() + "";
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    /**
     * This method is used to Load Edit Address Values.
     * @return Nothing.
     */
    private void loadEditValues() {
        mBundle = getIntent().getExtras();
        if (mBundle != null) {
            mFirstName = mBundle.getString("fname");
            mLastName = mBundle.getString("lname");
            mStreet = mBundle.getString("street");
            mCity = mBundle.getString("city");
            mPinCode = mBundle.getString("postcode");
            mPhone = mBundle.getString("telephone");
            mEditAddr = mBundle.getString("edit");
            stateSelected = mBundle.getString("state");
            countrySelected = mBundle.getString("country");
            mIdAddress = mBundle.getString("idaddress");
            mAddressType = mBundle.getString("addressSelect");
            mDefaultAddress = mBundle.getString("fromDefault");
            isDefault = mBundle.getBoolean("isDefault");
            if (mDefaultAddress != null) {
                if (mDefaultAddress.equals("fromDefault")) {
                    title.setText(AppConstants.getTextString(this, AppConstants.addNewAddressText));
                    cartIcon.setVisibility(View.GONE);
                    cartText.setVisibility(View.GONE);
                    searchIcon.setVisibility(View.GONE);
                } else if (mDefaultAddress.equals("fromDefaultEdit")) {
                    title.setText(AppConstants.getTextString(this, AppConstants.accountEditAddress));
                    mNewAddressLabel.setText(AppConstants.getTextString(this, AppConstants.accountEditAddress));
                    mEmailLayout.setVisibility(View.GONE);
                }
            }

            mNameEdit.setText(mFirstName);
            mLastNameEdit.setText(mLastName);
            mStreetEdit.setText(mStreet);
            mCityEdit.setText(mCity);
            mPincodeEdit.setText(mPinCode);
            mMobileEdit.setText(mPhone);
            if (mEditAddr != null) {
                mSubmit.setText(AppConstants.getTextString(this, AppConstants.saveChangesText));
            }
        }
    }

    /**
     * This method is used to Address success response.
     *
     * @param response The first parameter to address response method.
     * @return Nothing.
     */
    public void addressResponse(String response) {
        hideProgDialog();
        deleteValue();
        SharedPreference mPref = SharedPreference.getInstance();
        mPref.save("response", response);
        Intent intent = new Intent(AddNewAddress.this, CheckoutActivity.class);
        startActivity(intent);
    }

    /**
     * This method is used to Address failure response.
     *
     * @param errormsg The first parameter to address creation failure.
     * @return Nothing.
     */
    public void addressFailure(String errormsg) {
        snackBar(errormsg);
    }

    /**
     * This method is used to Click Listener
     * @param v This is the parameter used in onClick
     * @return Nothing.
     * */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * This method is used to Address Cancel Listener
             * */
            case R.id.new_address_cancel:
                finish();
                break;

            /**
             * Address Submit Listener
             * */
            case R.id.new_address_submit:
                if (!SharedPreference.getInstance().getValue("customerid").equals("0")) {
                    MyApplication.eventTracking("Add new Address", "New Address added", "Add new Address");
                    sendAddressDetails();
                } else {
                    guestCheckOut();
                }
                break;

            /**
             * State click listener
             */
            case R.id.state_img:
                mStateSpinner.performClick();
                break;

            /**
             * Country click listener
             */
            case R.id.country_img:
                mCountrySpinner.performClick();
                break;
        }
    }
}
