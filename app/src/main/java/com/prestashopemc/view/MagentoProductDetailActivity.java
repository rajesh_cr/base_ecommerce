package com.prestashopemc.view;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.text.InputFilter;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.android.gms.analytics.ecommerce.Product;
import com.prestashopemc.R;
import com.prestashopemc.adapter.ViewPagerAdapter;
import com.prestashopemc.controller.ProductDetailController;
import com.prestashopemc.controller.ProductListController;
import com.prestashopemc.controller.WriteReviewController;
import com.prestashopemc.customView.CustomTextView;
import com.prestashopemc.fragment.DescriptionFragment;
import com.prestashopemc.fragment.KeyfeaturesFragment;
import com.prestashopemc.fragment.MagentoOverviewFragment;
import com.prestashopemc.fragment.MagentoReviewFragment;
import com.prestashopemc.fragment.OverviewFragment;
import com.prestashopemc.fragment.RelatedProductFragment;
import com.prestashopemc.fragment.ReviewFragment;
import com.prestashopemc.model.Attributes;
import com.prestashopemc.model.CategoryProduct;
import com.prestashopemc.model.Feature;
import com.prestashopemc.model.KeyValues;
import com.prestashopemc.model.MagentoCategoryProduct;
import com.prestashopemc.model.MagentoProductDetail;
import com.prestashopemc.model.MagentoProductDetailMain;
import com.prestashopemc.model.MagentoProductinfo;
import com.prestashopemc.model.ProductDetail;
import com.prestashopemc.model.ProductDetailMain;
import com.prestashopemc.model.Productinfo;
import com.prestashopemc.model.RatingMainModel;
import com.prestashopemc.model.RatingResult;
import com.prestashopemc.model.Ribbon;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.SharedPreference;

import java.util.ArrayList;
import java.util.List;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>Product Detail Activity!</h1>
 * The Product Detail Activity implements product details and its functionalities.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 21 /3/16
 */
public class MagentoProductDetailActivity extends BaseActivity {
    private TabLayout mTabLayout;
    private ViewPager mViewPager;
    public MagentoProductinfo productinfo;
    public Ribbon productInfoRibbon;
    public Boolean productinfoRibbonEnabled;
    public Ribbon ribbon;
    public static List<Ribbon> sRibbonList = new ArrayList<>();
    public List<Feature> feature;
    public MagentoProductDetail productDetail;
    private ProductDetailController mProductDetailController;
    public String productId,productName;
    private Bundle mBundle;
    public String ratingReviews,wishlistStatus;
    private WriteReviewController mWriteReviewController;
    public List<RatingResult> ratingResult;
    public MagentoCategoryProduct categoryProduct;
    private RelativeLayout mMainLayout;
    private CustomTextView mAddCartText;
    private ProductListController mProductListController;
    private List<Attributes> mKeyValues;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     *
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_detail);
        MyApplication.getDefaultTracker("Product Detail Screen");
        initToolBar();
        initNetworkError();
        categoryTitle.setVisibility(View.VISIBLE);
        categoryTitle.setAllCaps(true);
        categoryTitle.setPadding(0, 13, 0, 0);
        logoImage.setVisibility(View.GONE);
        cartIcon.setVisibility(View.VISIBLE);
        searchIcon.setVisibility(View.VISIBLE);
        mMainLayout = (RelativeLayout) findViewById(R.id.main_layout);
        mBundle = getIntent().getExtras();
        if (mBundle != null) {
            productId = mBundle.getString("product_id");
            productName = mBundle.getString("product_name");
            //Log.e("Product ID ====== ", productId);
            categoryProduct = (MagentoCategoryProduct) getIntent().getSerializableExtra("model");
            categoryTitle.setFilters(new InputFilter[]{new InputFilter.LengthFilter(14)});
            categoryTitle.setText(productName);
        }
        cartCount();
        showProgDialiog();
        initializedLayout();
    }

    /**
     * This method is used to Initialized Layout
     */

    private void initializedLayout() {
        showProgDialiog();
        boolean isInternet = isInternetOn(this);
        if (!isInternet) {
            mMainLayout.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initializedLayout();
                }
            });
            hideProgDialog();
        } else {
            mMainLayout.setVisibility(View.VISIBLE);
            networkLayout.setVisibility(View.GONE);
            //hideProgDialog();
            mViewPager = (ViewPager) findViewById(R.id.viewpager);
            mTabLayout = (TabLayout) findViewById(R.id.tab_layout);
            mAddCartText = (CustomTextView) findViewById(R.id.add_to_cart_bottom_button);
            mTabLayout.setupWithViewPager(mViewPager);
            mTabLayout.addOnTabSelectedListener(tabSelectedListener);
            AppConstants.setBackgroundColor(mTabLayout);
            mProductDetailController = new ProductDetailController(MagentoProductDetailActivity.this);
            mProductDetailController.productDetail(productId);
            mProductListController = new ProductListController(MagentoProductDetailActivity.this);
            if (SharedPreference.getInstance().getValue("cart_section_display").equalsIgnoreCase("2")) {
                mAddCartText.setVisibility(View.VISIBLE);
                mAddCartText.setTypeface(robotoMedium);
                CustomBackground.setBackgroundText(mAddCartText);
            } else {
                mAddCartText.setVisibility(View.GONE);
            }
            mAddCartText.setOnClickListener(addCartClick);
        }
    }

    /**
     * This method is used to Refresh Cart Count
     */
    @Override
    public void onResume() {
        super.onResume();
        cartCount();
    }

    /**
     * This method is used to get ribon list.
     *
     * @return List<Ribbon>  returns list of ribbon values.
     */
    public static List<Ribbon> getRibbionList() {
        //  mRibbonList.addAll(productDetailMain.getprdDetail().getProductinfo().getRibbon());
        return sRibbonList;
    }

    /**
     * This method is used to Product details Success response from API
     *
     * @param productDetailMain This is the parameter to productDetailSuccess method.
     */
    public void productDetailSuccess(MagentoProductDetailMain productDetailMain) {
        hideProgDialog();
        productDetail = productDetailMain.getprdDetail();
        productinfo = productDetailMain.getprdDetail().getProductInfo();
        ribbon = productDetailMain.getprdDetail().getRibbon();
//        productinfoRibbonEnabled = productDetailMain.getprdDetail().getProductinfo().getRibbonEnabled();
//        /**Check Ribbon Enabled True or False*/
//        if (productDetailMain.getprdDetail().getProductinfo().getRibbonEnabled()) {
//            sRibbonList.clear();
//            sRibbonList.addAll(productDetailMain.getprdDetail().getProductinfo().getRibbon());
//            getRibbionList();
//        }

        feature = productDetail.getFeatures();
        categoryTitle.setText(productinfo.getName());
        setupViewPager(mViewPager);
        //ecommerce tracking for getting product impression details
        Product product = new Product()
                .setId(productId)
                .setName(productName)
                .setCategory(productinfo.getCategory());
                /*.setBrand("Google")
                .setVariant("Black")
                .setPosition(1)
                .setCustomDimension(1, "Member");*/
        HitBuilders.ScreenViewBuilder builder = new HitBuilders.ScreenViewBuilder()
                .addImpression(product, "Search Results");
        GoogleAnalytics googleAnalytics = GoogleAnalytics.getInstance(MagentoProductDetailActivity.this);
        //Tracker sTracker = googleAnalytics.newTracker(R.xml.app_tracker);
        Tracker sTracker = googleAnalytics.newTracker(SharedPreference.getInstance().getValue("google_tracking"));
        sTracker.setScreenName("Product Detail");
        sTracker.send(builder.build());
    }

    /**
     * This method is used to Product details Failure response from API
     *
     * @param failureResponse This is the parameter to productDetailFailure method.
     */
    public void productDetailFailure(String failureResponse) {
        hideProgDialog();
        snackBar(failureResponse);
    }

    /**
     * This method is used to Product details Rating & Reviews Success response from API
     *
     * @param ratingMainModel This is the parameter to getratingSuccess method.
     */
    public void getratingSuccess(RatingMainModel ratingMainModel) {
        hideProgDialog();
        ratingResult = ratingMainModel.getResult();
    }

    /**
     * This method is used to Product details Submit Rating Success response from API
     *
     * @param reviewSuccess This is the parameter to submitSuccessResponse method.
     */
    public void submitSuccessResponse(String reviewSuccess) {
        hideProgDialog();
        setupViewPager(mViewPager);
        Toast.makeText(this, reviewSuccess, Toast.LENGTH_SHORT).show();
        mProductDetailController.productDetail(productId);
    }

    /**
     * This method is used to Product details Rating Failure response from API
     *
     * @param failureresponse This is the parameter to getratingFailure method.
     */
    public void getratingFailure(String failureresponse) {
        hideProgDialog();
        snackBar(failureresponse);
    }

    /**
     * This method is used to Setup View Pager
     *
     * @param viewPager This is the parameter to setupViewPager method.
     */
    private void setupViewPager(ViewPager viewPager) {
        hideProgDialog();
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new MagentoOverviewFragment(), AppConstants.getTextString(this, AppConstants.overViewText));

        if (!productinfo.getDescription().equals("")) {
            adapter.addFragment(new DescriptionFragment(), AppConstants.getTextString(this, AppConstants.descriptionText));
        }
        adapter.addFragment(new RelatedProductFragment(), AppConstants.getTextString(this, AppConstants.relatedProductsText));
        ratingReviews = SharedPreference.getInstance().getValue("review_rating_status");
        wishlistStatus = SharedPreference.getInstance().getValue("wishlist_enable_status");
        if (ratingReviews.equals("true")) {
           /* mWriteReviewController = new WriteReviewController(ProductDetailActivity.this);
            mWriteReviewController.gettingReviewOption();*/
            //mTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
            adapter.addFragment(new MagentoReviewFragment(), AppConstants.getTextString(this, AppConstants.ratingsandReviewText));
        }
        if (feature.size() != 0) {
            //mTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
            adapter.addFragment(new KeyfeaturesFragment(), AppConstants.getTextString(this, AppConstants.keyFeaturesText));
        }
        viewPager.setAdapter(adapter);
        if (mTabLayout.getTabCount() <= 3) {
            mTabLayout.setTabMode(TabLayout.MODE_FIXED);
        } else {
            mTabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        }
    }

    /**
     * The Tab selected listener.
     */
    TabLayout.OnTabSelectedListener tabSelectedListener = new TabLayout.OnTabSelectedListener() {
        /**
         *  This method is used to get selected tab information.
         * @param tab This is the parameter to onTabSelected method.
         */
        @Override
        public void onTabSelected(TabLayout.Tab tab) {
            mViewPager.setCurrentItem(tab.getPosition());
        }

        /**
         *  This method is used to get unselected tab information.
         * @param tab This is the parameter to onTabUnselected method.
         */
        @Override
        public void onTabUnselected(TabLayout.Tab tab) {

        }

        /**
         *  This method is used to get reselected tab information.
         * @param tab This is the parameter to onTabReselected method.
         */
        @Override
        public void onTabReselected(TabLayout.Tab tab) {

        }
    };

    /**
     * This method is used to For getting ProductId
     *
     * @return String returns string value of product id.
     */
    public String getProductid() {
        return productId;
    }

    /**
     * This method is used to Update Cart Count & Product Name
     *
     * @param productName This is the param to updateCart method.
     */
    public void updateCart(String productName) {
        cartCount();
        hideProgDialog();
        snackBar(AppConstants.getTextString(this, AppConstants.addedCartText));
    }

    /**
     * This method is used to Update Cart Failure response from API
     *
     * @param response This is the param to updateCartFailure method.
     */
    public void updateCartFailure(String response) {
        hideProgDialog();
        snackBar(response);
    }

    /**
     * This method is used to Add Wishlist Success Response
     *
     * @param result This is the param to wishlistSuccess method.
     */
    public void wishlistSuccess(String result) {
        ProductListActivity.sIsWishList = true;
        mProductDetailController.productDetail(productId);
        /*finish();
        startActivity(getIntent());*/
        snackBar(result);
    }

    /**
     * This method is used to Add Wishlist Failure Response
     *
     * @param errormsg This is the param to wishlistFailure method.
     */
    public void wishlistFailure(String errormsg) {
        hideProgDialog();
        ProductListActivity.sIsWishList = false;
        snackBar(errormsg);
    }

    /**
     * This method is used to Add Wishlist Error Response
     *
     * @param string This is the param to failure method.
     */
    public void failure(String string) {
        hideProgDialog();
        snackBar(string);
    }

    /**
     * Product Add to cart
     */
    private View.OnClickListener addCartClick = new View.OnClickListener() {
        /**
         * This method is used to Bottom add to cart button listener
         * @param v This is the parameter to onClick method.
         */
        @Override
        public void onClick(View v) {
            showProgDialiog();
            mKeyValues = productDetail.getAttributesList();
            if (mKeyValues.size() != 0) {
                mProductListController.addToCart(productId, MagentoOverviewFragment.sQuantityCount + "", MagentoOverviewFragment.sProductAttributeId, "");
            } else {
                mProductListController.addToCart(productId, MagentoOverviewFragment.sQuantityCount + "", "0", "");
            }
        }
    };
}
