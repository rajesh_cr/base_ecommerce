package com.prestashopemc.view;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.prestashopemc.R;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.MyApplication;
import com.zopim.android.sdk.api.Chat;
import com.zopim.android.sdk.api.ZopimChat;
import com.zopim.android.sdk.chatlog.ZopimChatLogFragment;
import com.zopim.android.sdk.embeddable.ChatActions;
import com.zopim.android.sdk.prechat.ChatListener;
import com.zopim.android.sdk.prechat.PreChatForm;
import com.zopim.android.sdk.prechat.ZopimChatFragment;
import com.zopim.android.sdk.widget.ChatWidgetService;

/**
 * <h1>Sample Chat Activity!</h1>
 * The Sample Chat Activity implements chat details.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class SampleChatActivity extends BaseActivity implements ChatListener
{

    /**
     * The constant REQUEST_CODE.
     */
//public static int REQUEST_CODE = 5469;
    public final static int REQUEST_CODE = 65635;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sample_chat);
        MyApplication.getDefaultTracker("Chat Screen");
        // use toolbar as action bar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(Color.parseColor(CustomBackground.mThemeColor));
        checkDrawOverlayPermission();
        // orientation change
        if (savedInstanceState != null) {
            return;
        }

        /**
         * If starting activity while the chat widget is actively presented the activity will resume the current chat
         */
        boolean widgetWasActive = stopService(new Intent(this, ChatWidgetService.class));
        if (widgetWasActive) {
            resumeChat();
            return;
        }

        /**
         * We've received an intent request to resume the existing chat.
         * Resume the chat via {@link com.zopim.android.sdk.api.ZopimChat#resume(android.support.v4.app.FragmentActivity)} and
         * start the {@link ZopimChatLogFragment}
         */
        if (getIntent() != null) {
            String action = getIntent().getAction();
            if (ChatActions.ACTION_RESUME_CHAT.equals(action)) {
                resumeChat();
                return;
            }
        }

        /**
         * Attempt to resume chat. If there is an active chat it will be resumed.
         */
        Chat chat = ZopimChat.resume(this);
        if (!chat.hasEnded()) {
            resumeChat();
            return;
        }

        /**
         * Start a new chat
         */
        {
            // set pre chat fields as mandatory
            PreChatForm preChatForm = new PreChatForm.Builder()
                    .name(PreChatForm.Field.REQUIRED_EDITABLE)
                    .email(PreChatForm.Field.REQUIRED_EDITABLE)
                    .phoneNumber(PreChatForm.Field.REQUIRED_EDITABLE)
                    .department(PreChatForm.Field.REQUIRED_EDITABLE)
                    .message(PreChatForm.Field.REQUIRED_EDITABLE)
                    .build();
            // build chat config
            ZopimChat.SessionConfig config = new ZopimChat.SessionConfig()
                    .preChatForm(preChatForm);
            // prepare chat fragment
            ZopimChatFragment fragment = ZopimChatFragment.newInstance(config);
            // show fragment
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.chat_fragment_container, fragment, ZopimChatFragment.class.getName());
            transaction.commit();
        }
    }

    /**
     * This method is used to Resumes the chat and loads the {@link ZopimChatLogFragment}
     */
    private void resumeChat() {

        FragmentManager manager = getSupportFragmentManager();
        // find the retained fragment
        if (manager.findFragmentByTag(ZopimChatLogFragment.class.getName()) == null) {
            ZopimChatLogFragment chatLogFragment = new ZopimChatLogFragment();

            FragmentTransaction transaction = manager.beginTransaction();
            transaction.add(com.zopim.android.sdk.R.id.chat_fragment_container, chatLogFragment, ZopimChatLogFragment.class.getName());
            transaction.commit();
        }
    }

    /**
     * This method is used to chat loaded.
     * @param chat This is the parameter to onChatLoaded method.
     */
    @Override
    public void onChatLoaded(Chat chat) {
        // TODO
    }

    /**
     * This method is used to chat initialized
     */
    @Override
    public void onChatInitialized() {
        // TODO
    }

    /**
     * This method is used to chat ended
     */
    @Override
    public void onChatEnded() {
        // TODO
    }

    /**
     * This method is used to check draw overlay permission
     */
    public void checkDrawOverlayPermission() {
        /** check if we already  have permission to draw over other apps */
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Settings.canDrawOverlays(this)) {
                /** if not construct intent to request permission */
                Intent intent = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                        Uri.parse("package:" + this.getApplicationContext().getPackageName()));
                /** request permission via start activity for result */
                startActivityForResult(intent, REQUEST_CODE);
            }
        }
    }

    /**
     * This method is used to onActivityResult
     * @param requestCode This is the first parameter to onActivityResult method.
     * @param resultCode This is the second parameter to onActivityResult method.
     * @param data This is the third parameter to onActivityResult method.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode,  Intent data) {

        if (requestCode == REQUEST_CODE) {
            // ** if so check once again if we have permission */
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (Settings.canDrawOverlays(this)) {
                    // continue here - permission was granted
                    //goYourActivity();
                    /*Log.e("Zopim chat in Marshmallow ", "Permission was granted");*/
                }
            }
        }
    }
}
