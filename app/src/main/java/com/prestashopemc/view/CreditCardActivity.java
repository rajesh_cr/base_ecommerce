package com.prestashopemc.view;

import android.app.Activity;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.controller.CheckoutController;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.WebViewController;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>Credit Card Activity!</h1>
 * The Credit Card Activity implements webview pages
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class CreditCardActivity extends BaseActivity {

    private WebView webView;
    private Bundle bundle;
    private String paymentCode;
    private CheckoutController checkoutController;
    private String apiUrl;
    /**
     * The constant sCreditActivity.
     */
    public static Activity sCreditActivity;
    private TextView mCodThankText, mCodOrderReceivedText, mCodOrderIdLabel, mCodOrderId, mCodOrderDescription;
    private TextView mOrderFailureText, mOrderFailureCancelledText, mOrderFailureOk;
    private RelativeLayout mOrderCodLayout, mOrderFailureLayout, mMainLayout;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     *
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_card);
        MyApplication.getDefaultTracker("Webview Screen");
        initToolBar();
        initNetworkError();
        sCreditActivity = this;
        logoImage.setVisibility(View.GONE);
        searchIcon.setVisibility(View.GONE);
        cartIcon.setVisibility(View.GONE);
        cartText.setVisibility(View.GONE);
        title.setVisibility(View.VISIBLE);
        title.setAllCaps(true);
        title.setText(AppConstants.getTextString(this, AppConstants.paymentPageText));
        mMainLayout = (RelativeLayout) findViewById(R.id.main_layout);
        showProgDialiog();
        initializedLayout();
    }

    /**
     * This method is used to Initialized Layout
     */
    private void initializedLayout() {

        boolean isInternet = isInternetOn(this);
        if (!isInternet) {
            mMainLayout.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initializedLayout();
                }
            });
            hideProgDialog();

        } else {
            mMainLayout.setVisibility(View.VISIBLE);
            networkLayout.setVisibility(View.GONE);
            webView = (WebView) findViewById(R.id.credit_card_webview);
            /**
             * For RelativeLayout*/
            mOrderCodLayout = (RelativeLayout) findViewById(R.id.cod_layout);
            mOrderFailureLayout = (RelativeLayout) findViewById(R.id.order_failure_layout);
            /**
             * For COD Layout Texts*/
            mCodThankText = (TextView) findViewById(R.id.order_thank);
            mCodOrderReceivedText = (TextView) findViewById(R.id.order_has_received);
            mCodOrderIdLabel = (TextView) findViewById(R.id.order_id_label);
            mCodOrderId = (TextView) findViewById(R.id.order_id);
            mCodOrderDescription = (TextView) findViewById(R.id.order_description);
            /**
             * For Order Failure Layout Texts*/
            mOrderFailureText = (TextView) findViewById(R.id.wrong_text);
            mOrderFailureCancelledText = (TextView) findViewById(R.id.order_cancelled_label);
            mOrderFailureOk = (TextView) findViewById(R.id.order_cancel_ok);
            mOrderFailureOk.setTextColor(Color.parseColor(CustomBackground.mThemeColor));
            CustomBackground.setBackgroundRedWhiteCombination(mOrderFailureOk);
            mOrderFailureOk.setOnClickListener(this);
            dynamicTextValue();
            setCustomFont();
            bundle = getIntent().getExtras();
            if (bundle != null) {
                paymentCode = bundle.getString("paymentMethodCode");
            }
            checkoutController = new CheckoutController(CreditCardActivity.this);
            loadWebView();
        }
    }

    /**
     * This method is used to Dynamic Text Value Settings
     */
    private void dynamicTextValue() {
        mCodOrderReceivedText.setText(AppConstants.getTextString(this, AppConstants.orderReceivedStatusText));
        mCodThankText.setText(AppConstants.getTextString(this, AppConstants.thanksText));
        mCodOrderIdLabel.setText(AppConstants.getTextString(this, AppConstants.orderIdText) + " -");
        mCodOrderDescription.setText(AppConstants.getTextString(this, AppConstants.orderConfirmationText));
        mOrderFailureText.setText(AppConstants.getTextString(this, AppConstants.whoopsText));
        mOrderFailureCancelledText.setText(AppConstants.getTextString(this, AppConstants.orderCancelText));
        mOrderFailureOk.setText(AppConstants.getTextString(this, AppConstants.okText));
    }

    /**
     * This method is used to Set Custom Font
     */
    private void setCustomFont() {
        /**
         * For COD Text*/
        mCodThankText.setTypeface(robotoMedium);
        mCodOrderReceivedText.setTypeface(robotoLight);
        mCodOrderIdLabel.setTypeface(robotoLight);
        mCodOrderId.setTypeface(robotoBold);
        mCodOrderDescription.setTypeface(robotoLight);
        /**
         * For Failure Layout Text*/
        mOrderFailureText.setTypeface(robotoRegular);
        mOrderFailureCancelledText.setTypeface(robotoLight);
        mOrderFailureOk.setTypeface(robotoMedium);
    }

    /**
     * This method is used to Load WebView Page
     */
    public void loadWebView() {
        apiUrl = checkoutController.orderTelr(paymentCode);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebViewClient(new WebViewController(CreditCardActivity.this));
        webView.loadUrl(apiUrl);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setSupportMultipleWindows(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setUseWideViewPort(true);
        if (Build.VERSION.SDK_INT >= 21) {
            webView.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
        }

    }

    /**
     * This method is used to Payment Success
     *
     * @param orderId This is the parameter to webSuccessStatus method.
     */
    public void webSuccessStatus(String orderId) {
        cartIdDelete();
        deleteGuestDetails();
        deleteValue();
        hideProgDialog();
        webView.setVisibility(View.GONE);
        mOrderCodLayout.setVisibility(View.VISIBLE);
        mCodOrderId.setText(" #" + orderId);
    }

    /**
     * This method is used to Payment Failure
     */
    public void webFailureStatus() {
        hideProgDialog();
        webView.setVisibility(View.GONE);
        mOrderFailureLayout.setVisibility(View.VISIBLE);
    }

    /**
     * This method is used to on back pressed
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        CartActivity.sCartActivity.finish();
    }

    /**
     * This method is used to Clear Cart and Address details
     */
    public void clearData() {
        hideProgDialog();
        deleteGuestDetails();
        deleteValue();
    }

    /**
     * This method is used to on click function.
     *
     * @param v This is the parameter to onClick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * Failure page Ok Click Listener*/
            case R.id.order_cancel_ok:
                clearData();
                finish();
                break;
        }
    }
}
