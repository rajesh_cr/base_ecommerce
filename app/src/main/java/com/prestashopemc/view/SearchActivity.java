package com.prestashopemc.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.prestashopemc.R;
import com.prestashopemc.adapter.FilterExpandableAdapter;
import com.prestashopemc.adapter.ProductListAdapter;
import com.prestashopemc.adapter.ProductListMagentoAdapter;
import com.prestashopemc.adapter.SearchListAdapter;
import com.prestashopemc.controller.SearchController;
import com.prestashopemc.model.CartDeleteInfo;
import com.prestashopemc.model.CartDeleteModel;
import com.prestashopemc.model.CartDeleteResult;
import com.prestashopemc.model.CartQuantityModel;
import com.prestashopemc.model.CartQuantityResult;
import com.prestashopemc.model.CategoryProduct;
import com.prestashopemc.model.MagentoCategoryProduct;
import com.prestashopemc.model.MagentoProduct;
import com.prestashopemc.model.MagentoProductMain;
import com.prestashopemc.model.Product;
import com.prestashopemc.model.ProductMain;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.MyApplication;

import java.util.ArrayList;
import java.util.List;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>Search Activity!</h1>
 * The Search Activity implements search page details.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author Rajesh
 * @version 1.2
 * @since 25 -10-2016
 */
public class SearchActivity extends BaseActivity {

    private SearchController mProductController;
    private Bundle mBundle;
    private String mProductListLayout, mSortType, mSortBy;
    private RecyclerView mProductListView;
    private int mPreviousTotal = 0;
    private boolean mLoading = true;
    private int mVisibleThreshold = 5;
    private int mFirstVisibleItem, mVisibleItemCount, mTotalItemCount;
    private int mCurrentPage = 1;
    private int mSearchCurrentPage = 1;
    private int mPrdCount = 0;
    private int mTotalPageNumber = 0;
    private LinearLayoutManager mLayoutManager;
    private GridLayoutManager mGridLayoutManger;
    private ProgressBar mProgressBar, mBottomProgressBar;
    private SearchListAdapter mSearchListAdapter;
    private ProductListMagentoAdapter mSearchMagentoListAdapter;
    private List<CategoryProduct> mSearchProductList = new ArrayList<>();
    private List<MagentoCategoryProduct> mSearchMagentoProductList = new ArrayList<>();
    private Product mProduct;
    private MagentoProduct mMagentoProduct;
    private TextView mProductListCount, mLowtoHigh, mHightoLow, mAscendtoDescend, mDescendtoAscend, mSortby, mFilterText, mSortText;
    private RelativeLayout mSortLayout, mFilterLayout, mItemLayout, mSearchMainLayout;
    private boolean mIsGrid = false;
    private LinearLayout mSortMaster, mSortMain, mSortFilterLayout;
    private ImageView mItemTypeImg;
    public static boolean sIsFilter = false;
    private List<CategoryProduct> mFilterProductList = new ArrayList<>();
    private List<CategoryProduct> mSortProductList = new ArrayList<CategoryProduct>();

    private List<MagentoCategoryProduct> mSortMagnetoProductList = new ArrayList<MagentoCategoryProduct>();
    private List<MagentoCategoryProduct> mFilterMagentoProductList = new ArrayList<MagentoCategoryProduct>();

    private boolean mSortCheck, mViewCheck;
    private boolean mFilterCheck;
    public static String sSearchText, sCategoryId, sCategoryName, sFilterName, sCateId;
    public CartDeleteResult cartDeleteResult;
    public CartDeleteInfo cartDeleteInfo;
    public CartQuantityResult cartQuantityResult;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     *
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        MyApplication.getDefaultTracker("Search Screen");
        initToolBar();
        initNetworkError();
        categoryTitle.setVisibility(View.GONE);
        productCount.setVisibility(View.GONE);
        cartIcon.setVisibility(View.VISIBLE);
        searchIcon.setVisibility(View.VISIBLE);
        title.setVisibility(View.VISIBLE);
        cartCount();
        mBundle = getIntent().getExtras();
        if (mBundle != null) {
            sSearchText = mBundle.getString("search_text");
            sCateId = mBundle.getString("id_category");
        }
        mSearchMainLayout = (RelativeLayout) findViewById(R.id.search_main_layout);
        initalizeLayout();
    }

    /**
     * This method is used to resume activity.
     */
    @Override
    protected void onResume() {
        super.onResume();
        cartCount();
        if (sIsFilter) {
            sIsFilter = false;
            showProgDialiog();
            if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                mProductController.searchFilterSubmit(FilterActivity.sCatId, FilterActivity.sFilterObject, mSearchCurrentPage + "", sSearchText);
            }else {
                mProductController.produtNavigationFilter(FilterActivity.sCatId, mCurrentPage + "",
                        FilterActivity.sNavigationFilterFeatureObject, FilterActivity.sNavigationFilterAttributeObject,
                        FilterActivity.sCondition, FilterActivity.sManFactures, FilterActivity.sPriceObject,
                        FilterActivity.sWeightObject, FilterActivity.sAvailability);
            }
        } /*else {
            mProductController.searchProducts(mCurrentPage + "", searchText);
        }*/

    }

    /**
     * This method is used to Initialized Layout
     */
    private void initalizeLayout() {


        boolean isInternet = isInternetOn(this);
        if (!isInternet) {
            mSearchMainLayout.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initalizeLayout();
                }
            });
            hideProgDialog();

        } else {
            showProgDialiog();
            mSearchMainLayout.setVisibility(View.VISIBLE);
            networkLayout.setVisibility(View.GONE);
            //hideProgDialog();
            mProductController = new SearchController(SearchActivity.this);
            mProductListView = (RecyclerView) findViewById(R.id.product_list_view);
            mLayoutManager = new LinearLayoutManager(this);
            mGridLayoutManger = new GridLayoutManager(this, 2);
            mProductListCount = (TextView) findViewById(R.id.search_product_count);
            mLowtoHigh = (TextView) findViewById(R.id.search_low_high);
            mHightoLow = (TextView) findViewById(R.id.search_high_low);
            mAscendtoDescend = (TextView) findViewById(R.id.search_ascending_descending);
            mDescendtoAscend = (TextView) findViewById(R.id.search_descending_ascending);
            mSortby = (TextView) findViewById(R.id.search_sort_by);
            mFilterText = (TextView) findViewById(R.id.filter_text);
            mSortText = (TextView) findViewById(R.id.sort_text);
            mItemTypeImg = (ImageView) findViewById(R.id.item_type_image);
            mSortMaster = (LinearLayout) findViewById(R.id.sort_master_layout);
            mSortMain = (LinearLayout) findViewById(R.id.sort_main_layout);
            mSortFilterLayout = (LinearLayout) findViewById(R.id.sort_fliter_layout);
            mSortLayout = (RelativeLayout) findViewById(R.id.search_sort_layout);
            mFilterLayout = (RelativeLayout) findViewById(R.id.search_filter_layout);
            mItemLayout = (RelativeLayout) findViewById(R.id.search_item_type_image_layout);
            mProductListView.setLayoutManager(mLayoutManager);
            mProductListView.setHasFixedSize(true);
            mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
            mBottomProgressBar = (ProgressBar) findViewById(R.id.bottom_progress_bar);
            mProductListView.addOnScrollListener(mScrollListener);
            title.setText(sSearchText);
            title.setAllCaps(true);
            logoImage.setVisibility(View.GONE);
            cartCount();
            dynamicTextValue();
            setCustomFont();
            mProductListLayout = SharedPreference.getInstance().getValue("product_list_layout");
            if (sCateId != null) {
                mProductController.advancedSearchProducts(mCurrentPage + "", sSearchText, sCateId);
            } else {
                mProductController.searchProducts(mCurrentPage + "", sSearchText);
            }
            mSortLayout.setOnClickListener(this);
            mFilterLayout.setOnClickListener(this);
            mItemLayout.setOnClickListener(this);
            if (mIsGrid) {
                mProductListView.setLayoutManager(mGridLayoutManger);
            } else {
                mProductListView.setLayoutManager(mLayoutManager);
            }
        }
    }

    /**
     * This method is used to Dynamic Text Value Settings
     */
    private void dynamicTextValue() {
        mSortText.setText(AppConstants.getTextString(this, AppConstants.listSort));
        mFilterText.setText(AppConstants.getTextString(this, AppConstants.filterText));
        mSortby.setText(AppConstants.getTextString(this, AppConstants.sortText));
        mHightoLow.setText(AppConstants.getTextString(this, AppConstants.sortHighToLowText));
        mLowtoHigh.setText(AppConstants.getTextString(this, AppConstants.sortLowToHighText));
        mAscendtoDescend.setText(AppConstants.getTextString(this, AppConstants.sortAscendingtoDescendingText));
        mDescendtoAscend.setText(AppConstants.getTextString(this, AppConstants.sortDescendingtoAscendingText));
    }

    /**
     * This method is used to Set Custom Font
     */
    private void setCustomFont() {
        mSortText.setTypeface(robotoMedium);
        mFilterText.setTypeface(robotoMedium);
        mSortby.setTypeface(robotoMedium);
        mHightoLow.setTypeface(robotoRegular);
        mLowtoHigh.setTypeface(robotoRegular);
        mAscendtoDescend.setTypeface(robotoRegular);
        mDescendtoAscend.setTypeface(robotoRegular);
        title.setTypeface(robotoMedium);
        mProductListCount.setTypeface(robotoRegular);
    }

    /**
     * This method is used to Search Success Response
     *
     * @param mProductMain This is the parameter to searchSuccess method.
     */
    public void searchSuccess(ProductMain mProductMain) {
        hideProgDialog();
        mViewCheck = true;
        mSortCheck = false;
        mFilterCheck = false;
        mProgressBar.setVisibility(View.GONE);
        mBottomProgressBar.setVisibility(View.GONE);
        mProduct = mProductMain.getmProduct();
        if (mProduct != null) {
            mSearchProductList.addAll(mProduct.getCategoryProducts());
            mProductListCount.setText(AppConstants.getTextString(this, AppConstants.showingText) + " " + mProduct.getProductCount() + " "
                    + AppConstants.getTextString(this, AppConstants.resultText) + " " + sSearchText);
            mPrdCount = mProduct.getProductCount();
            if (mCurrentPage == 1) {
                totalPageNo();
                mSearchListAdapter = new SearchListAdapter(this, mSearchProductList, mIsGrid, Integer.parseInt(mProductListLayout));
                mProductListView.setAdapter(mSearchListAdapter);
            } else {
                mSearchListAdapter.notifyDataSetChanged();
            }
        }
    }


    /**
     * This method is used to Search Success Response
     *
     * @param mProductMain This is the parameter to searchSuccess method.
     */
    public void searchMagentoSuccess(MagentoProductMain mProductMain) {
        hideProgDialog();
        mViewCheck = true;
        mSortCheck = false;
        mFilterCheck = false;
        mProgressBar.setVisibility(View.GONE);
        mBottomProgressBar.setVisibility(View.GONE);
        mMagentoProduct = mProductMain.getMagentoProduct();
        if (mMagentoProduct != null) {
            mSearchMagentoProductList.addAll(mMagentoProduct.getCategoryProducts());
            mProductListCount.setText(AppConstants.getTextString(this, AppConstants.showingText) + " " + mMagentoProduct.getProductCount() + " "
                    + AppConstants.getTextString(this, AppConstants.resultText) + " " + sSearchText);
            mPrdCount = mMagentoProduct.getProductCount();
            if (mCurrentPage == 1) {
                totalPageNo();
                mSearchMagentoListAdapter = new ProductListMagentoAdapter(this, mSearchMagentoProductList, mIsGrid, Integer.parseInt(mProductListLayout));
                mProductListView.setAdapter(mSearchMagentoListAdapter);
            } else {
                mSearchMagentoListAdapter.notifyDataSetChanged();
            }
        }
    }

    /**
     * This method is used to Search Failure Response
     *
     * @param s This is the parameter to searchFailure method.
     */
    public void searchFailure(String s) {
        hideProgDialog();
        if (s.equalsIgnoreCase(getString(R.string.noProductsFoundText))
                || s.equalsIgnoreCase(getString(R.string.noResultsFoundText))) {
            mSortFilterLayout.setVisibility(View.GONE);
        }
        snackBar(s);
    }

    /**
     * This method is used to Find total page numbers
     */
    private void totalPageNo() {
        int extraPage = 0;
        if ((mPrdCount % AppConstants.productsPerPage) == 0) {
            extraPage = 0;
        } else {
            extraPage = 1;
        }
        mTotalPageNumber = (mPrdCount / AppConstants.productsPerPage) + extraPage;
    }

    /**
     * This method is used to Recyclerview Scroll Listener
     */
    private RecyclerView.OnScrollListener mScrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            if (!mIsGrid) {
                mVisibleItemCount = mProductListView.getChildCount();
                mTotalItemCount = mLayoutManager.getItemCount();
                mFirstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
            } else {
                mVisibleItemCount = mProductListView.getChildCount();
                mTotalItemCount = mGridLayoutManger.getItemCount();
                mFirstVisibleItem = mGridLayoutManger.findFirstVisibleItemPosition();
            }

            if (mLoading) {
                if (mTotalItemCount > mPreviousTotal) {
                    mLoading = false;
                    mPreviousTotal = mTotalItemCount;
                }
            }
            if (!mLoading && (mTotalItemCount - mVisibleItemCount)
                    <= (mFirstVisibleItem + mVisibleThreshold)) {
                // End has been reached
                mBottomProgressBar.setVisibility(View.VISIBLE);
                mSearchCurrentPage = mCurrentPage;
                mCurrentPage++;

                if (mTotalPageNumber >= mCurrentPage) {

                    if (mViewCheck == true) {
                        mProductController.searchProducts(mCurrentPage + "", sSearchText);
                    }
                    if (mSortCheck == true) {
                        mProductController.searchSort(mCurrentPage + "", mSortType, mSortBy, sSearchText);
                    }
                    if (mFilterCheck == true) {
                        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                            mProductController.searchFilterSubmit(FilterActivity.sCatId, FilterActivity.sFilterObject, mCurrentPage + "", sSearchText);
                        }else {
                            mProductController.produtNavigationFilter(FilterActivity.sCatId, mCurrentPage + "",
                                    FilterActivity.sNavigationFilterFeatureObject, FilterActivity.sNavigationFilterAttributeObject,
                                    FilterActivity.sCondition, FilterActivity.sManFactures, FilterActivity.sPriceObject,
                                    FilterActivity.sWeightObject, FilterActivity.sAvailability);
                        }
                    }
                } else {
                    //mCurrentPage = 1;
                    mBottomProgressBar.setVisibility(View.GONE);
                }

                mLoading = true;
            }
        }
    };

    /**
     * This method is used to Item view type Listener
     */
    private void GridView() {

        if (mSearchProductList.size() > 0 || mSearchMagentoProductList.size() > 0) {
            if (mIsGrid) {
                mIsGrid = false;
                mItemTypeImg.setImageResource(R.drawable.ic_grid);
                mProductListView.setLayoutManager(mLayoutManager);
            } else {
                mIsGrid = true;
                mItemTypeImg.setImageResource(R.drawable.ic_list);
                mProductListView.setLayoutManager(mGridLayoutManger);
            }
            if (AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
                mSearchMagentoListAdapter = new ProductListMagentoAdapter(this, mSearchMagentoProductList, mIsGrid, Integer.parseInt(mProductListLayout));
                mProductListView.setAdapter(mSearchMagentoListAdapter);
            } else {
                mSearchListAdapter = new SearchListAdapter(this, mSearchProductList, mIsGrid, Integer.parseInt(mProductListLayout));
                mProductListView.setAdapter(mSearchListAdapter);
            }

        }
    }

    /**
     * This method is used to Sort Layout Listener
     */
    private void sortLayout() {
        mCurrentPage = 1;
        mPreviousTotal = 0;
        mSortMaster.setVisibility(View.VISIBLE);
        mSortMaster.startAnimation(AppConstants.setAnimateIn(SearchActivity.this, R.anim.falling));
        mSearchMainLayout.setAlpha((float) 0.7);
        mSortMain.setOnClickListener(this);
        mLowtoHigh.setOnClickListener(this);
        mHightoLow.setOnClickListener(this);
        mAscendtoDescend.setOnClickListener(this);
        mDescendtoAscend.setOnClickListener(this);
    }

    /**
     * This method is used to Sort layout animation for open and close
     */
    private void sortAnimation() {
        showProgDialiog();
        mSearchMainLayout.setAlpha((float) 1);
        mProductController.searchSort(mCurrentPage + "", mSortType, mSortBy, sSearchText);
        mSortMaster.setVisibility(View.GONE);
        mSortMaster.startAnimation(AppConstants.setAnimateOut(SearchActivity.this, R.anim.fadeout));
    }

    /**
     * This method is used to back pressed
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        hideProgDialog();
        sIsFilter = false;
        FilterExpandableAdapter.mSelectedFilter.clear();
    }

    /**
     * This method is used to Search Filter Failure Response
     *
     * @param string This is the parameter to filterFailure method.
     */
    public void filterFailure(String string) {
        hideProgDialog();
        snackBar(string);
    }

    /**
     * This method is used to Search Filter Success Response
     *
     * @param mProductMainObject This is the parameter to filterSuccess method.
     */
    public void filterSuccess(ProductMain mProductMainObject) {
        hideProgDialog();
        mFilterCheck = true;
        mViewCheck = false;
        mSortCheck = false;
        mProduct = mProductMainObject.getmProduct();
        mProgressBar.setVisibility(View.GONE);
        mBottomProgressBar.setVisibility(View.GONE);
        if (mProduct != null) {
            mFilterProductList.addAll(mProduct.getCategoryProducts());
            mProductListCount.setText(AppConstants.getTextString(this, AppConstants.showingText) + " " + mProduct.getProductCount() + " "
                    + AppConstants.getTextString(this, AppConstants.resultText) + " " + sSearchText);
            mPrdCount = mProduct.getProductCount();
            if (mCurrentPage == 1) {
                totalPageNo();
                mFilterProductList.clear();
                mFilterProductList.addAll(mProduct.getCategoryProducts());
                mSearchListAdapter = new SearchListAdapter(this, mFilterProductList, mIsGrid, Integer.parseInt(mProductListLayout));
                mProductListView.setAdapter(mSearchListAdapter);
            } else {
                mSearchListAdapter.notifyDataSetChanged();
            }
        }

    }


    /**
     * This method is used to Search Filter Success Response
     *
     * @param mProductMainObject This is the parameter to filterSuccess method.
     */
    public void filterMagentoSuccess(MagentoProductMain mProductMainObject) {
        hideProgDialog();
        mFilterCheck = true;
        mViewCheck = false;
        mSortCheck = false;
        mMagentoProduct = mProductMainObject.getMagentoProduct();
        mProgressBar.setVisibility(View.GONE);
        mBottomProgressBar.setVisibility(View.GONE);
        if (mMagentoProduct != null) {
            mFilterMagentoProductList.addAll(mMagentoProduct.getCategoryProducts());
            mProductListCount.setText(AppConstants.getTextString(this, AppConstants.showingText) + " " + mMagentoProduct.getProductCount() + " "
                    + AppConstants.getTextString(this, AppConstants.resultText) + " " + sSearchText);
            mPrdCount = mMagentoProduct.getProductCount();
            if (mCurrentPage == 1) {
                totalPageNo();
                mFilterMagentoProductList.clear();
                mFilterMagentoProductList.addAll(mMagentoProduct.getCategoryProducts());
                Log.e("mFilterMagentoProduc",mFilterMagentoProductList.size()+"");
                mSearchMagentoListAdapter = new ProductListMagentoAdapter(this, mFilterMagentoProductList, mIsGrid, Integer.parseInt(mProductListLayout));
                mProductListView.setAdapter(mSearchMagentoListAdapter);
            } else {
                mSearchMagentoListAdapter.notifyDataSetChanged();
            }
        }

    }

    /**
     * This method is used to  Search Sort Success Response
     *
     * @param product   This is the first parameter to sortSuccess method
     * @param mSorttype This is the second parameter to sortSuccess method.
     * @param mSortby   This is the third parameter to sortSuccess method.
     */
    public void sortSuccess(Product product, String mSorttype, String mSortby) {
        hideProgDialog();
        mSortType = mSorttype;
        mSortBy = mSortby;
        mSortCheck = true;
        mViewCheck = false;
        mFilterCheck = false;
        mProgressBar.setVisibility(View.GONE);
        mBottomProgressBar.setVisibility(View.GONE);
        mPrdCount = product.getProductCount();
        if (product != null) {
            mSortProductList.addAll(product.getCategoryProducts());
            mProductListCount.setText(AppConstants.getTextString(this, AppConstants.showingText) + " " + product.getProductCount() + " "
                    + AppConstants.getTextString(this, AppConstants.resultText) + " " + sSearchText);
            mPrdCount = product.getProductCount();
            if (mCurrentPage == 1) {
                totalPageNo();
                mSortProductList.clear();
                mSortProductList.addAll(product.getCategoryProducts());
                mSearchListAdapter = new SearchListAdapter(this, mSortProductList, mIsGrid, Integer.parseInt(mProductListLayout));
                mProductListView.setAdapter(mSearchListAdapter);
            } else {
                mSearchListAdapter.notifyDataSetChanged();
            }
        }
    }


    /**
     * This method is used to  Search Sort Success Response
     *
     * @param product   This is the first parameter to sortSuccess method
     * @param mSorttype This is the second parameter to sortSuccess method.
     * @param mSortby   This is the third parameter to sortSuccess method.
     */
    public void sortMagentoSuccess(MagentoProduct product, String mSorttype, String mSortby) {
        hideProgDialog();
        mSortType = mSorttype;
        mSortBy = mSortby;
        mSortCheck = true;
        mViewCheck = false;
        mFilterCheck = false;
        mProgressBar.setVisibility(View.GONE);
        mBottomProgressBar.setVisibility(View.GONE);
        mPrdCount = product.getProductCount();
        if (product != null) {
            mSortMagnetoProductList.addAll(product.getCategoryProducts());
            mProductListCount.setText(AppConstants.getTextString(this, AppConstants.showingText) + " " + product.getProductCount() + " "
                    + AppConstants.getTextString(this, AppConstants.resultText) + " " + sSearchText);
            mPrdCount = product.getProductCount();

            if (mCurrentPage == 1) {
                totalPageNo();
                mSortMagnetoProductList.clear();
                mSortMagnetoProductList.addAll(product.getCategoryProducts());
                mSearchMagentoListAdapter = new ProductListMagentoAdapter(this, mSortMagnetoProductList, mIsGrid, Integer.parseInt(mProductListLayout));
                mProductListView.setAdapter(mSearchMagentoListAdapter);
            } else {
                mSearchMagentoListAdapter.notifyDataSetChanged();
            }
        }
    }


    /**
     * This method is used to Search Sort Failure Response
     *
     * @param errormsg This is the parameter to sortFailure method.
     */
    public void sortFailure(String errormsg) {
        hideProgDialog();
        snackBar(errormsg);
    }

    /**
     * This method is used to Add Wishlist Success Response
     *
     * @param result This is the parameter to wishlistSuccess method.
     */
    public void wishlistSuccess(String result) {
        hideProgDialog();
        Toast.makeText(SearchActivity.this, result, Toast.LENGTH_SHORT).show();
        refreshActivity();
        //snackBar(result);
    }

    /**
     * This method is used to Add Wishlist Failure Response
     *
     * @param errormsg This is the parameter to wishlistFailure method.
     */
    public void wishlistFailure(String errormsg) {
        hideProgDialog();
        snackBar(errormsg);
    }

    /**
     * This method is used to Add Wishlist Error Response
     *
     * @param string This is the parameter to failure method.
     */
    public void failure(String string) {
        hideProgDialog();
        snackBar(string);
    }

    /**
     * This method is used to Add to cart Success Response
     *
     * @param mProductName This is the parameter to updateCart method.
     */
    public void updateCart(String mProductName) {
        cartCount();
        hideProgDialog();
        snackBar(AppConstants.getTextString(this, AppConstants.addedCartText));
    }

    /**
     * This method is used to Add to cart Failure Response
     *
     * @param errorMsg This is the parameter to updateCartFailure method.
     */
    public void updateCartFailure(String errorMsg) {
        hideProgDialog();
        mProgressBar.setVisibility(View.GONE);
        snackBar(errorMsg);
    }

    /**
     * This method is used to Refresh Activity
     */
    public void refreshActivity() {
        finish();
        startActivity(getIntent());
    }

    /**
     * This method is used to Cart count delete method
     *
     * @param cartDeleteModel This is the parameter to updateDeleteView method.
     */
    public void updateDeleteView(CartDeleteModel cartDeleteModel) {
        cartDeleteResult = cartDeleteModel.getResult();
        cartDeleteInfo = cartDeleteResult.getInfo();
        SharedPreference mPref = SharedPreference.getInstance();
        mPref.save("cart_count", cartDeleteResult.getCartCount() + "");
        cartCount();
        if (cartDeleteResult.getCartCount() == 0) {
            snackBar(AppConstants.getTextString(this, AppConstants.noProductInCartText));
            hideProgDialog();
        } else {
            hideProgDialog();
        }
    }

    /**
     * This method is used to Cart failure method
     *
     * @param errorMsg This is the parameter to updateFailure method.
     */
    public void updateFailure(String errorMsg) {
        hideProgDialog();
        if (errorMsg.equalsIgnoreCase(getString(R.string.noProductsFoundText))
                || errorMsg.equalsIgnoreCase(getString(R.string.noResultsFoundText))) {
            mSortFilterLayout.setVisibility(View.GONE);
        }
        productCount.setText("0 " + AppConstants.getTextString(this, AppConstants.productsText));
        mProgressBar.setVisibility(View.GONE);
        snackBar(errorMsg);
    }

    /**
     * This method is used to Cart quantity decrease method
     *
     * @param mCartQuantityModel This is the first parameter to updateQuantityView method.
     * @param quantity           This is the second parameter to updateQuantityView method.
     * @param quantityPosition   This is the third parameter to updateQuantityView method.
     */
    public void updateQuantityView(CartQuantityModel mCartQuantityModel, String quantity, String quantityPosition) {
        cartQuantityResult = mCartQuantityModel.getResult();
        SharedPreference mPref = SharedPreference.getInstance();
        mPref.save("cart_id", cartQuantityResult.getCartId() + "");
        mPref.save("cart_count", cartQuantityResult.getCartCount() + "");
        cartCount();
        hideProgDialog();
    }

    /**
     * This method is used to onClick.
     *
     * @param v This is the parameter to onClick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * Sort Click Listener*/
            case R.id.search_sort_layout:
                sortLayout();
                break;

            /**
             * Filter Click Listener*/
            case R.id.search_filter_layout:
                mCurrentPage = 1;
                if (sCategoryId == null || sCategoryId.isEmpty()) {
                    Intent filterIntent = new Intent(SearchActivity.this, SearchFilterActivity.class);
                    filterIntent.putExtra("currentPage", mSearchCurrentPage);
                    startActivity(filterIntent);
                } else {
                    Intent filterIntent = new Intent(SearchActivity.this, FilterActivity.class);
                    filterIntent.putExtra("category_id", sCategoryId);
                    filterIntent.putExtra("category_name", sCategoryName);
                    filterIntent.putExtra("filter", sFilterName);
                    startActivity(filterIntent);
                }
                break;

            /**
             * Item Change Listener*/
            case R.id.search_item_type_image_layout:
                GridView();
                break;

            /**
             * Sort layout Click Listener*/
            case R.id.sort_main_layout:
                mSearchMainLayout.setAlpha((float) 1);
                mSortMaster.setVisibility(View.GONE);
                mSortMaster.startAnimation(AppConstants.setAnimateOut(SearchActivity.this, R.anim.fadeout));
                break;

            /**
             * search low to high Click Listener*/
            case R.id.search_low_high:
                mSortType = "asc";
                mSortBy = "price";
                sortAnimation();
                break;

            /**
             * search high to low Click Listener*/
            case R.id.search_high_low:
                mSortType = "desc";
                mSortBy = "price";
                sortAnimation();
                break;

            /**
             * search ascending to descending Click Listener*/
            case R.id.search_ascending_descending:
                mSortType = "asc";
                mSortBy = "name";
                sortAnimation();
                break;

            /**
             * search descending to ascending Click Listener*/
            case R.id.search_descending_ascending:
                mDescendtoAscend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mSortType = "desc";
                        mSortBy = "name";
                        sortAnimation();
                    }
                });
        }
    }
}
