package com.prestashopemc.view;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.adapter.ViewPagerAdapter;
import com.prestashopemc.fragment.AddressFragment;
import com.prestashopemc.fragment.ConfirmFragment;
import com.prestashopemc.fragment.PaymentFragment;
import com.prestashopemc.model.Address;
import com.prestashopemc.model.AddressMainModel;
import com.prestashopemc.model.MagentoDefaultAddressModel;
import com.prestashopemc.model.MagentoDefaultAddressShipping;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.CustomIcon;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.MyApplication;

/**
 * <h1>Checkout Activity!</h1>
 * The Checkout Activity implements Checkout functionality contains address, payment , confirm order
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class CheckoutActivity extends BaseActivity {

    private ViewPager mViewPager;
    private ImageView mAddressImage, mPaymentImage, mConfirmImage;
    private int mPagerPosition = 0;
    private TextView mAddressPayment, mPaymentConfirm, mAddressText, mPayment, mConfirm, mProceedToPayment;
    /**
     * The Addressmodel.
     */
    public Address addressmodel;
    public String name,address, addressSelect, idAddress, selectedAddress = "";
    private AddressMainModel mAddressMainModel;
    private MagentoDefaultAddressModel mMagentoDefaultAddressModel;
    public MagentoDefaultAddressShipping magentoDefaultAddressShipping;
    private Bundle mBundle;
    public String shippingId,billingId,paymentMethodName, paymentMethodCode,shippingMethod,checkoutType,paymentIdentifier;
    private SharedPreference mPref;
    private boolean mConfirmFragment = false;
    private boolean mPaymentFragment = false;
    /**
     * The constant sCheckOutActivity.
     */
    public static Activity sCheckOutActivity;
    private RelativeLayout mMainLayout;
    private Bitmap mFinalBitmap;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);
        MyApplication.getDefaultTracker("Checkout Screen");
        initToolBar();
        initNetworkError();
        customIcon();
        sCheckOutActivity = this;
        logoImage.setVisibility(View.GONE);
        searchIcon.setVisibility(View.VISIBLE);
        cartIcon.setVisibility(View.VISIBLE);
        cartText.setVisibility(View.VISIBLE);
        title.setVisibility(View.VISIBLE);
        title.setText(AppConstants.getTextString(this, AppConstants.checkoutText));
        title.setAllCaps(true);
        mMainLayout = (RelativeLayout) findViewById(R.id.main_layout);
        cartCount();
        loadAddress();
        initalizeLayout();
    }

    /**
     * This method is used to Load Registered Address
     */
    private void loadAddress() {
        mPref = SharedPreference.getInstance();
        String res = SharedPreference.getInstance().getValue("response");
        if (!res.equals("0")) {
            if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                mMagentoDefaultAddressModel = (MyApplication.getGsonInstance().fromJson(res, MagentoDefaultAddressModel.class));
                magentoDefaultAddressShipping = mMagentoDefaultAddressModel.getResult().getShippingAddress();
            }else {
                mAddressMainModel = (MyApplication.getGsonInstance().fromJson(res, AddressMainModel.class));
                addressmodel = mAddressMainModel.getAddress();
            }
        }
        checkoutType = SharedPreference.getInstance().getValue("checkout");
        mBundle = getIntent().getExtras();
        if (mBundle != null) {
            name = mBundle.getString("name");
            address = mBundle.getString("address");
            addressSelect = mBundle.getString("addressSelect");
            idAddress = mBundle.getString("idAddress");

            if (addressSelect != null) {
                if (addressSelect.equals("shipping")) {
                    mPref.save("shipping_name", name);
                    mPref.save("shipping_address", address);
                    mPref.save("shipping_id", idAddress);
                    selectedAddress = "shipping";
                }
                if (addressSelect.equals("billing")) {
                    mPref.save("billing_name", name);
                    mPref.save("billing_address", address);
                    mPref.save("billing_id", idAddress);
                    selectedAddress = "billing";
                }
            }
        } else {
            if (addressmodel != null && !addressmodel.equals("0")) {
                mPref.save("shipping_id", addressmodel.getIdAddress());
                mPref.save("billing_id", addressmodel.getIdAddress());
            }else if (magentoDefaultAddressShipping != null && !magentoDefaultAddressShipping.equals("0")){
                mPref.save("shipping_id", magentoDefaultAddressShipping.getEntityId());
                mPref.save("billing_id", magentoDefaultAddressShipping.getEntityId());
            }
        }
        shippingId = SharedPreference.getInstance().getValue("shipping_id");
        billingId = SharedPreference.getInstance().getValue("billing_id");
    }

    /**
     * This method is used to Initalized Layout
     */
    private void initalizeLayout() {
        showProgDialiog();
        mMainLayout.setVisibility(View.VISIBLE);
        networkLayout.setVisibility(View.GONE);
        hideProgDialog();
        mViewPager = (ViewPager) findViewById(R.id.viewpager);
        mAddressImage = (ImageView) findViewById(R.id.address_image);
        mPaymentImage = (ImageView) findViewById(R.id.payment_image);
        mConfirmImage = (ImageView) findViewById(R.id.confirm_image);
        mAddressPayment = (TextView) findViewById(R.id.address_payment);
        mPaymentConfirm = (TextView) findViewById(R.id.payment_confirm);
        mAddressText = (TextView) findViewById(R.id.address_text);
        mPayment = (TextView) findViewById(R.id.payment_text);
        mConfirm = (TextView) findViewById(R.id.confirm_text);
        mProceedToPayment = (TextView) findViewById(R.id.proceedTopayment);
        CustomBackground.setBackgroundText(mProceedToPayment);
        mProceedToPayment.setOnClickListener(this);
        dynamicTextValue();
        setCustomFont();
        setupViewPager(mViewPager);
        stepChanger(mPagerPosition);
        mViewPager.setCurrentItem(0);
        mViewPager.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    /**
     * This method is used to Dynamic Text Value Settings
     */
    private void dynamicTextValue() {
        mAddressText.setText(AppConstants.getTextString(this, AppConstants.addressText));
        mPayment.setText(AppConstants.getTextString(this, AppConstants.paymentText));
        mConfirm.setText(AppConstants.getTextString(this, AppConstants.confirmText));
    }

    /**
     * This method is used to Set Custom Font
     */
    private void setCustomFont() {
        mAddressText.setTypeface(robotoLight);
        mPayment.setTypeface(robotoLight);
        mConfirm.setTypeface(robotoLight);
        mProceedToPayment.setTypeface(robotoMedium);
    }

    /**
     * This method is used to Step Changer
     * @param postion This is the parameter to stepChanger method.
     */
    private void stepChanger(int postion) {

        if (postion == 0) {
            mViewPager.setCurrentItem(postion);
            mProceedToPayment.setText(AppConstants.getTextString(this, AppConstants.proceedToPaymentText));
            mAddressImage.setImageBitmap(mFinalBitmap);
            mPaymentImage.setImageResource(R.drawable.step_normal);
            mConfirmImage.setImageResource(R.drawable.step_normal);
            mAddressPayment.setBackgroundColor(getResources().getColor(R.color.checkout_line_color));
            mPaymentConfirm.setBackgroundColor(getResources().getColor(R.color.checkout_line_color));
            mAddressText.setTextColor(Color.BLACK);
            mPayment.setTextColor(getResources().getColor(R.color.checkout_nameNormal_color));
            mConfirm.setTextColor(getResources().getColor(R.color.checkout_nameNormal_color));
        } else if (postion == 1) {
            mPaymentFragment = true;
            setupViewPager(mViewPager);
            mViewPager.setCurrentItem(postion);
            mProceedToPayment.setText(AppConstants.getTextString(this, AppConstants.placeMyOrderText));
            mAddressImage.setImageBitmap(mFinalBitmap);
            mPaymentImage.setImageBitmap(mFinalBitmap);
            mConfirmImage.setImageResource(R.drawable.step_normal);
            mAddressPayment.setBackgroundColor(Color.parseColor(CustomBackground.mThemeColor));
            mPaymentConfirm.setBackgroundColor(getResources().getColor(R.color.checkout_line_color));
            mAddressText.setTextColor(Color.BLACK);
            mPayment.setTextColor(Color.BLACK);
            mConfirm.setTextColor(getResources().getColor(R.color.checkout_nameNormal_color));
        } else if (postion == 2) {
            if (paymentMethodCode != null) {
                mConfirmFragment = true;
                setupViewPager(mViewPager);
                mViewPager.setCurrentItem(postion);
                mProceedToPayment.setText(AppConstants.getTextString(this, AppConstants.confirmMyOrderText));
                mAddressImage.setImageBitmap(mFinalBitmap);
                mPaymentImage.setImageBitmap(mFinalBitmap);
                mConfirmImage.setImageBitmap(mFinalBitmap);
                mAddressPayment.setBackgroundColor(Color.parseColor(CustomBackground.mThemeColor));
                mPaymentConfirm.setBackgroundColor(Color.parseColor(CustomBackground.mThemeColor));
                mAddressText.setTextColor(Color.BLACK);
                mPayment.setTextColor(Color.BLACK);
                mConfirm.setTextColor(Color.BLACK);
                mPagerPosition = 3;
            } else {
                mPagerPosition = 1;
                snackBar(AppConstants.getTextString(this, AppConstants.selectPayementMethodText));
            }
        }
    }

    /**
     * This method is used to Setup View Pager
     * @param mViewPager This is the parameter to setupViewPager method
     */
    private void setupViewPager(ViewPager mViewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFrag(new AddressFragment());
        if (mPaymentFragment) {
            adapter.addFrag(new PaymentFragment());
        }
        if (mConfirmFragment) {
            adapter.addFrag(new ConfirmFragment());
        }
        mViewPager.setAdapter(adapter);
    }

    /**
     * This method is used to onBackPressed method.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        deleteValue();
        hideProgDialog();
    }

    /**
     * This method is used to Custom Icon
     */
    private void customIcon() {
        Drawable sourceDrawable = getResources().getDrawable(R.drawable.step_active);
        Bitmap sourceBitmap = CustomIcon.convertDrawableToBitmap(sourceDrawable);
        mFinalBitmap = CustomIcon.changeImageColor(sourceBitmap, Color.parseColor(CustomBackground.mThemeColor));
    }

    /**
     * This method is used to onClick function.
     * @param v This is the parameter to onClick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * Proceed to Payment Click Listener*/
            case R.id.proceedTopayment:
                MyApplication.eventTracking("payment option", "payment option", "");
                mPagerPosition++;
                if (mPagerPosition <= 2) {
                    stepChanger(mPagerPosition);
                } else {
               /* deleteValue();
                deleteGuestDetails();*/
                    showProgDialiog();
                    Log.e("Identifier == ", paymentIdentifier);
                    //paymentIdentifier = "paypal";
                    if (paymentIdentifier.equalsIgnoreCase(getResources().getString(R.string.telr))) {
                        Intent creditIntent = new Intent(CheckoutActivity.this, CreditCardActivity.class);
                        creditIntent.putExtra("paymentMethodCode", paymentMethodCode);
                        startActivity(creditIntent);
                        finish();
                    } else {
                        Intent orderIntent = new Intent(CheckoutActivity.this, OrderConfirmActivity.class);
                        orderIntent.putExtra("paymentMethodCode", paymentMethodCode);
                        orderIntent.putExtra("paymentIdentifier", paymentIdentifier);
                        startActivity(orderIntent);
                        finish();
                    }
                }
                break;
        }
    }
}
