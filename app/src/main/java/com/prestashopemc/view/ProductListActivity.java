package com.prestashopemc.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.adapter.FilterExpandableAdapter;
import com.prestashopemc.adapter.NavigationFilterAdapter;
import com.prestashopemc.adapter.ProductListAdapter;
import com.prestashopemc.adapter.ProductListMagentoAdapter;
import com.prestashopemc.controller.CartController;
import com.prestashopemc.controller.ProductListController;
import com.prestashopemc.customView.CustomTextView;
import com.prestashopemc.model.CartDeleteInfo;
import com.prestashopemc.model.CartDeleteModel;
import com.prestashopemc.model.CartDeleteResult;
import com.prestashopemc.model.CartQuantityModel;
import com.prestashopemc.model.CartQuantityResult;
import com.prestashopemc.model.CategoryProduct;
import com.prestashopemc.model.MagentoCategoryProduct;
import com.prestashopemc.model.MagentoProduct;
import com.prestashopemc.model.Product;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.MyApplication;

import java.util.ArrayList;
import java.util.List;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>Product List Activity!</h1>
 * The Product List Activity implements product list display and functionalities.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 17 /3/16
 */
public class ProductListActivity extends BaseActivity {

    private RecyclerView mProductListView;
    private ProductListAdapter mProductListAdapter;
    private ProductListMagentoAdapter mProductMagentoListAdapter;
    private CustomTextView mFilterText, mSortText, mLowtoHigh, mHightoLow, mAscendtoDescend, mDescendtoAscend, mSortby;
    private RelativeLayout mProductListActivity, mSortLayout, mFilterLayout;
    private LinearLayout mSortMaster, mSortMain, mSortSecond, mCartIndicateLayout, mSortFilterLayout;
    private TextView mCartIndicateText;
    private ProductListController mProductController;
    private ProgressBar mProgressBar, mBottomProgressBar;
    private LinearLayoutManager mLayoutManager;
    private GridLayoutManager mGridLayoutManger;
    private int mPreviousTotal = 0;
    private boolean mLoading = true;
    private int mVisibleThreshold = 5;
    private int mFirstVisibleItem, mVisibleItemCount, mTotalItemCount;
    private int mCurrentPage = 1;
    private List<CategoryProduct> mProductList = new ArrayList<CategoryProduct>();
    private List<CategoryProduct> mSortProductList = new ArrayList<CategoryProduct>();
    private List<CategoryProduct> mFilterProductList = new ArrayList<CategoryProduct>();
    private List<MagentoCategoryProduct> mProductMagentoList = new ArrayList<MagentoCategoryProduct>();
    private List<MagentoCategoryProduct> mSortMagentoProductList = new ArrayList<MagentoCategoryProduct>();
    private List<MagentoCategoryProduct> mFilterMagentoProductList = new ArrayList<MagentoCategoryProduct>();
    private boolean mIsGrid = false;
    private float mScale = 1f;
    private ScaleGestureDetector mScaleGestureDetector;
    private GestureDetector mDetector;
    private boolean mScaleOngoing;
    private ImageView mItemTypeImage;
    private String mCategoryId, mCategoryName, mProductCountValue;
    private Bundle mBundle;
    private RelativeLayout mItemtypeimageLayout;
    private String mProductListLayout;
    private String mSortType, mSortBy;
    private String[] mValue;
    private boolean mSortCheck, mViewCheck;
    private boolean mFilterCheck;
    private FilterActivity mFilterActivity;
    private int mPrdCount = 0;
    private int mTotalPageNumber = 0;
    /**
     * The constant sIsFilter.
     */
    public static boolean sIsFilter = false;
    /**
     * The constant sIsWishList.
     */
    public static boolean sIsWishList = false;
    /**
     * The Cart delete info.
     */
    public CartDeleteInfo cartDeleteInfo;
    /**
     * The Cart delete result.
     */
    public CartDeleteResult cartDeleteResult;
    /**
     * The Cart quantity result.
     */
    public CartQuantityResult cartQuantityResult;
    private CartController mCartController;
    private boolean mIsFilter = false;
    private boolean mIsSort = false;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     *
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        MyApplication.getDefaultTracker("Product List Screen");
        initToolBar();
        initNetworkError();
        categoryTitle.setVisibility(View.VISIBLE);
        productCount.setVisibility(View.VISIBLE);
        cartIcon.setVisibility(View.VISIBLE);
        searchIcon.setVisibility(View.VISIBLE);
        cartCount();
        showProgDialiog();
        mProductListActivity = (RelativeLayout) findViewById(R.id.product_list_activity_mainLayout);
        initalizeLayout();
    }

    /**
     * This method is used to resume activity.
     */
    @Override
    protected void onResume() {
        super.onResume();
        cartCount();
        productIndicateLayout();
        if (sIsFilter) {
            sIsFilter = false;
            showProgDialiog();
            if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                mProductController.produtFilter(mCategoryId, mCurrentPage + "", FilterActivity.sFilterObject);
            }else {
                mProductController.produtNavigationFilter(mCategoryId, mCurrentPage + "",
                        FilterActivity.sNavigationFilterFeatureObject, FilterActivity.sNavigationFilterAttributeObject,
                        FilterActivity.mConditionArray, FilterActivity.mManufacturesArray, FilterActivity.sPriceObject,
                        FilterActivity.sWeightObject, FilterActivity.sAvailability);
            }
        }
        if (sIsWishList) {
            sIsWishList = false;
            finish();
            startActivity(getIntent());
        }
    }

    /**
     * This method is used to Initalized Layout
     */
    private void initalizeLayout() {

        boolean isInternet = isInternetOn(this);
        if (!isInternet) {
            mProductListActivity.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initalizeLayout();
                }
            });
            hideProgDialog();

        } else {
            mProductListActivity.setVisibility(View.VISIBLE);
            networkLayout.setVisibility(View.GONE);
            mProductController = new ProductListController(ProductListActivity.this);
            mFilterActivity = new FilterActivity();
            mProductListView = (RecyclerView) findViewById(R.id.product_list_view);
            mSortLayout = (RelativeLayout) findViewById(R.id.sort_layout);
            mFilterLayout = (RelativeLayout) findViewById(R.id.filter_layout);
            mFilterText = (CustomTextView) findViewById(R.id.filter_text);
            mSortText = (CustomTextView) findViewById(R.id.sort_text);
            mSortby = (CustomTextView) findViewById(R.id.sort_by);
            mItemTypeImage = (ImageView) findViewById(R.id.item_type_image);
            mItemtypeimageLayout = (RelativeLayout) findViewById(R.id.item_type_image_layout);
            mCartIndicateLayout = (LinearLayout) findViewById(R.id.cart_indicate_layout);
            mCartIndicateText = (TextView) findViewById(R.id.cart_indicate_text);
            mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
            mBottomProgressBar = (ProgressBar) findViewById(R.id.bottom_progress_bar);
            mScaleGestureDetector = new ScaleGestureDetector(this, new ScaleListener());
            mLayoutManager = new LinearLayoutManager(this);
            mGridLayoutManger = new GridLayoutManager(this, 2);

            if (mIsGrid) {
                mProductListView.setLayoutManager(mGridLayoutManger);
            } else {
                mProductListView.setLayoutManager(mLayoutManager);
            }
            mProductListView.setHasFixedSize(true);
            mProductListView.addOnScrollListener(mScrollListener);
            mFilterLayout.setOnClickListener(this);
            mSortLayout.setOnClickListener(this);
            mItemtypeimageLayout.setOnClickListener(this);
            mProgressBar.setVisibility(View.VISIBLE);
            mBundle = getIntent().getExtras();
            logoImage.setVisibility(View.GONE);
            if (mBundle != null) {
                mCategoryId = mBundle.getString("sCategoriesId");
                mCategoryName = mBundle.getString("categoryName");
                mProductCountValue = mBundle.getString("productCount");
            }
            categoryTitle.setText(mCategoryName);
            mLowtoHigh = (CustomTextView) findViewById(R.id.low_high);
            mHightoLow = (CustomTextView) findViewById(R.id.high_low);
            mAscendtoDescend = (CustomTextView) findViewById(R.id.ascending_descending);
            mDescendtoAscend = (CustomTextView) findViewById(R.id.descending_ascending);
            mSortMaster = (LinearLayout) findViewById(R.id.sort_master_layout);
            mSortMain = (LinearLayout) findViewById(R.id.sort_main_layout);
            mSortSecond = (LinearLayout) findViewById(R.id.sort_second_layout);
            mSortFilterLayout = (LinearLayout) findViewById(R.id.sort_fliter_layout);
            dynamicTextValue();
            setCustomFont();
            cartCount();
            mProductListLayout = SharedPreference.getInstance().getValue("product_list_layout");
            mProductController.getProductList(mCurrentPage + "", mCategoryId);


        }
    }

    /**
     * This method is used to Dynamic Text Value Settings
     */
    private void dynamicTextValue() {
        mSortText.setText(AppConstants.getTextString(this, AppConstants.listSort));
        mFilterText.setText(AppConstants.getTextString(this, AppConstants.filterText));
        mSortby.setText(AppConstants.getTextString(this, AppConstants.sortText));
        mHightoLow.setText(AppConstants.getTextString(this, AppConstants.sortHighToLowText));
        mLowtoHigh.setText(AppConstants.getTextString(this, AppConstants.sortLowToHighText));
        mAscendtoDescend.setText(AppConstants.getTextString(this, AppConstants.sortAscendingtoDescendingText));
        mDescendtoAscend.setText(AppConstants.getTextString(this, AppConstants.sortDescendingtoAscendingText));
    }

    /**
     * This method is used to Set Custom Font
     */
    private void setCustomFont() {
        mSortText.setTypeface(robotoMedium);
        mFilterText.setTypeface(robotoMedium);
        mSortby.setTypeface(robotoMedium);
        mHightoLow.setTypeface(robotoRegular);
        mLowtoHigh.setTypeface(robotoRegular);
        mAscendtoDescend.setTypeface(robotoRegular);
        mDescendtoAscend.setTypeface(robotoRegular);
        productCount.setTypeface(robotoLight);
        categoryTitle.setTypeface(robotoMedium);
    }

    /**
     * The On touch listner.
     */
    View.OnTouchListener onTouchListner = new View.OnTouchListener() {
        /**
         * This method is used to touch event listener.
         * @param v This is the first parameter to onTouch method.
         * @param motionEvent This is the second parameter to onTouch method.
         * @return boolean returns boolean value of item touched or not.
         */
        @Override
        public boolean onTouch(View v, MotionEvent motionEvent) {
//            detector.onTouchEvent(event);
//            return true;
            mScaleGestureDetector.onTouchEvent(motionEvent);
            mDetector.onTouchEvent(motionEvent);
            return true;
        }
    };

    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
        /**
         * The On scale begin.
         */
        float onScaleBegin = 0;
        /**
         * The On scale end.
         */
        float onScaleEnd = 0;

        /**
         * This method is used to Scale Listener
         *
         * @param detector This is the parameter to onScale method.
         * @return boolean returns boolean value for scale or not.
         */
        @Override
        public boolean onScale(ScaleGestureDetector detector) {
            mScale *= detector.getScaleFactor();
            return true;
        }

        /**
         * This method is used to Scale begin .
         *
         * @param detector This is the parameter to onScaleBegin method.
         * @return boolean returns boolean value for scale or not.
         */
        @Override
        public boolean onScaleBegin(ScaleGestureDetector detector) {
            onScaleBegin = mScale;
            mScaleOngoing = true;
            return true;
        }

        /**
         * This method is used to Scale end.
         *
         * @param detector This is the parameter to onScaleEnd method.
         */
        @Override
        public void onScaleEnd(ScaleGestureDetector detector) {
            onScaleEnd = mScale;
            mScaleOngoing = false;
            if (onScaleEnd > onScaleBegin) {
                GridView();
            }
            if (onScaleEnd < onScaleBegin) {
                GridView();
            }
            super.onScaleEnd(detector);
        }
    }

    private RecyclerView.OnScrollListener mScrollListener = new RecyclerView.OnScrollListener() {
        /**
         * This method is used to Recyclerview Scroll Listener
         * @param recyclerView This is the first parameter to onScrolled method.
         * @param dx This is the second parameter to onScrolled method.
         * @param dy This is the third parameter to onScrolled method.
         */
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            if (!mIsGrid) {
                mVisibleItemCount = mProductListView.getChildCount();
                mTotalItemCount = mLayoutManager.getItemCount();
                mFirstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();
            } else {
                mVisibleItemCount = mProductListView.getChildCount();
                mTotalItemCount = mGridLayoutManger.getItemCount();
                mFirstVisibleItem = mGridLayoutManger.findFirstVisibleItemPosition();
            }

            if (mLoading) {
                if (mTotalItemCount > mPreviousTotal) {
                    mLoading = false;
                    mPreviousTotal = mTotalItemCount;
                }
            }
            if (!mLoading && (mTotalItemCount - mVisibleItemCount)
                    <= (mFirstVisibleItem + mVisibleThreshold)) {
                mBottomProgressBar.setVisibility(View.VISIBLE);
                mCurrentPage++;

                if (mTotalPageNumber >= mCurrentPage) {

                    if (mViewCheck == true) {
                        mProductController.getProductList(mCurrentPage + "", mCategoryId);
                    }
                    if (mSortCheck == true) {
                        mProductController.sortByProduct(mCurrentPage + "", mSortType, mSortBy, mCategoryId);
                    }
                    if (mFilterCheck == true) {
                        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                            mProductController.produtFilter(mCategoryId, mCurrentPage + "", FilterActivity.sFilterObject);
                        }else {
                            mProductController.produtNavigationFilter(mCategoryId, mCurrentPage + "",
                                    FilterActivity.sNavigationFilterFeatureObject, FilterActivity.sNavigationFilterAttributeObject,
                                    FilterActivity.mConditionArray, FilterActivity.mManufacturesArray, FilterActivity.sPriceObject,
                                    FilterActivity.sWeightObject, FilterActivity.sAvailability);
                        }
                    }
                } else {
                    mBottomProgressBar.setVisibility(View.GONE);
                }
                mLoading = true;
            }
        }
    };

    /**
     * This method is used to Gridview type layout
     */
    public void GridView() {
        if (mProductList.size() > 0 ||mProductMagentoList.size()>0) {
            if (mIsGrid) {
                mIsGrid = false;
                mItemTypeImage.setImageResource(R.drawable.ic_grid);
                mProductListView.setLayoutManager(mLayoutManager);

            } else {
                mIsGrid = true;
                mItemTypeImage.setImageResource(R.drawable.ic_list);
                mProductListView.setLayoutManager(mGridLayoutManger);
            }

            if (!mIsFilter && !mIsSort) {
                if (AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
                    mProductMagentoListAdapter = new ProductListMagentoAdapter(this, mProductMagentoList, mIsGrid, Integer.parseInt(mProductListLayout));
                    mProductListView.setAdapter(mProductMagentoListAdapter);
                } else {
                    mProductListAdapter = new ProductListAdapter(ProductListActivity.this, mProductList, mIsGrid, Integer.parseInt(mProductListLayout));
                    mProductListView.setAdapter(mProductListAdapter);
                }
            } else {
                if (mIsFilter) {
                    if (AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
                        mProductMagentoListAdapter = new ProductListMagentoAdapter(this, mProductMagentoList, mIsGrid, Integer.parseInt(mProductListLayout));
                        mProductListView.setAdapter(mProductMagentoListAdapter);
                    } else {
                        mProductListAdapter = new ProductListAdapter(ProductListActivity.this, mProductList, mIsGrid, Integer.parseInt(mProductListLayout));
                        mProductListView.setAdapter(mProductListAdapter);
                    }
                }

                if (mIsSort) {
                    if (AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
                        mProductMagentoListAdapter = new ProductListMagentoAdapter(this, mProductMagentoList, mIsGrid, Integer.parseInt(mProductListLayout));
                        mProductListView.setAdapter(mProductMagentoListAdapter);
                    } else {
                        mProductListAdapter = new ProductListAdapter(ProductListActivity.this, mProductList, mIsGrid, Integer.parseInt(mProductListLayout));
                        mProductListView.setAdapter(mProductListAdapter);
                    }
                }
            }
        }
    }

    /**
     * This method is used to Products List From API
     *
     * @param mProduct This is the parameter updateView method.
     */
    public void updateView(Product mProduct) {
        hideProgDialog();
        mIsSort = false;
        mIsFilter = false;
        mViewCheck = true;
        mSortCheck = false;
        mFilterCheck = false;
        mProgressBar.setVisibility(View.GONE);
        mBottomProgressBar.setVisibility(View.GONE);
        mPrdCount = mProduct.getProductCount();
        if (mProduct != null) {
            mProductList.addAll(mProduct.getCategoryProducts());
            categoryTitle.setText(mCategoryName);
            productCount.setText(mProduct.getProductCount() + " " + AppConstants.getTextString(this, AppConstants.productsText));
            if (mCurrentPage == 1) {
                totalPageNo();
                mProductListAdapter = new ProductListAdapter(this, mProductList, mIsGrid, Integer.parseInt(mProductListLayout));
                mProductListView.setAdapter(mProductListAdapter);
            } else {
                mProductListAdapter.notifyDataSetChanged();
            }
        }
    }

    /**
     * This method is used to Products List From API
     *
     * @param mProduct This is the parameter updateView method.
     */
    public void updateMagentoView(MagentoProduct mProduct) {
        hideProgDialog();
        mIsSort = false;
        mIsFilter = false;
        mViewCheck = true;
        mSortCheck = false;
        mFilterCheck = false;
        mProgressBar.setVisibility(View.GONE);
        mBottomProgressBar.setVisibility(View.GONE);
        mPrdCount = mProduct.getProductCount();
        if (mProduct != null) {
            mProductMagentoList.addAll(mProduct.getCategoryProducts());
            categoryTitle.setText(mCategoryName);
            productCount.setText(mProduct.getProductCount() + " " + AppConstants.getTextString(this, AppConstants.productsText));
            if (mCurrentPage == 1) {
                totalPageNo();
                mProductMagentoListAdapter = new ProductListMagentoAdapter(this, mProductMagentoList, mIsGrid, Integer.parseInt(mProductListLayout));
                mProductListView.setAdapter(mProductMagentoListAdapter);
            } else {
                mProductMagentoListAdapter.notifyDataSetChanged();
            }
        }
    }

    /**
     * This method is used to Sort Products list From API
     *
     * @param mProduct  This is the first parameter to updateSortView method.
     * @param mSorttype This is the second parameter to updateSortView method.
     * @param mSortby   This is the third parameter to updateSortView method.
     */
    public void updateSortView(Product mProduct, String mSorttype, String mSortby) {
        hideProgDialog();
        mIsSort = true;
        mIsFilter = false;
        mSortType = mSorttype;
        mSortBy = mSortby;
        mSortCheck = true;
        mViewCheck = false;
        mFilterCheck = false;
        mProgressBar.setVisibility(View.GONE);
        mBottomProgressBar.setVisibility(View.GONE);
        mPrdCount = mProduct.getProductCount();
        if (mProduct != null) {
            mSortProductList.addAll(mProduct.getCategoryProducts());
            categoryTitle.setText(mCategoryName);
            productCount.setText(mProduct.getProductCount() + " " + AppConstants.getTextString(this, AppConstants.productsText));
            if (mCurrentPage == 1) {
                totalPageNo();
                mProductListAdapter = new ProductListAdapter(this, mSortProductList, mIsGrid, Integer.parseInt(mProductListLayout));
                mProductListView.setAdapter(mProductListAdapter);
            } else {
                mProductListAdapter.notifyDataSetChanged();
            }
        }
    }

    /**
     * This method is used to Sort Products list From API
     *
     * @param mProduct  This is the first parameter to updateSortView method.
     * @param mSorttype This is the second parameter to updateSortView method.
     * @param mSortby   This is the third parameter to updateSortView method.
     */
    public void updateMagentoSortView(MagentoProduct mProduct, String mSorttype, String mSortby) {
        hideProgDialog();
        mIsSort = true;
        mIsFilter = false;
        mSortType = mSorttype;
        mSortBy = mSortby;
        mSortCheck = true;
        mViewCheck = false;
        mFilterCheck = false;
        mProgressBar.setVisibility(View.GONE);
        mBottomProgressBar.setVisibility(View.GONE);
        mPrdCount = mProduct.getProductCount();
        if (mProduct != null) {
            mSortMagentoProductList.addAll(mProduct.getCategoryProducts());
            categoryTitle.setText(mCategoryName);
            productCount.setText(mProduct.getProductCount() + " " + AppConstants.getTextString(this, AppConstants.productsText));
            if (mCurrentPage == 1) {
                totalPageNo();
                mSortMagentoProductList.clear();
                mSortMagentoProductList.addAll(mProduct.getCategoryProducts());
                mProductMagentoListAdapter = new ProductListMagentoAdapter(this, mSortMagentoProductList, mIsGrid, Integer.parseInt(mProductListLayout));
                mProductListView.setAdapter(mProductMagentoListAdapter);
            } else {
                mProductMagentoListAdapter.notifyDataSetChanged();
            }
        }
    }

    /**
     * This method is used to Filter Products list From API
     *
     * @param mProduct This is the parameter to updateFilterView method.
     */
    public void updateFilterView(Product mProduct) {
        hideProgDialog();
        mIsFilter = true;
        mIsSort = false;
        mFilterCheck = true;
        mViewCheck = false;
        mSortCheck = false;
        mProgressBar.setVisibility(View.GONE);
        mBottomProgressBar.setVisibility(View.GONE);
        mPrdCount = mProduct.getProductCount();
        if (mProduct != null) {
            mFilterProductList.addAll(mProduct.getCategoryProducts());
            categoryTitle.setText(mCategoryName);
            productCount.setText(mProduct.getProductCount() + " " + AppConstants.getTextString(this, AppConstants.productsText));
            if (mCurrentPage == 1) {
                totalPageNo();
                mFilterProductList.clear();
                mFilterProductList.addAll(mProduct.getCategoryProducts());
                mProductListAdapter = new ProductListAdapter(this, mFilterProductList, mIsGrid, Integer.parseInt(mProductListLayout));
                mProductListView.setAdapter(mProductListAdapter);
            } else {
                mProductListAdapter.notifyDataSetChanged();
            }
        }
    }


    /**
     * This method is used to Filter Products list From API
     *
     * @param mProduct This is the parameter to updateFilterView method.
     */
    public void updateMagentoFilterView(MagentoProduct mProduct) {
        hideProgDialog();
        mIsFilter = true;
        mIsSort = false;
        mFilterCheck = true;
        mViewCheck = false;
        mSortCheck = false;
        mProgressBar.setVisibility(View.GONE);
        mBottomProgressBar.setVisibility(View.GONE);
        mPrdCount = mProduct.getProductCount();
        if (mProduct != null) {
            mFilterMagentoProductList.addAll(mProduct.getCategoryProducts());
            categoryTitle.setText(mCategoryName);
            productCount.setText(mProduct.getProductCount() + " " + AppConstants.getTextString(this, AppConstants.productsText));
            if (mCurrentPage == 1) {
                totalPageNo();
                mFilterMagentoProductList.clear();
                mFilterMagentoProductList.addAll(mProduct.getCategoryProducts());
                mProductMagentoListAdapter = new ProductListMagentoAdapter(this, mFilterMagentoProductList, mIsGrid, Integer.parseInt(mProductListLayout));
                mProductListView.setAdapter(mProductMagentoListAdapter);
            } else {
                mProductMagentoListAdapter.notifyDataSetChanged();
            }
        }
    }

    /**
     * This method is used to Products list From DB
     *
     * @param productList This is the parameter to updateDatabaseView method.
     */
    public void updateDatabaseView(List<CategoryProduct> productList) {
        hideProgDialog();
        mIsSort = false;
        mIsFilter = false;
        mViewCheck = true;
        mProgressBar.setVisibility(View.GONE);
        mBottomProgressBar.setVisibility(View.GONE);
        mProductList.addAll(productList);
        mPrdCount = Integer.parseInt(mProductCountValue);
        if (mProductList.size() > 0) {
            categoryTitle.setText(mCategoryName);
            productCount.setText(mProductCountValue + " " + AppConstants.getTextString(this, AppConstants.productsText));
            if (mCurrentPage == 1) {
                totalPageNo();
                mProductListAdapter = new ProductListAdapter(this, mProductList, mIsGrid, Integer.parseInt(mProductListLayout));
                mProductListView.setAdapter(mProductListAdapter);
            } else {
                mProductListAdapter.notifyDataSetChanged();
            }
        }
    }


    /**
     * This method is used to Products list From DB
     *
     * @param productList This is the parameter to updateDatabaseView method.
     */
    public void updateMagentoDatabaseView(List<MagentoCategoryProduct> productList) {
        hideProgDialog();
        mIsSort = false;
        mIsFilter = false;
        mViewCheck = true;
        mProgressBar.setVisibility(View.GONE);
        mBottomProgressBar.setVisibility(View.GONE);
        mProductMagentoList.addAll(productList);
        mPrdCount = Integer.parseInt(mProductCountValue);
        if (mProductMagentoList.size() > 0) {
            categoryTitle.setText(mCategoryName);
            productCount.setText(mProductCountValue + " " + AppConstants.getTextString(this, AppConstants.productsText));
            if (mCurrentPage == 1) {
                totalPageNo();
                mProductMagentoListAdapter = new ProductListMagentoAdapter(this, mProductMagentoList, mIsGrid, Integer.parseInt(mProductListLayout));
                mProductListView.setAdapter(mProductMagentoListAdapter);
            } else {
                mProductMagentoListAdapter.notifyDataSetChanged();
            }
        }
    }


    /**
     * This method is used to Product list Failure response from API
     *
     * @param errorMsg This is the parameter to updateFailure method.
     */
    public void updateFailure(String errorMsg) {
        hideProgDialog();
        if (errorMsg.equalsIgnoreCase(getString(R.string.noProductsFoundText))
                || errorMsg.equalsIgnoreCase(getString(R.string.noResultsFoundText))) {
            mSortFilterLayout.setVisibility(View.GONE);
        }
        //productCount.setText("0 " + AppConstants.getTextString(this, AppConstants.productsText));
        mProgressBar.setVisibility(View.GONE);
        snackBar(errorMsg);
    }

    /**
     * This method is used to Cart update From API
     *
     * @param productName This is the parameter to updateCart method.
     */
    public void updateCart(String productName) {
        cartCount();
        hideProgDialog();
        snackBar(AppConstants.getTextString(this, AppConstants.addedCartText));
    }

    /**
     * This method is used to Cart Failure response From API
     *
     * @param response This is the parameter to updateCartFailure method.
     */
    public void updateCartFailure(String response) {
        hideProgDialog();
        productIndicateLayout();
        mProgressBar.setVisibility(View.GONE);
        snackBar(response);
    }

    /**
     * This method is used to Find total page numbers
     */
    private void totalPageNo() {
        int extraPage = 0;
        if ((mPrdCount % AppConstants.productsPerPage) == 0) {
            extraPage = 0;
        } else {
            extraPage = 1;
        }
        mTotalPageNumber = (mPrdCount / AppConstants.productsPerPage) + extraPage;
    }

    /**
     * This method is used to Sort layout Functionalities
     */
    private void sortLayout() {
        mCurrentPage = 1;
        mPreviousTotal = 0;
        mSortProductList.clear();
        mSortMaster.setVisibility(View.VISIBLE);
        mSortMaster.startAnimation(AppConstants.setAnimateIn(ProductListActivity.this, R.anim.falling));
        mProductListActivity.setAlpha((float) 0.7);
        mSortMain.setOnClickListener(this);
        mLowtoHigh.setOnClickListener(this);
        mHightoLow.setOnClickListener(this);
        mAscendtoDescend.setOnClickListener(this);
        mDescendtoAscend.setOnClickListener(this);
    }

    /**
     * This method is used to Sort layout animation for open and close
     */
    private void sortAnimation() {
        showProgDialiog();
        mProductListActivity.setAlpha((float) 1);
        mProductController.sortByProduct(mCurrentPage + "", mSortType, mSortBy, mCategoryId);
        mSortMaster.setVisibility(View.GONE);
        mSortMaster.startAnimation(AppConstants.setAnimateOut(ProductListActivity.this, R.anim.fadeout));
    }

    /**
     * This method is used to back pressed function calling.
     */
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        hideProgDialog();
        sIsFilter = false;
        sIsWishList = false;
        FilterExpandableAdapter.mSelectedFilter.clear();
    }

    /**
     * This method is used to Cart items delete
     *
     * @param cartDeleteModel This is the parameter to updateDeleteView method.
     */
    public void updateDeleteView(CartDeleteModel cartDeleteModel) {
        cartDeleteResult = cartDeleteModel.getResult();
        cartDeleteInfo = cartDeleteResult.getInfo();
        SharedPreference mPref = SharedPreference.getInstance();
        mPref.save("cart_count", cartDeleteResult.getCartCount() + "");
        mPref.save("cart_total", cartDeleteResult.getInfo().getGrandtotal());
        cartCount();
        productIndicateLayout();
        if (cartDeleteResult.getCartCount() == 0) {
            snackBar(AppConstants.getTextString(this, AppConstants.noProductInCartText));
            hideProgDialog();
        } else {
            hideProgDialog();
        }

    }

    /**
     * This method is used to Cart items Quantity update response
     *
     * @param cartQuantityModel This is the first parameter to updateQuantityView method.
     * @param quant             This is the second parameter to updateQuantityView method.
     * @param quantPosition     This is the third parameter to updateQuantityView method.
     */
    public void updateQuantityView(CartQuantityModel cartQuantityModel, String quant, String quantPosition) {
        cartQuantityResult = cartQuantityModel.getResult();
        SharedPreference mPref = SharedPreference.getInstance();
        mPref.save("cart_id", cartQuantityResult.getCartId() + "");
        mPref.save("cart_count", cartQuantityResult.getCartCount() + "");
        mPref.save("cart_total", cartQuantityResult.getGrandtotal());
        cartCount();
        productIndicateLayout();
        hideProgDialog();
    }


    /**
     * This method is used to Bottom Cart Product count indicate layout
     */
    private void productIndicateLayout() {
        String cartCount = SharedPreference.getInstance().getValue("cart_count");
        String currenySymbol = SharedPreference.getInstance().getValue("currency_symbol");
        String price = SharedPreference.getInstance().getValue("cart_total");
        if (!cartText.getText().equals("0")) {
            if (mProductListLayout.equals("3")) {
                mCartIndicateLayout.setVisibility(View.VISIBLE);
                mCartIndicateText.setText(AppConstants.getTextString(this, AppConstants.yourCartText) + cartText.getText() +
                        AppConstants.getTextString(this, AppConstants.productAvailableText) + currenySymbol + " " + price);
                mCartIndicateLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mCartIndicateLayout.setVisibility(View.GONE);
                    }
                }, 6000);
            } else {
                mCartIndicateLayout.setVisibility(View.GONE);
            }
        }
    }

    /**
     * This method is used to Add Wishlist Success Response
     *
     * @param result This is the parameter to wishlistSuccess method.
     */
    public void wishlistSuccess(String result) {
        hideProgDialog();
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            mProductMagentoListAdapter.notifyDataSetChanged();
        }else {
            mProductListAdapter.notifyDataSetChanged();
        }

        snackBar(result);
    }

    /**
     * This method is used to Add Wishlist Failure Response
     *
     * @param errormsg This is the parameter to wishlistFailure method.
     */
    public void wishlistFailure(String errormsg) {
        hideProgDialog();
        snackBar(errormsg);
    }

    /**
     * This method is used to Add Wishlist Error Response
     *
     * @param string This is the parameter to failure method.
     */
    public void failure(String string) {
        hideProgDialog();
        snackBar(string);
    }

    /**
     * This method is used to on click function.
     *
     * @param v This is the parameter to onClick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * Change list & grid type layouts
             */
            case R.id.item_type_image_layout:
                GridView();
                break;

            /**
             * To visible Sort layout
             */
            case R.id.sort_layout:
                sortLayout();
                break;

            /**
             * To Filter Activity
             */
            case R.id.filter_layout:
                mCurrentPage = 1;
                mPreviousTotal = 0;
                mFilterProductList.clear();
                Intent filterIntent = new Intent(ProductListActivity.this, FilterActivity.class);
                filterIntent.putExtra("category_id", mCategoryId);
                filterIntent.putExtra("filter", "productList");
                startActivity(filterIntent);
                break;

            /**
             * To Sort Activity
             */
            case R.id.sort_main_layout:
                mProductListActivity.setAlpha((float) 1);
                mSortMaster.setVisibility(View.GONE);
                mSortMaster.startAnimation(AppConstants.setAnimateOut(ProductListActivity.this, R.anim.fadeout));
                break;

            /**
             * Low to high sorting click
             */
            case R.id.low_high:
                mSortType = "asc";
                mSortBy = "price";
                sortAnimation();
                break;

            /**
             * High to low sorting click
             */
            case R.id.high_low:
                mSortType = "desc";
                mSortBy = "price";
                sortAnimation();
                break;

            /**
             * Ascending to Descending sorting click
             */
            case R.id.ascending_descending:
                mSortType = "asc";
                mSortBy = "name";
                sortAnimation();
                break;

            /**
             * Descending to Ascending sorting click
             */
            case R.id.descending_ascending:
                mSortType = "desc";
                mSortBy = "name";
                sortAnimation();
                break;
        }
    }
}
