package com.prestashopemc.view;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.controller.LicenseController;
import com.prestashopemc.model.LicenseMain;
import com.prestashopemc.model.LicenseResult;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.MyApplication;

import java.util.Calendar;
import java.util.Date;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>Setting Activity!</h1>
 * The Setting Activity implements chat details.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 19 /8/16
 */
public class SplashActivity extends BaseActivity {

    private RelativeLayout mSplashLayout, mLicenseLayout;
    private TextView mLicenseHeader, mLicenseMsg, mLicenseOk;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     *
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        MyApplication.getDefaultTracker("Splash Screen");
        initNetworkError();
        mSplashLayout = (RelativeLayout) findViewById(R.id.splash_layout);
        mLicenseLayout = (RelativeLayout) findViewById(R.id.license_layout);
        mLicenseHeader = (TextView) findViewById(R.id.header_text);
        mLicenseMsg = (TextView) findViewById(R.id.msg_text);
        mLicenseOk = (TextView) findViewById(R.id.ok_text);
        setCustomFont();

        SharedPreference.getInstance().saveBoolean("is_siteurl_save", true);

        Log.e("save_url", String.valueOf(SharedPreference.getInstance().getBoolean("is_siteurl_save")));

        if (SharedPreference.getInstance().getBoolean("is_siteurl_save")) {
            licenseCheck();
        } else {
            Intent selectIntent = new Intent(SplashActivity.this, SelectActivity.class);
            startActivity(selectIntent);
        }

    }

    /**
     * This method is used to Dynamic Text Value Settings
     */
    private void dynamicTextValue() {
        mLicenseHeader.setText(AppConstants.getTextString(this, AppConstants.accessDeniedText));
        mLicenseMsg.setText(AppConstants.getTextString(this, AppConstants.contactAdminText));
        mLicenseOk.setText(AppConstants.getTextString(this, AppConstants.okayText));
    }

    /**
     * This method is used to Set Custom Font
     */
    public void setCustomFont() {
        mLicenseHeader.setTypeface(robotoMedium);
        mLicenseMsg.setTypeface(robotoLight);
        mLicenseOk.setTypeface(robotoMedium);
    }

    /**
     * This method is used to License Check
     */
    public void licenseCheck() {

        long licCheck = SharedPreference.getInstance().getLong("lic_check");
        boolean hasLicense = SharedPreference.getInstance().getBoolean("has_license");
        long curTime = new Date().getTime();
        if (licCheck == 0 || !hasLicense) {
            callLicense();
        } else if (curTime == licCheck || curTime > licCheck) {
            callLicense();
        } else {
            boolean isInternet = isInternetOn(this);
            if (!isInternet) {
                dynamicTextValue();
                mSplashLayout.setVisibility(View.GONE);
                mLicenseLayout.setVisibility(View.GONE);
                networkLayout.setVisibility(View.VISIBLE);
                networkTry.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        licenseCheck();
                    }
                });
                hideProgDialog();

            } else {
                if (getIntent().getAction() == Intent.ACTION_VIEW) {
                    Uri uri = getIntent().getData();
                    // do stuff with uri
                    Log.e("URI Response ==== ", uri + "");
                }
                networkLayout.setVisibility(View.GONE);
                Handler handler = new Handler();

                final Runnable r = new Runnable() {
                    @Override
                    public void run() {
                        Intent loginIntent = new Intent(SplashActivity.this, MainActivity.class);
                        startActivity(loginIntent);
                    }
                };
                handler.postDelayed(r, 2000);
            }
        }
    }

    /**
     * This method is used to License Call Method
     */
    public void callLicense() {
        LicenseController mLicenseController = new LicenseController(SplashActivity.this);
        mLicenseController.licenseCheck();
        Date dtStartDate = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(dtStartDate);
        c.add(Calendar.DATE, 30);// number of days to add
        SharedPreference.getInstance().saveLong("lic_check", c.getTimeInMillis());
    }

    /**
     * This method is used to License Success Response
     *
     * @param status      This is the first parameter to licenseSuccess method.
     * @param licenseMain This is the second parameter to licenseSuccess method.
     */
    public void licenseSuccess(boolean status, LicenseMain licenseMain) {
        LicenseResult licenseResult = new LicenseResult();
        licenseResult = licenseMain.getResult();
        if (!licenseResult.getStatus()) {
            SharedPreference.getInstance().saveBoolean("has_license", status);
            Handler handler = new Handler();

            final Runnable r = new Runnable() {
                @Override
                public void run() {
                    Intent loginIntent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(loginIntent);
                }
            };
            handler.postDelayed(r, 2000);
        } else {
            mSplashLayout.setVisibility(View.GONE);
            mLicenseLayout.setVisibility(View.VISIBLE);
            SharedPreference.getInstance().saveBoolean("has_license", false);
            mLicenseOk.setOnClickListener(this);
        }

    }

    /**
     * This method is used to License Failure Response
     *
     * @param status This is the parameter to licenseError method.
     */
    public void licenseError(boolean status) {
        mSplashLayout.setVisibility(View.GONE);
        mLicenseLayout.setVisibility(View.VISIBLE);
        SharedPreference.getInstance().saveBoolean("has_license", status);
        mLicenseOk.setOnClickListener(this);
    }

    /**
     * This method is used to back pressed
     */
    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    /**
     * This method is used to onclick action
     *
     * @param v This is the parameter to onClick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * License Failure Ok Listener*/
            case R.id.ok_text:
                finish();
                System.exit(0);
                break;
        }
    }
}
