package com.prestashopemc.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.adapter.MyOrdersListAdapter;
import com.prestashopemc.controller.LoginController;
import com.prestashopemc.model.MyOrderListMain;
import com.prestashopemc.model.MyOrderListResult;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.MyApplication;

import java.util.List;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>My Order Activity!</h1>
 * The My Order Activity implements my order details.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author Rajesh
 * @version 1.2
 * @since 21 -10-2016
 */
public class MyOrdersActivity extends BaseActivity {

    private RecyclerView myOrderList;
    private LoginController mLoginController;
    private MyOrdersListAdapter myOrdersListAdapter;
    private List<MyOrderListResult> myOrdersListResults;
    private RelativeLayout mMainLayout, mNoOrdersLayout;
    private TextView mHeaderText, mMsgText, mContinueShopping;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     *
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_myorders);
        MyApplication.getDefaultTracker("My order Screen");
        initToolBar();
        initNetworkError();
        logoImage.setVisibility(View.GONE);
        searchIcon.setVisibility(View.GONE);
        cartIcon.setVisibility(View.GONE);
        cartText.setVisibility(View.GONE);
        title.setVisibility(View.VISIBLE);
        title.setText(AppConstants.getTextString(this, AppConstants.myOrdersText));
        title.setAllCaps(true);
        showProgDialiog();
        mMainLayout = (RelativeLayout) findViewById(R.id.main_layout);
        initializedLayout();
    }

    /**
     * This method is used to Initialized Layout
     */
    private void initializedLayout() {

        boolean isInternet = isInternetOn(this);
        if (!isInternet) {
            mMainLayout.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initializedLayout();
                }
            });
            hideProgDialog();

        } else {
            mMainLayout.setVisibility(View.VISIBLE);
            networkLayout.setVisibility(View.GONE);
            mNoOrdersLayout = (RelativeLayout) findViewById(R.id.no_orders_layout);
            mHeaderText = (TextView) findViewById(R.id.no_orders_header);
            mMsgText = (TextView) findViewById(R.id.no_orders_msg);
            mContinueShopping = (TextView) findViewById(R.id.no_orders_continue);
            CustomBackground.setBackgroundText(mContinueShopping);
            mLoginController = new LoginController(MyOrdersActivity.this);
            dynamicTextValue();
            myOrderList = (RecyclerView) findViewById(R.id.myorders_list);
            myOrderList.setLayoutManager(new LinearLayoutManager(MyOrdersActivity.this));
            mLoginController.myOrdersList();
            mHeaderText.setTypeface(robotoMedium);
            mMsgText.setTypeface(robotoLight);
            mContinueShopping.setTypeface(robotoMedium);
            mContinueShopping.setOnClickListener(this);
        }
    }

    /**
     * This method is used to Dynamic Text Value Settings
     */
    private void dynamicTextValue() {
        mHeaderText.setText(AppConstants.getTextString(this, AppConstants.noOrdersText));
        mMsgText.setText(AppConstants.getTextString(this, AppConstants.noOrdersMsgText));
        mContinueShopping.setText(AppConstants.getTextString(this, AppConstants.startShoppingText));
    }

    /**
     * This method is used to My Orders Success Response
     *
     * @param myOrderListMain This is the parameter to myOrdersSuccess method.
     */
    public void myOrdersSuccess(MyOrderListMain myOrderListMain) {
        hideProgDialog();
        myOrdersListResults = myOrderListMain.getResult();
        if (myOrdersListResults != null) {
            myOrdersListAdapter = new MyOrdersListAdapter(MyOrdersActivity.this, myOrdersListResults);
            myOrderList.setAdapter(myOrdersListAdapter);
        } else {
            myOrderList.setVisibility(View.GONE);
            mNoOrdersLayout.setVisibility(View.VISIBLE);
        }

    }

    /**
     * This method is used to My Orders Failure Response
     *
     * @param errormsg This is the method to myOrdersFailure method.
     */
    public void myOrdersFailure(String errormsg) {
        hideProgDialog();
        String msg = AppConstants.getTextString(this, AppConstants.noOrderText);
        if (errormsg.equals(msg) || errormsg.equals(getString(R.string.noOrderAvailableText))) {
            myOrderList.setVisibility(View.GONE);
            mNoOrdersLayout.setVisibility(View.VISIBLE);
        } else {
            snackBar(errormsg);
        }
    }

    /**
     * This method is used to onClick function.
     *
     * @param v This is the parameter to onClick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * Continue Shopping*/
            case R.id.no_orders_continue:
                finish();
                break;
        }
    }
}
