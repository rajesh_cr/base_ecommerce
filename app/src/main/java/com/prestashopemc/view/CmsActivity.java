package com.prestashopemc.view;

import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.RelativeLayout;

import com.prestashopemc.R;
import com.prestashopemc.controller.LoginController;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.WebViewController;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>CMS Activity!</h1>
 * The CMS Activity implements cms page.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 26 /8/16
 */
public class CmsActivity extends BaseActivity {

    private WebView mWebView;
    private Bundle mBundle;
    private String mCmsTitle, mCmsPageIdentifier;
    private LoginController mLoginController;
    private RelativeLayout mMainLayout;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     *
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cms);
        MyApplication.getDefaultTracker("CMS Screen");
        initToolBar();
        initNetworkError();
        logoImage.setVisibility(View.GONE);
        searchIcon.setVisibility(View.GONE);
        cartIcon.setVisibility(View.GONE);
        cartText.setVisibility(View.GONE);
        title.setVisibility(View.VISIBLE);
        title.setAllCaps(true);
        showProgDialiog();
        mMainLayout = (RelativeLayout) findViewById(R.id.main_layout);
        initializedLayout();
    }

    /**
     * This method is used to Initialized Layout
     */
    private void initializedLayout() {

        boolean isInternet = isInternetOn(this);
        if (!isInternet) {
            mMainLayout.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    showProgDialiog();
                    initializedLayout();
                }
            });
            hideProgDialog();

        } else {
            mMainLayout.setVisibility(View.VISIBLE);
            networkLayout.setVisibility(View.GONE);
            mLoginController = new LoginController(CmsActivity.this);
            mWebView = (WebView) findViewById(R.id.cms_webview);
            mBundle = getIntent().getExtras();
            if (mBundle != null) {
                mCmsTitle = mBundle.getString("cmsTitle");
                mCmsPageIdentifier = mBundle.getString("cmsIdentifier");
                title.setText(mCmsTitle);
            }
            getCmsDetails();
        }
    }

    /**
     * This method is used to Cms API call
     */
    private void getCmsDetails() {
        mLoginController.cmsDetails(mCmsPageIdentifier);
    }

    /**
     * This method is used to Cms details Success Response
     *
     * @param response This is the parameter to cmsSuccess method.
     */
    public void cmsSuccess(String response) {
        hideProgDialog();
        mWebView.getSettings().setJavaScriptEnabled(true);
        mWebView.setWebViewClient(new WebViewController(CmsActivity.this));
        mWebView.loadData(response, "text/html; charset=utf-8", "UTF-8");
        mWebView.getSettings().setBuiltInZoomControls(true);
        mWebView.getSettings().setDomStorageEnabled(true);
        mWebView.getSettings().setSupportMultipleWindows(true);
        mWebView.getSettings().setDisplayZoomControls(false);
        mWebView.getSettings().setUseWideViewPort(true);
    }

    /**
     * This method is used to Cms details Failure Response
     *
     * @param string This is the parameter to cmsFailure method.
     */
    public void cmsFailure(String string) {
        hideProgDialog();
        snackBar(string);
    }


    // Inner class
   /* public class WebViewController extends WebViewClient {
        ProgressDialog pd = null;

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            pd = new ProgressDialog(CmsActivity.this);
            pd.setMessage("Page is loading...");
            pd.show();
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            pd.dismiss();
        }
    }*/
}
