package com.prestashopemc.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.adapter.MyAddressAdapter;
import com.prestashopemc.controller.LoginController;
import com.prestashopemc.model.MyAdditionalAddress;
import com.prestashopemc.model.MyAdditionalAddressResult;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.MyApplication;

import java.util.ArrayList;
import java.util.List;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>My Address Activity!</h1>
 * The My Address Activity implements
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class MyAddressActivity extends BaseActivity {

    private RelativeLayout mAddAddressLayout;
    private TextView mAddAddressLabel;
    private RecyclerView mAdditionalAddressView;
    private List<MyAdditionalAddressResult> myAdditionalAddressResults = new ArrayList<>();
    private LoginController mLoginController;
    private MyAddressAdapter myAddressAdapter;
    private RelativeLayout mMainLayout;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     *
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_address);
        MyApplication.getDefaultTracker("My Address Screen");
        initToolBar();
        initNetworkError();
        logoImage.setVisibility(View.GONE);
        searchIcon.setVisibility(View.GONE);
        cartIcon.setVisibility(View.GONE);
        cartText.setVisibility(View.GONE);
        title.setVisibility(View.VISIBLE);
        title.setText(AppConstants.getTextString(this, AppConstants.addressInfoText));
        title.setAllCaps(true);
        showProgDialiog();
        mMainLayout = (RelativeLayout) findViewById(R.id.main_layout);
        initializedLayout();
    }

    /**
     * This method is used to restart activity.
     */
    @Override
    protected void onRestart() {
        super.onRestart();
        refreshActivity();
    }

    /**
     * This method is used to Initialized Layout
     */
    private void initializedLayout() {

        boolean isInternet = isInternetOn(this);
        if (!isInternet) {
            mMainLayout.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initializedLayout();
                }
            });
            hideProgDialog();

        } else {
            mMainLayout.setVisibility(View.VISIBLE);
            networkLayout.setVisibility(View.GONE);
            //hideProgDialog();
            mLoginController = new LoginController(MyAddressActivity.this);
            mLoginController.myAddress();
            mAddAddressLayout = (RelativeLayout) findViewById(R.id.add_new_address_layout);
            mAddAddressLabel = (TextView) findViewById(R.id.add_new_address_label);
            mAdditionalAddressView = (RecyclerView) findViewById(R.id.address_list);
            mAddAddressLayout.setOnClickListener(this);

            mAddAddressLabel.setText(AppConstants.getTextString(this, AppConstants.addNewAddressText));
            setCustomFont();
        }
    }

    /**
     * This method is used to Set Custom Font
     */
    private void setCustomFont() {
        mAddAddressLabel.setTypeface(robotoRegular);
    }

    /**
     * This method is used to My Address Success Response
     *
     * @param addressMainModel This is the parameter to addressSuccessResponse method.
     */
    public void addressSuccessResponse(MyAdditionalAddress addressMainModel) {
        hideProgDialog();
        myAdditionalAddressResults = addressMainModel.getResult();
        mAdditionalAddressView.setLayoutManager(new LinearLayoutManager(this));
        myAddressAdapter = new MyAddressAdapter(MyAddressActivity.this, myAdditionalAddressResults);
        mAdditionalAddressView.setAdapter(myAddressAdapter);
    }

    /**
     * This method is used to My Address Failure Response
     *
     * @param errorResponse This is the parameter to addressFailureResponse method.
     */
    public void addressFailureResponse(String errorResponse) {
        hideProgDialog();
        snackBar(errorResponse);
    }

    /**
     * This method is used to Default address Success
     *
     * @param response This is the parameter to defaultAddressSuccessResponse method.
     */
    public void defaultAddressSuccessResponse(String response) {
        hideProgDialog();
        mLoginController.myAddress();
        snackBar(response);
    }

    /**
     * This method is used to Default Address Failure
     *
     * @param errorResponse This is the parameter to defaultAddressFailureResponse method.
     */
    public void defaultAddressFailureResponse(String errorResponse) {
        hideProgDialog();
        snackBar(errorResponse);
    }

    /**
     * This method is used to Delete Address Success
     */
    public void successDelete() {
        hideProgDialog();
        snackBar(AppConstants.getTextString(this, AppConstants.newAddressDeletetext));
        mLoginController.myAddress();
    }

    /**
     * This method is used to Delete Address Failure
     *
     * @param errormsg This is the parameter to failure method.
     */
    public void failure(String errormsg) {
        hideProgDialog();
        snackBar(errormsg);
    }

    /**
     * This method is used to Refresh activity
     */
    public void refreshActivity() {
        finish();
        startActivity(getIntent());
    }

    /**
     * This method is used to onClick
     *
     * @param v This is the parameter to onClick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * Add Address Click Listener*/
            case R.id.add_new_address_layout:
                Intent newaddress = new Intent(MyAddressActivity.this, AddNewAddress.class);
                newaddress.putExtra("fromDefault", "fromDefault");
                startActivity(newaddress);
                break;
        }
    }
}
