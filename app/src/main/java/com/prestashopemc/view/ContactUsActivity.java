package com.prestashopemc.view;

import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.prestashopemc.R;
import com.prestashopemc.controller.LoginController;
import com.prestashopemc.model.ContactValues;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CommonValidation;
import com.prestashopemc.util.MyApplication;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>Contact Us Activity!</h1>
 * The Contact Us Activity implements get contact details such as email and phone number.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class ContactUsActivity extends BaseActivity {

    private TextView mNameLabel, mEmailLabel, mPhoneLabel, mCommentLabel, mSendBtn;
    private EditText mNameEdit, mEmailEdit, mPhoneEdit, mCommentEdit;
    private LoginController mLoginController;
    private RelativeLayout mMainLayout;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        MyApplication.getDefaultTracker("Contact Us Screen");
        initToolBar();
        initNetworkError();
        logoImage.setVisibility(View.GONE);
        searchIcon.setVisibility(View.GONE);
        cartIcon.setVisibility(View.GONE);
        cartText.setVisibility(View.GONE);
        title.setVisibility(View.VISIBLE);
        title.setText(AppConstants.getTextString(this, AppConstants.contactUsText));
        title.setAllCaps(true);
        mMainLayout = (RelativeLayout) findViewById(R.id.main_layout);
        showProgDialiog();
        initializedLayout();
    }

    /**
     * This method is used to Initialized Layout
     */
    private void initializedLayout() {

        boolean isInternet = isInternetOn(this);
        if (!isInternet) {
            mMainLayout.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initializedLayout();
                }
            });
            hideProgDialog();

        } else {
            mMainLayout.setVisibility(View.VISIBLE);
            networkLayout.setVisibility(View.GONE);
            hideProgDialog();
            mNameLabel = (TextView) findViewById(R.id.name_label);
            mEmailLabel = (TextView) findViewById(R.id.email_label);
            mPhoneLabel = (TextView) findViewById(R.id.phone_label);
            mCommentLabel = (TextView) findViewById(R.id.comment_label);
            mNameEdit = (EditText) findViewById(R.id.name_edit);
            mEmailEdit = (EditText) findViewById(R.id.email_edit);
            mPhoneEdit = (EditText) findViewById(R.id.phone_edit);
            mCommentEdit = (EditText) findViewById(R.id.comment_edit);
            mSendBtn = (TextView) findViewById(R.id.send_btn);
            mSendBtn.setOnClickListener(this);
            mLoginController = new LoginController(ContactUsActivity.this);
            dynamicTextValue();
            setCustomFont();

            mCommentEdit.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (v.getId() == R.id.comment_edit) {
                        v.getParent().requestDisallowInterceptTouchEvent(true);
                        switch (event.getAction() & MotionEvent.ACTION_MASK) {
                            case MotionEvent.ACTION_UP:
                                v.getParent().requestDisallowInterceptTouchEvent(false);
                                break;
                        }
                    }
                    return false;
                }
            });
        }

    }

    /**
     * This method is used to Dynamic Text Value Settings
     */
    private void dynamicTextValue() {
        mSendBtn.setText(AppConstants.getTextString(this, AppConstants.sendText));
        mCommentLabel.setText(AppConstants.getTextString(this, AppConstants.commentsText));
        mPhoneLabel.setText(AppConstants.getTextString(this, AppConstants.phoneText));
        mEmailLabel.setText(AppConstants.getTextString(this, AppConstants.emailText));
        mNameLabel.setText(AppConstants.getTextString(this, AppConstants.nameText));
    }

    /**
     * This method is used to Set Custom Font
     */
    private void setCustomFont() {
        mNameLabel.setTypeface(robotoMedium);
        mEmailLabel.setTypeface(robotoMedium);
        mPhoneLabel.setTypeface(robotoMedium);
        mCommentLabel.setTypeface(robotoMedium);
        mNameEdit.setTypeface(robotoLight);
        mEmailEdit.setTypeface(robotoLight);
        mPhoneEdit.setTypeface(robotoLight);
        mCommentEdit.setTypeface(robotoLight);
        mSendBtn.setTypeface(robotoMedium);
    }

    /**
     * This method is used to Success Response from API
     *
     * @param result This is the parameter to contactUsSuccess method.
     */
    public void contactUsSuccess(String result) {
        MyApplication.eventTracking("contact us details", "contact us details send button clicked", "contact us details send");
        hideProgDialog();
        Toast.makeText(ContactUsActivity.this, result, Toast.LENGTH_LONG).show();
        finish();
    }

    /**
     * This method is used to Failure Response From API
     *
     * @param errormsg This is the parameter to contactUsFailure method.
     */
    public void contactUsFailure(String errormsg) {
        hideProgDialog();
        snackBar(errormsg);
    }

    /**
     * This method is used to on click function.
     *
     * @param v This is the parameter to onClick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * Send Click Listener*/
            case R.id.send_btn:
                MyApplication.eventTracking("contact us details", "contact us details send button clicked", "contact us details send");
                showProgDialiog();
                ContactValues contactValues = new ContactValues();
                contactValues.setContactName(AppConstants.stringEncode(mNameEdit.getText().toString()));
                contactValues.setContactEmail(mEmailEdit.getText().toString());
                contactValues.setContactPhone(mPhoneEdit.getText().toString());
                contactValues.setContactComent(AppConstants.stringEncode(mCommentEdit.getText().toString()));

                CommonValidation validation = new CommonValidation();
                boolean status = validation.contactUsValidation(ContactUsActivity.this, contactValues);
                if (status) {
                    showProgDialiog();
                    mLoginController.contactUs(contactValues);
                } else {
                    hideProgDialog();
                }
                break;
        }
    }
}
