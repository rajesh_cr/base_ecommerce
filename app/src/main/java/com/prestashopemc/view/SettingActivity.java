package com.prestashopemc.view;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.adapter.CurrencyAdapter;
import com.prestashopemc.adapter.LanguageAdapter;
import com.prestashopemc.controller.LoginController;
import com.prestashopemc.database.CurrencyService;
import com.prestashopemc.database.LanguageService;
import com.prestashopemc.model.Currency;
import com.prestashopemc.model.Language;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.MyApplication;

import java.util.ArrayList;
import java.util.List;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>Setting Activity!</h1>
 * The Setting Activity implements chat details.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class SettingActivity extends BaseActivity {

    private TextView mAlertLabel, mOthersLabel, mPushNotificationLabel, mLangLabel, mCurrencyLabel, mSelectLang,
            mSelectCurrency, mSelectedText;
    private SwitchCompat mPushSwitch;
    private LinearLayout mLangLayout, mCurrencyLayout, mUpLayout, mDownLayout, mPushLayout;
    private RelativeLayout mSettingMainLayout;
    private View mHiddenPanel, mLineView;
    private RecyclerView mLangCurrencyList;
    /**
     * The Cancel btn.
     */
    public Button cancelBtn;
    private LanguageAdapter mLanguageAdapter;
    private LanguageService mLanguageService;
    private List<Language> mLanguages = new ArrayList<>();
    private CurrencyService mCurrencyService;
    private List<Currency> mCurrency = new ArrayList<>();
    private CurrencyAdapter mCurrencyAdapter;
    private boolean mIsPush = false;
    private LoginController mLoginController;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     *
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        MyApplication.getDefaultTracker("Settings Screen");
        initToolBar();
        initNetworkError();
        logoImage.setVisibility(View.GONE);
        searchIcon.setVisibility(View.GONE);
        cartIcon.setVisibility(View.GONE);
        cartText.setVisibility(View.GONE);
        title.setVisibility(View.VISIBLE);
        title.setText(AppConstants.getTextString(this, AppConstants.settingsText));
        title.setAllCaps(true);
        mSettingMainLayout = (RelativeLayout) findViewById(R.id.setting_layout);
        showProgDialiog();
        initializedLayout();
    }

    /**
     * This method is used to Initialized Layout
     */
    private void initializedLayout() {

        boolean isInternet = isInternetOn(this);
        if (!isInternet) {
            mSettingMainLayout.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initializedLayout();
                }
            });
            hideProgDialog();

        } else {
            mSettingMainLayout.setVisibility(View.VISIBLE);
            networkLayout.setVisibility(View.GONE);
            hideProgDialog();
            mLoginController = new LoginController(this);
            mLanguageService = new LanguageService(this);
            mCurrencyService = new CurrencyService(this);
            mAlertLabel = (TextView) findViewById(R.id.alert_label);
            mOthersLabel = (TextView) findViewById(R.id.others_label);
            mPushNotificationLabel = (TextView) findViewById(R.id.push_notification_label);
            mLangLabel = (TextView) findViewById(R.id.lang_label);
            mCurrencyLabel = (TextView) findViewById(R.id.currency_label);
            mSelectLang = (TextView) findViewById(R.id.lang_selected);
            mSelectCurrency = (TextView) findViewById(R.id.currency_selected);
            mPushSwitch = (SwitchCompat) findViewById(R.id.switch_compat);
            mLangLayout = (LinearLayout) findViewById(R.id.lang_layout);
            mCurrencyLayout = (LinearLayout) findViewById(R.id.currency_layout);
            mLineView = (View) findViewById(R.id.alert_line_view);
            mPushLayout = (LinearLayout) findViewById(R.id.push_notification_layout);
            dynamicTextValue();
            setCustomFont();
            mHiddenPanel = findViewById(R.id.hidden_layout);
            mUpLayout = (LinearLayout) findViewById(R.id.up_layout);
            mDownLayout = (LinearLayout) findViewById(R.id.down_layout);
            mSelectedText = (TextView) findViewById(R.id.selected_label);
            mLangCurrencyList = (RecyclerView) findViewById(R.id.lang_list_view);
            mLangCurrencyList.setLayoutManager(new LinearLayoutManager(this));
            cancelBtn = (Button) findViewById(R.id.hidden_cancel);
            mLangLayout.setOnClickListener(this);
            mCurrencyLayout.setOnClickListener(this);
            mUpLayout.setOnClickListener(this);
            cancelBtn.setOnClickListener(this);

            if (SharedPreference.getInstance().getValue("customerid").equals("0")) {
                mAlertLabel.setVisibility(View.GONE);
                mLineView.setVisibility(View.GONE);
                mPushLayout.setVisibility(View.GONE);
            }
            /**
             * Getting Languages And Currency From DB*/
            try {
                mLanguages = mLanguageService.retrieveLangList();
                mCurrency = mCurrencyService.retrieveCurrencyList();
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (SharedPreference.getInstance().getBoolean("isPush")) {
                mIsPush = SharedPreference.getInstance().getBoolean("isPush");
                if (mIsPush) {
                    CustomBackground.setSwitchColor(mPushSwitch, true);
                    mPushSwitch.setChecked(true);
                } else {
                    CustomBackground.setSwitchColor(mPushSwitch, false);
                    mPushSwitch.setChecked(false);
                }
            }

            mPushSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    showProgDialiog();
                    boolean push = mPushSwitch.isChecked();
                    String mPush = "";
                    if (push) {
                        mPush = "1";
                        CustomBackground.setSwitchColor(mPushSwitch, true);
                    } else {
                        mPush = "0";
                        CustomBackground.setSwitchColor(mPushSwitch, false);
                    }
                    mLoginController.pushEnableDisable(mPush);
                }
            });

            setLangCurrencyName();
        }
    }

    /**
     * This method is used to Dynamic Text Value Settings
     */
    private void dynamicTextValue() {
        mAlertLabel.setText(AppConstants.getTextString(this, AppConstants.alertText));
        mPushNotificationLabel.setText(AppConstants.getTextString(this, AppConstants.pushNotificationText));
        mOthersLabel.setText(AppConstants.getTextString(this, AppConstants.othersText));
        mLangLabel.setText(AppConstants.getTextString(this, AppConstants.languageText));
        mCurrencyLabel.setText(AppConstants.getTextString(this, AppConstants.currencyText));
    }

    /**
     * This method is used to Set Currency & Language Names
     */
    private void setLangCurrencyName() {
        String defaultLang = SharedPreference.getInstance().getValue("id_language");
        String defaultCurrency = SharedPreference.getInstance().getValue("id_currency");

        if (!SharedPreference.getInstance().getValue("language_name").equals("0")){
            mSelectLang.setText(SharedPreference.getInstance().getValue("language_name"));
        }else {
            for (int i = 0; i<mLanguages.size(); i++) {
                if (mLanguages.get(i).getIdLang().equals(defaultLang)) {
                    mSelectLang.setText(mLanguages.get(i).getName());
                }
            }
        }
        if (!SharedPreference.getInstance().getValue("currency_name").equals("0")){
            mSelectCurrency.setText(SharedPreference.getInstance().getValue("currency_name"));
        }else {
            for (int i = 0; i<mCurrency.size(); i++) {
                if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                    if (mCurrency.get(i).getIsoCode().equals(defaultCurrency)) {
                        mSelectCurrency.setText(mCurrency.get(i).getName());
                    }
                }else {
                    if (mCurrency.get(i).getIdCurrency().equals(defaultCurrency)) {
                        mSelectCurrency.setText(mCurrency.get(i).getName());
                    }
                }
            }
        }

    }

    /**
     * This method is used to Set Custom Font
     */
    public void setCustomFont() {
        mAlertLabel.setTypeface(robotoMedium);
        mOthersLabel.setTypeface(robotoMedium);
        mPushNotificationLabel.setTypeface(robotoRegular);
        mLangLabel.setTypeface(robotoRegular);
        mCurrencyLabel.setTypeface(robotoRegular);
        mSelectLang.setTypeface(robotoLight);
        mSelectCurrency.setTypeface(robotoLight);
    }

    /**
     * This method is used to Hidden Layout animation
     *
     * @param view          This is the first parameter to slideUpDown method.
     * @param mLangCurrency This is the second parameter to slideUpDown method.
     */
    public void slideUpDown(final View view, String mLangCurrency) {
        if (!isPanelShown()) {
            // Show the panel
            if (mLangCurrency.length() > 0) {
                Animation bottomUp = AnimationUtils.loadAnimation(this, R.anim.bottom_up);

                mHiddenPanel.startAnimation(bottomUp);
                mHiddenPanel.setVisibility(View.VISIBLE);
                mSettingMainLayout.setAlpha((float) 0.3);
                mSelectedText.setText(mLangCurrency);
                mSelectedText.setTypeface(robotoMedium);
                cancelBtn.setTypeface(robotoLight);
                if (mLangCurrency.contains(getString(R.string.languageText))) {
                    mLanguageAdapter = new LanguageAdapter(SettingActivity.this, mLanguages);
                    mLangCurrencyList.setAdapter(mLanguageAdapter);
                } else if (mLangCurrency.contains(getString(R.string.currencyText))) {
                    mCurrencyAdapter = new CurrencyAdapter(SettingActivity.this, mCurrency);
                    mLangCurrencyList.setAdapter(mCurrencyAdapter);
                }

            }
        } else {
            // Hide the Panel
            setLangCurrencyName();
            Animation bottomDown = AnimationUtils.loadAnimation(this, R.anim.bottom_down);

            mHiddenPanel.startAnimation(bottomDown);
            mHiddenPanel.setVisibility(View.GONE);
            mSettingMainLayout.setAlpha((float) 1);
        }
    }

    /**
     * This method is used to Panel View method
     *
     * @return boolean returns boolean for isPanelShown method.
     */
    private boolean isPanelShown() {
        return mHiddenPanel.getVisibility() == View.VISIBLE;
    }

    /**
     * This method is used to Push enable and disable response
     *
     * @param result  This is the first parameter to success method.
     * @param message This is the second parameter to success method.
     */
    public void success(Boolean result, String message) {
        hideProgDialog();
        if (result) {
            SharedPreference.getInstance().saveBoolean("isPush", result);
            snackBar(message);
        } else {
            SharedPreference.getInstance().saveBoolean("isPush", result);
            snackBar(message);
        }
    }

    /**
     * This method is used to Failure response
     *
     * @param result This is the parameter to failure method.
     */
    public void failure(String result) {
        hideProgDialog();
        snackBar(result);
    }

    /**
     * This method is used to on click action.
     *
     * @param v This is the parameter to onClick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * Language Click Listener*/
            case R.id.lang_layout:
                slideUpDown(v, getString(R.string.languageText));
                break;

            /**
             * Currency Click Listener*/
            case R.id.currency_layout:
                slideUpDown(v, getString(R.string.currencyText));
                break;

            /**
             * Close Hidden Layout*/
            case R.id.up_layout:
                slideUpDown(v, "");
                if (cancelBtn.getText().toString().equals("OK")) {
                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(getIntent());
                    overridePendingTransition(0, 0);
                }
                break;

            /**
             * Cancel Hidden Layout*/
            case R.id.hidden_cancel:
                slideUpDown(v, "");
                if (cancelBtn.getText().toString().equals("OK")) {
                    finish();
                    overridePendingTransition(0, 0);
                    startActivity(getIntent());
                    overridePendingTransition(0, 0);
                }
                break;
        }
    }
}
