package com.prestashopemc.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.adapter.ChangeAddressAdapter;
import com.prestashopemc.controller.CheckoutController;
import com.prestashopemc.model.ChangeAddressMainModel;
import com.prestashopemc.model.ChangeAddressModel;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.util.MyApplication;

import java.util.ArrayList;
import java.util.List;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>Change Address Activity!</h1>
 * The Change Address Activity implements an Change address for login and guest user.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author Rajesh
 * @version 1.2
 * @since 29 /9/16
 */
public class ChangeAddress extends BaseActivity implements View.OnClickListener {

    private RecyclerView mAddresslist;
    private TextView mNewAddressLable, mSelectAddressLable, mSelectAddress;
    private ChangeAddressAdapter mChangeAddressAdapter;
    private LinearLayoutManager mLinearLayoutManager;
    private List<ChangeAddressModel> mChageAddressModel;
    private CheckoutController mCheckoutController;
    private Bundle mBundle;
    private String mIntentValue;
    private RelativeLayout mMainLayout;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_address);
        MyApplication.getDefaultTracker("Change Address Screen");
        initToolBar();
        initNetworkError();
        mCheckoutController = new CheckoutController(this);
        logoImage.setVisibility(View.GONE);
        searchIcon.setVisibility(View.VISIBLE);
        cartIcon.setVisibility(View.VISIBLE);
        cartText.setVisibility(View.VISIBLE);
        title.setVisibility(View.VISIBLE);
        title.setText(AppConstants.getTextString(this, AppConstants.changeAddressText));
        title.setAllCaps(true);
        mMainLayout = (RelativeLayout) findViewById(R.id.main_layout);
        mBundle = getIntent().getExtras();
        cartCount();
        showProgDialiog();
        initialized();
        loadIntent();
    }

    /**
     * This method is used to restart activity.
     * Default Method for Activity creation.
     */
    @Override
    protected void onRestart() {
        super.onRestart();
        refreshActivity();
    }

    /**
     * This method is used to Load Intent Value
     */
    private void loadIntent() {
        if (mBundle != null) {
            mIntentValue = mBundle.getString("addressSelect");

            boolean isInternet = isInternetOn(this);
            if (!isInternet) {
                mMainLayout.setVisibility(View.GONE);
                networkLayout.setVisibility(View.VISIBLE);
                networkTry.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showProgDialiog();
                        loadIntent();
                    }
                });
                hideProgDialog();

            } else {
                mMainLayout.setVisibility(View.VISIBLE);
                networkLayout.setVisibility(View.GONE);
                mCheckoutController.addressList();
            }
        }
    }

    /**
     * This method is used to Guest Checkout
     */
    private void guestCheckout() {
        ChangeAddressModel changeAddressModel = new ChangeAddressModel();
        changeAddressModel.setFirstname(SharedPreference.getInstance().getValue("fname"));
        changeAddressModel.setLastname(SharedPreference.getInstance().getValue("lname"));
        changeAddressModel.setStreet(SharedPreference.getInstance().getValue("street"));
        changeAddressModel.setPostcode(SharedPreference.getInstance().getValue("zipcode"));
        changeAddressModel.setCity(SharedPreference.getInstance().getValue("city"));
        changeAddressModel.setCountry(SharedPreference.getInstance().getValue("country"));
        changeAddressModel.setTelephone(SharedPreference.getInstance().getValue("phone"));
        ArrayList<ChangeAddressModel> changeAddressModels = new ArrayList<ChangeAddressModel>();
        changeAddressModels.add(changeAddressModel);
        //mChageAddressModel.add(changeAddressModel);
        hideProgDialog();
        mChangeAddressAdapter = new ChangeAddressAdapter(ChangeAddress.this, changeAddressModels);
        mAddresslist.setAdapter(mChangeAddressAdapter);
    }

    /**
     * This method is used to Initialized Layout
     */
    private void initialized() {
        mAddresslist = (RecyclerView) findViewById(R.id.change_recycler_view);
        mNewAddressLable = (TextView) findViewById(R.id.change_address);
        mSelectAddressLable = (TextView) findViewById(R.id.change_select);
        mSelectAddress = (TextView) findViewById(R.id.proceedTopayment);
        CustomBackground.setBackgroundText(mSelectAddress);
        mLinearLayoutManager = new LinearLayoutManager(this);
        mAddresslist.setLayoutManager(mLinearLayoutManager);
        mAddresslist.setHasFixedSize(true);
        mNewAddressLable.setOnClickListener(this);
        mSelectAddress.setOnClickListener(this);
        dynamicTextValue();
        setCustomFont();
    }

    /**
     * This method is used to Dynamic Text Value Settings
     */
    private void dynamicTextValue() {
        mSelectAddress.setText(AppConstants.getTextString(this, AppConstants.changeAddressSelectText));
        mSelectAddressLable.setText(AppConstants.getTextString(this, AppConstants.changeAddressSelectText));
        mNewAddressLable.setText(AppConstants.getTextString(this, AppConstants.addNewAddressText));
    }

    /**
     * This method is used to Address List Success Response
     *
     * @param mChangeAddressMainModel This is the parameter used in updateAddressList method.
     */
    public void updateAddressList(ChangeAddressMainModel mChangeAddressMainModel) {
        mChageAddressModel = mChangeAddressMainModel.getAddress();
        hideProgDialog();
        mChangeAddressAdapter = new ChangeAddressAdapter(ChangeAddress.this, mChageAddressModel);
        mAddresslist.setAdapter(mChangeAddressAdapter);
    }

    /**
     * This method is used to Address list Failure Response
     *
     * @param errormsg This is parameter to failure method.
     */
    public void failure(String errormsg) {
        hideProgDialog();
        if (errormsg.equalsIgnoreCase(getString(R.string.noAddressText))) {
            finish();
        } else {
            snackBar(errormsg);
        }
    }

    /**
     * This method is used to Set Custom Font
     */
    private void setCustomFont() {
        mNewAddressLable.setTypeface(robotoLight);
        mSelectAddressLable.setTypeface(robotoMedium);
        mSelectAddress.setTypeface(robotoMedium);
    }

    /**
     * This method is used to Delete Address Success Response
     */
    public void successDelete() {
        mCheckoutController = new CheckoutController(this);
        mCheckoutController.addressList();
        hideProgDialog();
        mChangeAddressAdapter.notifyDataSetChanged();
        snackBar(AppConstants.getTextString(this, AppConstants.newAddressDeletetext));
    }

    /**
     * This method is used to Refresh activity
     */
    public void refreshActivity() {
        finish();
        startActivity(getIntent());
    }

    /**
     * This method is used to onClick function.
     * @param v This is the parameter to onClick method.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            /**
             * New Address page Listener*/
            case R.id.change_address:
                Intent newaddress = new Intent(ChangeAddress.this, AddNewAddress.class);
                newaddress.putExtra("fromDefault", "newAddress");
                startActivity(newaddress);
                break;

            /**
             * Select Address page Listener
             */
            case R.id.proceedTopayment:
                String name = mChangeAddressAdapter.mName;
                String address = mChangeAddressAdapter.mAddress;
                String idAddress = mChangeAddressAdapter.mIdAddress;
                if (name == null) {
                    snackBar(AppConstants.getTextString(ChangeAddress.this, AppConstants.changeAddressSelectText));
                } else {
                    Intent selectAddress = new Intent(ChangeAddress.this, CheckoutActivity.class);
                    selectAddress.putExtra("name", name);
                    selectAddress.putExtra("address", address);
                    selectAddress.putExtra("addressSelect", mIntentValue);
                    selectAddress.putExtra("idAddress", idAddress);
                    startActivity(selectAddress);
                    finish();
                }
                break;
        }
    }
}
