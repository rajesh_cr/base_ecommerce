package com.prestashopemc.view;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.crashlytics.android.Crashlytics;
import com.prestashopemc.R;
import com.prestashopemc.customView.CustomTextView;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.CustomIcon;
import com.prestashopemc.util.SharedPreference;
import com.squareup.picasso.Picasso;
import com.zopim.android.sdk.api.ZopimChat;
import com.zopim.android.sdk.model.VisitorInfo;
import com.zopim.android.sdk.prechat.PreChatForm;
import com.zopim.android.sdk.prechat.ZopimChatActivity;

import io.fabric.sdk.android.Fabric;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>Base Activity!</h1>
 * This Base Activity implements
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 14 /3/16
 */
public class BaseActivity extends AppCompatActivity implements View.OnClickListener {


    public Toolbar toolbar;
    public ImageView cartIcon, searchIcon, logoImage, notificationImage;
    public CustomTextView categoryTitle, productCount, clearAll, title, cartText;
    public ProgressDialog progressDialog;
    public CoordinatorLayout coordinatorLayout;
    public Typeface robotoBold, robotoLight, robotoMedium, robotoRegular, opensansRegular, proximanovaRegular, dinproRegular,
        dinproBold, dinMedium, proximanovaAltRegular, openSansCondBold, robotoItalic, timesNewRoman, verdana, comicSansMs,
        wildWest, bedRock, latoBold, latoItalic, latoRegular, openSansBold, openSansItalic, oxygenBold, oxygenItalic, oxygenRegular,
        ralewayBold, ralewayItalic, ralewayRegular;
    public TextView networkTitle, networkMsg, networkTry;
    public RelativeLayout networkLayout;
    private String mZopimKey;
    public FloatingActionButton chatButton;
    private Bitmap mFinalBitmap;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     *
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /**
     * This method is used to Initiate Toolbar method.
     *
     * @return Nothing.
     */
    public void initToolBar() {
        Fabric.with(this, new Crashlytics());
        setFont();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        cartIcon = (ImageView) findViewById(R.id.cart_icon);
        searchIcon = (ImageView) findViewById(R.id.search_icon);
        logoImage = (ImageView) findViewById(R.id.logo_image);
        notificationImage = (ImageView) findViewById(R.id.notification_icon);
        categoryTitle = (CustomTextView) findViewById(R.id.category_title);
        productCount = (CustomTextView) findViewById(R.id.product_count);
        title = (CustomTextView) findViewById(R.id.title);
        clearAll = (CustomTextView) findViewById(R.id.clear_all_text);
        cartText = (CustomTextView) findViewById(R.id.cart_text);
        CustomBackground.setCartCircle(cartText);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        AppConstants.setBackgroundColor(toolbar);
        logoImage.setVisibility(View.VISIBLE);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinatorLayout);
        floatingButton();
        textColors();
        iconColors();

        /**
         * This method is used to cart click listener.
         * @return Nothing.
         */
        cartIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BaseActivity.this, CartActivity.class);
                startActivity(intent);
            }
        });

        /**
         * This method is used to search click listener.
         * @return Nothing.
         */
        searchIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent recentSearchIntent = new Intent(BaseActivity.this, RecentSearchActivity.class);
                startActivity(recentSearchIntent);
                finish();
            }
        });
    }

    /**
     * This method is used to Text Color changes
     *
     * @return Nothing.
     */
    public void textColors() {
        if (!SharedPreference.getInstance().getValue("text_color").equals("0")) {
            title.setTextColor(Color.parseColor(CustomBackground.mTitleTextColor));
            categoryTitle.setTextColor(Color.parseColor(CustomBackground.mTitleTextColor));
            productCount.setTextColor(Color.parseColor(CustomBackground.mTitleTextColor));
            //cartText.setTextColor(Color.parseColor(CustomBackground.mTitleTextColor));
        }
    }

    /**
     * This method is used to Toolbar Icon Color changes
     *
     * @return Nothing.
     */
    public void iconColors() {
        if (!SharedPreference.getInstance().getValue("tint_color").equals("0")) {
            //final Drawable upArrow = new BitmapDrawable(getResources(), customIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_left)));
            //getSupportActionBar().setHomeAsUpIndicator(upArrow);
            cartIcon.setImageBitmap(customIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_cart)));
            searchIcon.setImageBitmap(customIcon(BitmapFactory.decodeResource(getResources(), R.drawable.ic_search)));
            notificationImage.setImageBitmap(customIcon(BitmapFactory.decodeResource(getResources(), R.drawable.home_notification)));
        }
    }

    /**
     * This method is used to Custom Icon
     *
     * @param bitmap This is the parameter used in custom icon.
     * @return Nothing. bitmap
     */
    public Bitmap customIcon(Bitmap bitmap) {
        Drawable sourceDrawable = new BitmapDrawable(getResources(), bitmap);
        Bitmap sourceBitmap = CustomIcon.convertDrawableToBitmap(sourceDrawable);
        mFinalBitmap = CustomIcon.changeImageColor(sourceBitmap, Color.parseColor(CustomBackground.mTintColor));
        return mFinalBitmap;
    }

    /**
     * This method is used to Chat Floating Action Button
     *
     * @return Nothing.
     */
    private void floatingButton() {
        chatButton = (FloatingActionButton) findViewById(R.id.chat_fab);
        chatButton.setVisibility(View.GONE);
        chatButton.setSize(FloatingActionButton.SIZE_NORMAL);
        if (!SharedPreference.getInstance().getValue("theme_color1").equals("0")) {
            chatButton.setBackgroundTintList(ColorStateList.valueOf(Color.parseColor(CustomBackground.mThemeColor)));
        } else {
            chatButton.setBackgroundTintList(ColorStateList.valueOf(Color.RED));
        }
        chatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PreChatForm preChatForm = new PreChatForm.Builder()
                        .name(PreChatForm.Field.REQUIRED_EDITABLE)
                        .email(PreChatForm.Field.REQUIRED_EDITABLE)
                        .phoneNumber(PreChatForm.Field.REQUIRED_EDITABLE)
                        .department(PreChatForm.Field.REQUIRED_EDITABLE)
                        .message(PreChatForm.Field.REQUIRED_EDITABLE)
                        .build();

                // build chat config
                ZopimChat.SessionConfig config = new ZopimChat.SessionConfig().preChatForm(preChatForm);

                // start chat activity with config
                ZopimChatActivity.startActivity(BaseActivity.this, config);

                // Sample breadcrumb
                ZopimChat.trackEvent("Started chat with mandatory pre-chat form");
            }
        });
    }

    /**
     * This method is used to Network error layout appear
     *
     * @return Nothing.
     */
    public void initNetworkError() {
        networkLayout = (RelativeLayout) findViewById(R.id.no_internet_layout);
        networkTitle = (TextView) findViewById(R.id.no_internet_title);
        networkMsg = (TextView) findViewById(R.id.no_internet_msg);
        networkTry = (TextView) findViewById(R.id.no_internet_try);
        boolean isInternet = isInternetOn(this);
        if (isInternet) {
            networkTitle.setText(AppConstants.getTextString(this, AppConstants.notAbleToConnectText));
            networkMsg.setText(AppConstants.getTextString(this, AppConstants.pleaseCheckYourInternetConnectionText));
            networkTry.setText(AppConstants.getTextString(this, AppConstants.tryAgainText));
        }
        networkTitle.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/robotomedium.ttf"));
        networkMsg.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/robotolight.ttf"));
        networkTry.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/robotomedium.ttf"));
    }

    /**
     * This method is used to find item information which is selected in menu.
     *
     * @param item This is the parameter used in onOptionsItemSelected method
     * @return Nothing.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();

        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * This method is used to find item information which is selected in menu.
     *
     * @param activityName This is the first parameter used in activityLaunch method
     * @param categoryId   This is the second parameter used in activityLaunch method
     * @param currentPage  This is the third parameter used in activityLaunch method
     * @return Nothing.
     */
    public void activityLaunch(String activityName, String categoryId, String currentPage) {
        Intent intent = new Intent();
        intent.setClassName(BaseActivity.this, "com.subzero.view." + activityName);
        intent.putExtra("category_id", categoryId);
        intent.putExtra("currentpage", currentPage);
        startActivity(intent);
    }


    /**
     * This is the Method to show progress dialog(loader)
     *
     * @return Nothing.
     */
    public void showProgDialiog() {
        progressDialog = new ProgressDialog(this);
        int size = AppConstants.sProgStack.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                ProgressDialog dia = AppConstants.sProgStack.get(i);
                if (dia != null && dia.isShowing()) {
                    try {
                        dia.dismiss();
                    } catch (Exception e) {
                    }
                }
            }
            AppConstants.sProgStack.clear();
        }
        if (progressDialog != null && !progressDialog.isShowing()) {
            progressDialog.show();
            progressDialog.setContentView(R.layout.activity_progress_bar);
            progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            progressDialog.setCancelable(false);
            AppConstants.sProgStack.add(progressDialog);
        }
    }

    /**
     * This Method to hide progress dialog(loader)
     *
     * @return Nothing.
     */
    public static void hideProgDialog() {
        int size = AppConstants.sProgStack.size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                ProgressDialog dia = AppConstants.sProgStack.get(i);
                if (dia != null && dia.isShowing()) {
                    try {
                        dia.dismiss();
                    } catch (Exception e) {
                    }
                }
            }
            AppConstants.sProgStack.clear();
        }
    }

    /**
     * This method is used to Load Logo.
     *
     * @return Nothing.
     */
    public void loadLogo() {
        if (SharedPreference.getInstance().getValue("logo").equals("0") ||
                SharedPreference.getInstance().getValue("logo").contains("No Logo Image is Available")) {
            Picasso.with(this).load(R.drawable.elitelogo)
                    .placeholder(R.drawable.place_holder).resize(150, 60)
                    .into(logoImage);
        }else {
            Picasso.with(this).load(SharedPreference.getInstance().getValue("logo"))
                    .placeholder(R.drawable.place_holder).resize(150, 60)
                    .into(logoImage);
        }
    }

    /**
     * This method is used to Cart Count display
     *
     * @return Nothing.
     */
    public void cartCount() {
        SharedPreference mPref = SharedPreference.getInstance();
        if (mPref.getValue("cart_count").equalsIgnoreCase("0")) {
            cartText.setVisibility(View.GONE);
        } else {
            cartText.setVisibility(View.VISIBLE);
            cartText.setText(SharedPreference.getInstance().getValue("cart_count"));
        }
    }

    /**
     * This method is used to Snack Bar display
     *
     * @param response This is the parameter used to snackBar method.
     * @return Nothing.
     */
    public void snackBar(String response) {
        final Snackbar snackbar = Snackbar.make(coordinatorLayout, response, Snackbar.LENGTH_SHORT);
        final View snackBarView = snackbar.getView();
        AppConstants.setBackgroundColor(snackBarView);
        AppConstants.setBackgroundColor(snackBarView);
        //To hide soft keyboard When snackbar displays
        final InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        snackBarView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

            /**
             * This method is used to hide soft keyboard When snackbar displays
             * @return Nothing.
             */
            @Override
            public void onGlobalLayout() {
                Rect r = new Rect();
                snackBarView.getWindowVisibleDisplayFrame(r);
                int screenHeight = snackBarView.getRootView().getHeight();
                // r.bottom is the position above soft keypad or device button.
                // if keypad is shown, the r.bottom is smaller than that before.
                int keypadHeight = screenHeight - r.bottom;
                if (keypadHeight > screenHeight * 0.15) { // 0.15 ratio is perhaps enough to determine keypad height.
                    // keyboard is opened
                    im.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
                } else {
                    // keyboard is closed
                }
            }
        });
        TextView tv = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        if(CustomBackground.mTitleTextColor.equals("#0")){
            tv.setTextColor(Color.parseColor("#FFFFFF"));
        }else {
            tv.setTextColor(Color.parseColor(CustomBackground.mTitleTextColor));
        }
        snackbar.show();
    }


    /**
     * This method is used to Set Font.
     *
     * @return Nothing.
     */
    public void setFont() {
        robotoLight = Typeface.createFromAsset(getAssets(), "fonts/robotolight.ttf");
        robotoBold = Typeface.createFromAsset(getAssets(), "fonts/robotobold.ttf");
        robotoMedium = Typeface.createFromAsset(getAssets(), "fonts/robotomedium.ttf");
        robotoRegular = Typeface.createFromAsset(getAssets(), "fonts/robotoregular.ttf");
        robotoItalic = Typeface.createFromAsset(getAssets(), "fonts/robotoitalic.ttf");
        proximanovaRegular = Typeface.createFromAsset(getAssets(), "fonts/proximanovaregular.otf");
        opensansRegular = Typeface.createFromAsset(getAssets(), "fonts/opensansregular.ttf");
        dinMedium = Typeface.createFromAsset(getAssets(), "fonts/dinmedium.ttf");
        dinproBold = Typeface.createFromAsset(getAssets(), "fonts/dinprobold.otf");
        dinproRegular = Typeface.createFromAsset(getAssets(), "fonts/dinproregular.otf");
        proximanovaAltRegular = Typeface.createFromAsset(getAssets(), "fonts/proximanovaaltregular.otf");
        openSansCondBold = Typeface.createFromAsset(getAssets(), "fonts/opensanscondbold.ttf");
        timesNewRoman = Typeface.createFromAsset(getAssets(), "fonts/timesnewroman.ttf");
        verdana = Typeface.createFromAsset(getAssets(), "fonts/verdana.ttf");
        comicSansMs = Typeface.createFromAsset(getAssets(), "fonts/comicsansmsbold.ttf");
        wildWest = Typeface.createFromAsset(getAssets(), "fonts/wildwestwind.ttf");
        bedRock = Typeface.createFromAsset(getAssets(), "fonts/bedrock.ttf");
        latoBold = Typeface.createFromAsset(getAssets(), "fonts/latobold.ttf");
        latoRegular = Typeface.createFromAsset(getAssets(), "fonts/latoregular.ttf");
        latoItalic = Typeface.createFromAsset(getAssets(), "fonts/latoitalic.ttf");
        openSansBold = Typeface.createFromAsset(getAssets(), "fonts/opensansbold.ttf");
        openSansItalic = Typeface.createFromAsset(getAssets(), "fonts/opensansitalic.ttf");
        oxygenBold = Typeface.createFromAsset(getAssets(), "fonts/oxygenbold.ttf");
        oxygenItalic = Typeface.createFromAsset(getAssets(), "fonts/oxygenitalic.otf");
        oxygenRegular = Typeface.createFromAsset(getAssets(), "fonts/oxygenregular.ttf");
        ralewayBold = Typeface.createFromAsset(getAssets(), "fonts/ralewaybold.ttf");
        ralewayRegular = Typeface.createFromAsset(getAssets(), "fonts/ralewayregular.ttf");
        ralewayItalic = Typeface.createFromAsset(getAssets(), "fonts/ralewayitalic.ttf");
    }

    /**
     * This is the method to Delete Specific value from SharedPreference
     *
     * @return Nothing.
     */
    public void deleteValue() {
        SharedPreference.getInstance().removeValue("checkout");
        SharedPreference.getInstance().removeValue("shipping_name");
        SharedPreference.getInstance().removeValue("shipping_address");
        SharedPreference.getInstance().removeValue("shipping_id");
        SharedPreference.getInstance().removeValue("billing_name");
        SharedPreference.getInstance().removeValue("billing_address");
        SharedPreference.getInstance().removeValue("billing_id");
    }

    /**
     * This is the method to Clear Cart ID from SharedPreference
     *
     * @return Nothing.
     */
    public void cartIdDelete() {
        SharedPreference.getInstance().removeValue("cart_id");
        SharedPreference.getInstance().removeValue("cart_count");
    }

    /**
     * This is the method to Clear Customer Credentials From SharedPreference
     *
     * @return Nothing.
     */
    public void deleteCredentials() {
        SharedPreference.getInstance().removeValue("customer");
        SharedPreference.getInstance().removeValue("customerid");
        SharedPreference.getInstance().removeValue("sessionid");
        SharedPreference.getInstance().removeValue("cart_id");
        SharedPreference.getInstance().removeValue("cart_count");
        SharedPreference.getInstance().removeValue("social_profile_id");
        SharedPreference.getInstance().removeValue("google_profile_id");
    }

    /**
     * This is the method to Clear Guest Address details
     *
     * @return Nothing.
     */
    public void deleteGuestDetails() {
        SharedPreference.getInstance().removeValue("shippingAddressValues");
        SharedPreference.getInstance().removeValue("billingAddressValues");
        SharedPreference.getInstance().removeValue("shippingCountryId");
        SharedPreference.getInstance().removeValue("billingCountryId");
        SharedPreference.getInstance().removeValue("checkout");
    }

    /**
     * This is the method Zopim Account Init
     *
     * @return Nothing.
     */
    public void zopimAccountKey() {
        mZopimKey = SharedPreference.getInstance().getValue("zopim_id");
        ZopimChat.trackEvent("Application created");
        ZopimChat.init(mZopimKey);
        VisitorInfo emptyVisitorInfo = new VisitorInfo.Builder().build();
        ZopimChat.setVisitorInfo(emptyVisitorInfo);
    }

    /**
     * This is the method to on click
     *
     * @param v This is the parameter to onClick method.
     * @return Nothing.
     */
    @Override
    public void onClick(View v) {
    }
}
