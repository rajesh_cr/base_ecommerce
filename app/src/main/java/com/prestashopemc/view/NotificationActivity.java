package com.prestashopemc.view;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.RelativeLayout;

import com.prestashopemc.R;
import com.prestashopemc.adapter.NotificationAdapter;
import com.prestashopemc.controller.LoginController;
import com.prestashopemc.model.NotificationMain;
import com.prestashopemc.model.NotificationResult;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.MyApplication;

import java.util.List;

import static com.prestashopemc.util.InternetCheck.isInternetOn;

/**
 * <h1>Notification Activity!</h1>
 * The Notification Activity implements notification details.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class NotificationActivity extends BaseActivity {

    private RecyclerView mNotificationList;
    private NotificationAdapter mNotificationAdapter;
    private List<NotificationResult> mNotificationResult;
    private LoginController mLoginController;
    private RelativeLayout mMainLayout;

    /**
     * This method is used to create activity.
     * Default Method for Activity creation.
     * @param savedInstanceState This is the parameter to oncreate method
     * @return Nothing.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);
        MyApplication.getDefaultTracker("Notification Screen");
        initToolBar();
        initNetworkError();
        logoImage.setVisibility(View.GONE);
        searchIcon.setVisibility(View.GONE);
        cartIcon.setVisibility(View.GONE);
        cartText.setVisibility(View.GONE);
        title.setVisibility(View.VISIBLE);
        title.setText(AppConstants.getTextString(this, AppConstants.notificationText));
        title.setAllCaps(true);
        mMainLayout = (RelativeLayout) findViewById(R.id.main_layout);
        showProgDialiog();
        initializedLayout();
    }

    /**
     * This method is used to Initialized Layout*/
    private void initializedLayout() {

        boolean isInternet = isInternetOn(this);
        if(!isInternet)
        {
            mMainLayout.setVisibility(View.GONE);
            networkLayout.setVisibility(View.VISIBLE);
            networkTry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    initializedLayout();
                }
            });
            hideProgDialog();

        }
        else {
            mMainLayout.setVisibility(View.VISIBLE);
            networkLayout.setVisibility(View.GONE);
            //hideProgDialog();
            mLoginController = new LoginController(this);
            mNotificationList = (RecyclerView) findViewById(R.id.notification_list_view);
            mNotificationList.setLayoutManager(new LinearLayoutManager(this));
            mLoginController.notificationList();
        }
    }

    /**
     * This method is used to Notification List Success Response
     *
     * @param notificationMain This is the parameter to success method.
     */
    public void success(NotificationMain notificationMain) {
        hideProgDialog();
        mNotificationResult = notificationMain.getResult();
        mNotificationAdapter = new NotificationAdapter(NotificationActivity.this, mNotificationResult);
        mNotificationList.setAdapter(mNotificationAdapter);
    }

    /**
     * This method is used to Notification List Failure Response
     *
     * @param errormsg This is the parameter to failure method.
     */
    public void failure(String errormsg) {
        hideProgDialog();
        snackBar(errormsg);
    }
}
