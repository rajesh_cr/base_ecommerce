
package com.prestashopemc.webModel;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * <h1>Web Product!</h1>
 * The Web Product is used to implements getter and setter for keys in Web Product table.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

@DatabaseTable(tableName = "WebProduct")
public class WebProduct {

    @DatabaseField
    private String slotPosition;
    @DatabaseField
    @SerializedName("name")
    @Expose
    private String name;
    @DatabaseField
    @SerializedName("id_product")
    @Expose
    private Integer idProduct;

    /**
     * Gets slot position.
     *
     * @return the slot position
     */
    public String getSlotPosition() {
        return slotPosition;
    }

    /**
     * Sets slot position.
     *
     * @param slotPosition the slot position
     */
    public void setSlotPosition(String slotPosition) {
        this.slotPosition = slotPosition;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets id product.
     *
     * @return the id product
     */
    public Integer getIdProduct() {
        return idProduct;
    }

    /**
     * Sets id product.
     *
     * @param idProduct the id product
     */
    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }
}
