
package com.prestashopemc.webModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * <h1>Image Border!</h1>
 * The Image border is used to implements getter and setter for keys in Image border table.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

@DatabaseTable(tableName = "Imageborder")
public class ImageBorder {

    @DatabaseField
    private String slotPosition;

    @DatabaseField
    @SerializedName("color")
    @Expose
    private String color;
    @DatabaseField
    @SerializedName("style_type")
    @Expose
    private String styleType;
    @DatabaseField
    @SerializedName("type")
    @Expose
    private String type;
    @DatabaseField
    @SerializedName("width")
    @Expose
    private String width;

    /**
     * Gets slot position.
     *
     * @return the slot position
     */
    public String getSlotPosition() {
        return slotPosition;
    }

    /**
     * Sets slot position.
     *
     * @param slotPosition the slot position
     */
    public void setSlotPosition(String slotPosition) {
        this.slotPosition = slotPosition;
    }

    /**
     * Gets color.
     *
     * @return the color
     */
    public String getColor() {
        return color;
    }

    /**
     * Sets color.
     *
     * @param color the color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * Gets style type.
     *
     * @return the style type
     */
    public String getStyleType() {
        return styleType;
    }

    /**
     * Sets style type.
     *
     * @param styleType the style type
     */
    public void setStyleType(String styleType) {
        this.styleType = styleType;
    }

    /**
     * Gets type.
     *
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets type.
     *
     * @param type the type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets width.
     *
     * @return the width
     */
    public String getWidth() {
        return width;
    }

    /**
     * Sets width.
     *
     * @param width the width
     */
    public void setWidth(String width) {
        this.width = width;
    }

}
