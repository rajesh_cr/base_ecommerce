
package com.prestashopemc.webModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * <h1>Sub title!</h1>
 * The Sub title is used to implements getter and setter for keys in Sub title table.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

@DatabaseTable(tableName = "Subtitle")
public class SubTitle {

    @DatabaseField
    private String slotPosition;

    @DatabaseField
    private int positionId;

    @DatabaseField
    @SerializedName("code")
    @Expose
    private String code;
    @DatabaseField
    @SerializedName("value")
    @Expose
    private String value;

    /**
     * Gets position id.
     *
     * @return the position id
     */
    public int getPositionId() {
        return positionId;
    }

    /**
     * Sets position id.
     *
     * @param positionId the position id
     */
    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    /**
     * Gets slot position.
     *
     * @return the slot position
     */
    public String getSlotPosition() {
        return slotPosition;
    }

    /**
     * Sets slot position.
     *
     * @param slotPosition the slot position
     */
    public void setSlotPosition(String slotPosition) {
        this.slotPosition = slotPosition;
    }

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param code the code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets value.
     *
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets value.
     *
     * @param value the value
     */
    public void setValue(String value) {
        this.value = value;
    }

}
