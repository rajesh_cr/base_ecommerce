
package com.prestashopemc.webModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Title!</h1>
 * The Title is used to implements getter and setter for keys in Title table.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

@DatabaseTable(tableName = "Title")
public class Title {

    @DatabaseField
    private String slotPosition;

    @DatabaseField
    private int positionId;
    @DatabaseField
    @SerializedName("background-color")
    @Expose
    private String backgroundColor;
    @DatabaseField
    @SerializedName("textcolor")
    @Expose
    private String textcolor;
    @DatabaseField
    @SerializedName("textsize")
    @Expose
    private String textsize;
    @DatabaseField
    @SerializedName("texttype")
    @Expose
    private String texttype;
    @DatabaseField
    @SerializedName("textalign")
    @Expose
    private String textalign;
    @SerializedName("title")
    @Expose
    private List<SubTitle> title = new ArrayList<>();
    @DatabaseField
    @SerializedName("is_bold")
    @Expose
    private Boolean isBold;
    @DatabaseField
    @SerializedName("is_italic")
    @Expose
    private Boolean isItalic;
    @DatabaseField
    @SerializedName("is_underline")
    @Expose
    private Boolean isUnderline;

    /**
     * Gets postion id.
     *
     * @return the postion id
     */
    public int getPostionId() {
        return positionId;
    }

    /**
     * Sets postion id.
     *
     * @param postionId the postion id
     */
    public void setPostionId(int postionId) {
        this.positionId = postionId;
    }

    /**
     * Gets slot position.
     *
     * @return the slot position
     */
    public String getSlotPosition() {
        return slotPosition;
    }

    /**
     * Sets slot position.
     *
     * @param slotPosition the slot position
     */
    public void setSlotPosition(String slotPosition) {
        this.slotPosition = slotPosition;
    }

    /**
     * Gets textsize.
     *
     * @return the textsize
     */
    public String getTextsize() {
        return textsize;
    }

    /**
     * Sets textsize.
     *
     * @param textsize the textsize
     */
    public void setTextsize(String textsize) {
        this.textsize = textsize;
    }

    /**
     * Gets is italic.
     *
     * @return the is italic
     */
    public Boolean getIsItalic() {
        return isItalic;
    }

    /**
     * Sets is italic.
     *
     * @param isItalic the is italic
     */
    public void setIsItalic(Boolean isItalic) {
        this.isItalic = isItalic;
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    public List<SubTitle> getTitle() {
        return title;
    }

    /**
     * Sets title.
     *
     * @param title the title
     */
    public void setTitle(List<SubTitle> title) {
        this.title = title;
    }

    /**
     * Gets is underline.
     *
     * @return the is underline
     */
    public Boolean getIsUnderline() {
        return isUnderline;
    }

    /**
     * Sets is underline.
     *
     * @param isUnderline the is underline
     */
    public void setIsUnderline(Boolean isUnderline) {
        this.isUnderline = isUnderline;
    }

    /**
     * Gets textalign.
     *
     * @return the textalign
     */
    public String getTextalign() {
        return textalign;
    }

    /**
     * Sets textalign.
     *
     * @param textalign the textalign
     */
    public void setTextalign(String textalign) {
        this.textalign = textalign;
    }

    /**
     * Gets is bold.
     *
     * @return the is bold
     */
    public Boolean getIsBold() {
        return isBold;
    }

    /**
     * Sets is bold.
     *
     * @param isBold the is bold
     */
    public void setIsBold(Boolean isBold) {
        this.isBold = isBold;
    }

    /**
     * Gets textcolor.
     *
     * @return the textcolor
     */
    public String getTextcolor() {
        return textcolor;
    }

    /**
     * Sets textcolor.
     *
     * @param textcolor the textcolor
     */
    public void setTextcolor(String textcolor) {
        this.textcolor = textcolor;
    }

    /**
     * Gets texttype.
     *
     * @return the texttype
     */
    public String getTexttype() {
        return texttype;
    }

    /**
     * Sets texttype.
     *
     * @param texttype the texttype
     */
    public void setTexttype(String texttype) {
        this.texttype = texttype;
    }

    /**
     * Gets background color.
     *
     * @return the background color
     */
    public String getBackgroundColor() {
        return backgroundColor;
    }

    /**
     * Sets background color.
     *
     * @param backgroundColor the background color
     */
    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

}
