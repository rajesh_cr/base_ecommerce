
package com.prestashopemc.webModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * <h1>Link!</h1>
 * The Link is used to implements getter and setter for keys in Link table.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

@DatabaseTable(tableName = "Link")
public class Link {

    @DatabaseField
    private String slotPosition;
    @DatabaseField
    @SerializedName("has_link")
    @Expose
    private Boolean hasLink;
    @DatabaseField
    @SerializedName("link_type")
    @Expose
    private String linkType;

    @SerializedName("category")
    @Expose
    private WebCategory category;

    @SerializedName("product")
    @Expose
    private WebProduct product;
    @DatabaseField
    @SerializedName("keyword")
    @Expose
    private String keyword;

    /**
     * Gets slot position.
     *
     * @return the slot position
     */
    public String getSlotPosition() {
        return slotPosition;
    }

    /**
     * Sets slot position.
     *
     * @param slotPosition the slot position
     */
    public void setSlotPosition(String slotPosition) {
        this.slotPosition = slotPosition;
    }

    /**
     * Gets category.
     *
     * @return the category
     */
    public WebCategory getCategory() {
        return category;
    }

    /**
     * Sets category.
     *
     * @param category the category
     */
    public void setCategory(WebCategory category) {
        this.category = category;
    }

    /**
     * Gets product.
     *
     * @return the product
     */
    public WebProduct getProduct() {
        return product;
    }

    /**
     * Sets product.
     *
     * @param product the product
     */
    public void setProduct(WebProduct product) {
        this.product = product;
    }

    /**
     * Gets has link.
     *
     * @return the has link
     */
    public Boolean getHasLink() {
        return hasLink;
    }

    /**
     * Sets has link.
     *
     * @param hasLink the has link
     */
    public void setHasLink(Boolean hasLink) {
        this.hasLink = hasLink;
    }

    /**
     * Gets keyword.
     *
     * @return the keyword
     */
    public String getKeyword() {
        return keyword;
    }

    /**
     * Sets keyword.
     *
     * @param keyword the keyword
     */
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    /**
     * Gets link type.
     *
     * @return the link type
     */
    public String getLinkType() {
        return linkType;
    }

    /**
     * Sets link type.
     *
     * @param linkType the link type
     */
    public void setLinkType(String linkType) {
        this.linkType = linkType;
    }

}
