
package com.prestashopemc.webModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * <h1>Web Category!</h1>
 * The Web Category is used to implements getter and setter for keys in Web Category table.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
@DatabaseTable(tableName = "WebCategory")
public class WebCategory {

    @DatabaseField
    private String slotPosition;
    @DatabaseField
    @SerializedName("product_count")
    @Expose
    private Integer productCount;
    @DatabaseField
    @SerializedName("category_count")
    @Expose
    private Integer categoryCount;
    @DatabaseField
    @SerializedName("id_category")
    @Expose
    private String categoryId;
    @DatabaseField
    @SerializedName("name")
    @Expose
    private String categoryName;

    /**
     * Gets slot position.
     *
     * @return the slot position
     */
    public String getSlotPosition() {
        return slotPosition;
    }

    /**
     * Sets slot position.
     *
     * @param slotPosition the slot position
     */
    public void setSlotPosition(String slotPosition) {
        this.slotPosition = slotPosition;
    }

    /**
     * Gets product count.
     *
     * @return the product count
     */
    public Integer getProductCount() {
        return productCount;
    }

    /**
     * Sets product count.
     *
     * @param productCount the product count
     */
    public void setProductCount(Integer productCount) {
        this.productCount = productCount;
    }

    /**
     * Gets category count.
     *
     * @return the category count
     */
    public Integer getCategoryCount() {
        return categoryCount;
    }

    /**
     * Sets category count.
     *
     * @param categoryCount the category count
     */
    public void setCategoryCount(Integer categoryCount) {
        this.categoryCount = categoryCount;
    }

    /**
     * Gets category id.
     *
     * @return the category id
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * Sets category id.
     *
     * @param categoryId the category id
     */
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * Gets category name.
     *
     * @return the category name
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * Sets category name.
     *
     * @param categoryName the category name
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

}
