
package com.prestashopemc.webModel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * <h1>Image List !</h1>
 * The Image List is used to implements getter and setter for keys in Image List table.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
@DatabaseTable(tableName = "Imagelist")
public class ImageList {

    @DatabaseField
    private String slotPosition;

    @DatabaseField
    private int positionId;

    @DatabaseField
    @SerializedName("medium_image_url")
    @Expose
    private String mediumImageUrl;
    @DatabaseField
    @SerializedName("big_image_url")
    @Expose
    private String bigImageUrl;
    @SerializedName("title")
    @Expose
    private Title title;
    @SerializedName("link")
    @Expose
    private Link link;

    /**
     * Gets position id.
     *
     * @return the position id
     */
    public int getPositionId() {
        return positionId;
    }

    /**
     * Sets position id.
     *
     * @param positionId the position id
     */
    public void setPositionId(int positionId) {
        this.positionId = positionId;
    }

    /**
     * Gets slot position.
     *
     * @return the slot position
     */
    public String getSlotPosition() {
        return slotPosition;
    }

    /**
     * Sets slot position.
     *
     * @param slotPosition the slot position
     */
    public void setSlotPosition(String slotPosition) {
        this.slotPosition = slotPosition;
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    public Title getTitle() {
        return title;
    }

    /**
     * Sets title.
     *
     * @param title the title
     */
    public void setTitle(Title title) {
        this.title = title;
    }

    /**
     * Gets link.
     *
     * @return the link
     */
    public Link getLink() {
        return link;
    }

    /**
     * Sets link.
     *
     * @param link the link
     */
    public void setLink(Link link) {
        this.link = link;
    }

    /**
     * Gets medium image url.
     *
     * @return the medium image url
     */
    public String getMediumImageUrl() {
        return mediumImageUrl;
    }

    /**
     * Sets medium image url.
     *
     * @param mediumImageUrl the medium image url
     */
    public void setMediumImageUrl(String mediumImageUrl) {
        this.mediumImageUrl = mediumImageUrl;
    }

    /**
     * Gets big image url.
     *
     * @return the big image url
     */
    public String getBigImageUrl() {
        return bigImageUrl;
    }

    /**
     * Sets big image url.
     *
     * @param bigImageUrl the big image url
     */
    public void setBigImageUrl(String bigImageUrl) {
        this.bigImageUrl = bigImageUrl;
    }

}
