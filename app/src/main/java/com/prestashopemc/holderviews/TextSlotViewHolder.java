package com.prestashopemc.holderviews;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.adapter.RecyclerViewHolders;

/**
 * <h1>Text Slot View Holder!</h1>
 * The Text Slot View Holder is used to create view holder for Text Slot Adapter.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class TextSlotViewHolder extends RecyclerViewHolders {

    /**
     * The M text main layout.
     */
    public RelativeLayout mTextMainLayout, /**
     * The M text content layout.
     */
    mTextContentLayout;
    /**
     * The M text content.
     */
    public TextView mTextContent;

    /**
     * Instantiates a new Text slot view holder.
     *
     * @param v the v
     */
    public TextSlotViewHolder(View v){
        super(v);
        this.mTextMainLayout = (RelativeLayout) v.findViewById(R.id.text_content_main_layout);
        this.mTextContentLayout = (RelativeLayout) v.findViewById(R.id.text_content_layout);
        this.mTextContent = (TextView) v.findViewById(R.id.text_content);
    }
}
