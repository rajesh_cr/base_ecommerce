package com.prestashopemc.holderviews;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.prestashopemc.R;
import com.prestashopemc.adapter.RecyclerViewHolders;

/**
 * <h1>Second Slot View Holder!</h1>
 * The Second Slot View Holder is used to create view holder for Second Slot Adapter.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class SecondSlotViewHolder extends RecyclerViewHolders {

    /**
     * The M second slot recycler view.
     */
    public RecyclerView mSecondSlotRecyclerView;

    /**
     * Instantiates a new Second slot view holder.
     *
     * @param v the v
     */
    public SecondSlotViewHolder(View v) {
        super(v);
        this.mSecondSlotRecyclerView = (RecyclerView) v.findViewById(R.id.second_slot_recycler_view);

    }
}
