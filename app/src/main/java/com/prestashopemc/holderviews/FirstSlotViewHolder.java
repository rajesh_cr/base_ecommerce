package com.prestashopemc.holderviews;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.prestashopemc.R;
import com.prestashopemc.adapter.RecyclerViewHolders;

/**
 * <h1>First Slot View Holder!</h1>
 * The First Slot View Holder is used to create view holder for First Slot Adapter.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class FirstSlotViewHolder extends RecyclerViewHolders {

    /**
     * The M first slot recycler view.
     */
    public RecyclerView mFirstSlotRecyclerView;
    //public TextView mSlotTitle;

    /**
     * Instantiates a new First slot view holder.
     *
     * @param v the v
     */
    public FirstSlotViewHolder(View v) {
        super(v);
        this.mFirstSlotRecyclerView = (RecyclerView) v.findViewById(R.id.first_slot_recycler_view);
        //this.mSlotTitle = (TextView) v.findViewById(R.id.main_title);
    }
}
