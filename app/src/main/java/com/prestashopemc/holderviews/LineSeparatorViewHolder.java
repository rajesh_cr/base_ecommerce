package com.prestashopemc.holderviews;

import android.view.View;
import android.widget.RelativeLayout;

import com.prestashopemc.R;
import com.prestashopemc.adapter.RecyclerViewHolders;

/**
 * <h1>Line Separator  View Holder!</h1>
 * The Line Separator  View Holder is used to create view holder for Line Separator  Adapter.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class LineSeparatorViewHolder extends RecyclerViewHolders {

    /**
     * The M space seperator layout.
     */
    public RelativeLayout mSpaceSeperatorLayout, /**
     * The M sapce seperator main layout.
     */
    mSapceSeperatorMainLayout;
    /**
     * The M view.
     */
    public View mView;

    /**
     * Instantiates a new Line separator view holder.
     *
     * @param v the v
     */
    public LineSeparatorViewHolder(View v){
        super(v);

        //this.mSpaceSeperatorLayout = (RelativeLayout) v.findViewById(R.id.line_seperator_layout);
        this.mView = (View) v.findViewById(R.id.line_seperator_layout);
        //this.mSapceSeperatorMainLayout = (RelativeLayout) v.findViewById(R.id.line_seperator_main_layout);
    }
}
