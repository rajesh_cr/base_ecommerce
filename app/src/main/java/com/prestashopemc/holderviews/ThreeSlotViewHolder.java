package com.prestashopemc.holderviews;

import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.prestashopemc.R;
import com.prestashopemc.adapter.RecyclerViewHolders;

/**
 * <h1>Three Slot View Holder!</h1>
 * The Three Slot View Holder is used to create view holder for Three Slot Adapter.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class ThreeSlotViewHolder extends RecyclerViewHolders {

    /**
     * The M third slot recycler view.
     */
    public RecyclerView mThirdSlotRecyclerView;

    /**
     * Instantiates a new Three slot view holder.
     *
     * @param v the v
     */
    public ThreeSlotViewHolder(View v) {
        super(v);
        this.mThirdSlotRecyclerView = (RecyclerView) v.findViewById(R.id.third_slot_recycler_view);

    }
}
