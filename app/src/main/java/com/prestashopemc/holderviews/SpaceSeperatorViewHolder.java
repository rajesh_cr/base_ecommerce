package com.prestashopemc.holderviews;

import android.view.View;
import android.widget.RelativeLayout;

import com.prestashopemc.R;
import com.prestashopemc.adapter.RecyclerViewHolders;

/**
 * <h1>Space Seperator View Holder!</h1>
 * The Space Seperator View Holder is used to create view holder for Space Seperator Adapter.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class SpaceSeperatorViewHolder extends RecyclerViewHolders {

    /**
     * The M space seperator layout.
     */
    public RelativeLayout mSpaceSeperatorLayout, /**
     * The M sapce seperator main layout.
     */
    mSapceSeperatorMainLayout;

    /**
     * Instantiates a new Space seperator view holder.
     *
     * @param v the v
     */
    public SpaceSeperatorViewHolder(View v){
        super(v);
        this.mSpaceSeperatorLayout = (RelativeLayout) v.findViewById(R.id.space_seperator_layout);
        //this.mSapceSeperatorMainLayout = (RelativeLayout) v.findViewById(R.id.space_seperator_main_layout);
    }
}
