package com.prestashopemc.holderviews;

import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.adapter.RecyclerViewHolders;

/**
 * <h1>Title Slot View Holder!</h1>
 * The Title Slot View Holder is used to create view holder for Title Slot Adapter.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class TitleSlotViewHolder extends RecyclerViewHolders {

    /**
     * The M title slot layout.
     */
    public RelativeLayout mTitleSlotLayout, /**
     * The M title main layout.
     */
    mTitleMainLayout;
    /**
     * The M title slot.
     */
    public TextView mTitleSlot;

    /**
     * Instantiates a new Title slot view holder.
     *
     * @param v the v
     */
    public TitleSlotViewHolder(View v){
        super(v);
        this.mTitleSlot = (TextView) v.findViewById(R.id.web_title_text);
        this.mTitleMainLayout = (RelativeLayout) v.findViewById(R.id.web_main_title_layout);
        this.mTitleSlotLayout = (RelativeLayout) v.findViewById(R.id.web_title_layout);
    }
}
