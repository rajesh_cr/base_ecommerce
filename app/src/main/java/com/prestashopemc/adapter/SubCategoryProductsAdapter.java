package com.prestashopemc.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.model.CategoryProduct;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.view.ProductDetailActivity;
import com.prestashopemc.view.SubCategoryActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by egrove on 6/5/17.
 */
public class SubCategoryProductsAdapter extends RecyclerView.Adapter<SubCategoryProductsAdapter.SubcategoeyProductsHolder> {

    private Context mContext;
    private CategoryProduct mCategoryProduct;
    private List<CategoryProduct> mCategoryProducts;
    private SubCategoryActivity mSubCategoryActivity;

    public SubCategoryProductsAdapter(Context context, List<CategoryProduct> categoryProducts){
        this.mContext = context;
        this.mCategoryProducts = categoryProducts;
    }

    @Override
    public SubcategoeyProductsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        mSubCategoryActivity = (SubCategoryActivity) mContext;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.subcategory_productlist_layout, null);

        SubcategoeyProductsHolder viewHolder = new SubcategoeyProductsHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(SubcategoeyProductsHolder holder, final int position) {
        String currenySymbol = SharedPreference.getInstance().getValue("currency_symbol");
        mCategoryProduct = mCategoryProducts.get(position);
        holder.mProductName.setTypeface(mSubCategoryActivity.robotoRegular);
        holder.mProductDescription.setTypeface(mSubCategoryActivity.robotoRegular);
        holder.mProductPrice.setTypeface(mSubCategoryActivity.robotoMedium);
        holder.mProductSplPrice.setTypeface(mSubCategoryActivity.robotoMedium);

        Picasso.with(mContext).load(mCategoryProduct.getImageUrl())
                .placeholder(R.drawable.place_holder).resize(265, 265)
                .into(holder.mProductImage);
        holder.mProductName.setText(mCategoryProduct.getProductName());
        holder.mProductDescription.setText(mCategoryProduct.getDescriptionShort());
        holder.mProductPrice.setText(mCategoryProduct.getPrice());
        CustomBackground.setRectangleBack(holder.mCardViewLayout);
        /**
         * Set Price and Spl price */
        if (!mCategoryProduct.getSpecialprice().equalsIgnoreCase(mContext.getString(R.string.not_set)) &&
                mCategoryProduct.getSpecialprice()!= null){
            holder.mProductPrice.setText(currenySymbol + " " + mCategoryProduct.getSpecialprice());
        }else {
            holder.mProductSplPrice.setVisibility(View.GONE);
            holder.mProductPrice.setText(currenySymbol + " " + mCategoryProduct.getPrice());
        }
        if (mCategoryProduct.getPrice()!= null){
            if (!mCategoryProduct.getPrice().equalsIgnoreCase(mContext.getString(R.string.not_set))){
                holder.mProductSplPrice.setText(currenySymbol + " " + mCategoryProduct.getPrice());
            }
        }

        holder.mCardViewLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCategoryProduct = mCategoryProducts.get(position);
                Intent filterIntent = new Intent(mContext, ProductDetailActivity.class);
                filterIntent.putExtra("product_id", "" + mCategoryProduct.getProductId());
                filterIntent.putExtra("product_name", mCategoryProduct.getProductName());
                filterIntent.putExtra("model", mCategoryProduct);
                mContext.startActivity(filterIntent);
            }
        });
        holder.mProductSplPrice.setPaintFlags(holder.mProductSplPrice
                .getPaintFlags()
                | Paint.STRIKE_THRU_TEXT_FLAG);
    }

    @Override
    public int getItemCount() {
        return this.mCategoryProducts.size();
    }

    public class SubcategoeyProductsHolder extends RecyclerViewHolders{

        public ImageView mProductImage;
        public TextView mProductName, mProductPrice, mProductSplPrice, mProductDescription;
        public CardView mCardViewLayout;

        /**
         * Instantiates a new Recycler view holders.
         *
         * @param itemView the item view
         */
        public SubcategoeyProductsHolder(View itemView) {
            super(itemView);
            mProductImage = (ImageView) itemView.findViewById(R.id.product_image);
            mProductName = (TextView) itemView.findViewById(R.id.subcategory_product_name);
            mProductDescription = (TextView) itemView.findViewById(R.id.subcategory_product_description);
            mProductPrice = (TextView) itemView.findViewById(R.id.subcategory_product_price);
            mProductSplPrice = (TextView) itemView.findViewById(R.id.subcategory_product_spl_price);
            mCardViewLayout = (CardView) itemView.findViewById(R.id.subcategory_card_view);
        }
    }
}
