package com.prestashopemc.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>View Pager Adapter Contains View Pager Holder</h1>
 * The View Pager Adapter implements an adapter view for View Pager.
 * <p>
 * <b>Note:</b> Adapter values are get from FragmentList model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {
    private final List<Fragment> mFragmentList = new ArrayList<>();
    private final List<String> mFragmentTitleList = new ArrayList<>();

    /**
     * Instantiates a new View pager adapter.
     *
     * @param manager the manager
     */
    public ViewPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    /**
     * @param position the position
     * @return
     */
    @Override
    public Fragment getItem(int position) {
        return mFragmentList.get(position);
    }

    /**
     * @return
     */
    @Override
    public int getCount() {
        return mFragmentList.size();
    }

    /**
     * Add fragment.
     *
     * @param fragment the fragment
     * @param title    the title
     */
    public void addFragment(Fragment fragment, String title) {
        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);

    }

    /**
     * Add frag.
     *
     * @param fragment the fragment
     */
    public void addFrag(Fragment fragment){
        mFragmentList.add(fragment);
    }

    /**
     * @param position the position
     * @return
     */
    @Override
    public CharSequence getPageTitle(int position) {
        return mFragmentTitleList.get(position);
    }


}
