package com.prestashopemc.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.prestashopemc.view.ProductDetailActivity;
import com.prestashopemc.view.ProductListActivity;
import com.prestashopemc.view.SearchActivity;
import com.prestashopemc.view.SubCategoryActivity;
import com.squareup.picasso.Picasso;
import com.prestashopemc.R;
import com.prestashopemc.model.Imagelist;

import java.util.List;

/**
 * <h1>Small Banner Adapter Contains Small Banner Holder</h1>
 * The Small Banner Adapter implements an adapter view for Small Banners.
 * <p>
 * <b>Note:</b> Adapter values are get from Image list model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class SmallBannerAdapter extends RecyclerView.Adapter<SmallBannerAdapter.SmallBannerHolder> {
    private List<Imagelist> subBannerList;
    private Context context;
    private Imagelist mSubBannerObj;
    private String bannerName;

    /**
     * Instantiates a new Small banner adapter.
     *
     * @param context       the context
     * @param subBannerList the sub banner list
     * @param bannerName    the banner name
     */
    public SmallBannerAdapter(Context context, List<Imagelist> subBannerList, String bannerName) {
        this.subBannerList = subBannerList;
        this.context = context;
        this.bannerName = bannerName;
    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public SmallBannerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.small_banner_adapter, null);
        WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        int height = windowManager.getDefaultDisplay().getHeight();
        view.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.MATCH_PARENT));
        SmallBannerHolder viewHolder = new SmallBannerHolder(view);
        return viewHolder;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(SmallBannerHolder holder, final int position) {

        mSubBannerObj = subBannerList.get(position);


//        if (bannerName.isEmpty()){
//            holder.mBannerName.setVisibility(View.GONE);
//        }else{
//            holder.mBannerName.setVisibility(View.VISIBLE);
//            holder.mBannerName.setText(bannerName);
//        }
            Picasso.with(context).load(mSubBannerObj.getImageUrl())
                    .placeholder(R.drawable.place_holder).fit()
                    .into(holder.mBannerImage);


        holder.mBannerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mSubBannerObj = subBannerList.get(position);

                Log.e("isType", mSubBannerObj.getIsType() + "");
                if (mSubBannerObj.getIsType()) {
                    Log.e("get Type", mSubBannerObj.getType() + "");
                    if (mSubBannerObj.getType().equalsIgnoreCase("product")) {
                        Intent in = new Intent(context, ProductDetailActivity.class);
                        in.putExtra("product_id", mSubBannerObj.getProductId());
                        in.putExtra("product_name",mSubBannerObj.getProductName());
                        context.startActivity(in);
                    } else if (mSubBannerObj.getType().equalsIgnoreCase("category")) {

                        if (String.valueOf(mSubBannerObj.getCategoryCount()).equalsIgnoreCase("0")) {

                            Intent intent = new Intent(context, ProductListActivity.class);
                            intent.putExtra("categoryid", mSubBannerObj.getCategoryId());
                            intent.putExtra("categoryName", mSubBannerObj.getCategoryName());
                            intent.putExtra("productCount", mSubBannerObj.getProductCount()+"");
                            context.startActivity(intent);
                        } else {
                            Intent intent = new Intent(context, SubCategoryActivity.class);
                            intent.putExtra("sCategoriesId", mSubBannerObj.getCategoryId());
                            intent.putExtra("categoryName", mSubBannerObj.getCategoryName());
                            context.startActivity(intent);
                        }
                    } else {
                        Intent intent = new Intent(context, SearchActivity.class);
                        intent.putExtra("search_text", mSubBannerObj.getSearchWord());
                        context.startActivity(intent);
                    }

                }

            }
        });


    }


    /**
     * The type Small banner holder.
     */
    public class SmallBannerHolder extends RecyclerViewHolders {

        public ImageView mBannerImage;
        public TextView mBannerName;


        /**
         * Instantiates a new Small banner holder.
         *
         * @param v the v
         */
        public SmallBannerHolder(View v) {
            super(v);
            this.mBannerImage = (ImageView) v.findViewById(R.id.banner_slot_two_image);
            this.mBannerName = (TextView) v.findViewById(R.id.banner_name);

        }
    }


    /**
     * @return
     */
    @Override
    public int getItemCount() {
        return this.subBannerList.size();
    }

}
