package com.prestashopemc.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.prestashopemc.R;
import com.prestashopemc.model.Imagelist;

import java.util.List;

/**
 * <h1>Home Page Banner Contains Home Page Holder</h1>
 * The Home Page Banner implements an adapter view for home page banners.
 * <p>
 * <b>Note:</b> Adapter values are get from Image list model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class HomePageBanner extends RecyclerView.Adapter<HomePageBanner.HomePageHolder> {

    private List<Imagelist> imagelists;
    private Context context;
    private Imagelist imagelist;

    /**
     * Instantiates a new Home page banner.
     *
     * @param context    the context
     * @param imagelists the imagelists
     */
    public HomePageBanner(Context context, List<Imagelist> imagelists){

        this.context = context;
        this.imagelists = imagelists;

    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public HomePageHolder onCreateViewHolder(ViewGroup parent, int viewType){
        View view = null;

        view = LayoutInflater.from(context).inflate(R.layout.home_banner, null);
        WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        int height = windowManager.getDefaultDisplay().getHeight();
        view.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.MATCH_PARENT));
        HomePageHolder homePageHolder = new HomePageHolder(view);
        return homePageHolder;
    }

    /**
     * @param homePageHolder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(HomePageHolder homePageHolder, int position){
        imagelist = imagelists.get(position);

        Log.e("ImageUrl", imagelist.getImageUrl() + "");
        Picasso.with(context).load(imagelist.getImageUrl())
                .placeholder(R.drawable.place_holder).fit()
                .into(homePageHolder.imageView);

    }

    /**
     * The type Home page holder.
     */
    public class HomePageHolder extends RecyclerViewHolders{

        public ImageView imageView;

        /**
         * Instantiates a new Home page holder.
         *
         * @param v the v
         */
        public HomePageHolder(View v){
            super(v);
            this.imageView = (ImageView)v.findViewById(R.id.banner_slot_image);
        }
    }

    /**
     * @return int the size of image list array
     */
    @Override
    public int getItemCount(){
        return imagelists.size();
    }

}
