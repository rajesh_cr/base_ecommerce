package com.prestashopemc.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import com.prestashopemc.R;
import com.prestashopemc.model.Imagelist;
import com.prestashopemc.view.ProductDetailActivity;
import com.prestashopemc.view.ProductListActivity;
import com.prestashopemc.view.SearchActivity;
import com.prestashopemc.view.SubCategoryActivity;
import com.squareup.picasso.Picasso;
import java.util.List;

/**
 * <h1>Square Banner Adapter Contains Square Banner Holder</h1>
 * The Square Banner Adapter implements an adapter view for Square Banners.
 * <p>
 * <b>Note:</b> Adapter values are get from Image list model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class SquareBannerAdapter extends RecyclerView.Adapter<SquareBannerAdapter.SquareBannerHolder> {
    private List<Imagelist> subBannerList;
    private Context context;
    private Imagelist mSubBannerObj;
    private String bannerName;

    /**
     * Instantiates a new Square banner adapter.
     *
     * @param context       the context
     * @param subBannerList the sub banner list
     * @param bannerName    the banner name
     */
    public SquareBannerAdapter(Context context, List<Imagelist> subBannerList, String bannerName) {
        this.subBannerList = subBannerList;
        this.context = context;
        this.bannerName = bannerName;
    }


    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public SquareBannerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.square_banner_adapter, null);
        /*WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        int height = windowManager.getDefaultDisplay().getHeight();*/
        //view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.MATCH_PARENT));
        SquareBannerHolder viewHolder = new SquareBannerHolder(view);
        return viewHolder;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(SquareBannerHolder holder, final int position) {

        mSubBannerObj = subBannerList.get(position);

//        if (bannerName.isEmpty()){
//            holder.mBannerName.setVisibility(View.GONE);
//        }else{
//            holder.mBannerName.setVisibility(View.VISIBLE);
//            holder.mBannerName.setText(bannerName);
//        }

        Log.e("ImageUrl", mSubBannerObj.getImageUrl() + "");
        Picasso.with(context).load(mSubBannerObj.getImageUrl())
                .placeholder(R.drawable.place_holder).fit()
                .into(holder.mBannerImage);

        holder.mBannerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSubBannerObj = subBannerList.get(position);
                Log.e("isType", mSubBannerObj.getIsType() + "");
                if(mSubBannerObj.getIsType()) {
                    Log.e("get Type", mSubBannerObj.getType() + "");
                    if (mSubBannerObj.getType().equalsIgnoreCase("product")) {
                        Intent in = new Intent(context, ProductDetailActivity.class);
                        in.putExtra("product_id", mSubBannerObj.getProductId());
                        in.putExtra("product_name", mSubBannerObj.getProductName());
                        //in.putExtra("reference", "list");
                        context.startActivity(in);
                    } else if (mSubBannerObj.getType().equalsIgnoreCase("category")) {

                        if (String.valueOf(mSubBannerObj.getCategoryCount()).equalsIgnoreCase("0")) {

                            Intent intent=new Intent(context,ProductListActivity.class);
                            intent.putExtra("categoryid", mSubBannerObj.getCategoryId());
                            intent.putExtra("categoryName", mSubBannerObj.getCategoryName());
                            intent.putExtra("productCount", mSubBannerObj.getProductCount()+"");
                            context.startActivity(intent);
                        } else {
                            Intent intent=new Intent(context,SubCategoryActivity.class);
                            intent.putExtra("sCategoriesId",mSubBannerObj.getCategoryId());
                            intent.putExtra("categoryName",mSubBannerObj.getCategoryName());
                            context.startActivity(intent);
                        }

                    }else {
                        Intent intent = new Intent(context, SearchActivity.class);
                        intent.putExtra("search_text", mSubBannerObj.getSearchWord());
                        context.startActivity(intent);
                    }
                }

            }
        });


    }


    /**
     * The type Square banner holder.
     */
    public class SquareBannerHolder extends RecyclerViewHolders {

        public ImageView mBannerImage;
        public TextView mBannerName;


        /**
         * Instantiates a new Square banner holder.
         *
         * @param v the v
         */
        public SquareBannerHolder(View v) {
            super(v);
            this.mBannerImage = (ImageView) v.findViewById(R.id.banner_slot_three_image);
            this.mBannerName = (TextView) v.findViewById(R.id.banner_name);
            int width = 0;
            WindowManager windowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
            Display display = windowManager.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            width = size.x / 2;
            v.setLayoutParams(new RecyclerView.LayoutParams(width,width));
        }
    }


    /**
     * @return int
     */
    @Override
    public int getItemCount() {
        return this.subBannerList.size();
    }


}
