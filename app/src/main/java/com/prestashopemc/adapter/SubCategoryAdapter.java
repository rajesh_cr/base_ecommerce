package com.prestashopemc.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.prestashopemc.R;
import com.prestashopemc.customView.CustomTextView;
import com.prestashopemc.model.Category;
import com.prestashopemc.model.ExpandCategory;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.view.ProductListActivity;
import com.prestashopemc.view.SubCategoryActivity;

import java.util.List;

/**
 * <h1>Sub Category Adapter Contains Sub Category Holder</h1>
 * The Sub Category Adapter implements an adapter view for Sub Category.
 * <p>
 * <b>Note:</b> Adapter values are get from Category model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class SubCategoryAdapter extends RecyclerView.Adapter<SubCategoryAdapter.SubCategoryHolder> {

    private List<ExpandCategory> mCategoryList;
    private Context mContext;
    private ExpandCategory mCategoryObject;
    private SubCategoryActivity mActivity;


    /**
     * Instantiates a new Sub category adapter.
     *
     * @param mContext      the m context
     * @param mCategoryList the m category list
     */
    public SubCategoryAdapter(Context mContext, List<ExpandCategory> mCategoryList) {
        this.mCategoryList = mCategoryList;
        this.mContext = mContext;
    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public SubCategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        mActivity = (SubCategoryActivity) mContext;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_parent, null);

        SubCategoryHolder viewHolder = new SubCategoryHolder(view);
        return viewHolder;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(SubCategoryHolder holder, final int position) {

        mCategoryObject = mCategoryList.get(position);
        holder.mCategoryName.setText(mCategoryObject.getName());
        holder.mCategoryName.setTypeface(mActivity.proximanovaAltRegular);

        holder.mSubCategoryLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ExpandCategory mCategoryObject = mCategoryList.get(position);
                Intent intent;

                if (mCategoryObject.getCategoryCount() == 0) {
                    intent = new Intent(mContext, ProductListActivity.class);
                    intent.putExtra("sCategoriesId", mCategoryObject.getIdCategory());
                    intent.putExtra("categoryName", mCategoryObject.getName());
                    intent.putExtra("productCount",mCategoryObject.getProductCount()+"");
                    SharedPreference.getInstance().save("subCategoryName", mCategoryObject.getName());
                    mContext.startActivity(intent);
                } else {
                    Intent subCategoryIntent = new Intent(mContext, SubCategoryActivity.class);
                    subCategoryIntent.putExtra("sCategoriesId", mCategoryObject.getIdCategory());
                    subCategoryIntent.putExtra("categoryName", mCategoryObject.getName());
                    SharedPreference.getInstance().save("subCategoryName", mCategoryObject.getName());
                    mContext.startActivity(subCategoryIntent);
                }


            }
        });

    }


    /**
     * The type Sub category holder.
     */
    public class SubCategoryHolder extends RecyclerViewHolders {

        private CustomTextView mCategoryName;
        private RelativeLayout mSubCategoryLayout;


        /**
         * Instantiates a new Sub category holder.
         *
         * @param v the v
         */
        public SubCategoryHolder(View v) {
            super(v);
            this.mCategoryName = (CustomTextView) v.findViewById(R.id.category_header_name);
            this.mSubCategoryLayout =(RelativeLayout)v.findViewById(R.id.parent_category_layout);
        }
    }


    /**
     * @return int
     */
    @Override
    public int getItemCount() {
        return this.mCategoryList.size();
    }
}
