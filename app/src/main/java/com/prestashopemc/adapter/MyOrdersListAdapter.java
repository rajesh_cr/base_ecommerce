package com.prestashopemc.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.model.MyOrderListResult;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.view.MyOrdersActivity;
import com.prestashopemc.view.MyOrdersDetailsActivity;

import java.util.List;

/**
 * <h1>My Order List Adapter Contains My Order List Holder</h1>
 * The My Order List Adapter implements an adapter view for list of order contents in My Order Activity.
 * <p>
 * <b>Note:</b> Adapter values are get from My Order List Result model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class MyOrdersListAdapter extends RecyclerView.Adapter<MyOrdersListAdapter.MyOrdersListHolder> {
    private Context mContext;
    private MyOrderListResult mMyOrderListResult;
    private List<MyOrderListResult> mMyOrderResultList;
    private MyOrdersActivity mMyOrdersActivity;
    private MyOrderChildAdapter mMyOrderChildAdapter;

    /**
     * Instantiates a new My orders list adapter.
     *
     * @param context            the context
     * @param mMyOrderListResult the m my order list result
     */
    public MyOrdersListAdapter(Context context, List<MyOrderListResult> mMyOrderListResult) {
        this.mContext = context;
        this.mMyOrderResultList = mMyOrderListResult;
    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public MyOrdersListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        mMyOrdersActivity = (MyOrdersActivity) mContext;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.myorders_list_adapter, null);
        MyOrdersListHolder viewHolder = new MyOrdersListHolder(view);
        return viewHolder;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(final MyOrdersListHolder holder, final int position) {
        mMyOrderListResult = mMyOrderResultList.get(position);
        setCustomFont(holder);
        holder.mOrderId.setText(mMyOrderListResult.getRealOrderId());
        holder.mOrderPlaced.setText(mMyOrderListResult.getOrderDate());
        holder.mOrderStatus.setText(mMyOrderListResult.getOrderStatus());

        holder.mMyOrderListChildView.setLayoutManager(new LinearLayoutManager(mContext));
        mMyOrderChildAdapter = new MyOrderChildAdapter(mContext, mMyOrderListResult.getProduct(), mMyOrderListResult.getCurrencySymbol(), mMyOrderListResult.getRealOrderId());
        holder.mMyOrderListChildView.setAdapter(mMyOrderChildAdapter);

        holder.mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMyOrderListResult = mMyOrderResultList.get(position);
                Intent orderDetailIntent = new Intent(mContext, MyOrdersDetailsActivity.class);
                orderDetailIntent.putExtra("orderId", mMyOrderListResult.getRealOrderId());
                mContext.startActivity(orderDetailIntent);
            }
        });
    }

    /**
     * Set Custome Font
     */
    private void setCustomFont(MyOrdersListHolder holder) {
        holder.mOrderIdLabel.setText(AppConstants.getTextString(mContext, AppConstants.orderIdLabelText));
        holder.mOrderPlacedLabel.setText(AppConstants.getTextString(mContext, AppConstants.placedOnText));
        holder.mOrderStatusLabel.setText(AppConstants.getTextString(mContext, AppConstants.statusText));

        holder.mOrderId.setTypeface(mMyOrdersActivity.robotoRegular);
        holder.mOrderPlaced.setTypeface(mMyOrdersActivity.robotoRegular);
        holder.mOrderStatus.setTypeface(mMyOrdersActivity.robotoRegular);
        holder.mOrderIdLabel.setTypeface(mMyOrdersActivity.robotoMedium);
        holder.mOrderPlacedLabel.setTypeface(mMyOrdersActivity.robotoMedium);
        holder.mOrderStatusLabel.setTypeface(mMyOrdersActivity.robotoMedium);
    }

    /**
     * Holder
     */
    public class MyOrdersListHolder extends RecyclerViewHolders {

        /**
         * The M order id label.
         */
        public TextView mOrderIdLabel, mOrderId, mOrderPlacedLabel, mOrderPlaced, mOrderStatusLabel, mOrderStatus;
        public RecyclerView mMyOrderListChildView;
        public RelativeLayout mLayout;

        /**
         * Instantiates a new My orders list holder.
         *
         * @param v the v
         */
        public MyOrdersListHolder(View v) {
            super(v);
            this.mMyOrderListChildView = (RecyclerView) v.findViewById(R.id.my_orders_child_list);
            this.mOrderIdLabel = (TextView) v.findViewById(R.id.order_no_label);
            this.mOrderId = (TextView) v.findViewById(R.id.order_no);
            this.mOrderPlacedLabel = (TextView) v.findViewById(R.id.order_date_label);
            this.mOrderPlaced = (TextView) v.findViewById(R.id.order_date);
            this.mOrderStatusLabel = (TextView) v.findViewById(R.id.order_success_label);
            this.mOrderStatus = (TextView) v.findViewById(R.id.order_success);
            this.mLayout = (RelativeLayout) v.findViewById(R.id.list_main_layout);
        }
    }

    /**
     * @return int
     */
    @Override
    public int getItemCount() {
        return this.mMyOrderResultList.size();
    }

}
