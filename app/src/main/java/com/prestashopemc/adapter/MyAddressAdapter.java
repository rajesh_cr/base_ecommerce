package com.prestashopemc.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.PopupMenu;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.controller.LoginController;
import com.prestashopemc.model.MyAdditionalAddressResult;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.view.AddNewAddress;
import com.prestashopemc.view.MyAddressActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * <h1>My Address Adapter Contains My Address Holder</h1>
 * The My Address Adapter implements an adapter view for customer address.
 * <p>
 * <b>Note:</b> Adapter values are get from My Additional Address Result model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class MyAddressAdapter extends RecyclerView.Adapter<MyAddressAdapter.MyAddressHolder> {

    private List<MyAdditionalAddressResult> mMyAddressResult;
    private Context mContext;
    private MyAdditionalAddressResult mMyAddressResults;
    private MyAddressActivity mMyAddressActivity;
    private String mName, mAddress, mDefaultId;
    private LoginController mLoginController;
    private JSONObject mDefaultObject;

    /**
     * Instantiates a new My address adapter.
     *
     * @param context         the context
     * @param myAddressResult the my address result
     */
    public MyAddressAdapter(Context context, List<MyAdditionalAddressResult> myAddressResult) {
        this.mContext = context;
        this.mMyAddressResult = myAddressResult;
    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public MyAddressHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mMyAddressActivity = (MyAddressActivity) mContext;
        mLoginController = new LoginController(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.myaddress_list_adapter, null);
        MyAddressHolder viewHolder = new MyAddressHolder(view);
        return viewHolder;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(final MyAddressHolder holder, final int position) {
        mMyAddressResults = mMyAddressResult.get(position);
        mName = mMyAddressResults.getFirstname() + " " + mMyAddressResults.getLastname();
        mAddress = mMyAddressResults.getStreet() + "\n" + mMyAddressResults.getCity() + ", " + mMyAddressResults.getCountry() + "\n"
                + mMyAddressResults.getPostcode() + ", " + mMyAddressResults.getTelephone();
        setCustomFont(holder);
        holder.mAddressName.setText(mName);
        holder.mAddress.setText(mAddress);
        if (mMyAddressResults.getIsDefaultAddress()) {
            mDefaultId = mMyAddressResults.getIdAddress();
            mDefaultObject = new JSONObject();
            try {
                mDefaultObject.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
                mDefaultObject.put("country_id", mMyAddressResults.getIdCountry());
                mDefaultObject.put("region_id", mMyAddressResults.getIdState());
                mDefaultObject.put("city", mMyAddressResults.getCity());
                mDefaultObject.put("postcode", mMyAddressResults.getPostcode());
                mDefaultObject.put("firstname", mMyAddressResults.getFirstname());
                mDefaultObject.put("lastname", mMyAddressResults.getLastname());
                mDefaultObject.put("street", mMyAddressResults.getStreet());
                mDefaultObject.put("telephone", mMyAddressResults.getTelephone());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            holder.mDefaultLayout.setVisibility(View.VISIBLE);
        }
        holder.mMoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMyAddressResults = mMyAddressResult.get(position);
                final PopupMenu popup = new PopupMenu(mContext, holder.mMoreBtn);
                //Inflating the Popup using xml file
                popup.inflate(R.menu.poupup_menu);
                popup.setGravity(Gravity.RIGHT);
                //registering popup with OnMenuItemClickListener
                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    public boolean onMenuItemClick(MenuItem item) {
                        int i = item.getItemId();
                        switch (i) {
                            case R.id.address_edit:
                                Intent intentAddNewAddrs = new Intent(mContext, AddNewAddress.class);
                                intentAddNewAddrs.putExtra("fname", mMyAddressResults.getFirstname());
                                intentAddNewAddrs.putExtra("lname", mMyAddressResults.getLastname());
                                intentAddNewAddrs.putExtra("street", mMyAddressResults.getStreet());
                                intentAddNewAddrs.putExtra("postcode", mMyAddressResults.getPostcode());
                                intentAddNewAddrs.putExtra("city", mMyAddressResults.getCity());
                                intentAddNewAddrs.putExtra("telephone", mMyAddressResults.getTelephone());
                                intentAddNewAddrs.putExtra("idaddress", mMyAddressResults.getIdAddress());
                                intentAddNewAddrs.putExtra("country", mMyAddressResults.getCountry());
                                intentAddNewAddrs.putExtra("state", mMyAddressResults.getRegion());
                                intentAddNewAddrs.putExtra("edit", "edit");
                                intentAddNewAddrs.putExtra("fromDefault", "fromDefaultEdit");
                                if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                                    intentAddNewAddrs.putExtra("isDefault",mMyAddressResults.getIsDefaultAddress());
                                }
                                mContext.startActivity(intentAddNewAddrs);
                                return true;
                            case R.id.address_delete:
                                mMyAddressActivity.showProgDialiog();
                                if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                                    if (mMyAddressResults.getIsDefaultAddress()){
                                        mMyAddressActivity.hideProgDialog();
                                        mMyAddressActivity.snackBar(AppConstants.getTextString(mContext, AppConstants.defaultDelete));
                                    }else {
                                        mLoginController.defaultAddressDelete(mMyAddressResults.getIdAddress());
                                    }
                                }else {
                                    mLoginController.defaultAddressDelete(mMyAddressResults.getIdAddress());
                                }
                                return true;
                            case R.id.set_default:
                                mMyAddressActivity.showProgDialiog();
                                JSONObject addressObject = new JSONObject();
                                try {
                                    addressObject.put("id_customer", SharedPreference.getInstance().getValue("customerid"));
                                    addressObject.put("country_id", mMyAddressResults.getIdCountry());
                                    addressObject.put("region_id", mMyAddressResults.getIdState());
                                    addressObject.put("city", mMyAddressResults.getCity());
                                    addressObject.put("postcode", mMyAddressResults.getPostcode());
                                    addressObject.put("firstname", mMyAddressResults.getFirstname());
                                    addressObject.put("lastname", mMyAddressResults.getLastname());
                                    addressObject.put("street", mMyAddressResults.getStreet());
                                    addressObject.put("telephone", mMyAddressResults.getTelephone());
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                                    mLoginController.magentoSetDefaultAddress(mMyAddressResults.getIdAddress());
                                }else {
                                    mLoginController.setDefaultAddress(mDefaultObject, addressObject, mMyAddressResults.getIdAddress(), mDefaultId);
                                }
                                return true;
                        }
                        return true;
                    }
                });

                popup.show();//showing popup menu
            }
        });
    }

    /**
     * The type My address holder.
     */
/*Declared Holder Variables*/
    public class MyAddressHolder extends RecyclerViewHolders {


        public TextView mAddressName, mAddress, mDefaultAddress;
        public ImageView mMoreBtn;
        public LinearLayout mDefaultLayout;

        /**
         * Instantiates a new My address holder.
         *
         * @param v the v
         */
        public MyAddressHolder(View v) {
            super(v);
            this.mAddressName = (TextView) v.findViewById(R.id.address_name_label);
            this.mAddress = (TextView) v.findViewById(R.id.address_label);
            this.mMoreBtn = (ImageView) v.findViewById(R.id.default_more_icon);
            this.mDefaultAddress = (TextView) v.findViewById(R.id.default_address_label);
            this.mDefaultLayout = (LinearLayout) v.findViewById(R.id.default_layout);
        }

    }

    /**
     * @return
     */
    @Override
    public int getItemCount() {
        return mMyAddressResult.size();
    }

    /*Set Custom Font*/
    private void setCustomFont(MyAddressHolder holder) {
        holder.mAddressName.setTypeface(mMyAddressActivity.robotoRegular);
        holder.mAddress.setTypeface(mMyAddressActivity.robotoLight);
        holder.mDefaultAddress.setTypeface(mMyAddressActivity.robotoMedium);
    }
}
