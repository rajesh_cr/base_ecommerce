package com.prestashopemc.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.prestashopemc.R;
import com.prestashopemc.model.MyOrderDetailItem;
import com.prestashopemc.view.MyOrdersDetailsActivity;

import java.util.List;

/**
 * <h1>My Order Detail Adapter Contains My Order Detail Holder</h1>
 * The My Order Detail Adapter implements an adapter view for list of order contents in My Order Detail Activity.
 * <p>
 * <b>Note:</b> Adapter values are get from My Order Detail Item Product model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class MyOrderDetailAdapter extends RecyclerView.Adapter<MyOrderDetailAdapter.MyOrdersListHolder> {
    private Context mContext;
    private MyOrderDetailItem mMyOrderDetailItem;
    private List<MyOrderDetailItem> mMyOrderDetailitemList;
    private MyOrdersDetailsActivity mMyOrderActivity;
    private String mCurrencySymbol;

    /**
     * Instantiates a new My order detail adapter.
     *
     * @param context             the context
     * @param mMyOrderListProduct the m my order list product
     * @param currencySymbol      the currency symbol
     */
    public MyOrderDetailAdapter(Context context, List<MyOrderDetailItem> mMyOrderListProduct, String currencySymbol) {
        this.mContext = context;
        this.mMyOrderDetailitemList = mMyOrderListProduct;
        this.mCurrencySymbol = currencySymbol;
    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public MyOrdersListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        mMyOrderActivity = (MyOrdersDetailsActivity) mContext;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.myorders_list_child_adapter, null);

        MyOrdersListHolder viewHolder = new MyOrdersListHolder(view);
        return viewHolder;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(final MyOrdersListHolder holder, final int position) {

        mMyOrderDetailItem = mMyOrderDetailitemList.get(position);
        setCustomFont(holder);
        Picasso.with(mContext).load(mMyOrderDetailItem.getImage())
                .placeholder(R.drawable.place_holder).resize(200, 200).centerCrop()
                .into(holder.mProductImage);

        holder.mProductName.setText(mMyOrderDetailItem.getName());
        holder.mProductPrice.setText(mCurrencySymbol + " " + mMyOrderDetailItem.getPrice());
        holder.mProductItem.setVisibility(View.VISIBLE);
        holder.mProductItem.setText("( " + mContext.getString(R.string.itemsText) + " " + mMyOrderDetailItem.getQty() + " )");

    }

    /*Set Custome Font*/
    private void setCustomFont(MyOrdersListHolder holder) {
        holder.mProductName.setTypeface(mMyOrderActivity.robotoLight);
        holder.mProductPrice.setTypeface(mMyOrderActivity.robotoMedium);
        holder.mProductItem.setTypeface(mMyOrderActivity.robotoItalic);
    }

    /**
     * The type My orders list holder.
     */
/* Holder*/
    public class MyOrdersListHolder extends RecyclerViewHolders {


        public TextView mProductName, mProductPrice, mProductItem;

        public ImageView mProductImage;

        /**
         * Instantiates a new My orders list holder.
         *
         * @param v the v
         */
        public MyOrdersListHolder(View v) {
            super(v);
            this.mProductName = (TextView) v.findViewById(R.id.product_name);
            this.mProductPrice = (TextView) v.findViewById(R.id.product_price);
            this.mProductImage = (ImageView) v.findViewById(R.id.product_image);
            this.mProductItem = (TextView) v.findViewById(R.id.order_product_quantity);
        }
    }


    /**
     * @return int
     */
    @Override
    public int getItemCount() {
        return this.mMyOrderDetailitemList.size();
    }

}
