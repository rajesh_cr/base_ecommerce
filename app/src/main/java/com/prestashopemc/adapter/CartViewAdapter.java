package com.prestashopemc.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.prestashopemc.R;
import com.prestashopemc.controller.CartController;
import com.prestashopemc.customView.CustomTextView;
import com.prestashopemc.model.Productiteminfo;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.view.CartActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * <h1>Cart View Adapter Contains Cart Detail Holder</h1>
 * The Cart View Adapter implements an adapter view for cart products.
 * <p>
 * <b>Note:</b> Adapter values are get from Product Item Info model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class CartViewAdapter extends RecyclerView.Adapter<CartViewAdapter.CartDetailHolder> {

    private Context mContext;
    private CartController mCartController;
    private List<Productiteminfo> mProductInfo;
    private Productiteminfo mProductItemInfo;
    private CartActivity mCartActivity;
    private CartDetailHolder mHold;

    /**
     * Constructor Method
     *
     * @param context      the context
     * @param mProductInfo the m product info
     */
    public CartViewAdapter(Context context, List<Productiteminfo> mProductInfo) {
        this.mProductInfo = mProductInfo;
        this.mContext = context;
    }

    /**
     * @param parent   the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public CartDetailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        mCartController = new CartController(mContext);
        mCartActivity = (CartActivity) mContext;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_list_adapter, null);
        CartDetailHolder viewHolder = new CartDetailHolder(view);
        return viewHolder;
    }

    /**
     * @param holder   the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(final CartDetailHolder holder, final int position) {
        mHold = holder;
        mProductItemInfo = mProductInfo.get(position);
        String currenySymbol = SharedPreference.getInstance().getValue("currency_symbol");
        Picasso.with(mContext).load(mProductItemInfo.getImageUrl())
                .placeholder(R.drawable.place_holder).resize(200, 200).centerCrop()
                .into(holder.mProductImage);
        setCustomFont(holder);
        holder.mProductName.setText(mProductItemInfo.getName());
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)){
            holder.mProductSplPrice.setText(currenySymbol + " " + mProductItemInfo.getBasePrice());
        }else {
            holder.mProductSplPrice.setText(currenySymbol + " " + mProductItemInfo.getPrice());
        }
        if (mProductItemInfo.getSpecialPrice() != null) {
            holder.mProductPrice.setText(currenySymbol + " " + mProductItemInfo.getSpecialPrice());
        } else {
            holder.mProductPrice.setVisibility(View.GONE);
        }
        holder.mProductPrice.setPaintFlags(holder.mProductPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        /*String supplierMsg = productiteminfo.getSupplierMessage();
        if (!supplierMsg.isEmpty() ){
            holder.mCartSupplierMsg.setVisibility(View.VISIBLE);
            String[] supplier1 = supplierMsg.split(":");
            String msg1 = supplier1[0];
            String msg2 = supplier1[1];

            String[] supplier2 = msg2.split(";");
            String msg3 = supplier2[0];
            String msg4 = supplier2[1];

            Spannable supplierMSG = new SpannableString(msg3);
            supplierMSG.setSpan(new ForegroundColorSpan(Color.BLACK), 0, supplierMSG.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.mCartSupplierMsg.setText(msg1+":");
            holder.mCartSupplierMsg.append(supplierMSG);
            holder.mCartSupplierMsg.append(msg4);
        }*/

        final int[] mCount = new int[]{mProductItemInfo.getQty()};
        holder.mQuantityNumber.setText(String.valueOf(mCount[0]));

        holder.mQuantityPlus.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mProductItemInfo = mProductInfo.get(position);
                mCount[0]++;
                ((CartActivity) mContext).showProgDialiog();
                holder.mQuantityNumber.setText(String.valueOf(mCount[0]));
                mCartController.cartQuantityUpdate(String.valueOf(mCount[0]), mProductItemInfo.getProductId(),
                        mProductItemInfo.getIdProductAttribute(), mProductItemInfo.getItemId());
            }
        });

        holder.mQuantityMinus.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mProductItemInfo = mProductInfo.get(position);
                if (mCount[0] != 0) {
                    mCount[0]--;
                    if (mCount[0] == 0) {
                        mCount[0]++;
                        holder.mQuantityNumber.setText(String.valueOf(mCount[0]));
                    } else {
                        ((CartActivity) mContext).showProgDialiog();
                        holder.mQuantityNumber.setText(String.valueOf(mCount[0]));
                        mCartController.cartQuantityUpdate(String.valueOf(mCount[0]), mProductItemInfo.getProductId(),
                                mProductItemInfo.getIdProductAttribute(), mProductItemInfo.getItemId());
                    }
                }
            }
        });

        holder.mDeleteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog(position);
            }
        });

    }

    /**
     * Set Custom Font
     */
    private void setCustomFont(CartDetailHolder holder) {
        holder.mProductName.setTypeface(mCartActivity.robotoRegular);
        holder.mProductSplPrice.setTypeface(mCartActivity.robotoMedium);
        holder.mProductPrice.setTypeface(mCartActivity.robotoLight);
        holder.mCartSupplierMsg.setTypeface(mCartActivity.robotoLight);
        holder.mQuantityNumber.setTypeface(mCartActivity.opensansRegular);
    }

    /**
     * Update Quantity number from Cart Activity
     *
     * @param quantity         the quantity
     * @param quantityPosition the quantity position
     */
    public void updateQuantity(String quantity, String quantityPosition) {
        mHold.mQuantityNumber.setTag(quantityPosition);
        int pos = Integer.parseInt(mHold.mQuantityNumber.getTag().toString());
        mHold.mQuantityNumber.setText(quantity);
    }

    /**
     * Holder
     */
    public class CartDetailHolder extends RecyclerViewHolders {


        public ImageView mProductImage, mDeleteIcon, mQuantityPlus,mQuantityMinus;

        public CustomTextView mProductName, mProductPrice, mProductSplPrice, mQuantityNumber, mCartSupplierMsg;

        /**
         * Instantiates a new Cart detail holder.
         *
         * @param v the v
         */
        public CartDetailHolder(View v) {
            super(v);
            this.mProductImage = (ImageView) v.findViewById(R.id.product_image);
            this.mProductName = (CustomTextView) v.findViewById(R.id.product_name);
            this.mDeleteIcon = (ImageView) v.findViewById(R.id.delete_icon);
            this.mProductSplPrice = (CustomTextView) v.findViewById(R.id.price);
            this.mProductPrice = (CustomTextView) v.findViewById(R.id.spl_price);
            this.mQuantityMinus = (ImageView) v.findViewById(R.id.quantity_minus);
            this.mQuantityPlus = (ImageView) v.findViewById(R.id.quantity_plus);
            this.mQuantityNumber = (CustomTextView) v.findViewById(R.id.quantity_text);
            this.mCartSupplierMsg = (CustomTextView) v.findViewById(R.id.cart_item_supplier);
        }
    }

    /**
     * @return
     */
    @Override
    public int getItemCount() {
        return this.mProductInfo.size();
    }

    /**
     * Alert Dialog for Item delete
     */
    private void alertDialog(final int posi) {
        new AlertDialog.Builder(mContext)
                .setTitle(AppConstants.getTextString(mContext, AppConstants.deleteItemText))
                .setMessage(AppConstants.getTextString(mContext, AppConstants.deleteInformationText))
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // continue with delete
                        mProductItemInfo = mProductInfo.get(posi);
                        ((CartActivity) mContext).showProgDialiog();
                        mProductInfo.remove(posi);
                        mCartController.cartItemDelete(mProductItemInfo.getProductId(), mProductItemInfo.getIdProductAttribute());
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }
}
