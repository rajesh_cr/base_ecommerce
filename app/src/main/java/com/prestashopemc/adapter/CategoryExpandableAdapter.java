package com.prestashopemc.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;
import com.prestashopemc.R;
import com.prestashopemc.customView.CustomTextView;
import com.prestashopemc.model.Category;
import com.prestashopemc.model.ExpandCategory;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.view.MainActivity;
import com.prestashopemc.view.ProductListActivity;
import com.prestashopemc.view.SubCategoryActivity;

import java.util.List;

/**
 * <h1>Category Expandable Adapter Contains Big Banner Holder</h1>
 * The Category Expandable Adapter implements an adapter view for expandable category.
 * <p>
 * <b>Note:</b> Adapter values are get from Image List model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class CategoryExpandableAdapter extends ExpandableRecyclerAdapter<CategoryExpandableAdapter.MyParentViewHolder, CategoryExpandableAdapter.MyChildViewHolder> {
    private LayoutInflater mInflater;
    private Context mContext;
    private Category mSubcategoryChildListItem;
    private MainActivity mActivity;

    /**
     * Instantiates a new Category expandable adapter.
     *
     * @param context        the context
     * @param parentItemList the parent item list
     */
    public CategoryExpandableAdapter(Context context, List<ParentListItem> parentItemList) {
        super(parentItemList);
        this.mContext = context;
        mInflater = LayoutInflater.from(context);
        mActivity = (MainActivity) mContext;
    }

    /**
     * @param viewGroup
     * @return
     */
    @Override
    public MyParentViewHolder onCreateParentViewHolder(ViewGroup viewGroup) {
        View view = mInflater.inflate(R.layout.category_parent, viewGroup, false);
        return new MyParentViewHolder(view);
    }

    /**
     * @param viewGroup
     * @return
     */
    @Override
    public MyChildViewHolder onCreateChildViewHolder(ViewGroup viewGroup) {
        View view = mInflater.inflate(R.layout.sub_category, viewGroup, false);
        return new MyChildViewHolder(view);
    }

    /**
     * @param parentViewHolder The {@code PVH} to bind data to
     * @param i
     * @param parentListItem   The {@link ParentListItem} which holds the data to
     */
    @Override
    public void onBindParentViewHolder(final MyParentViewHolder parentViewHolder, int i, final ParentListItem parentListItem) {
        Category subcategoryParentListItem = (Category) parentListItem;
        parentViewHolder.mCategoryName.setText(subcategoryParentListItem.getName());
        parentViewHolder.mCategoryName.setTypeface(mActivity.proximanovaAltRegular);

        parentViewHolder.mParentCategory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Category subcategoryChildListItem = (Category) parentListItem;
                Intent intent;

                if ((subcategoryChildListItem.getProductCount() != 0
                        && subcategoryChildListItem.getCategoryCount() != 0)) {
                    ((MainActivity) mContext).closeDrawer();
                    intent = new Intent(mContext, SubCategoryActivity.class);
                    intent.putExtra("sCategoriesId", subcategoryChildListItem.getIdCategory());
                    intent.putExtra("categoryName", subcategoryChildListItem.getName());
                    SharedPreference.getInstance().save("categoryName", subcategoryChildListItem.getName());
                    mContext.startActivity(intent);

                }

                if ((subcategoryChildListItem.getProductCount() != 0
                        && subcategoryChildListItem.getCategoryCount() == 0) ||
                        (subcategoryChildListItem.getProductCount() == 0
                                && subcategoryChildListItem.getCategoryCount() == 0)) {
                    ((MainActivity) mContext).closeDrawer();
                    intent = new Intent(mContext, ProductListActivity.class);
                    intent.putExtra("sCategoriesId", subcategoryChildListItem.getIdCategory());
                    intent.putExtra("categoryName", subcategoryChildListItem.getName());
                    intent.putExtra("productCount", subcategoryChildListItem.getProductCount() + "");
                    SharedPreference.getInstance().save("categoryName", subcategoryChildListItem.getName());
                    mContext.startActivity(intent);
                }

                if (subcategoryChildListItem.getProductCount() == 0 && subcategoryChildListItem.getCategoryCount() != 0) {
                    if (!parentViewHolder.isExpanded()) {
                        parentViewHolder.expandView();
                        parentViewHolder.mItemArrow.setImageResource(R.drawable.ic_down_arrow);
                    } else {
                        parentViewHolder.collapseView();
                        parentViewHolder.mItemArrow.setImageResource(R.drawable.ic_right_arrow);
                    }
                }

            }
        });
    }

    /**
     * @param childViewHolder The {@code CVH} to bind data to
     * @param position the position        The index in the list at which to bind
     * @param childListItem   The child list item which holds that data to be
     */
    @Override
    public void onBindChildViewHolder(MyChildViewHolder childViewHolder, int position, final Object childListItem) {
        ExpandCategory subcategoryChildListItem = (ExpandCategory) childListItem;
        childViewHolder.mCategoryName.setText(subcategoryChildListItem.getName());
        childViewHolder.mCategoryName.setTypeface(mActivity.proximanovaAltRegular);

        childViewHolder.mCategoryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ExpandCategory subcategoryChildListItem = (ExpandCategory) childListItem;
                ((MainActivity) mContext).closeDrawer();
                Intent intent;
                if (subcategoryChildListItem.getCategoryCount() == 0) {
                    ((MainActivity) mContext).closeDrawer();
                    intent = new Intent(mContext, ProductListActivity.class);
                    intent.putExtra("sCategoriesId", subcategoryChildListItem.getIdCategory());
                    intent.putExtra("categoryName", subcategoryChildListItem.getName());
                    intent.putExtra("productCount", subcategoryChildListItem.getProductCount() + "");
                    SharedPreference.getInstance().save("categoryName", subcategoryChildListItem.getName());
                    mContext.startActivity(intent);

                } else {
                    intent = new Intent(mContext, SubCategoryActivity.class);
                    intent.putExtra("sCategoriesId", subcategoryChildListItem.getIdCategory());
                    intent.putExtra("categoryName", subcategoryChildListItem.getName());
                    SharedPreference.getInstance().save("categoryName", subcategoryChildListItem.getName());
                    mContext.startActivity(intent);
                }

            }
        });
    }

    /**
     * The type My parent view holder.
     */
    public class MyParentViewHolder extends ParentViewHolder {

        public CustomTextView mCategoryName;
        public RelativeLayout mParentCategory;
        public ImageView mItemArrow;

        /**
         * Instantiates a new My parent view holder.
         *
         * @param itemView the item view
         */
        public MyParentViewHolder(View itemView) {
            super(itemView);
            mCategoryName = (CustomTextView) itemView.findViewById(R.id.category_header_name);
            mParentCategory = (RelativeLayout) itemView.findViewById(R.id.parent_category_layout);
            mItemArrow = (ImageView) itemView.findViewById(R.id.item_arrow);
        }
    }

    /**
     * The type My child view holder.
     */
    public class MyChildViewHolder extends ChildViewHolder {
        /**
         * The M category name.
         */
        public CustomTextView mCategoryName;

        /**
         * Instantiates a new My child view holder.
         *
         * @param itemView the item view
         */
        public MyChildViewHolder(View itemView) {
            super(itemView);
            mCategoryName = (CustomTextView) itemView.findViewById(R.id.category_name);
        }
    }
}
