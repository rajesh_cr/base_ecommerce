package com.prestashopemc.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prestashopemc.view.MyOrdersDetailsActivity;
import com.squareup.picasso.Picasso;
import com.prestashopemc.R;
import com.prestashopemc.model.MyOrderListProduct;
import com.prestashopemc.view.MyOrdersActivity;

import java.util.List;

/**
 * <h1>My Order Child Adapter Contains My Order Child Holder</h1>
 * The My Order Child Adapter implements an adapter view for list of order contents in My Order Activity.
 * <p>
 * <b>Note:</b> Adapter values are get from My Order List Product model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class MyOrderChildAdapter extends RecyclerView.Adapter<MyOrderChildAdapter.MyOrdersListHolder> {
    private Context mContext;
    private MyOrderListProduct mMyOrderListProduct;
    private List<MyOrderListProduct> mMyOrderProductList;
    private MyOrdersActivity mMyOrdersActivity;
    private String mCurrencySymbol, mOrderId;

    /**
     * Instantiates a new My order child adapter.
     *
     * @param context             the context
     * @param mMyOrderListProduct the m my order list product
     * @param currenySymbol       the curreny symbol
     * @param orderId             the order id
     */
    public MyOrderChildAdapter(Context context, List<MyOrderListProduct> mMyOrderListProduct, String currenySymbol, String orderId) {
        this.mContext = context;
        this.mMyOrderProductList = mMyOrderListProduct;
        this.mCurrencySymbol = currenySymbol;
        this.mOrderId = orderId;
    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public MyOrdersListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        mMyOrdersActivity = (MyOrdersActivity) mContext;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.myorders_list_child_adapter, null);
        MyOrdersListHolder viewHolder = new MyOrdersListHolder(view);
        return viewHolder;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(final MyOrdersListHolder holder, final int position) {
        mMyOrderListProduct = mMyOrderProductList.get(position);
        setCustomFont(holder);
        Picasso.with(mContext).load(mMyOrderListProduct.getImage())
                .placeholder(R.drawable.place_holder).resize(200, 200).centerCrop()
                .into(holder.mProductImage);

        holder.mProductName.setText(mMyOrderListProduct.getName());
        holder.mProductPrice.setText(mCurrencySymbol + " " + mMyOrderListProduct.getPrice());
        holder.mMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMyOrderListProduct = mMyOrderProductList.get(position);
                Intent orderDetailIntent = new Intent(mContext, MyOrdersDetailsActivity.class);
                orderDetailIntent.putExtra("orderId", mOrderId);
                mContext.startActivity(orderDetailIntent);
            }
        });

    }

    /*Set Custome Font*/
    private void setCustomFont(MyOrdersListHolder holder) {
        holder.mProductName.setTypeface(mMyOrdersActivity.robotoLight);
        holder.mProductPrice.setTypeface(mMyOrdersActivity.robotoMedium);
    }

    /**
     * The type My orders list holder.
     */
/* Holder*/
    public class MyOrdersListHolder extends RecyclerViewHolders {

        /**
         * The M product name.
         */
        public TextView mProductName, /**
         * The M product price.
         */
        mProductPrice;
        /**
         * The M product image.
         */
        public ImageView mProductImage;
        /**
         * The M main layout.
         */
        public RelativeLayout mMainLayout;

        /**
         * Instantiates a new My orders list holder.
         *
         * @param v the v
         */
        public MyOrdersListHolder(View v) {
            super(v);
            this.mProductName = (TextView) v.findViewById(R.id.product_name);
            this.mProductPrice = (TextView) v.findViewById(R.id.product_price);
            this.mProductImage = (ImageView) v.findViewById(R.id.product_image);
            this.mMainLayout = (RelativeLayout) v.findViewById(R.id.order_main_layout);
        }
    }


    /**
     * @return int the size of my order product list array
     */
    @Override
    public int getItemCount() {
        return this.mMyOrderProductList.size();
    }

}

