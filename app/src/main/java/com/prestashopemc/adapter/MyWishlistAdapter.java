package com.prestashopemc.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prestashopemc.util.AppConstants;
import com.prestashopemc.view.MagentoProductDetailActivity;
import com.squareup.picasso.Picasso;
import com.prestashopemc.R;
import com.prestashopemc.controller.LoginController;
import com.prestashopemc.model.WishlistResult;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.view.MyWishlistActivity;
import com.prestashopemc.view.ProductDetailActivity;

import java.util.List;

/**
 * <h1>My Wish list Adapter Contains My Wish list Holder</h1>
 * The My Wish list Adapter implements an adapter view for Customer Wish list Products.
 * <p>
 * <b>Note:</b> Adapter values are get from My Wish list Result model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class MyWishlistAdapter extends RecyclerView.Adapter<MyWishlistAdapter.MyWishlistHolder> {
    private Context mContext;
    private WishlistResult mWishlistResult;
    private List<WishlistResult> mWishlistResults;
    private MyWishlistActivity mMyWishlistActivity;
    private LoginController mLoginController;

    /**
     * Instantiates a new My wishlist adapter.
     *
     * @param context          the context
     * @param mWishlistResults the m wishlist results
     */
    public MyWishlistAdapter(Context context, List<WishlistResult> mWishlistResults) {
        this.mContext = context;
        this.mWishlistResults = mWishlistResults;
    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public MyWishlistHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        mLoginController = new LoginController(mContext);
        mMyWishlistActivity = (MyWishlistActivity) mContext;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.wishlist_list, null);
        MyWishlistHolder viewHolder = new MyWishlistHolder(view);
        return viewHolder;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(final MyWishlistHolder holder, final int position) {
        mWishlistResult = mWishlistResults.get(position);
        setCustomFont(holder);
        String currenySymbol = SharedPreference.getInstance().getValue("currency_symbol");
        Picasso.with(mContext).load(mWishlistResult.getImage())
                .placeholder(R.drawable.place_holder).resize(200, 200).centerCrop()
                .into(holder.mProductImage);

        holder.mWishlistProductName.setText(mWishlistResult.getName());
        holder.mProductDesc.setText(mWishlistResult.getDescription());

        /**
         * Set Price and Spl price */
        if (!mWishlistResult.getSpecialprice().equalsIgnoreCase(mContext.getString(R.string.not_set)) &&
                mWishlistResult.getSpecialprice()!= null){
            holder.mProductPrice.setText(currenySymbol + " " + mWishlistResult.getSpecialprice());
        }else {
            holder.mProductSplPrice.setVisibility(View.GONE);
            holder.mProductPrice.setText(currenySymbol + " " + mWishlistResult.getPrice());
        }
        if (mWishlistResult.getPrice()!= null){
            if (!mWishlistResult.getPrice().equalsIgnoreCase(mContext.getString(R.string.not_set))){
                holder.mProductSplPrice.setText(currenySymbol + " " + mWishlistResult.getPrice());
            }
        }

        holder.mProductSplPrice.setPaintFlags(holder.mProductSplPrice
                .getPaintFlags()
                | Paint.STRIKE_THRU_TEXT_FLAG);

        holder.mDeleteImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWishlistResult = mWishlistResults.get(position);
                mMyWishlistActivity.showProgDialiog();
                String productId = mWishlistResult.getProductid();
                String attrId = mWishlistResult.getIdProductAttribute();
                String wishlistId = mWishlistResult.getIdWishlist();
                mLoginController.myWishlistDelete(productId, attrId, wishlistId);
            }
        });

        holder.mMainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mWishlistResult = mWishlistResults.get(position);
                if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                    Intent detailIntent = new Intent(mContext, MagentoProductDetailActivity.class);
                    detailIntent.putExtra("product_id", "" + mWishlistResult.getProductid());
                    detailIntent.putExtra("product_name", mWishlistResult.getName());
                    mContext.startActivity(detailIntent);
                }else {
                    Intent detailIntent = new Intent(mContext, ProductDetailActivity.class);
                    detailIntent.putExtra("product_id", "" + mWishlistResult.getProductid());
                    detailIntent.putExtra("product_name", mWishlistResult.getName());
                    mContext.startActivity(detailIntent);
                }
            }
        });

    }

    /**
     * Set Custome Font
     */
    private void setCustomFont(MyWishlistHolder holder) {
        holder.mWishlistProductName.setTypeface(mMyWishlistActivity.robotoRegular);
        holder.mProductDesc.setTypeface(mMyWishlistActivity.robotoLight);
        holder.mProductPrice.setTypeface(mMyWishlistActivity.robotoMedium);
        holder.mProductSplPrice.setTypeface(mMyWishlistActivity.robotoLight);
    }

    /**
     * Holder
     */
    public class MyWishlistHolder extends RecyclerViewHolders {

        public ImageView mProductImage, mDeleteImage;
        public TextView mWishlistProductName, mProductDesc, mProductPrice, mProductSplPrice;
        public RelativeLayout mMainLayout;

        /**
         * Instantiates a new My wishlist holder.
         *
         * @param v the v
         */
        public MyWishlistHolder(View v) {
            super(v);
            this.mProductImage = (ImageView) v.findViewById(R.id.product_image);
            this.mDeleteImage = (ImageView) v.findViewById(R.id.wishlist_delete);
            this.mWishlistProductName = (TextView) v.findViewById(R.id.product_name);
            this.mProductDesc = (TextView) v.findViewById(R.id.product_description);
            this.mProductPrice = (TextView) v.findViewById(R.id.product_price);
            this.mProductSplPrice = (TextView) v.findViewById(R.id.product_spl_price);
            this.mMainLayout = (RelativeLayout) v.findViewById(R.id.mywishlist_list_layout);
        }
    }

    /**
     * @return
     */
    @Override
    public int getItemCount() {
        return this.mWishlistResults.size();
    }
}
