package com.prestashopemc.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.controller.HomePageController;
import com.prestashopemc.database.CategoryProductService;
import com.prestashopemc.database.MagentoCategoryProductService;
import com.prestashopemc.database.MagentoProductService;
import com.prestashopemc.database.ProductService;
import com.prestashopemc.model.Currency;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.CustomIcon;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.view.SettingActivity;

import java.util.List;

/**
 * <h1>Currency Adapter Contains Currency Holder</h1>
 * The Currencyt Adapter implements an adapter view for Currencies.
 * <p>
 * <b>Note:</b> Adapter values are get from Currency model.
 * The Category Product Service and  Product Service are deleted when currency is change
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class CurrencyAdapter extends RecyclerView.Adapter<CurrencyAdapter.CurrencyHolder> {

    private List<Currency> mCurrecy;
    private Context mContext;
    private Currency mCurrency;
    private SettingActivity mSettingActivity;
    private int mSelected = 0;
    private CategoryProductService mCategoryProductService;
    private ProductService mProductService;
    private MagentoCategoryProductService mMagentoCategoryProductService;
    private MagentoProductService mMagentoProductService;
    private Bitmap mFinalBitmap;
    private HomePageController mHompageController;

    /**
     * Instantiates a new Currency adapter.
     *
     * @param mContext      the m context
     * @param mCurrencyList the m currency list
     */
    public CurrencyAdapter(Context mContext, List<Currency> mCurrencyList) {
        this.mCurrecy = mCurrencyList;
        this.mContext = mContext;
    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return CurrencyHolder
     */
    @Override
    public CurrencyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        mSettingActivity = (SettingActivity) mContext;

        if(AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)){
            mMagentoCategoryProductService = new MagentoCategoryProductService(mContext);
            mMagentoProductService = new MagentoProductService(mContext);
        }else {
            mCategoryProductService = new CategoryProductService(mContext);
            mProductService = new ProductService(mContext);
        }

        mHompageController = new HomePageController(mContext);
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.language_currency_adapter, null);
        String defaultCurrency = SharedPreference.getInstance().getValue("id_currency");
        for (int i = 0; i < mCurrecy.size(); i++) {
            if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                if (mCurrecy.get(i).getIsoCode().equals(defaultCurrency)) {
                    mSelected = i;
                }
            }else {
                if (mCurrecy.get(i).getIdCurrency().equals(defaultCurrency)) {
                    mSelected = i;
                }
            }
        }
        CurrencyHolder viewHolder = new CurrencyHolder(view);
        return viewHolder;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(final CurrencyHolder holder, final int position) {
        mCurrency = mCurrecy.get(position);
        customIcon();
        holder.mLangCurrencyLabel.setText(mCurrency.getName());
        holder.mLangCurrencyLabel.setTypeface(mSettingActivity.robotoLight);
        holder.mLangCurrencyLabel.setTag(position);
        holder.mLangCurrencyLabel.setTextColor(mContext.getResources().getColor(R.color.lang_list_color));
        holder.mActivateRight.setVisibility(View.GONE);
        if (position == mSelected){
            holder.mActivateRight.setVisibility(View.VISIBLE);
            holder.mActivateRight.setImageBitmap(mFinalBitmap);
            holder.mLangCurrencyLabel.setTypeface(mSettingActivity.robotoRegular);
            holder.mLangCurrencyLabel.setTextColor(Color.parseColor(CustomBackground.mThemeColor));
        }
        if (!SharedPreference.getInstance().getValue("currency_position").equals("0")) {
            if (position == Integer.valueOf(SharedPreference.getInstance().getValue("currency_position"))) {
                holder.mLangCurrencyLabel.setTypeface(mSettingActivity.robotoRegular);
                holder.mLangCurrencyLabel.setTextColor(Color.parseColor(CustomBackground.mThemeColor));
                SharedPreference.getInstance().save("currency_symbol", mCurrecy.get(position).getSign());
                SharedPreference.getInstance().save("currency_name", mCurrecy.get(position).getName());
                SharedPreference.getInstance().save("currency_position", String.valueOf(position));
                SharedPreference.getInstance().save("currency_iso_code", mCurrecy.get(position).getIsoCode());
                if(AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
                    SharedPreference.getInstance().save("id_currency", mCurrecy.get(position).getIsoCode());
                }else {
                    SharedPreference.getInstance().save("id_currency", mCurrecy.get(position).getIdCurrency());
                }
                /*mCategoryProductService.deleteCategoryProductServiceRecord();
                mProductService.deleteProductServiceRecord();*/
            }
        } else if (position == mSelected) {
            holder.mLangCurrencyLabel.setTypeface(mSettingActivity.robotoRegular);
            holder.mLangCurrencyLabel.setTextColor(Color.parseColor(CustomBackground.mThemeColor));
            SharedPreference.getInstance().save("currency_symbol", mCurrecy.get(position).getSign());
            SharedPreference.getInstance().save("currency_name", mCurrecy.get(position).getName());
            SharedPreference.getInstance().save("currency_position", String.valueOf(position));
            SharedPreference.getInstance().save("currency_iso_code", mCurrecy.get(position).getIsoCode());
            if(AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
                SharedPreference.getInstance().save("id_currency", mCurrecy.get(position).getIsoCode());
            }else {
                SharedPreference.getInstance().save("id_currency", mCurrecy.get(position).getIdCurrency());
            }
            /*mCategoryProductService.deleteCategoryProductServiceRecord();
            mProductService.deleteProductServiceRecord();*/
        }
        holder.mLanguageLayout.setTag(position);
        holder.mLanguageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelected = -1;
                mSelected = (Integer) v.getTag();
                notifyDataSetChanged();
                mCurrency = mCurrecy.get(position);
                holder.mActivateRight.setVisibility(View.VISIBLE);
                holder.mActivateRight.setImageBitmap(mFinalBitmap);
                holder.mLangCurrencyLabel.setTypeface(mSettingActivity.robotoRegular);
                holder.mLangCurrencyLabel.setTextColor(Color.parseColor(CustomBackground.mThemeColor));
                SharedPreference.getInstance().save("currency_symbol", mCurrency.getSign());
                SharedPreference.getInstance().save("currency_name", mCurrency.getName());
                SharedPreference.getInstance().save("currency_position", String.valueOf(position));
                SharedPreference.getInstance().save("currency_iso_code", mCurrecy.get(position).getIsoCode());

                if(AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)){
                    SharedPreference.getInstance().save("id_currency", mCurrecy.get(position).getIsoCode());
                    mMagentoCategoryProductService.deleteCategoryProductServiceRecord();
                    mMagentoProductService.deleteProductServiceRecord();
                }else {
                    SharedPreference.getInstance().save("id_currency", mCurrency.getIdCurrency());
                    mCategoryProductService.deleteCategoryProductServiceRecord();
                    mProductService.deleteProductServiceRecord();
                }

                mSettingActivity.cancelBtn.setText(R.string.okText);
                for (int i = 0; i < mCurrecy.size(); i++) {
                    if (position != i) {
                        holder.mActivateRight.setVisibility(View.GONE);
                    } /*else {
                        holder.mActivateRight.setVisibility(View.GONE);
                    }*/
                }
                mHompageController.isLangCurrency = true;
                //mHompageController.getCategoryList();
            }
        });
    }


    /**
     * The type Currency holder.
     */
    public class CurrencyHolder extends RecyclerViewHolders {


        public LinearLayout mLanguageLayout;
        public TextView mLangCurrencyLabel;
        public ImageView mActivateRight;

        /**
         * Instantiates a new Currency holder.
         *
         * @param v the v
         */
        public CurrencyHolder(View v) {
            super(v);
            this.mLanguageLayout = (LinearLayout) v.findViewById(R.id.language_list_layout);
            this.mLangCurrencyLabel = (TextView) v.findViewById(R.id.lang_currency_label);
            this.mActivateRight = (ImageView) v.findViewById(R.id.activate_icon);
        }
    }


    /**
     * @return int The size of currency array is returned
     */
    @Override
    public int getItemCount() {
        return this.mCurrecy.size();
    }

    /*Custom Icon*/
    private void customIcon() {
        Drawable sourceDrawable = mContext.getResources().getDrawable(R.drawable.ic_activate_right);
        Bitmap sourceBitmap = CustomIcon.convertDrawableToBitmap(sourceDrawable);
        mFinalBitmap = CustomIcon.changeImageColor(sourceBitmap, Color.parseColor(CustomBackground.mThemeColor));
    }
}
