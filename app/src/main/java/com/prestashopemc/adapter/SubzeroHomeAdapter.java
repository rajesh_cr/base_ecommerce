package com.prestashopemc.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.picasso.Picasso;
import com.prestashopemc.R;
import com.prestashopemc.controller.ProductListController;
import com.prestashopemc.customView.CustomTextView;
import com.prestashopemc.model.SubzeroHomeResult;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.view.MainActivity;
import com.prestashopemc.view.ProductDetailActivity;

import java.util.List;

/**
 * <h1>Subzero Home Adapter Contains Subzero Home Holder</h1>
 * The Subzero Home Adapter implements an adapter view for Subzero Home.
 * <p>
 * <b>Note:</b> Adapter values are get from Subzero Home Result model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class SubzeroHomeAdapter extends RecyclerView.Adapter<SubzeroHomeAdapter.SubzeroHomeListViewHolder> {

    private List<SubzeroHomeResult> mProductList;
    private Context mContext;
    private SubzeroHomeResult mProduct;
    private String mWishListStatus;
    private ProductListController mProductListController;
    private MainActivity mActivity;

    /**
     * Instantiates a new Subzero home adapter.
     *
     * @param context  the context
     * @param itemList the item list
     */
    public SubzeroHomeAdapter(Context context, List<SubzeroHomeResult> itemList) {
        this.mProductList = itemList;
        this.mContext = context;
    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public SubzeroHomeListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        mProductListController = new ProductListController(mContext);
        mActivity = (MainActivity)mContext;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.home_products_list, null);

        SubzeroHomeListViewHolder viewHolder = new SubzeroHomeListViewHolder(view);
        return viewHolder;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(final SubzeroHomeListViewHolder holder, final int position) {

        mProduct = mProductList.get(position);

        String currenySymbol = SharedPreference.getInstance().getValue("currency_symbol");
        mWishListStatus = SharedPreference.getInstance().getValue("wishlist_enable_status");
        setCustomFont(holder);
        Picasso.with(mContext).load(mProduct.getImageUrl())
                .placeholder(R.drawable.place_holder).resize(200, 200).centerCrop()
                .into(holder.mProductImage);
        holder.mProductName.setText(mProduct.getProductName());
        holder.mPrice.setText(currenySymbol + " " + mProduct.getSpecialprice());

        if (mWishListStatus != null) {
            if (mWishListStatus.equals("false")) {
                holder.mWishlistImage.setVisibility(View.GONE);
            }
        }

        if (!mProduct.getSpecialprice().equalsIgnoreCase(mContext.getString(R.string.not_set))) {
            holder.mSpecialPrice.setText(currenySymbol + " " + mProduct.getPrice());
        }else {
            holder.mPrice.setText(currenySymbol + " " + mProduct.getPrice());
            //holder.mSpecialPrice.setVisibility(View.GONE);
        }


        holder.mShortDescription.setText(mProduct.getDescriptionShort());
        holder.mAddToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProduct = mProductList.get(position);
                mActivity.showProgDialiog();
                mProductListController.addToCart(mProduct.getIdProduct(), "1", "0", mProduct.getProductName());

            }
        });

        holder.mSpecialPrice.setPaintFlags(holder.mSpecialPrice
                .getPaintFlags()
                | Paint.STRIKE_THRU_TEXT_FLAG);

        holder.mListMainlayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProduct = mProductList.get(position);
                Intent filterIntent = new Intent(mContext, ProductDetailActivity.class);
                filterIntent.putExtra("product_id", "" + mProduct.getIdProduct());
                filterIntent.putExtra("product_name", mProduct.getProductName());
                mContext.startActivity(filterIntent);
            }
        });

        if (!SharedPreference.getInstance().getValue("customerid").equals("0")) {
            if (mProduct.isWishlist()) {
                holder.mWishlistImage.setImageResource(R.drawable.ic_wishlist_enable);
            }else {
                holder.mWishlistImage.setImageResource(R.drawable.ic_wishlist_disable);
            }
        }

        holder.mWishlistImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProduct = mProductList.get(position);
                if (!SharedPreference.getInstance().getValue("customerid").equals("0")) {
                    if (!mProduct.isWishlist()) {
                        mActivity.showProgDialiog();
                        mProductListController.addWishList(mProduct.getIdProduct(), "addwishlist");
                    }else {
                        mActivity.showProgDialiog();
                        mProductListController.addWishList(mProduct.getIdProduct(), "deletewishlist");
                    }
                }else {
                    mActivity.snackBar(mContext.getString(R.string.needLoginText));
                }
            }
        });

    }

    /*Set Custom Font*/
    private void setCustomFont(SubzeroHomeListViewHolder holder) {
        holder.mProductName.setTypeface(mActivity.robotoRegular);
        holder.mShortDescription.setTypeface(mActivity.robotoRegular);
        holder.mPrice.setTypeface(mActivity.robotoMedium);
        holder.mSpecialPrice.setTypeface(mActivity.robotoLight);
    }


    /**
     * The type Subzero home list view holder.
     */
/*Initiated View Holder*/
    public class SubzeroHomeListViewHolder extends RecyclerViewHolders {

        public ImageView mProductImage, mAddToCart, mWishlistImage;
        public CustomTextView mProductName, mPrice, mSpecialPrice, mShortDescription;
        public RelativeLayout mTopLayout, mListMainlayout;


        /**
         * Instantiates a new Subzero home list view holder.
         *
         * @param v the v
         */
        public SubzeroHomeListViewHolder(View v) {
            super(v);
            this.mProductImage = (ImageView) v.findViewById(R.id.product_image);
            this.mProductName = (CustomTextView) v.findViewById(R.id.product_name);
            this.mPrice = (CustomTextView) v.findViewById(R.id.price);
            this.mSpecialPrice = (CustomTextView) v.findViewById(R.id.spl_price);
            this.mWishlistImage = (ImageView) v.findViewById(R.id.product_wishlist);

            mTopLayout = (RelativeLayout) v.findViewById(R.id.top_layout);
            this.mListMainlayout = (RelativeLayout) v.findViewById(R.id.main_layout);
            this.mShortDescription = (CustomTextView) v.findViewById(R.id.product_short_description);
            this.mAddToCart = (ImageView) v.findViewById(R.id.add_to_cart);
        }
    }


    /**
     * @return int the size of mProductList array
     */
    @Override
    public int getItemCount() {
        return this.mProductList.size();
    }

}
