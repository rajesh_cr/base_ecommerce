package com.prestashopemc.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.prestashopemc.R;
import com.prestashopemc.customView.CustomTextView;
import com.prestashopemc.model.Feature;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.view.MagentoProductDetailActivity;
import com.prestashopemc.view.ProductDetailActivity;

import java.util.List;

/**
 * <h1>Key Features Adapter Contains Key Features View Holder</h1>
 * The Key Features Adapter implements an adapter view for Key Features for particular Products.
 * <p>
 * <b>Note:</b> Adapter values are get from Feature model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class KeyFeaturesAdapter extends RecyclerView.Adapter<KeyFeaturesAdapter.KeyFeaturesViewHolder> {
    private List<Feature> mFeaturesList;
    private Context mContext;
    private Feature mFeature;
    private ProductDetailActivity mProductDetailActivity;
    private MagentoProductDetailActivity mMagentoPrdDetailActivity;

    /**
     * Instantiates a new Key features adapter.
     *
     * @param mContext     the m context
     * @param featuresList the features list
     */
    public KeyFeaturesAdapter(Context mContext, List<Feature> featuresList) {
        this.mFeaturesList = featuresList;
        this.mContext = mContext;
    }

    /**
     * @param parent   the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public KeyFeaturesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if (AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
            mMagentoPrdDetailActivity = (MagentoProductDetailActivity) mContext;
        } else {
            mProductDetailActivity = (ProductDetailActivity) mContext;

        }

        view = LayoutInflater.from(mContext).inflate(R.layout.keyfeatures_item, null);
        KeyFeaturesViewHolder viewHolder = new KeyFeaturesViewHolder(view);
        return viewHolder;
    }

    /**
     * @param holder   the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(KeyFeaturesViewHolder holder, int position) {
        mFeature = mFeaturesList.get(position);
        holder.mKeyfeatureName.setText(mFeature.getName());
        holder.mKeyfeatureValue.setText(mFeature.getValue());

        if (AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
            holder.mKeyfeatureName.setTypeface(mMagentoPrdDetailActivity.robotoRegular);
            holder.mKeyfeatureValue.setTypeface(mMagentoPrdDetailActivity.robotoRegular);
        } else {
            holder.mKeyfeatureName.setTypeface(mProductDetailActivity.robotoRegular);
            holder.mKeyfeatureValue.setTypeface(mProductDetailActivity.robotoRegular);
        }

    }

    /**
     * The type Key features view holder.
     */
    public class KeyFeaturesViewHolder extends RecyclerViewHolders {

        public CustomTextView mKeyfeatureName, mKeyfeatureValue;

        /**
         * Instantiates a new Key features view holder.
         *
         * @param v the v
         */
        public KeyFeaturesViewHolder(View v) {
            super(v);
            this.mKeyfeatureName = (CustomTextView) v.findViewById(R.id.keyfeature_name);
            this.mKeyfeatureValue = (CustomTextView) v.findViewById(R.id.keyfeature_value);
        }
    }

    /**
     * @return int the size of feature list array
     */
    @Override
    public int getItemCount() {
        return this.mFeaturesList.size();
    }
}
