package com.prestashopemc.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.controller.CheckoutController;
import com.prestashopemc.model.ChangeAddressModel;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.view.AddNewAddress;
import com.prestashopemc.view.ChangeAddress;

import java.util.List;

/**
 * Created by Rajesh on 30/9/16.
 */
public class ChangeAddressAdapter extends RecyclerView.Adapter<ChangeAddressAdapter.ChangeAddressHolder> {

    private List<ChangeAddressModel> mChangeAddressList;
    private Context mContext;
    private ChangeAddressModel mChangeAddressModel;
    private ChangeAddress mChangeAddress;
    private String mNameValue, mAddressValue;
    /**
     * The M name.
     */
    public String mName, /**
     * The M address.
     */
    mAddress, /**
     * The M id address.
     */
    mIdAddress;
    private CheckoutController mCheckoutController;
    private int mSelected = -1;

    /**
     * Instantiates a new Change address adapter.
     *
     * @param context        the context
     * @param mChangeAddress the m change address
     */
    public ChangeAddressAdapter(Context context, List<ChangeAddressModel> mChangeAddress) {
        this.mContext = context;
        this.mChangeAddressList = mChangeAddress;
    }

    @Override
    public ChangeAddressHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mChangeAddress = (ChangeAddress) mContext;
        mCheckoutController = new CheckoutController(mContext);
        View view = LayoutInflater.from(mContext).inflate(R.layout.change_address_list, null);
        ChangeAddressHolder viewHolder = new ChangeAddressHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ChangeAddressHolder holder, final int position) {
        mChangeAddressModel = mChangeAddressList.get(position);

        setCustomFont(holder);
        mNameValue = mChangeAddressModel.getFirstname() + " " + mChangeAddressModel.getLastname();
        mAddressValue = mChangeAddressModel.getStreet() + "\n" + mChangeAddressModel.getCity() + ", " + mChangeAddressModel.getCountry() + "\n"
                + mChangeAddressModel.getPostcode() + ", " + mChangeAddressModel.getTelephone();
        holder.mShippingName.setText(mNameValue);
        holder.mShippingAddress.setText(mAddressValue);

        holder.mRadioButton.setTag(position);
        holder.mRadioButton.setChecked(position == mSelected);
        holder.mRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mChangeAddressModel = mChangeAddressList.get(position);
                mSelected = (Integer) v.getTag();
                notifyDataSetChanged();
                mIdAddress = mChangeAddressModel.getIdAddress();
                mName = mChangeAddressModel.getFirstname() + " " + mChangeAddressModel.getLastname();
                mAddress = mChangeAddressModel.getStreet() + "\n" + mChangeAddressModel.getCity() + ", " + mChangeAddressModel.getCountry() + "\n"
                        + mChangeAddressModel.getPostcode() + ", " + mChangeAddressModel.getTelephone();
                //Toast.makeText(context, position + "- position", Toast.LENGTH_SHORT).show();
            }
        });
        holder.mDeleteIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                    if (mChangeAddressModel.isDefaultAddress()){
                        mChangeAddress.snackBar(AppConstants.getTextString(mContext, AppConstants.defaultDelete));
                    }else {
                        mChangeAddressModel = mChangeAddressList.get(position);
                        mChangeAddress.showProgDialiog();
                        mChangeAddressList.remove(position);
                        mCheckoutController.addressDelete(mChangeAddressModel.getIdAddress());
                    }
                }else {
                    mChangeAddressModel = mChangeAddressList.get(position);
                    mChangeAddress.showProgDialiog();
                    mChangeAddressList.remove(position);
                    mCheckoutController.addressDelete(mChangeAddressModel.getIdAddress());
                }
            }
        });
        holder.mEditIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mChangeAddressModel = mChangeAddressList.get(position);
                Intent intentAddNewAddrs = new Intent(mContext, AddNewAddress.class);
                intentAddNewAddrs.putExtra("fname", mChangeAddressModel.getFirstname());
                intentAddNewAddrs.putExtra("lname", mChangeAddressModel.getLastname());
                intentAddNewAddrs.putExtra("street", mChangeAddressModel.getStreet());
                intentAddNewAddrs.putExtra("postcode", mChangeAddressModel.getPostcode());
                intentAddNewAddrs.putExtra("city", mChangeAddressModel.getCity());
                intentAddNewAddrs.putExtra("telephone", mChangeAddressModel.getTelephone());
                intentAddNewAddrs.putExtra("idaddress", mChangeAddressModel.getIdAddress());
                intentAddNewAddrs.putExtra("country", mChangeAddressModel.getCountry());
                intentAddNewAddrs.putExtra("state", mChangeAddressModel.getRegion());
                intentAddNewAddrs.putExtra("edit", "edit");
                intentAddNewAddrs.putExtra("fromDefault", "fromDefaultEdit");
                if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                    intentAddNewAddrs.putExtra("isDefault",mChangeAddressModel.isDefaultAddress());
                }
                mContext.startActivity(intentAddNewAddrs);
            }
        });
    }

    /**
     * Declared Holder Variables
     */
    public class ChangeAddressHolder extends RecyclerViewHolders {


        public TextView mShippingName, mShippingAddress;
        public ImageView mDeleteIcon, mEditIcon;
        public RadioButton mRadioButton;

        /**
         * Instantiates a new Change address holder.
         *
         * @param v the v
         */
        public ChangeAddressHolder(View v) {
            super(v);
            this.mShippingName = (TextView) v.findViewById(R.id.shipping_name);
            this.mShippingAddress = (TextView) v.findViewById(R.id.ship_address);
            this.mDeleteIcon = (ImageView) v.findViewById(R.id.delete_address);
            this.mEditIcon = (ImageView) v.findViewById(R.id.change_edit);
            this.mRadioButton = (RadioButton) v.findViewById(R.id.radio_button);
        }
    }

    @Override
    public int getItemCount() {
        return mChangeAddressList.size();
    }

    /**
     * Set Custom Font
     */
    private void setCustomFont(ChangeAddressHolder holder) {
        holder.mShippingName.setTypeface(mChangeAddress.robotoRegular);
        holder.mShippingAddress.setTypeface(mChangeAddress.robotoLight);
    }
}
