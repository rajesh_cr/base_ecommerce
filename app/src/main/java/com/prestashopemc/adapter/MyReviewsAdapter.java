package com.prestashopemc.adapter;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.prestashopemc.model.MagentoReviewResult;
import com.prestashopemc.util.AppConstants;
import com.squareup.picasso.Picasso;
import com.prestashopemc.R;
import com.prestashopemc.model.MyReviewsResult;
import com.prestashopemc.view.MyReviewsActivity;

import java.util.List;

/**
 * <h1>My Reviews Adapter Contains My Reviews Holder</h1>
 * The My Reviews Adapter implements an adapter view for Customer Reviews.
 * <p>
 * <b>Note:</b> Adapter values are get from My Reviews Result model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class MyReviewsAdapter extends RecyclerView.Adapter<MyReviewsAdapter.MyReviewsHolder> {
    private Context mContext;
    private MyReviewsResult mMyReviewsResult;
    private List<MyReviewsResult> mMyReviewsResultList;
    private MyReviewsActivity mMyReviewsActivity;
    private MagentoReviewResult magentoReviewResult;
    private List<MagentoReviewResult> magentoReviewResults;

    /**
     * Instantiates a new My reviews adapter.
     *
     * @param context            the context
     * @param mMyOrderListResult the m my order list result
     */
    public MyReviewsAdapter(Context context, List<MyReviewsResult> mMyOrderListResult, List<MagentoReviewResult> magentoresults) {
        this.mContext = context;
        this.mMyReviewsResultList = mMyOrderListResult;
        this.magentoReviewResults = magentoresults;
    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public MyReviewsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        mMyReviewsActivity = (MyReviewsActivity) mContext;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.reviews_list_adapter, null);
        MyReviewsHolder viewHolder = new MyReviewsHolder(view);
        return viewHolder;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(final MyReviewsHolder holder, final int position) {

        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            magentoReviewResult = magentoReviewResults.get(position);
            setCustomFont(holder);
            Picasso.with(mContext).load(magentoReviewResult.getImage())
                    .placeholder(R.drawable.place_holder).resize(200, 200).centerCrop()
                    .into(holder.mProductImage);

            holder.mReviewsProductName.setText(magentoReviewResult.getProductname());
            holder.mReviewsTitle.setText(magentoReviewResult.getTitle());
            holder.mReviewsDate.setText("( " + magentoReviewResult.getCreatedAt() + " )");
            holder.mReviewsDescription.setText(magentoReviewResult.getDetail());

            holder.mRatingBar.setRating(magentoReviewResult.getRatingAvailable());
            LayerDrawable stars = (LayerDrawable) holder.mRatingBar.getProgressDrawable();
            stars.getDrawable(2).setColorFilter(mContext.getResources().getColor(R.color.review_star), PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(1).setColorFilter(mContext.getResources().getColor(R.color.review_star), PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(0).setColorFilter(mContext.getResources().getColor(R.color.review_star_unselected), PorterDuff.Mode.SRC_ATOP);
        }else {
            mMyReviewsResult = mMyReviewsResultList.get(position);
            setCustomFont(holder);
            Picasso.with(mContext).load(mMyReviewsResult.getImage())
                    .placeholder(R.drawable.place_holder).resize(200, 200).centerCrop()
                    .into(holder.mProductImage);

            holder.mReviewsProductName.setText(mMyReviewsResult.getProductname());
            holder.mReviewsTitle.setText(mMyReviewsResult.getTitle());
            holder.mReviewsDate.setText("( " + mMyReviewsResult.getCreatedAt() + " )");
            holder.mReviewsDescription.setText(mMyReviewsResult.getDetail());

            holder.mRatingBar.setRating(Float.parseFloat(mMyReviewsResult.getRatings()));
            LayerDrawable stars = (LayerDrawable) holder.mRatingBar.getProgressDrawable();
            stars.getDrawable(2).setColorFilter(mContext.getResources().getColor(R.color.review_star), PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(1).setColorFilter(mContext.getResources().getColor(R.color.review_star), PorterDuff.Mode.SRC_ATOP);
            stars.getDrawable(0).setColorFilter(mContext.getResources().getColor(R.color.review_star_unselected), PorterDuff.Mode.SRC_ATOP);
        }
    }

    /**
     * Set Custome Font
     */
    private void setCustomFont(MyReviewsHolder holder) {
        holder.mReviewsProductName.setTypeface(mMyReviewsActivity.robotoRegular);
        holder.mReviewsTitle.setTypeface(mMyReviewsActivity.robotoMedium);
        holder.mReviewsDate.setTypeface(mMyReviewsActivity.robotoRegular);
        holder.mReviewsDescription.setTypeface(mMyReviewsActivity.robotoLight);
    }

    /**
     * Holder
     */
    public class MyReviewsHolder extends RecyclerViewHolders {


        public ImageView mProductImage;

        public TextView mReviewsProductName, mReviewsTitle, mReviewsDate, mReviewsDescription;
        public RatingBar mRatingBar;

        /**
         * Instantiates a new My reviews holder.
         *
         * @param v the v
         */
        public MyReviewsHolder(View v) {
            super(v);
            this.mProductImage = (ImageView) v.findViewById(R.id.reviews_product_img);
            this.mReviewsProductName = (TextView) v.findViewById(R.id.reviews_product_name);
            this.mReviewsTitle = (TextView) v.findViewById(R.id.reviews_title);
            this.mReviewsDate = (TextView) v.findViewById(R.id.reviews_date);
            this.mReviewsDescription = (TextView) v.findViewById(R.id.reviews_description);
            this.mRatingBar = (RatingBar) v.findViewById(R.id.reviews_rating_bar);
        }
    }


    /**
     * @return int
     */
    @Override
    public int getItemCount() {
        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
            return this.magentoReviewResults.size();
        }else {
            return this.mMyReviewsResultList.size();
        }
    }
}
