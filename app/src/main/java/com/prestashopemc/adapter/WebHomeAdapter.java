package com.prestashopemc.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.holderviews.FirstSlotViewHolder;
import com.prestashopemc.holderviews.LineSeparatorViewHolder;
import com.prestashopemc.holderviews.SpaceSeperatorViewHolder;
import com.prestashopemc.holderviews.TextSlotViewHolder;
import com.prestashopemc.holderviews.TitleSlotViewHolder;
import com.prestashopemc.model.HomePageList;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.view.MainActivity;
import com.prestashopemc.webModel.ImageBorder;
import com.prestashopemc.webModel.SubTitle;

import java.util.List;

/**
 * <h1>Web Home Adapter Contains Web Home Holder</h1>
 * The Web Home Adapter implements an adapter view for Web Home.
 * <p>
 * <b>Note:</b> Adapter values are get from HomePageList model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class WebHomeAdapter extends RecyclerView.Adapter<RecyclerViewHolders>{
    private List<HomePageList> mMainWebModels;
    private Context mContext;
    private HomePageList mMainModel;
    private MainActivity mMainActivity;
    private String mIsoCode;

    /**
     * Instantiates a new Web home adapter.
     *
     * @param context       the context
     * @param itemMainModel the item main model
     */
    public WebHomeAdapter(Context context, List<HomePageList> itemMainModel){
        this.mContext = context;
        this.mMainWebModels = itemMainModel;
    }

    /**
     * @param position the position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        mMainModel = mMainWebModels.get(position);
        return mMainModel.getSlotnumber();
    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        mMainActivity = (MainActivity)mContext;
        //Log.e("View Type === ", viewType+"");
        if (viewType == 16){
            return new TitleSlotViewHolder(LayoutInflater.from(mContext).inflate(R.layout.title_separator_layout, parent, false));
        }else if (viewType == 17){
            return new LineSeparatorViewHolder(LayoutInflater.from(mContext).inflate(R.layout.line_separator_layout, parent, false));
        } else if (viewType == 18){
            return new SpaceSeperatorViewHolder(LayoutInflater.from(mContext).inflate(R.layout.space_seperator_layout, parent, false));
        } else if (viewType == 19){
            return new TextSlotViewHolder(LayoutInflater.from(mContext).inflate(R.layout.text_content_layout, parent, false));
        } else {
            return new FirstSlotViewHolder(LayoutInflater.from(mContext).inflate(R.layout.big_banner_layout, parent, false));
        }
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {
        mMainModel = mMainWebModels.get(position);
        int slotNumber = mMainModel.getSlotnumber();
        mIsoCode = SharedPreference.getInstance().getValue("iso_code");
        if (slotNumber == 16){
            mMainModel = mMainWebModels.get(position);
            TitleSlotViewHolder titleHolder = (TitleSlotViewHolder) holder;
            for (SubTitle subTitle : mMainModel.getTitle()){
                if (subTitle.getCode().equals(mIsoCode)) {
                    titleHolder.mTitleSlot.setText(subTitle.getValue());
                }
            }
            customTextSettings(mMainModel, titleHolder.mTitleSlot);
            ImageBorder mBorder = mMainModel.getBorder();
            if (!mMainModel.getBackgroundColor().isEmpty()){
                titleHolder.mTitleSlotLayout.setBackgroundColor(Color.parseColor(mMainModel.getBackgroundColor()));
            }
            if (!mMainModel.getHeight().isEmpty()){
                String[] layoutHeight = mMainModel.getHeight().split("px");
                int dpHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, Float.parseFloat(layoutHeight[0]), mContext.getResources().getDisplayMetrics());
                titleHolder.mTitleMainLayout.getLayoutParams().height = dpHeight;
            }
            if (!mBorder.getColor().isEmpty()) {
                setImageBorder(titleHolder.mTitleMainLayout, mBorder);
            }else {
                titleHolder.mTitleMainLayout.setBackgroundResource(0);
            }
        } else if (slotNumber == 17){
            mMainModel = mMainWebModels.get(position);
            ImageBorder mBorder = mMainModel.getBorder();
            LineSeparatorViewHolder lineHolder = (LineSeparatorViewHolder) holder;

            if (!mBorder.getColor().isEmpty()) {

                int borderWidth;
                GradientDrawable gd = new GradientDrawable();
                gd.setShape(GradientDrawable.LINE);
                gd.setColor(Color.parseColor(mBorder.getColor()));

                if (!mBorder.getWidth().isEmpty()){
                    String[] widthString = mBorder.getWidth().split("px");
                    float pixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, Float.parseFloat(widthString[0]), mContext.getResources().getDisplayMetrics());
                    borderWidth = (int) pixels;
                }else {
                    borderWidth = 2;
                }

                if (mBorder.getType().equals("dashed")) {
                    gd.setStroke(borderWidth, Color.parseColor(mBorder.getColor()), 20, 10);
                } else if (mBorder.getType().equals("dotted")) {
                    gd.setStroke(borderWidth, Color.parseColor(mBorder.getColor()), 3, 3);
                } else if (mBorder.getType().equals("solid")){
                    gd.setStroke(borderWidth, Color.parseColor(mBorder.getColor()));
                } else {
                    gd.setStroke(borderWidth, Color.parseColor(mBorder.getColor()));
                }
                lineHolder.mView.setBackground(gd);
                //Log.e("Border Height === ", borderWidth+"");
                lineHolder.mView.getLayoutParams().height = 8 + borderWidth;
            }
        } else if (slotNumber == 18){
            mMainModel = mMainWebModels.get(position);
            SpaceSeperatorViewHolder spaceHolder = (SpaceSeperatorViewHolder) holder;
            if (!mMainModel.getHeight().isEmpty()){
                String[] layoutHeight = mMainModel.getHeight().split("px");
                float pixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, Float.parseFloat(layoutHeight[0]), mContext.getResources().getDisplayMetrics());
                spaceHolder.mSpaceSeperatorLayout.getLayoutParams().height = (int) pixels;
            }
        }else if (slotNumber == 19){
            mMainModel = mMainWebModels.get(position);
            ImageBorder mBorder = mMainModel.getBorder();
            TextSlotViewHolder textHolder = (TextSlotViewHolder) holder;
            for (SubTitle subTitle : mMainModel.getTitle()){
                if (subTitle.getCode().equals(mIsoCode)) {
                    textHolder.mTextContent.setText(subTitle.getValue());
                }
            }
            customTextSettings(mMainModel, textHolder.mTextContent);
            if (!mMainModel.getHeight().isEmpty()){
                String[] layoutHeight = mMainModel.getHeight().split("px");
                int dpHeight = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, Float.parseFloat(layoutHeight[0]), mContext.getResources().getDisplayMetrics());
                textHolder.mTextMainLayout.getLayoutParams().height = dpHeight;
            }
            if (!mBorder.getColor().isEmpty()) {
                setImageBorder(textHolder.mTextMainLayout, mBorder);
            }
            if (!mMainModel.getBackgroundColor().isEmpty()){
                textHolder.mTextContentLayout.setBackgroundColor(Color.parseColor(mMainModel.getBackgroundColor()));
            }
        } else {
            mMainModel = mMainWebModels.get(position);
            FirstSlotViewHolder holder3 = (FirstSlotViewHolder) holder;
            holder3.mFirstSlotRecyclerView.setVisibility(View.VISIBLE);
            MyCustomLayoutManager mFirstLinearLayout = new MyCustomLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false);
            holder3.mFirstSlotRecyclerView.setLayoutManager(mFirstLinearLayout);
            holder3.mFirstSlotRecyclerView.smoothScrollToPosition(0);
            holder3.mFirstSlotRecyclerView.setHasFixedSize(true);
            WebBannerAdapter webBannerAdapter = new WebBannerAdapter(mContext, mMainModel.getImagelist(), slotNumber, mMainModel.getBorder());
            holder3.mFirstSlotRecyclerView.setAdapter(webBannerAdapter);
            holder3.mFirstSlotRecyclerView.setNestedScrollingEnabled(true);
        }
    }

    /*Custom Text Settings*/
    private void customTextSettings(HomePageList homePageList, TextView textView) {
        if (!homePageList.getTitle().isEmpty()) {
            String[] txtSize = homePageList.getTextsize().split("px");
            textView.setVisibility(View.VISIBLE);
            if (!homePageList.getTextsize().isEmpty())
                textView.setTextSize(Float.parseFloat(txtSize[0]));
            if (!homePageList.getTextcolor().isEmpty())
                textView.setTextColor(Color.parseColor(homePageList.getTextcolor()));
            if (homePageList.getItalic())
                textView.setTypeface(null, Typeface.ITALIC);
            if (homePageList.getBold())
                textView.setTypeface(null, Typeface.BOLD);
            if (homePageList.getUnderline()) {
                textView.setPaintFlags(textView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
            }else {
                textView.setPaintFlags(textView.getPaintFlags() & (~ Paint.UNDERLINE_TEXT_FLAG));
            }
            if (!homePageList.getTexttype().isEmpty())
                setCustomFont(textView, homePageList);
            if (homePageList.getTextalign().contains("center")) {
                textView.setGravity(Gravity.CENTER);
            } else if (homePageList.getTextalign().contains("justify")) {
                textView.setGravity(Gravity.CENTER);
            }else if (homePageList.getTextalign().contains("right")) {
                textView.setGravity(Gravity.RIGHT);
            } else {
                textView.setGravity(Gravity.LEFT);
            }
        }else {
            textView.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return this.mMainWebModels.size();
    }

    /*Image ImageBorder*/
    private void setImageBorder(View v, ImageBorder mBorder){
        int borderWidth;
        if (!mBorder.getWidth().isEmpty()){
            String[] widthString = mBorder.getWidth().split("px");
            float pixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, Float.parseFloat(widthString[0]), mContext.getResources().getDisplayMetrics());
            borderWidth = (int) pixels;
        }else {
            borderWidth = 2;
        }
        String styleType = mBorder.getStyleType();
        if (styleType.equals("right-left") || styleType.equals("left-right"))
            v.setPadding(borderWidth, 0, borderWidth, 0);
        else if (styleType.equals("right"))
            v.setPadding(0, 0, borderWidth, 0);
        else if (styleType.equals("left"))
            v.setPadding(borderWidth, 0, 0, 0);
        else if (styleType.equals("top-bottom") || styleType.equals("bottom-top"))
            v.setPadding(0, borderWidth, 0, borderWidth);
        else if (styleType.equals("bottom"))
            v.setPadding(0, 0, 0, borderWidth);
        else if (styleType.equals("top"))
            v.setPadding(0, borderWidth, 0, 0);
        else if (styleType.equals("left-bottom") || styleType.equals("bottom-left"))
            v.setPadding(borderWidth, 0, 0, borderWidth);
        else if (styleType.equals("right-bottom") || styleType.equals("bottom-right"))
            v.setPadding(0, 0, borderWidth, borderWidth);
        else if (styleType.equals("left-top") || styleType.equals("top-left"))
            v.setPadding(borderWidth, borderWidth, 0, 0);
        else if (styleType.equals("right-top") || styleType.equals("top-right"))
            v.setPadding(0, borderWidth, borderWidth, 0);
        else
            v.setPadding(borderWidth, borderWidth, borderWidth, borderWidth);

        GradientDrawable gd = new GradientDrawable();
        gd.setGradientType(GradientDrawable.RECTANGLE);

        if ((!mBorder.getType().isEmpty() && !mBorder.getWidth().isEmpty()) || !mBorder.getType().isEmpty()) {
            if (mBorder.getType().equals("dashed")) {
                gd.setStroke(borderWidth, Color.parseColor(mBorder.getColor()), 15, 10);
            } else if (mBorder.getType().equals("dotted")) {
                gd.setStroke(borderWidth, Color.parseColor(mBorder.getColor()), 4, 4);
            } else if (mBorder.getType().equals("solid")){
                gd.setStroke(borderWidth, Color.parseColor(mBorder.getColor()));
            } else {
                gd.setStroke(borderWidth, Color.parseColor(mBorder.getColor()));
            }
        }else {
            gd.setStroke(borderWidth, Color.parseColor(mBorder.getColor()));
        }

        v.setBackground(gd);
    }

    /*Text Custom Font setup*/
    private void setCustomFont(TextView v, HomePageList homePageList){

        String textType = homePageList.getTexttype();
        boolean isItalic = homePageList.getItalic();
        boolean isBold = homePageList.getBold();

        if (textType.contains("roboto") && isBold && !isItalic)
            v.setTypeface(mMainActivity.robotoBold);
        else if (textType.contains("roboto") && isItalic && !isBold)
            v.setTypeface(mMainActivity.robotoItalic);
        else if (textType.contains("roboto") && !isItalic && !isBold)
            v.setTypeface(mMainActivity.robotoRegular);
        else if (textType.contains("oxygen") && isBold && !isItalic)
            v.setTypeface(mMainActivity.oxygenBold);
        else if (textType.contains("oxygen") && isItalic && !isBold)
            v.setTypeface(mMainActivity.oxygenItalic);
        else if (textType.contains("oxygen") && !isItalic && !isBold)
            v.setTypeface(mMainActivity.oxygenRegular);
        else if (textType.contains("lato") && isBold && !isItalic)
            v.setTypeface(mMainActivity.latoBold);
        else if (textType.contains("lato") && isItalic && !isBold)
            v.setTypeface(mMainActivity.latoItalic);
        else if (textType.contains("lato") && !isItalic && !isBold)
            v.setTypeface(mMainActivity.latoRegular);
        else if (textType.contains("open-sans") && isBold && !isItalic)
            v.setTypeface(mMainActivity.openSansBold);
        else if (textType.contains("open-sans") && isItalic && !isBold)
            v.setTypeface(mMainActivity.openSansItalic);
        else if (textType.contains("open-sans") && !isItalic && !isBold)
            v.setTypeface(mMainActivity.opensansRegular);
        else if (textType.contains("raleway") && isBold && !isItalic)
            v.setTypeface(mMainActivity.ralewayBold);
        else if (textType.contains("raleway") && isItalic && !isBold)
            v.setTypeface(mMainActivity.ralewayItalic);
        else if (textType.contains("raleway") && !isItalic && !isBold)
            v.setTypeface(mMainActivity.ralewayRegular);
    }

}
