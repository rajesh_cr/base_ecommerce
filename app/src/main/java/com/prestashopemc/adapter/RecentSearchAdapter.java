package com.prestashopemc.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.database.RecentSearchService;
import com.prestashopemc.model.RecentSearchModel;
import com.prestashopemc.view.RecentSearchActivity;
import com.prestashopemc.view.SearchActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Recent Search Adapter Contains Recent Search Holder</h1>
 * The Recent Search Adapter implements an adapter view for Recent Search Result.
 * <p>
 * <b>Note:</b> Adapter values are get from Recent Search Result model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class RecentSearchAdapter extends RecyclerView.Adapter<RecentSearchAdapter.RecentSearchHolder> {

    private Context mContext;
    private List<RecentSearchModel> mRecentSearchModelList = new ArrayList<RecentSearchModel>();
    private RecentSearchModel mRecentSearchModel;
    private RecentSearchActivity mRecentSearchActivity;
    private RecentSearchService mRecentSearchService;

    /**
     * Instantiates a new Recent search adapter.
     *
     * @param context        the context
     * @param paymentMethods the payment methods
     */
    public RecentSearchAdapter(Context context, List<RecentSearchModel> paymentMethods){
        this.mContext = context;
        this.mRecentSearchModelList = paymentMethods;
    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public RecentSearchHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.recent_search_list_adapter, null);
        mRecentSearchActivity = (RecentSearchActivity) mContext;
        mRecentSearchService = new RecentSearchService(mContext);
        RecentSearchHolder viewHolder = new RecentSearchHolder(view);
        return viewHolder;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(final RecentSearchHolder holder, final int position) {
        mRecentSearchModel = mRecentSearchModelList.get(position);
        holder.mRecentText.setText(mRecentSearchModel.getSearchKeyword());
        holder.mRecentText.setTypeface(mRecentSearchActivity.robotoLight);

        holder.mRecentText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRecentSearchModel = mRecentSearchModelList.get(position);
                Intent searchIntent = new Intent(mContext, SearchActivity.class);
                searchIntent.putExtra("search_text", mRecentSearchModel.getSearchKeyword());
                mContext.startActivity(searchIntent);
                mRecentSearchActivity.finish();
            }
        });

        holder.mCloseIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRecentSearchModel = mRecentSearchModelList.get(position);
                try {
                    mRecentSearchModelList.remove(position);
                    mRecentSearchService.deleteItem(mRecentSearchModel.getId());
                    RecentSearchAdapter.this.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Declared View Holder Variables
     */
    public class RecentSearchHolder extends RecyclerViewHolders {

        public TextView mRecentText;
        public ImageView mCloseIcon;

        /**
         * Instantiates a new Recent search holder.
         *
         * @param v the v
         */
        public RecentSearchHolder(View v) {
            super(v);
            this.mRecentText = (TextView) v.findViewById(R.id.recent_search_text);
            this.mCloseIcon = (ImageView) v.findViewById(R.id.recent_close_icon);
        }
    }

    /**
     * @return int
     */
    @Override
    public int getItemCount() {
        return mRecentSearchModelList.size();
    }
}
