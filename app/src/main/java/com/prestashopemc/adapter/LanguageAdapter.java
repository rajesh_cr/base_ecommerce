package com.prestashopemc.adapter;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.controller.HomePageController;
import com.prestashopemc.database.BannerService;
import com.prestashopemc.database.CategoryProductService;
import com.prestashopemc.database.CategoryService;
import com.prestashopemc.database.CmsService;
import com.prestashopemc.database.CurrencyService;
import com.prestashopemc.database.LanguageService;
import com.prestashopemc.database.MagentoCategoryProductService;
import com.prestashopemc.database.MagentoProductService;
import com.prestashopemc.database.MultipleLanguageService;
import com.prestashopemc.database.ProductService;
import com.prestashopemc.model.Language;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.CustomIcon;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.view.SettingActivity;

import java.util.List;
import java.util.Locale;

/**
 * <h1>Language Adapter Contains Language Holder</h1>
 * The Language Adapter implements an adapter view for Languages.
 * <p>
 * <b>Note:</b> Adapter values are get from Language model.
 * The Category Product Service and  Product Service are deleted when Language is change
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.LanguageHolder> {

    private List<Language> mLanguages;
    private Context mContext;
    private Language mLanguage;
    private SettingActivity mSettingActivity;
    private int mSelected = 0;
    private Bitmap mFinalBitmap;
    private CategoryProductService mCategoryProductService;
    private ProductService mProductService;
    private HomePageController mHompageController;
    private MagentoCategoryProductService mMagentoCategoryProductService;
    private MagentoProductService mMagentoProductService;

    /**
     * Instantiates a new Language adapter.
     *
     * @param mContext      the m context
     * @param mLanguageList the m language list
     */
    public LanguageAdapter(Context mContext, List<Language> mLanguageList) {
        this.mLanguages = mLanguageList;
        this.mContext = mContext;
    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public LanguageHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        mSettingActivity = (SettingActivity) mContext;
        if(AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)){
            mMagentoCategoryProductService = new MagentoCategoryProductService(mContext);
            mMagentoProductService = new MagentoProductService(mContext);
        }else {
            mCategoryProductService = new CategoryProductService(mContext);
            mProductService = new ProductService(mContext);
        }
        mHompageController = new HomePageController(mContext);
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.language_currency_adapter, null);
        String defaultLang = SharedPreference.getInstance().getValue("id_language");
        for (int j = 0; j < mLanguages.size(); j++) {
            if (mLanguages.get(j).getIdLang().equals(defaultLang)) {
                mSelected = j;
            }
        }
        LanguageHolder viewHolder = new LanguageHolder(view);
        return viewHolder;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(final LanguageHolder holder, final int position) {
        mLanguage = mLanguages.get(position);
        customIcon();
        holder.mLangCurrencyLabel.setText(mLanguage.getName());
        holder.mLangCurrencyLabel.setTypeface(mSettingActivity.robotoLight);
        holder.mLangCurrencyLabel.setTag(position);
        holder.mLangCurrencyLabel.setTextColor(mContext.getResources().getColor(R.color.lang_list_color));
        holder.mActivateRight.setVisibility(View.GONE);

        if (position == mSelected) {
            holder.mActivateRight.setVisibility(View.VISIBLE);
            holder.mActivateRight.setImageBitmap(mFinalBitmap);
            holder.mLangCurrencyLabel.setTypeface(mSettingActivity.robotoRegular);
            holder.mLangCurrencyLabel.setTextColor(Color.parseColor(CustomBackground.mThemeColor));
        }
        if (!SharedPreference.getInstance().getValue("language_position").equals("0")) {
            if (position == Integer.valueOf(SharedPreference.getInstance().getValue("language_position"))) {
                holder.mLangCurrencyLabel.setTypeface(mSettingActivity.robotoRegular);
                holder.mLangCurrencyLabel.setTextColor(Color.parseColor(CustomBackground.mThemeColor));
                SharedPreference.getInstance().save("language_name", mLanguages.get(position).getName());
                SharedPreference.getInstance().save("id_language", mLanguages.get(position).getIdLang());
                SharedPreference.getInstance().save("language_position", String.valueOf(position));
                SharedPreference.getInstance().save("iso_code", mLanguages.get(position).getIsoCode());
                /*mCategoryProductService.deleteCategoryProductServiceRecord();
                mProductService.deleteProductServiceRecord();*/
            }
        } else if (position == mSelected) {
            holder.mLangCurrencyLabel.setTypeface(mSettingActivity.robotoRegular);
            holder.mLangCurrencyLabel.setTextColor(Color.parseColor(CustomBackground.mThemeColor));
            SharedPreference.getInstance().save("language_name", mLanguages.get(position).getName());
            SharedPreference.getInstance().save("id_language", mLanguages.get(position).getIdLang());
            SharedPreference.getInstance().save("language_position", String.valueOf(position));
            SharedPreference.getInstance().save("iso_code", mLanguages.get(position).getIsoCode());
           /* mCategoryProductService.deleteCategoryProductServiceRecord();
            mProductService.deleteProductServiceRecord();*/
        }

        holder.mLanguageLayout.setTag(position);
        holder.mLanguageLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelected = -1;
                mSelected = (Integer) v.getTag();
                notifyDataSetChanged();
                mLanguage = mLanguages.get(position);
                holder.mActivateRight.setVisibility(View.VISIBLE);
                holder.mActivateRight.setImageBitmap(mFinalBitmap);
                holder.mLangCurrencyLabel.setTypeface(mSettingActivity.robotoRegular);
                holder.mLangCurrencyLabel.setTextColor(Color.parseColor(CustomBackground.mThemeColor));
                SharedPreference.getInstance().save("language_name", mLanguage.getName());
                SharedPreference.getInstance().save("id_language", mLanguage.getIdLang());
                SharedPreference.getInstance().save("language_position", String.valueOf(position));
                SharedPreference.getInstance().save("iso_code", mLanguages.get(position).getIsoCode());
                mSettingActivity.cancelBtn.setText(mContext.getString(R.string.okText));

                if(AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)){
                    mMagentoCategoryProductService.deleteCategoryProductServiceRecord();
                    mMagentoProductService.deleteProductServiceRecord();
                }else {
                    mCategoryProductService.deleteCategoryProductServiceRecord();
                    mProductService.deleteProductServiceRecord();
                }


                /**
                 * Custom language settings */
                Locale locale = new Locale(mLanguages.get(position).getIsoCode());
                Locale.setDefault(locale);
                Configuration config = new Configuration();
                config.locale = locale;
                mContext.getResources().updateConfiguration(config, mContext.getResources().getDisplayMetrics());

                for (int i = 0; i < mLanguages.size(); i++) {
                    if (position == i) {
                        holder.mActivateRight.setVisibility(View.VISIBLE);
                    } else {
                        holder.mActivateRight.setVisibility(View.GONE);
                    }
                }
                mHompageController.isLangCurrency = true;
                //mHompageController.getCategoryList();
            }
        });
    }

    /**
     * The type Language holder.
     */
    public class LanguageHolder extends RecyclerViewHolders {


        public LinearLayout mLanguageLayout;
        public TextView mLangCurrencyLabel;
        public ImageView mActivateRight;

        /**
         * Instantiates a new Language holder.
         *
         * @param v the v
         */
        public LanguageHolder(View v) {
            super(v);
            this.mLanguageLayout = (LinearLayout) v.findViewById(R.id.language_list_layout);
            this.mLangCurrencyLabel = (TextView) v.findViewById(R.id.lang_currency_label);
            this.mActivateRight = (ImageView) v.findViewById(R.id.activate_icon);
        }
    }

    /**
     * @return
     */
    @Override
    public int getItemCount() {
        return this.mLanguages.size();
    }

    /*Custom Icon*/
    private void customIcon() {
        Drawable sourceDrawable = mContext.getResources().getDrawable(R.drawable.ic_activate_right);
        Bitmap sourceBitmap = CustomIcon.convertDrawableToBitmap(sourceDrawable);
        mFinalBitmap = CustomIcon.changeImageColor(sourceBitmap, Color.parseColor(CustomBackground.mThemeColor));
    }
}
