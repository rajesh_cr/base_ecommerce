package com.prestashopemc.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarFinalValueListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.prestashopemc.R;
import com.prestashopemc.customView.CustomTextView;
import com.prestashopemc.model.FilterOption;
import com.prestashopemc.model.FilterResult;
import com.prestashopemc.model.NavigationFilterPrices;
import com.prestashopemc.model.NavigationFilterResult;
import com.prestashopemc.model.NavigationFilterValues;
import com.prestashopemc.model.NavigationFilterWeight;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.view.FilterActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <h1>Filter Expandable Adapter Contains Filter Expandable Holder</h1>
 * The Filter Expandable Adapter implements an adapter view for types of filters.
 * <p>
 * <b>Note:</b> Adapter values are get from Filter Result model.
 *
 * @author Rajesh
 * @version 1.2
 * @since 21/4/17
 */

public class NavigationFilterAdapter extends ExpandableRecyclerAdapter<NavigationFilterAdapter.MyParentViewHolder, NavigationFilterAdapter.MyChildViewHolder> {
    private LayoutInflater mInflater;
    private Context mContext;
    public String seekBarName, mAttributeType, mAttributeId;
    public static String mAvailability;
    public boolean isPriceRange = false, isWeightRange = false;
    private NavigationFilterPrices navigationFilterPrices;
    private NavigationFilterWeight navigationFilterWeight;
    /**
     * The constant mSelectedFilter.
     */
    public static ArrayList<String> mFeatureArray = new ArrayList<>();
    public static ArrayList<String> mAttributeArray = new ArrayList<>();
    private FilterActivity mFilterActivity;
    public static JSONObject mPriceObject = new JSONObject();
    public static JSONObject mWeightObject = new JSONObject();
    public static ArrayList<String> mConditionObject = new ArrayList<>();
    public static ArrayList<String> mManufacturesObject = new ArrayList<>();

    /**
     * FilterExpandableAdapter constructor
     *
     * @param context        the context
     * @param parentItemList the parent item list
     */
    public NavigationFilterAdapter(Context context, List<ParentListItem> parentItemList) {
        super(parentItemList);
        this.mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    /**
     * @param viewGroup
     * @return
     */
    @Override
    public MyParentViewHolder onCreateParentViewHolder(ViewGroup viewGroup) {
        mFilterActivity = (FilterActivity) mContext;
        View view = mInflater.inflate(R.layout.filter_parent_layout, viewGroup, false);
        return new MyParentViewHolder(view);
    }

    /**
     * @param viewGroup
     * @return
     */
    @Override
    public MyChildViewHolder onCreateChildViewHolder(ViewGroup viewGroup) {
        View view = mInflater.inflate(R.layout.filter_child_layout, viewGroup, false);
        return new MyChildViewHolder(view);
    }

    /**
     * @param parentViewHolder The {@code PVH} to bind data to
     * @param i
     * @param parentListItem   The {@link ParentListItem} which holds the data to
     */
    public void onBindParentViewHolder(final MyParentViewHolder parentViewHolder, final int i, final ParentListItem parentListItem) {
        final NavigationFilterResult headerParentListItem = (NavigationFilterResult) parentListItem;

        parentViewHolder.mFilterName.setText(headerParentListItem.getName());

        if (FilterActivity.sFilter.equals("productList")) {
            parentViewHolder.mExpandableImage.setImageResource(R.drawable.expandable_plus);
        } else if (FilterActivity.sFilter.equals("searchList")) {
            parentViewHolder.mExpandableImage.setImageResource(R.drawable.ic_cart_bottom);
        }
        parentViewHolder.mRangeSeekBarLayout.setVisibility(View.GONE);
        parentViewHolder.mParentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!parentViewHolder.isExpanded()) {
                    parentViewHolder.expandView();
                    mAttributeType = headerParentListItem.getType();
                    seekBarName = headerParentListItem.getName();
                    mAttributeId = headerParentListItem.getId();
                    if (FilterActivity.sFilter.equals("productList")) {
                        parentViewHolder.mExpandableImage.setImageResource(R.drawable.expandable_minus);
                    } else if (FilterActivity.sFilter.equals("searchList")) {
                        parentViewHolder.mExpandableImage.setImageResource(R.drawable.ic_cart_top);
                    }
                    if(seekBarName.equals("prices")){
                        isPriceRange = true;
                        navigationFilterPrices = headerParentListItem.getPrices();
                        loadPriceRangeBar(parentViewHolder, navigationFilterPrices, i);
                    } else if(seekBarName.equals("weight")){
                        isWeightRange = true;
                        navigationFilterWeight = headerParentListItem.getWeight();
                        loadWeightRangeBar(parentViewHolder, navigationFilterWeight, i);
                    }
                } else {
                    parentViewHolder.collapseView();
                    seekBarName = headerParentListItem.getName();
                    if (FilterActivity.sFilter.equals("productList")) {
                        parentViewHolder.mExpandableImage.setImageResource(R.drawable.expandable_plus);
                    } else if (FilterActivity.sFilter.equals("searchList")) {
                        parentViewHolder.mExpandableImage.setImageResource(R.drawable.ic_cart_bottom);
                    }
                    if (seekBarName.equals("prices") && isPriceRange){
                        parentViewHolder.mRangeSeekBarLayout.setVisibility(View.GONE);
                        isPriceRange = false;
                    }else if (seekBarName.equals("weight") && isWeightRange){
                        parentViewHolder.mRangeSeekBarLayout.setVisibility(View.GONE);
                        isWeightRange = false;
                    }
                }
            }
        });

        if (FilterActivity.sFilter.equals("productList")) {
            parentViewHolder.mFilterName.setTypeface(mFilterActivity.robotoRegular);
        } else if (FilterActivity.sFilter.equals("searchList")) {
            parentViewHolder.mFilterName.setTextColor(mContext.getResources().getColor(R.color.attribute_text_name));
            parentViewHolder.mFilterName.setTypeface(mFilterActivity.robotoRegular);
            parentViewHolder.mParentLayout.setBackgroundColor(mContext.getResources().getColor(R.color.attribute_bg_color));
        }
    }

    /**
     * @param childViewHolder The {@code CVH} to bind data to
     * @param position        the position        The index in the list at which to bind
     * @param childListItem   The child list item which holds that data to be
     */
    @Override
    public void onBindChildViewHolder(final MyChildViewHolder childViewHolder, final int position, final Object childListItem) {
        final NavigationFilterValues filterChildListItem = (NavigationFilterValues) childListItem;
        childViewHolder.mCheckBoxLayout.setVisibility(View.VISIBLE);
        childViewHolder.mFilterName.setText(filterChildListItem.getLabel());

        if (FilterActivity.sFilter.equals("productList")) {
            childViewHolder.mFilterName.setTypeface(mFilterActivity.robotoRegular);
        } else if (FilterActivity.sFilter.equals("searchList")) {
            childViewHolder.mFilterName.setTextColor(mContext.getResources().getColor(R.color.attribute_text_name));
            childViewHolder.mFilterName.setTypeface(mFilterActivity.robotoLight);
        }

        if (mAttributeArray.size() != 0) {
            if (mAttributeArray.contains(filterChildListItem.getValue())) {
                childViewHolder.mCheckBox.setChecked(true);
            }
        }else if (mFeatureArray.size() != 0) {
            if (mFeatureArray.contains(filterChildListItem.getValue())) {
                childViewHolder.mCheckBox.setChecked(true);
            }
        } else if (mConditionObject.size() != 0) {
            if (mConditionObject.contains(filterChildListItem.getValue())) {
                childViewHolder.mCheckBox.setChecked(true);
            }
        }else if (mManufacturesObject.size() != 0) {
            if (mManufacturesObject.contains(filterChildListItem.getValue())) {
                childViewHolder.mCheckBox.setChecked(true);
            }
        }

        CustomBackground.setCheckBoxColor(childViewHolder.mCheckBox);
        final List<NavigationFilterValues> navigationFilterValues = new ArrayList<NavigationFilterValues>();
        NavigationFilterValues navigationFilterValues1 = new NavigationFilterValues();
        navigationFilterValues1.setValue(filterChildListItem.getLabel());
        navigationFilterValues.add(navigationFilterValues1);
        childViewHolder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                boolean boxCheck = childViewHolder.mCheckBox.isChecked();
                if (boxCheck) {
                    childViewHolder.mCheckBox.setTag(position);
                    CustomBackground.setCheckBoxColor(childViewHolder.mCheckBox);

                    if (mAttributeType.equals("features")){
                        mFeatureArray.add(filterChildListItem.getValue());
                    }else if (mAttributeType.equals("attributes")){
                        mAttributeArray.add(filterChildListItem.getValue());
                    }else if (mAttributeType.equals("condition")){
                        mConditionObject.add(filterChildListItem.getValue());
                    }else if (mAttributeType.equals("manufacturers")){
                        mManufacturesObject.add(filterChildListItem.getValue());
                    }else if (mAttributeType.equals("availability")){
                        mAvailability = filterChildListItem.getValue();
                    }
                } else {
                    childViewHolder.mCheckBox.setTag(position);
                    CustomBackground.setCheckBoxColor(childViewHolder.mCheckBox);
                    if (mAttributeType.equals("features")){
                        mAttributeArray.remove(filterChildListItem.getValue());
                    }else if (mAttributeType.equals("attributes")){
                        mFeatureArray.remove(filterChildListItem.getValue());
                    }else if (mAttributeType.equals("condition")){
                        mConditionObject.remove(filterChildListItem.getValue());
                    }else if (mAttributeType.equals("manufacturers")){
                        mManufacturesObject.remove(filterChildListItem.getValue());
                    }else if (mAttributeType.equals("availability")){
                        mAvailability="";
                    }
                }
            }
        });
    }

    /**
     * Parent view holder for Filter Attribute
     */
    public class MyParentViewHolder extends ParentViewHolder {

        public CustomTextView mFilterName;
        public ImageView mExpandableImage;
        public RelativeLayout mParentLayout, mRangeSeekBarLayout;
        public CrystalRangeSeekbar mRangeSeekBar;
        public TextView mMinPrice, mMaxPrice;

        /**
         * Instantiates a new My parent view holder.
         *
         * @param itemView the item view
         */
        public MyParentViewHolder(View itemView) {
            super(itemView);
            mFilterName = (CustomTextView) itemView.findViewById(R.id.filter_header_name);
            mParentLayout = (RelativeLayout) itemView.findViewById(R.id.parent_filter_layout);
            mExpandableImage = (ImageView) itemView.findViewById(R.id.item_plus);
            mRangeSeekBar = (CrystalRangeSeekbar) itemView.findViewById(R.id.rangeSeekbar);
            mMinPrice = (TextView) itemView.findViewById(R.id.min_price);
            mMaxPrice = (TextView) itemView.findViewById(R.id.max_price);
            mRangeSeekBarLayout = (RelativeLayout) itemView.findViewById(R.id.seekbar_layout);
        }
    }

    /**
     * Child view holder for Filter Attribute
     */
    public class MyChildViewHolder extends ChildViewHolder {

        public CustomTextView mFilterName;
        public AppCompatCheckBox mCheckBox;
        public RelativeLayout mCheckBoxLayout;

        /**
         * Instantiates a new My child view holder.
         *
         * @param itemView the item view
         */
        public MyChildViewHolder(View itemView) {
            super(itemView);
            mFilterName = (CustomTextView) itemView.findViewById(R.id.filter_child_name);
            mCheckBox = (AppCompatCheckBox) itemView.findViewById(R.id.child_checkBox);
            mCheckBoxLayout = (RelativeLayout) itemView.findViewById(R.id.child_checkbox_layout);
        }
    }

    /**
     * Price Range Seekbar method*/
    private void loadPriceRangeBar(final MyParentViewHolder parentViewHolder, NavigationFilterPrices navigationFilterPrices, int position) {
        parentViewHolder.mRangeSeekBarLayout.setTag(position);
        parentViewHolder.mRangeSeekBarLayout.setVisibility(View.VISIBLE);
        parentViewHolder.mRangeSeekBar.setBarHighlightColor(Color.parseColor(CustomBackground.mThemeColor));
        parentViewHolder.mRangeSeekBar.setBarColor(Color.LTGRAY);
        parentViewHolder.mRangeSeekBar.setLeftThumbColor(Color.parseColor(CustomBackground.mPrimaryThemeColor));
        parentViewHolder.mRangeSeekBar.setRightThumbColor(Color.parseColor(CustomBackground.mPrimaryThemeColor));

        parentViewHolder.mRangeSeekBar.setMaxValue(Float.parseFloat(navigationFilterPrices.getMaxValue()));
        parentViewHolder.mRangeSeekBar.setMinValue(Float.parseFloat(navigationFilterPrices.getMinValue()));

        parentViewHolder.mRangeSeekBar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                parentViewHolder.mMaxPrice.setText("RS " + String.valueOf(maxValue));
                parentViewHolder.mMinPrice.setText("RS " + String.valueOf(minValue));
            }
        });
        parentViewHolder.mRangeSeekBar.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number minValue, Number maxValue) {
                parentViewHolder.mMaxPrice.setText("RS " + String.valueOf(maxValue));
                parentViewHolder.mMinPrice.setText("RS " + String.valueOf(minValue));
                try {
                    mPriceObject.put("max_price", String.valueOf(maxValue));
                    mPriceObject.put("min_price", String.valueOf(minValue));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Weight Range Seekbar method*/
    private void loadWeightRangeBar(final MyParentViewHolder parentViewHolder, NavigationFilterWeight navigationFilterWeight, int position) {
        parentViewHolder.mRangeSeekBarLayout.setTag(position);
        parentViewHolder.mRangeSeekBarLayout.setVisibility(View.VISIBLE);
        parentViewHolder.mRangeSeekBar.setBarHighlightColor(Color.parseColor(CustomBackground.mThemeColor));
        parentViewHolder.mRangeSeekBar.setBarColor(Color.LTGRAY);
        parentViewHolder.mRangeSeekBar.setLeftThumbColor(Color.parseColor(CustomBackground.mPrimaryThemeColor));
        parentViewHolder.mRangeSeekBar.setRightThumbColor(Color.parseColor(CustomBackground.mPrimaryThemeColor));

        parentViewHolder.mRangeSeekBar.setMaxValue(Float.parseFloat(navigationFilterWeight.getMaxWeight()));
        parentViewHolder.mRangeSeekBar.setMinValue(Float.parseFloat(navigationFilterWeight.getMinWeight()));

        parentViewHolder.mRangeSeekBar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                parentViewHolder.mMaxPrice.setText(String.valueOf(maxValue)+" lb");
                parentViewHolder.mMinPrice.setText(String.valueOf(minValue)+" lb");
            }
        });
        parentViewHolder.mRangeSeekBar.setOnRangeSeekbarFinalValueListener(new OnRangeSeekbarFinalValueListener() {
            @Override
            public void finalValue(Number minValue, Number maxValue) {
                parentViewHolder.mMaxPrice.setText(String.valueOf(maxValue)+" lb");
                parentViewHolder.mMinPrice.setText(String.valueOf(minValue)+" lb");
                try {
                    mWeightObject.put("max_weight", String.valueOf(maxValue));
                    mWeightObject.put("min_weight", String.valueOf(minValue));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}

