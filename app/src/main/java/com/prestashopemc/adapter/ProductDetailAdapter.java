package com.prestashopemc.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.prestashopemc.R;
import com.prestashopemc.customView.CustomTextView;

import java.util.List;

/**
 * <h1>Product Detail Adapter Contains Product Detail Holder</h1>
 * The Product Detail Adapter implements an adapter view for products contents.
 * <p>
 * <b>Note:</b> Adapter values are get from Product List model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class ProductDetailAdapter extends RecyclerView.Adapter<ProductDetailAdapter.ProductDetailHolder> {

    private List<String> mProductList;
    private Context mContext;

    /**
     * Instantiates a new Product detail adapter.
     *
     * @param context  the context
     * @param itemList the item list
     */
    public ProductDetailAdapter(Context context, List<String> itemList) {
        this.mProductList = itemList;
        this.mContext = context;
    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public ProductDetailHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_configurable_adapter, null);
        ProductDetailHolder viewHolder = new ProductDetailHolder(view);
        return viewHolder;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(ProductDetailHolder holder, int position) {

        for (int i = 0; i < 10; i++) {

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(10, 0, 10, 10);
            CustomTextView cust = new CustomTextView(mContext);
            cust.setPadding(10, 10, 10, 10);
            cust.setBackgroundResource(R.drawable.rectangle_shape_border);
            cust.setText("Orange");
            cust.setTextColor(mContext.getResources().getColor(R.color.black_color));
            cust.setLayoutParams(params);
            holder.mConfigLayout.addView(cust);
        }
    }

    /**
     * The type Product detail holder.
     */
    public class ProductDetailHolder extends RecyclerViewHolders {

        public CustomTextView configurable_title;
        public LinearLayout mConfigLayout;

        /**
         * Instantiates a new Product detail holder.
         *
         * @param v the v
         */
        public ProductDetailHolder(View v) {
            super(v);
            this.configurable_title = (CustomTextView) v.findViewById(R.id.configurable_title);
            mConfigLayout = (LinearLayout) v.findViewById(R.id.linearLayout1);
        }
    }

    /**
     * @return int
     */
    @Override
    public int getItemCount() {

        return this.mProductList.size();
    }
}
