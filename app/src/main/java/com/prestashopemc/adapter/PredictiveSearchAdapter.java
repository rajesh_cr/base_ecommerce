package com.prestashopemc.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableStringBuilder;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.database.RecentSearchService;
import com.prestashopemc.model.PredictiveSearchResult;
import com.prestashopemc.model.RecentSearchModel;
import com.prestashopemc.view.RecentSearchActivity;
import com.prestashopemc.view.SearchActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * <h1>Predictive Search Adapter Contains Predictive Search Holder</h1>
 * The Predictive Search Adapter implements an adapter view for predictive search result.
 * <p>
 * <b>Note:</b> Adapter values are get from Predictive Search Result model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class PredictiveSearchAdapter extends RecyclerView.Adapter<PredictiveSearchAdapter.PredictiveSearchHolder> {

    private Context context;
    private List<PredictiveSearchResult> mPredictiveSearchResult= new ArrayList<PredictiveSearchResult>();
    private PredictiveSearchResult mPredictiveSearchResultValue;
    private RecentSearchActivity recentSearchActivity;
    private RecentSearchService recentSearchService;
    private String stringEntered;
    private RecentSearchModel mRecentSearchModel;
    private RecentSearchService mRecentSearchService;

    /**
     * Instantiates a new Predictive search adapter.
     *
     * @param context                the context
     * @param predictiveSearchResult the predictive search result
     * @param stringEntered          the string entered
     */
    public PredictiveSearchAdapter(Context context, List<PredictiveSearchResult> predictiveSearchResult,String stringEntered){
        this.context = context;
        this.mPredictiveSearchResult = predictiveSearchResult;
        this.stringEntered=stringEntered;
    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public PredictiveSearchHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.recent_search_list_adapter, null);
        mRecentSearchService = new RecentSearchService(context);
        recentSearchActivity = (RecentSearchActivity) context;
        recentSearchService = new RecentSearchService(context);
        PredictiveSearchHolder viewHolder = new PredictiveSearchHolder(view);
        return viewHolder;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(final PredictiveSearchHolder holder, final int position) {
        mPredictiveSearchResultValue = mPredictiveSearchResult.get(position);
        SpannableStringBuilder builder=makeSectionOfTextBold(mPredictiveSearchResultValue.getName(),stringEntered);
        holder.mRecentText.setText(builder);
        holder.mRecentText.setTypeface(recentSearchActivity.robotoLight);

        holder.mRecentText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPredictiveSearchResultValue = mPredictiveSearchResult.get(position);
                mRecentSearchModel = new RecentSearchModel();
                mRecentSearchModel.setSearchKeyword(mPredictiveSearchResultValue.getName());
                List<RecentSearchModel> recentSearchModels = new ArrayList<RecentSearchModel>();
                recentSearchModels.add(mRecentSearchModel);
                try {
                    mRecentSearchService.insertRecentSearch(recentSearchModels);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                /*Intent filterIntent = new Intent(context, ProductDetailActivity.class);
                filterIntent.putExtra("product_id", "" + mPredictiveSearchResultValue.getIdProduct());
                filterIntent.putExtra("product_name", mPredictiveSearchResultValue.getName());
                context.startActivity(filterIntent);
                recentSearchActivity.finish();*/
                Intent searchIntent = new Intent(context, SearchActivity.class);
                searchIntent.putExtra("search_text",stringEntered);
                searchIntent.putExtra("id_category", mPredictiveSearchResultValue.getCategoryId()+"");
                context.startActivity(searchIntent);
                recentSearchActivity.finish();
            }
        });

        holder.mCloseIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPredictiveSearchResultValue = mPredictiveSearchResult.get(position);
                try {
                    mPredictiveSearchResult.remove(position);
                    recentSearchService.deleteItem(mPredictiveSearchResultValue.getIdProduct());
                    PredictiveSearchAdapter.this.notifyDataSetChanged();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Declared View Holder Variables
     */
    public class PredictiveSearchHolder extends RecyclerViewHolders {

        public TextView mRecentText;
        public ImageView mCloseIcon;

        /**
         * Instantiates a new Predictive search holder.
         *
         * @param v the v
         */
        public PredictiveSearchHolder(View v) {
            super(v);
            this.mRecentText = (TextView) v.findViewById(R.id.recent_search_text);
            this.mCloseIcon = (ImageView) v.findViewById(R.id.recent_close_icon);
        }

    }

    /**
     * @return int
     */
    @Override
    public int getItemCount() {
        return mPredictiveSearchResult.size();
    }

    /**
     * Make section of text bold spannable string builder.
     *
     * @param text       the text
     * @param textToBold the text to bold
     * @return the spannable string builder
     */
    public static SpannableStringBuilder makeSectionOfTextBold(String text, String textToBold){

        SpannableStringBuilder builder=new SpannableStringBuilder();

        if(textToBold != null && !textToBold.isEmpty()){

            //for counting start/end indexes
            String testText = text.toLowerCase(Locale.US);
            String testTextToBold = textToBold.toLowerCase(Locale.US);
            int startingIndex = testText.indexOf(testTextToBold);
            int endingIndex = startingIndex + testTextToBold.length();
            //for counting start/end indexes

            if(startingIndex < 0 || endingIndex <0){
                return builder.append(text);
            }
            else if(startingIndex >= 0 && endingIndex >=0){

                builder.append(text);
                builder.setSpan(new StyleSpan(Typeface.BOLD), startingIndex, endingIndex, 0);
            }
        }else{
            return builder.append(text);
        }

        return builder;
    }
}
