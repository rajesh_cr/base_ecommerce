package com.prestashopemc.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.model.NotificationResult;
import com.prestashopemc.view.NotificationActivity;

import java.util.List;

/**
 * <h1>Notification Adapter Contains Notification Holder</h1>
 * The Notification Adapter implements an adapter view for Notification.
 * <p>
 * <b>Note:</b> Adapter values are get from Notification Result model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.NotificationListHolder>
{
    private Context mContext;
    private NotificationResult mNotificationResult;
    private List<NotificationResult> mNotificationResultList;
    private NotificationActivity mNotificationActivity;

    /**
     * Instantiates a new Notification adapter.
     *
     * @param context             the context
     * @param notificationResults the notification results
     */
    public NotificationAdapter(Context context, List<NotificationResult> notificationResults){
        this.mContext = context;
        this.mNotificationResultList = notificationResults;
    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public NotificationListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        mNotificationActivity = (NotificationActivity) mContext;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_list_adapter, null);

        NotificationListHolder viewHolder = new NotificationListHolder(view);
        return viewHolder;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(final NotificationListHolder holder, final int position) {
        mNotificationResult = mNotificationResultList.get(position);
        holder.mNotificationDate.setText(mNotificationResult.getPushdate());
        if (mNotificationResult.getProductname() != null)
            holder.mNotificationTitle.setText(mNotificationResult.getProductname());
        else {
            holder.mNotificationTitle.setVisibility(View.GONE);
            holder.mNotificationSubTitle.setPadding(30,30,0,0);

        }
        holder.mNotificationSubTitle.setText(mNotificationResult.getRulemessage());

        setCustomFont(holder);
    }

    /*Set Custome Font*/
    private void setCustomFont(NotificationListHolder holder) {
        holder.mNotificationDate.setTypeface(mNotificationActivity.robotoRegular);
        holder.mNotificationTitle.setTypeface(mNotificationActivity.robotoRegular);
        holder.mNotificationSubTitle.setTypeface(mNotificationActivity.robotoLight);
    }

    /**
     * The type Notification list holder.
     */
/* Holder*/
    public class NotificationListHolder extends RecyclerViewHolders {

        public TextView mNotificationDate, mNotificationTitle, mNotificationSubTitle;
        public ImageView mNotificationIcon;

        /**
         * Instantiates a new Notification list holder.
         *
         * @param v the v
         */
        public NotificationListHolder(View v) {
            super(v);
            this.mNotificationDate = (TextView) v.findViewById(R.id.notification_date);
            this.mNotificationTitle = (TextView) v.findViewById(R.id.notification_title);
            this.mNotificationSubTitle = (TextView) v.findViewById(R.id.notification_sub_title);
            this.mNotificationIcon = (ImageView) v.findViewById(R.id.notification_icon);
        }
    }

    /**
     * @return
     */
    @Override
    public int getItemCount() {
        return this.mNotificationResultList.size();
    }
}
