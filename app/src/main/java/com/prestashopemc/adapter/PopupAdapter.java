package com.prestashopemc.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;
import com.prestashopemc.R;
import com.prestashopemc.model.LocatorResult;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.view.StoreLocatorActivity;

/**
 * <h1>Popup Adapter Contains Popup Holder</h1>
 * The Popup Adapter implements an adapter view for store locator Popup.
 * <p>
 * <b>Note:</b> Adapter values are get from Locator Result model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class PopupAdapter implements InfoWindowAdapter {

    /**
     * The M inflater.
     */
    LayoutInflater mInflater = null;
    private StoreLocatorActivity mStoreLocatorActivity;
    private LocatorResult mResults;
    private Context mContext;

    /**
     * Instantiates a new Popup adapter.
     *
     * @param inflater the inflater
     * @param mResult  the m result
     * @param mContext the m context
     */
    public PopupAdapter(LayoutInflater inflater, LocatorResult mResult, Context mContext) {
        this.mInflater = inflater;
        this.mResults = mResult;
        this.mContext = mContext;
    }

    /**
     * @param marker
     * @return
     */
    @Override
    public View getInfoWindow(Marker marker) {
        return (null);
    }

    /**
     * @param marker
     * @return
     */
    @Override
    public View getInfoContents(Marker marker) {
        mStoreLocatorActivity = new StoreLocatorActivity();
        View popup = mInflater.inflate(R.layout.location_address_adapter, null);

        TextView title = ((TextView) popup.findViewById(R.id.header_title));
        TextView address = ((TextView) popup.findViewById(R.id.locator_address));
        TextView phone = ((TextView) popup.findViewById(R.id.locator_phone));

        title.setText(AppConstants.getTextString(mContext, AppConstants.addressText));
        title.setTypeface(mStoreLocatorActivity.robotoMedium);
        address.setText(mResults.getAddress() + "\n" + mResults.getCity() + "," + mResults.getCountry() + "\n" +
                mResults.getZipcode());
        address.setTypeface(mStoreLocatorActivity.robotoRegular);
        if (!mResults.getPhone().isEmpty()) {
            phone.setText(mResults.getPhone());
        } else {
            phone.setVisibility(View.GONE);
        }
        phone.setTypeface(mStoreLocatorActivity.robotoRegular);
        return (popup);
    }
}
