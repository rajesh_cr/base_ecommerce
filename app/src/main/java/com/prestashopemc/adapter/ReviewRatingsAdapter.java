package com.prestashopemc.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.LayerDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;

import com.prestashopemc.R;
import com.prestashopemc.customView.CustomTextView;
import com.prestashopemc.model.RatingResult;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.view.MagentoProductDetailActivity;
import com.prestashopemc.view.ProductDetailActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

/**
 * <h1>Review Ratings Adapter Contains Review Ratings Holder</h1>
 * The Review Ratings Adapter implements an adapter view for Review Ratings.
 * <p>
 * <b>Note:</b> Adapter values are get from Review Ratings model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class ReviewRatingsAdapter extends RecyclerView.Adapter<ReviewRatingsAdapter.ReviewRatingsHolder> {

    private List<RatingResult> mRatingList;
    private Context mContext;
    private RatingResult mRating;
    /**
     * The M rating value.
     */
    public int mRatingValue = 0, /**
     * The K.
     */
    k = 0;
    /**
     * The M rating id.
     */
    public String mRatingId;
    /**
     * The M rating object.
     */
    public JSONObject mRatingObject = new JSONObject();
    /**
     * The M rating array.
     */
    public HashMap<String, String> mRatingArray = new HashMap<String, String>();
    private ProductDetailActivity mProductDetailActivity;
    private MagentoProductDetailActivity mMagentoProductDetailActivity;

    /**
     * Instantiates a new Review ratings adapter.
     *
     * @param context  the context
     * @param itemList the item list
     */
    public ReviewRatingsAdapter(Context context, List<RatingResult> itemList) {
        this.mRatingList = itemList;
        this.mContext = context;
    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public ReviewRatingsHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if(AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)){
            mMagentoProductDetailActivity = (MagentoProductDetailActivity) mContext;
        }else {
            mProductDetailActivity = (ProductDetailActivity) mContext;
        }


        View view = LayoutInflater.from(mContext).inflate(R.layout.review_ratings_adapter, null);
        ReviewRatingsHolder viewHolder = new ReviewRatingsHolder(view);
        return viewHolder;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(final ReviewRatingsHolder holder, final int position) {

        LayerDrawable stars = (LayerDrawable) holder.mRatingBar.getProgressDrawable();
        stars.getDrawable(2).setColorFilter(Color.YELLOW, PorterDuff.Mode.SRC_ATOP);
        stars.getDrawable(0).setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);

        mRating = mRatingList.get(position);

        holder.mValueText.setText(mRating.getLable());

        if(AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)){
            holder.mValueText.setTypeface(mMagentoProductDetailActivity.robotoRegular);
        }else {
            holder.mValueText.setTypeface(mProductDetailActivity.robotoRegular);
        }


        holder.mRatingBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                mRating = mRatingList.get(position);
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    float touchPositionX = event.getX();
                    float width = holder.mRatingBar.getWidth();
                    float starsf = (touchPositionX / width) * 5.0f;
                    int stars = (int) starsf + 1;
                    holder.mRatingBar.setRating(stars);

                    //Log.e("Rating Stars value ==== ", stars+"");
                    v.setPressed(false);
                    mRatingValue = stars;
                    mRatingId = mRating.getId();

                    int length = mRatingList.size();
                    String key[] = new String[length];
                    for (int i = 0; i < length; i++) {
                        key[i] = mRating.getId();
                        if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                            mRatingArray.put(mRating.getId(), mRating.getOptions().get(stars - 1) + "");
                        }else {
                            mRatingArray.put(mRating.getId(), stars+"");
                        }
                    }
                    Log.e("Rating Array ===== ", mRatingArray + "");

                    try {
                        for (int j = 0; j < length; j++) {
                            mRatingObject.put(key[j], Integer.parseInt(mRatingArray.get(key[j])));
                        }
                        //Log.e("Rating Object =====", mRatingObject.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    v.setPressed(true);
                }

                if (event.getAction() == MotionEvent.ACTION_CANCEL) {
                    v.setPressed(false);
                }
                return true;
            }
        });
    }

    /**
     * The type Review ratings holder.
     */
    public class ReviewRatingsHolder extends RecyclerViewHolders {



        public CustomTextView mValueText;
        public RatingBar mRatingBar;

        /**
         * Instantiates a new Review ratings holder.
         *
         * @param v the v
         */
        public ReviewRatingsHolder(View v) {
            super(v);
            this.mValueText = (CustomTextView) v.findViewById(R.id.value_text);
            this.mRatingBar = (RatingBar) v.findViewById(R.id.value_rating_bar);
        }
    }

    /**
     * @return int
     */
    @Override
    public int getItemCount() {
        return mRatingList.size();
    }
}
