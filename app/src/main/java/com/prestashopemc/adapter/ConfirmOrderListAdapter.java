package com.prestashopemc.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.prestashopemc.util.AppConstants;
import com.squareup.picasso.Picasso;
import com.prestashopemc.R;
import com.prestashopemc.model.ConfirmProduct;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.view.CheckoutActivity;

import java.util.List;

/**
 * <h1>Confirm order List Adapter Contains Confirm order List Holder</h1>
 * The Confirm order List Adapter implements an adapter view for Confirm order list.
 * <p>
 * <b>Note:</b> Adapter values are get from Confirm order model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class ConfirmOrderListAdapter extends RecyclerView.Adapter<ConfirmOrderListAdapter.ConfirmOrderHolder> {

    private List<ConfirmProduct> mConfirmProducts;
    private Context mContext;
    private ConfirmProduct mConfirmProduct;
    private CheckoutActivity mCheckoutActivity;

    /**
     * Instantiates a new Confirm order list adapter.
     *
     * @param context         the context
     * @param confirmProducts the confirm products
     */
    public ConfirmOrderListAdapter(Context context, List<ConfirmProduct> confirmProducts) {
        this.mConfirmProducts = confirmProducts;
        this.mContext = context;
    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public ConfirmOrderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        mCheckoutActivity = (CheckoutActivity) mContext;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.confirm_order_list, null);
        ConfirmOrderHolder viewHolder = new ConfirmOrderHolder(view);
        return viewHolder;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(final ConfirmOrderHolder holder, final int position) {
        mConfirmProduct = mConfirmProducts.get(position);

        String currenySymbol = SharedPreference.getInstance().getValue("currency_symbol");
        Picasso.with(mContext).load(mConfirmProduct.getImageUrl())
                .placeholder(R.drawable.place_holder).resize(200, 200).centerCrop()
                .into(holder.mProductImage);

        setCustomFont(holder);
        if (mConfirmProduct.getName() != null) {
            holder.mProductName.setText(mConfirmProduct.getName());
        }else {
            holder.mProductName.setText(mConfirmProduct.getProductName());
        }
        //holder.mProductSplPrice.setText(currenySymbol+" "+mConfirmProduct.getPrice());
        holder.mProductPrice.setText(currenySymbol + " " + mConfirmProduct.getPrice());
        holder.mProductSplPrice.setPaintFlags(holder.mProductPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        holder.mQuantity.setText(mConfirmProduct.getQty() + " " + AppConstants.getTextString(mContext, AppConstants.itemsText));

        /*String supplierMsg = mConfirmProduct.getSupplierMessage();
        if (!supplierMsg.isEmpty()) {
            holder.mProductSupplierMsg.setVisibility(View.VISIBLE);
            String[] supplier1 = supplierMsg.split(":");
            String msg1 = supplier1[0];
            String msg2 = supplier1[1];

            String[] supplier2 = msg2.split(";");
            String msg3 = supplier2[0];
            String msg4 = supplier2[1];

            Spannable supplierMSG = new SpannableString(msg3);
            supplierMSG.setSpan(new ForegroundColorSpan(Color.BLACK), 0, supplierMSG.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            holder.mProductSupplierMsg.setText(msg1 + ":");
            holder.mProductSupplierMsg.append(supplierMSG);
            holder.mProductSupplierMsg.append(" " + msg4);
        }*/
    }

    /**
     * The type Confirm order holder.
     */
    public class ConfirmOrderHolder extends RecyclerViewHolders {


        public ImageView mProductImage;
        public TextView mProductName, mProductPrice, mProductSplPrice, mProductSupplierMsg,mQuantityLabel, mQuantity;

        /**
         * Instantiates a new Confirm order holder.
         *
         * @param v the view
         */
        public ConfirmOrderHolder(View v) {
            super(v);
            this.mProductImage = (ImageView) v.findViewById(R.id.product_image);
            this.mProductName = (TextView) v.findViewById(R.id.product_name);
            this.mProductPrice = (TextView) v.findViewById(R.id.price);
            this.mProductSplPrice = (TextView) v.findViewById(R.id.spl_price);
            this.mProductSupplierMsg = (TextView) v.findViewById(R.id.item_supplier);
            this.mQuantityLabel = (TextView) v.findViewById(R.id.quantity_label);
            this.mQuantity = (TextView) v.findViewById(R.id.confirm_quantity_text);
        }
    }

    /**
     * @return nothing
     */
    @Override
    public int getItemCount() {
        return this.mConfirmProducts.size();
    }

    /**
     * Set Custom Font
     * @param holder the holder
     */
    private void setCustomFont(ConfirmOrderHolder holder) {
        holder.mQuantityLabel.setText(AppConstants.getTextString(mContext, AppConstants.confirmQuantityText));
        holder.mProductName.setTypeface(mCheckoutActivity.robotoRegular);
        holder.mProductSplPrice.setTypeface(mCheckoutActivity.robotoLight);
        holder.mProductPrice.setTypeface(mCheckoutActivity.robotoMedium);
        holder.mProductSupplierMsg.setTypeface(mCheckoutActivity.robotoLight);
        holder.mQuantityLabel.setTypeface(mCheckoutActivity.robotoLight);
        holder.mQuantity.setTypeface(mCheckoutActivity.robotoRegular);
    }


}
