package com.prestashopemc.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.OvershootInterpolator;
import android.view.animation.ScaleAnimation;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.controller.ProductListController;
import com.prestashopemc.customView.CustomTextView;
import com.prestashopemc.model.CategoryProduct;
import com.prestashopemc.model.CustomAttribute;
import com.prestashopemc.model.HomePage;
import com.prestashopemc.model.MagentoCategoryProduct;
import com.prestashopemc.model.MagentoProductLabel;
import com.prestashopemc.model.Option;
import com.prestashopemc.model.Ribbon;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.util.MyApplication;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.view.MagentoProductDetailActivity;
import com.prestashopemc.view.ProductListActivity;
import com.prestashopemc.view.SearchActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Product List Adapter Contains Product List Holder</h1>
 * The Product List Adapter implements an adapter view for Product List.
 * <p>
 * <b>Note:</b> Adapter values are get from Product List model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class ProductListMagentoAdapter extends RecyclerView.Adapter<ProductListMagentoAdapter.ProductListViewHolder> {
    private List<MagentoCategoryProduct> mProductList;
    private Context mContext;
    private MagentoCategoryProduct mProduct;
    private List<Ribbon> mRibbonProductList;
    private boolean mIsGrid;
    private int mLayoutType, mQuantityCount, mOutOfStock;
    private ProductListController mProductListController;
    private String mWishListStatus, mSpinnerPrice, mSpinnerSplPrice, mRibbionVisibilty;
    private ProductListActivity mProductListActivity;
    private SearchActivity mSearchProductListActivity;
    private Boolean mRibbonEnabled = false;
    private String mLeftTop, mLeftBottom, mRightTop, mRightBottom;
    private ArrayList<String> optionValueList = new ArrayList<>();
    private String mProductAttribute = "";
    private ArrayList<MagentoProductLabel> magentoProductLabel = new ArrayList<>();

    /**
     * Instantiates a new Product list adapter.
     *
     * @param context    the context
     * @param itemList   the item list
     * @param isGrid     the is grid
     * @param layoutType the layout type
     */
    public ProductListMagentoAdapter(Context context, List<MagentoCategoryProduct> itemList, boolean isGrid, int layoutType) {
        this.mProductList = itemList;
        this.mContext = context;
        this.mIsGrid = isGrid;
        this.mLayoutType = layoutType;
    }

    @Override
    public ProductListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        mProductListController = new ProductListController(mContext);

        if (mLayoutType == 0){
            mLayoutType = 2;
        }
        if (mContext instanceof ProductListActivity) {
            mProductListActivity = (ProductListActivity) mContext;
        }else if (mContext instanceof SearchActivity) {
            mSearchProductListActivity = (SearchActivity) mContext;
        }
        if (mLayoutType == 1) {
            if (mIsGrid) {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_grid_first_adapter, null);
            } else {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_list_first_adapter, null);
            }
        } else if (mLayoutType == 2) {
            if (mIsGrid) {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_grid_second_adapter, null);
            } else {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_list_second_adapter, null);
            }
        } else if (mLayoutType == 3) {
            if (mIsGrid) {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_grid_third_adapter, null);
            } else {
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_list_third_adapter, null);
            }
        }

        ProductListViewHolder viewHolder = new ProductListViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ProductListViewHolder holder, final int position) {
        mProduct = mProductList.get(position);
        final String currenySymbol = SharedPreference.getInstance().getValue("currency_symbol");
        mWishListStatus = SharedPreference.getInstance().getValue("wishlist_enable_status");

        setCustomFont(holder);
        mOutOfStock = mProduct.getStock();
        holder.mOutOfStock.setTag(position);
        if (mOutOfStock == 0) {
            holder.mProductImage.setAlpha((float) 0.2);
            holder.mOutOfStock.setVisibility(View.VISIBLE);
        } else {
            holder.mProductImage.setAlpha((float) 1);
            holder.mOutOfStock.setVisibility(View.GONE);
        }
        if (mWishListStatus.equals("false")) {
            holder.mWishlistImage.setVisibility(View.GONE);
        }

        if (!mIsGrid) {
            if (!mProduct.getImageUrl().isEmpty())
                Picasso.with(mContext).load(mProduct.getImageUrl())
                        .placeholder(R.drawable.place_holder).resize(200, 200).centerCrop()
                        .into(holder.mProductImage);
        } else {
            if (!mProduct.getImageUrl().isEmpty())
                Picasso.with(mContext).load(mProduct.getImageUrl())
                        .placeholder(R.drawable.place_holder).resize(265, 265).centerCrop()
                        .into(holder.mProductImage);
        }
        holder.mProductName.setText(mProduct.getProductName());

        /**
         * Set Price and Spl price */
        if (!mProduct.getSpecialprice().equalsIgnoreCase(mContext.getString(R.string.not_set)) &&
                mProduct.getSpecialprice() != null) {
            holder.mPrice.setText(currenySymbol + " " + mProduct.getSpecialprice());
        } else {
            holder.mSpecialPrice.setVisibility(View.GONE);
            holder.mPrice.setText(currenySymbol + " " + mProduct.getPrice());
        }
        if (mProduct.getPrice() != null) {
            if (!mProduct.getPrice().equalsIgnoreCase(mContext.getString(R.string.not_set))) {
                holder.mSpecialPrice.setText(currenySymbol + " " + mProduct.getPrice());
            }
        }

        if (mLayoutType == 2) {

            if (mWishListStatus.equals("false")) {
                if (mIsGrid) {
                    holder.mWishListView.setVisibility(View.GONE);
                    LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) holder.mAddToCart.getLayoutParams();
                    params.gravity = Gravity.CENTER;
                    params.setMargins(0,0,0,0);
                    holder.mAddToCart.setLayoutParams(params);
                }
            }
            String homePageList = SharedPreference.getInstance().getValue("homePageList");
            HomePage homePage = MyApplication.getGsonInstance().fromJson(homePageList, HomePage.class);
            magentoProductLabel.clear();
            magentoProductLabel.addAll(homePage.getResult().getMagentoProductLabel());

            if (mProduct.getProductLabel().size() > 0) {
                /**ribbion is enabled Add ribbion Arraylist*/
                mRibbonProductList = new ArrayList<>();

                if (mProduct.getProductLabel().size() != 0) {
                    for (int i = 0; i < magentoProductLabel.size(); i++) {
                        if (!mProduct.getProductLabel().contains(magentoProductLabel.get(i).getId()))
                            magentoProductLabel.remove(i);
                    }
                    for (int i = 0; i < magentoProductLabel.size(); i++) {
                        /**Check Ribbion Image not Null*/
                        if (magentoProductLabel.get(i).getImage() != null) {
                            mRibbionVisibilty = magentoProductLabel.get(i).getPosition();
                            if (mRibbionVisibilty.contains("custom_right_bottom")) {
                                holder.mProductLabel4.setVisibility(View.VISIBLE);
                                Picasso.with(mContext).load(magentoProductLabel.get(i).getImage())
                                        .placeholder(R.drawable.place_holder).resize(500, 500)
                                        .into(holder.mProductLabel4);
                            }
                            if (mRibbionVisibilty.contains("custom_left_bottom")) {
                                holder.mProductLabel3.setVisibility(View.VISIBLE);
                                Picasso.with(mContext).load(magentoProductLabel.get(i).getImage())
                                        .placeholder(R.drawable.place_holder).resize(500, 500)
                                        .into(holder.mProductLabel3);
                            }
                            if (mRibbionVisibilty.contains("custom_left_top")) {
                                holder.mProductLabel1.setVisibility(View.VISIBLE);
                                Picasso.with(mContext).load(magentoProductLabel.get(i).getImage())
                                        .placeholder(R.drawable.place_holder).resize(500, 500)
                                        .into(holder.mProductLabel1);
                            }
                            if (mRibbionVisibilty.contains("custom_right_top")) {
                                holder.mProductLabel2.setVisibility(View.VISIBLE);
                                Picasso.with(mContext).load(magentoProductLabel.get(i).getImage())
                                        .placeholder(R.drawable.place_holder).resize(500, 500)
                                        .into(holder.mProductLabel2);
                            }
                        }
                    }
                }
            } else {
                holder.mProductLabel4.setVisibility(View.INVISIBLE);
                holder.mProductLabel3.setVisibility(View.INVISIBLE);
                holder.mProductLabel1.setVisibility(View.INVISIBLE);
                holder.mProductLabel2.setVisibility(View.INVISIBLE);
            }

            holder.mShortDescription.setText(mProduct.getDescriptionShort());
            if (mContext instanceof ProductListActivity) {
                holder.mShortDescription.setTypeface(mProductListActivity.robotoRegular);
            }else {
                holder.mShortDescription.setTypeface(mSearchProductListActivity.robotoRegular);
            }

            holder.mAddToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mProduct = mProductList.get(position);
                    if (mContext instanceof ProductListActivity) {
                        mProductListActivity.showProgDialiog();
                    }else {
                        mSearchProductListActivity.showProgDialiog();
                    }

                    if (!mProduct.getIsConfig() && !mProduct.getIsCombination()) {
                        mProductListController.addToCart(mProduct.getIdProduct(), "1", mProductAttribute, mProduct.getProductName());
                    }else {
                        if (mContext instanceof ProductListActivity) {
                            mProductListActivity.hideProgDialog();
                            mProductListActivity.snackBar(AppConstants.getTextString(mContext, AppConstants.cartAlertText));
                        }else {
                            mSearchProductListActivity.hideProgDialog();
                            mSearchProductListActivity.snackBar(AppConstants.getTextString(mContext, AppConstants.cartAlertText));
                        }
                    }
                }
            });

            holder.mShortDescription.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mProductList.size() != 0) {
                        mProduct = mProductList.get(position);
                        Intent filterIntent = new Intent(mContext, MagentoProductDetailActivity.class);
                        filterIntent.putExtra("product_id", "" + mProduct.getIdProduct());
                        filterIntent.putExtra("product_name", mProduct.getProductName());
                        filterIntent.putExtra("model", mProduct);
                        mContext.startActivity(filterIntent);
                    }
                }
            });

        } else if (mLayoutType == 3) {
            holder.mShortDescription.setText(mProduct.getDescriptionShort());

            if(mContext instanceof ProductListActivity){
                holder.mQuantityNumber.setTypeface(mProductListActivity.robotoLight);
                holder.mShortDescription.setTypeface(mProductListActivity.robotoRegular);
                holder.mAddToCartText.setTypeface(mProductListActivity.robotoLight);
            }else{
                holder.mQuantityNumber.setTypeface(mSearchProductListActivity.robotoLight);
                holder.mShortDescription.setTypeface(mSearchProductListActivity.robotoRegular);
                holder.mAddToCartText.setTypeface(mSearchProductListActivity.robotoLight);
            }

            holder.mAddToCartText.setTextColor(Color.parseColor(CustomBackground.mThemeColor));
            if (mProduct.getIsConfig()) {
                if (mProduct.getIsCombination()) {
                    holder.mConfigSpinnerLayout.setVisibility(View.VISIBLE);
                    //holder.mAddToCart.setVisibility(View.VISIBLE);
                    holder.mAddToCartLayout.setVisibility(View.VISIBLE);
                    holder.mQuantityMinus.setVisibility(View.VISIBLE);
                    holder.mQuantityNumber.setVisibility(View.VISIBLE);
                    holder.mQuantityPlus.setVisibility(View.VISIBLE);
                    for (Option option : mProduct.getAttributesList().get(0).getmOptions()) {
                        optionValueList.add(option.getLabel());
                    }
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(mContext,
                            R.layout.spinner_text, optionValueList);
                    //dataAdapter.setDropDownViewResource(R.layout.spinner_text);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    holder.mConfigurableSpinner.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
                    holder.mConfigurableSpinner.setAdapter(dataAdapter);
                } else {
                    holder.mConfigSpinnerLayout.setVisibility(View.GONE);
                    //holder.mAddToCart.setVisibility(View.INVISIBLE);
                    holder.mAddToCartLayout.setVisibility(View.INVISIBLE);
                    holder.mQuantityMinus.setVisibility(View.INVISIBLE);
                    holder.mQuantityNumber.setVisibility(View.INVISIBLE);
                    holder.mQuantityPlus.setVisibility(View.INVISIBLE);
                }
//
            } else {
                holder.mConfigSpinnerLayout.setVisibility(View.GONE);
                //holder.mAddToCart.setVisibility(View.VISIBLE);
                holder.mAddToCartLayout.setVisibility(View.VISIBLE);
                holder.mQuantityMinus.setVisibility(View.VISIBLE);
                holder.mQuantityNumber.setVisibility(View.VISIBLE);
                holder.mQuantityPlus.setVisibility(View.VISIBLE);
            }


            final int[] mCount = {1};
            holder.mQuantityPlus.setTag(position);
            holder.mQuantityPlus.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    mProduct = mProductList.get(position);
                    mCount[0]++;
                    holder.mQuantityNumber.setText(String.valueOf(mCount[0]));
                    mQuantityCount = mCount[0];

                    if(mContext instanceof ProductListActivity){
                        mProductListActivity.showProgDialiog();
                    }else {
                        mSearchProductListActivity.showProgDialiog();
                    }

//                    mProductListController.addToCart(mProduct.getIdProduct(), mQuantityCount + "", productAttribute, mProduct.getProductName());
                    mProductListController.addToCart(mProduct.getIdProduct(), 1 + "", mProductAttribute, mProduct.getProductName());
                    mProductAttribute = "";
                }
            });

            holder.mQuantityMinus.setTag(position);
            CustomBackground.setBackgroundRedWhiteCombination(holder.mCartAddLayout);
            holder.mQuantityMinus.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {
                    mProduct = mProductList.get(position);
                    if (mCount[0] != 1) {
                        mCount[0]--;
                        holder.mQuantityNumber.setText(String.valueOf(mCount[0]));
                        mQuantityCount = mCount[0];
                        if(mContext instanceof ProductListActivity){
                            mProductListActivity.showProgDialiog();
                        }else {
                            mSearchProductListActivity.showProgDialiog();
                        }
                        mProductListController.cartQuantityUpdate(mQuantityCount + "", mProduct.getIdProduct(), mProductAttribute,position + "");

                        // mProductListController.addToCart(mProduct.getIdProduct(), mQuantityCount + "", productAttribute, mProduct.getProductName());
                    } else {
                        if(mContext instanceof ProductListActivity){
                            mProductListActivity.showProgDialiog();
                        }else {
                            mSearchProductListActivity.showProgDialiog();
                        }
                        mProductListController.deleteItem(mProduct.getIdProduct(), mProductAttribute);
                        holder.mCartAddLayout.setVisibility(View.VISIBLE);
                        holder.mQuantityLayout.setVisibility(View.GONE);
                    }
                }
            });

            //holder.mAddToCartLayout.setTag(position);
            //holder.mCartAddLayout.setTag(position);
            holder.mCartAddLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mProduct = mProductList.get(position);
                    //holder.mQuantityLayout.setTag(position);
                    if (mProduct.getStock() != 0) {
                        holder.mCartAddLayout.setVisibility(View.GONE);
                        holder.mQuantityLayout.setVisibility(View.VISIBLE);
                        holder.mQuantityNumber.setText(String.valueOf(mCount[0]));

                        if(mContext instanceof ProductListActivity){
                            mProductListActivity.showProgDialiog();
                        }else {
                            mSearchProductListActivity.showProgDialiog();
                        }
                        mProductListController.addToCart(mProduct.getIdProduct(), "1", mProductAttribute, mProduct.getProductName());
                        mProductAttribute = "";
                    } else {

                        if(mContext instanceof ProductListActivity){
                            mProductListActivity.snackBar(AppConstants.getTextString(mContext, AppConstants.outOfStockText));
                        }else {
                            mSearchProductListActivity.snackBar(AppConstants.getTextString(mContext, AppConstants.outOfStockText));
                        }

                    }
                }
            });
           /* holder.mAddToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    mProduct = productList.get(position);
                    mProductListController.addToCart(mProduct.getIdProduct(), mQuantityCount + "", productAttribute, mProduct.getProductName());
                    mCount[0] = 0;
                    holder.mQuantityNumber.setText("" + mCount[0]);

                    cartAnimation(holder.mAddToCart);

                }
            });

            /*holder.mSpinnerImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    holder.mConfigurableSpinner.performClick();
                }
            });*/
            holder.mConfigurableSpinner.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
            holder.mConfigurableSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long id) {
                    mProduct = mProductList.get(position);
                    String keyValue = arg0.getItemAtPosition(pos)
                            .toString();

                    if(mContext instanceof ProductListActivity){
                        ((TextView) arg0.getChildAt(0)).setTypeface(mProductListActivity.robotoRegular);
                    } else {
                        ((TextView) arg0.getChildAt(0)).setTypeface(mSearchProductListActivity.robotoRegular);
                    }


                    for (Option optionAttribute : mProduct.getAttributesList().get(0).getmOptions()) {
                        if (optionAttribute.getLabel().contains(keyValue)) {

                            String attributeId = optionAttribute.getProducts().get(0);
                            if (attributeId != null) {
                                mProductAttribute = attributeId;
                            } else {
                                mProductAttribute = "";
                            }

                            if (!optionAttribute.getPrice().equalsIgnoreCase(mContext.getString(R.string.not_set))) {
                                mSpinnerPrice = Double.parseDouble(mProduct.getPrice()) + Double.parseDouble(optionAttribute.getPrice()) + "";
                            }

                            if (!mProduct.getSpecialprice().equalsIgnoreCase(mContext.getString(R.string.not_set))) {
                                mSpinnerSplPrice = Double.parseDouble(mProduct.getSpecialprice()) + Double.parseDouble(optionAttribute.getPrice()) + "";
                            }


                            if (mSpinnerPrice != null) {
                                holder.mSpecialPrice.setText(currenySymbol + " " + mSpinnerPrice);
                            }
                            if (mSpinnerSplPrice != null) {
                                if (!mSpinnerSplPrice.equalsIgnoreCase(mContext.getString(R.string.not_set))) {
                                    holder.mPrice.setText(currenySymbol + " " + mSpinnerSplPrice);
                                }
                            } else {
                                holder.mSpecialPrice.setText("");
                                holder.mPrice.setText(currenySymbol + " " + mSpinnerPrice);
                            }
                        }

                    }


                }

                @Override
                public void onNothingSelected(AdapterView<?> arg0) {
                    // TODO Auto-generated method stub

                }
            });

            holder.mShortDescription.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mProductList.size() != 0) {
                        mProduct = mProductList.get(position);
                        Intent filterIntent = new Intent(mContext, MagentoProductDetailActivity.class);
                        filterIntent.putExtra("product_id", "" + mProduct.getIdProduct());
                        filterIntent.putExtra("product_name", mProduct.getProductName());
                        filterIntent.putExtra("model", mProduct);
                        mContext.startActivity(filterIntent);
                    }
                }
            });

        }

        holder.mSpecialPrice.setPaintFlags(holder.mSpecialPrice
                .getPaintFlags()
                | Paint.STRIKE_THRU_TEXT_FLAG);

        holder.mPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mProductList.size() != 0) {
                    mProduct = mProductList.get(position);
                    Intent filterIntent = new Intent(mContext, MagentoProductDetailActivity.class);
                    filterIntent.putExtra("product_id", "" + mProduct.getIdProduct());
                    filterIntent.putExtra("product_name", mProduct.getProductName());
                    filterIntent.putExtra("model", mProduct);
                    mContext.startActivity(filterIntent);
                }
            }
        });

        holder.mSpecialPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mProductList.size() != 0) {
                    mProduct = mProductList.get(position);
                    Intent filterIntent = new Intent(mContext, MagentoProductDetailActivity.class);
                    filterIntent.putExtra("product_id", "" + mProduct.getIdProduct());
                    filterIntent.putExtra("product_name", mProduct.getProductName());
                    filterIntent.putExtra("model", mProduct);
                    mContext.startActivity(filterIntent);
                }
            }
        });

        holder.mProductName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mProductList.size() != 0) {
                    mProduct = mProductList.get(position);
                    Intent filterIntent = new Intent(mContext, MagentoProductDetailActivity.class);
                    filterIntent.putExtra("product_id", "" + mProduct.getIdProduct());
                    filterIntent.putExtra("product_name", mProduct.getProductName());
                    filterIntent.putExtra("model", mProduct);
                    mContext.startActivity(filterIntent);
                }
            }
        });
        holder.mProductImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mProductList.size() != 0) {
                    mProduct = mProductList.get(position);
                    Intent filterIntent = new Intent(mContext, MagentoProductDetailActivity.class);
                    filterIntent.putExtra("product_id", "" + mProduct.getIdProduct());
                    filterIntent.putExtra("product_name", mProduct.getProductName());
                    filterIntent.putExtra("model", mProduct);
                    mContext.startActivity(filterIntent);
                }
            }
        });

        if (!SharedPreference.getInstance().getValue("customerid").equals("0")) {
            if (mProduct.getWishlist()) {
                holder.mWishlistImage.setImageResource(R.drawable.ic_wishlist_enable);
            } else {
                holder.mWishlistImage.setImageResource(R.drawable.ic_wishlist_disable);
            }
        }
        holder.mWishlistImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mProduct = mProductList.get(position);

                if (!SharedPreference.getInstance().getValue("customerid").equals("0")) {
                    if (!mProduct.getWishlist()) {
                        if(mContext instanceof ProductListActivity){
                            mProductListActivity.showProgDialiog();
                        }else {
                            mSearchProductListActivity.showProgDialiog();
                        }
                        MyApplication.eventTracking("addto wishlist", "product added to wishlist", "addto wishlist");
                        mProductListController.addWishlist(mProduct.getIdProduct(), AppConstants.sAddWishlist, null,mProduct);
                    } else {
                        if(mContext instanceof ProductListActivity){
                            mProductListActivity.showProgDialiog();
                        }else {
                            mSearchProductListActivity.showProgDialiog();
                        }
                        MyApplication.eventTracking("product deleted from wishlist", "product deleted from wishlist", "product deleted from wishlist");
                        mProductListController.addWishlist(mProduct.getIdProduct(), AppConstants.sDeleteWishlist, null,mProduct);
                    }
                } else {

                    if(mContext instanceof ProductListActivity){
                        mProductListActivity.snackBar(AppConstants.getTextString(mContext, AppConstants.needLoginText));
                    }else {
                        mSearchProductListActivity.snackBar(AppConstants.getTextString(mContext, AppConstants.needLoginText));
                    }
                }
            }
        });

    }


    /**
     * Product Listview Holder
     */
    public class ProductListViewHolder extends RecyclerViewHolders {

        public ImageView mProductImage, mAddToCart, mQuantityMinus, mQuantityPlus, mWishlistImage, mProductLabel1, mProductLabel2, mProductLabel3, mProductLabel4, mSpinnerImage;
        public CustomTextView mProductName, mPrice, mSpecialPrice, mShortDescription, mQuantityNumber;
        public RelativeLayout mTopLayout, mListMainlayout, mAddToCartLayout;
        public Spinner mConfigurableSpinner;
        private LinearLayout mConfigSpinnerLayout, mQuantityLayout, mCartAddLayout;
        public TextView mAddToCartText, mOutOfStock;
        public View mWishListView;


        /**
         * Instantiates a new Product list view holder.
         *
         * @param v the v
         */
        public ProductListViewHolder(View v) {
            super(v);
            this.mProductImage = (ImageView) v.findViewById(R.id.product_image);
            this.mProductName = (CustomTextView) v.findViewById(R.id.product_name);
            this.mPrice = (CustomTextView) v.findViewById(R.id.price);
            this.mSpecialPrice = (CustomTextView) v.findViewById(R.id.spl_price);
            this.mWishlistImage = (ImageView) v.findViewById(R.id.wishlist_image);
            this.mOutOfStock = (TextView) v.findViewById(R.id.out_of_stock);
            mTopLayout = (RelativeLayout) v.findViewById(R.id.top_layout);
            this.mListMainlayout = (RelativeLayout) v.findViewById(R.id.main_layout);

            if (mLayoutType == 2) {

                this.mShortDescription = (CustomTextView) v.findViewById(R.id.product_short_description);
                this.mAddToCart = (ImageView) v.findViewById(R.id.add_to_cart);
                this.mProductLabel1 = (ImageView) v.findViewById(R.id.product_label_first);
                this.mProductLabel2 = (ImageView) v.findViewById(R.id.product_label_second);
                this.mProductLabel3 = (ImageView) v.findViewById(R.id.product_label_third);
                this.mProductLabel4 = (ImageView) v.findViewById(R.id.product_label_four);
                this.mWishListView = (View) v.findViewById(R.id.wishlist_line);

            } else if (mLayoutType == 3) {
                this.mAddToCartText = (TextView) v.findViewById(R.id.add_to_cart_text);
                this.mShortDescription = (CustomTextView) v.findViewById(R.id.product_short_description);
                this.mSpinnerImage = (ImageView) v.findViewById(R.id.spinner_image);
                this.mQuantityMinus = (ImageView) v.findViewById(R.id.quantity_minus);
                this.mQuantityPlus = (ImageView) v.findViewById(R.id.quantity_plus);
                this.mQuantityNumber = (CustomTextView) v.findViewById(R.id.quantity_text);
                this.mConfigurableSpinner = (Spinner) v.findViewById(R.id.configurable_spinner);
                this.mAddToCart = (ImageView) v.findViewById(R.id.add_to_cart);
                this.mConfigSpinnerLayout = (LinearLayout) v.findViewById(R.id.config_spinner_layout);
                this.mQuantityLayout = (LinearLayout) v.findViewById(R.id.quantity_layout);
                this.mAddToCartLayout = (RelativeLayout) v.findViewById(R.id.addToCart_layout);
                this.mCartAddLayout = (LinearLayout) v.findViewById(R.id.cart_layout);
            }
        }
    }


    @Override
    public int getItemCount() {
        return this.mProductList.size();
    }


    /**
     * Add to Cart Animation
     */
    private void cartAnimation(ImageView cartImage) {
        ScaleAnimation scale = new ScaleAnimation(0, 1, 0, 1, ScaleAnimation.RELATIVE_TO_SELF, .5f, ScaleAnimation.RELATIVE_TO_SELF, .5f);
        scale.setDuration(500);
        scale.setInterpolator(new OvershootInterpolator());
        cartImage.startAnimation(scale);
    }


    /**
     * Set Custom Font
     */
    private void setCustomFont(ProductListViewHolder holder) {

        if(mContext instanceof ProductListActivity){
            holder.mProductName.setTypeface(mProductListActivity.robotoRegular);
            holder.mPrice.setTypeface(mProductListActivity.robotoMedium);
            holder.mSpecialPrice.setTypeface(mProductListActivity.robotoLight);
        }else {
            holder.mProductName.setTypeface(mSearchProductListActivity.robotoRegular);
            holder.mPrice.setTypeface(mSearchProductListActivity.robotoMedium);
            holder.mSpecialPrice.setTypeface(mSearchProductListActivity.robotoLight);
        }

    }


    /**
     * Check value string.
     *
     * @param left_top     the left top
     * @param left_bottom  the left bottom
     * @param right_top    the right top
     * @param right_bottom the right bottom
     * @return the string
     */
    public String checkValue(String left_top, String left_bottom, String right_top, String right_bottom) {
        String value = "";
        if (left_top.equals("1")) {
            value = "left_top";
        }
        if (left_bottom.equals("1")) {
            value = value + "left_bottom";
        }
        if (right_top.equals("1")) {
            value = value + "right_top";
        }
        if (right_bottom.equals("1")) {
            value = value + "right_bottom";
        }
        if (right_bottom.equals("1")) {
            value = value + "right_bottom";
        }
        return value;
    }

}