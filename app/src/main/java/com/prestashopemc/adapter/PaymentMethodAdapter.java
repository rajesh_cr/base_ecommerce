package com.prestashopemc.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.model.PaymentMethod;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.view.CheckoutActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Payment Method Adapter Contains Payment Method Holder</h1>
 * The Payment Method Adapter implements an adapter view for payment.
 * <p>
 * <b>Note:</b> Adapter values are get from Payment Method List model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class PaymentMethodAdapter extends RecyclerView.Adapter<PaymentMethodAdapter.PaymentMethodHolder> {

    private Context mContext;
    private CheckoutActivity mCheckoutActivity;
    private int mSelected = 0;
    private List<PaymentMethod> mPaymentMethodList = new ArrayList<PaymentMethod>();
    private PaymentMethod mPaymentMethod;

    /**
     * Instantiates a new Payment method adapter.
     *
     * @param context        the context
     * @param paymentMethods the payment methods
     */
    public PaymentMethodAdapter(Context context, List<PaymentMethod> paymentMethods){
        this.mContext = context;
        this.mPaymentMethodList = paymentMethods;
    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public PaymentMethodHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mCheckoutActivity = (CheckoutActivity)mContext;
        View view = LayoutInflater.from(mContext).inflate(R.layout.payment_method_list, null);
        PaymentMethodHolder viewHolder = new PaymentMethodHolder(view);
        return viewHolder;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(final PaymentMethodHolder holder, final int position) {
        mPaymentMethod = mPaymentMethodList.get(position);

        holder.mPaymentMethodName.setTypeface(mCheckoutActivity.robotoLight);
        holder.mPaymentMethodName.setText(mPaymentMethod.getName());
        holder.mPaymentRadioButton.setTag(position);
        holder.mPaymentRadioButton.setChecked(position == mSelected);
        holder.mPaymentRadioButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPaymentMethod = mPaymentMethodList.get(position);
                mSelected = (Integer) v.getTag();
                notifyDataSetChanged();
                mCheckoutActivity.paymentMethodName = mPaymentMethod.getName();
                mCheckoutActivity.paymentMethodCode = mPaymentMethod.getCode();
                SharedPreference.getInstance().save("paymentMethod", mPaymentMethod.getCode());
            }
        });
        if(position == mSelected){
            mCheckoutActivity.paymentMethodName = mPaymentMethodList.get(position).getName();
            mCheckoutActivity.paymentMethodCode = mPaymentMethodList.get(position).getCode();
            mCheckoutActivity.paymentIdentifier = mPaymentMethodList.get(position).getIdentifier();
            SharedPreference.getInstance().save("paymentMethod", mPaymentMethod.getCode());
        }
        holder.mPaymentMethodLayout.setTag(position);
        holder.mPaymentMethodLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelected = -1;
                mPaymentMethod = mPaymentMethodList.get(position);
                mSelected = (Integer) v.getTag();
                notifyDataSetChanged();
                mCheckoutActivity.paymentMethodName = mPaymentMethod.getName();
                mCheckoutActivity.paymentMethodCode = mPaymentMethod.getCode();
                mCheckoutActivity.paymentIdentifier = mPaymentMethod.getIdentifier();
                SharedPreference.getInstance().save("paymentMethod", mPaymentMethod.getCode());
            }
        });

    }

    /**
     * The type Payment method holder.
     */
/*Declared View Holder Variables*/
    public class PaymentMethodHolder extends RecyclerViewHolders {

        public RadioButton mPaymentRadioButton;
        public TextView mPaymentMethodName;
        public LinearLayout mPaymentMethodLayout;

        /**
         * Instantiates a new Payment method holder.
         *
         * @param v the v
         */
        public PaymentMethodHolder(View v) {
            super(v);
            this.mPaymentRadioButton = (RadioButton)v.findViewById(R.id.payment_radio_button);
            this.mPaymentMethodName = (TextView)v.findViewById(R.id.payment_method_name);
            this.mPaymentMethodLayout = (LinearLayout)v.findViewById(R.id.payment_method_layout);
        }

    }

    /**
     * @return int the size of mPaymentMethodList array
     */
    @Override
    public int getItemCount() {
        return mPaymentMethodList.size();
    }

}
