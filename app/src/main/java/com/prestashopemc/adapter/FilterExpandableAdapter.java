package com.prestashopemc.adapter;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;
import com.prestashopemc.R;
import com.prestashopemc.customView.CustomTextView;
import com.prestashopemc.model.FilterOption;
import com.prestashopemc.model.FilterResult;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.CustomBackground;
import com.prestashopemc.view.FilterActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Filter Expandable Adapter Contains Filter Expandable Holder</h1>
 * The Filter Expandable Adapter implements an adapter view for types of filters.
 * <p>
 * <b>Note:</b> Adapter values are get from Filter Result model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class FilterExpandableAdapter extends ExpandableRecyclerAdapter<FilterExpandableAdapter.MyParentViewHolder, FilterExpandableAdapter.MyChildViewHolder> {
    private LayoutInflater mInflater;
    private Context mContext;
    /**
     * The constant mSelectedFilter.
     */
    public static ArrayList<String> mSelectedFilter = new ArrayList<>();
    private FilterActivity mFilterActivity;

    /**
     * FilterExpandableAdapter constructor
     *
     * @param context        the context
     * @param parentItemList the parent item list
     */
    public FilterExpandableAdapter(Context context, List<ParentListItem> parentItemList) {
        super(parentItemList);
        this.mContext = context;
        mInflater = LayoutInflater.from(context);
    }

    /**
     * @param viewGroup
     * @return
     */
    @Override
    public MyParentViewHolder onCreateParentViewHolder(ViewGroup viewGroup) {
        mFilterActivity = (FilterActivity) mContext;
        View view = mInflater.inflate(R.layout.filter_parent_layout, viewGroup, false);
        return new MyParentViewHolder(view);
    }

    /**
     * @param viewGroup
     * @return
     */
    @Override
    public MyChildViewHolder onCreateChildViewHolder(ViewGroup viewGroup) {
        View view = mInflater.inflate(R.layout.filter_child_layout, viewGroup, false);
        return new MyChildViewHolder(view);
    }

    /**
     * @param parentViewHolder The {@code PVH} to bind data to
     * @param i
     * @param parentListItem   The {@link ParentListItem} which holds the data to
     */
    public void onBindParentViewHolder(final MyParentViewHolder parentViewHolder, final int i, final ParentListItem parentListItem) {
        final FilterResult headerParentListItem = (FilterResult) parentListItem;

        if (FilterActivity.sFilter.equals("productList")) {
            parentViewHolder.mExpandableImage.setImageResource(R.drawable.expandable_plus);
        } else if (FilterActivity.sFilter.equals("searchList")) {
            parentViewHolder.mExpandableImage.setImageResource(R.drawable.ic_cart_bottom);
        }

        parentViewHolder.mParentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!parentViewHolder.isExpanded()) {
                    parentViewHolder.expandView();
                    if (FilterActivity.sFilter.equals("productList")) {
                        parentViewHolder.mExpandableImage.setImageResource(R.drawable.expandable_minus);
                    } else if (FilterActivity.sFilter.equals("searchList")) {
                        parentViewHolder.mExpandableImage.setImageResource(R.drawable.ic_cart_top);
                    }
                } else {
                    parentViewHolder.collapseView();
                    if (FilterActivity.sFilter.equals("productList")) {
                        parentViewHolder.mExpandableImage.setImageResource(R.drawable.expandable_plus);
                    } else if (FilterActivity.sFilter.equals("searchList")) {
                        parentViewHolder.mExpandableImage.setImageResource(R.drawable.ic_cart_bottom);
                    }
                }
            }
        });


        if (AppConstants.sPrestashopMagento.equalsIgnoreCase(AppConstants.sMagento)) {
            parentViewHolder.mFilterName.setText(headerParentListItem.getLabel());

        } else {
            parentViewHolder.mFilterName.setText(headerParentListItem.getName());

        }

        if (FilterActivity.sFilter.equals("productList")) {
            parentViewHolder.mFilterName.setTypeface(mFilterActivity.robotoRegular);
        } else if (FilterActivity.sFilter.equals("searchList")) {
            parentViewHolder.mFilterName.setTextColor(mContext.getResources().getColor(R.color.attribute_text_name));
            parentViewHolder.mFilterName.setTypeface(mFilterActivity.robotoRegular);
            parentViewHolder.mParentLayout.setBackgroundColor(mContext.getResources().getColor(R.color.attribute_bg_color));
        }

    }

    /**
     * @param childViewHolder The {@code CVH} to bind data to
     * @param position        the position        The index in the list at which to bind
     * @param childListItem   The child list item which holds that data to be
     */
    @Override
    public void onBindChildViewHolder(final MyChildViewHolder childViewHolder, int position, final Object childListItem) {
        final FilterOption filterChildListItem = (FilterOption) childListItem;

        childViewHolder.mFilterName.setText(filterChildListItem.getLabel());
        if (FilterActivity.sFilter.equals("productList")) {
            childViewHolder.mFilterName.setTypeface(mFilterActivity.robotoRegular);
        } else if (FilterActivity.sFilter.equals("searchList")) {
            childViewHolder.mFilterName.setTextColor(mContext.getResources().getColor(R.color.attribute_text_name));
            childViewHolder.mFilterName.setTypeface(mFilterActivity.robotoLight);
        }

        if (mSelectedFilter.size() != 0) {
            if (mSelectedFilter.contains(filterChildListItem.getValue())) {
                childViewHolder.mCheckBox.setChecked(true);
            }
        }
        CustomBackground.setCheckBoxColor(childViewHolder.mCheckBox);

        childViewHolder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                boolean boxCheck = childViewHolder.mCheckBox.isChecked();
                if (boxCheck) {
                    CustomBackground.setCheckBoxColor(childViewHolder.mCheckBox);
                    mSelectedFilter.add(filterChildListItem.getValue());
                } else {
                    CustomBackground.setCheckBoxColor(childViewHolder.mCheckBox);
                    mSelectedFilter.remove(filterChildListItem.getValue());
                }
            }
        });

    }

    /**
     * Parent view holder for Filter Attribute
     */
    public class MyParentViewHolder extends ParentViewHolder {

        /**
         * The M filter name.
         */
        public CustomTextView mFilterName;
        /**
         * The M expandable image.
         */
        public ImageView mExpandableImage;
        /**
         * The M parent layout.
         */
        public RelativeLayout mParentLayout;

        /**
         * Instantiates a new My parent view holder.
         *
         * @param itemView the item view
         */
        public MyParentViewHolder(View itemView) {
            super(itemView);
            mFilterName = (CustomTextView) itemView.findViewById(R.id.filter_header_name);
            mParentLayout = (RelativeLayout) itemView.findViewById(R.id.parent_filter_layout);
            mExpandableImage = (ImageView) itemView.findViewById(R.id.item_plus);
        }
    }

    /**
     * Child view holder for Filter Attribute
     */
    public class MyChildViewHolder extends ChildViewHolder {

        public CustomTextView mFilterName;
        public AppCompatCheckBox mCheckBox;

        /**
         * Instantiates a new My child view holder.
         *
         * @param itemView the item view
         */
        public MyChildViewHolder(View itemView) {
            super(itemView);
            mFilterName = (CustomTextView) itemView.findViewById(R.id.filter_child_name);
            mCheckBox = (AppCompatCheckBox) itemView.findViewById(R.id.child_checkBox);
        }
    }
}
