package com.prestashopemc.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.model.Imagelist;
import com.prestashopemc.view.ProductDetailActivity;
import com.prestashopemc.view.ProductListActivity;
import com.prestashopemc.view.SearchActivity;
import com.prestashopemc.view.SubCategoryActivity;
import com.squareup.picasso.Picasso;

import java.util.List;


/**
 * <h1>Big Banner Adapter Contains Big Banner Holder</h1>
 * The Big Banner Adapter implements an adapter view for big Banners.
 * <p>
 * <b>Note:</b> Adapter values are get from Image List model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class BigBannerAdapter extends RecyclerView.Adapter<BigBannerAdapter.BigBannerHolder> {
    private List<Imagelist> mSubBannerList;
    private Context mContext;
    private Imagelist mSubBannerObj;
    private String mBannerName;

    /**
     * Instantiates a new Big banner adapter.
     *
     * @param context       the context
     * @param subBannerList the sub banner list
     * @param bannerName    the banner name
     */
    public BigBannerAdapter(Context context, List<Imagelist> subBannerList, String bannerName) {
        this.mSubBannerList = subBannerList;
        this.mContext = context;
        this.mBannerName = bannerName;
    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public BigBannerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.big_banner_adapter, null);
        WindowManager windowManager = (WindowManager)mContext.getSystemService(Context.WINDOW_SERVICE);
        int width = windowManager.getDefaultDisplay().getWidth();
        int height = windowManager.getDefaultDisplay().getHeight();
        view.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.MATCH_PARENT));

        BigBannerHolder viewHolder = new BigBannerHolder(view);
        return viewHolder;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(BigBannerHolder holder, final int position) {

        mSubBannerObj = mSubBannerList.get(position);

        Picasso.with(mContext).load(mSubBannerObj.getImageUrl())
                .placeholder(R.drawable.place_holder).fit()
                .into(holder.mBannerImage);
//
//        if (bannerName.isEmpty()) {
//            holder.mBannerName.setVisibility(View.GONE);
//        } else {
//            holder.mBannerName.setVisibility(View.VISIBLE);
//            holder.mBannerName.setText(bannerName);
//        }

        holder.mBannerImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mSubBannerObj = mSubBannerList.get(position);



                if (mSubBannerObj.getIsType()) {
                    if (mSubBannerObj.getType().equalsIgnoreCase("product")) {
                        Intent in = new Intent(mContext, ProductDetailActivity.class);
                        in.putExtra("product_id", mSubBannerObj.getProductId());
                        in.putExtra("product_name", mSubBannerObj.getProductName());
                        mContext.startActivity(in);
                    } else if (mSubBannerObj.getType().equalsIgnoreCase("category")) {

                        if (String.valueOf(mSubBannerObj.getCategoryCount()).equalsIgnoreCase("0")) {
                            Intent intent = new Intent(mContext, ProductListActivity.class);
                            intent.putExtra("categoryid", mSubBannerObj.getCategoryId());
                            intent.putExtra("categoryName", mSubBannerObj.getCategoryName());
                            intent.putExtra("productCount", mSubBannerObj.getProductCount()+"");
                            mContext.startActivity(intent);
                        } else {
                            Intent intent = new Intent(mContext, SubCategoryActivity.class);
                            intent.putExtra("sCategoriesId", mSubBannerObj.getCategoryId());
                            intent.putExtra("categoryName", mSubBannerObj.getCategoryName());
                            mContext.startActivity(intent);
                        }

                    } else {
                        Intent intent = new Intent(mContext, SearchActivity.class);
                        intent.putExtra("search_text", mSubBannerObj.getSearchWord());
                        mContext.startActivity(intent);
                    }
                }
            }
        });
    }

    /**
     * The type Big banner holder.
     */
    public class BigBannerHolder extends RecyclerViewHolders {

        public ImageView mBannerImage;
        public TextView mBannerName;

        /**
         * Instantiates a new Big banner holder.
         *
         * @param v the v
         */
        public BigBannerHolder(View v) {
            super(v);
            this.mBannerImage = (ImageView) v.findViewById(R.id.banner_slot_one_image);
            this.mBannerName = (TextView) v.findViewById(R.id.banner_name);
        }
    }

    /**
     * @return
     */
    @Override
    public int getItemCount() {
        return this.mSubBannerList.size();
    }

}



