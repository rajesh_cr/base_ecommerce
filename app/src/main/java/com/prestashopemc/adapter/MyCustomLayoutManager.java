package com.prestashopemc.adapter;

import android.content.Context;
import android.graphics.PointF;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSmoothScroller;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;

/**
 * <h1>MyCustomLayoutManager extends LinearLayoutManager</h1>
 * The My Custom Layout implements an custom layout.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class MyCustomLayoutManager extends LinearLayoutManager {
    private static final float MILLISECONDS_PER_INCH = 50f;
    private Context mContext;

    /**
     * Instantiates a new My custom layout manager.
     *
     * @param context the context
     */
    public MyCustomLayoutManager(Context context) {
        super(context);
        mContext = context;
    }

    /**
     * Instantiates a new My custom layout manager.
     *
     * @param context       the context
     * @param orientation   the orientation
     * @param reverseLayout the reverse layout
     */
    public MyCustomLayoutManager(Context context, int orientation, boolean reverseLayout) {
        super(context, orientation, reverseLayout);
        mContext = context;
//        setOrientation(orientation);
//        setReverseLayout(reverseLayout);
//        setAutoMeasureEnabled(true);
    }

    /**
     * @param recyclerView
     * @param state
     * @param position the position
     */
    @Override
    public void smoothScrollToPosition(RecyclerView recyclerView,
                                       RecyclerView.State state, final int position) {

        LinearSmoothScroller smoothScroller =
                new LinearSmoothScroller(mContext) {

                    //This controls the direction in which smoothScroll looks
                    //for your view
                    @Override
                    public PointF computeScrollVectorForPosition
                    (int targetPosition) {
                        return MyCustomLayoutManager.this
                                .computeScrollVectorForPosition(targetPosition);
                    }

                    //This returns the milliseconds it takes to
                    //scroll one pixel.
                    @Override
                    protected float calculateSpeedPerPixel
                    (DisplayMetrics displayMetrics) {
                        return MILLISECONDS_PER_INCH / displayMetrics.densityDpi;
                    }
                };

        smoothScroller.setTargetPosition(position);
        startSmoothScroll(smoothScroller);
    }

}
