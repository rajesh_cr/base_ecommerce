package com.prestashopemc.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.prestashopemc.R;
import com.prestashopemc.customView.CustomTextView;
import com.prestashopemc.model.CmsPage;
import com.prestashopemc.view.CmsActivity;
import com.prestashopemc.view.MainActivity;

import java.util.List;

/**
 * <h1>CMS List Adapter Contains CMS List Holder</h1>
 * The CMS List Adapter implements an adapter view for cms list.
 * <p>
 * <b>Note:</b> Adapter values are get from CMS Page model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class CmsListAdapter extends RecyclerView.Adapter<CmsListAdapter.CmsListHolder> {

    private List<CmsPage> mCmsPageList;
    private Context mContext;
    private CmsPage mCmsObject;
    private MainActivity mActivity;

    /**
     * Constructor
     *
     * @param mContext     the m context
     * @param mCmsPageList the m cms page list
     */
    public CmsListAdapter(Context mContext, List<CmsPage> mCmsPageList) {
        this.mCmsPageList = mCmsPageList;
        this.mContext = mContext;
        mActivity = (MainActivity) mContext;
    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public CmsListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_parent, null);
        CmsListHolder viewHolder = new CmsListHolder(view);
        return viewHolder;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(CmsListHolder holder, final int position) {

        mCmsObject = mCmsPageList.get(position);
        holder.mCmsText.setText(mCmsObject.getTitle());
        holder.mCmsText.setTypeface(mActivity.proximanovaAltRegular);

        /**
         * CMS details view Listener*/
        holder.mCmsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCmsObject = mCmsPageList.get(position);
                Intent cmsIntent = new Intent(mContext, CmsActivity.class);
                cmsIntent.putExtra("cmsTitle", mCmsObject.getTitle());
                cmsIntent.putExtra("cmsIdentifier", mCmsObject.getPage_identifier());
                mContext.startActivity(cmsIntent);
            }
        });
    }

    /**
     * Holder
     */
    public class CmsListHolder extends RecyclerViewHolders {

        public CustomTextView mCmsText;
        public RelativeLayout mCmsLayout;

        /**
         * Instantiates a new Cms list holder.
         *
         * @param v the v
         */
        public CmsListHolder(View v) {
            super(v);
            this.mCmsText = (CustomTextView) v.findViewById(R.id.category_header_name);
            this.mCmsLayout = (RelativeLayout) v.findViewById(R.id.cms_layout);
        }
    }


    /**
     * @return
     */
    @Override
    public int getItemCount() {
        return this.mCmsPageList.size();
    }
}
