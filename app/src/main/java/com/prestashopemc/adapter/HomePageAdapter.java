package com.prestashopemc.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.prestashopemc.R;
import com.prestashopemc.holderviews.FirstSlotViewHolder;
import com.prestashopemc.holderviews.SecondSlotViewHolder;
import com.prestashopemc.holderviews.ThreeSlotViewHolder;
import com.prestashopemc.model.HomePageList;
import com.prestashopemc.model.Imagelist;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Home Page Adapter Contains Home Page Holder</h1>
 * The Home Page Adapter implements an adapter view for home page banner slots.
 * <p>
 * <b>Note:</b> Adapter values are get from Home Page List model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class HomePageAdapter extends RecyclerView.Adapter<RecyclerViewHolders> {
    private List<HomePageList> itemList;
    private Context context;
    public static final int mHomeOne = 1;
    public static final int mHomeTwo = 2;
    public static final int mHomeThree = 3;
    private HomePageList mNewBanObj;
    private List<Imagelist> subBannerList = new ArrayList<Imagelist>();

    /**
     * Instantiates a new Home page adapter.
     *
     * @param context  the context
     * @param itemList the item list
     */
    public HomePageAdapter(Context context, List<HomePageList> itemList) {
        this.itemList = itemList;
        this.context = context;
    }

    /*@Override
    public int getItemViewType(int position) {
        // here your custom logic to choose the view type
        mNewBanObj = itemList.get(position);

        return Integer.parseInt(mNewBanObj.getSlotNumber());
    }*/

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {

            case mHomeOne:
                return new FirstSlotViewHolder(LayoutInflater.from(context).inflate(R.layout.big_banner_layout, parent, false));
            case mHomeTwo:
                return new SecondSlotViewHolder(LayoutInflater.from(context).inflate(R.layout.small_banner_layout, parent, false));
            case mHomeThree:
                return new ThreeSlotViewHolder(LayoutInflater.from(context).inflate(R.layout.square_banner_layout, parent, false));
        }
        return null;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, int position) {

        mNewBanObj = itemList.get(position);



       /* int slotNumber = Integer.parseInt(mNewBanObj.getSlotNumber());

        if (slotNumber == mHomeOne) {
            HomePageList mNewBanObj = itemList.get(position);
            FirstSlotViewHolder holder3 = (FirstSlotViewHolder) holder;
            holder3.first_slot_recycler_view.setVisibility(View.VISIBLE);
            MyCustomLayoutManager mFirstLinearLayout = new MyCustomLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            holder3.first_slot_recycler_view.setLayoutManager(mFirstLinearLayout);
            holder3.first_slot_recycler_view.smoothScrollToPosition(0);
            holder3.first_slot_recycler_view.setHasFixedSize(true);
            BigBannerAdapter mFirstSlotAdapter = new BigBannerAdapter(context, mNewBanObj.getImagelist(), mNewBanObj.getBannerName());
            holder3.first_slot_recycler_view.setAdapter(mFirstSlotAdapter);
            holder3.first_slot_recycler_view.setNestedScrollingEnabled(true);

        } else if (slotNumber == mHomeTwo) {
            HomePageList mNewBanObj = itemList.get(position);
            SecondSlotViewHolder secondHolder = (SecondSlotViewHolder) holder;
            secondHolder.second_slot_recycler_view.setVisibility(View.VISIBLE);
            MyCustomLayoutManager mSecondLinearLayout = new MyCustomLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            secondHolder.second_slot_recycler_view.setLayoutManager(mSecondLinearLayout);
            secondHolder.second_slot_recycler_view.smoothScrollToPosition(0);
            secondHolder.second_slot_recycler_view.setHasFixedSize(true);
            SmallBannerAdapter secondSlotAdapter = new SmallBannerAdapter(context, mNewBanObj.getImagelist(), mNewBanObj.getBannerName());
            secondHolder.second_slot_recycler_view.setAdapter(secondSlotAdapter);
            secondHolder.second_slot_recycler_view.setNestedScrollingEnabled(true);
        } else if (slotNumber == mHomeThree) {

            HomePageList mNewBanObj = itemList.get(position);
            ThreeSlotViewHolder threeHolder = (ThreeSlotViewHolder) holder;
            threeHolder.third_slot_recycler_view.setVisibility(View.VISIBLE);
            MyCustomLayoutManager mThreeLinearLayout = new MyCustomLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
            threeHolder.third_slot_recycler_view.setLayoutManager(mThreeLinearLayout);
            threeHolder.third_slot_recycler_view.smoothScrollToPosition(0);
            threeHolder.third_slot_recycler_view.setHasFixedSize(true);
            SquareBannerAdapter threeSlotAdapter = new SquareBannerAdapter(context, mNewBanObj.getImagelist(), mNewBanObj.getBannerName());
            threeHolder.third_slot_recycler_view.setAdapter(threeSlotAdapter);
            threeHolder.third_slot_recycler_view.setNestedScrollingEnabled(true);
        }*/
    }

    /**
     * @return int the size of item list array
     */
    @Override
    public int getItemCount() {
        return this.itemList.size();
    }

}

