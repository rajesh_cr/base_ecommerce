package com.prestashopemc.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;


/**
 * <h1>Recycler View Holder!</h1>
 * TheRecycler View Holder holds an view holder for image view.
 * <p>
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

    public ImageView mCategoryImage;

    /**
     * Instantiates a new Recycler view holders.
     *
     * @param itemView the item view
     */
    public RecyclerViewHolders(View itemView) {
        super(itemView);
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
    }
}