package com.prestashopemc.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Typeface;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.prestashopemc.R;
import com.prestashopemc.util.AppConstants;
import com.prestashopemc.util.SharedPreference;
import com.prestashopemc.view.MagentoProductDetailActivity;
import com.prestashopemc.view.MainActivity;
import com.prestashopemc.view.ProductDetailActivity;
import com.prestashopemc.view.ProductListActivity;
import com.prestashopemc.view.SearchActivity;
import com.prestashopemc.view.SubCategoryActivity;
import com.prestashopemc.webModel.ImageBorder;
import com.prestashopemc.webModel.ImageList;
import com.prestashopemc.webModel.Link;
import com.prestashopemc.webModel.SubTitle;
import com.prestashopemc.webModel.Title;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * <h1>Web Banner Adapter Contains Web Banner Holder</h1>
 * The Web Banner Adapter implements an adapter view for Web Banner.
 * <p>
 * <b>Note:</b> Adapter values are get from ImageList model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class WebBannerAdapter extends RecyclerView.Adapter<WebBannerAdapter.WebBannerHolder>{

    private Context mContext;
    private List<ImageList> mImageLists;
    private ImageList mImageList;
    private int mSlotNumber;
    private ImageBorder mBorderModel = new ImageBorder();
    private MainActivity mMainActivity;
    private String mIsoCode;

    /**
     * Instantiates a new Web banner adapter.
     *
     * @param context     the context
     * @param imageLists  the image lists
     * @param mSlotNumber the m slot number
     * @param border      the border
     */
    public WebBannerAdapter(Context context, List<ImageList> imageLists, int mSlotNumber, ImageBorder border){
        this.mContext = context;
        this.mImageLists = imageLists;
        this.mSlotNumber = mSlotNumber;
        this.mBorderModel = border;
    }

    /**
     * @return
     */
    @Override
    public int getItemCount() {
        return this.mImageLists.size();
    }

    /**
     * @param position the position
     * @return
     */
    @Override
    public int getItemViewType(int position) {
        return mSlotNumber;
    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public WebBannerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mMainActivity = (MainActivity)mContext;
        View view = null;

        switch (viewType){
            case 1:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.static_full_banner, null);
                setCategoryAdjustWidth(view, "fixed");
                break;
            case 2:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.static_half_banner, null);
                setCategoryAdjustWidth(view, "fixed");
                break;
            case 3:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.slider_full_banner, null);
                setCategoryAdjustWidth(view, "fixed");
                break;
            case 4:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.slider_half_banner, null);
                setCategoryAdjustWidth(view, "fixed");
                break;
            case 5:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.static_square_banner, null);
                setCategoryAdjustWidth(view, "square");
                break;
            case 6:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.three_four_slider_full, null);
                break;
            case 7:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.three_four_slider_half, null);
                break;
            case 8:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.static_full_categroy_three, null);
                setCategoryAdjustWidth(view, "three");
                break;
            case 9:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.static_half_category_three, null);
                setCategoryAdjustWidth(view, "three");
                break;
            case 10:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.slider_full_category_three, null);
                setCategoryAdjustWidth(view, "three");
                break;
            case 11:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.slider_half_category_three, null);
                setCategoryAdjustWidth(view, "three");
                break;
            case 12:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.static_full_category_four, null);
                setCategoryAdjustWidth(view, "four");
                break;
            case 13:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.static_half_category_four, null);
                setCategoryAdjustWidth(view, "four");
                break;
            case 14:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.slider_full_category_four, null);
                setCategoryAdjustWidth(view, "four");
                break;
            case 15:
                view = LayoutInflater.from(parent.getContext()).inflate(R.layout.slider_half_category_four, null);
                setCategoryAdjustWidth(view, "four");
                break;
        }

        WebBannerHolder viewHolder = new WebBannerHolder(view);
        return viewHolder;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(WebBannerHolder holder, final int position) {
        mImageList = mImageLists.get(position);
        mIsoCode = SharedPreference.getInstance().getValue("iso_code");
        if (mSlotNumber == 1) {
                ImageList imageList = mImageLists.get(position);
                Title mTitle = imageList.getTitle();
                final Link mLink = imageList.getLink();
                int i = 0;
                for (SubTitle subTitle : mTitle.getTitle()){
                    if (subTitle.getCode().equals(mIsoCode)) {
                        if (!subTitle.getValue().isEmpty()) {
                            holder.staticFullBannerName.setVisibility(View.VISIBLE);
                            holder.staticFullBannerName.setText(subTitle.getValue());
                        }else {
                            holder.staticFullBannerName.setVisibility(View.GONE);
                        }
                    }
                }
                customTextSettings(holder.staticFullBannerName, mTitle, mTitle.getBackgroundColor());
                if (mTitle.getTitle().isEmpty()) {
                    holder.staticFullBannerImage.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                }
                if (!imageList.getMediumImageUrl().isEmpty()) {
                    Picasso.with(mContext).load(imageList.getMediumImageUrl()).placeholder(R.drawable.place_holder).fit().into(holder.staticFullBannerImage);
                    if (!mBorderModel.getColor().isEmpty()) {
                        setImageBorder(holder.staticFullBannerLayout, mBorderModel);
                    }
                } else {
                    holder.staticFullBannerImage.setVisibility(View.GONE);
                }

                holder.staticFullBannerImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setRedirectLink(mLink);
                    }
                });

        } else if (mSlotNumber == 2){
            ImageList imageList = mImageLists.get(position);
                Title mTitle = imageList.getTitle();
                final Link mLink = imageList.getLink();
                int i = 0;
                for (SubTitle subTitle : mTitle.getTitle()){
                    if (subTitle.getCode().equals(mIsoCode)) {

                        if (!subTitle.getValue().isEmpty()) {
                            holder.staticHalfBannerName.setVisibility(View.VISIBLE);
                            holder.staticHalfBannerName.setText(subTitle.getValue());
                        }else {
                            holder.staticHalfBannerName.setVisibility(View.GONE);
                        }
                    }
                }
                customTextSettings(holder.staticHalfBannerName, mTitle, mTitle.getBackgroundColor());
                if (mTitle.getTitle().isEmpty()) {
                    holder.staticHalfBannerImage.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                }
                if (!imageList.getMediumImageUrl().isEmpty()) {
                    Picasso.with(mContext).load(imageList.getMediumImageUrl()).placeholder(R.drawable.place_holder).fit().into(holder.staticHalfBannerImage);
                    if (!mBorderModel.getColor().isEmpty()) {
                        setImageBorder(holder.staticHalfBannerLayout, mBorderModel);
                    }
                } else {
                    holder.staticHalfBannerImage.setVisibility(View.GONE);
                }

                holder.staticHalfBannerImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setRedirectLink(mLink);
                    }
                });

        } else if (mSlotNumber == 3){
            ImageList imageList = mImageLists.get(position);
                Title mTitle = imageList.getTitle();
                final Link mLink = imageList.getLink();
                int i = 0;

                for (SubTitle subTitle : mTitle.getTitle()){
                    if (subTitle.getCode().equals(mIsoCode)) {

                        if (!subTitle.getValue().isEmpty()) {
                            holder.sliderFullBannerName.setVisibility(View.VISIBLE);
                            holder.sliderFullBannerName.setText(subTitle.getValue());
                        }else {
                            holder.sliderFullBannerName.setVisibility(View.GONE);
                        }
                    }
                }
                customTextSettings(holder.sliderFullBannerName, mTitle, mTitle.getBackgroundColor());
                if (mTitle.getTitle().isEmpty()) {
                    holder.sliderFullBannerImage.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                }
                if (!imageList.getMediumImageUrl().isEmpty()) {
                    String imgUrl = imageList.getMediumImageUrl();
                    Picasso.with(mContext).load(imgUrl).placeholder(R.drawable.place_holder).fit().into(holder.sliderFullBannerImage);
                    if (!mBorderModel.getColor().isEmpty()) {
                        setImageBorder(holder.sliderFullBannerLayout, mBorderModel);
                    }
                } else {
                    holder.sliderFullBannerImage.setVisibility(View.GONE);
                }

                holder.sliderFullBannerImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setRedirectLink(mLink);
                    }
                });

        } else if (mSlotNumber == 4){
            ImageList imageList = mImageLists.get(position);
                Title mTitle = imageList.getTitle();
                final Link mLink = imageList.getLink();
                int i = 0;
                for (SubTitle subTitle : mTitle.getTitle()){
                    if (subTitle.getCode().equals(mIsoCode)) {

                        if (!subTitle.getValue().isEmpty()) {
                            holder.sliderHalfBannerName.setVisibility(View.VISIBLE);
                            holder.sliderHalfBannerName.setText(subTitle.getValue());
                        }else {
                            holder.sliderHalfBannerName.setVisibility(View.GONE);
                        }
                    }
                }
                customTextSettings(holder.sliderHalfBannerName, mTitle, mTitle.getBackgroundColor());
                if (mTitle.getTitle().isEmpty()) {
                    holder.sliderHalfBannerImage.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                }
                if (!imageList.getMediumImageUrl().isEmpty()) {
                    Picasso.with(mContext).load(imageList.getMediumImageUrl()).placeholder(R.drawable.place_holder).fit().into(holder.sliderHalfBannerImage);
                    if (!mBorderModel.getColor().isEmpty()) {
                        setImageBorder(holder.sliderHalfBannerLayout, mBorderModel);
                    }
                } else {
                    holder.sliderHalfBannerImage.setVisibility(View.GONE);
                }

                holder.sliderHalfBannerImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setRedirectLink(mLink);
                    }
                });

        } else if (mSlotNumber == 5){
            ImageList imageList = mImageLists.get(position);
                Title mTitle = imageList.getTitle();
                final Link mLink = imageList.getLink();
                int i = 0;
                for (SubTitle subTitle : mTitle.getTitle()){
                    if (subTitle.getCode().equals(mIsoCode)) {

                        if (!subTitle.getValue().isEmpty()) {
                            holder.staticSquareBannerName.setVisibility(View.VISIBLE);
                            holder.staticSquareBannerName.setText(subTitle.getValue());
                        }else {
                            holder.staticSquareBannerName.setVisibility(View.GONE);
                        }
                    }
                }
                customTextSettings(holder.staticSquareBannerName, mTitle, mTitle.getBackgroundColor());
                if (mTitle.getTitle().isEmpty()) {
                    holder.staticSquareBannerImage.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                }
                if (!imageList.getMediumImageUrl().isEmpty()) {
                    String imgUrl = imageList.getMediumImageUrl();
                    Picasso.with(mContext).load(imgUrl).placeholder(R.drawable.place_holder).fit().into(holder.staticSquareBannerImage);
                    if (!mBorderModel.getColor().isEmpty()) {
                        setImageBorder(holder.staticSquareBannerLayout, mBorderModel);
                    }
                } else {
                    holder.staticSquareBannerImage.setVisibility(View.GONE);
                }

                holder.staticSquareBannerImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setRedirectLink(mLink);
                    }
                });


        } else if (mSlotNumber == 6){
            ImageList imageList = mImageLists.get(position);
                Title mTitle = imageList.getTitle();
                final Link mLink = imageList.getLink();
            int i = 0;
                for (SubTitle subTitle : mTitle.getTitle()){
                    if (subTitle.getCode().equals(mIsoCode)) {

                        if (!subTitle.getValue().isEmpty()) {
                            holder.threeFourSliderFullName.setVisibility(View.VISIBLE);
                            holder.threeFourSliderFullName.setText(subTitle.getValue());
                        }else {
                            holder.threeFourSliderFullName.setVisibility(View.GONE);
                        }
                    }
                }
                customTextSettings(holder.threeFourSliderFullName, mTitle, mTitle.getBackgroundColor());
                if (mTitle.getTitle().isEmpty()) {
                    holder.threeFourSliderFullImage.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                }
                if (!imageList.getMediumImageUrl().isEmpty()) {
                    Picasso.with(mContext).load(imageList.getMediumImageUrl()).placeholder(R.drawable.place_holder).fit().into(holder.threeFourSliderFullImage);
                    if (!mBorderModel.getColor().isEmpty()) {
                        setImageBorder(holder.threeFourSliderFullLayout, mBorderModel);
                    }
                } else {
                    holder.threeFourSliderFullImage.setVisibility(View.GONE);
                }

                holder.threeFourSliderFullImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setRedirectLink(mLink);
                    }
                });

        } else if (mSlotNumber == 7){
            ImageList imageList = mImageLists.get(position);
                Title mTitle = imageList.getTitle();
                final Link mLink = imageList.getLink();
                int i = 0;
                for (SubTitle subTitle : mTitle.getTitle()){
                    if (subTitle.getCode().equals(mIsoCode)) {

                        if (!subTitle.getValue().isEmpty()) {
                            holder.threeFourSliderHalfName.setVisibility(View.VISIBLE);
                            holder.threeFourSliderHalfName.setText(subTitle.getValue());
                        }else {
                            holder.threeFourSliderHalfName.setVisibility(View.GONE);
                        }
                    }
                }
                customTextSettings(holder.threeFourSliderHalfName, mTitle, mTitle.getBackgroundColor());
                if (mTitle.getTitle().isEmpty()) {
                    holder.threeFourSliderHalfImage.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                }
                if (!imageList.getMediumImageUrl().isEmpty()) {
                    Picasso.with(mContext).load(imageList.getMediumImageUrl()).placeholder(R.drawable.place_holder).fit().into(holder.threeFourSliderHalfImage);
                    if (!mBorderModel.getColor().isEmpty()) {
                        setImageBorder(holder.threeFourSliderHalfLayout, mBorderModel);
                    }
                } else {
                    holder.threeFourSliderHalfImage.setVisibility(View.GONE);
                }

                holder.threeFourSliderHalfImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setRedirectLink(mLink);
                    }
                });

        } else if (mSlotNumber == 8){
            ImageList imageList = mImageLists.get(position);
                Title mTitle = imageList.getTitle();
                final Link mLink = imageList.getLink();
                int i = 0;

                for (SubTitle subTitle : mTitle.getTitle()){

                    if (subTitle.getCode().equals(mIsoCode)) {

                        if (!subTitle.getValue().isEmpty()) {
                            holder.staticFullCatThreeName.setVisibility(View.VISIBLE);
                            holder.staticFullCatThreeName.setText(subTitle.getValue());
                        }else {
                            holder.staticFullCatThreeName.setVisibility(View.GONE);
                        }
                    }
                }

                customTextSettings(holder.staticFullCatThreeName, mTitle, mTitle.getBackgroundColor());
                if (mTitle.getTitle().isEmpty()) {
                    holder.staticFullCatThreeImage.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                }
                if (!imageList.getMediumImageUrl().isEmpty()) {
                    Picasso.with(mContext).load(imageList.getMediumImageUrl()).placeholder(R.drawable.place_holder).fit().into(holder.staticFullCatThreeImage);
                    if (!mBorderModel.getColor().isEmpty()) {
                        setImageBorder(holder.staticFullCatThreeLayout, mBorderModel);
                    }
                } else {
                    holder.staticFullCatThreeImage.setVisibility(View.GONE);
                }

                holder.staticFullCatThreeImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setRedirectLink(mLink);
                    }
                });

        } else if (mSlotNumber == 9){
            ImageList imageList = mImageLists.get(position);
                Title mTitle = imageList.getTitle();
                final Link mLink = imageList.getLink();
                int i = 0;
                for (SubTitle subTitle : mTitle.getTitle()){
                    if (subTitle.getCode().equals(mIsoCode)) {

                        if (!subTitle.getValue().isEmpty()) {
                            holder.staticHalfCatThreeName.setVisibility(View.VISIBLE);
                            holder.staticHalfCatThreeName.setText(subTitle.getValue());
                        }else {
                            holder.staticHalfCatThreeName.setVisibility(View.GONE);
                        }
                    }
                }
                customTextSettings(holder.staticHalfCatThreeName, mTitle, mTitle.getBackgroundColor());
                if (mTitle.getTitle().isEmpty()) {
                    holder.staticHalfCatThreeImage.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                }
                if (!imageList.getMediumImageUrl().isEmpty()) {
                    Picasso.with(mContext).load(imageList.getMediumImageUrl()).placeholder(R.drawable.place_holder).fit().into(holder.staticHalfCatThreeImage);
                    if (!mBorderModel.getColor().isEmpty()) {
                        setImageBorder(holder.staticHalfCatThreeLayout, mBorderModel);
                    }
                } else {
                    holder.staticHalfCatThreeImage.setVisibility(View.GONE);
                }

                holder.staticHalfCatThreeImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setRedirectLink(mLink);
                    }
                });

        } else if (mSlotNumber == 10){
            ImageList imageList = mImageLists.get(position);
                Title mTitle = imageList.getTitle();
                final Link mLink = imageList.getLink();
                int i = 0;
                for (SubTitle subTitle : mTitle.getTitle()){
                    if (subTitle.getCode().equals(mIsoCode)) {

                        if (!subTitle.getValue().isEmpty()) {
                            holder.sliderFullCatThreeName.setVisibility(View.VISIBLE);
                            holder.sliderFullCatThreeName.setText(subTitle.getValue());
                        }else {
                            holder.sliderFullCatThreeName.setVisibility(View.GONE);
                        }
                    }
                }
                customTextSettings(holder.sliderFullCatThreeName, mTitle, mTitle.getBackgroundColor());
                if (mTitle.getTitle().isEmpty()) {
                    holder.sliderFullCatThreeImage.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                }
                if (!imageList.getMediumImageUrl().isEmpty()) {
                    String imgUrl = imageList.getMediumImageUrl();
                    Picasso.with(mContext).load(imgUrl).placeholder(R.drawable.place_holder).fit().into(holder.sliderFullCatThreeImage);
                    if (!mBorderModel.getColor().isEmpty()) {
                        setImageBorder(holder.sliderFullCatThreeLayout, mBorderModel);
                    }
                } else {
                    holder.sliderFullCatThreeImage.setVisibility(View.GONE);
                }

                holder.sliderFullCatThreeImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setRedirectLink(mLink);
                    }
                });

        } else if (mSlotNumber == 11){

            ImageList imageList = mImageLists.get(position);
                Title mTitle = imageList.getTitle();
                final Link mLink = imageList.getLink();
                int i = 0;
                for (SubTitle subTitle : mTitle.getTitle()){
                    if (subTitle.getCode().equals(mIsoCode)) {

                        if (!subTitle.getValue().isEmpty()) {
                            holder.sliderHalfCatThreeName.setVisibility(View.VISIBLE);
                            holder.sliderHalfCatThreeName.setText(subTitle.getValue());
                        }else {
                            holder.sliderHalfCatThreeName.setVisibility(View.GONE);
                        }
                    }
                }
                customTextSettings(holder.sliderHalfCatThreeName, mTitle, mTitle.getBackgroundColor());
                if (mTitle.getTitle().isEmpty()) {
                    holder.sliderHalfCatThreeImage.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                }
                if (!imageList.getMediumImageUrl().isEmpty()) {
                    Picasso.with(mContext).load(imageList.getMediumImageUrl()).placeholder(R.drawable.place_holder).fit().into(holder.sliderHalfCatThreeImage);
                    if (!mBorderModel.getColor().isEmpty()) {
                        setImageBorder(holder.sliderHalfCatThreeLayout, mBorderModel);
                    }
                } else {
                    holder.sliderHalfCatThreeImage.setVisibility(View.GONE);
                }

                holder.sliderHalfCatThreeImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setRedirectLink(mLink);
                    }
                });

        } else if (mSlotNumber == 12){
            ImageList imageList = mImageLists.get(position);
                Title mTitle = imageList.getTitle();
                final Link mLink = imageList.getLink();
                int i = 0;
                for (SubTitle subTitle : mTitle.getTitle()){
                    if (subTitle.getCode().equals(mIsoCode)) {

                        if (!subTitle.getValue().isEmpty()) {
                            holder.staticFullCatFourName.setVisibility(View.VISIBLE);
                            holder.staticFullCatFourName.setText(subTitle.getValue());
                        }else {
                            holder.staticFullCatFourName.setVisibility(View.GONE);
                        }
                    }
                }
                customTextSettings(holder.staticFullCatFourName, mTitle, mTitle.getBackgroundColor());
                if (mTitle.getTitle().isEmpty()) {
                    holder.staticFullCatFourImage.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                }
                if (!imageList.getMediumImageUrl().isEmpty()) {
                    String imgUrl = imageList.getMediumImageUrl();
                    Picasso.with(mContext).load(imgUrl).placeholder(R.drawable.place_holder).fit().into(holder.staticFullCatFourImage);
                    if (!mBorderModel.getColor().isEmpty()) {
                        setImageBorder(holder.staticFullCatFourLayout, mBorderModel);
                    }
                } else {
                    holder.staticFullCatFourImage.setVisibility(View.GONE);
                }

                holder.staticFullCatFourImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setRedirectLink(mLink);
                    }
                });

        } else if (mSlotNumber == 13) {
            ImageList imageList = mImageLists.get(position);
                Title mTitle = imageList.getTitle();
                final Link mLink = imageList.getLink();
                int i = 0;
                for (SubTitle subTitle : mTitle.getTitle()){
                    if (subTitle.getCode().equals(mIsoCode)) {

                        if (!subTitle.getValue().isEmpty()) {
                            holder.staticHalfCatFourName.setVisibility(View.VISIBLE);
                            holder.staticHalfCatFourName.setText(subTitle.getValue());
                        }else {
                            holder.staticHalfCatFourName.setVisibility(View.GONE);
                        }
                    }
                }
                customTextSettings(holder.staticHalfCatFourName, mTitle, mTitle.getBackgroundColor());
                if (mTitle.getTitle().isEmpty()) {
                    holder.staticHalfCatFourImage.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                }
                if (!imageList.getMediumImageUrl().isEmpty()) {
                    Picasso.with(mContext).load(imageList.getMediumImageUrl()).placeholder(R.drawable.place_holder).fit().into(holder.staticHalfCatFourImage);
                    if (!mBorderModel.getColor().isEmpty()) {
                        setImageBorder(holder.staticHalfCatFourLayout, mBorderModel);
                    }
                } else {
                    holder.staticHalfCatFourImage.setVisibility(View.GONE);
                }

                holder.staticHalfCatFourImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setRedirectLink(mLink);
                    }
                });

        } else if (mSlotNumber == 14){
            ImageList imageList = mImageLists.get(position);
                Title mTitle = imageList.getTitle();
                final Link mLink = imageList.getLink();
                int i = 0;
                for (SubTitle subTitle : mTitle.getTitle()){
                    if (subTitle.getCode().equals(mIsoCode)) {

                        if (!subTitle.getValue().isEmpty()) {
                            holder.sliderFullCatFourName.setVisibility(View.VISIBLE);
                            holder.sliderFullCatFourName.setText(subTitle.getValue());
                        }else {
                            holder.sliderFullCatFourName.setVisibility(View.GONE);
                        }
                    }
                }
                customTextSettings(holder.sliderFullCatFourName, mTitle,mTitle.getBackgroundColor());
                if (mTitle.getTitle().isEmpty()) {
                    holder.sliderFullCatFourImage.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                }
                if (!imageList.getMediumImageUrl().isEmpty()) {
                    Picasso.with(mContext).load(imageList.getMediumImageUrl()).placeholder(R.drawable.place_holder).fit().into(holder.sliderFullCatFourImage);
                    if (!mBorderModel.getColor().isEmpty()) {
                        setImageBorder(holder.sliderFullCatFourLayout, mBorderModel);
                    }
                } else {
                    holder.sliderFullCatFourImage.setVisibility(View.GONE);
                }

                holder.sliderFullCatFourImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setRedirectLink(mLink);
                    }
                });

        } else if (mSlotNumber == 15){
            ImageList imageList = mImageLists.get(position);
                Title mTitle = imageList.getTitle();
                final Link mLink = imageList.getLink();
                int i = 0;
                for (SubTitle subTitle : mTitle.getTitle()){
                    if (subTitle.getCode().equals(mIsoCode)) {

                        if (!subTitle.getValue().isEmpty()) {
                            holder.sliderHalfCatFourName.setVisibility(View.VISIBLE);
                            holder.sliderHalfCatFourName.setText(subTitle.getValue());
                        }else {
                            holder.sliderHalfCatFourName.setVisibility(View.GONE);
                        }
                    }
                }
                customTextSettings(holder.sliderHalfCatFourName, mTitle, mTitle.getBackgroundColor());
                if (mTitle.getTitle().isEmpty()) {
                    holder.sliderHalfCatFourImage.getLayoutParams().height = ViewGroup.LayoutParams.MATCH_PARENT;
                }
                if (!imageList.getMediumImageUrl().isEmpty()) {
                    Picasso.with(mContext).load(imageList.getMediumImageUrl()).placeholder(R.drawable.place_holder).fit().into(holder.sliderHalfCatFourImage);
                    if (!mBorderModel.getColor().isEmpty()) {
                        setImageBorder(holder.sliderHalfCatFourLayout, mBorderModel);
                    }
                } else {
                    holder.sliderHalfCatFourImage.setVisibility(View.GONE);
                }

                holder.sliderHalfCatFourImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        setRedirectLink(mLink);
                    }
                });
        }
    }

    /*Custom Text Settings*/
    private void customTextSettings(TextView textView, Title mTitle, String bgColor) {
        String[] txtSize = mTitle.getTextsize().split("px");
        if (!mTitle.getTextsize().isEmpty())
            textView.setTextSize(Float.parseFloat(txtSize[0]));
        if (!mTitle.getTextcolor().isEmpty()) {
            textView.setTextColor(Color.parseColor(mTitle.getTextcolor()));
        }else {
            textView.setTextColor(Color.GRAY);
        }
        if (!bgColor.isEmpty())
            textView.setBackgroundColor(Color.parseColor(bgColor));
        if (mTitle.getIsItalic())
            textView.setTypeface(null, Typeface.ITALIC);
        if (mTitle.getIsBold())
            textView.setTypeface(null, Typeface.BOLD);
        if (mTitle.getIsUnderline())
            textView.setPaintFlags(textView.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        if (!mTitle.getTexttype().isEmpty())
            setCustomFont(textView, mTitle);
        if (mTitle.getTextalign().contains("center")) {
            textView.setGravity(Gravity.CENTER);
        } else if (mTitle.getTextalign().contains("justify")) {
            textView.setGravity(Gravity.CENTER);
        }else if (mTitle.getTextalign().contains("right")) {
            textView.setGravity(Gravity.RIGHT);
        } else {
            textView.setGravity(Gravity.LEFT);
        }
    }

    /**
     * The type Web banner holder.
     */
    public class WebBannerHolder extends RecyclerViewHolders {

        public TextView staticFullBannerName, staticHalfBannerName, sliderFullBannerName, sliderHalfBannerName, staticSquareBannerName;
        public TextView threeFourSliderFullName, threeFourSliderHalfName, staticFullCatThreeName, staticHalfCatThreeName, sliderFullCatThreeName;
        public TextView sliderHalfCatThreeName, staticFullCatFourName, staticHalfCatFourName, sliderFullCatFourName, sliderHalfCatFourName;
        public ImageView staticFullBannerImage, staticHalfBannerImage, sliderFullBannerImage, sliderHalfBannerImage, staticSquareBannerImage;
        public ImageView threeFourSliderFullImage, threeFourSliderHalfImage, staticFullCatThreeImage, staticHalfCatThreeImage, sliderFullCatThreeImage;
        public ImageView sliderHalfCatThreeImage, staticFullCatFourImage, staticHalfCatFourImage, sliderFullCatFourImage, sliderHalfCatFourImage;
        public RelativeLayout staticFullBannerLayout, staticHalfBannerLayout, sliderFullBannerLayout, sliderHalfBannerLayout, staticSquareBannerLayout;
        public RelativeLayout threeFourSliderFullLayout, threeFourSliderHalfLayout, staticFullCatThreeLayout, staticHalfCatThreeLayout, sliderFullCatThreeLayout;
        public RelativeLayout sliderHalfCatThreeLayout, staticFullCatFourLayout, staticHalfCatFourLayout, sliderFullCatFourLayout, sliderHalfCatFourLayout;

        /**
         * Instantiates a new Web banner holder.
         *
         * @param v the v
         */
        public WebBannerHolder(View v) {
            super(v);

            staticFullBannerImage = (ImageView) v.findViewById(R.id.static_full_banner_image);
            staticFullBannerName = (TextView) v.findViewById(R.id.static_full_banner_name);
            staticFullBannerLayout = (RelativeLayout) v.findViewById(R.id.static_full_banner_layout);

            staticHalfBannerImage = (ImageView) v.findViewById(R.id.static_half_banner_image);
            staticHalfBannerName = (TextView) v.findViewById(R.id.static_half_banner_name);
            staticHalfBannerLayout = (RelativeLayout) v.findViewById(R.id.static_half_banner_layout);

            sliderFullBannerImage = (ImageView) v.findViewById(R.id.slider_full_banner_image);
            sliderFullBannerName = (TextView) v.findViewById(R.id.slider_full_banner_name);
            sliderFullBannerLayout = (RelativeLayout) v.findViewById(R.id.slider_full_banner_layout);

            sliderHalfBannerImage = (ImageView) v.findViewById(R.id.slider_half_banner_image);
            sliderHalfBannerName = (TextView) v.findViewById(R.id.slider_half_banner_name);
            sliderHalfBannerLayout = (RelativeLayout) v.findViewById(R.id.slider_half_banner_layout);

            staticSquareBannerImage = (ImageView) v.findViewById(R.id.static_square_banner_image);
            staticSquareBannerName = (TextView) v.findViewById(R.id.static_square_banner_name);
            staticSquareBannerLayout = (RelativeLayout) v.findViewById(R.id.static_square_banner_layout);

            threeFourSliderFullImage = (ImageView) v.findViewById(R.id.three_four_slider_full_banner_image);
            threeFourSliderFullName = (TextView) v.findViewById(R.id.three_four_slider_full_banner_name);
            threeFourSliderFullLayout = (RelativeLayout) v.findViewById(R.id.three_four_slider_full_banner_layout);

            threeFourSliderHalfImage = (ImageView) v.findViewById(R.id.three_four_slider_half_banner_image);
            threeFourSliderHalfName = (TextView) v.findViewById(R.id.three_four_slider_half_banner_name);
            threeFourSliderHalfLayout = (RelativeLayout) v.findViewById(R.id.three_four_slider_half_banner_layout);

            staticFullCatThreeImage = (ImageView) v.findViewById(R.id.static_full_category_image_three);
            staticFullCatThreeName = (TextView) v.findViewById(R.id.static_full_category_name_three);
            staticFullCatThreeLayout = (RelativeLayout) v.findViewById(R.id.static_full_category_three_layout);

            staticHalfCatThreeImage = (ImageView) v.findViewById(R.id.static_half_category_image_three);
            staticHalfCatThreeName = (TextView) v.findViewById(R.id.static_half_category_name_three);
            staticHalfCatThreeLayout = (RelativeLayout) v.findViewById(R.id.static_half_category_three_layout);

            sliderFullCatThreeImage = (ImageView) v.findViewById(R.id.slider_full_category_image_three);
            sliderFullCatThreeName = (TextView) v.findViewById(R.id.slider_full_category_name_three);
            sliderFullCatThreeLayout = (RelativeLayout) v.findViewById(R.id.slider_full_category_three_layout);

            sliderHalfCatThreeImage = (ImageView) v.findViewById(R.id.slider_half_category_image_three);
            sliderHalfCatThreeName = (TextView) v.findViewById(R.id.slider_half_category_name_three);
            sliderHalfCatThreeLayout = (RelativeLayout) v.findViewById(R.id.slider_half_category_three_layout);

            staticFullCatFourImage = (ImageView) v.findViewById(R.id.static_full_category_image_four);
            staticFullCatFourName = (TextView) v.findViewById(R.id.static_full_category_name_four);
            staticFullCatFourLayout = (RelativeLayout) v.findViewById(R.id.static_full_category_four_layout);

            staticHalfCatFourImage = (ImageView) v.findViewById(R.id.static_half_category_image_four);
            staticHalfCatFourName = (TextView) v.findViewById(R.id.static_half_category_name_four);
            staticHalfCatFourLayout = (RelativeLayout) v.findViewById(R.id.static_half_category_four_layout);

            sliderFullCatFourImage = (ImageView) v.findViewById(R.id.slider_full_category_image_four);
            sliderFullCatFourName = (TextView) v.findViewById(R.id.slider_full_category_name_four);
            sliderFullCatFourLayout = (RelativeLayout) v.findViewById(R.id.slider_full_category_four_layout);

            sliderHalfCatFourImage = (ImageView) v.findViewById(R.id.slider_half_category_image_four);
            sliderHalfCatFourName = (TextView) v.findViewById(R.id.slider_half_category_name_four);
            sliderHalfCatFourLayout = (RelativeLayout) v.findViewById(R.id.slider_half_category_four_layout);
        }
    }

    /*Set Category Adjust Width For banner*/
    private void setCategoryAdjustWidth(View view, String categoryType) {
        int width = 0;
        WindowManager windowManager = (WindowManager)mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = windowManager.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        if (categoryType.equals("three")) {
            if(Build.VERSION.SDK_INT> Build.VERSION_CODES.HONEYCOMB)
                width = size.x / 3;
        }else if (categoryType.equals("four")){
            if(Build.VERSION.SDK_INT> Build.VERSION_CODES.HONEYCOMB)
                width = size.x / 4;
        }else if(categoryType.equals("square")){
            if(Build.VERSION.SDK_INT> Build.VERSION_CODES.HONEYCOMB)
                width = size.x / 2;
        }else {
            if(Build.VERSION.SDK_INT> Build.VERSION_CODES.HONEYCOMB)
                width = size.x;
        }
        view.setLayoutParams(new RecyclerView.LayoutParams(width, RecyclerView.LayoutParams.MATCH_PARENT));
    }

    /*Text Custom Font setup*/
    private void setCustomFont(TextView v, Title mTitle){

        String textType = mTitle.getTexttype();
        boolean isItalic = mTitle.getIsItalic();
        boolean isBold = mTitle.getIsBold();

        if (textType.contains("roboto") && isBold && !isItalic)
            v.setTypeface(mMainActivity.robotoBold);
        else if (textType.contains("roboto") && isItalic && !isBold)
            v.setTypeface(mMainActivity.robotoItalic);
        else if (textType.contains("roboto") && !isItalic && !isBold)
            v.setTypeface(mMainActivity.robotoRegular);
        else if (textType.contains("oxygen") && isBold && !isItalic)
            v.setTypeface(mMainActivity.oxygenBold);
        else if (textType.contains("oxygen") && isItalic && !isBold)
            v.setTypeface(mMainActivity.oxygenItalic);
        else if (textType.contains("oxygen") && !isItalic && !isBold)
            v.setTypeface(mMainActivity.oxygenRegular);
        else if (textType.contains("lato") && isBold && !isItalic)
            v.setTypeface(mMainActivity.latoBold);
        else if (textType.contains("lato") && isItalic && !isBold)
            v.setTypeface(mMainActivity.latoItalic);
        else if (textType.contains("lato") && !isItalic && !isBold)
            v.setTypeface(mMainActivity.latoRegular);
        else if (textType.contains("open-sans") && isBold && !isItalic)
            v.setTypeface(mMainActivity.openSansBold);
        else if (textType.contains("open-sans") && isItalic && !isBold)
            v.setTypeface(mMainActivity.openSansItalic);
        else if (textType.contains("open-sans") && !isItalic && !isBold)
            v.setTypeface(mMainActivity.opensansRegular);
        else if (textType.contains("raleway") && isBold && !isItalic)
            v.setTypeface(mMainActivity.ralewayBold);
        else if (textType.contains("raleway") && isItalic && !isBold)
            v.setTypeface(mMainActivity.ralewayItalic);
        else if (textType.contains("raleway") && !isItalic && !isBold)
            v.setTypeface(mMainActivity.ralewayRegular);
    }

    /*Image ImageBorder*/
    private void setImageBorder(View v, ImageBorder mBorder){
        int borderWidth;
        if (!mBorder.getWidth().isEmpty()){
            String[] widthString = mBorder.getWidth().split("px");
            float pixels = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, Float.parseFloat(widthString[0]), mContext.getResources().getDisplayMetrics());
            borderWidth = (int) pixels;
        }else {
            borderWidth = 2;
        }
        String styleType = mBorder.getStyleType();
        if (styleType.equals("right-left") || styleType.equals("left-right"))
            v.setPadding(borderWidth, 0, borderWidth, 0);
        else if (styleType.equals("right"))
            v.setPadding(0, 0, borderWidth, 0);
        else if (styleType.equals("left"))
            v.setPadding(borderWidth, 0, 0, 0);
        else if (styleType.equals("top-bottom") || styleType.equals("bottom-top"))
            v.setPadding(0, borderWidth, 0, borderWidth);
        else if (styleType.equals("bottom"))
            v.setPadding(0, 0, 0, borderWidth);
        else if (styleType.equals("top"))
            v.setPadding(0, borderWidth, 0, 0);
        else if (styleType.equals("left-bottom") || styleType.equals("bottom-left"))
            v.setPadding(borderWidth, 0, 0, borderWidth);
        else if (styleType.equals("right-bottom") || styleType.equals("bottom-right"))
            v.setPadding(0, 0, borderWidth, borderWidth);
        else if (styleType.equals("left-top") || styleType.equals("top-left"))
            v.setPadding(borderWidth, borderWidth, 0, 0);
        else if (styleType.equals("right-top") || styleType.equals("top-right"))
            v.setPadding(0, borderWidth, borderWidth, 0);
        else
            v.setPadding(borderWidth, borderWidth, borderWidth, borderWidth);


        GradientDrawable gd = new GradientDrawable();
        gd.setGradientType(GradientDrawable.RECTANGLE);

        if (!mBorder.getType().isEmpty() && !mBorder.getWidth().isEmpty()) {
            if (mBorder.getType().equals("dashed")) {
                gd.setStroke(borderWidth, Color.parseColor(mBorder.getColor()), 15, 10);
            } else if (mBorder.getType().equals("dotted")) {
                gd.setStroke(borderWidth, Color.parseColor(mBorder.getColor()), 4, 4);
            } else if (mBorder.getType().equals("solid")){
                gd.setStroke(borderWidth, Color.parseColor(mBorder.getColor()));
            } else {
                gd.setStroke(borderWidth, Color.parseColor(mBorder.getColor()));
            }
        }else {
            gd.setStroke(borderWidth, Color.parseColor(mBorder.getColor()));
        }

        v.setBackground(gd);
    }

    /*Link Re-Direction*/
    private void setRedirectLink(Link link){
        if (link.getHasLink()) {
            if (!link.getLinkType().isEmpty()) {
                if (link.getLinkType().equals("product")) {
                    if (AppConstants.sPrestashopMagento.equals(AppConstants.sMagento)) {
                        Intent in = new Intent(mContext, MagentoProductDetailActivity.class);
                        in.putExtra("product_id", link.getProduct().getIdProduct() + "");
                        in.putExtra("product_name", link.getProduct().getName());
                        mContext.startActivity(in);
                    }else {
                        Intent in = new Intent(mContext, ProductDetailActivity.class);
                        in.putExtra("product_id", link.getProduct().getIdProduct() + "");
                        in.putExtra("product_name", link.getProduct().getName());
                        mContext.startActivity(in);
                    }
                } else if (link.getLinkType().equals("category")) {
                if (link.getCategory().getCategoryCount() == 0) {
                    Intent intent = new Intent(mContext, ProductListActivity.class);
                    intent.putExtra("sCategoriesId", link.getCategory().getCategoryId());
                    intent.putExtra("categoryName", link.getCategory().getCategoryName());
                    intent.putExtra("productCount", link.getCategory().getProductCount()+"");
                    mContext.startActivity(intent);
                } else {
                    Intent intent = new Intent(mContext, SubCategoryActivity.class);
                    intent.putExtra("sCategoriesId", link.getCategory().getCategoryId());
                    intent.putExtra("categoryName", link.getCategory().getCategoryName());
                    mContext.startActivity(intent);
                    }
                }
            }
        }else {
            if (!link.getKeyword().isEmpty()) {
                Intent intent = new Intent(mContext, SearchActivity.class);
                intent.putExtra("search_text", link.getKeyword());
                mContext.startActivity(intent);
            }
        }
    }

}
