package com.prestashopemc.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.prestashopemc.R;
import com.prestashopemc.customView.CustomTextView;
import com.prestashopemc.model.FilterCategoryResult;
import com.prestashopemc.view.FilterActivity;
import com.prestashopemc.view.SearchActivity;
import com.prestashopemc.view.SearchFilterActivity;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Search Filter Category Adapter Contains Search Filter Category Holder</h1>
 * The Search Filter Category Adapter implements an adapter view for Search Filter Category.
 * <p>
 * <b>Note:</b> Adapter values are get from Filter Category Result model.
 *
 * @author dinakaran
 * @version 1.2
 * @since 3 /8/16
 */
public class SearchFilterCategoryAdapter extends RecyclerView.Adapter<SearchFilterCategoryAdapter.CategoryHolder> {
    private Context mContext;
    private List<FilterCategoryResult> mFilterCategoryResults;
    private FilterCategoryResult mFilterCategoryResult;
    private SearchFilterActivity mSearchFilterActivity;
    /**
     * The constant mSelectedFilter.
     */
    public static ArrayList<String> mSelectedFilter = new ArrayList<>();
    /**
     * The constant mCategoryId.
     */
    public static String mCategoryId;

    /**
     * Instantiates a new Search filter category adapter.
     *
     * @param context               the context
     * @param filterCategoryResults the filter category results
     */
    public SearchFilterCategoryAdapter(Context context, List<FilterCategoryResult> filterCategoryResults) {
        this.mContext = context;
        this.mFilterCategoryResults = filterCategoryResults;
    }

    /**
     * @param parent the parent
     * @param viewType the view type
     * @return
     */
    @Override
    public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        mSearchFilterActivity = (SearchFilterActivity) mContext;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_parent, null);
        CategoryHolder viewHolder = new CategoryHolder(view);
        return viewHolder;
    }

    /**
     * @param holder the holder
     * @param position the position
     */
    @Override
    public void onBindViewHolder(final CategoryHolder holder, final int position) {
        mFilterCategoryResult = mFilterCategoryResults.get(position);
        holder.mFilterName.setText(mFilterCategoryResult.getName());
        holder.mFilterName.setTextColor(mContext.getResources().getColor(R.color.category_name_clocr));
        holder.mFilterName.setTypeface(mSearchFilterActivity.robotoLight);
       /* if (mSelectedFilter.size() != 0) {
            if (mSelectedFilter.contains(mFilterCategoryResult.getIdCategory())) {
                holder.mCheckBox.setChecked(true);
            }
        }
        holder.mCheckBox.setVisibility(View.GONE);
        holder.mCheckBox.setTag(position);
        holder.mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                mFilterCategoryResult = filterCategoryResults.get(position);
                boolean boxCheck = holder.mCheckBox.isChecked();
                if (boxCheck) {
                    mSelectedFilter.add(mFilterCategoryResult.getIdCategory());
                } else {
                    mSelectedFilter.remove(mFilterCategoryResult.getIdCategory());
                }
            }
        });*/
        holder.mLayout.setBackgroundColor(mContext.getResources().getColor(R.color.category_layout_clocr));
        holder.mLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFilterCategoryResult = mFilterCategoryResults.get(position);
                mCategoryId = mFilterCategoryResult.getIdCategory();
                Intent filterIntent = new Intent(mContext, FilterActivity.class);
                filterIntent.putExtra("category_id", mFilterCategoryResult.getIdCategory());
                filterIntent.putExtra("category_name", mFilterCategoryResult.getName());
                filterIntent.putExtra("filter", "searchList");
                SearchActivity.sCategoryId = mFilterCategoryResult.getIdCategory();
                SearchActivity.sCategoryName = mFilterCategoryResult.getName();
                SearchActivity.sFilterName = "searchList";
                mContext.startActivity(filterIntent);
                mSearchFilterActivity.finish();
            }
        });
    }

    /**
     * The type Category holder.
     */
/*Holder*/
    public class CategoryHolder extends RecyclerViewHolders {

        public CustomTextView mFilterName;
        public RelativeLayout mLayout;

        /**
         * Instantiates a new Category holder.
         *
         * @param v the v
         */
        public CategoryHolder(View v) {
            super(v);
            mFilterName = (CustomTextView) itemView.findViewById(R.id.category_header_name);
            mLayout = (RelativeLayout) itemView.findViewById(R.id.cms_layout);
        }
    }

    /**
     * @return int
     */
    @Override
    public int getItemCount() {
        return this.mFilterCategoryResults.size();
    }
}
