package com.prestashopemc.customView;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.prestashopemc.R;

/**
 * <h1>Custom Button!</h1>
 * The Custom Button is used to customized the button globally.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class CustomButton extends Button {

    /**
     * Instantiates a new Custom button.
     *
     * @param context  the context
     * @param attrs    the attrs
     * @param defStyle the def style
     */
    public CustomButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }

    /**
     * Instantiates a new Custom button.
     *
     * @param context the context
     * @param attrs   the attrs
     */
    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);

    }

    /**
     * Instantiates a new Custom button.
     *
     * @param context the context
     */
    public CustomButton(Context context) {
        super(context);
        init(null);
    }

    private void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.customTextview);
            String fontName = a.getString(R.styleable.customTextview_fontName);
            Typeface myTypeface = null;
            if (fontName != null) {
                myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + fontName);

            } else {
                myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + "robotoregular.ttf");
            }
            setTypeface(myTypeface);
            a.recycle();
        }
    }
}
