package com.prestashopemc.util;

import android.app.Application;
import android.content.Context;
import android.os.StrictMode;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.crashlytics.android.Crashlytics;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.paypal.android.sdk.cw;
import com.prestashopemc.model.EditProfile;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.Socket;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;

import io.fabric.sdk.android.Fabric;

/**
 * <h1>My Application!</h1>
 * The My Application implements the network call functionality for the whole projects.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class MyApplication extends Application {
    private static Context sContext;
    private static RequestQueue sRequestQueue, sRequestQueueSSlV3Retrify;
    private static GsonBuilder sGsonBuilder;
    private static Gson sGson;
    private static String mLangId = "1", mCurrencyId = "1";
    private HurlStack mStack;
    private static GoogleAnalytics sAnalytics;
    private static Tracker sTracker;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        sContext = this;
        sRequestQueue = Volley.newRequestQueue(getContext());
        sAnalytics = GoogleAnalytics.getInstance(MyApplication.this);
        GoogleAnalytics.getInstance(MyApplication.this).setLocalDispatchPeriod(30);

        SSLSocketFactoryExtended factory = null;
        try {
            factory = new SSLSocketFactoryExtended();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
        final SSLSocketFactoryExtended finalFactory = factory;
        mStack = new HurlStack() {
            @Override
            protected HttpURLConnection createConnection(URL url) throws IOException {
                HttpsURLConnection httpsURLConnection = (HttpsURLConnection) super.createConnection(url);
                try {
                    httpsURLConnection.setSSLSocketFactory(finalFactory);
                    httpsURLConnection.setRequestProperty("charset", "utf-8");

                } catch (Exception e) {
                    e.printStackTrace();
                }
                return httpsURLConnection;
            }

        };
        sRequestQueueSSlV3Retrify = Volley.newRequestQueue(MyApplication.getContext(), mStack, -1);


        sGsonBuilder = new GsonBuilder();
        sGson = sGsonBuilder.create();
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy =
                    new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        SharedPreference mPref = SharedPreference.getInstance();

        mPref.save("barcode_data", "0");
        getGsonInstance();
        getRequestQueue();
        getRequestQueueSSlV3Retrify();
    }

    /**
     * Gets context.
     *
     * @return the context
     */
    public static Context getContext() {
        return sContext;
    }

    /**
     * Network call.
     *
     * @param url      the url
     * @param params   the params
     * @param callBack the call back
     */

    public static void NetworkCall(String url, Map<String, String> params, final VolleyCallback callBack) {

        if (!SharedPreference.getInstance().getValue("id_language").equals("0")) {
            mLangId = SharedPreference.getInstance().getValue("id_language");
        }
        if (!SharedPreference.getInstance().getValue("id_currency").equals("0")) {
            mCurrencyId = SharedPreference.getInstance().getValue("id_currency");
        }
        params.put("securitykey", SharedPreference.getInstance().getValue("security_key"));
        params.put("id_currency", mCurrencyId);
        params.put("id_lang", mLangId);

        String api_url = url;

        for (Map.Entry<String, String> entry : params.entrySet()) {
            api_url = api_url + "&" + entry.getKey() + "=" + entry.getValue();
        }

        Log.e("Request url++++", api_url);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, api_url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Volley Response++++", response);
                        callBack.Success(response);

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        String statusCode = error.getClass().getSimpleName();
                        Log.e("Volley Error Response", statusCode);
                        callBack.Failure(statusCode);

                    }
                });
        getRequestQueue().add(stringRequest);
        stringRequest.setShouldCache(true);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }


    /**
     * Network call params.
     *
     * @param requestMethod the request method
     * @param url           the url
     * @param headers       the headers
     * @param callBack      the call back
     */
    public static void NetworkCallParams(int requestMethod, String url, final HashMap<String, String> headers, final VolleyCallback callBack) {
        StringRequest stringRequest = new StringRequest(requestMethod, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.e("Volley Response++++", response.toString());
                        callBack.Success(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("VolleyError++++", error.toString());
                        callBack.Failure(error.toString());
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                return headers;
            }
        };
        getRequestQueueSSlV3Retrify().add(stringRequest);
        stringRequest.setShouldCache(true);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                900000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }


    /**
     * Gets gson instance.
     *
     * @return the gson instance
     */
    public static Gson getGsonInstance() {
        return sGson;
    }

    /**
     * Gets request queue.
     *
     * @return the request queue
     */
    public static RequestQueue getRequestQueue() {
        return sRequestQueue;
    }

    /**
     * Gets request queue s sl v 3 retrify.
     *
     * @return the request queue s sl v 3 retrify
     */
    public static RequestQueue getRequestQueueSSlV3Retrify() {
        return sRequestQueueSSlV3Retrify;
    }


    /**
     * Image upload network call.
     *
     * @param url       the url
     * @param callBack  the call back
     * @param params    the params
     * @param imageFile the image file
     */

    public static void imageUploadNetworkCall(String url, final VolleyCallback callBack,
                                              Map<String, String> params, File imageFile) {

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy =
                    new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (!SharedPreference.getInstance().getValue("id_language").equals("0")) {
            mLangId = SharedPreference.getInstance().getValue("id_language");
        }
        if (!SharedPreference.getInstance().getValue("id_currency").equals("0")) {
            mCurrencyId = SharedPreference.getInstance().getValue("id_currency");
        }
        params.put("securitykey", SharedPreference.getInstance().getValue("security_key"));
        params.put("id_currency", mCurrencyId);
        params.put("id_lang", mLangId);

        String api_url = url;

        for (Map.Entry<String, String> entry : params.entrySet()) {
            api_url = api_url + "&" + entry.getKey() + "=" + entry.getValue();
        }

        Log.e("Request url++++", api_url.toString().trim());

        /*
        * For handle http and https services */

        /*try {
            HttpRequest req=new HttpRequest(api_url);
            req.withHeaders("Content-Type: application/json");
            req.prepare(HttpRequest.Method.POST);
            String charset = "UTF-8";
            String requestURL = api_url;
            MultipartUtility multipart = new MultipartUtility(requestURL, charset, imageFile);
            String response = multipart.finish(); // response from server.
            int status = multipart.getStatus();

            if(status == 200)
            {
                callBack.Success(response);
            }
            else
            {
                response = "Error occurred! Http Status Code: "
                        + status;
                Log.e("Failure Response", response);
                callBack.Failure(response);

            }

            Log.e("response", response);
        } catch (IOException e) {
            e.printStackTrace();
        }*/

        try {
            HttpPost httppost = new HttpPost(api_url);

            if (imageFile != null) {
                MultipartEntityBuilder builder = MultipartEntityBuilder.create();
                FileBody fileBody = new FileBody(imageFile);
                builder.addPart("image", fileBody);
                HttpEntity entity = builder.build();
                httppost.setEntity(entity);
            }
            // Making server call
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity responseEntity = response.getEntity();
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // Server response
                try {
                    String responseString = EntityUtils.toString(responseEntity);

                    Log.e("Success Response", responseString);
                    if (responseString != null) {
                        callBack.Success(responseString);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                String responseString = "Error occurred! Http Status Code: "
                        + statusCode;
                Log.e("Failure Response", responseString);
                callBack.Failure(responseString);

            }

        } catch (ClientProtocolException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();

        }


    }


    public static void imageMagentoNetworkCall(String url, final VolleyCallback callBack,
                                               Map<String, String> params, File imageFile, String SocialName, Boolean isEdit) {

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy =
                    new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        if (!SharedPreference.getInstance().getValue("id_language").equals("0")) {
            mLangId = SharedPreference.getInstance().getValue("id_language");
        }
        if (!SharedPreference.getInstance().getValue("currency_iso_code").equals("0")) {
            mCurrencyId = SharedPreference.getInstance().getValue("currency_iso_code");
        }else {
            mCurrencyId = "USD";
        }
        Log.e("Request url++++", url + params.get("customerData").toString().trim());
        try {
            HttpPost httppost = new HttpPost(url);
            if (imageFile != null) {
                MultipartEntityBuilder builder = MultipartEntityBuilder.create();
                FileBody fileBody = new FileBody(imageFile);
                builder.addPart("image", fileBody);

                builder.addTextBody("securitykey", SharedPreference.getInstance().getValue("security_key"));

                builder.addTextBody("customerData", params.get("customerData"));
                if (!SocialName.isEmpty()) {
                    builder.addTextBody("login_type", SocialName);
                }
                builder.addTextBody("cart_id", SharedPreference.getInstance().getValue("cart_id"));
                builder.addTextBody("store_id",mLangId);

                if (isEdit) {
                    builder.addTextBody("id_customer", params.get("id_customer"));
                }else {
                    builder.addTextBody("website_id", "1");
                }

                HttpEntity entity = builder.build();
                httppost.setEntity(entity);
            } else {
                MultipartEntityBuilder builder = MultipartEntityBuilder.create();
                builder.addTextBody("securitykey", SharedPreference.getInstance().getValue("security_key"));
                builder.addTextBody("customerData", params.get("customerData"));
                if (!SocialName.isEmpty()) {
                    builder.addTextBody("login_type", SocialName);
                }
                builder.addTextBody("cart_id", SharedPreference.getInstance().getValue("cart_id"));
                builder.addTextBody("store_id", mLangId);
                if (isEdit) {
                    builder.addTextBody("id_customer", params.get("id_customer"));
                }else {
                    builder.addTextBody("website_id", "1");
                }
                HttpEntity entity = builder.build();
                httppost.setEntity(entity);

            }

            // Making server call
            HttpClient httpclient = new DefaultHttpClient();
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity responseEntity = response.getEntity();
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode == 200) {
                // Server response
                try {
                    String responseString = EntityUtils.toString(responseEntity);
                    Log.e("Success Response", responseString);
                    if (responseString != null) {
                        callBack.Success(responseString);
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                String responseString = "Error occurred! Http Status Code: "
                        + statusCode;
                Log.e("Failure Response", responseString);
                callBack.Failure(responseString);

            }

        } catch (ClientProtocolException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();

        }


    }


    /**
     * Magento Api Network call.
     *
     * @param url      the url
     * @param params   the params
     * @param callBack the call back
     */
    public static void magentoNetworkCall(String url, final Map<String, String> params, final VolleyCallback callBack) {
        if (!SharedPreference.getInstance().getValue("id_language").equals("0")) {
            mLangId = SharedPreference.getInstance().getValue("id_language");
        }
        if (!SharedPreference.getInstance().getValue("currency_iso_code").equals("0")) {
            mCurrencyId = SharedPreference.getInstance().getValue("currency_iso_code");
        }else {
            mCurrencyId = "USD";
        }
        params.put("securitykey", SharedPreference.getInstance().getValue("security_key"));
        params.put("id_currency", mCurrencyId);
        params.put("store_id", mLangId);//id_lang
        params.put("website_id", "1");

        Log.e("Request magento url++++", url + params.toString());

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.e("Volley Response++++", response.toString());
                callBack.Success(response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Error Response++++", error.toString());
                callBack.Failure(error.toString());
            }
        })

        {
            @Override
            public byte[] getBody() {

                if (params != null && params.size() > 0) {
                    return encodeParameters(params, getParamsEncoding());
                }
                return null;
            }

            @Override
            public String getBodyContentType() {
                return "application/x-www-form-urlencoded; charset=UTF-8";
            }
        };

        getRequestQueue().add(jsonObjectRequest);
        jsonObjectRequest.setShouldCache(true);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                80000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
    }

    /**
     * convert parameters as encoded form
     */
    private static byte[] encodeParameters(Map<String, String> params, String paramsEncoding) {
        StringBuilder encodedParams = new StringBuilder();
        try {
            for (Map.Entry<String, String> entry : params.entrySet()) {
                try {
                    encodedParams.append(URLEncoder.encode(entry.getKey(), paramsEncoding));
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                encodedParams.append('=');
                encodedParams.append(URLEncoder.encode(entry.getValue(), paramsEncoding));
                encodedParams.append('&');
            }
            return encodedParams.toString().getBytes(paramsEncoding);
        } catch (UnsupportedEncodingException uee) {
            throw new RuntimeException("Encoding not supported: " + paramsEncoding, uee);
        }
    }

    /**
     * The type Ssl socket factory extended.
     */
    public class SSLSocketFactoryExtended extends SSLSocketFactory {
        private SSLContext mSSLContext;
        private String[] mCiphers;
        private String[] mProtocols;


        /**
         * Instantiates a new Ssl socket factory extended.
         *
         * @throws NoSuchAlgorithmException the no such algorithm exception
         * @throws KeyManagementException   the key management exception
         */
        public SSLSocketFactoryExtended() throws NoSuchAlgorithmException, KeyManagementException {
            initSSLSocketFactoryEx(null, null, null);
        }

        /**
         * @return chiphers
         */
        public String[] getDefaultCipherSuites() {
            return mCiphers;
        }

        /**
         * @return chiphers
         */
        public String[] getSupportedCipherSuites() {
            return mCiphers;
        }

        /**
         * @param s
         * @param host
         * @param port
         * @param autoClose
         * @return
         * @throws IOException
         */
        public Socket createSocket(Socket s, String host, int port, boolean autoClose) throws IOException {
            SSLSocketFactory factory = mSSLContext.getSocketFactory();
            SSLSocket ss = (SSLSocket) factory.createSocket(s, host, port, autoClose);

            ss.setEnabledProtocols(mProtocols);
            ss.setEnabledCipherSuites(mCiphers);

            return ss;
        }

        /**
         * @param address
         * @param port
         * @param localAddress
         * @param localPort
         * @return
         * @throws IOException
         */
        public Socket createSocket(InetAddress address, int port, InetAddress localAddress, int localPort) throws IOException {
            SSLSocketFactory factory = mSSLContext.getSocketFactory();
            SSLSocket ss = (SSLSocket) factory.createSocket(address, port, localAddress, localPort);

            ss.setEnabledProtocols(mProtocols);
            ss.setEnabledCipherSuites(mCiphers);

            return ss;
        }

        /**
         * @param host
         * @param port
         * @param localHost
         * @param localPort
         * @return
         * @throws IOException
         */
        public Socket createSocket(String host, int port, InetAddress localHost, int localPort) throws IOException {
            SSLSocketFactory factory = mSSLContext.getSocketFactory();
            SSLSocket ss = (SSLSocket) factory.createSocket(host, port, localHost, localPort);

            ss.setEnabledProtocols(mProtocols);
            ss.setEnabledCipherSuites(mCiphers);

            return ss;
        }

        /**
         * @param host
         * @param port
         * @return
         * @throws IOException
         */
        public Socket createSocket(InetAddress host, int port) throws IOException {
            SSLSocketFactory factory = mSSLContext.getSocketFactory();
            SSLSocket ss = (SSLSocket) factory.createSocket(host, port);

            ss.setEnabledProtocols(mProtocols);
            ss.setEnabledCipherSuites(mCiphers);

            return ss;
        }

        /**
         * @param host
         * @param port
         * @return
         * @throws IOException
         */
        public Socket createSocket(String host, int port) throws IOException {
            SSLSocketFactory factory = mSSLContext.getSocketFactory();
            SSLSocket ss = (SSLSocket) factory.createSocket(host, port);

            ss.setEnabledProtocols(mProtocols);
            ss.setEnabledCipherSuites(mCiphers);

            return ss;
        }

        /**
         * @param km
         * @param tm
         * @param random
         * @throws NoSuchAlgorithmException
         * @throws KeyManagementException
         */
        private void initSSLSocketFactoryEx(KeyManager[] km, TrustManager[] tm, SecureRandom random)
                throws NoSuchAlgorithmException, KeyManagementException {
            mSSLContext = SSLContext.getInstance("TLS");
            mSSLContext.init(km, tm, random);

            mProtocols = GetProtocolList();
            mCiphers = GetCipherList();
        }

        /**
         * Get protocol list string [ ].
         *
         * @return the string [ ]
         */
        protected String[] GetProtocolList() {
            String[] protocols = {"TLSv1", "TLSv1.1", "TLSv1.2", "TLSv1.3"};
            String[] availableProtocols = null;

            SSLSocket socket = null;

            try {
                SSLSocketFactory factory = mSSLContext.getSocketFactory();
                socket = (SSLSocket) factory.createSocket();

                availableProtocols = socket.getSupportedProtocols();
            } catch (Exception e) {
                return new String[]{"TLSv1"};
            } finally {
                if (socket != null)
                    try {
                        socket.close();
                    } catch (IOException e) {
                    }
            }

            List<String> resultList = new ArrayList<String>();
            for (int i = 0; i < protocols.length; i++) {
                int idx = Arrays.binarySearch(availableProtocols, protocols[i]);
                if (idx >= 0)
                    resultList.add(protocols[i]);
            }

            return resultList.toArray(new String[0]);
        }

        /**
         * Get cipher list string [ ].
         *
         * @return the string [ ]
         */
        protected String[] GetCipherList() {
            List<String> resultList = new ArrayList<String>();
            SSLSocketFactory factory = mSSLContext.getSocketFactory();
            for (String s : factory.getSupportedCipherSuites()) {
                Log.e("CipherSuite type = ", s);
                resultList.add(s);
            }
            return resultList.toArray(new String[resultList.size()]);
        }

    }

    /**
     * Gets default tracker.
     *
     * @param s the s
     */
    synchronized static public void getDefaultTracker(String s) {
        // To enable debug logging use: adb shell setprop log.tag.GAv4 DEBUG
        if (sTracker == null) {
            //sTracker = sAnalytics.newTracker(R.xml.app_tracker);
            //sTracker = sAnalytics.newTracker("UA-92183710-1");
            sTracker = sAnalytics.newTracker(SharedPreference.getInstance().getValue("google_tracking"));
            sAnalytics.setLocalDispatchPeriod(30);
            sTracker.setScreenName(s);
            sTracker.send(new HitBuilders.ScreenViewBuilder().build());
            sTracker.enableAutoActivityTracking(true);
        } else {
            sTracker.setScreenName(s);
            sAnalytics.setLocalDispatchPeriod(30);
            sTracker.send(new HitBuilders.ScreenViewBuilder().build());
            sTracker.enableAutoActivityTracking(true);
        }
    }

    /**
     * Event tracking.
     *
     * @param category the category
     * @param action   the action
     * @param label    the label
     */
    public static void eventTracking(String category, String action, String label) {
        if (sTracker == null) {
            //sTracker = sAnalytics.newTracker(R.xml.app_tracker);
            //sTracker = sAnalytics.newTracker("UA-92183710-1");
            sTracker = sAnalytics.newTracker(SharedPreference.getInstance().getValue("google_tracking"));
            sTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(category)
                    .setAction(action)
                    .setLabel(label)
                    .build());
        } else {
            sTracker.send(new HitBuilders.EventBuilder()
                    .setCategory(category)
                    .setAction(action)
                    .setLabel(label)
                    .build());
        }
    }


}
