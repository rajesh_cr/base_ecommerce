package com.prestashopemc.util;

import com.prestashopemc.model.Product;

import java.util.List;

/**
 * <h1>Fragment Interface!</h1>
 * TheFragment Interface is used to update the view of fragment.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public interface FragmentInterface {

    /**
     * Update view.
     *
     * @param productList the product list
     */
    public void updateView(List<Product> productList);
}
