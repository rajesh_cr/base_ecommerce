package com.prestashopemc.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * <h1>Internet Check!</h1>
 * The Internet Check is used to check whether the internet connection is available or not.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class InternetCheck {

    /**
     * Get network info network info.
     *
     * @param context the context
     * @return the network info
     */
    public static NetworkInfo getNetworkInfo(Context context){
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo();
    }

    /**
     * Is internet on boolean.
     *
     * @param context the context
     * @return the boolean
     */
    public static boolean isInternetOn(Context context){
        NetworkInfo info = InternetCheck.getNetworkInfo(context);
        return info != null && info.isConnected();
    }
}
