package com.prestashopemc.util;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.prestashopemc.R;
import com.prestashopemc.view.NotificationActivity;

/**
 * <h1>My Android Firebase Msg Service!</h1>
 * The My Android Firebase Msg Service is used to collect the data message from the firebase push notification.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class MyAndroidFirebaseMsgService extends FirebaseMessagingService {
    private static final String TAG = "MyAndroidFCMService";
    private String mNotificationType, mId, mTitle, mMessage, mMessagebody;
    private Notification mNotification;

    /**
     * @param remoteMessage the remote message
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Log data to Log Cat
        //Log.e(TAG, "From: " + remoteMessage.getFrom());
        //Log.e(TAG, "Notification Message Body: " + remoteMessage.getNotification().getBody());
        //create mNotification

        mMessagebody = remoteMessage.getData().toString();
        mNotificationType =remoteMessage.getData().get("notification_type");
        mId =remoteMessage.getData().get("id");
        mTitle =remoteMessage.getData().get("title");
        mMessage =remoteMessage.getData().get("message");
        createNotification(mMessage, mNotificationType, mTitle, mId);
    }

    /**
     * @param messageBody the messageBody
     * @param notificationtype the notificationtype
     * @param title the title
     * @param id the id
     */
    private void createNotification(String messageBody, String notificationtype, String title, String id) {
        Intent intent = new Intent( this , NotificationActivity. class );
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent resultIntent = PendingIntent.getActivity( this , 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri notificationSoundURI = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder mNotificationBuilder = new NotificationCompat.Builder( this);

        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1) {
            mNotificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
        } else {
            mNotificationBuilder.setSmallIcon(R.mipmap.ic_launcher);
        }

        mNotification = mNotificationBuilder.setTicker(title).setWhen(0)
                .setContentTitle(getString(R.string.app_name))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setContentText(messageBody)
                .setAutoCancel( true )
                .setLargeIcon(BitmapFactory.decodeResource(this.getResources(),R.mipmap.ic_launcher))
                .setSound(notificationSoundURI)
                .setContentIntent(resultIntent).build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, mNotification);
    }

}
