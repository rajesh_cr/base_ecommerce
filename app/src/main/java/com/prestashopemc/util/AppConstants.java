package com.prestashopemc.util;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.prestashopemc.database.DatabaseHelper;
import com.prestashopemc.database.MultipleLanguageService;
import com.prestashopemc.model.Attributes;
import com.prestashopemc.model.MagentoCategoryProduct;
import com.prestashopemc.model.MagentoProduct;
import com.prestashopemc.model.MagentoProductMain;
import com.prestashopemc.model.Option;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by dinakaran on 25/3/16.
 */
public class AppConstants {

    public static String sMagento = "Magento";


   /* public static String sMagentoBaseUrl = SharedPreference.getInstance().getValue("magento_url") ;
    public static String sPrestashopMagento = SharedPreference.getInstance().getValue("presmagento");
    public static String sBaseUrl = SharedPreference.getInstance().getValue("base_url");*/

    public static String sPrestashopMagento = "Prestashop";
    public static String sMagentoBaseUrl = "http://emc.opentestdrive.com/magento2804/index.php/mobile/";
    //public static String sBaseUrl = "http://apps.opentestdrive.com:8080/mybeautymart/index.php?fc=module&module=emcapp&controller=";




    /**
     * Prestashop Open Test Drive URL
     */
    //public static String sMagentoBaseUrl = "http://emc.opentestdrive.com/mage1703/index.php/mobile/";
    public static String sBaseUrl = "http://emc.opentestdrive.com/emc_ruby/index.php?fc=module&module=emcapp&controller=";
    //public static String sBaseUrl = "http://emc.opentestdrive.com/emc2.0/index.php?fc=module&module=emcapp&controller=";

    public static String sShareUrl = "http://emc.opentestdrive.com/emc2.0/index.php?controller=";
    public static String sSuccessUrl = sBaseUrl + "payment&status=success";
    public static String sFailureUrl = sBaseUrl + "payment&status=failure";
    public static String sCartInfo = sBaseUrl + "cart";
    public static String sLogin = sBaseUrl + "login";

    public static String mAlertTitle = "INFORMATION";
    public static String mPositiveBut = "OK";

    public static String getsMagentoBaseUrl() {
        return sMagentoBaseUrl;
    }

    /**
     * Magento Open Test Drive URL
     */
    public static String sMagentoShareUrl = "http://emartproducts.com/";
    public static String sMagentoSuccessUrl = sMagentoBaseUrl + "payment&status=success";
    public static String sMagentoFailureUrl = sMagentoBaseUrl + "payment&status=failure";
    public static String sMagentoLogin = sMagentoBaseUrl + "customer/login?";
    public static String sMagentoCustomer = sMagentoBaseUrl + "customer";
    public static String sMagentoCategory = sMagentoBaseUrl + "category";
    public static String sMagentoRegister = sMagentoBaseUrl + "customer/create?";
    public static String sMagentoForgotPassword = sMagentoBaseUrl + "customer/forgotpassword?";
    public static String sMagentoUpdateProfile = sMagentoBaseUrl + "customer/Save?";
    public static String sMagentoProductDetail = sMagentoBaseUrl + "product/info?";
    public static String sMagentoAddToCart = sMagentoBaseUrl + "index/cartCount?";
    public static String sMagentoCartInfo = sMagentoBaseUrl + "cart/info?";
    public static String sMagentoRootCategories = sMagentoBaseUrl + "cart/rootCategories?";
    public static String sMagentoUpdateCart = sMagentoBaseUrl + "version/updateCartinfo?";
    public static String sMagentoContactUs = sMagentoBaseUrl + "contactus/post?";
    public static String sMagentoProductList = sMagentoBaseUrl + "product/productlist?";
    public static String sMagentoRatingOption = sMagentoBaseUrl + "Review/getRatingOption?";
    public static String sMagentoReviewCreate = sMagentoBaseUrl + "review/create?";
    public static String sMagentoCmsResult = sMagentoBaseUrl + "cms/getCmsContent?";
    public static String sMagentoGetWishlist = sMagentoBaseUrl + "wishlist/list?";
    public static String sMagentoRemoveWishlist = sMagentoBaseUrl + "wishlist/remove?";
    public static String sMagentoAddWishlist = sMagentoBaseUrl + "wishlist/add?";
    public static String sMagentoLicense = sMagentoBaseUrl + "licence/getlicencestatus?";
    public static String sMagentoMyOrderList = sMagentoBaseUrl + "customer/myOrder?";
    public static String sMagentoMyOrderDetails = sMagentoBaseUrl + "cart/orderView?";
    public static String sMagentoMyReviews = sMagentoBaseUrl + "customer/myReview?";
    public static String sMagentoCartRemove = sMagentoBaseUrl + "cart/remove?";
    public static String sMagentoDeleteAddress = sMagentoBaseUrl + "customer/deleteAddress?";
    public static String sMagentoCouponRemove = sMagentoBaseUrl + "coupon/remove?";
    public static String sMagentoStateList = sMagentoBaseUrl + "directory/regionList?";
    public static String sMagentoCountryList = sMagentoBaseUrl + "directory/countryList?";
    public static String sMagentoNewAddress = sMagentoBaseUrl + "customer/createAddress?";
    public static String sMagentoUpdateAddress = sMagentoBaseUrl + "customer/updateAddress?";
    public static String sMagentoCouponAdd = sMagentoBaseUrl + "coupon/add?";
    public static String sMagentoFilterAttribute = sMagentoBaseUrl + "predictivesearch/searchattributefilter?";
    public static String sMagentoProductListType = sMagentoBaseUrl + "product/productlisttype?";
    public static String sMagentoSetShippingMethod = sMagentoBaseUrl + "cart/setShippingMethod?";
    public static String sMagentoAddShippingAddress = sMagentoBaseUrl + "cart/addshippingaddress?";
    public static String sMagentoMultipleBillingAddress = sMagentoBaseUrl + "customer/multipleBillingAddress";
    public static String sMagentoSubCategories = sMagentoBaseUrl + "product/assignedproducts?";
    public static String sMagentoDefaultAddress = sMagentoBaseUrl + "customer/defaultBillingshippingAddress?";
    public static String sMagentoRelatedProduct = sMagentoBaseUrl + "product/relatedproduct?";
    public static String sMagentoCartQntyUpdate = sMagentoBaseUrl + "cart/updateqty?";
    public static String sMagentoConfirmOrderList = sMagentoBaseUrl + "cart/confirmorderlist?";
    public static String sMagentoCreateOrder = sMagentoBaseUrl + "cart/createOrder?";
    public static String sMagentoSetDefaultAddress = sMagentoBaseUrl + "customer/setdefaultaddress?";
    public static String sMagentoProductFilter = sMagentoBaseUrl + "filter/productfilter?";
    public static String sMagentoProductSearch = sMagentoBaseUrl + "search/product?";
    public static String sMagentoPredictiveSearch = sMagentoBaseUrl + "predictivesearch/predictive?";
    public static String sMagentoSearchCategoryProduct = sMagentoBaseUrl + "predictivesearch/predictivesearch?";
    public static String sMagentoKeywordCategory = sMagentoBaseUrl + "predictivesearch/categoryfilter?";
    public static String sMagentoSearchSort = sMagentoBaseUrl + "search/searchsort?";
    public static String sMagentoSearchFilter = sMagentoBaseUrl + "filter/searchfilterproducts?";
    public static String sMagentoChangePassword = sMagentoBaseUrl + "customer/updatepassword?";
    public static String sMagentoPushEnableDisable = sMagentoBaseUrl + "customer/pushenable?";
    public static String sMagentoNotificationList = sMagentoBaseUrl + "customer/pushlist?";

    /**
     * Prestashop action
     */
    public static String sRootCategories = "rootcategories";
    public static String sSubCategories = "subcategories";
    public static String sProductList = "productlist";
    public static String sCategory = "Category";
    public static String sActionCart = "addtocart";
    public static String sUpdateCartInfo = "updatecartinfo";
    public static String sActionProduct = "productdetail";
    public static String sCategorySort = "categorysort";
    public static String sFilterAction = "attributelist";
    public static String sGetRatingAction = "getRatingOption";
    public static String sSubmitRating = "addreview";
    public static String sCartAction = "cartproducts";
    public static String sCartDeletAction = "deletecartproduct";
    public static String sCartQuantityAction = "cartquantity";
    public static String sSubzeroHomePopular = "popularproducts";
    public static String sSubzeroHomeArrivals = "newarrivals";
    public static String sSubzeroHomeSeller = "specials";
    public static String sProductFilter = "productfilter";
    public static String sCouponVoucher = "addvoucher";
    public static String sCouponCancelVoucher = "cancelvoucher";
    public static String sCheckoutAction = "address";
    public static String sCheckoutChangeAction = "additionaladdress";
    public static String sAddNewAddress = "addaddress";
    public static String sDeleteAddress = "deleteaddress";
    public static String sGetCountry = "getcountry";
    public static String sGetState = "getstates";
    public static String sEditAddress = "updateaddress";
    public static String sPayment = "addshippingaddress";
    public static String sConfirm = "confirmorderlist";
    public static String sConfirmOrder = "confirmorder";
    public static String sLoginAction = "login";
    public static String sCreateAction = "register";
    public static String sAddWishlist = "addwishlist";
    public static String sDeleteWishlist = "deletewishlist";
    public static String sCmsAction = "cmsdetail";
    public static String sMyorderAction = "myorder";
    public static String sMyorderDetailsAction = "orderinfo";
    public static String sMyWishlist = "wishlistdetails";
    public static String sMyWishlistDelete = "deletewishlist";
    public static String sMyReviews = "myreviews";
    public static String sSearchProduct = "productsearch";
    public static String sSearchProductList = "productlist";
    public static String sUpdateProfile = "editaccount";
    public static String sUpdatePassword = "updatepassword";
    public static String sSearchFilterAction = "categoryfilter";
    public static String sSearchFilterProductAction = "categoryproductfilter";
    public static String sContactUs = "contactus";
    public static String sGetAddress = "getalladdress";
    public static String sSetDefaultAddress = "setdefaultaddress";
    public static String sShippingPrice = "applyshippingmethod";
    public static String sLicense = "licence";
    public static String sForgotPassword = "forgetpassword";
    public static String sPush = "pushlist";
    public static String sLocator = "getstoreaddress";
    public static String sRelatedProducts = "relatedproduct";
    public static String sPushEnable = "pushenable";
    public static String sPredictiveSearch = "protectivesearch";
    public static String sAdvancedSearch = "advancedsearch";

    /**
     * Dynamic changes for Static Strings
     */
    public static String categoryText = "categoryText";
    public static String moreText = "moreText";
    public static String myOrdersText = "myOrdersText";
    public static String wishlistText = "wishlistText";
    public static String storeLocateText = "storeLocateText";
    public static String contactUsText = "contactUsText";
    public static String filterByText = "filterByText";
    public static String overViewText = "overViewText";
    public static String descriptionText = "descriptionText";
    public static String relatedProductsText = "relatedProductsText";
    public static String ratingsandReviewText = "ratingsandReviewText";
    public static String keyFeaturesText = "keyFeaturesText";
    public static String generalText = "generalText";
    public static String sortText = "sortText";
    public static String sortLowToHighText = "sortLowToHighText";
    public static String sortHighToLowText = "sortHighToLowText";
    public static String sortAscendingtoDescendingText = "sortAscendingtoDescendingText";
    public static String rateThisProductText = "rateThisProductText";
    public static String submitReviewText = "submitReviewText";
    public static String summaryText = "summaryText";
    public static String reviewText = "reviewText";
    public static String cancelText = "cancelText";
    public static String priceDetailText = "priceDetailText";
    public static String noImagesText = "noImagesText";
    public static String myCartText = "myCartText";
    public static String addedCartText = "addedCartText";
    public static String reviewsText = "reviewsText";
    public static String enterCouponText = "enterCouponText";
    public static String removeCouponText = "removeCouponText";
    public static String filterText = "filterText";
    public static String listSort = "listSort";
    public static String loginText = "loginText";
    public static String passwordText = "passwordText";
    public static String signUpText = "signUpText";
    public static String forgotPasswordText = "forgotPasswordText";
    public static String orSignUpWithText = "orSignUpWithText";
    public static String facebookText = "facebookText";
    public static String googleText = "googleText";
    public static String rememberMeText = "rememberMeText";
    public static String mobileNumberText = "mobileNumberText";
    public static String firstnameText = "firstnameText";
    public static String lastnameText = "lastnameText";
    public static String emailIdText = "emailIdText";
    public static String confirmPasswordText = "confirmPasswordText";
    public static String createAccountText = "createAccountText";
    public static String alreadyHaveAccountText = "alreadyHaveAccountText";
    public static String signInText = "signInText";
    public static String submitText = "submitText";
    public static String oldPasswordEmptyAlertText = "oldPasswordEmptyAlertText";
    public static String newPasswordEmptyAlertText = "newPasswordEmptyAlertText";
    public static String confirmPasswordEmptyAlertText = "confirmPasswordEmptyAlertText";
    public static String passwordMismatchAlertText = "passwordMismatchAlertText";
    public static String myAddressText = "myAddressText";
    public static String myAccountText = "myAccountText";
    public static String settingsText = "settingsText";
    public static String logoutText = "logoutText";
    public static String accountText = "accountText";
    public static String myWishlistText = "myWishlistText";
    public static String myReviewsText = "myReviewsText";
    public static String othersText = "othersText";
    public static String storeLocatorText = "storeLocatorText";
    public static String qrcodeText = "qrcodeText";
    public static String productsText = "productsText";
    public static String outOfStockText = "outOfStockText";
    public static String internalServerErrorText = "internalServerErrorText";
    public static String clearAllText = "clearAllText";
    public static String clearText = "clearText";
    public static String itemsText = "itemsText";
    public static String applyText = "applyText";
    public static String rateSelectText = "rateSelectText";
    public static String rateSummaryText = "rateSummaryText";
    public static String rateYourReviewText = "rateYourReviewText";
    public static String continueShoppingText = "continueShoppingText";
    public static String cartCheckoutText = "cartCheckoutText";
    public static String cartOptionText = "cartOptionText";
    public static String cartApplyCouponText = "cartApplyCouponText";
    public static String discountText = "discountText";
    public static String cartShippingcostText = "cartShippingcostText";
    public static String cartTaxesText = "cartTaxesText";
    public static String cartTotalText = "cartTotalText";
    public static String cartPriceText = "cartPriceText";
    public static String noProductInCartText = "noProductInCartText";
    public static String addressText = "addressText";
    public static String paymentText = "paymentText";
    public static String confirmText = "confirmText";
    public static String proceedToPaymentText = "proceedToPaymentText";
    public static String checkoutText = "checkoutText";
    public static String checkoutShippingAddressText = "checkoutShippingAddressText";
    public static String billingAddressText = "billingAddressText";
    public static String changeAddressText = "changeAddressText";
    public static String addNewAddressText = "addNewAddressText";
    public static String newAddressFirstnameText = "newAddressFirstnameText";
    public static String newAddressLastnameText = "newAddressLastnameText";
    public static String emailText = "emailText";
    public static String newAddressMobileText = "newAddressMobileText";
    public static String newAddressStreetText = "newAddressStreetText";
    public static String newAddressPincodetext = "newAddressPincodetext";
    public static String newAddressCityText = "newAddressCityText";
    public static String newAddressStateText = "newAddressStateText";
    public static String newAddressSuccessText = "newAddressSuccessText";
    public static String newAddressCountryText = "newAddressCountryText";
    public static String newAddressUpdatedSuccess = "newAddressUpdatedSuccess";
    public static String newAddressDeletetext = "newAddressDeletetext";
    public static String placeMyOrderText = "placeMyOrderText";
    public static String changeAddressSelectText = "changeAddressSelectText";
    public static String selectPaymentMethodText = "selectPaymentMethodText";
    public static String selectShipingOptionText = "selectShipingOptionText";
    public static String paymentSummaryText = "paymentSummaryText";
    public static String subTotalText = "subTotalText";
    public static String paymentShippingHandlingText = "paymentShippingHandlingText";
    public static String taxText = "taxText";
    public static String amountPayableText = "amountPayableText";
    public static String reviewYourOrderText = "reviewYourOrderText";
    public static String shippingMethodText = "shippingMethodText";
    public static String confirmShippingAddressText = "confirmShippingAddressText";
    public static String confirmBillingAddressText = "confirmBillingAddressText";
    public static String paymentMethodText = "paymentMethodText";
    public static String selectPayementMethodText = "selectPayementMethodText";
    public static String confirmMyOrderText = "confirmMyOrderText";
    public static String orderSuccessText = "orderSuccessText";
    public static String mailTextfieldEmptyAlertText = "mailTextfieldEmptyAlertText";
    public static String pwdMandatoryText = "pwdMandatoryText";
    public static String pwdInvalidText = "pwdInvalidText";
    public static String loginSuccessText = "loginSuccessText";
    public static String createSuccessText = "createSuccessText";
    public static String updateSuccessText = "updateSuccessText";
    public static String allFieldMandatoryText = "allFieldMandatoryText";
    public static String nameMandatoryText = "nameMandatoryText";
    public static String firstNameTextfieldEmptyAlertText = "firstNameTextfieldEmptyAlertText";
    public static String lastNameTextfieldEmptyAlertText = "lastNameTextfieldEmptyAlertText";
    public static String mobileNumberTextfieldEmptyAlertText = "mobileNumberTextfieldEmptyAlertText";
    public static String invalidFirstNameText = "invalidFirstNameText";
    public static String invalidLastNameText = "invalidLastNameText";
    public static String nameValidText = "nameValidText";
    public static String commentMandatoryText = "commentMandatoryText";
    public static String invalidEmailText = "invalidEmailText";
    public static String needLoginText = "needLoginText";
    public static String orderDetailsPage = "orderDetailsPage";
    public static String continueGuestText = "continueGuestText";
    public static String takePhotoText = "takePhotoText";
    public static String galleryText = "galleryText";
    public static String selectPhotoText = "selectPhotoText";
    public static String saveChangesText = "saveChangesText";
    public static String changePasswordText = "changePasswordText";
    public static String confirmQuantityText = "confirmQuantityText";
    public static String editProfileText = "editProfileText";
    public static String oldPasswordText = "oldPasswordText";
    public static String newPasswordText = "newPasswordText";
    public static String showingText = "showingText";
    public static String resultText = "resultText";
    public static String selectCategoryText = "selectCategoryText";
    public static String orderStatusText = "orderStatusText";
    public static String orderPageStatusText = "orderPageStatusText";
    public static String orderReceivedStatusText = "orderReceivedStatusText";
    public static String thanksText = "thanksText";
    public static String orderConfirmationText = "orderConfirmationText";
    public static String whoopsText = "whoopsText";
    public static String orderCancelText = "orderCancelText";
    public static String sendBankWireText = "sendBankWireText";
    public static String orderAmountText = "orderAmountText";
    public static String bankAccountDetailText = "bankAccountDetailText";
    public static String accountnameText = "accountnameText";
    public static String accountNoText = "accountNoText";
    public static String accountBanknametext = "accountBanknametext";
    public static String accountBranchText = "accountBranchText";
    public static String orderIdText = "orderIdText";
    public static String insertOrderReferenceText = "insertOrderReferenceText";
    public static String subOfBankWireText = "subOfBankWireText";
    public static String noWishlistText = "noWishlistText";
    public static String noOrderText = "noOrderText";
    public static String orderIdLabelText = "orderIdLabelText";
    public static String placedOnText = "placedOnText";
    public static String statusText = "statusText";
    public static String productDetailsText = "productDetailsText";
    public static String shippingAddressText = "shippingAddressText";
    public static String noReviewText = "noReviewText";
    public static String alertText = "alertText";
    public static String pushNotificationText = "pushNotificationText";
    public static String languageText = "languageText";
    public static String currencyText = "currencyText";
    public static String noItemsInyourCartText = "noItemsInyourCartText";
    public static String shopNowText = "shopNowText";
    public static String noItemsCartMessageText = "noItemsCartMessageText";
    public static String nameText = "nameText";
    public static String phoneText = "phoneText";
    public static String commentsText = "commentsText";
    public static String sendText = "sendText";
    public static String addressInfoText = "addressInfoText";
    public static String newAddressText = "newAddressText";
    public static String accountEditAddress = "accountEditAddress";
    public static String searchPlaceHolderText = "searchPlaceHolderText";
    public static String notificationText = "notificationText";
    public static String searchKeywordText = "searchKeywordText";
    public static String accessDeniedText = "accessDeniedText";
    public static String contactAdminText = "contactAdminText";
    public static String okayText = "okayText";
    public static String referRegisteredMailText = "referRegisteredMailText";
    public static String notAbleToConnectText = "notAbleToConnectText";
    public static String pleaseCheckYourInternetConnectionText = "pleaseCheckYourInternetConnectionText";
    public static String tryAgainText = "tryAgainText";
    public static String emptyWishlistTitleText = "emptyWishlistTitleText";
    public static String emptyWishlistDescriptionText = "emptyWishlistDescriptionText";
    public static String noOrdersText = "noOrdersText";
    public static String noOrdersMsgText = "noOrdersMsgText";
    public static String startShoppingText = "startShoppingText";
    public static String phoneNumberErrorAlertText = "phoneNumberErrorAlertText";
    public static String welcomeGuestText = "welcomeGuestText";
    public static String signInSignUpText = "signInSignUpText";
    public static String deleteItemText = "deleteItemText";
    public static String deleteInformationText = "deleteInformationText";
    public static String okText = "okText";
    public static String logoutAlertText = "logoutAlertText";
    public static String informationText = "informationText";
    public static String noText = "noText";
    public static String yesText = "yesText";
    public static String permissionCameraRationale = "permission_camera_rationale";
    public static String noCameraPermission = "no_camera_permission";
    public static String lowStorageError = "low_storage_error";
    public static String reviewSubmittedAlertText = "reviewSubmittedAlertText";
    public static String sortDescendingtoAscendingText = "sortDescendingtoAscendingText";
    public static String changeText = "changeText";
    public static String noOfQuantityText = "noOfQuantityText";
    public static String estimatedShippingText = "estimatedShippingText";
    public static String orderBeforeText = "orderBeforeText";
    public static String orderAfterText = "orderAfterText";
    public static String streetMandatoryText = "streetMandatoryText";
    public static String invalidTelephoneText = "invalidTelephoneText";
    public static String cityMandatoryText = "cityMandatoryText";
    public static String zipcodeMandatoryText = "zipcodeMandatoryText";
    public static String streetValidText = "streetValidText";
    public static String cityValidText = "cityValidText";
    public static String selectCountryText = "selectCountryText";
    public static String paymentPageText = "paymentPageText";
    public static String yourCartText = "yourCartText";
    public static String productAvailableText = "productAvailableText";
    public static String chequeAccountDetails = "chequeAccountDetails";
    public static String chequeName = "chequeName";
    public static String chequeDetails = "chequeDetails";
    public static String recentlySearchesText = "recentlySearchesText";
    public static String defaultDelete = "defaultDelete";
    public static String cartAlertText = "cartAlertText";
    public static String currentPasswordText = "currentPasswordText";
    public static String confirmPwdInvalidText = "confirmPwdInvalidText";
    public static String registerPwdMismatchText = "registerPwdMismatchText";
    public static String subcategoryProductCount = "subcategoryProductCount";
    public static String subcategoryViewAll = "subcategoryViewAll";

    /**
     * Progress Dialog
     */
    public static ArrayList<ProgressDialog> sProgStack = new ArrayList<ProgressDialog>();

    /**
     * Presatashop controllers
     */
    public static String sCategoriesController = sBaseUrl + "categories";
    public static String sCartController = sBaseUrl + "cart";
    public static String sSortByProduct = sBaseUrl + "advancedsearch";
    public static String sCheckout = sBaseUrl + "login";
    public static String sNewAddress = sBaseUrl + "address";
    public static String sConfirmController = sBaseUrl + "order";
    public static String sSearchFilter = sBaseUrl + "filtersearch";
    public static String sControllLicense = sBaseUrl + "licence";

    /**
     * Prestashop Share URL
     */
    public static String sShareOptionController = sShareUrl + "product";

    /**
     * Constants
     */
    public static String spageLimit = "10";
    public static String sStoreId = "1";
    public static String sFilterType = "normal";
    public static String sSubzeroHomePagelimit = "10";

    public static int productsPerPage = 10;
    public static int totalProductsRow = 500;

    /**
     * Additional Values
     */
    public static String selectCountry = "Select Country";
    public static String selectState = "Select Region";
    public static String paypalTransUrl, oauthUrl;

    /**
     * Dynamic background color change for Buttons
     *
     * @param gradientDrawable the gradient drawable
     * @param normalcolor      the normalcolor
     * @param context          the context
     * @param strokeColor      the stroke color
     */
    public static void backgroundColor(GradientDrawable gradientDrawable, int normalcolor, Context context, int strokeColor) {

        int colorGradient = ContextCompat.getColor(context, normalcolor);
        int strokeColorGradient = ContextCompat.getColor(context, strokeColor);
        gradientDrawable.setColor(colorGradient);
        gradientDrawable.setStroke(2, strokeColorGradient);

    }

    /**
     * Dynamic color change for App theme
     *
     * @param view the view
     */
    public static void setBackgroundColor(View view) {
        if (!SharedPreference.getInstance().getValue("theme_color").equals("0")) {
            view.setBackgroundColor(Color.parseColor("#" + SharedPreference.getInstance().getValue("theme_color")));
        }
    }

    /**
     * Check status for Database is empty or not
     *
     * @param mContext the m context
     * @return the boolean
     */
    public static boolean isDbExists(Context mContext) {
        DatabaseHelper mDatabaseHelper = new DatabaseHelper(mContext);
        boolean isDbExists = mDatabaseHelper.doesDatabaseExist(mContext, DatabaseHelper.DATABASE_NAME);
        return isDbExists;
    }

    /**
     * Integer to String type convert method
     *
     * @param id      the id
     * @param context the context
     * @return the string
     */
    public static String getString(int id, Context context) {
        String value = context.getString(id);
        return value;
    }

    /**
     * Dynamic background color changes for Image viewer in Product detail page on bottom imagees background
     *
     * @param view    the view
     * @param id      the id
     * @param context the context
     */
    public static void setBackground(View view, int id, Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) { //>= API 21
            view.setBackground(context.getResources().getDrawable(id, context.getTheme()));
        } else {
            view.setBackgroundDrawable(context.getResources().getDrawable(id));
        }
    }

    /**
     * Sort layout falling up performance in Product list page & Search list page
     *
     * @param con     the con
     * @param falling the falling
     * @return the animate in
     */
    public static Animation setAnimateIn(Context con, int falling) {
        Animation animationFadeIn = AnimationUtils.loadAnimation(con, falling);
        return animationFadeIn;
    }

    /**
     * Sort layout fadeout performance in Product list page & Search list page
     *
     * @param mCon    the m con
     * @param fadeout the fadeout
     * @return the animate out
     */
    public static Animation setAnimateOut(Context mCon, int fadeout) {
        Animation animationFadeOut = AnimationUtils.loadAnimation(mCon, fadeout);
        return animationFadeOut;

    }

    /**
     * Image Circle method used in Edit profile page and Create account page
     *
     * @param bitmap the bitmap
     * @return the circle bitmap
     */
    public static Bitmap getCircleBitmap(Bitmap bitmap) {
        final Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
                bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(output);

        final int color = Color.RED;
        final Paint paint = new Paint();
        final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
        final RectF rectF = new RectF(rect);

        paint.setAntiAlias(true);
        canvas.drawARGB(0, 0, 0, 0);
        paint.setColor(color);
        canvas.drawOval(rectF, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));
        canvas.drawBitmap(bitmap, rect, rect, paint);

        bitmap.recycle();
        return output;
    }

    /**
     * String Encode method used for edit text white space remove purpose
     *
     * @param s the s
     * @return the string
     */
    public static String stringEncode(String s) {
        String encodeText = null;
        try {
            encodeText = URLEncoder.encode(s, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return encodeText;
    }

    /**
     * Custom Language Settings
     *
     * @param context the context
     */
    public static void customeLanguage(Context context) {
        String isoCode = SharedPreference.getInstance().getValue("iso_code");
        if (!isoCode.equals("0")) {
            Locale locale = new Locale(isoCode);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
        }
    }

    /**
     * Dynamic text translation method
     *
     * @param mContext the m context
     * @param textKey  the text key
     * @return the text string
     */
    public static String getTextString(Context mContext, String textKey) {
        String dynamicString = "";
        String isoCode = SharedPreference.getInstance().getValue("iso_code");
        MultipleLanguageService mTranslationService = new MultipleLanguageService(mContext);
        try {
            /*String data = ".\u0623\u062a\u064e\u062d\u064e\u062f\u064e\u0651\u062b\u064f \u0642\u0644\u064a\u0644\u0627\u064b \u0645\u0646 \u0627\u0644\u0652\u0639\u064e\u0631\u064e\u0628\u064a\u0651\u0629";
            byte[] bute = data.getBytes("UTF-8");
            try {
                String asd= new String(bute);
                Log.e("Arabic Text === ", asd);
                sss = asd;
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }*/
            if (!isoCode.equals("0")) {
                dynamicString = mTranslationService.retriveTranslationValue(isoCode, textKey);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (dynamicString.isEmpty() || dynamicString == null) {
            Log.e("Missing Key  ==== ", textKey + " === > ISO CODE === " + isoCode);
            String packageName = SharedPreference.getInstance().getValue("package");
            int identifier = mContext.getResources().getIdentifier(textKey, "string", packageName);
            if (identifier != 0) {
                dynamicString = mContext.getResources().getString(identifier);
            }
        }
        return dynamicString;
    }

    /**
     * get density name method
     *
     * @param context the context
     * @return the density name
     */
    public static String getDensityName(Context context) {
        float density = context.getResources().getDisplayMetrics().density;
        if (density >= 4.0) {
            return "xxxhdpi";
        }
        if (density >= 3.0) {
            return "xxhdpi";
        }
        if (density >= 2.0) {
            return "xhdpi";
        }
        if (density >= 1.5) {
            return "hdpi";
        }
        if (density >= 1.0) {
            return "mdpi";
        }
        return "ldpi";
    }

    public static MagentoProductMain productListParser(String response) {
        MagentoProductMain magentoMainProduct = new MagentoProductMain();
        try {
            MagentoProduct magentoProduct = new MagentoProduct();
            ArrayList<MagentoCategoryProduct> mMagentoCatgeoryList = new ArrayList<>();
            ArrayList<Attributes> attributesList = new ArrayList<>();
            ArrayList<Option> optionsList = new ArrayList<>();
            ArrayList<String> productLabelList = new ArrayList<>();
            JSONObject jsonObject = new JSONObject(response);
            JSONArray categoryProduct = new JSONArray();
            JSONObject result = new JSONObject();
            if (jsonObject.has("result")) {
                result = jsonObject.getJSONObject("result");
                categoryProduct = result.getJSONArray("category_products");
                magentoProduct.setProductCount(result.getInt("product_count"));
            }
            for (int i = 0; i < categoryProduct.length(); i++) {
                MagentoCategoryProduct magentoCategoryProduct = new MagentoCategoryProduct();
                JSONObject categoryObject = categoryProduct.getJSONObject(i);
                JSONArray produtLabelArray;
                JSONArray keyArray = null;
                if (categoryObject.has("key")) {
                    keyArray = categoryObject.getJSONArray("key");
                }
                produtLabelArray = categoryObject.getJSONArray("product_label");
                for (int label = 0; label < produtLabelArray.length(); label++) {
                    productLabelList.add(produtLabelArray.getString(label));
                }
                magentoCategoryProduct.setProductLabel(productLabelList);
                magentoCategoryProduct.setProductName(categoryObject.getString("product_name"));
                magentoCategoryProduct.setIdProduct(categoryObject.getString("id_product"));
                magentoCategoryProduct.setWishlist(categoryObject.getBoolean("wishlist"));
                magentoCategoryProduct.setStock(categoryObject.getInt("stock"));
                magentoCategoryProduct.setManageStock(categoryObject.getBoolean("manage_stock"));
                magentoCategoryProduct.setDescriptionShort(categoryObject.getString("description_short"));
                magentoCategoryProduct.setImageUrl(categoryObject.getString("image_url"));
                magentoCategoryProduct.setPrice(categoryObject.getString("price"));
                if (categoryObject.has("specialprice")) {
                    magentoCategoryProduct.setSpecialprice(categoryObject.getString("specialprice"));
                }
                magentoCategoryProduct.setQuantity(categoryObject.getInt("quantity"));

                if (categoryObject.has("product_type")) {
                    magentoCategoryProduct.setProductType(categoryObject.getString("product_type"));
                }
                if (categoryObject.has("category")) {
                    magentoCategoryProduct.setCategory(categoryObject.getString("category"));
                }
                magentoCategoryProduct.setConfig(categoryObject.getBoolean("is_config"));
                magentoCategoryProduct.setCombination(categoryObject.getBoolean("is_combination"));

                if (categoryObject.getBoolean("is_combination")) {
                    for (int key = 0; key < keyArray.length(); key++) {
                        Attributes attributes = new Attributes();
                        JSONObject attributeObject = categoryObject.getJSONObject("attributes");
                        JSONArray keyValueArray = attributeObject.getJSONArray(keyArray.get(key).toString());
                        for (int keyvalue = 0; keyvalue < keyValueArray.length(); keyvalue++) {
                            JSONObject keyValueObject = keyValueArray.getJSONObject(keyvalue);
                            attributes.setmId(keyValueObject.getString("id"));
                            attributes.setmCode(keyValueObject.getString("code"));
                            attributes.setmLabel(keyValueObject.getString("label"));
                            JSONArray optionArray = keyValueObject.getJSONArray("options");
                            for (int optionValue = 0; optionValue < optionArray.length(); optionValue++) {
                                Option option = new Option();
                                ArrayList<String> productList = new ArrayList<>();
                                JSONObject optionObject = optionArray.getJSONObject(optionValue);
                                JSONArray productArray = optionObject.getJSONArray("products");

                                for (int productValue = 0; productValue < productArray.length(); productValue++) {
                                    productList.add(productArray.get(productValue).toString());
                                }
                                option.setId(optionObject.getString("id"));
                                option.setLabel(optionObject.getString("label"));
                                option.setPrice(optionObject.getString("price"));
                                option.setProducts(productList);
                                optionsList.add(option);
                            }
                            attributes.setmOptions(optionsList);
                            attributesList.add(attributes);
                        }
                    }
                    magentoCategoryProduct.setAttributesList(attributesList);
                }
                mMagentoCatgeoryList.add(magentoCategoryProduct);
            }

            magentoProduct.setCategoryProducts(mMagentoCatgeoryList);
            magentoMainProduct.setStatus(jsonObject.getString("status"));

            if (jsonObject.has("errormsg"))
                magentoMainProduct.setmErrorMsg(jsonObject.getString("errormsg"));
            magentoMainProduct.setMagentoProduct(magentoProduct);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return magentoMainProduct;
    }

    public static AlertDialog showAlert(Context con, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(con);
        builder.setTitle(mAlertTitle);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton(mPositiveBut,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });
        AlertDialog alert = builder.create();
        return alert;
    }

    public static void setsMagentoBaseUrl(String magentoBaseUrl) {
        sMagentoBaseUrl = magentoBaseUrl;
        sMagentoLicense = magentoBaseUrl + "licence/getlicencestatus?";
        sMagentoRootCategories = magentoBaseUrl + "cart/rootCategories?";
        sMagentoUpdateCart = magentoBaseUrl + "version/updateCartinfo?";

        sMagentoSuccessUrl = magentoBaseUrl + "payment&status=success";
        sMagentoFailureUrl = magentoBaseUrl + "payment&status=failure";
        sMagentoLogin = magentoBaseUrl + "customer/login?";
        sMagentoCustomer = magentoBaseUrl + "customer";
        sMagentoCategory = magentoBaseUrl + "category";
        sMagentoRegister = magentoBaseUrl + "customer/create?";
        sMagentoForgotPassword = magentoBaseUrl + "customer/forgotpassword?";
        sMagentoUpdateProfile = magentoBaseUrl + "customer/Save?";
        sMagentoProductDetail = magentoBaseUrl + "product/info?";
        sMagentoAddToCart = magentoBaseUrl + "index/cartCount?";
        sMagentoCartInfo = magentoBaseUrl + "cart/info?";
        sMagentoContactUs = magentoBaseUrl + "contactus/post?";
        sMagentoProductList = magentoBaseUrl + "product/productlist?";
        sMagentoRatingOption = magentoBaseUrl + "Review/getRatingOption?";
        sMagentoReviewCreate = magentoBaseUrl + "review/create?";
        sMagentoCmsResult = magentoBaseUrl + "cms/getCmsContent?";
        sMagentoGetWishlist = magentoBaseUrl + "wishlist/list?";
        sMagentoRemoveWishlist = magentoBaseUrl + "wishlist/remove?";
        sMagentoAddWishlist = magentoBaseUrl + "wishlist/add?";
        sMagentoMyOrderList = magentoBaseUrl + "customer/myOrder?";
        sMagentoMyOrderDetails = magentoBaseUrl + "cart/orderView?";
        sMagentoMyReviews = magentoBaseUrl + "customer/myReview?";
        sMagentoCartRemove = magentoBaseUrl + "cart/remove?";
        sMagentoDeleteAddress = magentoBaseUrl + "customer/deleteAddress?";
        sMagentoCouponRemove = magentoBaseUrl + "coupon/remove?";
        sMagentoStateList = magentoBaseUrl + "directory/regionList?";
        sMagentoCountryList = magentoBaseUrl + "directory/countryList?";
        sMagentoNewAddress = magentoBaseUrl + "customer/createAddress?";
        sMagentoUpdateAddress = magentoBaseUrl + "customer/updateAddress?";
        sMagentoCouponAdd = magentoBaseUrl + "coupon/add?";
        sMagentoFilterAttribute = magentoBaseUrl + "predictivesearch/searchattributefilter?";
        sMagentoProductListType = magentoBaseUrl + "product/productlisttype?";
        sMagentoSetShippingMethod = magentoBaseUrl + "cart/setShippingMethod?";
        sMagentoAddShippingAddress = magentoBaseUrl + "cart/addshippingaddress?";
        sMagentoMultipleBillingAddress = magentoBaseUrl + "customer/multipleBillingAddress";
        sMagentoSubCategories = magentoBaseUrl + "product/assignedproducts?";
        sMagentoDefaultAddress = magentoBaseUrl + "customer/defaultBillingshippingAddress?";
        sMagentoRelatedProduct = magentoBaseUrl + "product/relatedproduct?";
        sMagentoCartQntyUpdate = magentoBaseUrl + "cart/updateqty?";
        sMagentoConfirmOrderList = magentoBaseUrl + "cart/confirmorderlist?";
        sMagentoCreateOrder = magentoBaseUrl + "cart/createOrder?";
        sMagentoSetDefaultAddress = magentoBaseUrl + "customer/setdefaultaddress?";
        sMagentoProductFilter = magentoBaseUrl + "filter/productfilter?";
        sMagentoProductSearch = magentoBaseUrl + "search/product?";
        sMagentoPredictiveSearch = magentoBaseUrl + "predictivesearch/predictive?";
        sMagentoSearchCategoryProduct = magentoBaseUrl + "predictivesearch/predictivesearch?";
        sMagentoKeywordCategory = magentoBaseUrl + "predictivesearch/categoryfilter?";
        sMagentoSearchSort = magentoBaseUrl + "search/searchsort?";
        sMagentoSearchFilter = magentoBaseUrl + "filter/searchfilterproducts?";
        sMagentoChangePassword = magentoBaseUrl + "customer/updatepassword?";
        sMagentoPushEnableDisable = magentoBaseUrl + "customer/pushenable?";
        sMagentoNotificationList = magentoBaseUrl + "customer/pushlist?";
    }

    public static void setBaseUrl(String baseUrl) {
        sSuccessUrl = baseUrl + "payment&status=success";
        sFailureUrl = baseUrl + "payment&status=failure";
        sCartInfo = baseUrl + "cart";
        sLogin = baseUrl + "login";

        /**
         * Presatashop controllers
         */
       sCategoriesController = baseUrl + "categories";
       sCartController = baseUrl + "cart";
       sSortByProduct = baseUrl + "advancedsearch";
       sCheckout = baseUrl + "login";
       sNewAddress = baseUrl + "address";
       sConfirmController = baseUrl + "order";
       sSearchFilter = baseUrl + "filtersearch";
       sControllLicense = baseUrl + "licence";
    }
}
