package com.prestashopemc.util;

import android.content.Context;

import com.prestashopemc.R;
import com.prestashopemc.model.AddressValues;
import com.prestashopemc.model.ContactValues;
import com.prestashopemc.model.Customer;
import com.prestashopemc.model.LoginValues;
import com.prestashopemc.model.RegisterValues;
import com.prestashopemc.view.AccountActivity;
import com.prestashopemc.view.AddNewAddress;
import com.prestashopemc.view.ChangePasswordActivity;
import com.prestashopemc.view.ContactUsActivity;
import com.prestashopemc.view.EditProfileActivity;

/**
 * <h1>Common Validation!</h1>
 * The Common Validation contains all the validation function used in entire products.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class CommonValidation {

    /**
     * The Status.
     */
    boolean status = true;
    /**
     * The Msg.
     */
    String msg = "";

    /**
     * The Email regex.
     */
    String EMAIL_REGEX = "^([A-Za-z0-9_%+-])+(.)+([A-Za-z0-9_%+-])+@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,4})$";
    /**
     * The Name pattern.
     */
    String NAME_PATTERN = "^[a-zA-Z\\.\\_\\-\\s]{4,25}$";
    /**
     * The Nick name pat.
     */
    String NICK_NAME_pat = "^[A-Za-z]+$";

    /**
     * The Password pattern.
     */
    String PASSWORD_PATTERN = "^[A-Za-z0-9]{6,10}+$";
    /**
     * The Phone pattern.
     */
// String PHONE_PATTERN = "^[0-9-]{5,15}$";
    String PHONE_PATTERN = "^[0-9\\-\\s]{10}+$";
    /**
     * The Zip pattern.
     */
    String ZIP_PATTERN = "^[0-9\\-\\s]{6}+$";

    /**
     * The Street pat.
     */
    String street_pat = "^[0-9a-zA-Z+\\.\\-\\s\\,\\_\\#]+$";
    /**
     * The City pat.
     */
    String city_pat = "^[\\p{L} .'-+]+$";

    /**
     * The Summary pattern.
     */
    String SUMMARY_PATTERN = "^[A-Za-z, .]+$";
    /**
     * The Review pattern.
     */
    String REVIEW_PATTERN = "^[A-Za-z, .]+$";

    /**
     * validation for address @param ctx the ctx
     *
     * @param values        the values
     * @param is_state_edit the is state edit
     * @return the boolean
     */
    public boolean addressValidation(Context ctx, AddressValues values, boolean is_state_edit) {
        if (checkEmpty(values.getFname()) && checkEmpty(values.getLname()) && checkEmpty(values.getPhone())
                && checkEmpty(values.getStreet()) && checkEmpty(values.getCity())
                && checkEmpty(values.getZipcode()) && values.getCountry().equalsIgnoreCase(AppConstants.selectCountry)
                && values.getState().equalsIgnoreCase(AppConstants.selectState)) {
            msg = AppConstants.getTextString(ctx, AppConstants.allFieldMandatoryText);
            status = false;
        } else if (checkEmpty(values.getFname())) {
            msg = AppConstants.getTextString(ctx, AppConstants.firstNameTextfieldEmptyAlertText);
            status = false;
        } else if (checkEmpty(values.getLname())) {
            msg = AppConstants.getTextString(ctx, AppConstants.lastNameTextfieldEmptyAlertText);
            status = false;
        } else if (checkEmpty(values.getEmail())) {
            msg = AppConstants.getTextString(ctx, AppConstants.mailTextfieldEmptyAlertText);
            status = false;
        } else if (checkEmpty(values.getPhone())) {
            msg = AppConstants.getTextString(ctx, AppConstants.mobileNumberTextfieldEmptyAlertText);
            status = false;
        } else if (checkEmpty(values.getStreet())) {
            msg = AppConstants.getTextString(ctx, AppConstants.streetMandatoryText);
            status = false;
        } else if (checkEmpty(values.getCity())) {
            msg = AppConstants.getTextString(ctx, AppConstants.cityMandatoryText);
            status = false;
        } else if (checkEmpty(values.getZipcode())) {
            msg = AppConstants.getTextString(ctx, AppConstants.zipcodeMandatoryText);
            status = false;
        } else if (values.getCountry().equalsIgnoreCase(AppConstants.selectCountry)) {
            msg = AppConstants.getTextString(ctx, AppConstants.selectCountryText);
            status = false;
        } else if (!checkRegExp(values.getFname(), NAME_PATTERN)) {
            msg = AppConstants.getTextString(ctx, AppConstants.invalidFirstNameText);
            status = false;
        } else if (!checkRegExp(values.getLname(), NAME_PATTERN)) {
            msg = AppConstants.getTextString(ctx, AppConstants.invalidLastNameText);
            status = false;
        } else if (!phoneNumber(values.getPhone())) {
            msg = AppConstants.getTextString(ctx, AppConstants.invalidTelephoneText);
            status = false;
        } /*else if (!checkRegExp(values.getStreet(), street_pat)) {
            msg = AppConstants.getTextString(ctx, AppConstants.streetValidText);
            status = false;
        }*/ else if (!checkRegExp(values.getCity(), city_pat)) {
            msg = AppConstants.getTextString(ctx, AppConstants.cityValidText);
            status = false;
        } else {
            status = true;
        }

        if (status) {

            return status;
        } else {
            AddNewAddress addNewAddress = (AddNewAddress) ctx;
            addNewAddress.snackBar(msg);
            return status;
        }
    }

    /**
     * Validation Method for Login @param ctx the ctx
     *
     * @param loginValues the login values
     * @return the boolean
     */
    public boolean loginValidation(Context ctx, LoginValues loginValues) {
        if (checkEmpty(loginValues.getEmail())) {
            msg = AppConstants.getTextString(ctx, AppConstants.mailTextfieldEmptyAlertText);
            status = false;
        } else if (checkEmpty(loginValues.getPassword())) {
            msg = AppConstants.getTextString(ctx, AppConstants.pwdMandatoryText);
            status = false;
        } else if (!checkPassword(loginValues.getPassword())) {
            msg = AppConstants.getTextString(ctx, AppConstants.pwdInvalidText);
            status = false;
        } else {
            status = true;
        }

        if (status) {
            return status;
        } else {
            AccountActivity accountActivity = (AccountActivity) ctx;
            accountActivity.snackBar(msg);
            return status;
        }
    }

    /**
     * Validation method for create account @param ctx the ctx
     *
     * @param registerValues  the register values
     * @param confirmPassword the confirm password
     * @return the boolean
     */
    public boolean registerValidation(Context ctx, RegisterValues registerValues, String confirmPassword) {
        if (checkEmpty(registerValues.getFirstName())
                && checkEmpty(registerValues.getLastName())
                && checkEmpty(registerValues.getEmail())
                && checkEmpty(registerValues.getPassword()))
        //&& checkEmpty(registerValues.getOtpPassword())//)
        {
            msg = AppConstants.getTextString(ctx, AppConstants.allFieldMandatoryText);
            status = false;
        } else if (checkEmpty(registerValues.getFirstName())) {
            msg = AppConstants.getTextString(ctx, AppConstants.firstNameTextfieldEmptyAlertText);
            status = false;
        } else if (checkEmpty(registerValues.getLastName())) {
            msg = AppConstants.getTextString(ctx, AppConstants.lastNameTextfieldEmptyAlertText);
            status = false;
        } else if (checkEmpty(registerValues.getEmail())) {
            msg = AppConstants.getTextString(ctx, AppConstants.mailTextfieldEmptyAlertText);
            status = false;
        } else if (checkEmpty(registerValues.getPassword())) {
            msg = AppConstants.getTextString(ctx, AppConstants.pwdMandatoryText);
            status = false;
        }else if (checkEmpty(confirmPassword)) {
            msg = AppConstants.getTextString(ctx, AppConstants.confirmPasswordEmptyAlertText);
            status = false;
        } else if (!passwordField(registerValues.getPassword())) {
            msg = AppConstants.getTextString(ctx, AppConstants.pwdInvalidText);
            status = false;
        } else if (!passwordField(confirmPassword)) {
            msg = AppConstants.getTextString(ctx, AppConstants.confirmPwdInvalidText);
            status = false;
        } else if (!checkRegExp(registerValues.getFirstName(), NAME_PATTERN)) {
            msg = AppConstants.getTextString(ctx, AppConstants.invalidFirstNameText);
            status = false;
        } else if (!checkRegExp(registerValues.getLastName(), NAME_PATTERN)) {
            msg = AppConstants.getTextString(ctx, AppConstants.invalidLastNameText);
            status = false;
        } else if (!checkRegExp(registerValues.getEmail(), EMAIL_REGEX)) {
            msg = AppConstants.getTextString(ctx, AppConstants.invalidEmailText);
            status = false;
        } else if (checkEmpty(registerValues.getMobileNum())) {
            msg = AppConstants.getTextString(ctx, AppConstants.mobileNumberTextfieldEmptyAlertText);
            status = false;
        } else if (!phoneNumber(registerValues.getMobileNum())) {
            msg = AppConstants.getTextString(ctx, AppConstants.invalidTelephoneText);
            status = false;
        } else if (!checkPassword(registerValues.getPassword())) {
            msg = AppConstants.getTextString(ctx, AppConstants.pwdInvalidText);
            status = false;
        } else if (!matchPassword(registerValues.getPassword(), confirmPassword)) {
            msg = AppConstants.getTextString(ctx, AppConstants.registerPwdMismatchText);
            status = false;
        } else {
            status = true;
        }
        if (status) {
            return status;
        } else {
            AccountActivity accountActivity = (AccountActivity) ctx;
            accountActivity.snackBar(msg);
            return status;
        }
    }

    /**
     * Validation method for Change password @param ctx the ctx
     *
     * @param oldPass     the old pass
     * @param newPass     the new pass
     * @param confirmPass the confirm pass
     * @return the boolean
     */
    public boolean passwordValidation(Context ctx, String oldPass, String newPass, String confirmPass) {
        if (checkEmpty(oldPass) && checkEmpty(newPass) && checkEmpty(confirmPass)) {
            msg = AppConstants.getTextString(ctx, AppConstants.allFieldMandatoryText);
            status = false;
        } else if (checkEmpty(oldPass)) {
            msg = AppConstants.getTextString(ctx, AppConstants.oldPasswordEmptyAlertText);
            status = false;
        } else if (checkEmpty(newPass)) {
            msg = AppConstants.getTextString(ctx, AppConstants.newPasswordEmptyAlertText);
            status = false;
        }/* else if (!oldPasswordField(oldPass)) {
            msg = AppConstants.getTextString(ctx, AppConstants.currentPasswordText);
            status = false;
        }*/ else if (!passwordField(newPass)) {
            msg = AppConstants.getTextString(ctx, AppConstants.pwdInvalidText);
            status = false;
        } else if (!passwordField(confirmPass)) {
            msg = AppConstants.getTextString(ctx, AppConstants.confirmPwdInvalidText);
            status = false;
        } else if (checkEmpty(confirmPass)) {
            msg = AppConstants.getTextString(ctx, AppConstants.confirmPasswordEmptyAlertText);
            status = false;
        } /*else if (!checkPassword(oldPass)) {
            msg = AppConstants.getTextString(ctx, AppConstants.oldPasswordEmptyAlertText);
            status = false;
        }*/ else if (!matchPassword(newPass, confirmPass)) {
            msg = AppConstants.getTextString(ctx, AppConstants.passwordMismatchAlertText);
            status = false;
        } else {
            status = true;
        }

        if (status) {
            return status;
        } else {
            if (ctx instanceof ChangePasswordActivity) {
                ChangePasswordActivity changePasswordActivity = (ChangePasswordActivity) ctx;
                changePasswordActivity.snackBar(msg);
            }
            return status;
        }
    }

    /**
     * Validation method for Edit Profile @param ctx the ctx
     *
     * @param values_obj the values obj
     * @return the boolean
     */
    public boolean editProfileValidation(Context ctx, Customer values_obj) {
        if (checkEmpty(values_obj.getmFirstName()) && checkEmpty(values_obj.getmLastName())
                && checkEmpty(values_obj.getmEmail()) && checkEmpty(values_obj.getmMobileNumber())) {
            msg = AppConstants.getTextString(ctx, AppConstants.allFieldMandatoryText);
            status = false;
        } else if (checkEmpty(values_obj.getmFirstName())) {
            msg = AppConstants.getTextString(ctx, AppConstants.firstNameTextfieldEmptyAlertText);
            status = false;
        } else if (checkEmpty(values_obj.getmLastName())) {
            msg = AppConstants.getTextString(ctx, AppConstants.lastNameTextfieldEmptyAlertText);
            status = false;
        } else if (checkEmpty(values_obj.getmEmail())) {
            msg = AppConstants.getTextString(ctx, AppConstants.mailTextfieldEmptyAlertText);
            status = false;
        } else if (checkEmpty(values_obj.getmMobileNumber())) {
            msg = AppConstants.getTextString(ctx, AppConstants.mobileNumberTextfieldEmptyAlertText);
            status = false;
        } else if (!checkRegExp(values_obj.getmFirstName(), NAME_PATTERN)) {
            msg = AppConstants.getTextString(ctx, AppConstants.invalidFirstNameText);
            status = false;
        } else if (!checkRegExp(values_obj.getmLastName(), NAME_PATTERN)) {
            msg = AppConstants.getTextString(ctx, AppConstants.invalidLastNameText);
            status = false;
        } else if (!checkRegExp(values_obj.getmEmail(), EMAIL_REGEX)) {
            msg = AppConstants.getTextString(ctx, AppConstants.invalidEmailText);
            status = false;
        } else if (!phoneNumber(values_obj.getmMobileNumber())) {
            msg = AppConstants.getTextString(ctx, AppConstants.invalidTelephoneText);
            status = false;
        } else {
            status = true;
        }

        if (status) {
            return status;
        } else {
            EditProfileActivity editProfileActivity = (EditProfileActivity) ctx;
            editProfileActivity.snackBar(msg);
            return status;
        }
    }

    /**
     * validation method for contact us @param ctx the ctx
     *
     * @param values the values
     * @return the boolean
     */
    public boolean contactUsValidation(Context ctx, ContactValues values) {
        if (checkEmpty(values.getContactName()) && checkEmpty(values.getContactEmail())
                && checkEmpty(values.getContactComent())) {
            msg = AppConstants.getTextString(ctx, AppConstants.allFieldMandatoryText);
            status = false;
        } else if (checkEmpty(values.getContactName())) {
            msg = AppConstants.getTextString(ctx, AppConstants.nameMandatoryText);
            status = false;
        } else if (checkEmpty(values.getContactEmail())) {
            msg = AppConstants.getTextString(ctx, AppConstants.mailTextfieldEmptyAlertText);
            status = false;
        } else if (checkEmpty(values.getContactPhone())) {
            msg = AppConstants.getTextString(ctx, AppConstants.mobileNumberTextfieldEmptyAlertText);
            status = false;
        } else if (!phoneNumber(values.getContactPhone())) {
            msg = AppConstants.getTextString(ctx, AppConstants.invalidTelephoneText);
            status = false;
        } else if (checkEmpty(values.getContactComent())) {
            msg = AppConstants.getTextString(ctx, AppConstants.commentMandatoryText);
            status = false;
        } else if (!checkRegExp(values.getContactName(), NAME_PATTERN)) {
            msg = AppConstants.getTextString(ctx, AppConstants.nameValidText);
            status = false;
        } else if (!checkRegExp(values.getContactEmail(), EMAIL_REGEX)) {
            msg = AppConstants.getTextString(ctx, AppConstants.invalidEmailText);
            status = false;
        } else {
            status = true;
        }

        if (status) {
            return status;
        } else {
            ContactUsActivity contactUsActivity = (ContactUsActivity) ctx;
            contactUsActivity.snackBar(msg);
            return status;
        }
    }

    /**
     * Validation method to check field is empty @param field_value the field value
     *
     * @return the boolean
     */
    public boolean checkEmpty(String field_value) {
        return field_value.trim().isEmpty() ? true : false;
    }

    /**
     * Street and City validation @param field_value the field value
     *
     * @return the boolean
     */
    public boolean checkStreetCity(String field_value) {
        if (field_value.length() >= 1) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Validation method to check regular expression @param field_value the field value
     *
     * @param exp_pattern the exp pattern
     * @return the boolean
     */
    public boolean checkRegExp(String field_value, String exp_pattern) {
        return field_value.matches(exp_pattern) ? true : false;
    }

    /**
     * Validation method to check password character length @param field_value the field value
     *
     * @return the boolean
     */
    public boolean checkPassword(String field_value) {
        return field_value.length() >= 6 ? true : false;
    }

    /**
     * Validation method to check password character length @param field_value1 the field value 1
     *
     * @param field_value2 the field value 2
     * @return the boolean
     */
    public boolean matchPassword(String field_value1, String field_value2) {
        return field_value1.equals(field_value2) ? true : false;
    }

    /**
     * Phone number validation method @param field_value the field value
     *
     * @return the boolean
     */
    public boolean phoneNumber(String field_value) {
        if (field_value.length() >= 10) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Password validation method @param field_value the field value
     *
     * @return the boolean
     */
    public boolean passwordField(String field_value) {
        if (field_value.length() >= 6) {
            return true;
        } else {
            return false;
        }
    }

    public boolean oldPasswordField(String field_value) {
        if (field_value.equals(SharedPreference.getInstance().getValue("currentPassword"))) {
            return true;
        } else {
            return false;
        }
    }

    public boolean checkSiteValidation(Context mContext, String sitename, String selectedCategoryValue) {
        if (sitename.equalsIgnoreCase("") && selectedCategoryValue.equalsIgnoreCase("")) {
            msg = mContext.getResources().getString(R.string.site_category_field_mandatory);
            status = false;
        } else if (sitename.equalsIgnoreCase("") || selectedCategoryValue.equalsIgnoreCase("")) {
            if (sitename.equalsIgnoreCase(""))
                msg = mContext.getResources().getString(R.string.enter_site_text);
            else
                msg = mContext.getResources().getString(R.string.select_your_category);
            status = false;
        } else {
            status = true;
        }

        if (status)
        {
            return true;
        }
        else {
           /* SelectActivity selectActivity = (SelectActivity) mContext;
            selectActivity.snackBar(msg);*/
            AppConstants.showAlert(mContext, msg).show();
            return false;
        }
    }
}
