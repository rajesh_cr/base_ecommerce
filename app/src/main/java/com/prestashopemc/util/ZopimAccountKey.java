package com.prestashopemc.util;

/**
 * <h1>Zopim Account Key!</h1>
 * The Zopim Account Key contains the zopim account key.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class ZopimAccountKey {
    /**
     * Replace this key with your Zopim account key
     */
    public static final String ACCOUNT_KEY = "your account key";
}
