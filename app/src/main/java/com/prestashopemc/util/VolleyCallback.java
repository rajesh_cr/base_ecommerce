package com.prestashopemc.util;

/**
 * <h1>Volley Callback!</h1>
 * The Volley Callback is used to implement the call back for success anf failure response for the network call response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public interface VolleyCallback {

    /**
     * Success.
     *
     * @param response the response
     */
    public void Success(String response);

    /**
     * Failure.
     *
     * @param errorResponse the error response
     */
    public void Failure(String errorResponse);
}
