package com.prestashopemc.util;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.SwitchCompat;
import android.view.View;

import com.prestashopemc.R;

/**
 * <h1>Custom Background!</h1>
 * The Custom Background is used to customized the background color for entire project.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class CustomBackground {

    /**
     * The constant mThemeColor.
     */
    public static String mThemeColor = "#" + SharedPreference.getInstance().getValue("theme_color1");
    /**
     * The constant mTitleTextColor.
     */
    public static String mTitleTextColor = "#" + SharedPreference.getInstance().getValue("text_color");
    /**
     * The constant mTintColor.
     */
    public static String mTintColor = "#" + SharedPreference.getInstance().getValue("tint_color");

    /**
     *  The constant Primary ThemeColor.
     */

    public static String mPrimaryThemeColor = "#" + SharedPreference.getInstance().getValue("theme_color");

    /**
     * Background Text
     *
     * @param v the v
     */
    public static void setBackgroundText(View v) {

        GradientDrawable gd = new GradientDrawable();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor(mThemeColor));
        gd.setCornerRadius(5);
        v.setBackground(gd);
    }

    /**
     * Cart Circle
     *
     * @param v the v
     */
    public static void setCartCircle(View v) {

        GradientDrawable gd = new GradientDrawable();
        gd.setShape(GradientDrawable.OVAL);
        gd.setColor(Color.RED);
        v.setBackground(gd);
    }

    /**
     * Background Text
     *
     * @param v the v
     */
    public static void setBackgroundRedWhiteCombination(View v) {

        GradientDrawable gd = new GradientDrawable();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.WHITE);
        gd.setStroke(3, Color.parseColor(mThemeColor));
        gd.setCornerRadius(12);
        v.setBackground(gd);
    }

    /**
     * Image Border
     *
     * @param v the v
     */
    public static void setImageBorder(View v) {
        GradientDrawable gd = new GradientDrawable();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.WHITE);
        gd.setStroke(4, Color.parseColor(mThemeColor));
        v.setBackground(gd);
    }

    /**
     * Rectangle Background
     *
     * @param v the v
     */
    public static void setRectangleBackground(View v) {

        GradientDrawable gd = new GradientDrawable();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.parseColor("#008AD6"));
        gd.setCornerRadius(4);
        v.setBackground(gd);
    }

    /**
     * Rectangle Background
     *
     * @param v the v
     */
    public static void setRectangleBack(View v) {

        GradientDrawable gd = new GradientDrawable();
        gd.setShape(GradientDrawable.RECTANGLE);
        gd.setColor(Color.WHITE);
        gd.setStroke(2, Color.parseColor("#D7D7D7"));
        gd.setCornerRadius(3);
        v.setBackground(gd);
    }

    /**
     * Checkbox color dynamically changes */
    public static void setCheckBoxColor(AppCompatCheckBox mRememberCheckBox) {
        ColorStateList colorStateList = new ColorStateList(
                new int[][]{

                        new int[]{-android.R.attr.state_checked}, //disabled
                        new int[]{android.R.attr.state_checked} //enabled
                },
                new int[] {

                        Color.parseColor(mPrimaryThemeColor) //disabled
                        ,Color.parseColor(mPrimaryThemeColor) //enabled

                }
        );

        mRememberCheckBox.setSupportButtonTintList(colorStateList);
    }

    /**
     * SwitchCompat color change
     */
    public static void setSwitchColor(SwitchCompat v, Boolean b) {
        // thumb color of your choice
        int thumbColor = Color.RED;

        // trackColor is the thumbColor with 30% transparency (77)
        int trackColor = Color.argb(77, Color.red(thumbColor), Color.green(thumbColor), Color.blue(thumbColor));

        if (b){
            // setting the thumb color
            DrawableCompat.setTintList(v.getThumbDrawable(), new ColorStateList(
                    new int[][]{
                            new int[]{android.R.attr.state_activated},
                            new int[]{}
                    },
                    new int[]{
                            thumbColor,
                            Color.WHITE
                    }));

            // setting the track color
            DrawableCompat.setTintList(v.getTrackDrawable(), new ColorStateList(
                    new int[][]{
                            new int[]{android.R.attr.state_activated},
                            new int[]{}
                    },
                    new int[]{
                            trackColor,
                            Color.parseColor(mPrimaryThemeColor) // full black with 30% transparency (4D)
                    }));
        }else {
            // setting the thumb color
            DrawableCompat.setTintList(v.getThumbDrawable(), new ColorStateList(
                    new int[][]{
                            new int[]{android.R.attr.state_activated},
                            new int[]{}
                    },
                    new int[]{
                            thumbColor,
                            Color.WHITE
                    }));

            // setting the track color
            DrawableCompat.setTintList(v.getTrackDrawable(), new ColorStateList(
                    new int[][]{
                            new int[]{android.R.attr.state_activated},
                            new int[]{}
                    },
                    new int[]{
                            trackColor,
                            Color.LTGRAY // full black with 30% transparency (4D)
                    }));
        }
    }
}
