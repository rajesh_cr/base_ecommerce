package com.prestashopemc.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.prestashopemc.view.BaseActivity;
import com.prestashopemc.view.CreditCardActivity;

/**
 * <h1>Web View Controller!</h1>
 * The Web View Controller is used to control the web view while url is browsed.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class WebViewController extends WebViewClient {

    private Context mContext;
//    private String creditPayment = "https://prestashopemc.com/?fc=module&module=emcapp&controller=paymentfailure&orderid=";

    /**
     * Instantiates a new Web view controller.
     *
     * @param context the context
     */
    public WebViewController(Context context){
        this.mContext = context;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        BaseActivity.hideProgDialog();
        Log.e("Payment Status URL= ==", url);
        Log.e("failure ur","urls"+AppConstants.sFailureUrl);
        if (url.contains(AppConstants.sSuccessUrl)){
            ((CreditCardActivity) mContext).showProgDialiog();
            String[] orderId = url.split("orderid=");
            ((CreditCardActivity) mContext).webSuccessStatus(orderId[1]);
        } else if (url.contains(AppConstants.sFailureUrl)){
            ((CreditCardActivity) mContext).showProgDialiog();
            ((CreditCardActivity) mContext).webFailureStatus();
        }
    }
}
