package com.prestashopemc.util;

import android.content.Context;
import android.content.SharedPreferences;
/**
 * <h1>Shared Preference!</h1>
 * The Shared Preference is used for saving and retriving the preference values globally.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class SharedPreference {

    /**
     * The constant PREFS_NAME.
     */
    public static final String PREFS_NAME = "HOME_PREFS";
    /**
     * The constant PREFS_KEY.
     */
    public static final String PREFS_KEY = "HOME_PREFS_String";

    /**
     * Instantiates a new Shared preference.
     */
    public SharedPreference() {
        super();
    }

      /* singleton implementation for shared pref values*/

    private static SharedPreference pref = new SharedPreference();

    /**
     * Gets instance.
     *
     * @return the instance
     */
    public static SharedPreference getInstance() {
        return pref;
    }

    /**
     * To save string object values method @param Keyvalue the keyvalue
     *
     * @param text the text
     */
    public void save(String Keyvalue, String text) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = MyApplication.getContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE); //1
        editor = settings.edit(); //2
        editor.putString(Keyvalue, text); //3
        editor.commit(); //4
    }

    /**
     * To get string object values method @param Keyvalue the keyvalue
     *
     * @return the value
     */
    public String getValue(String Keyvalue) {
        SharedPreferences settings;
        String text;
        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = MyApplication.getContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = settings.getString(Keyvalue, "0");
        return text;
    }

    /**
     * To clear all saved values method
     */
    public void clearSharedPreference() {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = MyApplication.getContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.clear();
        editor.commit();
    }

    /**
     * To remove particular saved value method @param Keyvalue the keyvalue
     */
    public void removeValue(String Keyvalue) {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = MyApplication.getContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.remove(Keyvalue);
        editor.commit();
    }

    /**
     * To save boolean type values method @param Keyvalue the keyvalue
     *
     * @param status the status
     */
    public void saveBoolean(String Keyvalue, boolean status)
    {
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = MyApplication.getContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE); //1
        editor = settings.edit(); //2
        editor.putBoolean(Keyvalue, status); //3
        editor.commit(); //4
    }

    /**
     * To get boolean type values method @param Keyvalue the keyvalue
     *
     * @return the boolean
     */
    public boolean getBoolean(String Keyvalue) {

        SharedPreferences settings;
        boolean text;
        //settings = PreferenceManager.getDefaultSharedPreferences(context);
        settings = MyApplication.getContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = settings.getBoolean(Keyvalue, false);
        return text;
    }

    /**
     * To save long type values method @param Keyvalue the keyvalue
     *
     * @param lvalue the lvalue
     */
    public void saveLong(String Keyvalue, long lvalue){
        SharedPreferences settings;
        SharedPreferences.Editor editor;
        settings = MyApplication.getContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        editor = settings.edit();
        editor.putLong(Keyvalue, lvalue);
        editor.commit();
    }

    /**
     * To get long type values method @param Keyvalue the keyvalue
     *
     * @return the long
     */
    public long getLong(String Keyvalue){
        SharedPreferences settings;
        long text;
        settings = MyApplication.getContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        text = settings.getLong(Keyvalue, 0);
        return text;
    }
}
