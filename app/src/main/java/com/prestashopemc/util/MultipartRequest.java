package com.prestashopemc.util;

import android.content.Context;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.Gson;
import com.prestashopemc.model.EditProfile;

import org.apache.http.HttpEntity;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.Map;

/**
 * <h1>Multi part Request!</h1>
 * The Multi part Request is used to provide the request for multiple api calls.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class MultipartRequest extends Request<EditProfile>
{
   /* public MultipartRequest(String url, Map<String, String> params, File imageFile, String imgfile, String imgfilefield, Response.ErrorListener errorListener, Response.Listener<String> listener) {
        super(Method.POST, url, errorListener);
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        Log.e("parse network", response.toString());
        return null;
    }

    @Override
    protected void deliverResponse(String response) {
        Log.e("deliver response", response.toString());
    }*/
   private final Gson gson = new Gson();
    /**
     * The constant KEY_PICTURE.
     */
    public static final String KEY_PICTURE = "mypicture";
    /**
     * The constant KEY_PICTURE_NAME.
     */
    public static final String KEY_PICTURE_NAME = "filename";
    /**
     * The constant KEY_ROUTE_ID.
     */
    public static final String KEY_ROUTE_ID = "route_id";
    private Context context;

    private HttpEntity mHttpEntity;

    private String mRouteId;
    private Response.Listener mListener;

    /*public MultipartRequest(String url, String filePath, String routeId,
                            Response.Listener<String> listener,
                            Response.ErrorListener errorListener) {
        super(Method.POST, url, errorListener);

        mRouteId = routeId;
        mListener = listener;
        mHttpEntity = buildMultipartEntity(filePath);
    }*/

    /**
     * Instantiates a new Multipart request.
     *
     * @param url           the url
     * @param filePath      the file path
     * @param file          the file
     * @param routeid       the routeid
     * @param listener      the listener
     * @param errorListener the error listener
     */
    public MultipartRequest(String url, Map<String, String> filePath, File file,
                            String routeid, Response.ErrorListener listener,
                            Response.Listener<EditProfile> errorListener) {
        super(Method.POST, url, listener);
        Log.e("image file", String.valueOf(file));
        mRouteId = routeid;
        mListener = errorListener;
        mHttpEntity = buildMultipartEntity(file);
    }

    private HttpEntity buildMultipartEntity(String filePath) {
        File file = new File(filePath);
        return buildMultipartEntity(file);
    }

    private HttpEntity buildMultipartEntity(File file) {
        MultipartEntityBuilder builder = MultipartEntityBuilder.create();
        String fileName = file.getName();
        FileBody fileBody = new FileBody(file);
        builder.addPart(KEY_PICTURE, fileBody);
        builder.addTextBody(KEY_PICTURE_NAME, fileName);
        builder.addTextBody(KEY_ROUTE_ID, mRouteId);
        return builder.build();
    }

    @Override
    public String getBodyContentType() {
        return mHttpEntity.getContentType().getValue();
    }

    @Override
    public byte[] getBody() throws AuthFailureError {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            mHttpEntity.writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e("IOException writing to ByteArrayOutputStream");
        }
        return bos.toByteArray();
    }

    @Override
    protected Response<EditProfile> parseNetworkResponse(NetworkResponse response) {
        String json = null;
        try {
            json = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return Response.success(gson.fromJson(json, EditProfile.class),
                HttpHeaderParser.parseCacheHeaders(response));
    }

    @Override
    protected void deliverResponse(EditProfile response) {
        Log.e("deliver", String.valueOf(response.getStatus())+"\t"+String.valueOf(response.getCustomerdetails().getProfileImage()));
        mListener.onResponse(response);
    }
}
