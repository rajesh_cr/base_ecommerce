package com.prestashopemc.paypal;

/**
 * PayPalException handles all exceptions related to REST services
 */
public class PayPalRESTException extends Exception {

	/**
	 * Serial Version ID
	 */
	private static final long serialVersionUID = 1L;

    /**
     * Instantiates a new Pay pal rest exception.
     *
     * @param message the message
     */
    public PayPalRESTException(String message) {
		super("ERROR***"+message);
	}

    /**
     * Instantiates a new Pay pal rest exception.
     *
     * @param message   the message
     * @param throwable the throwable
     */
    public PayPalRESTException(String message, Throwable throwable) {
		super("ERROR***"+message, throwable);
	}

    /**
     * Instantiates a new Pay pal rest exception.
     *
     * @param throwable the throwable
     */
    public PayPalRESTException(Throwable throwable) {
		super(throwable);
	}

}
