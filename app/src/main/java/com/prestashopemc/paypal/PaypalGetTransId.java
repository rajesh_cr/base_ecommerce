package com.prestashopemc.paypal;


import android.os.StrictMode;
import android.util.Log;

import com.prestashopemc.util.AppConstants;
import com.prestashopemc.view.OrderConfirmActivity;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.X509TrustManager;

/**
 * The type Paypal get trans id.
 */
public class PaypalGetTransId {
    /**
     * The Trans id.
     */
    String trans_id="0";

    /**
     * Gets trans id.
     *
     * @param pay_key the pay key
     * @return the trans id
     */
    public String getTransId(String pay_key) {
		try {
//			trustEveryone();
            if (android.os.Build.VERSION.SDK_INT > 9) {
                StrictMode.ThreadPolicy policy =
                        new StrictMode.ThreadPolicy.Builder().permitAll().build();
                StrictMode.setThreadPolicy(policy);
            }
			String base64ClientID = null;
			byte[] encoded = null;
			String clientCredentials = OrderConfirmActivity.sPaypalClientId + ":" + OrderConfirmActivity.sPaySecretKey;
			try {
				encoded = Base64.encodeBase64(clientCredentials
						.getBytes("UTF-8"));
				base64ClientID = new String(encoded, "UTF-8");
			} catch (UnsupportedEncodingException e) {
			}

			DefaultHttpClient httpclient = new DefaultHttpClient();
			HttpPost post = new HttpPost(
					"https://api.sandbox.paypal.com/v1/oauth2/token");
			String responseString = "";
			String accessToken = "";
			String transresponseString = "";

			List<NameValuePair> pairs = new ArrayList<NameValuePair>();
			pairs.add(new BasicNameValuePair("grant_type", "client_credentials"));
			post.setEntity(new UrlEncodedFormEntity(pairs));
			post.setHeader("Accept", "application/json");
			post.setHeader("Authorization", "Basic " + base64ClientID);
			try {
				Log.e("eeadasa","terewwerwe"+post);
				HttpResponse response = httpclient.execute(post);
				StatusLine statusLine = response.getStatusLine();
				int statusCode = statusLine.getStatusCode();
				if (statusCode == HttpStatus.SC_OK) {
					HttpEntity httpEntity = response.getEntity();
					responseString = EntityUtils.toString(httpEntity);
					JSONObject obj = new JSONObject(responseString);
					Log.e("eeadsdaasa","terewaswerwe"+responseString);

					accessToken = obj.getString("token_type") + " "
							+ obj.getString("access_token");

					DefaultHttpClient httpclient1 = new DefaultHttpClient();
					HttpGet httpGet = new HttpGet(AppConstants.paypalTransUrl
									+ pay_key);
					httpGet.setHeader("Content-Type", "application/json");
					httpGet.setHeader("Authorization", accessToken);
					try {
						HttpResponse response1 = httpclient1.execute(httpGet); // execute
																				// httpGet
						StatusLine statusLine1 = response1.getStatusLine();
						int statusCode1 = statusLine1.getStatusCode();
						if (statusCode1 == HttpStatus.SC_OK) {
							HttpEntity httpEntity1 = response1.getEntity();
							transresponseString = EntityUtils
									.toString(httpEntity1);
							JSONObject in_obj = new JSONObject(
									transresponseString);
							JSONArray transactions = in_obj
									.getJSONArray("transactions");
							int out_len = transactions.length();
							for (int j = 0; j < out_len; j++) {
								JSONObject out_b = transactions
										.getJSONObject(j);
								JSONArray related_resources = out_b
										.getJSONArray("related_resources");
								int len = related_resources.length();
								for (int i = 0; i < len; i++) {
									JSONObject ob = related_resources
											.getJSONObject(i);
									JSONObject sale = ob.getJSONObject("sale");
									trans_id = sale.getString("id");
									System.out
											.println("@@@@@@@@@@@@@@@@@@@@@@@"
													+ trans_id);
								}
							}
						} else {

							HttpEntity httpEntity1 = response1.getEntity();
							transresponseString = EntityUtils
									.toString(httpEntity1);
						}
					} catch (ClientProtocolException e) {
						//e.printStackTrace();
					} catch (IOException e) {
						//e.printStackTrace();
					}
				} else {
					HttpEntity httpEntity = response.getEntity();
					transresponseString = EntityUtils.toString(httpEntity);
					Log.e("eeadsdaadsaasa","terewadfsdswerwe"+transresponseString);

				}
			} catch (ClientProtocolException e) {
				//e.printStackTrace();
			} catch (IOException e) {
				//e.printStackTrace();
				Log.e("eeadsdaasasafdsa","terewdfsdsdaswerwe"+e);

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			Log.e("eeadsasasdaasasafdsa","terewdfsdsdadassaaswerwe"+e);

		}
		return trans_id;
	}
	private static void trustEveryone() {
		try {
			HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
				public boolean verify(String hostname, SSLSession session) {
					return true;
				}
			});
			SSLContext context = SSLContext.getInstance("TLS");
			context.init(null, new X509TrustManager[]{new X509TrustManager(){
				public void checkClientTrusted(X509Certificate[] chain,
											   String authType) throws CertificateException {}
				public void checkServerTrusted(X509Certificate[] chain,
											   String authType) throws CertificateException {}
				public X509Certificate[] getAcceptedIssuers() {
					return new X509Certificate[0];
				}}}, new SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(
					context.getSocketFactory());
		} catch (Exception e) { // should never happen
			e.printStackTrace();
		}
	}

}
