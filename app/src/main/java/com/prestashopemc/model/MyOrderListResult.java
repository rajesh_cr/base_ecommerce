
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * The type My order list result.
 */
public class MyOrderListResult {

    @SerializedName("currency_symbol")
    @Expose
    private String currencySymbol;
    @SerializedName("real_order_id")
    @Expose
    private String realOrderId;
    @SerializedName("product")
    @Expose
    private List<MyOrderListProduct> product = new ArrayList<MyOrderListProduct>();
    @SerializedName("order_date")
    @Expose
    private String orderDate;
    @SerializedName("order_amount")
    @Expose
    private String orderAmount;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("id_order_status")
    @Expose
    private String idOrderStatus;
    @SerializedName("order_status")
    @Expose
    private String orderStatus;

    /**
     * Gets currency symbol.
     *
     * @return The      currencySymbol
     */
    public String getCurrencySymbol() {
        return currencySymbol;
    }

    /**
     * Sets currency symbol.
     *
     * @param currencySymbol The currency_symbol
     */
    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    /**
     * Gets real order id.
     *
     * @return The      realOrderId
     */
    public String getRealOrderId() {
        return realOrderId;
    }

    /**
     * Sets real order id.
     *
     * @param realOrderId The real_order_id
     */
    public void setRealOrderId(String realOrderId) {
        this.realOrderId = realOrderId;
    }

    /**
     * Gets product.
     *
     * @return The      product
     */
    public List<MyOrderListProduct> getProduct() {
        return product;
    }

    /**
     * Sets product.
     *
     * @param product The product
     */
    public void setProduct(List<MyOrderListProduct> product) {
        this.product = product;
    }

    /**
     * Gets order date.
     *
     * @return The      orderDate
     */
    public String getOrderDate() {
        return orderDate;
    }

    /**
     * Sets order date.
     *
     * @param orderDate The order_date
     */
    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    /**
     * Gets order amount.
     *
     * @return The      orderAmount
     */
    public String getOrderAmount() {
        return orderAmount;
    }

    /**
     * Sets order amount.
     *
     * @param orderAmount The order_amount
     */
    public void setOrderAmount(String orderAmount) {
        this.orderAmount = orderAmount;
    }

    /**
     * Gets payment method.
     *
     * @return The      paymentMethod
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets payment method.
     *
     * @param paymentMethod The payment_method
     */
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    /**
     * Gets id order status.
     *
     * @return The      idOrderStatus
     */
    public String getIdOrderStatus() {
        return idOrderStatus;
    }

    /**
     * Sets id order status.
     *
     * @param idOrderStatus The id_order_status
     */
    public void setIdOrderStatus(String idOrderStatus) {
        this.idOrderStatus = idOrderStatus;
    }

    /**
     * Gets order status.
     *
     * @return The      orderStatus
     */
    public String getOrderStatus() {
        return orderStatus;
    }

    /**
     * Sets order status.
     *
     * @param orderStatus The order_status
     */
    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

}
