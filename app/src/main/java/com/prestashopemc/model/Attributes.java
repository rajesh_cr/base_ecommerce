
package com.prestashopemc.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

public class Attributes implements Serializable {
    @DatabaseField
    private String mId;
    @DatabaseField
    private String mCode;
    @DatabaseField
    private String mLabel;

    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private ArrayList<Option> mOptions = new ArrayList<>();

    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private ArrayList<String> optionValue = new ArrayList<String>();

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmCode() {
        return mCode;
    }

    public void setmCode(String mCode) {
        this.mCode = mCode;
    }

    public String getmLabel() {
        return mLabel;
    }

    public void setmLabel(String mLabel) {
        this.mLabel = mLabel;
    }

    public List<Option> getmOptions() {
        return mOptions;
    }

    public void setmOptions(ArrayList<Option> mOptions) {
        this.mOptions = mOptions;
    }

    public List<String> getOptionValue() {
        return optionValue;
    }

    public void setOptionValue(ArrayList<String> optionValue) {
        this.optionValue = optionValue;
    }
}
