
package com.prestashopemc.model;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * <h1>Filter Result!</h1>
 * The Filter Result is used to implements getter and setter for Filter Result response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class FilterResult implements ParentListItem{

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("attribute_id")
    @Expose
    private String attributeId;
    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("label")
    @Expose
    private String label;

    @SerializedName("options")
    @Expose
    private List<FilterOption> options = new ArrayList<FilterOption>();

    /**
     * Gets name.
     *
     * @return The      name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets attribute id.
     *
     * @return The      attributeId
     */
    public String getAttributeId() {
        return attributeId;
    }

    /**
     * Sets attribute id.
     *
     * @param attributeId The attribute_id
     */
    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    /**
     * Gets status.
     *
     * @return The      status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Gets options.
     *
     * @return The      options
     */
    public List<FilterOption> getOptions() {
        return options;
    }

    /**
     * Sets options.
     *
     * @param options The options
     */
    public void setOptions(List<FilterOption> options) {
        this.options = options;
    }


    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public Collection<FilterOption> getChildItemList() {
        return options;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
