
package com.prestashopemc.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MagentoReviewMain {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("result")
    @Expose
    private List<MagentoReviewResult> result = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<MagentoReviewResult> getResult() {
        return result;
    }

    public void setResult(List<MagentoReviewResult> result) {
        this.result = result;
    }

}
