
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Product Detail!</h1>
 * The Product Detail is used to implements getter and setter for Product Detail response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class MagentoProductDetail {

    private String sku;

    private List<String> image = new ArrayList<String>();

    private List<Attributes> attributesList = new ArrayList<>();

    private List<Feature> features = new ArrayList<Feature>();

    private String estimatedShippingFrom;

    private String estimatedShippingTo;

    private Object shippingNotes;

    private double averageRating;

    private int reviewCount;

    private Object isCustomRequired;

    private Integer isCustom;

    private Ribbon ribbon;

    private Boolean guestComment;

    private Boolean isProductReviews;

    private Boolean isDescription;

    private Boolean wishlist;

    private List<String> productLabel = new ArrayList<String>();

    private MagentoProductinfo productInfo;

    private String productUrl;

    /**
     * Gets guest comment.
     *
     * @return the guest comment
     */
    public Boolean getGuestComment() {
        return guestComment;
    }

    /**
     * Sets guest comment.
     *
     * @param guestComment the guest comment
     */
    public void setGuestComment(Boolean guestComment) {
        this.guestComment = guestComment;
    }

    /**
     * Gets average rating.
     *
     * @return the average rating
     */
    public double getAverageRating() {
        return averageRating;
    }

    /**
     * Sets average rating.
     *
     * @param averageRating the average rating
     */
    public void setAverageRating(double averageRating) {
        this.averageRating = averageRating;
    }

    /**
     * Gets review count.
     *
     * @return the review count
     */
    public int getReviewCount() {
        return reviewCount;
    }

    /**
     * Sets review count.
     *
     * @param reviewCount the review count
     */
    public void setReviewCount(int reviewCount) {
        this.reviewCount = reviewCount;
    }

    /**
     * Gets sku.
     *
     * @return The      sku
     */
    public String getSku() {
        return sku;
    }

    /**
     * Sets sku.
     *
     * @param sku The sku
     */
    public void setSku(String sku) {
        this.sku = sku;
    }

    /**
     * Gets image.
     *
     * @return The      image
     */
    public List<String> getImage() {
        return image;
    }

    /**
     * Sets image.
     *
     * @param image The image
     */
    public void setImage(List<String> image) {
        this.image = image;
    }


    /**
     * Gets features.
     *
     * @return The      features
     */
    public List<Feature> getFeatures() {
        return features;
    }

    /**
     * Sets features.
     *
     * @param features The features
     */
    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    /**
     * Gets estimated shipping from.
     *
     * @return The      estimatedShipping
     */
    public String getEstimatedShippingFrom() {
        return estimatedShippingFrom;
    }

    /**
     * Sets estimated shipping from.
     *
     * @param estimatedShippingFrom The estimated_shipping
     */
    public void setEstimatedShippingFrom(String estimatedShippingFrom) {
        this.estimatedShippingFrom = estimatedShippingFrom;
    }

    /**
     * Gets estimated shipping to.
     *
     * @return the estimated shipping to
     */
    public String getEstimatedShippingTo() {
        return estimatedShippingTo;
    }

    /**
     * Sets estimated shipping to.
     *
     * @param estimatedShippingTo the estimated shipping to
     */
    public void setEstimatedShippingTo(String estimatedShippingTo) {
        this.estimatedShippingTo = estimatedShippingTo;
    }

    /**
     * Gets shipping notes.
     *
     * @return The      shippingNotes
     */
    public Object getShippingNotes() {
        return shippingNotes;
    }

    /**
     * Sets shipping notes.
     *
     * @param shippingNotes The shipping_notes
     */
    public void setShippingNotes(Object shippingNotes) {
        this.shippingNotes = shippingNotes;
    }

    /**
     * Gets is custom required.
     *
     * @return The      isCustomRequired
     */
    public Object getIsCustomRequired() {
        return isCustomRequired;
    }

    /**
     * Sets is custom required.
     *
     * @param isCustomRequired The is_custom_required
     */
    public void setIsCustomRequired(Object isCustomRequired) {
        this.isCustomRequired = isCustomRequired;
    }

    /**
     * Gets is custom.
     *
     * @return The      isCustom
     */
    public Integer getIsCustom() {
        return isCustom;
    }

    /**
     * Sets is custom.
     *
     * @param isCustom The is_custom
     */
    public void setIsCustom(Integer isCustom) {
        this.isCustom = isCustom;
    }

    /**
     * Gets ribbon.
     *
     * @return The      ribbon
     */
    public Ribbon getRibbon() {
        return ribbon;
    }

    /**
     * Sets ribbon.
     *
     * @param ribbon The ribbon
     */
    public void setRibbon(Ribbon ribbon) {
        this.ribbon = ribbon;
    }

    /**
     * Gets wishlist.
     *
     * @return The      wishlist
     */
    public Boolean getWishlist() {
        return wishlist;
    }

    /**
     * Sets wishlist.
     *
     * @param wishlist The wishlist
     */
    public void setWishlist(Boolean wishlist) {
        this.wishlist = wishlist;
    }

    public List<Attributes> getAttributesList() {
        return attributesList;
    }

    public void setAttributesList(List<Attributes> attributesList) {
        this.attributesList = attributesList;
    }

    public Boolean getProductReviews() {
        return isProductReviews;
    }

    public void setProductReviews(Boolean productReviews) {
        isProductReviews = productReviews;
    }

    public Boolean getDescription() {
        return isDescription;
    }

    public void setDescription(Boolean description) {
        isDescription = description;
    }

    public List<String> getProductLabel() {
        return productLabel;
    }

    public void setProductLabel(List<String> productLabel) {
        this.productLabel = productLabel;
    }

    public String getProductUrl() {
        return productUrl;
    }

    public void setProductUrl(String productUrl) {
        this.productUrl = productUrl;
    }

    public MagentoProductinfo getProductInfo() {
        return productInfo;
    }

    public void setProductInfo(MagentoProductinfo mProductInfo) {
        this.productInfo = mProductInfo;
    }
}
