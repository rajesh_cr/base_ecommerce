
package com.prestashopemc.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

/**
 * <h1>Currency!</h1>
 * The Currency is used to implements getter and setter for Currency response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class Currency {

    @SerializedName("id_currency")
    @Expose
    @DatabaseField
    private String idCurrency;
    @SerializedName("name")
    @Expose
    @DatabaseField
    private String name;
    @SerializedName("iso_code")
    @Expose
    @DatabaseField
    private String isoCode;
    @SerializedName("sign")
    @Expose
    @DatabaseField
    private String sign;
    @SerializedName("active")
    @Expose
    @DatabaseField
    private String active;

    /**
     * Gets id currency.
     *
     * @return The      idCurrency
     */
    public String getIdCurrency() {
        return idCurrency;
    }

    /**
     * Sets id currency.
     *
     * @param idCurrency The id_currency
     */
    public void setIdCurrency(String idCurrency) {
        this.idCurrency = idCurrency;
    }

    /**
     * Gets name.
     *
     * @return The      name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets iso code.
     *
     * @return The      isoCode
     */
    public String getIsoCode() {
        return isoCode;
    }

    /**
     * Sets iso code.
     *
     * @param isoCode The iso_code
     */
    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    /**
     * Gets sign.
     *
     * @return The      sign
     */
    public String getSign() {
        return sign;
    }

    /**
     * Sets sign.
     *
     * @param sign The sign
     */
    public void setSign(String sign) {
        this.sign = sign;
    }

    /**
     * Gets active.
     *
     * @return The      active
     */
    public String getActive() {
        return active;
    }

    /**
     * Sets active.
     *
     * @param active The active
     */
    public void setActive(String active) {
        this.active = active;
    }

}
