
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MagentoOrderMain {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("result")
    @Expose
    private OrderConfirmResult result;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public OrderConfirmResult getResult() {
        return result;
    }

    public void setResult(OrderConfirmResult result) {
        this.result = result;
    }

}
