
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Subzero Home Model!</h1>
 * The Subzero Home Model is used to implements getter and setter for Subzero Home Model response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class SubzeroHomeModel {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("product_count")
    @Expose
    private int productCount;

    /**
     * Gets product count.
     *
     * @return the product count
     */
    public int getProductCount() {
        return productCount;
    }

    /**
     * Sets product count.
     *
     * @param productCount the product count
     */
    public void setProductCount(int productCount) {
        this.productCount = productCount;
    }

    @SerializedName("result")
    @Expose
    private List<SubzeroHomeResult> result = new ArrayList<SubzeroHomeResult>();

    /**
     * Gets status.
     *
     * @return The      status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * Gets result.
     *
     * @return The      result
     */
    public List<SubzeroHomeResult> getResult() {
        return result;
    }

    /**
     * Sets result.
     *
     * @param result The result
     */
    public void setResult(List<SubzeroHomeResult> result) {
        this.result = result;
    }

}
