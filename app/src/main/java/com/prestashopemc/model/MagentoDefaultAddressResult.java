
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MagentoDefaultAddressResult {

    @SerializedName("billing_address")
    @Expose
    private MagentoDefaultAddressBilling billingAddress;
    @SerializedName("shipping_address")
    @Expose
    private MagentoDefaultAddressShipping shippingAddress;

    public MagentoDefaultAddressBilling getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(MagentoDefaultAddressBilling billingAddress) {
        this.billingAddress = billingAddress;
    }

    public MagentoDefaultAddressShipping getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(MagentoDefaultAddressShipping shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

}
