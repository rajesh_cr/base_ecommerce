
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.stmt.query.In;

import java.util.List;

/**
 * <h1>Product info!</h1>
 * The Product info is used to implements getter and setter for Product info response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class MagentoProductinfo {

    private String idProduct;

    private String type;

    private String sku;

    private List<Ribbon> ribbon = null;

    private Boolean ribbonEnabled;

    private String taxDispay;

    private String isTaxDispay;

    private int stock;

    private String price;

    private String specialprice;

    private String name;

    private String description;

    private String shortDescription;

    private String minimumQuantity;

    private Integer quantity;

    private String category;

    private Boolean manageStock;


    /**
     * Gets ribbon.
     *
     * @return the ribbon
     */
    public List<Ribbon> getRibbon() {
        return ribbon;
    }

    /**
     * Sets ribbon.
     *
     * @param ribbon the ribbon
     */
    public void setRibbon(List<Ribbon> ribbon) {
        this.ribbon = ribbon;
    }

    /**
     * Gets ribbon enabled.
     *
     * @return the ribbon enabled
     */
    public Boolean getRibbonEnabled() {
        return ribbonEnabled;
    }

    /**
     * Sets ribbon enabled.
     *
     * @param ribbonEnabled the ribbon enabled
     */
    public void setRibbonEnabled(Boolean ribbonEnabled) {
        this.ribbonEnabled = ribbonEnabled;
    }


    /**
     * Gets tax dispay.
     *
     * @return The      taxDispay
     */
    public String getTaxDispay() {
        return taxDispay;
    }

    /**
     * Sets tax dispay.
     *
     * @param taxDispay The tax_dispay
     */
    public void setTaxDispay(String taxDispay) {
        this.taxDispay = taxDispay;
    }

    /**
     * Gets is tax dispay.
     *
     * @return The      isTaxDispay
     */
    public String getIsTaxDispay() {
        return isTaxDispay;
    }

    /**
     * Sets is tax dispay.
     *
     * @param isTaxDispay The is_tax_dispay
     */
    public void setIsTaxDispay(String isTaxDispay) {
        this.isTaxDispay = isTaxDispay;
    }

    /**
     * Gets stock.
     *
     * @return The      stock
     */
    public Integer getStock() {
        return stock;
    }

    /**
     * Sets stock.
     *
     * @param stock The stock
     */
    public void setStock(Integer stock) {
        this.stock = stock;
    }

    /**
     * Gets price.
     *
     * @return The      price
     */
    public String getPrice() {
        return price;
    }

    /**
     * Sets price.
     *
     * @param price The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * Gets specialprice.
     *
     * @return The      specialprice
     */
    public String getSpecialprice() {
        return specialprice;
    }

    /**
     * Sets specialprice.
     *
     * @param specialprice The specialprice
     */
    public void setSpecialprice(String specialprice) {
        this.specialprice = specialprice;
    }

    /**
     * Gets name.
     *
     * @return The      name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets description.
     *
     * @return The      description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets description short.
     *
     * @return The      descriptionShort
     */
    public String getDescriptionShort() {
        return shortDescription;
    }

    /**
     * Sets description short.
     *
     * @param descriptionShort The description_short
     */
    public void setDescriptionShort(String descriptionShort) {
        this.shortDescription = descriptionShort;
    }

    /**
     * Gets minimum quantity.
     *
     * @return The      minimumQuantity
     */
    public String getMinimumQuantity() {
        return minimumQuantity;
    }

    /**
     * Sets minimum quantity.
     *
     * @param minimumQuantity The minimum_quantity
     */
    public void setMinimumQuantity(String minimumQuantity) {
        this.minimumQuantity = minimumQuantity;
    }

    /**
     * Gets quantity.
     *
     * @return The      quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * Sets quantity.
     *
     * @param quantity The quantity
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * Gets category.
     *
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * Sets category.
     *
     * @param category the category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getManageStock() {
        return manageStock;
    }

    public void setManageStock(Boolean manageStock) {
        this.manageStock = manageStock;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    @Override
    public String toString() {
        return "ribbonEnabled :" + ribbonEnabled;
    }
}
