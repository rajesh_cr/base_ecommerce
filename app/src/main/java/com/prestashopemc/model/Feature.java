
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>Feature!</h1>
 * The Feature is used to implements getter and setter for Feature response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class Feature {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("id_feature")
    @Expose
    private String idFeature;

    /**
     * Gets name.
     *
     * @return The      name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets value.
     *
     * @return The      value
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets value.
     *
     * @param value The value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets id feature.
     *
     * @return The      idFeature
     */
    public String getIdFeature() {
        return idFeature;
    }

    /**
     * Sets id feature.
     *
     * @param idFeature The id_feature
     */
    public void setIdFeature(String idFeature) {
        this.idFeature = idFeature;
    }

}
