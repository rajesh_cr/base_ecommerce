
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>My Order Detail OrderData !</h1>
 * The My Order Detail OrderData  is used to implements getter and setter for My Order Detail OrderData  response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class MyOrderDetailOrderData {

    @SerializedName("currency_symbol")
    @Expose
    private String currencySymbol;
    @SerializedName("order_date")
    @Expose
    private String orderDate;
    @SerializedName("billingaddress")
    @Expose
    private MyOrderDetailBillingaddress billingaddress;
    @SerializedName("shipping_address")
    @Expose
    private MyOrderDetailShippingAddress shippingAddress;
    @SerializedName("items")
    @Expose
    private List<MyOrderDetailItem> items = new ArrayList<MyOrderDetailItem>();
    @SerializedName("shipping_method")
    @Expose
    private String shippingMethod;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("sub_total")
    @Expose
    private String subTotal;
    @SerializedName("shipping_amount")
    @Expose
    private String shippingAmount;
    @SerializedName("discount_amount")
    @Expose
    private String discountAmount;
    @SerializedName("grand_total")
    @Expose
    private String grandTotal;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("tax_amount")
    @Expose
    private String taxAmount;

    /**
     * Gets currency symbol.
     *
     * @return The      currencySymbol
     */
    public String getCurrencySymbol() {
        return currencySymbol;
    }

    /**
     * Sets currency symbol.
     *
     * @param currencySymbol The currency_symbol
     */
    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    /**
     * Gets order date.
     *
     * @return The      orderDate
     */
    public String getOrderDate() {
        return orderDate;
    }

    /**
     * Sets order date.
     *
     * @param orderDate The order_date
     */
    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    /**
     * Gets billingaddress.
     *
     * @return The      billingaddress
     */
    public MyOrderDetailBillingaddress getBillingaddress() {
        return billingaddress;
    }

    /**
     * Sets billingaddress.
     *
     * @param billingaddress The billingaddress
     */
    public void setBillingaddress(MyOrderDetailBillingaddress billingaddress) {
        this.billingaddress = billingaddress;
    }

    /**
     * Gets shipping address.
     *
     * @return The      shippingAddress
     */
    public MyOrderDetailShippingAddress getShippingAddress() {
        return shippingAddress;
    }

    /**
     * Sets shipping address.
     *
     * @param shippingAddress The shipping_address
     */
    public void setShippingAddress(MyOrderDetailShippingAddress shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    /**
     * Gets items.
     *
     * @return The      items
     */
    public List<MyOrderDetailItem> getItems() {
        return items;
    }

    /**
     * Sets items.
     *
     * @param items The items
     */
    public void setItems(List<MyOrderDetailItem> items) {
        this.items = items;
    }

    /**
     * Gets shipping method.
     *
     * @return The      shippingMethod
     */
    public String getShippingMethod() {
        return shippingMethod;
    }

    /**
     * Sets shipping method.
     *
     * @param shippingMethod The shipping_method
     */
    public void setShippingMethod(String shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    /**
     * Gets payment method.
     *
     * @return The      paymentMethod
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets payment method.
     *
     * @param paymentMethod The payment_method
     */
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    /**
     * Gets sub total.
     *
     * @return The      subTotal
     */
    public String getSubTotal() {
        return subTotal;
    }

    /**
     * Sets sub total.
     *
     * @param subTotal The sub_total
     */
    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    /**
     * Gets shipping amount.
     *
     * @return The      shippingAmount
     */
    public String getShippingAmount() {
        return shippingAmount;
    }

    /**
     * Sets shipping amount.
     *
     * @param shippingAmount The shipping_amount
     */
    public void setShippingAmount(String shippingAmount) {
        this.shippingAmount = shippingAmount;
    }

    /**
     * Gets discount amount.
     *
     * @return The      discountAmount
     */
    public String getDiscountAmount() {
        return discountAmount;
    }

    /**
     * Sets discount amount.
     *
     * @param discountAmount The discount_amount
     */
    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    /**
     * Gets grand total.
     *
     * @return The      grandTotal
     */
    public String getGrandTotal() {
        return grandTotal;
    }

    /**
     * Sets grand total.
     *
     * @param grandTotal The grand_total
     */
    public void setGrandTotal(String grandTotal) {
        this.grandTotal = grandTotal;
    }

    /**
     * Gets status.
     *
     * @return The      status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Gets tax amount.
     *
     * @return The      taxAmount
     */
    public String getTaxAmount() {
        return taxAmount;
    }

    /**
     * Sets tax amount.
     *
     * @param taxAmount The tax_amount
     */
    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }

}
