package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Predictive Search Model!</h1>
 * The Predictive Search Model is used to implements getter and setter for Predictive Search Model response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class PredictiveSearchModel {

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("result")
    @Expose
    private List<PredictiveSearchResult> result = new ArrayList<PredictiveSearchResult>();

    /**
     * Gets status.
     *
     * @return the status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status the status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Gets result.
     *
     * @return the result
     */
    public List<PredictiveSearchResult> getResult() {
        return result;
    }

    /**
     * Sets result.
     *
     * @param result the result
     */
    public void setResult(List<PredictiveSearchResult> result) {
        this.result = result;
    }

}

