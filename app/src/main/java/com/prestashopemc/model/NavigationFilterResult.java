
package com.prestashopemc.model;

import java.util.Collection;
import java.util.List;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NavigationFilterResult implements ParentListItem {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("values")
    @Expose
    private List<NavigationFilterValues> values = null;
    @SerializedName("prices")
    @Expose
    private NavigationFilterPrices prices;
    @SerializedName("weight")
    @Expose
    private NavigationFilterWeight weight;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<NavigationFilterValues> getValues() {
        return values;
    }

    public void setValues(List<NavigationFilterValues> values) {
        this.values = values;
    }

    public NavigationFilterPrices getPrices() {
        return prices;
    }

    public void setPrices(NavigationFilterPrices prices) {
        this.prices = prices;
    }

    public NavigationFilterWeight getWeight() {
        return weight;
    }

    public void setWeight(NavigationFilterWeight weight) {
        this.weight = weight;
    }

    @Override
    public Collection<NavigationFilterValues> getChildItemList() {
        return values;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
