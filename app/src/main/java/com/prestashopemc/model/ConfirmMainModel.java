
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * <h1>Confirm Main Model!</h1>
 * The Confirm Main Model is used to implements getter and setter for Confirm Main Model response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class ConfirmMainModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("confirm_list")
    @Expose
    private ConfirmList confirmList;

    /**
     * Gets status.
     *
     * @return The      status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Gets confirm list.
     *
     * @return The      confirmList
     */
    public ConfirmList getConfirmList() {
        return confirmList;
    }

    /**
     * Sets confirm list.
     *
     * @param confirmList The confirm_list
     */
    public void setConfirmList(ConfirmList confirmList) {
        this.confirmList = confirmList;
    }

}
