
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>Cart Delete Result!</h1>
 * The Cart Delete Result is used to implements getter and setter for Cart Delete Result response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class CartDeleteResult {

    @SerializedName("cart_count")
    @Expose
    private Integer cartCount;
    @SerializedName("info")
    @Expose
    private CartDeleteInfo info;

    /**
     * Gets cart count.
     *
     * @return The      cartCount
     */
    public Integer getCartCount() {
        return cartCount;
    }

    /**
     * Sets cart count.
     *
     * @param cartCount The cart_count
     */
    public void setCartCount(Integer cartCount) {
        this.cartCount = cartCount;
    }

    /**
     * Gets info.
     *
     * @return The      info
     */
    public CartDeleteInfo getInfo() {
        return info;
    }

    /**
     * Sets info.
     *
     * @param info The info
     */
    public void setInfo(CartDeleteInfo info) {
        this.info = info;
    }

}
