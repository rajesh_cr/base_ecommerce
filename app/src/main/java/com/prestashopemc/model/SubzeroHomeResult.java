
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>Subzero Home Result!</h1>
 * The Subzero Home Result is used to implements getter and setter for Subzero Home Result response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class SubzeroHomeResult {

    @SerializedName("id_product")
    @Expose
    private String idProduct;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("description_short")
    @Expose
    private String descriptionShort;
    @SerializedName("category")
    @Expose
    private String category;
    @SerializedName("id_image")
    @Expose
    private String idImage;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("stock")
    @Expose
    private String stock;
    @SerializedName("tax_dispay")
    @Expose
    private String taxDispay;
    @SerializedName("is_tax_dispay")
    @Expose
    private String isTaxDispay;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("specialprice")
    @Expose
    private String specialprice;
    @SerializedName("ribbon_image")
    @Expose
    private String ribbonImage;
    @SerializedName("left_top")
    @Expose
    private String leftTop;
    @SerializedName("left_bottom")
    @Expose
    private String leftBottom;
    @SerializedName("right_top")
    @Expose
    private String rightTop;
    @SerializedName("right_bottom")
    @Expose
    private String rightBottom;
    @SerializedName("width")
    @Expose
    private Integer width;
    @SerializedName("height")
    @Expose
    private Integer height;
    @SerializedName("wishlist")
    @Expose
    private boolean wishlist;

    /**
     * Is wishlist boolean.
     *
     * @return the boolean
     */
    public boolean isWishlist() {
        return wishlist;
    }

    /**
     * Sets wishlist.
     *
     * @param wishlist the wishlist
     */
    public void setWishlist(boolean wishlist) {
        this.wishlist = wishlist;
    }

    /**
     * Gets id product.
     *
     * @return The      idProduct
     */
    public String getIdProduct() {
        return idProduct;
    }

    /**
     * Sets id product.
     *
     * @param idProduct The id_product
     */
    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    /**
     * Gets product name.
     *
     * @return The      productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Sets product name.
     *
     * @param productName The product_name
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * Gets description short.
     *
     * @return The      descriptionShort
     */
    public String getDescriptionShort() {
        return descriptionShort;
    }

    /**
     * Sets description short.
     *
     * @param descriptionShort The description_short
     */
    public void setDescriptionShort(String descriptionShort) {
        this.descriptionShort = descriptionShort;
    }

    /**
     * Gets category.
     *
     * @return The      category
     */
    public String getCategory() {
        return category;
    }

    /**
     * Sets category.
     *
     * @param category The category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * Gets id image.
     *
     * @return The      idImage
     */
    public String getIdImage() {
        return idImage;
    }

    /**
     * Sets id image.
     *
     * @param idImage The id_image
     */
    public void setIdImage(String idImage) {
        this.idImage = idImage;
    }

    /**
     * Gets image url.
     *
     * @return The      imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * Sets image url.
     *
     * @param imageUrl The image_url
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * Gets quantity.
     *
     * @return The      quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * Sets quantity.
     *
     * @param quantity The quantity
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * Gets stock.
     *
     * @return The      stock
     */
    public String getStock() {
        return stock;
    }

    /**
     * Sets stock.
     *
     * @param stock The stock
     */
    public void setStock(String stock) {
        this.stock = stock;
    }

    /**
     * Gets tax dispay.
     *
     * @return The      taxDispay
     */
    public String getTaxDispay() {
        return taxDispay;
    }

    /**
     * Sets tax dispay.
     *
     * @param taxDispay The tax_dispay
     */
    public void setTaxDispay(String taxDispay) {
        this.taxDispay = taxDispay;
    }

    /**
     * Gets is tax dispay.
     *
     * @return The      isTaxDispay
     */
    public String getIsTaxDispay() {
        return isTaxDispay;
    }

    /**
     * Sets is tax dispay.
     *
     * @param isTaxDispay The is_tax_dispay
     */
    public void setIsTaxDispay(String isTaxDispay) {
        this.isTaxDispay = isTaxDispay;
    }

    /**
     * Gets price.
     *
     * @return The      price
     */
    public String getPrice() {
        return price;
    }

    /**
     * Sets price.
     *
     * @param price The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * Gets specialprice.
     *
     * @return The      specialprice
     */
    public String getSpecialprice() {
        return specialprice;
    }

    /**
     * Sets specialprice.
     *
     * @param specialprice The specialprice
     */
    public void setSpecialprice(String specialprice) {
        this.specialprice = specialprice;
    }

    /**
     * Gets ribbon image.
     *
     * @return The      ribbonImage
     */
    public String getRibbonImage() {
        return ribbonImage;
    }

    /**
     * Sets ribbon image.
     *
     * @param ribbonImage The ribbon_image
     */
    public void setRibbonImage(String ribbonImage) {
        this.ribbonImage = ribbonImage;
    }

    /**
     * Gets left top.
     *
     * @return The      leftTop
     */
    public String getLeftTop() {
        return leftTop;
    }

    /**
     * Sets left top.
     *
     * @param leftTop The left_top
     */
    public void setLeftTop(String leftTop) {
        this.leftTop = leftTop;
    }

    /**
     * Gets left bottom.
     *
     * @return The      leftBottom
     */
    public String getLeftBottom() {
        return leftBottom;
    }

    /**
     * Sets left bottom.
     *
     * @param leftBottom The left_bottom
     */
    public void setLeftBottom(String leftBottom) {
        this.leftBottom = leftBottom;
    }

    /**
     * Gets right top.
     *
     * @return The      rightTop
     */
    public String getRightTop() {
        return rightTop;
    }

    /**
     * Sets right top.
     *
     * @param rightTop The right_top
     */
    public void setRightTop(String rightTop) {
        this.rightTop = rightTop;
    }

    /**
     * Gets right bottom.
     *
     * @return The      rightBottom
     */
    public String getRightBottom() {
        return rightBottom;
    }

    /**
     * Sets right bottom.
     *
     * @param rightBottom The right_bottom
     */
    public void setRightBottom(String rightBottom) {
        this.rightBottom = rightBottom;
    }

    /**
     * Gets width.
     *
     * @return The      width
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * Sets width.
     *
     * @param width The width
     */
    public void setWidth(Integer width) {
        this.width = width;
    }

    /**
     * Gets height.
     *
     * @return The      height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * Sets height.
     *
     * @param height The height
     */
    public void setHeight(Integer height) {
        this.height = height;
    }

}
