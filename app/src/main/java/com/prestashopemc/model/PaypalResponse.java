package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>Paypal Response!</h1>
 * The Paypal Response is used to implements getter and setter for Paypal Response response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class PaypalResponse {

    @SerializedName("access_token")
    @Expose
    private String accesstoken;

    @SerializedName("token_type")
    @Expose
    private String tokentype;

    /**
     * Gets accesstoken.
     *
     * @return the accesstoken
     */
    public String getAccesstoken() {
        return accesstoken;
    }

    /**
     * Sets accesstoken.
     *
     * @param accesstoken the accesstoken
     */
    public void setAccesstoken(String accesstoken) {
        this.accesstoken = accesstoken;
    }

    /**
     * Gets tokentype.
     *
     * @return the tokentype
     */
    public String getTokentype() {
        return tokentype;
    }

    /**
     * Sets tokentype.
     *
     * @param tokentype the tokentype
     */
    public void setTokentype(String tokentype) {
        this.tokentype = tokentype;
    }

}
