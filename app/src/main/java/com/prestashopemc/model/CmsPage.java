package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

/**
 * <h1>Cms Page!</h1>
 * The Cms Page is used to implements getter and setter for Cms Page response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class CmsPage {

    @SerializedName("page_identifier")
    @Expose
    @DatabaseField
    private String page_identifier;
    @SerializedName("title")
    @Expose
    @DatabaseField
    private String title;

    /**
     * Gets page identifier.
     *
     * @return the page identifier
     */
    public String getPage_identifier() {
        return page_identifier;
    }

    /**
     * Sets page identifier.
     *
     * @param page_identifier the page identifier
     */
    public void setPage_identifier(String page_identifier) {
        this.page_identifier = page_identifier;
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets title.
     *
     * @param title the title
     */
    public void setTitle(String title) {
        this.title = title;
    }
}
