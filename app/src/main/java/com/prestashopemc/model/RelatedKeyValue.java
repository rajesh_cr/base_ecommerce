
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * <h1>Related Key Value!</h1>
 * The Related Key Value is used to implements getter and setter for Related Key Value response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class RelatedKeyValue {

    @SerializedName("attribute_name")
    @Expose
    private String attributeName;
    @SerializedName("attribute_value")
    @Expose
    private List<String> attributeValue = null;

    /**
     * Gets attribute name.
     *
     * @return The      attributeName
     */
    public String getAttributeName() {
        return attributeName;
    }

    /**
     * Sets attribute name.
     *
     * @param attributeName The attribute_name
     */
    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    /**
     * Gets attribute value.
     *
     * @return The      attributeValue
     */
    public List<String> getAttributeValue() {
        return attributeValue;
    }

    /**
     * Sets attribute value.
     *
     * @param attributeValue The attribute_value
     */
    public void setAttributeValue(List<String> attributeValue) {
        this.attributeValue = attributeValue;
    }

}
