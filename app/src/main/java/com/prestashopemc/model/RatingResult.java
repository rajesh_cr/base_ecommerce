
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Rating Result!</h1>
 * The Rating Result is used to implements getter and setter for Rating Result response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class RatingResult {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("lable")
    @Expose
    private String lable;

    @SerializedName("options")
    @Expose
    private List<String> options = new ArrayList<>();

    /**
     * Gets id.
     *
     * @return The      id
     */
    public String getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id The id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * Gets lable.
     *
     * @return The      lable
     */
    public String getLable() {
        return lable;
    }

    /**
     * Sets lable.
     *
     * @param lable The lable
     */
    public void setLable(String lable) {
        this.lable = lable;
    }


    public List<String> getOptions() {
        return options;
    }

    public void setOptions(List<String> options) {
        this.options = options;
    }
}
