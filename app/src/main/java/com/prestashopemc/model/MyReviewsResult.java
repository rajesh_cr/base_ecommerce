
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>My Reviews Result!</h1>
 * The My Reviews Result is used to implements getter and setter for My Reviews Result response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class MyReviewsResult {

    @SerializedName("id_product")
    @Expose
    private String idProduct;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("detail")
    @Expose
    private String detail;
    @SerializedName("ratings")
    @Expose
    private String ratings;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("productname")
    @Expose
    private String productname;

    /**
     * Gets id product.
     *
     * @return The      idProduct
     */
    public String getIdProduct() {
        return idProduct;
    }

    /**
     * Sets id product.
     *
     * @param idProduct The id_product
     */
    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    /**
     * Gets image.
     *
     * @return The      image
     */
    public String getImage() {
        return image;
    }

    /**
     * Sets image.
     *
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * Gets title.
     *
     * @return The      title
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets title.
     *
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * Gets detail.
     *
     * @return The      detail
     */
    public String getDetail() {
        return detail;
    }

    /**
     * Sets detail.
     *
     * @param detail The detail
     */
    public void setDetail(String detail) {
        this.detail = detail;
    }

    /**
     * Gets ratings.
     *
     * @return The      ratings
     */
    public String getRatings() {
        return ratings;
    }

    /**
     * Sets ratings.
     *
     * @param ratings The ratings
     */
    public void setRatings(String ratings) {
        this.ratings = ratings;
    }

    /**
     * Gets created at.
     *
     * @return The      createdAt
     */
    public String getCreatedAt() {
        return createdAt;
    }

    /**
     * Sets created at.
     *
     * @param createdAt The created_at
     */
    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    /**
     * Gets productname.
     *
     * @return The      productname
     */
    public String getProductname() {
        return productname;
    }

    /**
     * Sets productname.
     *
     * @param productname The productname
     */
    public void setProductname(String productname) {
        this.productname = productname;
    }

}
