
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.List;

/**
 * <h1>Multiple Language Result!</h1>
 * The Multiple Language Result is used to implements getter and setter for Translation Result response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
@DatabaseTable(tableName = "TranslationResult")
public class MultipleLanguageResult {

    @DatabaseField
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("value")
    @Expose
    private List<MultipleLanguageValue> value = null;

    /**
     * Gets code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param code the code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets value.
     *
     * @return the value
     */
    public List<MultipleLanguageValue> getValue() {
        return value;
    }

    /**
     * Sets value.
     *
     * @param value the value
     */
    public void setValue(List<MultipleLanguageValue> value) {
        this.value = value;
    }

}
