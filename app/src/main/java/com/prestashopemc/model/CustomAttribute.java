
package com.prestashopemc.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * <h1>Custom Attribute!</h1>
 * The Custom Attribute is used to implements getter and setter for Custom Attribute response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
    public class CustomAttribute implements Serializable {

    @DatabaseField(generatedId = true)
    private int id;

    @SerializedName("id_product_attribute")
    @Expose
    @DatabaseField
    private String idProductAttribute;

    @DatabaseField
    private String productId;

    @SerializedName("attribute_combination")
    @Expose
    private List<String> attributeCombination = new ArrayList<String>();

    @SerializedName("price")
    @Expose
    @DatabaseField
    private String price;

    @SerializedName("specialprice")
    @Expose
    @DatabaseField
    private String specialPrice;

    @DatabaseField(foreign=true, foreignAutoCreate=true, foreignAutoRefresh=true)
    private CategoryProduct categoryProduct;

    /**
     * Gets id product attribute.
     *
     * @return The idProductAttribute
     */
    public String getIdProductAttribute() {
        return idProductAttribute;
    }

    /**
     * Sets id product attribute.
     *
     * @param idProductAttribute The id_product_attribute
     */
    public void setIdProductAttribute(String idProductAttribute) {
        this.idProductAttribute = idProductAttribute;
    }


    /**
     * Gets price.
     *
     * @return The price
     */
    public String getPrice() {
        return price;
    }

    /**
     * Sets price.
     *
     * @param price The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * Gets special price.
     *
     * @return the special price
     */
    public String getSpecialPrice() {
        return specialPrice;
    }

    /**
     * Sets special price.
     *
     * @param specialPrice the special price
     */
    public void setSpecialPrice(String specialPrice) {
        this.specialPrice = specialPrice;
    }

    /**
     * Gets attribute combination.
     *
     * @return the attribute combination
     */
    public List<String> getAttributeCombination() {
        return attributeCombination;
    }

    /**
     * Sets attribute combination.
     *
     * @param attributeCombination the attribute combination
     */
    public void setAttributeCombination(List<String> attributeCombination) {
        this.attributeCombination = attributeCombination;
    }

    /**
     * Gets category product.
     *
     * @return the category product
     */
    public CategoryProduct getCategoryProduct() {
        return categoryProduct;
    }

    /**
     * Sets category product.
     *
     * @param categoryProduct the category product
     */
    public void setCategoryProduct(CategoryProduct categoryProduct) {
        this.categoryProduct = categoryProduct;
    }

    /**
     * Gets product id.
     *
     * @return the product id
     */
    public String getProductId() {
        return productId;
    }

    /**
     * Sets product id.
     *
     * @param productId the product id
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }
}
