package com.prestashopemc.model;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * <h1>Cart Model!</h1>
 * The Cart Model is used to implements getter and setter for Cart Model response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class CartModel {

    /**
     * The Map.
     */
    Map<String, Object> map = new LinkedHashMap<String, Object>();

    /**
     * Gets map.
     *
     * @return the map
     */
    public Map<String, Object> getMap() {
        return map;
    }

    /**
     * Sets map.
     *
     * @param map the map
     */
    public void setMap(Map<String, Object> map) {
        this.map = map;
    }

}

