package com.prestashopemc.model;

import com.j256.ormlite.field.DatabaseField;
/**
 * <h1>My String!</h1>
 * The My String is used to implements getter and setter for My String response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class MyString {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField(canBeNull = true, foreign = true)
    private KeyValues mKeyValues;

    @DatabaseField
    private String text;

    @DatabaseField(canBeNull = true, foreign = true)
    private CustomAttribute mCustomAttribute;


    /**
     * Gets key values.
     *
     * @return the key values
     */
    public KeyValues getmKeyValues() {
        return mKeyValues;
    }

    /**
     * Sets key values.
     *
     * @param mKeyValues the m key values
     */
    public void setmKeyValues(KeyValues mKeyValues) {
        this.mKeyValues = mKeyValues;
    }

    /**
     * Gets text.
     *
     * @return the text
     */
    public String getText() {
        return text;
    }

    /**
     * Sets text.
     *
     * @param text the text
     */
    public void setText(String text) {
        this.text = text;
    }

    /**
     * Gets custom attribute.
     *
     * @return the custom attribute
     */
    public CustomAttribute getmCustomAttribute() {
        return mCustomAttribute;
    }

    /**
     * Sets custom attribute.
     *
     * @param mCustomAttribute the m custom attribute
     */
    public void setmCustomAttribute(CustomAttribute mCustomAttribute) {
        this.mCustomAttribute = mCustomAttribute;
    }
}
