
package com.prestashopemc.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

public class MagentoProduct {

    private List<MagentoCategoryProduct> categoryProducts = null;
    @DatabaseField
    private Integer productCount;

    public List<MagentoCategoryProduct> getCategoryProducts() {
        return categoryProducts;
    }

    public void setCategoryProducts(List<MagentoCategoryProduct> categoryProducts) {
        this.categoryProducts = categoryProducts;
    }

    public Integer getProductCount() {
        return productCount;
    }

    public void setProductCount(Integer productCount) {
        this.productCount = productCount;
    }

}
