
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * <h1>Version!</h1>
 * The Version is used to implements getter and setter for Version response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class Version {

    @SerializedName("catalog_version")
    @Expose
    private String catalogVersion;

    @SerializedName("cart_count")
    @Expose
    private String cartCount;

    @SerializedName("homepage_version")
    @Expose
    private String homepageVersion;

    @SerializedName("product_setting_version")
    @Expose
    private String productSettingVersion;

    /**
     * Gets cart count.
     *
     * @return the cart count
     */
    public String getCartCount() {
        return cartCount;
    }

    /**
     * Sets cart count.
     *
     * @param cartCount the cart count
     */
    public void setCartCount(String cartCount) {
        this.cartCount = cartCount;
    }

    /**
     * Sets homepage version.
     *
     * @param homepageVersion the homepage version
     */
    public void setHomepageVersion(String homepageVersion) {
        this.homepageVersion = homepageVersion;
    }

    /**
     * Gets product setting version.
     *
     * @return the product setting version
     */
    public String getProductSettingVersion() {
        return productSettingVersion;
    }

    /**
     * Sets product setting version.
     *
     * @param productSettingVersion the product setting version
     */
    public void setProductSettingVersion(String productSettingVersion) {
        this.productSettingVersion = productSettingVersion;
    }

    /**
     * Gets catalog version.
     *
     * @return The catalogVersion
     */
    public String getCatalogVersion() {
        return catalogVersion;
    }

    /**
     * Sets catalog version.
     *
     * @param catalogVersion The catalog_version
     */
    public void setCatalogVersion(String catalogVersion) {
        this.catalogVersion = catalogVersion;
    }

    /**
     * Gets homepage version.
     *
     * @return The homepageVersion
     */
    public String getHomepageVersion() {
        return homepageVersion;
    }

    /**
     * @param homepageVersion The homepage_version
     */

}
