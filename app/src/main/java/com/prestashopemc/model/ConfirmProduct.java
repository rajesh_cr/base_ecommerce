
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>Confirm Product!</h1>
 * The Confirm Product is used to implements getter and setter for Confirm Product response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class ConfirmProduct {

    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("id_product")
    @Expose
    private String idProduct;
    @SerializedName("supplier_message")
    @Expose
    private String supplierMessage;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("qty")
    @Expose
    private Integer qty;
    @SerializedName("row_total")
    @Expose
    private String rowTotal;
    @SerializedName("id_product_attribute")
    @Expose
    private String idProductAttribute;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets product name.
     *
     * @return The      productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Sets product name.
     *
     * @param productName The product_name
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * Gets id product.
     *
     * @return The      idProduct
     */
    public String getIdProduct() {
        return idProduct;
    }

    /**
     * Sets id product.
     *
     * @param idProduct The id_product
     */
    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    /**
     * Gets supplier message.
     *
     * @return The      supplierMessage
     */
    public String getSupplierMessage() {
        return supplierMessage;
    }

    /**
     * Sets supplier message.
     *
     * @param supplierMessage The supplier_message
     */
    public void setSupplierMessage(String supplierMessage) {
        this.supplierMessage = supplierMessage;
    }

    /**
     * Gets price.
     *
     * @return The      price
     */
    public String getPrice() {
        return price;
    }

    /**
     * Sets price.
     *
     * @param price The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * Gets qty.
     *
     * @return The      qty
     */
    public Integer getQty() {
        return qty;
    }

    /**
     * Sets qty.
     *
     * @param qty The qty
     */
    public void setQty(Integer qty) {
        this.qty = qty;
    }

    /**
     * Gets row total.
     *
     * @return The      rowTotal
     */
    public String getRowTotal() {
        return rowTotal;
    }

    /**
     * Sets row total.
     *
     * @param rowTotal The row_total
     */
    public void setRowTotal(String rowTotal) {
        this.rowTotal = rowTotal;
    }

    /**
     * Gets id product attribute.
     *
     * @return The      idProductAttribute
     */
    public String getIdProductAttribute() {
        return idProductAttribute;
    }

    /**
     * Sets id product attribute.
     *
     * @param idProductAttribute The id_product_attribute
     */
    public void setIdProductAttribute(String idProductAttribute) {
        this.idProductAttribute = idProductAttribute;
    }

    /**
     * Gets image url.
     *
     * @return The      imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * Sets image url.
     *
     * @param imageUrl The image_url
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
