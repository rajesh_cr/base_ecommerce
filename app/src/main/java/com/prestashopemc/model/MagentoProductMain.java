
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MagentoProductMain {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("result")
    @Expose

    private MagentoProduct magentoProduct;

    @SerializedName("errormsg")
    @Expose
    private String mErrorMsg;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public MagentoProduct getMagentoProduct() {
        return magentoProduct;
    }

    public void setMagentoProduct(MagentoProduct magentoProduct) {
        this.magentoProduct = magentoProduct;
    }

    public String getmErrorMsg() {
        return mErrorMsg;
    }

    public void setmErrorMsg(String mErrorMsg) {
        this.mErrorMsg = mErrorMsg;
    }
}
