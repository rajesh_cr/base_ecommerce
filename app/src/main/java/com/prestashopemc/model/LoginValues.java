package com.prestashopemc.model;
/**
 * <h1>Login Values!</h1>
 * The Login Values is used to implements getter and setter for Login Values response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class LoginValues {

    /**
     * The Email.
     */
    String email;
    /**
     * The Password.
     */
    String password;
    /**
     * The Android id.
     */
    String androidId;
    /**
     * The Cart id.
     */
    String cartId;

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets android id.
     *
     * @return the android id
     */
    public String getAndroidId() {
        return androidId;
    }

    /**
     * Sets android id.
     *
     * @param androidId the android id
     */
    public void setAndroidId(String androidId) {
        this.androidId = androidId;
    }

    /**
     * Gets cart id.
     *
     * @return the cart id
     */
    public String getCartId() {
        return cartId;
    }

    /**
     * Sets cart id.
     *
     * @param cartId the cart id
     */
    public void setCartId(String cartId) {
        this.cartId = cartId;
    }
}
