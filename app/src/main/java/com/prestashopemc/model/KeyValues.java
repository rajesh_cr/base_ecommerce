
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * <h1>Key Values!</h1>
 * The Key Values is used to implements getter and setter for Key Values response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class KeyValues implements Serializable{

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String productId;

    @DatabaseField
    @SerializedName("attribute_name")
    @Expose
    private String attributeName;


    @SerializedName("attribute_value")
    @Expose
    private List<String> attributeValue = new ArrayList<String>();

//    @SerializedName("attribute_value")
//    @ForeignCollectionField
//    ForeignCollection<MyString> attributeValueCollection;


    @DatabaseField(foreign = true, foreignAutoCreate = true, foreignAutoRefresh = true)
    private CategoryProduct categoryProduct;

    /**
     * Gets attribute name.
     *
     * @return The attributeName
     */
    public String getAttributeName() {
        return attributeName;
    }

    /**
     * Sets attribute name.
     *
     * @param attributeName The attribute_name
     */
    public void setAttributeName(String attributeName) {
        this.attributeName = attributeName;
    }

    /**
     * Gets attribute value.
     *
     * @return The attributeValue
     */
    public List<String> getAttributeValue() {
        return attributeValue;
    }

    /**
     * Sets attribute value.
     *
     * @param attributeValue The attribute_value
     */
    public void setAttributeValue(List<String> attributeValue) {
        this.attributeValue = attributeValue;
    }

    /**
     * Gets category product.
     *
     * @return the category product
     */
    public CategoryProduct getCategoryProduct() {
        return categoryProduct;
    }

    /**
     * Sets category product.
     *
     * @param categoryProduct the category product
     */
    public void setCategoryProduct(CategoryProduct categoryProduct) {
        this.categoryProduct = categoryProduct;
    }

//    public ForeignCollection<MyString> getAttributeValueCollection() {
//        return attributeValueCollection;
//    }
//
//    public void setAttributeValueCollection(ForeignCollection<MyString> attributeValueCollection) {
//        this.attributeValueCollection = attributeValueCollection;
//    }

    /**
     * Gets product id.
     *
     * @return the product id
     */
    public String getProductId() {
        return productId;
    }

    /**
     * Sets product id.
     *
     * @param productId the product id
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }
}
