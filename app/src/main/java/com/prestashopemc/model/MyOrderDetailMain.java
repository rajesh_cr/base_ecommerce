
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>My Order Detail Main!</h1>
 * The My Order Detail Main is used to implements getter and setter for My Order Detail Main response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class MyOrderDetailMain {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("order_data")
    @Expose
    private MyOrderDetailOrderData orderData;

    /**
     * Gets status.
     *
     * @return The      status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * Gets order data.
     *
     * @return The      orderData
     */
    public MyOrderDetailOrderData getOrderData() {
        return orderData;
    }

    /**
     * Sets order data.
     *
     * @param orderData The order_data
     */
    public void setOrderData(MyOrderDetailOrderData orderData) {
        this.orderData = orderData;
    }

}
