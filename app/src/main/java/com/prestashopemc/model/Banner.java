
package com.prestashopemc.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * <h1>Banner!</h1>
 * The Banner is used to implements getter and setter for Banner response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class Banner {

    @SerializedName("imageurl")
    @Expose
    private String imageurl;
    @SerializedName("position")
    @Expose
    private String position;

    /**
     * Gets imageurl.
     *
     * @return The      imageurl
     */
    public String getImageurl() {
        return imageurl;
    }

    /**
     * Sets imageurl.
     *
     * @param imageurl The imageurl
     */
    public void setImageurl(String imageurl) {
        this.imageurl = imageurl;
    }

    /**
     * Gets position.
     *
     * @return The      position
     */
    public String getPosition() {
        return position;
    }

    /**
     * Sets position.
     *
     * @param position The position
     */
    public void setPosition(String position) {
        this.position = position;
    }

}
