
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Change Address Main Model!</h1>
 * The Change Address Main Model is used to implements getter and setter for Change Address Main Model response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class ChangeAddressMainModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName(value="name", alternate={"address", "result"})
    @Expose
    private List<ChangeAddressModel> address = new ArrayList<ChangeAddressModel>();

    /**
     * Gets status.
     *
     * @return The      status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Gets address.
     *
     * @return The      address
     */
    public List<ChangeAddressModel> getAddress() {
        return address;
    }

    /**
     * Sets address.
     *
     * @param address The address
     */
    public void setAddress(List<ChangeAddressModel> address) {
        this.address = address;
    }

}
