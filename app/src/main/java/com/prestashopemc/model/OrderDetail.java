
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>Order Detail!</h1>
 * The Order Detail is used to implements getter and setter for Order Detail response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class OrderDetail {

    @SerializedName("bank_wire_owner")
    @Expose
    private String bankWireOwner;
    @SerializedName("bank_wire_details")
    @Expose
    private String bankWireDetails;
    @SerializedName("bank_wire_address")
    @Expose
    private String bankWireAddress;
    @SerializedName("reference")
    @Expose
    private String reference;

    /**
     * Gets bank wire owner.
     *
     * @return The      bankWireOwner
     */
    public String getBankWireOwner() {
        return bankWireOwner;
    }

    /**
     * Sets bank wire owner.
     *
     * @param bankWireOwner The bank_wire_owner
     */
    public void setBankWireOwner(String bankWireOwner) {
        this.bankWireOwner = bankWireOwner;
    }

    /**
     * Gets bank wire details.
     *
     * @return The      bankWireDetails
     */
    public String getBankWireDetails() {
        return bankWireDetails;
    }

    /**
     * Sets bank wire details.
     *
     * @param bankWireDetails The bank_wire_details
     */
    public void setBankWireDetails(String bankWireDetails) {
        this.bankWireDetails = bankWireDetails;
    }

    /**
     * Gets bank wire address.
     *
     * @return The      bankWireAddress
     */
    public String getBankWireAddress() {
        return bankWireAddress;
    }

    /**
     * Sets bank wire address.
     *
     * @param bankWireAddress The bank_wire_address
     */
    public void setBankWireAddress(String bankWireAddress) {
        this.bankWireAddress = bankWireAddress;
    }

    /**
     * Gets reference.
     *
     * @return The      reference
     */
    public String getReference() {
        return reference;
    }

    /**
     * Sets reference.
     *
     * @param reference The reference
     */
    public void setReference(String reference) {
        this.reference = reference;
    }

}
