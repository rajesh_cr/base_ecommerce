package com.prestashopemc.model;
/**
 * <h1>Customer!</h1>
 * The Customer is used to implements getter and setter for Customer response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */


public class Customer {
    private String mProfileImage;
    private String mCustomerId;
    private String mFirstName;
    private String mLastName;
    private String mMobileNumber;
    private String mEmail;
    private Boolean mPushEnable;

    /**
     * Gets push enable.
     *
     * @return the push enable
     */
    public Boolean getmPushEnable() {
        return mPushEnable;
    }

    /**
     * Sets push enable.
     *
     * @param mPushEnable the m push enable
     */
    public void setmPushEnable(Boolean mPushEnable) {
        this.mPushEnable = mPushEnable;
    }

    /**
     * Gets profile image.
     *
     * @return the profile image
     */
    public String getmProfileImage() {
        return mProfileImage;
    }

    /**
     * Sets profile image.
     *
     * @param mProfileImage the m profile image
     */
    public void setmProfileImage(String mProfileImage) {
        this.mProfileImage = mProfileImage;
    }

    /**
     * Gets customer id.
     *
     * @return the customer id
     */
    public String getmCustomerId() {
        return mCustomerId;
    }

    /**
     * Sets customer id.
     *
     * @param mCustomerId the m customer id
     */
    public void setmCustomerId(String mCustomerId) {
        this.mCustomerId = mCustomerId;
    }

    /**
     * Gets first name.
     *
     * @return the first name
     */
    public String getmFirstName() {
        return mFirstName;
    }

    /**
     * Sets first name.
     *
     * @param mFirstName the m first name
     */
    public void setmFirstName(String mFirstName) {
        this.mFirstName = mFirstName;
    }

    /**
     * Gets last name.
     *
     * @return the last name
     */
    public String getmLastName() {
        return mLastName;
    }

    /**
     * Sets last name.
     *
     * @param mLastName the m last name
     */
    public void setmLastName(String mLastName) {
        this.mLastName = mLastName;
    }

    /**
     * Gets mobile number.
     *
     * @return the mobile number
     */
    public String getmMobileNumber() {
        return mMobileNumber;
    }

    /**
     * Sets mobile number.
     *
     * @param mMobileNumber the m mobile number
     */
    public void setmMobileNumber(String mMobileNumber) {
        this.mMobileNumber = mMobileNumber;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getmEmail() {
        return mEmail;
    }

    /**
     * Sets email.
     *
     * @param mEmail the m email
     */
    public void setmEmail(String mEmail) {
        this.mEmail = mEmail;
    }
}
