package com.prestashopemc.model;

import com.j256.ormlite.field.DatabaseField;

/**
 * <h1>Custom Attribute Value!</h1>
 * The Custom Attribute Value is used to implements getter and setter for Custom Attribute Value response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class CustomAttributeValue {

    @DatabaseField
    private String productId;

    @DatabaseField
    private String keyValue;

    /**
     * Gets product id.
     *
     * @return the product id
     */
    public String getProductId() {
        return productId;
    }

    /**
     * Sets product id.
     *
     * @param productId the product id
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * Gets custom attribute value.
     *
     * @return the custom attribute value
     */
    public String getCustomAttributeValue() {
        return keyValue;
    }

    /**
     * Sets custom attribute value.
     *
     * @param keyValue the key value
     */
    public void setCustomAttributeValue(String keyValue) {
        this.keyValue = keyValue;
    }
}


