
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * The type Notification result.
 */
public class NotificationResult {

    @SerializedName("rulemessage")
    @Expose
    private String rulemessage;
    @SerializedName("pushdate")
    @Expose
    private String pushdate;
    @SerializedName("productname")
    @Expose
    private String productname;
    @SerializedName("is_type")
    @Expose
    private Boolean isType;

    /**
     * Gets rulemessage.
     *
     * @return The      rulemessage
     */
    public String getRulemessage() {
        return rulemessage;
    }

    /**
     * Sets rulemessage.
     *
     * @param rulemessage The rulemessage
     */
    public void setRulemessage(String rulemessage) {
        this.rulemessage = rulemessage;
    }

    /**
     * Gets pushdate.
     *
     * @return The      pushdate
     */
    public String getPushdate() {
        return pushdate;
    }

    /**
     * Sets pushdate.
     *
     * @param pushdate The pushdate
     */
    public void setPushdate(String pushdate) {
        this.pushdate = pushdate;
    }

    /**
     * Gets productname.
     *
     * @return The      productname
     */
    public String getProductname() {
        return productname;
    }

    /**
     * Sets productname.
     *
     * @param productname The productname
     */
    public void setProductname(String productname) {
        this.productname = productname;
    }

    /**
     * Gets is type.
     *
     * @return The      isType
     */
    public Boolean getIsType() {
        return isType;
    }

    /**
     * Sets is type.
     *
     * @param isType The is_type
     */
    public void setIsType(Boolean isType) {
        this.isType = isType;
    }

}
