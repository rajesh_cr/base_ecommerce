
package com.prestashopemc.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

public class MagentoCategoryProduct implements Serializable {

    @DatabaseField(generatedId = true)
    private int id;
    @DatabaseField
    private String productName;
    @DatabaseField
    private String idProduct;
    @DatabaseField
    private Boolean wishlist;
    @DatabaseField
    private Integer stock;
    @DatabaseField
    private Boolean manageStock;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private ArrayList<String> productLabel = null;
    @DatabaseField
    private String descriptionShort;
    @DatabaseField
    private String imageUrl;
    @DatabaseField
    private String price;
    @DatabaseField
    private String specialprice;
    @DatabaseField
    private Integer quantity;
    @DatabaseField
    private String productType;
    @DatabaseField
    private String category;
    @DatabaseField
    private Boolean isConfig;
    @DatabaseField
    private Boolean isCombination;
    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private ArrayList<Attributes> attributesList = new ArrayList<>();
    @DatabaseField
    private String categoryId;

    @DatabaseField
    private String currentPage;

    @DatabaseField
    private String customerId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public Boolean getWishlist() {
        return wishlist;
    }

    public void setWishlist(Boolean wishlist) {
        this.wishlist = wishlist;
    }

    public Integer getStock() {
        return stock;
    }

    public void setStock(Integer stock) {
        this.stock = stock;
    }

    public Boolean getManageStock() {
        return manageStock;
    }

    public void setManageStock(Boolean manageStock) {
        this.manageStock = manageStock;
    }

    public ArrayList<String> getProductLabel() {
        return productLabel;
    }

    public void setProductLabel(ArrayList<String> productLabel) {
        this.productLabel = productLabel;
    }

    public String getDescriptionShort() {
        return descriptionShort;
    }

    public void setDescriptionShort(String descriptionShort) {
        this.descriptionShort = descriptionShort;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSpecialprice() {
        return specialprice;
    }

    public void setSpecialprice(String specialprice) {
        this.specialprice = specialprice;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public Boolean getIsConfig() {
        return isConfig;
    }

    public void setIsConfig(Boolean isConfig) {
        this.isConfig = isConfig;
    }

    public Boolean getIsCombination() {
        return isCombination;
    }

    public void setIsCombination(Boolean isCombination) {
        this.isCombination = isCombination;
    }
//
//    public List<String> getKey() {
//        return key;
//    }
//
//    public void setKey(List<String> key) {
//        this.key = key;
//    }

    public Boolean getConfig() {
        return isConfig;
    }

    public void setConfig(Boolean config) {
        isConfig = config;
    }

    public Boolean getCombination() {
        return isCombination;
    }

    public void setCombination(Boolean combination) {
        isCombination = combination;
    }

    public List<Attributes> getAttributesList() {
        return attributesList;
    }

    public void setAttributesList(ArrayList<Attributes> attributesList) {
        this.attributesList = attributesList;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}
