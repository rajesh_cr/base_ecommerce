
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


/**
 * <h1>Main Page!</h1>
 * The Main Page is used to implements getter and setter for Main Page response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class MainPage {

    @SerializedName("logo")
    @Expose
    private String logo;
    @SerializedName("categories")
    @Expose
    private List<Category> categories = new ArrayList<Category>();

    @SerializedName("product_label")
    @Expose
    private ArrayList<MagentoProductLabel> magentoProductLabel = new ArrayList<MagentoProductLabel>();

    @SerializedName("cms_list")
    @Expose
    private List<CmsPage> cmsList = new ArrayList<CmsPage>();
    @SerializedName("cart_count")
    @Expose
    private String cartCount;
    @SerializedName("currency_symbol")
    @Expose
    private String currencySymbol;
    @SerializedName("catalog_version")
    @Expose
    private String catalogVersion;
    @SerializedName("homepage_version")
    @Expose
    private String homepageVersion;

    @SerializedName("product_setting_version")
    @Expose
    private String productSettingVersion;

    @SerializedName("product_list_layout")
    @Expose
    private String productListLayout;

    @SerializedName("languages")
    @Expose
    private List<Language> languages = new ArrayList<Language>();
    @SerializedName("currency")
    @Expose
    private List<Currency> currency = new ArrayList<Currency>();
    @SerializedName("tax_display")
    @Expose
    private String taxDisplay;
    @SerializedName("zopim_id")
    @Expose
    private String zopimId;
    @SerializedName("default_lang")
    @Expose
    private String defaultLang;
    @SerializedName("default_currency")
    @Expose
    private String defaultCurrency;
    @SerializedName("google_tracking")
    @Expose
    private String googleTracking;
    @SerializedName("paypal_client_id")
    @Expose
    private String paypalClientId;
    @SerializedName("paypal_secret_key")
    @Expose
    private String paypalSecretKey;
    @SerializedName("paypal_mode")
    @Expose
    private String paypalMode;
    @SerializedName("theme_color")
    @Expose
    private String themeColor;
    @SerializedName("theme_color1")
    @Expose
    private String themeColor1;
    @SerializedName("text_color")
    @Expose
    private String textColor;
    @SerializedName("tint_color")
    @Expose
    private String tintColor;
    @SerializedName("guest_checkout_status")
    @Expose
    private String guestCheckoutStatus;
    @SerializedName("product_label_status")
    @Expose
    private String productLabelStatus;
    @SerializedName("wishlist_enable_status")
    @Expose
    private String wishlistEnableStatus;
    @SerializedName("filter_enable_status")
    @Expose
    private String filterEnableStatus;
    @SerializedName("stock_management_status")
    @Expose
    private String stockManagementStatus;
    @SerializedName("review_rating_status")
    @Expose
    private String reviewRatingStatus;
    @SerializedName("qrcode_scanner_status")
    @Expose
    private String qrcodeScannerStatus;
    @SerializedName("store_locatore_status")
    @Expose
    private String storeLocatoreStatus;
    @SerializedName("reorder_status")
    @Expose
    private String reorderStatus;
    @SerializedName("call_back_number")
    @Expose
    private String callBackNumber;
    @SerializedName("translation_result")
    @Expose
    private List<MultipleLanguageResult> multipleListResult = new ArrayList<MultipleLanguageResult>();
    @SerializedName("home_page_list")
    @Expose
    private List<HomePageList> homePageList = new ArrayList<HomePageList>();
    @SerializedName("coupon_section_display")
    @Expose
    private String couponSectionDisplay;

    @SerializedName("cart_section_display")
    @Expose
    private String cartSectionDisplay;
    @SerializedName("search_below_header")
    @Expose
    private Boolean mSearchBelowheader;

    /**
     * Gets multiple list result.
     *
     * @return the multiple list result
     */
    public List<MultipleLanguageResult> getMultipleListResult() {
        return multipleListResult;
    }

    /**
     * Sets multiple list result.
     *
     * @param multipleListResult the multiple list result
     */
    public void setMultipleListResult(List<MultipleLanguageResult> multipleListResult) {
        this.multipleListResult = multipleListResult;
    }

    /**
     * Gets cart section display.
     *
     * @return the cart section display
     */
    public String getCartSectionDisplay() {
        return cartSectionDisplay;
    }

    /**
     * Sets cart section display.
     *
     * @param cartSectionDisplay the cart section display
     */
    public void setCartSectionDisplay(String cartSectionDisplay) {
        this.cartSectionDisplay = cartSectionDisplay;
    }

    /**
     * Gets multiple lang.
     *
     * @return the multiple lang
     */
    public List<MultipleLanguageResult> getMultipleLang() {
        return multipleListResult;
    }

    /**
     * Sets multiple lang.
     *
     * @param MultipleLang the multiple lang
     */
    public void setMultipleLang(List<MultipleLanguageResult> MultipleLang) {
        this.multipleListResult = MultipleLang;
    }

    /**
     * Gets zopim id.
     *
     * @return the zopim id
     */
    public String getZopimId() {
        return zopimId;
    }

    /**
     * Sets zopim id.
     *
     * @param zopimId the zopim id
     */
    public void setZopimId(String zopimId) {
        this.zopimId = zopimId;
    }

    /**
     * Gets coupon section display.
     *
     * @return the coupon section display
     */
    public String getCouponSectionDisplay() {
        return couponSectionDisplay;
    }

    /**
     * Gets product setting version.
     *
     * @return the product setting version
     */
    public String getProductSettingVersion() {
        return productSettingVersion;
    }

    /**
     * Sets product setting version.
     *
     * @param productSettingVersion the product setting version
     */
    public void setProductSettingVersion(String productSettingVersion) {
        this.productSettingVersion = productSettingVersion;
    }

    /**
     * Sets coupon section display.
     *
     * @param couponSectionDisplay the coupon section display
     */
    public void setCouponSectionDisplay(String couponSectionDisplay) {
        this.couponSectionDisplay = couponSectionDisplay;
    }



    public Boolean getSearchBelowHeader() {
        return mSearchBelowheader;
    }


    public void setSearchBelowHeader(Boolean mSearchBelowheader) {
        this.mSearchBelowheader = mSearchBelowheader;
    }

    /**
     * Gets theme color 1.
     *
     * @return the theme color 1
     */
    public String getThemeColor1() {
        return themeColor1;
    }

    /**
     * Sets theme color 1.
     *
     * @param themeColor1 the theme color 1
     */
    public void setThemeColor1(String themeColor1) {
        this.themeColor1 = themeColor1;
    }

    /**
     * Gets text color.
     *
     * @return the text color
     */
    public String getTextColor() {
        return textColor;
    }

    /**
     * Sets text color.
     *
     * @param textColor the text color
     */
    public void setTextColor(String textColor) {
        this.textColor = textColor;
    }

    /**
     * Gets tint color.
     *
     * @return the tint color
     */
    public String getTintColor() {
        return tintColor;
    }

    /**
     * Sets tint color.
     *
     * @param tintColor the tint color
     */
    public void setTintColor(String tintColor) {
        this.tintColor = tintColor;
    }

    /**
     * Gets logo.
     *
     * @return The      logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * Sets logo.
     *
     * @param logo The logo
     */
    public void setLogo(String logo) {
        this.logo = logo;
    }

    /**
     * Gets categories.
     *
     * @return The      categories
     */
    public List<Category> getCategories() {
        return categories;
    }

    /**
     * Sets categories.
     *
     * @param categories The categories
     */
    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

//    /**
//     * Gets banner.
//     *
//     * @return The      banner
//     */
//    public List<Banner> getBanner() {
//        return banner;
//    }
//
//    /**
//     * Sets banner.
//     *
//     * @param banner The banner
//     */
//    public void setBanner(List<Banner> banner) {
//        this.banner = banner;
//    }
//
//    /**
//     * Gets cms list.
//     *
//     * @return The      cmsList
//     */


    public List<CmsPage> getCmsList() {
        return cmsList;
    }

    /**
     * Sets cms list.
     *
     * @param cmsList The cms_list
     */
    public void setCmsList(List<CmsPage> cmsList) {
        this.cmsList = cmsList;
    }

    /**
     * Gets cart count.
     *
     * @return The      cartCount
     */
    public String getCartCount() {
        return cartCount;
    }

    /**
     * Sets cart count.
     *
     * @param cartCount The cart_count
     */
    public void setCartCount(String cartCount) {
        this.cartCount = cartCount;
    }

    /**
     * Gets currency symbol.
     *
     * @return The      currencySymbol
     */
    public String getCurrencySymbol() {
        return currencySymbol;
    }

    /**
     * Sets currency symbol.
     *
     * @param currencySymbol The currency_symbol
     */
    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    /**
     * Gets catalog version.
     *
     * @return The      catalogVersion
     */
    public String getCatalogVersion() {
        return catalogVersion;
    }

    /**
     * Sets catalog version.
     *
     * @param catalogVersion The catalog_version
     */
    public void setCatalogVersion(String catalogVersion) {
        this.catalogVersion = catalogVersion;
    }

    /**
     * Gets homepage version.
     *
     * @return The      homepageVersion
     */
    public String getHomepageVersion() {
        return homepageVersion;
    }

    /**
     * Sets homepage version.
     *
     * @param homepageVersion The homepage_version
     */
    public void setHomepageVersion(String homepageVersion) {
        this.homepageVersion = homepageVersion;
    }

    /**
     * Gets languages.
     *
     * @return The      languages
     */
    public List<Language> getLanguages() {
        return languages;
    }

    /**
     * Sets languages.
     *
     * @param languages The languages
     */
    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    /**
     * Gets currency.
     *
     * @return The      currency
     */
    public List<Currency> getCurrency() {
        return currency;
    }

    /**
     * Sets currency.
     *
     * @param currency The currency
     */
    public void setCurrency(List<Currency> currency) {
        this.currency = currency;
    }

    /**
     * Gets tax display.
     *
     * @return The      taxDisplay
     */
    public String getTaxDisplay() {
        return taxDisplay;
    }

    /**
     * Sets tax display.
     *
     * @param taxDisplay The tax_display
     */
    public void setTaxDisplay(String taxDisplay) {
        this.taxDisplay = taxDisplay;
    }

    /**
     * Gets default lang.
     *
     * @return The      defaultLang
     */
    public String getDefaultLang() {
        return defaultLang;
    }

    /**
     * Sets default lang.
     *
     * @param defaultLang The default_lang
     */
    public void setDefaultLang(String defaultLang) {
        this.defaultLang = defaultLang;
    }

    /**
     * Gets default currency.
     *
     * @return The      defaultCurrency
     */
    public String getDefaultCurrency() {
        return defaultCurrency;
    }

    /**
     * Sets default currency.
     *
     * @param defaultCurrency The default_currency
     */
    public void setDefaultCurrency(String defaultCurrency) {
        this.defaultCurrency = defaultCurrency;
    }

    /**
     * Gets google tracking.
     *
     * @return The      googleTracking
     */
    public String getGoogleTracking() {
        return googleTracking;
    }

    /**
     * Sets google tracking.
     *
     * @param googleTracking The google_tracking
     */
    public void setGoogleTracking(String googleTracking) {
        this.googleTracking = googleTracking;
    }

    /**
     * Gets paypal client id.
     *
     * @return The      paypalClientId
     */
    public String getPaypalClientId() {
        return paypalClientId;
    }

    /**
     * Sets paypal client id.
     *
     * @param paypalClientId The paypal_client_id
     */
    public void setPaypalClientId(String paypalClientId) {
        this.paypalClientId = paypalClientId;
    }

    /**
     * Gets paypal secret key.
     *
     * @return The      paypalSecretKey
     */
    public String getPaypalSecretKey() {
        return paypalSecretKey;
    }

    /**
     * Sets paypal secret key.
     *
     * @param paypalSecretKey The paypal_secret_key
     */
    public void setPaypalSecretKey(String paypalSecretKey) {
        this.paypalSecretKey = paypalSecretKey;
    }

    /**
     * Gets paypal mode.
     *
     * @return The      paypalMode
     */
    public String getPaypalMode() {
        return paypalMode;
    }

    /**
     * Sets paypal mode.
     *
     * @param paypalMode The paypal_mode
     */
    public void setPaypalMode(String paypalMode) {
        this.paypalMode = paypalMode;
    }

    /**
     * Gets theme color.
     *
     * @return The      mThemeColor
     */
    public String getThemeColor() {
        return themeColor;
    }

    /**
     * Sets theme color.
     *
     * @param themeColor The theme_color
     */
    public void setThemeColor(String themeColor) {
        this.themeColor = themeColor;
    }

    /**
     * Gets guest checkout status.
     *
     * @return The      guestCheckoutStatus
     */
    public String getGuestCheckoutStatus() {
        return guestCheckoutStatus;
    }

    /**
     * Sets guest checkout status.
     *
     * @param guestCheckoutStatus The guest_checkout_status
     */
    public void setGuestCheckoutStatus(String guestCheckoutStatus) {
        this.guestCheckoutStatus = guestCheckoutStatus;
    }

    /**
     * Gets product label status.
     *
     * @return The      productLabelStatus
     */
    public String getProductLabelStatus() {
        return productLabelStatus;
    }

    /**
     * Sets product label status.
     *
     * @param productLabelStatus The product_label_status
     */
    public void setProductLabelStatus(String productLabelStatus) {
        this.productLabelStatus = productLabelStatus;
    }

    /**
     * Gets wishlist enable status.
     *
     * @return The      wishlistEnableStatus
     */
    public String getWishlistEnableStatus() {
        return wishlistEnableStatus;
    }

    /**
     * Sets wishlist enable status.
     *
     * @param wishlistEnableStatus The wishlist_enable_status
     */
    public void setWishlistEnableStatus(String wishlistEnableStatus) {
        this.wishlistEnableStatus = wishlistEnableStatus;
    }

    /**
     * Gets filter enable status.
     *
     * @return The      filterEnableStatus
     */
    public String getFilterEnableStatus() {
        return filterEnableStatus;
    }

    /**
     * Sets filter enable status.
     *
     * @param filterEnableStatus The filter_enable_status
     */
    public void setFilterEnableStatus(String filterEnableStatus) {
        this.filterEnableStatus = filterEnableStatus;
    }

    /**
     * Gets stock management status.
     *
     * @return The      stockManagementStatus
     */
    public String getStockManagementStatus() {
        return stockManagementStatus;
    }

    /**
     * Sets stock management status.
     *
     * @param stockManagementStatus The stock_management_status
     */
    public void setStockManagementStatus(String stockManagementStatus) {
        this.stockManagementStatus = stockManagementStatus;
    }

    /**
     * Gets review rating status.
     *
     * @return The      reviewRatingStatus
     */
    public String getReviewRatingStatus() {
        return reviewRatingStatus;
    }

    /**
     * Sets review rating status.
     *
     * @param reviewRatingStatus The review_rating_status
     */
    public void setReviewRatingStatus(String reviewRatingStatus) {
        this.reviewRatingStatus = reviewRatingStatus;
    }

    /**
     * Gets qrcode scanner status.
     *
     * @return The      qrcodeScannerStatus
     */
    public String getQrcodeScannerStatus() {
        return qrcodeScannerStatus;
    }

    /**
     * Sets qrcode scanner status.
     *
     * @param qrcodeScannerStatus The qrcode_scanner_status
     */
    public void setQrcodeScannerStatus(String qrcodeScannerStatus) {
        this.qrcodeScannerStatus = qrcodeScannerStatus;
    }

    /**
     * Gets store locatore status.
     *
     * @return The      storeLocatoreStatus
     */
    public String getStoreLocatoreStatus() {
        return storeLocatoreStatus;
    }

    /**
     * Sets store locatore status.
     *
     * @param storeLocatoreStatus The store_locatore_status
     */
    public void setStoreLocatoreStatus(String storeLocatoreStatus) {
        this.storeLocatoreStatus = storeLocatoreStatus;
    }

    /**
     * Gets reorder status.
     *
     * @return The      reorderStatus
     */
    public String getReorderStatus() {
        return reorderStatus;
    }

    /**
     * Sets reorder status.
     *
     * @param reorderStatus The reorder_status
     */
    public void setReorderStatus(String reorderStatus) {
        this.reorderStatus = reorderStatus;
    }

    /**
     * Gets call back number.
     *
     * @return The      callBackNumber
     */
    public String getCallBackNumber() {
        return callBackNumber;
    }

    /**
     * Sets call back number.
     *
     * @param callBackNumber The call_back_number
     */
    public void setCallBackNumber(String callBackNumber) {
        this.callBackNumber = callBackNumber;
    }

    /**
     * Gets home page list.
     *
     * @return The      homePageList
     */
    public List<HomePageList> getHomePageList() {
        return homePageList;
    }

    /**
     * Sets home page list.
     *
     * @param homePageList The home_page_list
     */
    public void setHomePageList(List<HomePageList> homePageList) {
        this.homePageList = homePageList;
    }

    /**
     * Gets product list layout.
     *
     * @return the product list layout
     */
    public String getProductListLayout() {
        return productListLayout;
    }

    /**
     * Sets product list layout.
     *
     * @param productListLayout the product list layout
     */
    public void setProductListLayout(String productListLayout) {
        this.productListLayout = productListLayout;
    }

    public List<MagentoProductLabel> getMagentoProductLabel() {
        return magentoProductLabel;
    }

    public void setMagentoProductLabel(ArrayList<MagentoProductLabel> magentoProductLabel) {
        this.magentoProductLabel = magentoProductLabel;
    }
}
