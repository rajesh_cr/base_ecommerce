
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * <h1>Related Custom Attribute!</h1>
 * The Related Custom Attribute is used to implements getter and setter for Related Custom Attribute response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class RelatedCustomAttribute {

    @SerializedName("id_product_attribute")
    @Expose
    private String idProductAttribute;
    @SerializedName("attribute_combination")
    @Expose
    private List<String> attributeCombination = null;
    @SerializedName("special_price")
    @Expose
    private String specialPrice;
    @SerializedName("price")
    @Expose
    private String price;

    /**
     * Gets id product attribute.
     *
     * @return The      idProductAttribute
     */
    public String getIdProductAttribute() {
        return idProductAttribute;
    }

    /**
     * Sets id product attribute.
     *
     * @param idProductAttribute The id_product_attribute
     */
    public void setIdProductAttribute(String idProductAttribute) {
        this.idProductAttribute = idProductAttribute;
    }

    /**
     * Gets attribute combination.
     *
     * @return The      attributeCombination
     */
    public List<String> getAttributeCombination() {
        return attributeCombination;
    }

    /**
     * Sets attribute combination.
     *
     * @param attributeCombination The attribute_combination
     */
    public void setAttributeCombination(List<String> attributeCombination) {
        this.attributeCombination = attributeCombination;
    }

    /**
     * Gets special price.
     *
     * @return The      specialPrice
     */
    public String getSpecialPrice() {
        return specialPrice;
    }

    /**
     * Sets special price.
     *
     * @param specialPrice The special_price
     */
    public void setSpecialPrice(String specialPrice) {
        this.specialPrice = specialPrice;
    }

    /**
     * Gets price.
     *
     * @return The      price
     */
    public String getPrice() {
        return price;
    }

    /**
     * Sets price.
     *
     * @param price The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

}
