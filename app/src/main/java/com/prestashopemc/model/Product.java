
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

import java.util.ArrayList;
import java.util.List;


/**
 * <h1>Product!</h1>
 * The Product is used to implements getter and setter for Product response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class Product {

    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    @SerializedName("product_count")
    @Expose
    private Integer productCount;

    @SerializedName("category_products")
    @Expose
    private List<CategoryProduct> categoryProducts = new ArrayList<CategoryProduct>();


    @DatabaseField
    @Expose
    private String categoryId;

    @DatabaseField
    @Expose
    private String currentPage;

    @DatabaseField
    @Expose
    private String customerId;

    /**
     * Gets customer id.
     *
     * @return the customer id
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Sets customer id.
     *
     * @param customerId the customer id
     */
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

//    @SerializedName("category_products")
//    @ForeignCollectionField (columnName = "categoryProduct")
//    Collection<CategoryProduct> categoryProduct;


    /**
     * Gets product count.
     *
     * @return The      productCount
     */
    public Integer getProductCount() {
        return productCount;
    }

    /**
     * Sets product count.
     *
     * @param productCount The product_count
     */
    public void setProductCount(Integer productCount) {
        this.productCount = productCount;
    }

    /**
     * Gets category products.
     *
     * @return The      categoryProducts
     */
    public List<CategoryProduct> getCategoryProducts() {
        return categoryProducts;
    }

    /**
     * Sets category products.
     *
     * @param categoryProducts The category_products
     */
    public void setCategoryProducts(List<CategoryProduct> categoryProducts) {
        this.categoryProducts = categoryProducts;
    }
//
//    public Collection<CategoryProduct> getCategoryProduct() {
//        return categoryProduct;
//    }
//
//    public void setCategoryProduct(Collection<CategoryProduct> categoryProduct) {
//        this.categoryProduct = categoryProduct;
//    }


    /**
     * Gets category id.
     *
     * @return the category id
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * Sets category id.
     *
     * @param categoryId the category id
     */
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * Gets current page.
     *
     * @return the current page
     */
    public String getCurrentPage() {
        return currentPage;
    }

    /**
     * Sets current page.
     *
     * @param currentPage the current page
     */
    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }
}
