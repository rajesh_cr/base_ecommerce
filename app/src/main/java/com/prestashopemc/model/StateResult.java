
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>State Result!</h1>
 * The State Result is used to implements getter and setter for State Result response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class StateResult {

    @SerializedName("region_id")
    @Expose
    private String regionId;
    @SerializedName("default_name")
    @Expose
    private String defaultName;

    /**
     * Gets region id.
     *
     * @return The      regionId
     */
    public String getRegionId() {
        return regionId;
    }

    /**
     * Sets region id.
     *
     * @param regionId The region_id
     */
    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    /**
     * Gets default name.
     *
     * @return The      defaultName
     */
    public String getDefaultName() {
        return defaultName;
    }

    /**
     * Sets default name.
     *
     * @param defaultName The default_name
     */
    public void setDefaultName(String defaultName) {
        this.defaultName = defaultName;
    }

    @Override
    public String toString() {
        return defaultName;
    }
}
