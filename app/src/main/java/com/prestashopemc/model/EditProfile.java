
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * <h1>Edit Profile!</h1>
 * The Edit Profile is used to implements getter and setter for Edit Profile response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class EditProfile {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("customerdetails")
    @Expose
    private Customerdetails customerdetails;
    @SerializedName("sessionid")
    @Expose
    private Integer sessionid;
    @SerializedName("errormsg")
    @Expose
    private String errormsg;

    /**
     * Gets status.
     *
     * @return the status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status the status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * Gets customerdetails.
     *
     * @return the customerdetails
     */
    public Customerdetails getCustomerdetails() {
        return customerdetails;
    }

    /**
     * Sets customerdetails.
     *
     * @param customerdetails the customerdetails
     */
    public void setCustomerdetails(Customerdetails customerdetails) {
        this.customerdetails = customerdetails;
    }

    /**
     * Gets sessionid.
     *
     * @return the sessionid
     */
    public Integer getSessionid() {
        return sessionid;
    }

    /**
     * Sets sessionid.
     *
     * @param sessionid the sessionid
     */
    public void setSessionid(Integer sessionid) {
        this.sessionid = sessionid;
    }

}
