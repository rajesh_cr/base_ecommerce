
package com.prestashopemc.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NavigationFilterMain {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("result")
    @Expose
    private List<NavigationFilterResult> result = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<NavigationFilterResult> getResult() {
        return result;
    }

    public void setResult(List<NavigationFilterResult> result) {
        this.result = result;
    }

}
