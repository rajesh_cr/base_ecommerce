
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>Country Result!</h1>
 * The Country Result is used to implements getter and setter for Country Result response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class CountryResult {

    @SerializedName("country_id")
    @Expose
    private String countryId;
    @SerializedName("name")
    @Expose
    private String name;

    /**
     * Gets country id.
     *
     * @return The      countryId
     */
    public String getCountryId() {
        return countryId;
    }

    /**
     * Sets country id.
     *
     * @param countryId The country_id
     */
    public void setCountryId(String countryId) {
        this.countryId = countryId;
    }

    /**
     * Gets name.
     *
     * @return The      name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
