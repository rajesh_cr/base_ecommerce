
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>Order Confirm Result!</h1>
 * The Order Confirm Result is used to implements getter and setter for Order Confirm Result response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class OrderConfirmResult {

    @SerializedName("grandtotal")
    @Expose
    private String grandtotal;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("orderid")
    @Expose
    private String orderid;
    @SerializedName("order_detail")
    @Expose
    private OrderDetail orderDetail;

    @SerializedName("cheque_detail")
    @Expose
    private ChequeDetail chequeDetail;

    /**
     * Gets grandtotal.
     *
     * @return The grandtotal
     */
    public String getGrandtotal() {
        return grandtotal;
    }

    /**
     * Sets grandtotal.
     *
     * @param grandtotal The grandtotal
     */
    public void setGrandtotal(String grandtotal) {
        this.grandtotal = grandtotal;
    }

    /**
     * Gets code.
     *
     * @return The code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param code The code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets orderid.
     *
     * @return The orderid
     */
    public String getOrderid() {
        return orderid;
    }

    /**
     * Sets orderid.
     *
     * @param orderid The orderid
     */
    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    /**
     * Gets order detail.
     *
     * @return The orderDetail
     */
    public OrderDetail getOrderDetail() {
        return orderDetail;
    }

    /**
     * Sets order detail.
     *
     * @param orderDetail The order_detail
     */
    public void setOrderDetail(OrderDetail orderDetail) {
        this.orderDetail = orderDetail;
    }

    /**
     * Gets cheque detail.
     *
     * @return the cheque detail
     */
    public ChequeDetail getChequeDetail() {
        return chequeDetail;
    }

    /**
     * Sets cheque detail.
     *
     * @param chequeDetail the cheque detail
     */
    public void setChequeDetail(ChequeDetail chequeDetail) {
        this.chequeDetail = chequeDetail;
    }
}