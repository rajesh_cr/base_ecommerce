
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Confirm List!</h1>
 * The Confirm List is used to implements getter and setter for Confirm List response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class ConfirmList {

    @SerializedName("sub_total")
    @Expose
    private String subTotal;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("shipping_amount")
    @Expose
    private String shippingAmount;
    @SerializedName("discount_amount")
    @Expose
    private String discountAmount;
    @SerializedName("tax_amount")
    @Expose
    private String taxAmount;
    @SerializedName("product")
    @Expose
    private List<ConfirmProduct> product = new ArrayList<ConfirmProduct>();

    /**
     * Gets sub total.
     *
     * @return The      subTotal
     */
    public String getSubTotal() {
        return subTotal;
    }

    /**
     * Sets sub total.
     *
     * @param subTotal The sub_total
     */
    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    /**
     * Gets total.
     *
     * @return The      total
     */
    public String getTotal() {
        return total;
    }

    /**
     * Sets total.
     *
     * @param total The total
     */
    public void setTotal(String total) {
        this.total = total;
    }

    /**
     * Gets shipping amount.
     *
     * @return The      shippingAmount
     */
    public String getShippingAmount() {
        return shippingAmount;
    }

    /**
     * Sets shipping amount.
     *
     * @param shippingAmount The shipping_amount
     */
    public void setShippingAmount(String shippingAmount) {
        this.shippingAmount = shippingAmount;
    }

    /**
     * Gets discount amount.
     *
     * @return The      discountAmount
     */
    public String getDiscountAmount() {
        return discountAmount;
    }

    /**
     * Sets discount amount.
     *
     * @param discountAmount The discount_amount
     */
    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    /**
     * Gets tax amount.
     *
     * @return The      taxAmount
     */
    public String getTaxAmount() {
        return taxAmount;
    }

    /**
     * Sets tax amount.
     *
     * @param taxAmount The tax_amount
     */
    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }

    /**
     * Gets product.
     *
     * @return The      product
     */
    public List<ConfirmProduct> getProduct() {
        return product;
    }

    /**
     * Sets product.
     *
     * @param product The product
     */
    public void setProduct(List<ConfirmProduct> product) {
        this.product = product;
    }

}
