
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Payment Result!</h1>
 * The Payment Result is used to implements getter and setter for Payment Result response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class PaymentResult {

    @SerializedName("payment_method")
    @Expose
    private List<PaymentMethod> paymentMethod = new ArrayList<PaymentMethod>();
    @SerializedName("shipping_method")
    @Expose
    private List<PaymentShippingMethod> shippingMethod = new ArrayList<PaymentShippingMethod>();
    @SerializedName("prices")
    @Expose
    private PaymentPrices prices;

    /**
     * Gets payment method.
     *
     * @return The      paymentMethod
     */
    public List<PaymentMethod> getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * Sets payment method.
     *
     * @param paymentMethod The payment_method
     */
    public void setPaymentMethod(List<PaymentMethod> paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    /**
     * Gets shipping method.
     *
     * @return The      shippingMethod
     */
    public List<PaymentShippingMethod> getShippingMethod() {
        return shippingMethod;
    }

    /**
     * Sets shipping method.
     *
     * @param shippingMethod The shipping_method
     */
    public void setShippingMethod(List<PaymentShippingMethod> shippingMethod) {
        this.shippingMethod = shippingMethod;
    }

    /**
     * Gets prices.
     *
     * @return The      prices
     */
    public PaymentPrices getPrices() {
        return prices;
    }

    /**
     * Sets prices.
     *
     * @param prices The prices
     */
    public void setPrices(PaymentPrices prices) {
        this.prices = prices;
    }

}
