
package com.prestashopemc.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MagentoReviewResult {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("detail")
    @Expose
    private String detail;
    @SerializedName("productname")
    @Expose
    private String productname;
    @SerializedName("id_product")
    @Expose
    private String idProduct;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("created_at")
    @Expose
    private String createdAt;
    @SerializedName("ratings")
    @Expose
    private List<MagentoReviewRating> ratings = null;
    @SerializedName("rating_available")
    @Expose
    private Integer ratingAvailable;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public List<MagentoReviewRating> getRatings() {
        return ratings;
    }

    public void setRatings(List<MagentoReviewRating> ratings) {
        this.ratings = ratings;
    }

    public Integer getRatingAvailable() {
        return ratingAvailable;
    }

    public void setRatingAvailable(Integer ratingAvailable) {
        this.ratingAvailable = ratingAvailable;
    }

}
