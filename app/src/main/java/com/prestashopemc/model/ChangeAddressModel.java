
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>Change Address Model!</h1>
 * The Change Address Model is used to implements getter and setter for Change Address Model response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class ChangeAddressModel {

    @SerializedName("alias")
    @Expose
    private String alias;
    @SerializedName("id_address")
    @Expose
    private String idAddress;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("street")
    @Expose
    private String street;
    @SerializedName("postcode")
    @Expose
    private String postcode;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("region")
    @Expose
    private String region;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("telephone")
    @Expose
    private String telephone;
    @SerializedName("is_default_address")
    @Expose
    private boolean isDefaultAddress;

    public boolean isDefaultAddress() {
        return isDefaultAddress;
    }

    public void setDefaultAddress(boolean defaultAddress) {
        isDefaultAddress = defaultAddress;
    }

    /**
     * Gets alias.
     *
     * @return The      alias
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Sets alias.
     *
     * @param alias The alias
     */
    public void setAlias(String alias) {
        this.alias = alias;
    }

    /**
     * Gets id address.
     *
     * @return The      idAddress
     */
    public String getIdAddress() {
        return idAddress;
    }

    /**
     * Sets id address.
     *
     * @param idAddress The id_address
     */
    public void setIdAddress(String idAddress) {
        this.idAddress = idAddress;
    }

    /**
     * Gets firstname.
     *
     * @return The      firstname
     */
    public String getFirstname() {
        return firstname;
    }

    /**
     * Sets firstname.
     *
     * @param firstname The firstname
     */
    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    /**
     * Gets lastname.
     *
     * @return The      lastname
     */
    public String getLastname() {
        return lastname;
    }

    /**
     * Sets lastname.
     *
     * @param lastname The lastname
     */
    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    /**
     * Gets street.
     *
     * @return The      street
     */
    public String getStreet() {
        return street;
    }

    /**
     * Sets street.
     *
     * @param street The street
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * Gets postcode.
     *
     * @return The      postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * Sets postcode.
     *
     * @param postcode The postcode
     */
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    /**
     * Gets city.
     *
     * @return The      city
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets city.
     *
     * @param city The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Gets region.
     *
     * @return The      region
     */
    public String getRegion() {
        return region;
    }

    /**
     * Sets region.
     *
     * @param region The region
     */
    public void setRegion(String region) {
        this.region = region;
    }

    /**
     * Gets country.
     *
     * @return The      country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets country.
     *
     * @param country The country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Gets telephone.
     *
     * @return The      telephone
     */
    public String getTelephone() {
        return telephone;
    }

    /**
     * Sets telephone.
     *
     * @param telephone The telephone
     */
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

}
