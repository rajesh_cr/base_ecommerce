
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import com.prestashopemc.webModel.ImageBorder;
import com.prestashopemc.webModel.ImageList;
import com.prestashopemc.webModel.SubTitle;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Home Page List!</h1>
 * The Home Page List is used to implements getter and setter for Home Page List response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

@DatabaseTable(tableName = "HomePageList")
public class HomePageList {

   /* @DatabaseField
    @SerializedName("banner_name")
    @Expose
    private String bannerName;

    @DatabaseField
    @SerializedName("slot_number")
    @Expose
    private String slotNumber;

    @DatabaseField
    @SerializedName("slot_position")
    @Expose
    private String slotPosition;

    @SerializedName("imagelist")
    @Expose
    private List<Imagelist> imagelist = new ArrayList<Imagelist>();

    *//**
     * @return The bannerName
     *//*
    public String getBannerName() {
        return bannerName;
    }

    *//**
     * @param bannerName The banner_name
     *//*
    public void setBannerName(String bannerName) {
        this.bannerName = bannerName;
    }

    *//**
     * @return The slotNumber
     *//*
    public String getSlotNumber() {
        return slotNumber;
    }

    *//**
     * @param slotNumber The slot_number
     *//*
    public void setSlotNumber(String slotNumber) {
        this.slotNumber = slotNumber;
    }

    *//**
     * @return The slotPosition
     *//*
    public String getSlotPosition() {
        return slotPosition;
    }

    *//**
     * @param slotPosition The slot_position
     *//*
    public void setSlotPosition(String slotPosition) {
        this.slotPosition = slotPosition;
    }

    *//**
     * @return The imagelist
     *//*
    public List<Imagelist> getImagelist() {
        return imagelist;
    }

    *//**
     * @param imagelist The imagelist
     *//*
    public void setImagelist(List<Imagelist> imagelist) {
        this.imagelist = imagelist;
    }*/

    @DatabaseField
    @SerializedName("slottype")
    @Expose
    private String slottype;
    @DatabaseField
    @SerializedName("slotnumber")
    @Expose
    private Integer slotnumber;
    @DatabaseField
    @SerializedName("slotposition")
    @Expose
    private Integer slotposition;
    @DatabaseField
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("border")
    @Expose
    private ImageBorder border;
    @SerializedName("imagelist")
    @Expose
    private List<ImageList> imagelist = new ArrayList<>();
    @DatabaseField
    @SerializedName("textsize")
    @Expose
    private String textsize;
    @DatabaseField
    @SerializedName("is_italic")
    @Expose
    private Boolean isItalic;
    @SerializedName("title")
    @Expose
    private List<SubTitle> title = new ArrayList<>();
    @DatabaseField
    @SerializedName("is_underline")
    @Expose
    private Boolean isUnderline;
    @DatabaseField
    @SerializedName("texttype")
    @Expose
    private String texttype;
    @DatabaseField
    @SerializedName("textalign")
    @Expose
    private String textalign;
    @DatabaseField
    @SerializedName("textcolor")
    @Expose
    private String textcolor;
    @DatabaseField
    @SerializedName("background-color")
    @Expose
    private String backgroundColor;
    @DatabaseField
    @SerializedName("is_bold")
    @Expose
    private Boolean isBold;
    @DatabaseField
    @SerializedName("height")
    @Expose
    private String height;

    /**
     * Gets textsize.
     *
     * @return the textsize
     */
    public String getTextsize() {
        return textsize;
    }

    /**
     * Sets textsize.
     *
     * @param textsize the textsize
     */
    public void setTextsize(String textsize) {
        this.textsize = textsize;
    }

    /**
     * Gets italic.
     *
     * @return the italic
     */
    public Boolean getItalic() {
        return isItalic;
    }

    /**
     * Sets italic.
     *
     * @param italic the italic
     */
    public void setItalic(Boolean italic) {
        isItalic = italic;
    }

    /**
     * Gets title.
     *
     * @return the title
     */
    public List<SubTitle> getTitle() {
        return title;
    }

    /**
     * Sets title.
     *
     * @param title the title
     */
    public void setTitle(List<SubTitle> title) {
        this.title = title;
    }

    /**
     * Gets underline.
     *
     * @return the underline
     */
    public Boolean getUnderline() {
        return isUnderline;
    }

    /**
     * Sets underline.
     *
     * @param underline the underline
     */
    public void setUnderline(Boolean underline) {
        isUnderline = underline;
    }

    /**
     * Gets texttype.
     *
     * @return the texttype
     */
    public String getTexttype() {
        return texttype;
    }

    /**
     * Sets texttype.
     *
     * @param texttype the texttype
     */
    public void setTexttype(String texttype) {
        this.texttype = texttype;
    }

    /**
     * Gets textalign.
     *
     * @return the textalign
     */
    public String getTextalign() {
        return textalign;
    }

    /**
     * Sets textalign.
     *
     * @param textalign the textalign
     */
    public void setTextalign(String textalign) {
        this.textalign = textalign;
    }

    /**
     * Gets textcolor.
     *
     * @return the textcolor
     */
    public String getTextcolor() {
        return textcolor;
    }

    /**
     * Sets textcolor.
     *
     * @param textcolor the textcolor
     */
    public void setTextcolor(String textcolor) {
        this.textcolor = textcolor;
    }

    /**
     * Gets background color.
     *
     * @return the background color
     */
    public String getBackgroundColor() {
        return backgroundColor;
    }

    /**
     * Sets background color.
     *
     * @param backgroundColor the background color
     */
    public void setBackgroundColor(String backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    /**
     * Gets bold.
     *
     * @return the bold
     */
    public Boolean getBold() {
        return isBold;
    }

    /**
     * Sets bold.
     *
     * @param bold the bold
     */
    public void setBold(Boolean bold) {
        isBold = bold;
    }

    /**
     * Gets height.
     *
     * @return the height
     */
    public String getHeight() {
        return height;
    }

    /**
     * Sets height.
     *
     * @param height the height
     */
    public void setHeight(String height) {
        this.height = height;
    }

    /**
     * Gets slottype.
     *
     * @return the slottype
     */
    public String getSlottype() {
        return slottype;
    }

    /**
     * Sets slottype.
     *
     * @param slottype the slottype
     */
    public void setSlottype(String slottype) {
        this.slottype = slottype;
    }

    /**
     * Gets slotnumber.
     *
     * @return the slotnumber
     */
    public Integer getSlotnumber() {
        return slotnumber;
    }

    /**
     * Sets slotnumber.
     *
     * @param slotnumber the slotnumber
     */
    public void setSlotnumber(Integer slotnumber) {
        this.slotnumber = slotnumber;
    }

    /**
     * Gets slotposition.
     *
     * @return the slotposition
     */
    public Integer getSlotposition() {
        return slotposition;
    }

    /**
     * Sets slotposition.
     *
     * @param slotposition the slotposition
     */
    public void setSlotposition(Integer slotposition) {
        this.slotposition = slotposition;
    }

    /**
     * Gets imagelist.
     *
     * @return the imagelist
     */
    public List<ImageList> getImagelist() {
        return imagelist;
    }

    /**
     * Sets imagelist.
     *
     * @param imagelist the imagelist
     */
    public void setImagelist(List<ImageList> imagelist) {
        this.imagelist = imagelist;
    }

    /**
     * Gets border.
     *
     * @return the border
     */
    public ImageBorder getBorder() {
        return border;
    }

    /**
     * Sets border.
     *
     * @param border the border
     */
    public void setBorder(ImageBorder border) {
        this.border = border;
    }

    /**
     * Gets id.
     *
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(Integer id) {
        this.id = id;
    }

}
