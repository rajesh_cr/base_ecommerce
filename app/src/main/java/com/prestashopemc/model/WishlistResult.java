
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * <h1>Wish list Result!</h1>
 * The Wish list Result is used to implements getter and setter for Wish list Result response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class WishlistResult {

    @SerializedName("specialprice")
    @Expose
    private String specialprice;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("productid")
    @Expose
    private String productid;
    @SerializedName("description_short")
    @Expose
    private String description;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("id_product_attribute")
    @Expose
    private String idProductAttribute;
    @SerializedName("id_wishlist")
    @Expose
    private String idWishlist;
    @SerializedName("is_tax_display")
    @Expose
    private String isTaxDisplay;

    /**
     * Gets specialprice.
     *
     * @return The      specialprice
     */
    public String getSpecialprice() {
        return specialprice;
    }

    /**
     * Sets specialprice.
     *
     * @param specialprice The specialprice
     */
    public void setSpecialprice(String specialprice) {
        this.specialprice = specialprice;
    }

    /**
     * Gets price.
     *
     * @return The      price
     */
    public String getPrice() {
        return price;
    }

    /**
     * Sets price.
     *
     * @param price The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * Gets productid.
     *
     * @return The      productid
     */
    public String getProductid() {
        return productid;
    }

    /**
     * Sets productid.
     *
     * @param productid The productid
     */
    public void setProductid(String productid) {
        this.productid = productid;
    }

    /**
     * Gets quantity.
     *
     * @return The      quantity
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * Sets quantity.
     *
     * @param quantity The quantity
     */
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    /**
     * Gets name.
     *
     * @return The      name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets image.
     *
     * @return The      image
     */
    public String getImage() {
        return image;
    }

    /**
     * Sets image.
     *
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

    /**
     * Gets id product attribute.
     *
     * @return The      idProductAttribute
     */
    public String getIdProductAttribute() {
        return idProductAttribute;
    }

    /**
     * Sets id product attribute.
     *
     * @param idProductAttribute The id_product_attribute
     */
    public void setIdProductAttribute(String idProductAttribute) {
        this.idProductAttribute = idProductAttribute;
    }

    /**
     * Gets id wishlist.
     *
     * @return The      idWishlist
     */
    public String getIdWishlist() {
        return idWishlist;
    }

    /**
     * Sets id wishlist.
     *
     * @param idWishlist The id_wishlist
     */
    public void setIdWishlist(String idWishlist) {
        this.idWishlist = idWishlist;
    }

    /**
     * Gets is tax display.
     *
     * @return The      isTaxDisplay
     */
    public String getIsTaxDisplay() {
        return isTaxDisplay;
    }

    /**
     * Sets is tax display.
     *
     * @param isTaxDisplay The is_tax_display
     */
    public void setIsTaxDisplay(String isTaxDisplay) {
        this.isTaxDisplay = isTaxDisplay;
    }

    /**
     * Gets description.
     *
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description the description
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
