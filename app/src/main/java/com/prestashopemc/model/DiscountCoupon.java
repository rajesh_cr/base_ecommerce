
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>Discount Coupon!</h1>
 * The Discount Coupon is used to implements getter and setter for Discount Coupon response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class DiscountCoupon {

    @SerializedName("code")
    @Expose
    private String code;

    /**
     * Gets code.
     *
     * @return The      code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets code.
     *
     * @param code The code
     */
    public void setCode(String code) {
        this.code = code;
    }

}
