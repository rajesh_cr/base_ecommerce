
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Info!</h1>
 * The Info is used to implements getter and setter for Info response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class Info {

    /*@SerializedName("discount_coupon")
    @Expose
    private List<DiscountCoupon> discountCoupon = new ArrayList<DiscountCoupon>();*/
    @SerializedName("subtotal")
    @Expose
    private String subtotal;
    @SerializedName("shipping_amount")
    @Expose
    private String shippingAmount;
    @SerializedName("discount_amount")
    @Expose
    private String discountAmount;
    @SerializedName("total_price_without_tax")
    @Expose
    private String totalPriceWithoutTax;
    @SerializedName("grandtotal")
    @Expose
    private String grandtotal;
    @SerializedName("is_tax_dispay")
    @Expose
    private String isTaxDispay;
    @SerializedName("tax_amount")
    @Expose
    private String taxAmount;
    @SerializedName("productiteminfo")
    @Expose
    private List<Productiteminfo> productiteminfo = new ArrayList<Productiteminfo>();
    @SerializedName("cart_count")
    @Expose
    private Integer itemcount;
    @SerializedName("coupon_code")
    @Expose
    private String couponTitle;
    @SerializedName("coupon_id")
    @Expose
    private String couponId;

    public String getCouponTitle() {
        return couponTitle;
    }

    public void setCouponTitle(String couponTitle) {
        this.couponTitle = couponTitle;
    }

    public String getCouponId() {
        return couponId;
    }

    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    /**
     *
     * @return
     *     The discountCoupon
     */
    /*public List<DiscountCoupon> getDiscountCoupon() {
        return discountCoupon;
    }*/

    /**
     *
     * @param discountCoupon
     *     The discount_coupon
     */
   /* public void setDiscountCoupon(List<DiscountCoupon> discountCoupon) {
        this.discountCoupon = discountCoupon;
    }*/

    /**
     * Gets subtotal.
     *
     * @return The      subtotal
     */
    public String getSubtotal() {
        return subtotal;
    }

    /**
     * Sets subtotal.
     *
     * @param subtotal The subtotal
     */
    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    /**
     * Gets shipping amount.
     *
     * @return The      shippingAmount
     */
    public String getShippingAmount() {
        return shippingAmount;
    }

    /**
     * Sets shipping amount.
     *
     * @param shippingAmount The shipping_amount
     */
    public void setShippingAmount(String shippingAmount) {
        this.shippingAmount = shippingAmount;
    }

    /**
     * Gets discount amount.
     *
     * @return The      discountAmount
     */
    public String getDiscountAmount() {
        return discountAmount;
    }

    /**
     * Sets discount amount.
     *
     * @param discountAmount The discount_amount
     */
    public void setDiscountAmount(String discountAmount) {
        this.discountAmount = discountAmount;
    }

    /**
     * Gets total price without tax.
     *
     * @return The      totalPriceWithoutTax
     */
    public String getTotalPriceWithoutTax() {
        return totalPriceWithoutTax;
    }

    /**
     * Sets total price without tax.
     *
     * @param totalPriceWithoutTax The total_price_without_tax
     */
    public void setTotalPriceWithoutTax(String totalPriceWithoutTax) {
        this.totalPriceWithoutTax = totalPriceWithoutTax;
    }

    /**
     * Gets grandtotal.
     *
     * @return The      grandtotal
     */
    public String getGrandtotal() {
        return grandtotal;
    }

    /**
     * Sets grandtotal.
     *
     * @param grandtotal The grandtotal
     */
    public void setGrandtotal(String grandtotal) {
        this.grandtotal = grandtotal;
    }

    /**
     * Gets is tax dispay.
     *
     * @return The      isTaxDispay
     */
    public String getIsTaxDispay() {
        return isTaxDispay;
    }

    /**
     * Sets is tax dispay.
     *
     * @param isTaxDispay The is_tax_dispay
     */
    public void setIsTaxDispay(String isTaxDispay) {
        this.isTaxDispay = isTaxDispay;
    }

    /**
     * Gets tax amount.
     *
     * @return The      taxAmount
     */
    public String getTaxAmount() {
        return taxAmount;
    }

    /**
     * Sets tax amount.
     *
     * @param taxAmount The tax_amount
     */
    public void setTaxAmount(String taxAmount) {
        this.taxAmount = taxAmount;
    }

    /**
     * Gets productiteminfo.
     *
     * @return The      productiteminfo
     */
    public List<Productiteminfo> getProductiteminfo() {
        return productiteminfo;
    }

    /**
     * Sets productiteminfo.
     *
     * @param productiteminfo The productiteminfo
     */
    public void setProductiteminfo(List<Productiteminfo> productiteminfo) {
        this.productiteminfo = productiteminfo;
    }

    /**
     * Gets itemcount.
     *
     * @return The      itemcount
     */
    public Integer getItemcount() {
        return itemcount;
    }

    /**
     * Sets itemcount.
     *
     * @param itemcount The itemcount
     */
    public void setItemcount(Integer itemcount) {
        this.itemcount = itemcount;
    }

}
