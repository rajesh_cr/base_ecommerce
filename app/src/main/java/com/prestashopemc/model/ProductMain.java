
package com.prestashopemc.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>Product Main!</h1>
 * The Product Main is used to implements getter and setter for Product Main response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class ProductMain {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("result")
    @Expose
    private Product mProduct;

    @SerializedName("errormsg")
    @Expose
    private String mErrorMsg;


    /**
     * Gets status.
     *
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Gets product.
     *
     * @return the product
     */
    public Product getmProduct() {
        return mProduct;
    }

    /**
     * Sets product.
     *
     * @param mProduct the m product
     */
    public void setmProduct(Product mProduct) {
        this.mProduct = mProduct;
    }

    /**
     * Gets error msg.
     *
     * @return the error msg
     */
    public String getmErrorMsg() {
        return mErrorMsg;
    }

    /**
     * Sets error msg.
     *
     * @param mErrorMsg the m error msg
     */
    public void setmErrorMsg(String mErrorMsg) {
        this.mErrorMsg = mErrorMsg;
    }
}
