
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * <h1>Product info!</h1>
 * The Product info is used to implements getter and setter for Product info response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class Productinfo {
    @SerializedName("ribbon")
    @Expose
    private List<Ribbon> ribbon = null;
    @SerializedName("ribbon_enabled")
    @Expose
    private Boolean ribbonEnabled;
    @SerializedName("tax_dispay")
    @Expose
    private String taxDispay;
    @SerializedName("is_tax_dispay")
    @Expose
    private String isTaxDispay;
    @SerializedName("stock")
    @Expose
    private String stock;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("specialprice")
    @Expose
    private String specialprice;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("description_short")
    @Expose
    private String descriptionShort;
    @SerializedName("minimum_quantity")
    @Expose
    private String minimumQuantity;
    @SerializedName("quantity")
    @Expose
    private Integer quantity;
    @SerializedName("category")
    @Expose
    private String category;


    /**
     * Gets ribbon.
     *
     * @return the ribbon
     */
    public List<Ribbon> getRibbon() {
        return ribbon;
    }

    /**
     * Sets ribbon.
     *
     * @param ribbon the ribbon
     */
    public void setRibbon(List<Ribbon> ribbon) {
        this.ribbon = ribbon;
    }

    /**
     * Gets ribbon enabled.
     *
     * @return the ribbon enabled
     */
    public Boolean getRibbonEnabled() {
        return ribbonEnabled;
    }

    /**
     * Sets ribbon enabled.
     *
     * @param ribbonEnabled the ribbon enabled
     */
    public void setRibbonEnabled(Boolean ribbonEnabled) {
        this.ribbonEnabled = ribbonEnabled;
    }


    /**
     * Gets tax dispay.
     *
     * @return The      taxDispay
     */
    public String getTaxDispay() {
        return taxDispay;
    }

    /**
     * Sets tax dispay.
     *
     * @param taxDispay The tax_dispay
     */
    public void setTaxDispay(String taxDispay) {
        this.taxDispay = taxDispay;
    }

    /**
     * Gets is tax dispay.
     *
     * @return The      isTaxDispay
     */
    public String getIsTaxDispay() {
        return isTaxDispay;
    }

    /**
     * Sets is tax dispay.
     *
     * @param isTaxDispay The is_tax_dispay
     */
    public void setIsTaxDispay(String isTaxDispay) {
        this.isTaxDispay = isTaxDispay;
    }

    /**
     * Gets stock.
     *
     * @return The      stock
     */
    public String getStock() {
        return stock;
    }

    /**
     * Sets stock.
     *
     * @param stock The stock
     */
    public void setStock(String stock) {
        this.stock = stock;
    }

    /**
     * Gets price.
     *
     * @return The      price
     */
    public String getPrice() {
        return price;
    }

    /**
     * Sets price.
     *
     * @param price The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * Gets specialprice.
     *
     * @return The      specialprice
     */
    public String getSpecialprice() {
        return specialprice;
    }

    /**
     * Sets specialprice.
     *
     * @param specialprice The specialprice
     */
    public void setSpecialprice(String specialprice) {
        this.specialprice = specialprice;
    }

    /**
     * Gets name.
     *
     * @return The      name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets description.
     *
     * @return The      description
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets description.
     *
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * Gets description short.
     *
     * @return The      descriptionShort
     */
    public String getDescriptionShort() {
        return descriptionShort;
    }

    /**
     * Sets description short.
     *
     * @param descriptionShort The description_short
     */
    public void setDescriptionShort(String descriptionShort) {
        this.descriptionShort = descriptionShort;
    }

    /**
     * Gets minimum quantity.
     *
     * @return The      minimumQuantity
     */
    public String getMinimumQuantity() {
        return minimumQuantity;
    }

    /**
     * Sets minimum quantity.
     *
     * @param minimumQuantity The minimum_quantity
     */
    public void setMinimumQuantity(String minimumQuantity) {
        this.minimumQuantity = minimumQuantity;
    }

    /**
     * Gets quantity.
     *
     * @return The      quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * Sets quantity.
     *
     * @param quantity The quantity
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * Gets category.
     *
     * @return the category
     */
    public String getCategory() {
        return category;
    }

    /**
     * Sets category.
     *
     * @param category the category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    @Override
    public String toString() {


        return "ribbonEnabled :" + ribbonEnabled;
    }
}
