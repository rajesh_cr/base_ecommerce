package com.prestashopemc.model;

/**
 * <h1>Address Values!</h1>
 * The Address Values is used to implements getter and setter for Address Values response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class AddressValues {

    /**
     * The Fname.
     */
    String fname;
    /**
     * The Lname.
     */
    String lname;
    /**
     * The Email.
     */
    String email;
    /**
     * The Phone.
     */
    String phone;
    /**
     * The Street.
     */
    String street;
    /**
     * The City.
     */
    String city;
    /**
     * The State.
     */
    String state;
    /**
     * The Country.
     */
    String country;
    /**
     * The Zipcode.
     */
    String zipcode;
    /**
     * The State id.
     */
    String state_id;
    /**
     * The Is checked.
     */
    boolean isChecked;

    /**
     * Gets state id.
     *
     * @return the state id
     */
    public String getState_id() {
        return state_id;
    }

    /**
     * Sets state id.
     *
     * @param state_id the state id
     */
    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    /**
     * Gets fname.
     *
     * @return the fname
     */
    public String getFname() {
        return fname;
    }

    /**
     * Sets fname.
     *
     * @param fname the fname
     */
    public void setFname(String fname) {
        this.fname = fname;
    }

    /**
     * Gets lname.
     *
     * @return the lname
     */
    public String getLname() {
        return lname;
    }

    /**
     * Sets lname.
     *
     * @param lname the lname
     */
    public void setLname(String lname) {
        this.lname = lname;
    }

    /**
     * Gets phone.
     *
     * @return the phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Sets phone.
     *
     * @param phone the phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Gets street.
     *
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * Sets street.
     *
     * @param street the street
     */
    public void setStreet(String street) {
        this.street = street;
    }

    /**
     * Gets city.
     *
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets city.
     *
     * @param city the city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Gets state.
     *
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * Sets state.
     *
     * @param state the state
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * Gets country.
     *
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets country.
     *
     * @param country the country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Gets zipcode.
     *
     * @return the zipcode
     */
    public String getZipcode() {
        return zipcode;
    }

    /**
     * Sets zipcode.
     *
     * @param zipcode the zipcode
     */
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    /**
     * Is checked boolean.
     *
     * @return the boolean
     */
    public boolean isChecked() {
        return isChecked;
    }

    /**
     * Sets checked.
     *
     * @param isChecked the is checked
     */
    public void setChecked(boolean isChecked) {
        this.isChecked = isChecked;
    }
}
