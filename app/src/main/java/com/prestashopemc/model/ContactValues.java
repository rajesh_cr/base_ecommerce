package com.prestashopemc.model;

/**
 * <h1>Contact Values!</h1>
 * The Contact Values is used to implements getter and setter for Contact Values response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class ContactValues {
    private String contactName;
    private String contactEmail;
    private String contactPhone;
    private String contactComent;

    /**
     * Gets contact name.
     *
     * @return the contact name
     */
    public String getContactName() {
        return contactName;
    }

    /**
     * Sets contact name.
     *
     * @param contactName the contact name
     */
    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    /**
     * Gets contact email.
     *
     * @return the contact email
     */
    public String getContactEmail() {
        return contactEmail;
    }

    /**
     * Sets contact email.
     *
     * @param contactEmail the contact email
     */
    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    /**
     * Gets contact phone.
     *
     * @return the contact phone
     */
    public String getContactPhone() {
        return contactPhone;
    }

    /**
     * Sets contact phone.
     *
     * @param contactPhone the contact phone
     */
    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    /**
     * Gets contact coment.
     *
     * @return the contact coment
     */
    public String getContactComent() {
        return contactComent;
    }

    /**
     * Sets contact coment.
     *
     * @param contactComent the contact coment
     */
    public void setContactComent(String contactComent) {
        this.contactComent = contactComent;
    }
}
