
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>Filter Option!</h1>
 * The Filter Option is used to implements getter and setter for Filter Option response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class FilterOption {

    @SerializedName("label")
    @Expose
    private String label;
    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("status")
    @Expose
    private String status;

    private String attributeId;

    /**
     * Gets label.
     *
     * @return The label
     */
    public String getLabel() {
        return label;
    }

    /**
     * Sets label.
     *
     * @param label The label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * Gets value.
     *
     * @return The value
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets value.
     *
     * @param value The value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets status.
     *
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Gets attribute id.
     *
     * @return the attribute id
     */
    public String getAttributeId() {
        return attributeId;
    }

    /**
     * Sets attribute id.
     *
     * @param attributeId the attribute id
     */
    public void setAttributeId(String attributeId) {
        this.attributeId = attributeId;
    }

    @Override
    public String toString() {
        return attributeId + value;
    }
}
