package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * <h1>Expand Category!</h1>
 * The Expand Category is used to implements getter and setter for Expand Category table.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

@DatabaseTable(tableName = "ExpandCategory")
public class ExpandCategory {

    @DatabaseField
    private String parentId;
    @SerializedName("id_category")
    @Expose
    @DatabaseField
    private String idCategory;
    @SerializedName("name")
    @Expose
    @DatabaseField
    private String name;
    @SerializedName("category_count")
    @Expose
    @DatabaseField
    private Integer categoryCount;

    @SerializedName("product_count")
    @Expose
    @DatabaseField
    private Integer productCount;

    /**
     * The M category.
     */
    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    Category mCategory;

    /**
     * Gets id category.
     *
     * @return the id category
     */
    public String getIdCategory() {
        return idCategory;
    }

    /**
     * Sets id category.
     *
     * @param idCategory the id category
     */
    public void setIdCategory(String idCategory) {
        this.idCategory = idCategory;
    }

    /**
     * Gets name.
     *
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name the name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets category count.
     *
     * @return the category count
     */
    public Integer getCategoryCount() {
        return categoryCount;
    }

    /**
     * Sets category count.
     *
     * @param categoryCount the category count
     */
    public void setCategoryCount(Integer categoryCount) {
        this.categoryCount = categoryCount;
    }

    /**
     * Gets product count.
     *
     * @return the product count
     */
    public Integer getProductCount() {
        return productCount;
    }

    /**
     * Sets product count.
     *
     * @param productCount the product count
     */
    public void setProductCount(Integer productCount) {
        this.productCount = productCount;
    }

    /**
     * Gets category.
     *
     * @return the category
     */
    public Category getmCategory() {
        return mCategory;
    }

    /**
     * Sets category.
     *
     * @param mCategory the m category
     */
    public void setmCategory(Category mCategory) {
        this.mCategory = mCategory;
    }


    /**
     * Gets parent id.
     *
     * @return the parent id
     */
    public String getParentId() {
        return parentId;
    }

    /**
     * Sets parent id.
     *
     * @param parentId the parent id
     */
    public void setParentId(String parentId) {
        this.parentId = parentId;
    }
}
