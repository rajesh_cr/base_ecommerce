
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MagentoDefaultAddressModel {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("result")
    @Expose
    private MagentoDefaultAddressResult result;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public MagentoDefaultAddressResult getResult() {
        return result;
    }

    public void setResult(MagentoDefaultAddressResult result) {
        this.result = result;
    }

}
