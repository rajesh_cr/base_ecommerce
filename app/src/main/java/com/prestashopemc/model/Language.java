
package com.prestashopemc.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;


/**
 * <h1>Language!</h1>
 * The Language is used to implements getter and setter for Language response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class Language {

    @SerializedName("id_lang")
    @Expose
    @DatabaseField
    private String idLang;
    @SerializedName("name")
    @Expose
    @DatabaseField
    private String name;
    @SerializedName("iso_code")
    @Expose
    @DatabaseField
    private String isoCode;
    @SerializedName("language_code")
    @Expose
    @DatabaseField
    private String languageCode;
    @SerializedName("active")
    @Expose
    @DatabaseField
    private String active;

    /**
     * Gets id lang.
     *
     * @return The      idLang
     */
    public String getIdLang() {
        return idLang;
    }

    /**
     * Sets id lang.
     *
     * @param idLang The id_lang
     */
    public void setIdLang(String idLang) {
        this.idLang = idLang;
    }

    /**
     * Gets name.
     *
     * @return The      name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets iso code.
     *
     * @return The      isoCode
     */
    public String getIsoCode() {
        return isoCode;
    }

    /**
     * Sets iso code.
     *
     * @param isoCode The iso_code
     */
    public void setIsoCode(String isoCode) {
        this.isoCode = isoCode;
    }

    /**
     * Gets language code.
     *
     * @return The      languageCode
     */
    public String getLanguageCode() {
        return languageCode;
    }

    /**
     * Sets language code.
     *
     * @param languageCode The language_code
     */
    public void setLanguageCode(String languageCode) {
        this.languageCode = languageCode;
    }

    /**
     * Gets active.
     *
     * @return The      active
     */
    public String getActive() {
        return active;
    }

    /**
     * Sets active.
     *
     * @param active The active
     */
    public void setActive(String active) {
        this.active = active;
    }

}
