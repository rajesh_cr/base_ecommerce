package com.prestashopemc.model;

/**
 * <h1>Register Values!</h1>
 * The Register Values is used to implements getter and setter for Register Values response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class RegisterValues {

    /**
     * The First name.
     */
    String firstName;
    /**
     * The Last name.
     */
    String lastName;
    /**
     * The Email.
     */
    String email;
    /**
     * The Password.
     */
    String password;
    /**
     * The Otp password.
     */
    String otpPassword;
    /**
     * The Android id.
     */
    String androidId;
    /**
     * The Mobile num.
     */
    String mobileNum;

    /**
     * Gets first name.
     *
     * @return the first name
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets first name.
     *
     * @param firstName the first name
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets last name.
     *
     * @return the last name
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets last name.
     *
     * @param lastName the last name
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets email.
     *
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets email.
     *
     * @param email the email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * Gets password.
     *
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets password.
     *
     * @param password the password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets otp password.
     *
     * @return the otp password
     */
    public String getOtpPassword() {
        return otpPassword;
    }

    /**
     * Sets otp password.
     *
     * @param otpPassword the otp password
     */
    public void setOtpPassword(String otpPassword) {
        this.otpPassword = otpPassword;
    }

    /**
     * Gets android id.
     *
     * @return the android id
     */
    public String getAndroidId() {
        return androidId;
    }

    /**
     * Sets android id.
     *
     * @param androidId the android id
     */
    public void setAndroidId(String androidId) {
        this.androidId = androidId;
    }

    /**
     * Gets mobile num.
     *
     * @return the mobile num
     */
    public String getMobileNum() {
        return mobileNum;
    }

    /**
     * Sets mobile num.
     *
     * @param mobileNum the mobile num
     */
    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }
}
