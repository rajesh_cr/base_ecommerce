package com.prestashopemc.model;

import com.j256.ormlite.field.DatabaseField;

/**
 * <h1>Key Attribute Value!</h1>
 * The Key Attribute Value is used to implements getter and setter for Key Attribute Value response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class KeyAttributeValue {

    @DatabaseField
    private String productId;

    @DatabaseField
    private String keyValue;

    /**
     * Gets product id.
     *
     * @return the product id
     */
    public String getProductId() {
        return productId;
    }

    /**
     * Sets product id.
     *
     * @param productId the product id
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * Gets key value.
     *
     * @return the key value
     */
    public String getKeyValue() {
        return keyValue;
    }

    /**
     * Sets key value.
     *
     * @param keyValue the key value
     */
    public void setKeyValue(String keyValue) {
        this.keyValue = keyValue;
    }
}
