
package com.prestashopemc.model;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>Update Version!</h1>
 * The Update Version is used to implements getter and setter for Update Version response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class UpdateVersion {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("result")
    @Expose
    private Version version;

    /**
     * Gets status.
     *
     * @return The      status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Gets version.
     *
     * @return The      result
     */
    public Version getVersion() {
        return version;
    }

    /**
     * Sets version.
     *
     * @param version The version
     */
    public void setVersion(Version version) {
        this.version = version;
    }

}
