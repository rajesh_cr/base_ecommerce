
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Category Product!</h1>
 * The Category Product is used to implements getter and setter for Category Product response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class CategoryProduct implements Serializable {
   // @DatabaseField(id = true, canBeNull = false, columnName = "id")
    @DatabaseField(generatedId = true)
    private int id;
    @SerializedName("ribbon")
    @Expose
    private List<Ribbon> ribbon = new ArrayList<Ribbon>();;
    @SerializedName("ribbon_enabled")
    @Expose
    @DatabaseField
    private Boolean ribbonEnabled;
    @SerializedName("id_product")
    @Expose
    @DatabaseField
    private String productId;
    @SerializedName("product_name")
    @Expose
    @DatabaseField
    private String productName;
    @SerializedName("description_short")
    @Expose
    @DatabaseField
    private String descriptionShort;
    @SerializedName("category")
    @Expose
    @DatabaseField
    private String category;
    @SerializedName("image_url")
    @Expose
    @DatabaseField
    private String imageUrl;
    @SerializedName("quantity")
    @Expose
    @DatabaseField
    private Integer quantity;
    @SerializedName("stock")
    @Expose
    @DatabaseField
    private String stock;
    @SerializedName("tax_dispay")
    @Expose
    @DatabaseField
    private String taxDispay;
    @SerializedName("is_tax_dispay")
    @Expose
    @DatabaseField
    private String isTaxDispay;
    @SerializedName("price")
    @Expose
    @DatabaseField
    private String price;
    @SerializedName("specialprice")
    @Expose
    @DatabaseField
    private String specialprice;
    @SerializedName("ribbon_image")
    @Expose
    @DatabaseField
    private String ribbonImage;
    @SerializedName("left_top")
    @Expose
    @DatabaseField
    private String leftTop;
    @SerializedName("left_bottom")
    @Expose
    @DatabaseField
    private String leftBottom;
    @SerializedName("right_top")
    @Expose
    @DatabaseField
    private String rightTop;
    @SerializedName("right_bottom")
    @Expose
    @DatabaseField
    private String rightBottom;
    @SerializedName("width")
    @Expose
    @DatabaseField
    private Integer width;
    @SerializedName("height")
    @Expose
    @DatabaseField
    private Integer height;
    @SerializedName("wishlist")
    @Expose
    @DatabaseField
    private Boolean wishlist;
    @SerializedName("is_config")
    @Expose
    @DatabaseField
    private Boolean isConfig;
    @SerializedName("is_combination")
    @Expose
    @DatabaseField
    private Boolean isCombination;

    @SerializedName("key_values")
    @Expose
    private List<KeyValues> keyValuesList = new ArrayList<KeyValues>();

 public int getId() {
  return id;
 }

 public void setId(int id) {
  this.id = id;
 }

 /**
     * Gets ribbon.
     *
     * @return the ribbon
     */
    public List<Ribbon> getRibbon() {
        return ribbon;
    }

    /**
     * Sets ribbon.
     *
     * @param ribbon the ribbon
     */
    public void setRibbon(List<Ribbon> ribbon) {
        this.ribbon = ribbon;
    }

    /**
     * Gets ribbon enabled.
     *
     * @return the ribbon enabled
     */
    public Boolean getRibbonEnabled() {
        return ribbonEnabled;
    }

    /**
     * Sets ribbon enabled.
     *
     * @param ribbonEnabled the ribbon enabled
     */
    public void setRibbonEnabled(Boolean ribbonEnabled) {
        this.ribbonEnabled = ribbonEnabled;
    }
    @SerializedName("custom_attributes")
    @Expose
    private List<CustomAttribute> customAttributes = new ArrayList<CustomAttribute>();

    @DatabaseField
    private String categoryId;

    @DatabaseField
    private String currentPage;

    @DatabaseField
    private String customerId;

    /**
     * Gets customer id.
     *
     * @return the customer id
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Sets customer id.
     *
     * @param customerId the customer id
     */
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    //    @SerializedName("custom_attributes")
//    @ForeignCollectionField
//    ForeignCollection<CustomAttribute> customAttributeCollection;
//
//    @DatabaseField(foreign=true, foreignAutoCreate=true, foreignAutoRefresh=true)
//    private Product mProduct;

    /**
     * Gets product id.
     *
     * @return The      productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     * Sets product id.
     *
     * @param productId The product_id
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * Gets product name.
     *
     * @return The      productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Sets product name.
     *
     * @param productName The product_name
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * Gets description short.
     *
     * @return The      descriptionShort
     */
    public String getDescriptionShort() {
        return descriptionShort;
    }

    /**
     * Sets description short.
     *
     * @param descriptionShort The description_short
     */
    public void setDescriptionShort(String descriptionShort) {
        this.descriptionShort = descriptionShort;
    }

    /**
     * Gets category.
     *
     * @return The      category
     */
    public String getCategory() {
        return category;
    }

    /**
     * Sets category.
     *
     * @param category The category
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * Gets image url.
     *
     * @return The      imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * Sets image url.
     *
     * @param imageUrl The image_url
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * Gets quantity.
     *
     * @return The      quantity
     */
    public Integer getQuantity() {
        return quantity;
    }

    /**
     * Sets quantity.
     *
     * @param quantity The quantity
     */
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    /**
     * Gets stock.
     *
     * @return The      stock
     */
    public String getStock() {
        return stock;
    }

    /**
     * Sets stock.
     *
     * @param stock The stock
     */
    public void setStock(String stock) {
        this.stock = stock;
    }

    /**
     * Gets tax dispay.
     *
     * @return The      taxDispay
     */
    public String getTaxDispay() {
        return taxDispay;
    }

    /**
     * Sets tax dispay.
     *
     * @param taxDispay The tax_dispay
     */
    public void setTaxDispay(String taxDispay) {
        this.taxDispay = taxDispay;
    }

    /**
     * Gets is tax dispay.
     *
     * @return The      isTaxDispay
     */
    public String getIsTaxDispay() {
        return isTaxDispay;
    }

    /**
     * Sets is tax dispay.
     *
     * @param isTaxDispay The is_tax_dispay
     */
    public void setIsTaxDispay(String isTaxDispay) {
        this.isTaxDispay = isTaxDispay;
    }

    /**
     * Gets price.
     *
     * @return The      price
     */
    public String getPrice() {
        return price;
    }

    /**
     * Sets price.
     *
     * @param price The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * Gets specialprice.
     *
     * @return The      specialprice
     */
    public String getSpecialprice() {
        return specialprice;
    }

    /**
     * Sets specialprice.
     *
     * @param specialprice The specialprice
     */
    public void setSpecialprice(String specialprice) {
        this.specialprice = specialprice;
    }

    /**
     * Gets ribbon image.
     *
     * @return The      ribbonImage
     */
    public String getRibbonImage() {
        return ribbonImage;
    }

    /**
     * Sets ribbon image.
     *
     * @param ribbonImage The ribbon_image
     */
    public void setRibbonImage(String ribbonImage) {
        this.ribbonImage = ribbonImage;
    }

    /**
     * Gets left top.
     *
     * @return The      leftTop
     */
    public String getLeftTop() {
        return leftTop;
    }

    /**
     * Sets left top.
     *
     * @param leftTop The left_top
     */
    public void setLeftTop(String leftTop) {
        this.leftTop = leftTop;
    }

    /**
     * Gets left bottom.
     *
     * @return The      leftBottom
     */
    public String getLeftBottom() {
        return leftBottom;
    }

    /**
     * Sets left bottom.
     *
     * @param leftBottom The left_bottom
     */
    public void setLeftBottom(String leftBottom) {
        this.leftBottom = leftBottom;
    }

    /**
     * Gets right top.
     *
     * @return The      rightTop
     */
    public String getRightTop() {
        return rightTop;
    }

    /**
     * Sets right top.
     *
     * @param rightTop The right_top
     */
    public void setRightTop(String rightTop) {
        this.rightTop = rightTop;
    }

    /**
     * Gets right bottom.
     *
     * @return The      rightBottom
     */
    public String getRightBottom() {
        return rightBottom;
    }

    /**
     * Sets right bottom.
     *
     * @param rightBottom The right_bottom
     */
    public void setRightBottom(String rightBottom) {
        this.rightBottom = rightBottom;
    }

    /**
     * Gets width.
     *
     * @return The      width
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * Sets width.
     *
     * @param width The width
     */
    public void setWidth(Integer width) {
        this.width = width;
    }

    /**
     * Gets height.
     *
     * @return The      height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * Sets height.
     *
     * @param height The height
     */
    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * Gets wishlist.
     *
     * @return The      wishlist
     */
    public Boolean getWishlist() {
        return wishlist;
    }

    /**
     * Sets wishlist.
     *
     * @param wishlist The wishlist
     */
    public void setWishlist(Boolean wishlist) {
        this.wishlist = wishlist;
    }

    /**
     * Gets is config.
     *
     * @return The      isConfig
     */
    public Boolean getIsConfig() {
        return isConfig;
    }

    /**
     * Sets is config.
     *
     * @param isConfig The is_config
     */
    public void setIsConfig(Boolean isConfig) {
        this.isConfig = isConfig;
    }

    /**
     * Gets is combination.
     *
     * @return The      isCombination
     */
    public Boolean getIsCombination() {
        return isCombination;
    }

    /**
     * Sets is combination.
     *
     * @param isCombination The is_combination
     */
    public void setIsCombination(Boolean isCombination) {
        this.isCombination = isCombination;
    }

    /**
     * Gets key values list.
     *
     * @return the key values list
     */
    public List<KeyValues> getKeyValuesList() {
        return keyValuesList;
    }

    /**
     * Sets key values list.
     *
     * @param keyValuesList the key values list
     */
    public void setKeyValuesList(List<KeyValues> keyValuesList) {
        this.keyValuesList = keyValuesList;
    }

    /**
     * Gets custom attributes.
     *
     * @return The      customAttributes
     */
    public List<CustomAttribute> getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets custom attributes.
     *
     * @param customAttributes The custom_attributes
     */
    public void setCustomAttributes(List<CustomAttribute> customAttributes) {
        this.customAttributes = customAttributes;
    }


//    public ForeignCollection<KeyValues> getKeyValuesCollection() {
//        return keyValuesCollection;
//    }
//
//    public void setKeyValuesCollection(ForeignCollection<KeyValues> keyValuesCollection) {
//        this.keyValuesCollection = keyValuesCollection;
//    }
//
//    public ForeignCollection<CustomAttribute> getCustomAttributeCollection() {
//        return customAttributeCollection;
//    }
//
//    public void setCustomAttributeCollection(ForeignCollection<CustomAttribute> customAttributeCollection) {
//        this.customAttributeCollection = customAttributeCollection;
//    }

//    public Product getmProduct() {
//        return mProduct;
//    }
//
//    public void setmProduct(Product mProduct) {
//        this.mProduct = mProduct;
//    }


    /**
     * Gets category id.
     *
     * @return the category id
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * Sets category id.
     *
     * @param categoryId the category id
     */
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * Gets current page.
     *
     * @return the current page
     */
    public String getCurrentPage() {
        return currentPage;
    }

    /**
     * Sets current page.
     *
     * @param currentPage the current page
     */
    public void setCurrentPage(String currentPage) {
        this.currentPage = currentPage;
    }

    @Override
    public String toString() {


        return "ribbon : " + ribbon;
    }
}
