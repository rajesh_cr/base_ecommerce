
package com.prestashopemc.model;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * <h1>Category!</h1>
 * The Category is used to implements getter and setter for Category table.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

@DatabaseTable(tableName = "Category")
public class Category implements ParentListItem {

    @DatabaseField(generatedId = true)
    private int id;
    @SerializedName("id_category")
    @Expose
    @DatabaseField
    private String idCategory;
    @SerializedName("name")
    @Expose
    @DatabaseField
    private String name;
    @SerializedName("category_count")
    @Expose
    @DatabaseField
    private Integer categoryCount;

    @SerializedName("product_count")
    @Expose
    @DatabaseField
    private Integer productCount;

    @SerializedName("subcategory")
    @Expose

    @ForeignCollectionField
    private Collection<ExpandCategory> categories = new ArrayList<ExpandCategory>();

    /**
     * Gets id category.
     *
     * @return The      idCategory
     */
    public String getIdCategory() {
        return idCategory;
    }

    /**
     * Sets id category.
     *
     * @param idCategory The id_category
     */
    public void setIdCategory(String idCategory) {
        this.idCategory = idCategory;
    }

    /**
     * Gets name.
     *
     * @return The      name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets category count.
     *
     * @return The      categoryCount
     */
    public Integer getCategoryCount() {
        return categoryCount;
    }

    /**
     * Sets category count.
     *
     * @param categoryCount The category_count
     */
    public void setCategoryCount(Integer categoryCount) {
        this.categoryCount = categoryCount;
    }

    /**
     * Gets categories.
     *
     * @return the categories
     */
    public Collection<ExpandCategory> getCategories() {
        return categories;
    }

    /**
     * Sets categories.
     *
     * @param categories the categories
     */
    public void setCategories(List<ExpandCategory> categories) {
        this.categories = categories;
    }


    @Override
    public Collection<ExpandCategory> getChildItemList() {
        return categories;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }

    /**
     * Gets product count.
     *
     * @return the product count
     */
    public Integer getProductCount() {
        return productCount;
    }

    /**
     * Sets product count.
     *
     * @param productCount the product count
     */
    public void setProductCount(Integer productCount) {
        this.productCount = productCount;
    }

    /**
     * Sets categories.
     *
     * @param categories the categories
     */
    public void setCategories(Collection<ExpandCategory> categories) {
        this.categories = categories;
    }
}
