
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>Product Detail Main!</h1>
 * The Product Detail Main is used to implements getter and setter for Product Detail Main response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class MagentoProductDetailMain {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("result")
    @Expose
    private MagentoProductDetail prdDetail;

    /**
     * Gets status.
     *
     * @return The      status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Gets detail.
     *
     * @return The      result
     */
    public MagentoProductDetail getprdDetail() {
        return prdDetail;
    }

    /**
     * Sets detail.
     *
     * @param prdDetail The result
     */
    public void setprdDetail(MagentoProductDetail prdDetail) {
        this.prdDetail = prdDetail;
    }

}
