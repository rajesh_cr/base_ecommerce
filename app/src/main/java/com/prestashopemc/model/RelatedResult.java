
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * <h1>Related Result!</h1>
 * The Related Result is used to implements getter and setter for Related Result response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class RelatedResult {

    @SerializedName("product_count")
    @Expose
    private Integer productCount;
    @SerializedName("category_products")
    @Expose
    private List<CategoryProduct> categoryProducts = null;

    /**
     * Gets product count.
     *
     * @return The      productCount
     */
    public Integer getProductCount() {
        return productCount;
    }

    /**
     * Sets product count.
     *
     * @param productCount The product_count
     */
    public void setProductCount(Integer productCount) {
        this.productCount = productCount;
    }

    /**
     * Gets category products.
     *
     * @return The      categoryProducts
     */
    public List<CategoryProduct> getCategoryProducts() {
        return categoryProducts;
    }

    /**
     * Sets category products.
     *
     * @param categoryProducts The category_products
     */
    public void setCategoryProducts(List<CategoryProduct> categoryProducts) {
        this.categoryProducts = categoryProducts;
    }

}
