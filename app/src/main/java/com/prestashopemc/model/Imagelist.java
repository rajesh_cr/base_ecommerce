
package com.prestashopemc.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * <h1>Image list!</h1>
 * The Image list is used to implements getter and setter for Image list table.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

@DatabaseTable(tableName = "Imagelist")
public class Imagelist {

    @DatabaseField
    private String slotPosition;
    @SerializedName("imageUrl")
    @Expose
    @DatabaseField
    private String imageUrl;
    @SerializedName("type")
    @Expose
    @DatabaseField
    private String type;
    @SerializedName("product_id")
    @Expose
    @DatabaseField
    private String productId;
    @SerializedName("is_type")
    @Expose
    @DatabaseField
    private Boolean isType;

    @SerializedName("banner_id")
    @Expose
    @DatabaseField
    private String bannerId;

    @SerializedName("category_id")
    @Expose
    @DatabaseField
    private String categoryId;

    @SerializedName("category_name")
    @Expose
    @DatabaseField
    private String categoryName;

    @SerializedName("product_name")
    @Expose
    @DatabaseField
    private String productName;

    @SerializedName("searchword")
    @Expose
    @DatabaseField
    private String searchWord;

    @SerializedName("product_count")
    @Expose
    @DatabaseField
    private int productCount;


    @SerializedName("category_count")
    @Expose
    @DatabaseField
    private int categoryCount;


    /**
     * Sets type.
     *
     * @param type the type
     */
    public void setType(Boolean type) {
        isType = type;
    }

    /**
     * Gets product name.
     *
     * @return the product name
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Sets product name.
     *
     * @param productName the product name
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * Gets image url.
     *
     * @return The imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * Sets image url.
     *
     * @param imageUrl The imageUrl
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * Gets type.
     *
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * Sets type.
     *
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * Gets product id.
     *
     * @return The productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     * Sets product id.
     *
     * @param productId The product_id
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * Gets is type.
     *
     * @return The isType
     */
    public Boolean getIsType() {
        return isType;
    }

    /**
     * Sets is type.
     *
     * @param isType The is_type
     */
    public void setIsType(Boolean isType) {
        this.isType = isType;
    }

    /**
     * Gets banner id.
     *
     * @return The bannerId
     */
    public String getBannerId() {
        return bannerId;
    }

    /**
     * Sets banner id.
     *
     * @param bannerId The banner_id
     */
    public void setBannerId(String bannerId) {
        this.bannerId = bannerId;
    }


    /**
     * Gets slot position.
     *
     * @return the slot position
     */
    public String getSlotPosition() {
        return slotPosition;
    }

    /**
     * Sets slot position.
     *
     * @param slotPosition the slot position
     */
    public void setSlotPosition(String slotPosition) {
        this.slotPosition = slotPosition;
    }

    /**
     * Gets category id.
     *
     * @return the category id
     */
    public String getCategoryId() {
        return categoryId;
    }

    /**
     * Sets category id.
     *
     * @param categoryId the category id
     */
    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    /**
     * Gets category name.
     *
     * @return the category name
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * Sets category name.
     *
     * @param categoryName the category name
     */
    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    /**
     * Gets search word.
     *
     * @return the search word
     */
    public String getSearchWord() {
        return searchWord;
    }

    /**
     * Sets search word.
     *
     * @param searchWord the search word
     */
    public void setSearchWord(String searchWord) {
        this.searchWord = searchWord;
    }

    /**
     * Gets product count.
     *
     * @return the product count
     */
    public int getProductCount() {
        return productCount;
    }

    /**
     * Sets product count.
     *
     * @param productCount the product count
     */
    public void setProductCount(int productCount) {
        this.productCount = productCount;
    }

    /**
     * Gets category count.
     *
     * @return the category count
     */
    public int getCategoryCount() {
        return categoryCount;
    }

    /**
     * Sets category count.
     *
     * @param categoryCount the category count
     */
    public void setCategoryCount(int categoryCount) {
        this.categoryCount = categoryCount;
    }
}
