
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>Address Main Model!</h1>
 * The Address Main Model is used to implements getter and setter for main address api response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class AddressMainModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("address")
    @Expose
    private Address address;

    /**
     * Gets status.
     *
     * @return The      status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Gets address.
     *
     * @return The      address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * Sets address.
     *
     * @param address The address
     */
    public void setAddress(Address address) {
        this.address = address;
    }

}
