
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>Product item info!</h1>
 * The Product item info is used to implements getter and setter for Product item info response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class Productiteminfo {

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName(value="namee", alternate={"id_product", "product_id"})
    @Expose
    private String productId;
    @SerializedName("supplier_message")
    @Expose
    private String supplierMessage;
    @SerializedName("is_tax_dispay")
    @Expose
    private String isTaxDispay;
    @SerializedName("base_row_total")
    @Expose
    private String baseRowTotal;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("special_price")
    @Expose
    private String specialPrice;
    @SerializedName("qty")
    @Expose
    private Integer qty;
    @SerializedName("id_product_attribute")
    @Expose
    private String idProductAttribute;
    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("item_id")
    @Expose
    private String itemId;
    @SerializedName("base_price")
    @Expose
    private String basePrice;
    @SerializedName("stock")
    @Expose
    private Boolean stock;

    public String getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(String basePrice) {
        this.basePrice = basePrice;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    /**
     * Gets name.
     *
     * @return The      name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets product name.
     *
     * @return The      productName
     */
    public String getProductName() {
        return productName;
    }

    /**
     * Sets product name.
     *
     * @param productName The product_name
     */
    public void setProductName(String productName) {
        this.productName = productName;
    }

    /**
     * Gets product id.
     *
     * @return The      productId
     */
    public String getProductId() {
        return productId;
    }

    /**
     * Sets product id.
     *
     * @param productId The product_id
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }

    /**
     * Gets supplier message.
     *
     * @return The      supplierMessage
     */
    public String getSupplierMessage() {
        return supplierMessage;
    }

    /**
     * Sets supplier message.
     *
     * @param supplierMessage The supplier_message
     */
    public void setSupplierMessage(String supplierMessage) {
        this.supplierMessage = supplierMessage;
    }

    /**
     * Gets is tax dispay.
     *
     * @return The      isTaxDispay
     */
    public String getIsTaxDispay() {
        return isTaxDispay;
    }

    /**
     * Sets is tax dispay.
     *
     * @param isTaxDispay The is_tax_dispay
     */
    public void setIsTaxDispay(String isTaxDispay) {
        this.isTaxDispay = isTaxDispay;
    }

    /**
     * Gets base row total.
     *
     * @return The      baseRowTotal
     */
    public String getBaseRowTotal() {
        return baseRowTotal;
    }

    /**
     * Sets base row total.
     *
     * @param baseRowTotal The base_row_total
     */
    public void setBaseRowTotal(String baseRowTotal) {
        this.baseRowTotal = baseRowTotal;
    }

    /**
     * Gets price.
     *
     * @return The      price
     */
    public String getPrice() {
        return price;
    }

    /**
     * Sets price.
     *
     * @param price The price
     */
    public void setPrice(String price) {
        this.price = price;
    }


    /**
     * Gets special price.
     *
     * @return the special price
     */
    public String getSpecialPrice() {
        return specialPrice;
    }

    /**
     * Sets special price.
     *
     * @param specialPrice the special price
     */
    public void setSpecialPrice(String specialPrice) {
        this.specialPrice = specialPrice;
    }

    /**
     * Gets qty.
     *
     * @return The      qty
     */
    public Integer getQty() {
        return qty;
    }

    /**
     * Sets qty.
     *
     * @param qty The qty
     */
    public void setQty(Integer qty) {
        this.qty = qty;
    }

    /**
     * Gets id product attribute.
     *
     * @return The      idProductAttribute
     */
    public String getIdProductAttribute() {
        return idProductAttribute;
    }

    /**
     * Sets id product attribute.
     *
     * @param idProductAttribute The id_product_attribute
     */
    public void setIdProductAttribute(String idProductAttribute) {
        this.idProductAttribute = idProductAttribute;
    }

    /**
     * Gets image url.
     *
     * @return The      imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * Sets image url.
     *
     * @param imageUrl The image_url
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * Gets stock.
     *
     * @return The      stock
     */
    public Boolean getStock() {
        return stock;
    }

    /**
     * Sets stock.
     *
     * @param stock The stock
     */
    public void setStock(Boolean stock) {
        this.stock = stock;
    }

}
