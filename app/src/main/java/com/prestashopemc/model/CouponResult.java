
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>Coupon Result!</h1>
 * The Coupon Result is used to implements getter and setter for Coupon Result response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class CouponResult {

    @SerializedName("id_rule")
    @Expose
    private String idRule;
    @SerializedName("prices")
    @Expose
    private Prices prices;

    /**
     * Gets id rule.
     *
     * @return The      idRule
     */
    public String getIdRule() {
        return idRule;
    }

    /**
     * Sets id rule.
     *
     * @param idRule The id_rule
     */
    public void setIdRule(String idRule) {
        this.idRule = idRule;
    }

    /**
     * Gets prices.
     *
     * @return The      prices
     */
    public Prices getPrices() {
        return prices;
    }

    /**
     * Sets prices.
     *
     * @param prices The prices
     */
    public void setPrices(Prices prices) {
        this.prices = prices;
    }

}
