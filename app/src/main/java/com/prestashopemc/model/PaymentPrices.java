
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>Payment Prices!</h1>
 * The Payment Prices is used to implements getter and setter for Payment Prices response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class PaymentPrices {

    @SerializedName("coupon_title")
    @Expose
    private String couponTitle;
    @SerializedName("coupon_id")
    @Expose
    private String couponId;
    @SerializedName("subtotal")
    @Expose
    private String subtotal;
    @SerializedName("shipping_amount")
    @Expose
    private String shippingAmount;
    @SerializedName("discount")
    @Expose
    private String discount;
    @SerializedName("total_price_without_tax")
    @Expose
    private String totalPriceWithoutTax;
    @SerializedName("grandtotal")
    @Expose
    private String grandtotal;
    @SerializedName("is_tax_dispay")
    @Expose
    private String isTaxDispay;
    @SerializedName("tax")
    @Expose
    private String tax;

    /**
     * Gets coupon title.
     *
     * @return The      couponTitle
     */
    public String getCouponTitle() {
        return couponTitle;
    }

    /**
     * Sets coupon title.
     *
     * @param couponTitle The coupon_title
     */
    public void setCouponTitle(String couponTitle) {
        this.couponTitle = couponTitle;
    }

    /**
     * Gets coupon id.
     *
     * @return The      couponId
     */
    public String getCouponId() {
        return couponId;
    }

    /**
     * Sets coupon id.
     *
     * @param couponId The coupon_id
     */
    public void setCouponId(String couponId) {
        this.couponId = couponId;
    }

    /**
     * Gets subtotal.
     *
     * @return The      subtotal
     */
    public String getSubtotal() {
        return subtotal;
    }

    /**
     * Sets subtotal.
     *
     * @param subtotal The subtotal
     */
    public void setSubtotal(String subtotal) {
        this.subtotal = subtotal;
    }

    /**
     * Gets shipping amount.
     *
     * @return The      shippingAmount
     */
    public String getShippingAmount() {
        return shippingAmount;
    }

    /**
     * Sets shipping amount.
     *
     * @param shippingAmount The shipping_amount
     */
    public void setShippingAmount(String shippingAmount) {
        this.shippingAmount = shippingAmount;
    }

    /**
     * Gets discount.
     *
     * @return The      discount
     */
    public String getDiscount() {
        return discount;
    }

    /**
     * Sets discount.
     *
     * @param discount The discount
     */
    public void setDiscount(String discount) {
        this.discount = discount;
    }

    /**
     * Gets total price without tax.
     *
     * @return The      totalPriceWithoutTax
     */
    public String getTotalPriceWithoutTax() {
        return totalPriceWithoutTax;
    }

    /**
     * Sets total price without tax.
     *
     * @param totalPriceWithoutTax The total_price_without_tax
     */
    public void setTotalPriceWithoutTax(String totalPriceWithoutTax) {
        this.totalPriceWithoutTax = totalPriceWithoutTax;
    }

    /**
     * Gets grandtotal.
     *
     * @return The      grandtotal
     */
    public String getGrandtotal() {
        return grandtotal;
    }

    /**
     * Sets grandtotal.
     *
     * @param grandtotal The grandtotal
     */
    public void setGrandtotal(String grandtotal) {
        this.grandtotal = grandtotal;
    }

    /**
     * Gets is tax dispay.
     *
     * @return The      isTaxDispay
     */
    public String getIsTaxDispay() {
        return isTaxDispay;
    }

    /**
     * Sets is tax dispay.
     *
     * @param isTaxDispay The is_tax_dispay
     */
    public void setIsTaxDispay(String isTaxDispay) {
        this.isTaxDispay = isTaxDispay;
    }

    /**
     * Gets tax.
     *
     * @return The      tax
     */
    public String getTax() {
        return tax;
    }

    /**
     * Sets tax.
     *
     * @param tax The tax
     */
    public void setTax(String tax) {
        this.tax = tax;
    }

}
