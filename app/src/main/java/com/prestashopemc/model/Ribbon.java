
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.j256.ormlite.field.DatabaseField;

import java.io.Serializable;

/**
 * <h1>Ribbon!</h1>
 * The Ribbon is used to implements getter and setter for Ribbon response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class Ribbon implements Serializable {
    @DatabaseField
    private String productId;
    @DatabaseField
    @SerializedName("ribbon_image")
    @Expose
    private String ribbonImage;
    @DatabaseField
    @SerializedName("left_top")
    @Expose
    private String leftTop;
    @DatabaseField
    @SerializedName("left_bottom")
    @Expose
    private String leftBottom;
    @DatabaseField
    @SerializedName("right_top")
    @Expose
    private String rightTop;
    @DatabaseField
    @SerializedName("right_bottom")
    @Expose
    private String rightBottom;
    @DatabaseField
    @SerializedName("width")
    @Expose
    private Integer width;
    @DatabaseField
    @SerializedName("height")
    @Expose
    private Integer height;

    /**
     * Gets ribbon image.
     *
     * @return The      ribbonImage
     */
    public String getRibbonImage() {
        return ribbonImage;
    }

    /**
     * Sets ribbon image.
     *
     * @param ribbonImage The ribbon_image
     */
    public void setRibbonImage(String ribbonImage) {
        this.ribbonImage = ribbonImage;
    }

    /**
     * Gets left top.
     *
     * @return The      leftTop
     */
    public String getLeftTop() {
        return leftTop;
    }

    /**
     * Sets left top.
     *
     * @param leftTop The left_top
     */
    public void setLeftTop(String leftTop) {
        this.leftTop = leftTop;
    }

    /**
     * Gets left bottom.
     *
     * @return The      leftBottom
     */
    public String getLeftBottom() {
        return leftBottom;
    }

    /**
     * Sets left bottom.
     *
     * @param leftBottom The left_bottom
     */
    public void setLeftBottom(String leftBottom) {
        this.leftBottom = leftBottom;
    }

    /**
     * Gets right top.
     *
     * @return The      rightTop
     */
    public String getRightTop() {
        return rightTop;
    }

    /**
     * Sets right top.
     *
     * @param rightTop The right_top
     */
    public void setRightTop(String rightTop) {
        this.rightTop = rightTop;
    }

    /**
     * Gets right bottom.
     *
     * @return The      rightBottom
     */
    public String getRightBottom() {
        return rightBottom;
    }

    /**
     * Sets right bottom.
     *
     * @param rightBottom The right_bottom
     */
    public void setRightBottom(String rightBottom) {
        this.rightBottom = rightBottom;
    }

    /**
     * Gets width.
     *
     * @return The      width
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * Sets width.
     *
     * @param width The width
     */
    public void setWidth(Integer width) {
        this.width = width;
    }

    /**
     * Gets height.
     *
     * @return The      height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * Sets height.
     *
     * @param height The height
     */
    public void setHeight(Integer height) {
        this.height = height;
    }


    /**
     * Gets product id.
     *
     * @return the product id
     */
    public String getProductId() {
        return productId;
    }

    /**
     * Sets product id.
     *
     * @param productId the product id
     */
    public void setProductId(String productId) {
        this.productId = productId;
    }
    @Override
    public String toString() {
        return "productId:" + productId+ " ribbon_image: " + ribbonImage;
    }


}
