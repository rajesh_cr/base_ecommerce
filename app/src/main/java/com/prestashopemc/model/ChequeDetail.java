package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>Cheque Detail!</h1>
 * The Cheque Detail is used to implements getter and setter for Cheque Detail response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class ChequeDetail {

    @SerializedName("cheque_name")
    @Expose
    private String chequeName;
    @SerializedName("cheque_address")
    @Expose
    private String chequeAddress;

    /**
     * Gets cheque name.
     *
     * @return the cheque name
     */
    public String getChequeName() {
        return chequeName;
    }

    /**
     * Sets cheque name.
     *
     * @param chequeName the cheque name
     */
    public void setChequeName(String chequeName) {
        this.chequeName = chequeName;
    }

    /**
     * Gets cheque address.
     *
     * @return the cheque address
     */
    public String getChequeAddress() {
        return chequeAddress;
    }

    /**
     * Sets cheque address.
     *
     * @param chequeAddress the cheque address
     */
    public void setChequeAddress(String chequeAddress) {
        this.chequeAddress = chequeAddress;
    }
}
