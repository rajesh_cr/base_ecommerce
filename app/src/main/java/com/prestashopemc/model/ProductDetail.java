
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Product Detail!</h1>
 * The Product Detail is used to implements getter and setter for Product Detail response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class ProductDetail {

    @SerializedName("productinfo")
    @Expose
    private Productinfo productinfo;
    @SerializedName("sku")
    @Expose
    private String sku;
    @SerializedName("image")
    @Expose
    private List<String> image = new ArrayList<String>();
    @SerializedName("product_reviews")
    @Expose
    private List<Object> productReviews = new ArrayList<Object>();
    @SerializedName("features")
    @Expose
    private List<Feature> features = new ArrayList<Feature>();
    @SerializedName("estimated_shipping_from")
    @Expose
    private String estimatedShippingFrom;
    @SerializedName("estimated_shipping_to")
    @Expose
    private String estimatedShippingTo;
    @SerializedName("shipping_notes")
    @Expose
    private Object shippingNotes;
    @SerializedName("average_rating")
    @Expose
    private double averageRating;
    @SerializedName("review_count")
    @Expose
    private int reviewCount;
    @SerializedName("keyvalues")
    @Expose
    private List<KeyValues> keyValuesList = new ArrayList<KeyValues>();
    @SerializedName("custom_attributes")
    @Expose
    private List<CustomAttribute> customAttributes = new ArrayList<CustomAttribute>();
    @SerializedName("is_custom_required")
    @Expose
    private Object isCustomRequired;
    @SerializedName("is_custom")
    @Expose
    private Integer isCustom;
    @SerializedName("ribbon")
    @Expose
    private Ribbon ribbon;
    @SerializedName("wishlist")
    @Expose
    private Boolean wishlist;
    @SerializedName("guest_comment")
    @Expose
    private Boolean guestComment;

    /**
     * Gets guest comment.
     *
     * @return the guest comment
     */
    public Boolean getGuestComment() {
        return guestComment;
    }

    /**
     * Sets guest comment.
     *
     * @param guestComment the guest comment
     */
    public void setGuestComment(Boolean guestComment) {
        this.guestComment = guestComment;
    }

    /**
     * Gets average rating.
     *
     * @return the average rating
     */
    public double getAverageRating() {
        return averageRating;
    }

    /**
     * Sets average rating.
     *
     * @param averageRating the average rating
     */
    public void setAverageRating(double averageRating) {
        this.averageRating = averageRating;
    }

    /**
     * Gets review count.
     *
     * @return the review count
     */
    public int getReviewCount() {
        return reviewCount;
    }

    /**
     * Sets review count.
     *
     * @param reviewCount the review count
     */
    public void setReviewCount(int reviewCount) {
        this.reviewCount = reviewCount;
    }

    /**
     * Gets productinfo.
     *
     * @return The      productinfo
     */
    public Productinfo getProductinfo() {
        return productinfo;
    }

    /**
     * Sets productinfo.
     *
     * @param productinfo The productinfo
     */
    public void setProductinfo(Productinfo productinfo) {
        this.productinfo = productinfo;
    }

    /**
     * Gets sku.
     *
     * @return The      sku
     */
    public String getSku() {
        return sku;
    }

    /**
     * Sets sku.
     *
     * @param sku The sku
     */
    public void setSku(String sku) {
        this.sku = sku;
    }

    /**
     * Gets image.
     *
     * @return The      image
     */
    public List<String> getImage() {
        return image;
    }

    /**
     * Sets image.
     *
     * @param image The image
     */
    public void setImage(List<String> image) {
        this.image = image;
    }

    /**
     * Gets product reviews.
     *
     * @return The      productReviews
     */
    public List<Object> getProductReviews() {
        return productReviews;
    }

    /**
     * Sets product reviews.
     *
     * @param productReviews The product_reviews
     */
    public void setProductReviews(List<Object> productReviews) {
        this.productReviews = productReviews;
    }

    /**
     * Gets features.
     *
     * @return The      features
     */
    public List<Feature> getFeatures() {
        return features;
    }

    /**
     * Sets features.
     *
     * @param features The features
     */
    public void setFeatures(List<Feature> features) {
        this.features = features;
    }

    /**
     * Gets estimated shipping from.
     *
     * @return The      estimatedShipping
     */
    public String getEstimatedShippingFrom() {
        return estimatedShippingFrom;
    }

    /**
     * Sets estimated shipping from.
     *
     * @param estimatedShippingFrom The estimated_shipping
     */
    public void setEstimatedShippingFrom(String estimatedShippingFrom) {
        this.estimatedShippingFrom = estimatedShippingFrom;
    }

    /**
     * Gets estimated shipping to.
     *
     * @return the estimated shipping to
     */
    public String getEstimatedShippingTo() {
        return estimatedShippingTo;
    }

    /**
     * Sets estimated shipping to.
     *
     * @param estimatedShippingTo the estimated shipping to
     */
    public void setEstimatedShippingTo(String estimatedShippingTo) {
        this.estimatedShippingTo = estimatedShippingTo;
    }

    /**
     * Gets shipping notes.
     *
     * @return The      shippingNotes
     */
    public Object getShippingNotes() {
        return shippingNotes;
    }

    /**
     * Sets shipping notes.
     *
     * @param shippingNotes The shipping_notes
     */
    public void setShippingNotes(Object shippingNotes) {
        this.shippingNotes = shippingNotes;
    }

    /**
     * Gets key values list.
     *
     * @return the key values list
     */
    public List<KeyValues> getKeyValuesList() {
        return keyValuesList;
    }

    /**
     * Sets key values list.
     *
     * @param keyValuesList the key values list
     */
    public void setKeyValuesList(List<KeyValues> keyValuesList) {
        this.keyValuesList = keyValuesList;
    }

    /**
     * Gets custom attributes.
     *
     * @return The      customAttributes
     */
    public List<CustomAttribute> getCustomAttributes() {
        return customAttributes;
    }

    /**
     * Sets custom attributes.
     *
     * @param customAttributes The custom_attributes
     */
    public void setCustomAttributes(List<CustomAttribute> customAttributes) {
        this.customAttributes = customAttributes;
    }

    /**
     * Gets is custom required.
     *
     * @return The      isCustomRequired
     */
    public Object getIsCustomRequired() {
        return isCustomRequired;
    }

    /**
     * Sets is custom required.
     *
     * @param isCustomRequired The is_custom_required
     */
    public void setIsCustomRequired(Object isCustomRequired) {
        this.isCustomRequired = isCustomRequired;
    }

    /**
     * Gets is custom.
     *
     * @return The      isCustom
     */
    public Integer getIsCustom() {
        return isCustom;
    }

    /**
     * Sets is custom.
     *
     * @param isCustom The is_custom
     */
    public void setIsCustom(Integer isCustom) {
        this.isCustom = isCustom;
    }

    /**
     * Gets ribbon.
     *
     * @return The      ribbon
     */
    public Ribbon getRibbon() {
        return ribbon;
    }

    /**
     * Sets ribbon.
     *
     * @param ribbon The ribbon
     */
    public void setRibbon(Ribbon ribbon) {
        this.ribbon = ribbon;
    }

    /**
     * Gets wishlist.
     *
     * @return The      wishlist
     */
    public Boolean getWishlist() {
        return wishlist;
    }

    /**
     * Sets wishlist.
     *
     * @param wishlist The wishlist
     */
    public void setWishlist(Boolean wishlist) {
        this.wishlist = wishlist;
    }

}
