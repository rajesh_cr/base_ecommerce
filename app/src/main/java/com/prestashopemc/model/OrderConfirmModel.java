
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>Order Confirm Model!</h1>
 * The Order Confirm Model is used to implements getter and setter for Order Confirm Model response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class OrderConfirmModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("result")
    @Expose
    private OrderConfirmResult result;

    /**
     * Gets status.
     *
     * @return The      status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Gets result.
     *
     * @return The      result
     */
    public OrderConfirmResult getResult() {
        return result;
    }

    /**
     * Sets result.
     *
     * @param result The result
     */
    public void setResult(OrderConfirmResult result) {
        this.result = result;
    }

}
