
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>My Order Detail Item!</h1>
 * The My Order Detail Item is used to implements getter and setter for My Order Detail Item response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class MyOrderDetailItem {

    @SerializedName("id_product")
    @Expose
    private String idProduct;
    @SerializedName("product_attribute_id")
    @Expose
    private String productAttributeId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("qty")
    @Expose
    private String qty;
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("row_total")
    @Expose
    private String rowTotal;
    @SerializedName("image")
    @Expose
    private String image;

    /**
     * Gets id product.
     *
     * @return The      idProduct
     */
    public String getIdProduct() {
        return idProduct;
    }

    /**
     * Sets id product.
     *
     * @param idProduct The id_product
     */
    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    /**
     * Gets product attribute id.
     *
     * @return The      productAttributeId
     */
    public String getProductAttributeId() {
        return productAttributeId;
    }

    /**
     * Sets product attribute id.
     *
     * @param productAttributeId The product_attribute_id
     */
    public void setProductAttributeId(String productAttributeId) {
        this.productAttributeId = productAttributeId;
    }

    /**
     * Gets name.
     *
     * @return The      name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * Gets qty.
     *
     * @return The      qty
     */
    public String getQty() {
        return qty;
    }

    /**
     * Sets qty.
     *
     * @param qty The qty
     */
    public void setQty(String qty) {
        this.qty = qty;
    }

    /**
     * Gets price.
     *
     * @return The      price
     */
    public String getPrice() {
        return price;
    }

    /**
     * Sets price.
     *
     * @param price The price
     */
    public void setPrice(String price) {
        this.price = price;
    }

    /**
     * Gets row total.
     *
     * @return The      rowTotal
     */
    public String getRowTotal() {
        return rowTotal;
    }

    /**
     * Sets row total.
     *
     * @param rowTotal The row_total
     */
    public void setRowTotal(String rowTotal) {
        this.rowTotal = rowTotal;
    }

    /**
     * Gets image.
     *
     * @return The      image
     */
    public String getImage() {
        return image;
    }

    /**
     * Sets image.
     *
     * @param image The image
     */
    public void setImage(String image) {
        this.image = image;
    }

}
