package com.prestashopemc.model;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


/**
 * <h1>Assigned product!</h1>
 * The Assigned product is used to implements getter and setter for Assigned product response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class Assignedproduct {

    @SerializedName("status")
    @Expose
    private Boolean status;

    @SerializedName("assignedproducts")
    @Expose
    private List<Product> assignedproducts = new ArrayList<Product>();

    @SerializedName("product_count")
    @Expose
    private Integer productCount;

    /**
     * Gets status.
     *
     * @return The status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * Gets products.
     *
     * @return The products
     */
    public List<Product> getProducts() {
        return assignedproducts;
    }

    /**
     * Sets products.
     *
     * @param products The products
     */
    public void setProducts(List<Product> products) {
        this.assignedproducts = products;
    }

    /**
     * Gets product count.
     *
     * @return The productCount
     */
    public Integer getProductCount() {
        return productCount;
    }

    /**
     * Sets product count.
     *
     * @param productCount The product_count
     */
    public void setProductCount(Integer productCount) {
        this.productCount = productCount;
    }

}
