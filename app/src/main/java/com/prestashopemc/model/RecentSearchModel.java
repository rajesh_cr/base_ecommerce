package com.prestashopemc.model;

import com.j256.ormlite.field.DatabaseField;

/**
 * <h1>Recent Search Model!</h1>
 * The Recent Search Model is used to implements getter and setter for Recent Search Model response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class RecentSearchModel {

    /**
     * The Id.
     */
    @DatabaseField(generatedId = true)
    int id;
    @DatabaseField(columnName = "searchKeyword", unique = true)
    private String searchKeyword;

    @DatabaseField
    private String customerId;

    /**
     * Gets id.
     *
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * Sets id.
     *
     * @param id the id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Gets search keyword.
     *
     * @return the search keyword
     */
    public String getSearchKeyword() {
        return searchKeyword;
    }

    /**
     * Sets search keyword.
     *
     * @param searchKeyword the search keyword
     */
    public void setSearchKeyword(String searchKeyword) {
        this.searchKeyword = searchKeyword;
    }

    /**
     * Gets customer id.
     *
     * @return the customer id
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Sets customer id.
     *
     * @param customerId the customer id
     */
    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }
}
