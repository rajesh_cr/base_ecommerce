
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>Filter Category Result!</h1>
 * The Filter Category Result is used to implements getter and setter for Filter Category Result response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class FilterCategoryResult {

    @SerializedName("id_category")
    @Expose
    private String idCategory;
    @SerializedName("name")
    @Expose
    private String name;

    /**
     * Gets id category.
     *
     * @return The      idCategory
     */
    public String getIdCategory() {
        return idCategory;
    }

    /**
     * Sets id category.
     *
     * @param idCategory The id_category
     */
    public void setIdCategory(String idCategory) {
        this.idCategory = idCategory;
    }

    /**
     * Gets name.
     *
     * @return The      name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets name.
     *
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

}
