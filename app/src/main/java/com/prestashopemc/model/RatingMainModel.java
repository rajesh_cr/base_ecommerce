
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>Rating Main Model!</h1>
 * The Rating Main Model is used to implements getter and setter for Rating Main Model response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class RatingMainModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("result")
    @Expose
    private List<RatingResult> result = new ArrayList<RatingResult>();

    /**
     * Gets status.
     *
     * @return The      status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Gets result.
     *
     * @return The      result
     */
    public List<RatingResult> getResult() {
        return result;
    }

    /**
     * Sets result.
     *
     * @param result The result
     */
    public void setResult(List<RatingResult> result) {
        this.result = result;
    }

}
