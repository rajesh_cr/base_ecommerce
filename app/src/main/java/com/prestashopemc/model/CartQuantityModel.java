
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>Cart Quantity Model!</h1>
 * The Cart Quantity Model is used to implements getter and setter for Cart Quantity Model response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class CartQuantityModel {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("result")
    @Expose
    private CartQuantityResult result;

    /**
     * Gets status.
     *
     * @return The      status
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * Gets result.
     *
     * @return The      result
     */
    public CartQuantityResult getResult() {
        return result;
    }

    /**
     * Sets result.
     *
     * @param result The result
     */
    public void setResult(CartQuantityResult result) {
        this.result = result;
    }

}
