
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * <h1>License Result!</h1>
 * The License Result is used to implements getter and setter for License Result response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */

public class LicenseResult {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("errormsg")
    @Expose
    private String errormsg;
    @SerializedName("currency_symbol")
    @Expose
    private String currencySymbol;
    @SerializedName("egrsecuritykey")
    @Expose
    private String egrsecuritykey;
    @SerializedName("theme_color")
    @Expose
    private String themeColor;
    @SerializedName("languages")
    @Expose
    private List<Language> languages = new ArrayList<Language>();
    @SerializedName("default_lang")
    @Expose
    private String defaultLang;
    @SerializedName("currency")
    @Expose
    private List<Currency> currency = new ArrayList<Currency>();
    @SerializedName("default_currency")
    @Expose
    private String defaultCurrency;
    @SerializedName("message")
    @Expose
    private String message;

    /**
     * Gets message.
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets message.
     *
     * @param message the message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * Gets status.
     *
     * @return The      status
     */
    public Boolean getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status The status
     */
    public void setStatus(Boolean status) {
        this.status = status;
    }

    /**
     * Gets errormsg.
     *
     * @return The      errormsg
     */
    public String getErrormsg() {
        return errormsg;
    }

    /**
     * Sets errormsg.
     *
     * @param errormsg The errormsg
     */
    public void setErrormsg(String errormsg) {
        this.errormsg = errormsg;
    }

    /**
     * Gets currency symbol.
     *
     * @return The      currencySymbol
     */
    public String getCurrencySymbol() {
        return currencySymbol;
    }

    /**
     * Sets currency symbol.
     *
     * @param currencySymbol The currency_symbol
     */
    public void setCurrencySymbol(String currencySymbol) {
        this.currencySymbol = currencySymbol;
    }

    /**
     * Gets egrsecuritykey.
     *
     * @return The      egrsecuritykey
     */
    public String getEgrsecuritykey() {
        return egrsecuritykey;
    }

    /**
     * Sets egrsecuritykey.
     *
     * @param egrsecuritykey The egrsecuritykey
     */
    public void setEgrsecuritykey(String egrsecuritykey) {
        this.egrsecuritykey = egrsecuritykey;
    }

    /**
     * Gets theme color.
     *
     * @return The      mThemeColor
     */
    public String getThemeColor() {
        return themeColor;
    }

    /**
     * Sets theme color.
     *
     * @param themeColor The theme_color
     */
    public void setThemeColor(String themeColor) {
        this.themeColor = themeColor;
    }

    /**
     * Gets languages.
     *
     * @return The      languages
     */
    public List<Language> getLanguages() {
        return languages;
    }

    /**
     * Sets languages.
     *
     * @param languages The languages
     */
    public void setLanguages(List<Language> languages) {
        this.languages = languages;
    }

    /**
     * Gets default lang.
     *
     * @return The      defaultLang
     */
    public String getDefaultLang() {
        return defaultLang;
    }

    /**
     * Sets default lang.
     *
     * @param defaultLang The default_lang
     */
    public void setDefaultLang(String defaultLang) {
        this.defaultLang = defaultLang;
    }

    /**
     * Gets currency.
     *
     * @return The      currency
     */
    public List<Currency> getCurrency() {
        return currency;
    }

    /**
     * Sets currency.
     *
     * @param currency The currency
     */
    public void setCurrency(List<Currency> currency) {
        this.currency = currency;
    }

    /**
     * Gets default currency.
     *
     * @return The      defaultCurrency
     */
    public String getDefaultCurrency() {
        return defaultCurrency;
    }

    /**
     * Sets default currency.
     *
     * @param defaultCurrency The default_currency
     */
    public void setDefaultCurrency(String defaultCurrency) {
        this.defaultCurrency = defaultCurrency;
    }

}
