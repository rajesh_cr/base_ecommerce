
package com.prestashopemc.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * <h1>Locator Result!</h1>
 * The Locator Result is used to implements getter and setter for Locator Result response.
 * <p>
 * <b>Note:</b> Giving proper comments in your program makes it more
 * user friendly and it is assumed as a high quality code.
 *
 * @author dinakaran
 * @version 1.2
 * @since 27 /9/16
 */
public class LocatorResult {

    @SerializedName("location_id")
    @Expose
    private String locationId;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("region")
    @Expose
    private Object region;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("zipcode")
    @Expose
    private String zipcode;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("status")
    @Expose
    private Integer status;

    /**
     * Gets location id.
     *
     * @return The      locationId
     */
    public String getLocationId() {
        return locationId;
    }

    /**
     * Sets location id.
     *
     * @param locationId The location_id
     */
    public void setLocationId(String locationId) {
        this.locationId = locationId;
    }

    /**
     * Gets country.
     *
     * @return The      country
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets country.
     *
     * @param country The country
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * Gets region.
     *
     * @return The      region
     */
    public Object getRegion() {
        return region;
    }

    /**
     * Sets region.
     *
     * @param region The region
     */
    public void setRegion(Object region) {
        this.region = region;
    }

    /**
     * Gets city.
     *
     * @return The      city
     */
    public String getCity() {
        return city;
    }

    /**
     * Sets city.
     *
     * @param city The city
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     * Gets address.
     *
     * @return The      address
     */
    public String getAddress() {
        return address;
    }

    /**
     * Sets address.
     *
     * @param address The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * Gets zipcode.
     *
     * @return The      zipcode
     */
    public String getZipcode() {
        return zipcode;
    }

    /**
     * Sets zipcode.
     *
     * @param zipcode The zipcode
     */
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    /**
     * Gets latitude.
     *
     * @return The      latitude
     */
    public Double getLatitude() {
        return latitude;
    }

    /**
     * Sets latitude.
     *
     * @param latitude The latitude
     */
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /**
     * Gets longitude.
     *
     * @return The      longitude
     */
    public Double getLongitude() {
        return longitude;
    }

    /**
     * Sets longitude.
     *
     * @param longitude The longitude
     */
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    /**
     * Gets phone.
     *
     * @return The      phone
     */
    public String getPhone() {
        return phone;
    }

    /**
     * Sets phone.
     *
     * @param phone The phone
     */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
     * Gets status.
     *
     * @return The      status
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * Sets status.
     *
     * @param status The status
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

}
