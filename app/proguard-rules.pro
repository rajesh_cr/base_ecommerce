# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /home/egrove/Android/Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}


##---------------End: proguard configuration for Design,Ads  ----------
#---------------Begin: proguard configuration for Http client library  ----------
-dontwarn org.apache.commons.**
-keep class org.apache.http.** { *; }
-dontwarn org.apache.http.**
#---------------End: proguard configuration for Http client library  ----------
#---------------Begin: proguard configuration for Picasso library  ----------
-dontwarn com.squareup.okhttp.**
#---------------End: proguard configuration for Picasso library  ----------
#---------------Begin: proguard configuration for Cardview----------

-keep class android.support.v7.widget.RoundRectDrawable { *; }
#---------------End: proguard configuration for Cardview library  ----------

-keep class com.prestashopemc.** { *; }

# OrmLite uses reflection
-keep class com.j256.**
-keepclassmembers class com.j256.** { *; }
-keep enum com.j256.**
-keepclassmembers enum com.j256.** { *; }
-keep interface com.j256.**
-keepclassmembers interface com.j256.** { *; }